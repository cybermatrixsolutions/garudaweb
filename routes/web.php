<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','AdminController@login');

Auth::routes();

	

	/*Route::get('/', function () {
	    return view('welcome');
	});*/

Route::group(['middleware'=>'auth'],function(){
    Route::get('/home','AdminController@Dashboard');
	Route::get('masterAdmin','AdminController@login');
	Route::post('DashboardLogin','AdminController@adminLogin');
	Route::get('Dashboard','AdminController@Dashboard')->name('dashboard');


	/* Principal Management Start */

	Route::get('principal_code','AdminController@principalCode');
	Route::post('add_principal','AdminController@add_principal');
	Route::get('principal_listing','AdminController@principalListing');
	Route::get('principalUpdate/{id}','AdminController@principalUpdate');
	Route::post('principalEdit/{id}','AdminController@principalEdit');
	Route::get('principalDeleted/{id}','AdminController@principalDeleted');
	Route::get('principalDeactive/{id}','AdminController@principalDeactive');
	Route::get('principalActive/{id}','AdminController@principalActive');
	/* Principal Management End */


	/* Unit Of Measure Management Start */

	Route::get('unit_of_measure','AdminController@unitMeasure');
	Route::post('add_unit_measure','AdminController@add_unit_measure');
	Route::get('unit_of_measure','AdminController@unit_measure_listing');
	Route::get('unit_measure_update/{id}','AdminController@unit_measure_update');
	Route::post('unit_of_measure/{id}','AdminController@unitMeasureEdit');
	Route::get('unit_measure_delete/{id}','AdminController@unit_measure_delete');
	Route::get('unitMeasureDeactive/{id}','AdminController@unitMeasureDeactive');
	Route::get('unitMeasureActive/{id}','AdminController@unitMeasureActive');
	/* Unit Of Measure Management End */




	/* Stock Item Group Management Start */

	//Route::get('stock_item_group','AdminController@stock_item_group');
	Route::post('add_stock_item_group','AdminController@add_stock_item_group');
	Route::get('stock_item_group','AdminController@stock_item_group_listing');
	Route::get('stock_item_group_update/{id}','AdminController@stock_item_group_update');
	Route::post('stock_item_group_edit/{id}','AdminController@stock_item_group_edit');
	Route::get('stock_item_group_delete/{id}','AdminController@stock_item_group_delete');
	Route::get('stockItemGroupDeactive/{id}','AdminController@stockItemGroupDeactive');
	Route::get('stockItemGroupActive/{id}','AdminController@stockItemGroupActive');
	/* Stock Item Group Management End */


	/* Designation Management Start */

	Route::get('designation','AdminController@designation');
	Route::post('add_designation','AdminController@add_designation');
	Route::get('designation','AdminController@designation_listing');
	Route::get('designation_update/{id}','AdminController@designation_update');
	Route::post('designation_edit/{id}','AdminController@designation_edit');
	Route::get('designation_delete/{id}','AdminController@designation_delete');
	Route::get('designationDeactive/{id}','AdminController@designationDeactive');
	Route::get('designationActive/{id}','AdminController@designationActive');
	/* Designation Management End */


	/* Dip Stick Management Start */

	Route::get('dip_stick','AdminController@dip_stick');
	Route::post('add_dip_stick','AdminController@add_dip_stick');
	Route::get('dip_stick','AdminController@dip_stick_listing');
	Route::get('dip_stick_update/{id}','AdminController@dip_stick_update');
	Route::post('dip_stick_edit/{id}','AdminController@dip_stick_edit');
	Route::get('dip_stick_delete/{id}','AdminController@dip_stick_delete');
	Route::get('dipStickDeactive/{id}','AdminController@dipStickDeactive');
	Route::get('dipStickActive/{id}','AdminController@dipStickActive');
	/* Dip Stick Management End */


	/*  Management Start */

	Route::get('vehicle','AdminController@vehicle');
	Route::post('add_vehicle','AdminController@add_vehicle');
	Route::get('vehicle','AdminController@vehicle_listing');
	Route::get('vehicle_update/{id}','AdminController@vehicle_update');
	Route::post('vehicle_edit/{id}','AdminController@vehicle_edit');
	Route::get('vehicle_delete/{id}','AdminController@vehicle_delete');


	/* sms setting start here */
	Route::get('smssetting','SmsSettingController@index')->name('smssetting');
	Route::post('smssetting/update','SmsSettingController@update')->name('smssetting.update');


	/* Tax Management Start */

	Route::get('vehicle','AdminController@vehicle');
	Route::post('add_vehicle','AdminController@add_vehicle');
	Route::get('vehicle','AdminController@vehicle_listing');
	Route::get('vehicle_update/{id}','AdminController@vehicle_update');
	Route::post('vehicle_edit/{id}','AdminController@vehicle_edit');
	Route::get('vehicle_delete/{id}','AdminController@vehicle_delete');
	Route::get('aadipStickDeactive/{id}','AdminController@dipStickDeactive');
	Route::get('aadipStickActive/{id}','AdminController@dipStickActive');
	 /* Tax Management Start */

	Route::get('tax','AdminController@tax');
	Route::post('add_tax','AdminController@add_tax');
	Route::get('tax','AdminController@tax_listing');
	Route::get('tax_update/{id}','AdminController@tax_update');
	Route::post('tax_edit/{id}','AdminController@tax_edit');
	Route::get('tax_delete/{id}','AdminController@tax_delete');
	Route::get('taxDeactive/{id}','AdminController@taxDeactive');
	Route::get('taxActive/{id}','AdminController@taxActive');
	/* Tax Management End */ 


	/* Accounting Management Start */

	Route::get('accounting','AdminController@accounting');
	Route::post('add_accounting','AdminController@add_accounting');
	Route::get('accounting','AdminController@accounting_listing');
	Route::get('accounting_update/{id}','AdminController@accounting_update');
	Route::post('accounting_edit/{id}','AdminController@accounting_edit');
	Route::get('accounting_delete/{id}','AdminController@accounting_delete');
	Route::get('accountingDeactive/{id}','AdminController@accountingDeactive');
	Route::get('accountingActive/{id}','AdminController@accountingActive');

	/* Accounting Management End */ 

  
    Route::get('getregierRegistration_Number','AdminController@getRegistration_Number');
	Route::get('stock_item_management','StockItemManagementController@index');
	Route::get('getstrocksubgroup','StockItemManagementController@getstocksub');

	Route::get('auth/logout', 'Auth\AuthController@getLogout');



	 /* Retail Outlet  Controller Management Start */


	// Route::get('retail','AdminController@retail');
	// Route::get('pedestal','AdminController@pedestalc');
	Route::post('add_pedestal','AdminController@add_pedestalc');
	Route::get('pedestal','AdminController@pedestalc_listing');
	Route::get('getTankfuel/{id}','NozzleController@getTankfuel');
	Route::get('getTankname/{id}','NozzleController@getTankname');
	Route::get('pedestlDeactive/{id}','AdminController@pedestlDeactive');
	Route::get('pedestlActive/{id}','AdminController@pedestlActive');  
	Route::get('pestal_update/{id}','AdminController@pestal_update');
	Route::post('pedestal_edit/{id}','AdminController@pedestal_edits');
	Route::get('pestal_delete/{id}','AdminController@pestal_delete');
	Route::post('add_retail','AdminController@add_retail');
	Route::get('retail','AdminController@retail_listing');

	Route::any('retails_view/{id}','AdminController@retail_updatea');

	Route::get('retail_update/{id}','AdminController@retail_update');
	Route::post('retail_edit/{id}','AdminController@retail_edit');
	Route::get('retail_delete/{id}','AdminController@retail_delete');
	Route::get('retailDeactive/{id}','AdminController@retailDeactive');
	Route::get('retailActive/{id}','AdminController@retailActive');  
	// Route::get('nozzles','AdminController@nozzlesc');
	Route::post('add_pedestalnew','AdminController@add_pedestalnews');
	Route::get('nozzles','AdminController@pedestalcnew_listing');
	Route::any('pesstalnew_delete/{id}','AdminController@pestalnew_deletes');
	Route::get('pedestlnewDeactive/{id}','AdminController@pedestlDeactivenews');
	Route::get('pedestlnewActive/{id}','AdminController@pedestlnewsActivenews');
	Route::get('pestalnew_update/{id}','AdminController@pestalnew_updateabh');
	Route::post('pedestalnew_edit/{id}','AdminController@pestalnew_edits');
	Route::get('tank','AdminController@tankc');
	Route::get('owner','AdminController@ownerc');

	//

	//Abhishek Working Here //
	// VechilManagment//
	// Route::get('vehiclemanagement','VechilManageController@index');
	// Route::post('vehiclemanagement/update/{id}','VechilManageController@update');
	// Route::get('vehiclemanagement/active/{id}/{actt}','VechilManageController@active');
	// Route::post('vehiclemanagement/deactive/{id}','VechilManageController@deactive');
	// Route::get('vehiclemanagement/edit/{id}','VechilManageController@edit');
	// Route::get('vehiclemanagement/delete/{id}','VechilManageController@delete');
	// Route::post('vechstore','VechilManageController@vechstore');
	//QR_Code Managemanet//
	Route::get('qr_code','Qr_codeController@index');
	//Route::get('qr_code/update/{id}','Qr_codeController@update');
	Route::get('qr_code/active/{id}/{actt}','Qr_codeController@active');
	//Route::get('qr_code/edit/{id}','Qr_codeController@edit');
	Route::get('qr_code/delete/{id}','Qr_codeController@delete');
	Route::post('qr_code','Qr_codeController@add_qr_code');

	//Route::post('getrocode','Credit_limitController@add_credit_limit');

	//Customer Credit Limit //
	Route::post('credit_limitaa','Credit_limitController@add_credit_limit');
	Route::get('credit_limit','Credit_limitController@index');
	Route::get('credit_limit/edit/{id}','Credit_limitController@edit');
	Route::get('credit_limit/active/{id}/{actt}','Credit_limitController@active');
	Route::get('credit_limit/delete/{id}','Credit_limitController@delete');
	Route::post('credit_limit/update/{id}','Credit_limitController@update');

	// manager
	Route::get('manager','RoManagerController@index');
	Route::post('manager/create','RoManagerController@store');
	Route::get('manager/edit/{id}','RoManagerController@edit');
	Route::post('manager/edit/{id}','RoManagerController@update');
	Route::get('manager/delete/{id}','RoManagerController@destroy');
	Route::get('manager/active/{id}/{actt}','RoManagerController@active');
	// end manager 
	Route::get('persoonel','AdminController@personalc');
	Route::get('price_list','RoPieceListManagementController@index');
	//Route::get('Customer','AdminController@customerc');
	Route::get('driver','AdminController@driverc');


	//Route::get('qr_code','AdminController@qr_codec');
	//Route::get('credit_limit','AdminController@credit_limitc');


	// VechilManagment//
	Route::get('vehiclemanagement','VechilManageController@index');
	Route::post('vehiclemanagement/update/{id}','VechilManageController@update');
	Route::get('vehiclemanagement/active/{id}/{actt}','VechilManageController@active');
	Route::post('vehiclemanagement/deactive/{id}','VechilManageController@deactive');
	Route::get('vehiclemanagement/edit/{id}','VechilManageController@edit');
	Route::get('vehiclemanagement/View/{id}','VechilManageController@show');
	Route::get('vehiclemanagement/delete/{id}','VechilManageController@delete');
	Route::post('vechstore','VechilManageController@vechstore');

	//kush rout

	Route::any('save_tank_management','AdminController@saveTankMangementData');
	Route::get('tank_update/{id}','AdminController@editTankPage');
	Route::any('owner_edit_page/{id}','RoOwnerMangerController@edit');
	Route::get('persoonel','AdminController@personalc');
	Route::any('persoonel/edit/{id}','RoPersonalManagementContrller@edit');
	Route::any('persoonel/view/{id}','RoPersonalManagementContrller@view');
	Route::any('tank_edit_data/{id}','AdminController@tankEditData');
	Route::any('delete_tank/{id}','AdminController@DeletetankData');
	Route::any('owner_delete/{id}','RoOwnerMangerController@destroy');
	Route::any('personal_delete_page/{id}','RoPersonalManagementContrller@destroy');
	Route::any('owner_update/{id}','RoOwnerMangerController@update');
	Route::any('personal_update/{id}','RoPersonalManagementContrller@update');
	Route::post('save_owner_management','RoOwnerMangerController@store');
	Route::post('save_personal_management','RoPersonalManagementContrller@store');
	Route::any('save_price_list','RoPieceListManagementController@store');
	Route::any('delete_price/{id}','RoPieceListManagementController@destroy');
	Route::any('price/edit/{id}','RoPieceListManagementController@edit');
	Route::any('price/view/{id}','RoPieceListManagementController@view');
	Route::any('price_update/{id}','RoPieceListManagementController@update');
	Route::get('Customer','AdminController@customerc');
	Route::post('save_customer_data','RoCustomertManagementController@store');
	Route::any('customer/delete/{id}','RoCustomertManagementController@destroy');
	Route::any('customer/edit/{id}','RoCustomertManagementController@edit');
	Route::any('customer/show/{id}','RoCustomertManagementController@show');
	Route::any('item_tax_allo','RoPieceListManagementController@itemshow');
	Route::any('getstockcode','RoPieceListManagementController@getstockgr');
    Route::any('getItembyajex/{pum}/{tags}','RoPieceListManagementController@getItemBYajex');
    Route::any('getrocode1','RoPieceListManagementController@getrotax');
    Route::post('itemtaxsave','RoPieceListManagementController@itemtaxsavetable');
    Route::any('stock-item-master-csv','RoPieceListManagementController@export');
    Route::any('all-price-list-csv-file','RoPieceListManagementController@exportroall');
    Route::post('exmportcsv','RoPieceListManagementController@saveexport');
	Route::any('customer_update/{id}','RoCustomertManagementController@update');
	

	//driver
	Route::get('driver','CustomerDriverManagementController@index');
	Route::post('save_customber_driver_data','CustomerDriverManagementController@store');
	Route::any('customer_driver/delete/{id}','CustomerDriverManagementController@destroy');
	Route::get('customer_driver/edit/{id}','CustomerDriverManagementController@edit');
	Route::any('customer_driver/view/{id}','CustomerDriverManagementController@show');
	Route::post('customer_driver_update/{id}','CustomerDriverManagementController@update');
	Route::any('getrocode','AdminController@getrocode');
	Route::any('getregierid','AdminController@getregierid');
	Route::get('getrocodes','AdminController@getrocodes');
	Route::get('getregierids','AdminController@getregierids');
	Route::get('getTaxoftax','AdminController@getTaxOfTax');
	Route::get('getfueltype/{ro}/{cus}/{type}','AdminController@getFuelType');
	Route::get('driver/active/{id}/{actt}','CustomerDriverManagementController@active');

	Route::any('tankDeactive/{id}','AdminController@tankDeactive');
	Route::any('tankActive/{id}','AdminController@tankActive');
	Route::any('owner_deactive/{id}','RoOwnerMangerController@ownerDeactive');
	Route::any('owner_active/{id}','RoOwnerMangerController@ownerActive');
	Route::get('personalmanagement/active/{id}/{actt}','RoPersonalManagementContrller@active');
	Route::get('pricemanagement/active/{id}/{actt}','RoPieceListManagementController@active');
	Route::get('customermanagement/active/{id}/{actt}','RoCustomertManagementController@active');

	Route::any('save_stock_item_management','StockItemManagementController@store');
	Route::any('stock_edit_page/{id}','StockItemManagementController@edit');
	Route::any('stock_view_page/{id}','StockItemManagementController@show');
	Route::any('stock_update/{id}','StockItemManagementController@update');
	Route::get('stockmanagement/active/{id}/{actt}','StockItemManagementController@active');
	Route::any('stock_delete_page/{id}','StockItemManagementController@destroy');


	//end driver
	//Petrol Diesel Management //addpetroldiesel

	Route::any('petroldiesel','RoPetrolDesielController@index');
	Route::any('addpetroldiesel','RoPetrolDesielController@store');
	Route::get('petroldiesel/active/{id}/{actt}','RoPetrolDesielController@active');
	Route::any('pestalnew_delete/{id}','RoPetrolDesielController@destroy');
	Route::any('pestalnew_update/edit/{id}/','RoPetrolDesielController@update');


	//Petrol Diesel  Add Price Management 

	Route::any('petDmaster','RoPDMasterController@index');
	Route::any('addpetDmaster','RoPDMasterController@store');
	Route::get('petDmaster/active/{id}/{actt}','RoPDMasterController@active');
	Route::any('petDmaster_delete/{id}','RoPDMasterController@destroy');
	Route::any('PDmaster_update/{id}/','RoPDMasterController@update');
	Route::any('PDmasteredit/edit/{id}','RoPDMasterController@edit');
	Route::any('getpetmaster','RoPDMasterController@getrocode');


	//country
	Route::any('get_country_state','AdminController@getState');
	Route::any('get_state_city','AdminController@getCity');
	Route::any('getstatecoded','AdminController@getstatecoded');

	Route::post('getstategstcoded','AdminController@getstategstcoded')->name("getstategstcoded");
	Route::get('getrocodetx','RoPieceListManagementController@gettax');
	Route::any('logodelete/delete/{id}','AdminController@deletelogo');
 
 // City Management
    Route::get('stategetcity/{id}','CityController@index'); 
    Route::post('save_citydata','CityController@create');
    Route::any('update_citydata/{id}/','CityController@storeupdate');

	//999***********petrol*********

	Route::any('add_petrol_diesel','AddPetrolDieselController@index');
	Route::any('save_petrol_diesel_data','AddPetrolDieselController@store');
	Route::any('add_petrol_diesel/view/{id}','AddPetrolDieselController@show');
	Route::any('add_petrol_diesel/delete/{id}','AddPetrolDieselController@destroy');
	Route::get('diesel_petrol_request/active/{id}/{actt}','AddPetrolDieselController@active');

	//petrol end
	//lube request
	Route::any('lube_request','CustomerLubeRequestController@create');
	Route::any('save_lube_request','CustomerLubeRequestController@store');
	Route::any('lube_request/delete/{id}','CustomerLubeRequestController@destroy');
	Route::any('lube_request/view/{id}','CustomerLubeRequestController@show');
	Route::get('lube_request/active/{id}/{actt}','CustomerLubeRequestController@active');
	Route::get('luberequest','CustomerLubeRequestController@index');


    //reports
    Route::get('salesRegisterBilled','ReportController@salesRegisterBilled');
    Route::get('salesRegisterBilledDownload/{fule_type}','ReportController@salesRegisterBilledDownload')->name('salesRegisterBilledDownload');

     Route::get('salesRegisterBilledSummary','ReportController@salesRegisterBilledSummary');
     Route::get('salesRegisterBilledDownloadSummary/{fule_type}','ReportController@salesRegisterBilledDownloadSummary')->name('salesRegisterBilledDownloadSummary');
      Route::get('salesRegisterBilledDownloadSummaryGSTBill/{fule_type}','ReportController@salesRegisterBilledDownloadSummaryGSTBill')->name('salesRegisterBilledDownloadSummaryGSTBill');
     
     Route::get('salesRegisterBilledDownloadSummaryGSTInvoice/{fule_type}','ReportController@salesRegisterBilledDownloadSummaryGSTInvoice')->name('salesRegisterBilledDownloadSummaryGSTInvoice');

      Route::get('DailyTotalShiftSalesAndSettlementReport','ReportController@DailyTotalShiftSalesAndSettlementReport');
    
    Route::get('item-wise-nozzle-wise-reading-report','ReportController@ItemwiseNozzleWiseReadingReport')->name('reading-report');
    Route::get('item-wise-nozzle-wise-reading-report/download','ReportController@downloadNozzleReadingReport');
    Route::get('partywise-itemwise-datewise-deliveries-pending','ReportController@partywiseItemwiseDatewiseDeliveriesPending')->name('deliveriesPendingReport');
    Route::get('partywise-itemwise-datewise-deliveries-pending/download','ReportController@downloadPartywiseItemwiseDatewiseDeliveriesPendingReport');
    Route::get('partywise-Credit-Limits-and-Status-Report','ReportController@partywiseCreditLimitsandStatusReport')->name('partywise-Credit-Limits-Report');
    Route::get('partywise-Credit-Limits-and-Status-Report/download','ReportController@downloadPartywiseCreditLimitsandStatusReport');
    Route::get('partywise-Vehiclewise-Sales-Report','ReportController@partywiseVehiclewiseSalesReport')->name('partywise-Vehiclewise-Sales-Report');
    Route::get('partywise-Vehiclewise-Sales-Report/download','ReportController@partywiseVehiclewiseSalesReportDownload');
    Route::get('SalesmanWise-Sales-Report','ReportController@salesmanWiseSalesReport')->name('SalesmanWise-Sales-Report');
    Route::get('SalesmanWise-Sales-Report/download','ReportController@salesmanWiseSalesReportDownload');
    Route::get('Party-Driver-Vehicle-Listings','ReportController@partyDriverVehicleListings')->name('Party-Driver-Vehicle-Listings');
    Route::get('Party-Driver-Vehicle-Listings/download','ReportController@partyDriverVehicleListingsDownload');
    Route::get('Partywise-Itemwise-Datewise-Deliveries-Pending-for-Billing','ReportController@PartywiseItemwiseDatewiseDeliveriesPendingforBilling')->name('PartyDeliveriesPendingforBilling'); 
    Route::get('Partywise-Itemwise-Datewise-Deliveries-Pending-for-Billing/download','ReportController@partywiseItemwiseDatewiseDeliveriesPendingforBillingDownload'); 

	//transaction

	Route::any('transaction_list','TransactionController@create');
	Route::any('transaction/view/{id}','TransactionController@show');
	Route::any('transaction/export','TransactionController@exportCSV');
	//reading ro 
	Route::any('ro_nozzel_reading','AdminController@ro_nozzel_reading');
	//principle
	Route::any('checkexit','AdminController@getcheck');
	//pedstel management
	Route::any('/check_Pedestal_Number_unique','RoPetrolDesielController@checkPedestalNumberUnique');
	Route::any('/check_Pedestal_Number_unique_cust','RoPetrolDesielController@checkPedestalNumberUniqueCus')->name('NumberUnique');
	Route::any('/check_customer_Email_ro_unique','RoPetrolDesielController@checkCustomerEmailWithRo');


    Route::any('/print_val/{id}','Qr_codeController@print_val');
    Route::any('/QrcodeReganaret/{id}','Qr_codeController@QrcodeReganaret');

	//pedestel
	Route::any('nozal_show/{id}','AdminController@nozalShow');
	
	//tank 
	Route::any('tank_view_page/{id}','AdminController@tankViewPage');

	//fuel price management

	Route::get('fuel_price_management','FuelPriceManagementController@index');
	Route::post('update_fuel_price','FuelPriceManagementController@update');
	//Route::post('get_fuel_price','FuelPriceManagementController@store');
	Route::any('save_fuel_price','FuelPriceManagementController@saveFuelPrice');

	Route::get('Any_price_management','AnyItemPriceController@index');
	Route::post('Any_price_management','AnyItemPriceController@store');
	Route::any('save_any_price','AnyItemPriceController@saveFuelPrice');
	Route::get('getsugroup/{grup}','AnyItemPriceController@getsubgroup');
//lob item selection

	Route::get('lob_master_page','LOBMasterController@index');
	Route::get('roLobUpdate','RoLobItemController@roLobUpdate');
	Route::post('roLobUpdateSave','RoLobItemController@roLobUpdateSave');
	
	Route::post('save_lob_data','LOBMasterController@store');
	Route::get('lob_master/active/{id}/{actt}','LOBMasterController@active');
	

	Route::get('save_selected_lob','RoLobItemController@index')->name('lob');

	Route::post('save_selected_lob','RoLobItemController@store');
  
  
   //Dip chart routes here
   Route::get('dipCharts','DipchartController@index')->name('dipCharts');
   Route::get('dipCharts/active/{id}/{actt}','DipchartController@active');
   Route::get('dipCharts/view/{id}','DipchartController@view');
   Route::post('dipCharts','DipchartController@store');
   Route::get('get_discreaption','DipchartController@discreaption');
   Route::get('dipChartsview','DipchartController@dipChartsview');
   Route::get('viewdipchart/{id}','DipchartController@viewdipchart');
   Route::get('download/csv','DipchartController@download');

   //Tank Reading
   Route::get('tankRiding','TankReadingController@index')->name('TankReadings'); 
   Route::get('dipedit/edit/{id}','TankReadingController@edit');
   Route::post('dipedit/{id}','TankReadingController@update');
   Route::post('tankRiding','TankReadingController@store'); 
   Route::get('gettankcode/{code}','TankReadingController@getTank')->name('gettankcode'); 
   Route::get('tankRiding/active/{id}/{actt}','TankReadingController@active');
   Route::get('gettankdipchartcalculation','TankReadingController@gettankdipchartcalculation'); 

   //slip entry fuel

	Route::get('customer_slip_entry_page','CustomerSlipEntryControll@index');
	Route::post('save_slip_detail_data','CustomerSlipEntryControll@store');
	Route::get('getietmpricebydate/{itemId}','CustomerSlipList@getItemPrice');
	Route::get('getShiftByDate','ShiftController@getShift');
	//slip no make unique by vijay
	Route::get('customer_slip_entry_lube_page/slipunique','CustomerSlipEntryControll@checkslipunique')->name('slipunique');

	Route::get('slipbook','SlipBookController@index')->name('slipbook');
	Route::get('slipbook/delete/{id}','SlipBookController@destroy')->name('slipbook.delete');
	Route::get('slipbook/edit/{id}','SlipBookController@edit')->name('slipbook.edit');
	Route::post('slipbook/update/{id}','SlipBookController@update')->name('slipbook.update');
	Route::post('saveSlipBook','SlipBookController@store')->name('saveSlipBook');
	Route::get('slipbooklist','SlipBookController@slipList')->name('slipbooklist');

	//slip entry lube
	Route::get('customer_slip_entry_lube_page','CustomerLubeSlipEntryController@index');

	Route::get('customer_slip_entry_lube_page/slipdetail','CustomerLubeSlipEntryController@checkslipdetailunique')->name('slipdetail');

	Route::post('save_lube_slip_entry','CustomerLubeSlipEntryController@store');

	//add state master
	Route::get('add_state_page','StateMasterController@index');
	Route::get('stat_data_update/{id}','StateMasterController@edit');
	Route::post('save_state_data/{id}','StateMasterController@update');
	Route::post('save_state_data','StateMasterController@store');

	//right customer driver

	Route::get('customer_driver','CustomerDriverController@index');
	Route::post('right_customber_driver_data_save','CustomerDriverController@store');
	Route::post('right_customer_driver_update/{id}','CustomerDriverController@update');
	Route::get('right_customer_driver/edit/{id}','CustomerDriverController@edit');
	Route::get('right_customer_driver/view/{id}','CustomerDriverController@show');


	//right customer vehicle

	Route::get('customer_vehiclemanagement','CustomerVehicleDriverController@index');
	Route::post('customer_vehicle_data','CustomerVehicleDriverController@vechstore');
	Route::post('customer_vehiclemanagement/update/{id}','CustomerVehicleDriverController@update');
	Route::get('customer_vehiclemanagement/edit/{id}','CustomerVehicleDriverController@edit');
	Route::get('customer_vehiclemanagement/View/{id}','CustomerVehicleDriverController@show');


	//abhishek router
	Route::any('emportstocka','StockItemManagementController@emportstock');
    Route::post('stocksave','StockItemManagementController@stocksaveexport');

    //Ro transaction

    Route::get('ro_transaction_list','RoTransactionController@create')->name('ro_transaction_list');
    Route::get('ro_transaction_list/export','RoTransactionController@exportCSV');
    Route::get('ro_transaction/view/{id}','RoTransactionController@show');


    //update price using csv file
     Route::get('get_all_item','CsvItemPriceUpdateController@DownloadItem');
     Route::post('update_csv_data','CsvItemPriceUpdateController@updateCsvData');
    //update fuel price csv file 
     Route::get('getallfuel','FuelPriceManagementController@DownloadItemFUEL');
     Route::post('update_csv_datafuel','FuelPriceManagementController@updateCsvDataFuel');
   // Create invoice  

	Route::get('generate-pdf','PdfGenerateController@pdfview')->name('generate-pdf');
	Route::get('pdfview', 'PdfGenerateController@pdfview');
	Route::get('invoiceindex', 'PdfGenerateController@index');
	Route::get('billsGenerated', 'PdfGenerateController@billsGenerated');
	Route::get('getVehicleByCustCode', 'PdfGenerateController@getVehicleByCustCode')->name('getVehicleByCustCode');
	Route::post('invoiceindex', 'PdfGenerateController@gettransectionasSelectList');
	Route::post('invoicepdf', 'PdfGenerateController@gettransection');
	Route::get('checkgst/{gst}','AdminController@checkgst');
	Route::get('invoice/{id}','PdfGenerateController@downloadpdf');
	Route::get('mailToCustomer/{id}','PdfGenerateController@mailToCustomer')->name('mailToCustomer');
	Route::get('getCustByDate','PdfGenerateController@getCustByDate')->name('getCustByDate');
    // credit limit 
    Route::get('invoicePendingCollection','PdfGenerateController@invoicePendingCollection')->name('invoicePendingCollection');
    Route::post('invoicePendingCollection','PdfGenerateController@invoicePendingCollectionFillter');
    Route::get('bill/settled/{id}','PdfGenerateController@settled');
    Route::post('bill/settled/{id}','PdfGenerateController@settledUdate');
    Route::get('bill/view/{id}','PdfGenerateController@viewSettled');

    Route::get('slipPendingPrinting','CustomerCommission@slipPendingPrinting')->name('slipPendingPrinting');
    Route::post('slipPendingPrinting','CustomerCommission@invoicePendingCollectionFillter');
    Route::get('commition/settled/{id}','CustomerCommission@settled');
    Route::post('commition/settled/{id}','CustomerCommission@settledUdate');
    Route::get('commition/view/{id}','CustomerCommission@viewSettled');

   // Services for item
	Route::get('item_services','ItemServicesController@index');
	Route::any('getrocodesevices','ItemServicesController@getrotaxservices');
    Route::post('itemservices','ItemServicesController@add_item')->name('itemservices');
	// Customer Commissions inv

	Route::get('customer_commision','CustomerCommission@index')->name('customer_commision');
	Route::get('customer_commision/create','CustomerCommission@craete');
	Route::post('customer_commision/save','CustomerCommission@indexpdf');
	
    Route::get('generate-pdf','CustomerCommission@pdfviewcustomer')->name('generate-pdf');
	
	Route::get('getcustomerinvoice/{customer}/{fule_type}','CustomerCommission@getinvoice');
	
  //Reset Password 
   Route::get('resetpassword/reset','Auth\ForgotPasswordController@index');

   //customer Session
   Route::post('createSession','RoCustomertManagementController@customerSession');
	
  //
   Route::get('getVihicleFuleType/{vechilregno}','CustomerSlipEntryControll@vechilregnoget');
   Route::get('shift_settelment','ShiftSettelmentController@index');
   Route::get('getcapcitys','AdminController@getcapcitys');
   Route::get('getnozzelno','AdminController@getnozzelno');
   Route::post('nozzelReadiingdata','AdminController@nozzelReadiingdata');
   Route::get('getstartingend','AdminController@getstartingend');
   
   // Vechile Model Add

   Route::get('vechilemodel','VechilModelController@index');
   Route::post('save_vechiledata','VechilModelController@create');
   Route::post('update_modeldata/{id}','VechilModelController@storeupdate');

   //Vehicle Make Add

   // Vechile Model Add

   Route::get('vechilemake','VehicleMakeController@index');
   Route::post('save_vechilemakedata','VehicleMakeController@create');
   Route::post('update_makedata/{id}','VehicleMakeController@storeupdate');
  
   // Payment Mode Master 

   Route::get('paymentmood','PaymentModeController@index');
   Route::post('save_paymentmode','PaymentModeController@create');
   Route::get('ro/active/paymentmode/{id}/{act}','PaymentModeController@activePaymentMode')->name('active.paymentmode');
   Route::post('update_paymentmode/{id}','PaymentModeController@storeupdate');
   Route::post('updatePaymentModeOrder','PaymentModeController@updatePaymentModeOrder')->name('updatePaymentModeOrder');
   
   
  	//  sub group

   Route::get('getstrocksubgroupprice','RoPieceListManagementController@getstocksubprice');
   Route::get('getstockupdategroup','RoPieceListManagementController@getstockupdategroup');




   //Customer Transactions 

   Route::get('customernewtransactions','CustomerNewTransactions@index');
   Route::post('customernewtransactions','CustomerNewTransactions@index');
   Route::get('createnewtransactionsa','CustomerNewTransactions@storedata');
   Route::post('storetrans','CustomerNewTransactions@storetransactions');
   Route::get('sedndotp','CustomerNewTransactions@getmobile');
   Route::get('verifity','CustomerNewTransactions@verifity');
   Route::get('othertransaction','CustomerNewTransactions@storeothertrans');
   Route::post('othersotetrans','CustomerNewTransactions@createtransaction');
   Route::get('sedndotpother','CustomerNewTransactions@sendotherotp');
  



   Route::get('shift/create','ShiftController@index')->name('shiftAllocation');
   Route::get('shift/closer','ShiftController@shiftCloder')->name('shiftCloder');
   Route::post('prevuioucmr/update','ShiftController@previouscmr_update')->name('previouscmr-update');
   Route::get('shift/settlement','ShiftController@settlement')->name('settlement');
   Route::get('shift/settlementconfirm','ShiftController@settlementconfirm')->name('settlementconfirm');

   Route::get('shift/get_all_shift','ShiftController@getshiftbyshiftmanager')->name('shift.get_all_shift');


   Route::post('shift/settlementconfirm','ShiftController@settlementconfirmstore');
   Route::get('shift/settlementLueb','ShiftController@settlementLueb')->name('settlementLueb');
   Route::get('shift/getShiftOfManager/{id}','ShiftController@getShiftOfManager');
   Route::post('shift/create/store','ShiftController@store');
   Route::post('shift/closer/close','ShiftController@shiftCloderStore');
   Route::post('shift/settlement','ShiftController@settlementstore');
   Route::get('shift/edit/{ids}','ShiftController@edit');
   Route::post('shiftupdate/{id}','ShiftController@update');
   Route::get('shift/getPrevShift/{shiftManager}','ShiftController@getPreShift');
   Route::get('getPrevShift/{shiftManager}','ShiftController@getPreShift');
  
   // re print
    Route::get('reprint/settlement','ReprintController@settlement')->name('reprint_settlement');
    Route::post('reprint/settlement','ReprintController@settlementReprint');
    Route::get('reprint/get_all_shift','ReprintController@getshiftbyshiftmanager')->name('reprint.get_all_shift');
	
	Route::get('reprint/delivery-slip-fuel','ReprintController@deliverySlipFuel')->name('reprint.delivery-slip-fuel');
	Route::post('reprint/delivery-slip-fuel-print','ReprintController@deliverySlipFuelPrint')->name('reprint.delivery-slip-fuel-print');

	//gst fuel
	Route::get('reprint/gst-slip-fuel','GstReprintController@gstSlipFuel')->name('reprint.gst-slip-fuel');
	Route::post('reprint/gst-slip-fuel-print','GstReprintController@gstSlipFuelPrint')->name('reprint.gst-slip-fuel-print');
  
   
   // tank Inward
   Route::get('tankInward','TankInwardController@index')->name('tankInward');
   //Route::get('tankInward/create','TankInwardController@create');
   Route::post('tankInward/store','TankInwardController@store');
   Route::get('tankInward/delete/{id}','TankInwardController@delete')->name('tankInward.delete');
   Route::post('tankInward/update/{id}','TankInwardController@update');
   Route::get('get_tank_by_ajex/{item}','TankInwardController@getTankbyAjex');
   Route::post('tankInward/edit','TankInwardController@edit');
   Route::get('get_tank_value_by_ajax/{item}','TankInwardController@getTankValueByAjax');
   
   

   // Model  Vechil 
   Route::post('getmodelname','VechilManageController@getmodelnameupdate');
   Route::get('getmodelnameupdate','VechilManageController@getmodelnameupdate');
   Route::get('getstrocksubgroupedit','StockItemManagementController@getstocksubedit');
   Route::get('getItemByModel','VechilManageController@getItemByModel')->name('getItemByModel');

   //Tax Rate Management
   Route::get('taxrates','TaxRateController@index');
   Route::get('getTaxRate','TaxRateController@getTaxRateAccordingType');
   Route::post('save_tax_rate','TaxRateController@store');
   //getcapacityfortankreading
   Route::get('gettankcapacity','TankReadingController@gettankcapacity');
   Route::get('getTankStack/{id}','TankReadingController@getTankStack');

   //Master Payment Mode
   Route::get('masterpayment','MasterPaymentController@index');
   Route::post('savemasterpayment','MasterPaymentController@create');
   Route::post('update_masaterpaymentmode/{id}','MasterPaymentController@storeupdate');
   Route::post('mastersavepayment','PaymentModeController@masterpaymentsave');
    Route::get('tallyConfigsettlement','PaymentModeController@editmethod');
   Route::post('tallyConfigsettlement','PaymentModeController@updatemethod')->name('updatetally.dailyshiftsettlement');

   //Tally Coinfig
   Route::get('tallyConfigfuel','PaymentModeController@edittally');
   Route::post('tallyConfigfuel','PaymentModeController@updatetally')->name('updatetally');

   Route::get('tallyConfigGST','PaymentModeController@edittallyGST');
   Route::post('tallyConfigGST','PaymentModeController@updatetallyGST')->name('updatetally.GST');

   Route::get('tallyConfigDailyShift','PaymentModeController@edittallyDailyShift');
   Route::post('tallyConfigDailyShift','PaymentModeController@updatetallyDailyShift')->name('updatetally.DailyShift');

   Route::get('tallyConfigPeriodic','PaymentModeController@edittallyPeriodic');
   Route::post('tallyConfigPeriodic','PaymentModeController@updatetallyPeriodic')->name('updatetally.Periodic');

   Route::get('tallyConfigBillingCharges','PaymentModeController@edittallyBillingCharges');
   Route::post('tallyConfigBillingCharges','PaymentModeController@updatetallyBillingCharges')->name('updatetally.BillingCharges');


   //reset  tally flag

   Route::get('tallyConfigRestView','TallyConfigController@tallyConfigRestView')->name('tallyConfigRestView');
   Route::post('tallyConfigRest','TallyConfigController@tallyConfigRest')->name('tallyConfigRest');


   // get Nozzel pedestal
   Route::get('viewdipcharts/{id}','PetestalgetNozzelController@getnozzgelnu');
   Route::get('tankchart/{id}','TankReadingController@gettanknozzel');
   Route::get('getsubgroup','RoPieceListManagementController@getsubgroup');
   Route::get('roconfig','RoConfigController@index');
   Route::post('admincofigsave','RoConfigController@create');
   Route::post('rocofigsave','OutletController@create');
   Route::get('outletconfig','OutletController@index');
   Route::get('shifttype','ShiftController@shifttype');
   Route::get('shifttypeedit/{id}','ShiftController@shifttypeedit')->name('shifttypeedit');
   Route::post('shifttype','ShiftController@shifttypestore')->name('shifttypestore');
   Route::post('shifttypeedit/{id}','ShiftController@shifttypeupdate');
   Route::get('tallyConfig','TallyConfigController@index')->name('tallyConfig');
   Route::post('storeTally','TallyConfigController@store')->name('storeTally');
   Route::get('changekey','TallyConfigController@changekey')->name('changekey');
   Route::get('tallyConfigMaster','TallyConfigController@showMaster')->name('tallyConfigMaster');
   Route::get('tallyConfigMasterPersonnel','TallyConfigController@tallyConfigMasterPersonnel')->name('tallyConfigMasterPersonnel');
   Route::get('tallyConfigMasterInventoryMaster','TallyConfigController@tallyConfigMasterInventoryMaster')->name('tallyConfigMasterInventoryMaster');
   Route::get('tallyConfigMasterCustomerMaster','TallyConfigController@tallyConfigMasterCustomerMaster')->name('tallyConfigMasterCustomerMaster');
   Route::get('customersliplist','CustomerSlipList@index');
   Route::post('customerrequestfind','CustomerSlipList@requestFillter');
   Route::get('fuelslipedit/{id}','CustomerSlipList@edit');
   Route::post('slipupdated/{id}','CustomerSlipList@update');
   Route::get('customerlubelist','CustomerLubeSlipList@index');
   Route::post('customerlubelist','CustomerLubeSlipList@requestFillter');
   Route::get('editlubelist/{id}','CustomerSlipList@lubeedit');
   Route::post('lubeupdated/{id}','CustomerSlipList@lubeupdate');
   //Customer Profile 
   Route::get('customerprofile/{id}','CustomerProfileController@editprofile'); 
   Route::get('changeRo','CustomerProfileController@changeRo'); 
   Route::post('customerprofileupdate/{id}','CustomerProfileController@updateprofile');
   // Indusrty department
   Route::get('industry','Indusrtydepartment@index'); 
   Route::post('create','Indusrtydepartment@create');
   Route::post('industryupdate/{id}','Indusrtydepartment@industryupdate'); 
   Route::get('get_vertical','Indusrtydepartment@get_vertical');
   
   //all download csv
	Route::get('principal_listing/download_principal_listing','AdminController@csvfileprincipallisting')->name('download_principal_listing');
	Route::get('unit_of_measure/download_csv_unitofmeasure','AdminController@csvfileunitofmeasur')->name('download_csv_unitofmeasure');
	Route::get('stock_item_group/download_stock_item_groups','AdminController@csvfilestockitemgroup')->name('download_stock_item_groups');
	Route::get('retail/download_newretails1','AdminController@csvfilenewretails')->name('download_newretails1');
	Route::get('vechilemodel/download_vehicle_mode','VechilModelController@csvvehiclemodel')->name('download_vehicle_mode');
	Route::get('vechilemake/download_vehicle_make','VehicleMakeController@csvfilevehicle')->name('download_vehicle_make');
	Route::get('masterpayment/download_csv','MasterPaymentController@csvdownload')->name('download_csv');


});
