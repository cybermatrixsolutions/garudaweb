<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Inventory_Detail_View extends Vtiger_Detail_View {
	function preProcess(Vtiger_Request $request) {
		$viewer = $this->getViewer($request);
		$viewer->assign('NO_SUMMARY', true);
		parent::preProcess($request);
	}

	/**
	 * Function returns Inventory details
	 * @param Vtiger_Request $request
	 */
	function showModuleDetailView(Vtiger_Request $request) {
		echo parent::showModuleDetailView($request);
		$this->showLineItemDetails($request);
	}

	/**
	 * Function returns Inventory details
	 * @param Vtiger_Request $request
	 * @return type
	 */
	function showDetailViewByMode(Vtiger_Request $request) {
		return $this->showModuleDetailView($request);
	}

	function showModuleBasicView($request) {
		return $this->showModuleDetailView($request);
	}
	/**
	 * Function returns Inventory Line Items
	 * @param Vtiger_Request $request
	 */
	function showLineItemDetails(Vtiger_Request $request) {
		global $adb;
		$record = $request->get('record');
		$moduleName = $request->getModule();

		$recordModel = Inventory_Record_Model::getInstanceById($record);
		$relatedProducts = $recordModel->getProducts();

		//##Final details convertion started
		$finalDetails = $relatedProducts[1]['final_details'];

		//Final tax details convertion started
		$taxtype = $finalDetails['taxtype'];
		if ($taxtype == 'group') {
			$taxDetails = $finalDetails['taxes'];
			$taxCount = count($taxDetails);
			for($i=0; $i<$taxCount; $i++) {
				$taxDetails[$i]['amount'] = Vtiger_Currency_UIType::transformDisplayValue($taxDetails[$i]['amount'], null, true);
			}
			$finalDetails['taxes'] = $taxDetails;
		}
		//Final tax details convertion ended

		//Final shipping tax details convertion started
		$shippingTaxDetails = $finalDetails['sh_taxes'];
		$taxCount = count($shippingTaxDetails);
		for($i=0; $i<$taxCount; $i++) {
			$shippingTaxDetails[$i]['amount'] = Vtiger_Currency_UIType::transformDisplayValue($shippingTaxDetails[$i]['amount'], null, true);
		}
		$finalDetails['sh_taxes'] = $shippingTaxDetails;
		//Final shipping tax details convertion ended

		$currencyFieldsList = array('adjustment', 'grandTotal', 'hdnSubTotal', 'preTaxTotal', 'tax_totalamount',
									'shtax_totalamount', 'discountTotal_final', 'discount_amount_final', 'shipping_handling_charge', 'totalAfterDiscount');
		foreach ($currencyFieldsList as $fieldName) {
			$finalDetails[$fieldName] = Vtiger_Currency_UIType::transformDisplayValue($finalDetails[$fieldName], null, true);
		}

		$relatedProducts[1]['final_details'] = $finalDetails;
		//##Final details convertion ended

		//##Product details convertion started
		$productsCount = count($relatedProducts);
		for ($i=1; $i<=$productsCount; $i++) {
			$product = $relatedProducts[$i];

			//Product tax details convertion started
			if ($taxtype == 'individual') {
				$taxDetails = $product['taxes'];
				$taxCount = count($taxDetails);
				for($j=0; $j<$taxCount; $j++) {
					$taxDetails[$j]['amount'] = Vtiger_Currency_UIType::transformDisplayValue($taxDetails[$j]['amount'], null, true);
				}
				$product['taxes'] = $taxDetails;
			}
			//Product tax details convertion ended

			$currencyFieldsList = array('taxTotal', 'netPrice', 'listPrice', 'unitPrice', 'productTotal',
										'discountTotal', 'discount_amount', 'totalAfterDiscount');
			foreach ($currencyFieldsList as $fieldName) {
				$product[$fieldName.$i] = Vtiger_Currency_UIType::transformDisplayValue($product[$fieldName.$i], null, true);
			}

			$relatedProducts[$i] = $product;
		}
		//##Product details convertion ended

		$viewer = $this->getViewer($request);
		$viewer->assign('RELATED_PRODUCTS', $relatedProducts);
		$viewer->assign('RECORD', $recordModel);
		$viewer->assign('MODULE_NAME',$moduleName);

		$viewer->view('LineItemsDetail.tpl', 'Inventory');

		$sqlseq=$adb->pquery("select seqno from vtiger_payments where id='".$_REQUEST['record']."' order by seqno desc limit 1");
		$recordres1	= $adb->query_result($sqlseq,0,'seqno');
		$j=1;
		$sql1="select seqno,amount,utrno,bankname,remarks from vtiger_payments where id='".$_REQUEST['record']."' order by seqno";
		$sql_res=$adb->pquery($sql1);
		if($adb->num_rows($sql_res)) {
			while($recordres= $adb->fetch_array($sql_res)){
			$res0 =$recordres['seqno'];
			$res1 =$recordres['amount'];
			$res2 =$recordres['utrno'];
			$res3 =$recordres['bankname'];
			$res4 =$recordres['remarks'];
			$viewer->assign('SEQ', $res0);
			$viewer->assign('AMOUNT', $res1);
			$viewer->assign('UTRNO', $res2);
			$viewer->assign('BANKNAME', $res3);
			$viewer->assign('REMARK', $res4);
			$viewer->assign('RECORDCOUNT', $recordres);
			$viewer->view('LineItemsPaymentDetail.tpl', 'Inventory');
			}
		}
		$viewer->view('LineItemsPaymentDetailContinue.tpl', 'Inventory');
	
		$lognusersql=$adb->pquery("SELECT carrier FROM `vtiger_purchaseorder` WHERE purchaseorderid = '".$_REQUEST["record"]."'");
		$lognusr= $adb->query_result($lognusersql,0,'carrier');	
	if($moduleName=='PurchaseOrder'){
		//if($lognusr != 'ACE'){
			$viewer->view('Detailcolumn.tpl', 'Inventory');

			$sqlseq_od=$adb->pquery("select sequence_no from vtiger_odproduct where id='".$_REQUEST['record']."' 
			order by sequence_no desc limit 1");
			$recordresod	= $adb->query_result($sqlseq_od,0,'sequence_no');
			$j=1;
			$sql_od="select sequence_no,prodname,prodcode,podqty,orderqty,dispatchqty from vtiger_odproduct 
				where id='".$_REQUEST['record']."' order by sequence_no";
			$res_od=$adb->pquery($sql_od);
			if($adb->num_rows($res_od)) {
				while($record_od= $adb->fetch_array($res_od)){
				$sequence_no =$record_od['sequence_no'];
				$prodname =$record_od['prodname'];
				$prodcode =$record_od['prodcode'];
				$podqty =$record_od['podqty'];
				$orderqty =$record_od['orderqty'];
				$dispatchqty =$record_od['dispatchqty'];
				$viewer->assign('sequence', $sequence_no);
				$viewer->assign('prodname', $prodname);
				$viewer->assign('prodcode', $prodcode);
				$viewer->assign('orderqty', $orderqty);
				//$viewer->assign('podqty', $podqty);
				$viewer->assign('dispatchqty', $dispatchqty);
				$viewer->view('LineItemsODDetail.tpl', 'Inventory');
				}
			}
	//	}
	}
}


	/**
	 * Function to get the list of Script models to be included
	 * @param Vtiger_Request $request
	 * @return <Array> - List of Vtiger_JsScript_Model instances
	 */
	function getHeaderScripts(Vtiger_Request $request) {
		$headerScriptInstances = parent::getHeaderScripts($request);

		$moduleName = $request->getModule();

		//Added to remove the module specific js, as they depend on inventory files
		$modulePopUpFile = 'modules.'.$moduleName.'.resources.Popup';
		$moduleEditFile = 'modules.'.$moduleName.'.resources.Edit';
		$moduleDetailFile = 'modules.'.$moduleName.'.resources.Detail';
		unset($headerScriptInstances[$modulePopUpFile]);
		unset($headerScriptInstances[$moduleEditFile]);
		unset($headerScriptInstances[$moduleDetailFile]);

		$jsFileNames = array(
			'modules.Inventory.resources.Popup',
            'modules.Inventory.resources.Detail',
			'modules.Inventory.resources.Edit',
			"modules.$moduleName.resources.Detail",
		);
		$jsFileNames[] = $moduleEditFile;
		$jsFileNames[] = $modulePopUpFile;
		$jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
		$headerScriptInstances = array_merge($headerScriptInstances, $jsScriptInstances);
		return $headerScriptInstances;
	}
}
