<?php
include("connection.php");
include("common.php");


if(isset($_POST['key']))
{
    	
	$key=mysqli_real_escape_string($con,$_POST['key']);
	
	$Item_Code=mysqli_real_escape_string($con,$_POST['Item_Code']);
	
	$Item_Code_arr=explode(",",$Item_Code); 
	
	$Vehicle_Reg_No=mysqli_real_escape_string($con,$_POST['Vehicle_Reg_No']);
	$request_id=mysqli_real_escape_string($con,$_POST['request_id']);
	$dmobile=mysqli_real_escape_string($con,$_POST['dmobile']);
	$cus_code=mysqli_real_escape_string($con,$_POST['Customer_Code']);
	
	$Item_qty=mysqli_real_escape_string($con,$_POST['Item_qty']);
	
	
	
	$Item_qty_arr=explode(",",$Item_qty); 
	
	$request_id="RQ".mt_rand();
	
	

   // $password=mysqli_real_escape_string($con,$_POST['']);
    $query="select t1.Customer_Code,t3.name as city,t2.pump_legal_name,t1.RO_code ,Limit_Value
	from  tbl_customer_master t1 inner join tbl_ro_master t2 on t1.RO_code=t2.RO_code
	inner join cities t3 on t3.id=t2.city
	inner join tbl_customer_limit t4 on t4.Customer_Code=t1.Customer_Code

	where t1.user_token ='$key' and t1.Customer_Code='$cus_code' and t1.is_active=1";
    $res=mysqli_query($con,$query);
    $count=mysqli_num_rows($res);
	while($rows = mysqli_fetch_assoc($res))
     {
     $Customer_Code = $rows['Customer_Code'];
	 $Ro_code = $rows['RO_code'];
	 $pump_legal_name = $rows['pump_legal_name'];
	 $city = $rows['city'];
	 $Limit_Value = $rows['Limit_Value'];
	 
      
     }
     if($count >= 1)
     {
		 
		 
		 $checkcredit=creditlimit_lube($cus_code,$Limit_Value,$Vehicle_Reg_No,$Item_Code_arr,$Item_qty_arr);
		
		if ($checkcredit==0)
		{
			 $json = array("msg"=>"Credit Limit Exhausted", "key"=>$key,);
			 $json1 = array('status'=>"failed", "data"=> $json);
			  header('Content-type: application/json'); 
             echo json_encode($json1);
			 exit();
		}
		 
		 
		 $query="select * from  tbl_customer_request_lube where customer_code ='$cus_code' and Vehicle_Reg_No='$Vehicle_Reg_No' and Execution_date is null";
		 $res=mysqli_query($con,$query);
         $count=mysqli_num_rows($res);
		
		
	
	     if($count >= 1)
		 { 
			 $json = array("msg"=>"request already Exist", "key"=>$key);
			 $json1 = array('status'=>"failed", "data"=> $json);
			  header('Content-type: application/json'); 
             echo json_encode($json1);
			 exit();
		 }
		 
		 
		
		
		 
		 
		 
		 
		 
		 else
		 
		 {

		 	$con->autocommit(FALSE);
	        try{
	        	
		
			for ($i = 0; $i < count($Item_Code_arr); $i++) 
			{
		 
		 
			$created_at = date('Y-m-d H:i:s');
        
			$query="insert into  tbl_customer_request_lube(customer_code,item_id,Vehicle_Reg_No,RO_code,created_at,updated_at,request_id,current_driver_mobile,quantity)
			values ('$cus_code','$Item_Code_arr[$i]','$Vehicle_Reg_No','$Ro_code','$created_at','$created_at','$request_id','$dmobile','$Item_qty_arr[$i]')";
			$res=mysqli_query($con,$query);
              if(!$res)
              throw new Exception("");

			$count=mysqli_num_rows($res);
			
			
			
		 		 		
		    //print_r($query);
			//exit();
		  
			}
			
			$created_at = date('Y-m-d H:i:s');
			
			$dtd = date('d/m/Y');
			
			$otp=substr(mt_rand(),0,4);
			
			$query1="insert into  tbl_trans_otp(OTP,request_id,mobile,created_at,updated_at)
			values ('$otp','$request_id','$dmobile','$created_at','$created_at')";
			$res=mysqli_query($con,$query1);

			if(!$res)
              throw new Exception("");
			
			
			$message="Your OTP is $otp for #$request_id dtd. $dtd - Others - $Vehicle_Reg_No at $pump_legal_name, $city";

			
			//$message="Your One Time Password is: '$otp' for request #'$request_id'.Please do not share OTP to anyone.";



			 $con->commit();
			    $con->autocommit(TRUE);
			    $con->close();


            } catch (Exception $e) {
		    $con->rollback();
               $con->autocommit(TRUE);

               $json = array( "msg"=>" Something Wrong..!" );
	           $json1 = array('status'=>"failed","data"=> $json);
			    header('Content-type: application/json'); 
               echo json_encode($json1);

               exit();

		    
		}
			
			
			
			
					try 
					{
					
						$msg=sendsms($message,$dmobile);
					}
					catch(Exception $e) 
					{
							//echo 'Message: ' .$e->getMessage();
					}
			
			
			
		 
			$json = array( "msg"=>"Lube request Added..!" ,"key"=>$key,"Request_Id"=>$request_id);
			$json1 = array('status'=>"success","data"=> $json);
			header('Content-type: application/json');   
			echo json_encode($json1);
		 }
	 
		 
           

    }

    else

    {
             $json = array("msg"=>"login failed..! Not Valid Pin", "key"=>$key);
			 $json1 = array('status'=>"failed", "data"=> $json);
			  header('Content-type: application/json'); 
             echo json_encode($json1);

    }
    
}
else
{
               $json = array( "msg"=>"Not receive required data..!" );
	           $json1 = array('status'=>"failed","data"=> $json);
			    header('Content-type: application/json'); 
               echo json_encode($json1);

}

?>