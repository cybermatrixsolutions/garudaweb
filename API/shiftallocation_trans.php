<?php
include("connection.php");
date_default_timezone_set('Asia/Kolkata');

if(isset($_POST['key']))
{
    	
	$key=mysqli_real_escape_string($con,$_POST['key']);
	$type=mysqli_real_escape_string($con,$_POST['type']);
	
	$pedestal=mysqli_real_escape_string($con,$_POST['pedestal']);
	
	$pedestal_arr=explode(",",$pedestal);  
	
	$personal=mysqli_real_escape_string($con,$_POST['personal']);
	
	$personal_arr=explode(",",$personal); 

	$tank=mysqli_real_escape_string($con,$_POST['tank']);
	
	$tank_arr=explode(",",$tank); 

	$shift_type=mysqli_real_escape_string($con,$_POST['shifttype']);	
	
	
   // $password=mysqli_real_escape_string($con,$_POST['']);
    $query="select * from   tbl_personnel_master where user_token ='$key' and is_active=1";
    $res=mysqli_query($con,$query);
    $count=mysqli_num_rows($res);
	while($rows = mysqli_fetch_assoc($res))
     {
    
	 $id = $rows['id'];
	 $RO_CODE = $rows['RO_Code'];

     }
     if($count == 1)
     {
		
		$query="select * from   ro_shift_config where RO_Code ='$RO_CODE' and id='$shift_type'";
		$res=mysqli_query($con,$query);
		
		while($rows = mysqli_fetch_assoc($res))
		{
    
			$start_time = $rows['start_time'];
			$end_time = $rows['end_time'];

		}

	  $flexishiftsql="select * from outlet_config  where rocode ='$RO_CODE' and field_name = 'flexi_shift' limit 1";

		//echo();
		


		 $flexishiftq = mysqli_query($con,$flexishiftsql);

		 while($rows = mysqli_fetch_assoc($flexishiftq))
     {
    
	 $flexishift = $rows['value'];
	

     }

      if($flexishift){
        if($flexishift=="on"){
           $flexishiftstatus="flexi";
         }else{
         $flexishiftstatus="regular";

         }
        }else{
              $flexishiftstatus="regular";
        }






          $currenttime=date('H:i:s');


          if($flexishiftstatus=="flexi"){

          	$start_time=$currenttime;
          }





		
		$cdate=date('Y-m-d');
		
		$created_at = $cdate." ".$start_time;

		
		
		//$cdate=date_format($created_at,"Y-m-d");
		// print_r($created_at);
		// exit();




		
		


   
		
		 if( $flexishiftstatus=="flexi"){


		 $sql="SELECT * from shifts where shift_manager='$id'  
		and (closer_date is null or created_at='$created_at') ";

		
		
	}else{
        $sql="SELECT * from shifts where shift_manager='$id'  
		and (closer_date is null or created_at='$created_at')";

	}
         
	//print_r($sql);
	//exit();
	
        $res1  = mysqli_query($con,$sql);
		$count=mysqli_num_rows($res1);
		
		if($count >= 1)
		{
			 $json = array("msg"=>"Previous Shift not closed", "key"=>$key);
			 $json1 = array('status'=>"failed", "data"=> $json);
			 header('Content-type: application/json'); 
             echo json_encode($json1);
			 exit();

		}	
        else

		{


			$con->autocommit(FALSE);
	     try{

	     	 
			
			$sql1="insert into shifts(shift_manager,ro_code,created_at,updated_at,type,shift_type,shifttype,transation_date) values ('$id','$RO_CODE','$created_at','$created_at','$type','$shift_type','$flexishiftstatus','$created_at')";
         
		
	
			$res1  = mysqli_query($con,$sql1);

			  /* print_r($res1 );
			 exit();
*/
			if(!$res1)
            throw new Exception("");



			$count1=mysqli_num_rows($res1);
			
			$last_id = mysqli_insert_id($con);
			
			if ($res1)
			{
			
			for ($i = 0; $i < count($pedestal_arr); $i++) 
			{
				
			
				$sql="insert into shift_pedestal(shift_id,pedestal_id)
				values ('$last_id','$pedestal_arr[$i]')";
         
			
				$res1  = mysqli_query($con,$sql);
					if(!$res1)
                         throw new Exception("");
				
				
				
			}
			
			for ($j = 0; $j < count($personal_arr); $j++) 
			{
				
			
				$sql="insert into  shift_personnel(shift_id,personnel_id)
				values ('$last_id','$personal_arr[$j]')";
         
			
				$res1  = mysqli_query($con,$sql);
					if(!$res1)
                         throw new Exception("");
				
				
				
			}
			for ($k = 0; $k < count($tank_arr); $k++) 
			{
				
			
				$sql="insert into  shift_tank(shift_id,tank_id)
				values ('$last_id','$tank_arr[$k]')";
         
			
				$res1  = mysqli_query($con,$sql);
				if(!$res1)
                         throw new Exception("");
				
				
				
			}
			}
			
			
			
			    $con->commit();
			    $con->autocommit(TRUE);
			    $con->close();
			
			$json = array("msg"=>"Shift Added", "key"=>$key);
			$json1 = array('status'=>"success", "data"=> $json);
			header('Content-type: application/json'); 
            echo json_encode($json1);
            exit();




            } catch (Exception $e) {
		    $con->rollback();
               $con->autocommit(TRUE);

               $json = array( "msg"=>" Something Wrong..!" );
	           $json1 = array('status'=>"failed","data"=> $json);
			    header('Content-type: application/json'); 
               echo json_encode($json1);

               exit();

		    
		}


			
		}			
   
		 
		 
						
         
		

    }

    else

    {
             $json = array("msg"=>"login failed..! Not Valid ", "key"=>$key,);
			 $json1 = array('status'=>"failed", "data"=> $json);
			  header('Content-type: application/json'); 
             echo json_encode($json1);

    }
    
}
else
{
               $json = array( "msg"=>"Not receive required data..!" );
	           $json1 = array('status'=>"failed","data"=> $json);
			    header('Content-type: application/json'); 
               echo json_encode($json1);

}

?>