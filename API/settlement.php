<?php
include("connection.php");
date_default_timezone_set('Asia/Kolkata');

if(isset($_POST['key']))
{
    	
	$key=mysqli_real_escape_string($con,$_POST['key']);
	
	$shift_id=mysqli_real_escape_string($con,$_POST['shift_id']);
	
	$trans_date=mysqli_real_escape_string($con,$_POST['trans_date']);
	
	
   // $password=mysqli_real_escape_string($con,$_POST['']);
    $query="select * from   tbl_personnel_master where user_token ='$key' and is_active=1";
    $res=mysqli_query($con,$query);
    $count=mysqli_num_rows($res);
	while($rows = mysqli_fetch_assoc($res))
     {
    
	 $id = $rows['id'];
	 $RO_CODE = $rows['RO_Code'];

     }
     if($count == 1)
     {
		 
		$created_at = date('Y-m-d H:i:s');
		 $sql5="SELECT id,type from shifts 
		 where id='$shift_id'";
	     $res3  = mysqli_query($con,$sql5);
		 while($rows = mysqli_fetch_assoc($res3))
         {
              $type = $rows['type'];
	     }
		 
		 if ($type==1 || $type==3)
		 {
			$sql="SELECT * from tbl_item_price_list t1 inner join tbl_price_list_master t2 on t1.item_id=t2.id where date(t1.effective_date)=date('$created_at') 
			and RO_Code='$RO_CODE'";
			
			$res3  = mysqli_query($con,$sql);
			$count=mysqli_num_rows($res3);
			
			if($count == 0)
			{
				$json = array("msg"=>"Fuel Price not updated ", "key"=>$key,);
				$json1 = array('status'=>"failed", "data"=> $json);
				header('Content-type: application/json'); 
				echo json_encode($json1);
				exit();
			}
				 
			 
			 
		 }
		 
		 
		
		//price update
        
		$sqlpriceupdate="update  tbl_ro_nozzle_reading t1 inner join tbl_pedestal_nozzle_master t2 
			on t1.Nozzle_No=t2.Nozzle_Number 
				inner join tbl_item_price_list t3 on t3.item_id=t2.fuel_type
				inner join shifts t4 on t4.id=t1.shift_id
                set t1.price=t3.price
				where date(t4.created_at)=date(t3.effective_date)
				and t4.id='$shift_id'";
         
		
	
        $respriceupdate  = mysqli_query($con,$sqlpriceupdate);
		
		
		
		
		
		
		
		//total lube sales
        
		$sql="SELECT round(sum(item_price*petroldiesel_qty),2) as trans_amount_p from tbl_customer_transaction 
		t1 inner join tbl_price_list_master t2 on t1.item_code=t2.id 
		
		inner join tbl_stock_item_group t3 on t3.id=t2.Stock_Group 
		
		
		
		where Trans_By='$id' and Group_Name='LUBES'
		and shift_id='$shift_id' and petrol_or_lube=2 ";
         
		
	
        $res1  = mysqli_query($con,$sql);
   
		 while($row = mysqli_fetch_assoc($res1))
		 {
			
			$total_lube= $row['trans_amount_p'];
		
		 }
		 
		 
		 //total other sales
        
		$sql="SELECT round(sum(item_price*petroldiesel_qty),2) as trans_amount_p from tbl_customer_transaction 
		t1 inner join tbl_price_list_master t2 on t1.item_code=t2.id 
		
		inner join tbl_stock_item_group t3 on t3.id=t2.Stock_Group 
		
		
		
		where Trans_By='$id' and Group_Name='OTHERS'
		and shift_id='$shift_id' and petrol_or_lube=2 ";
         
         
		
	
        $res1  = mysqli_query($con,$sql);
   
		 while($row = mysqli_fetch_assoc($res1))
		 {
			
			$total_others= $row['trans_amount_p'];
		
		 }
		 
		 
		 
		
		 
		 
		$sql3="SELECT round(sum(item_price*petroldiesel_qty),2) as trans_amount_l from tbl_customer_transaction where Trans_By='$id' 
		and shift_id='$shift_id' and cust_name='credit' and trans_mode<>'SLIPS'";
        // print_r($sql3);
		 //exit();
		
	
        $res3  = mysqli_query($con,$sql3);
   
		 while($row = mysqli_fetch_assoc($res3))
		 {
			
			$credit_sales = $row['trans_amount_l']; 
		
		 }
		 
		 
		$sql3="SELECT round(sum(item_price*petroldiesel_qty),2) as trans_amount_l from tbl_customer_transaction where Trans_By='$id' 
		and shift_id='$shift_id' and cust_name='credit' and trans_mode='SLIPS'";
        // print_r($sql3);
		 //exit();
		
	
        $res3  = mysqli_query($con,$sql3);
   
		 while($row = mysqli_fetch_assoc($res3))
		 {
			
			$credit_sales_manual = $row['trans_amount_l']; 
		
		 }
		 
		 //$sql3="SELECT sum(trans_amount) as trans_amount_l from tbl_customer_transaction_invoice where Trans_By='$id' 
		//and shift_id='$shift_id' and cust_type='credit' and petrol_or_lube=2 ";
         
		
	
        //$res3  = mysqli_query($con,$sql3);
   
		// while($row = mysqli_fetch_assoc($res3))
		// {
			
			//$trans_amount_l = $row['trans_amount_l']; 
		
		 //}
		 
		 
		
		$sql2="SELECT distinct t1.Nozzle_No,t1.Nozzle_Start,t1.Nozzle_End,t1.price,t1.test,t1.reading,t3.item_name from tbl_ro_nozzle_reading t1 
		inner join tbl_pedestal_nozzle_master t2 on trim(t1.Nozzle_No)=trim(t2.Nozzle_Number)
		inner join tbl_price_list_master t3 on t3.id=t2.fuel_type 
		
		inner join shift_pedestal t5 on t5.pedestal_id=t2.Pedestal_id
		inner join shifts t6 on t6.id=t5.shift_id
		where Reading_by='$id' and t6.id='$shift_id' and t6.fuel=0
		and t1.shift_id='$shift_id' AND Nozzle_End is NOT NULL
	
		";
		
	
         
		//print_r($sql2);
		//exit();
	
        $res2  = mysqli_query($con,$sql2);
   
		 while($row = mysqli_fetch_assoc($res2))
		 {
			
			$data2[] = $row; 
		
		 }
		 
		 
		 
		
		
		$sqltankreading="SELECT distinct Tank_Number,t1.Capacity,Unit_Symbol as Unit_of_Measure,Item_Name as 
		fuel_type,value as volume from tbl_tank_master t1
		inner join shift_tank t2 on t1.id=t2.tank_id
		inner join shifts t3 on t3.id=t2.shift_id
		
		inner join tbl_tank_reading t8 on t8.shift_id=t3.id and t8.Tank_code=t1.id
		
		inner join tbl_price_list_master t6 on t6.id=t1.fuel_type
		
		inner join tbl_unit_of_measure t7 on t7.id=t1.Unit_of_Measure
		
		where t3.id='$shift_id'";
		
		
         
		
	
        $res3  = mysqli_query($con,$sqltankreading);
   
		 while($row = mysqli_fetch_assoc($res3))
		 {
			
			$data3[] = $row; 
		
		 }
		 
		 
		 
						
          $json = array( "msg"=>"Settlement!" ,"key"=>$key,'total_others'=>$total_others,'total_lube'=>$total_lube,'credit_sales'=>$credit_sales,'credit_sales_manual'=>$credit_sales_manual,
		  'Nozzle_No'=>$data2,'Tank'=>$data3);
          $json1 = array('status'=>"success","data"=> $json);
          header('Content-type: application/json');   
		  echo json_encode($json1);
		

    }

    else

    {
             $json = array("msg"=>"login failed..! Not Valid ", "key"=>$key,);
			 $json1 = array('status'=>"failed", "data"=> $json);
			  header('Content-type: application/json'); 
             echo json_encode($json1);

    }
    
}
else
{
               $json = array( "msg"=>"Not receive required data..!" );
	           $json1 = array('status'=>"failed","data"=> $json);
			    header('Content-type: application/json'); 
               echo json_encode($json1);

}

?>