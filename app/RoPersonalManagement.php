<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoPersonalManagement extends Model
{
    protected $table="tbl_personnel_master";
    public $timestamps = false;

   public function getShift(){
   	return $this->belongsToMany(Shift::class,'shift_personnel','personnel_id','shift_id');
   }

   public function getTrangections(){
   	return $this->hasMany(TransactionModel::class,'Trans_By','id');
   }

   
}
