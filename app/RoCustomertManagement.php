<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoCustomertManagement extends Model
{
    protected $table = "tbl_customer_master";
    public $timestamps = false;

    public function getstate(){

        return $this->belongsTo(StateManagementModel::class,'state','id');

    }
    public function getcity(){

        return $this->belongsTo(CityModel::class,'city','id');

    }
    public function getro(){

        return $this->belongsTo(RoMaster::class,'RO_code','RO_code');

    }
}
