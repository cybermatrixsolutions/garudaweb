<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\RoMaster;
use App\AdminconfigtModel;
use App\OutletConfigModel;
use App\TableStockItemMaster;
use App\RoPieceListManagement;
use App\RoOwnerManger;
use App\RoCustomertManagement;
use App\IndustryDepartment;
use App\RoPersonalManagement;
use App\TableDesignationMaster;
use App\TableItemPriceListModel;
use App\TableTankMasterModel;
use App\StockItemManagement;
use App\nozzelsReadingModel;
use App\UnitModel;
use App\Principal;
use App\User;
use Auth;
use Session;
use DB;
use App\MastorRO;
use Illuminate\Support\Facades\Input;
use File;
use Carbon\Carbon;
use App\Dipchart;
use Gate;
use App\Stock_item_group;
use App\RoDetails;
use App\TaxRatesModel;
use App\LOBMasterModel;
use App\VechilManage;
use App\Nozzle;
use Log;
class AdminController extends Controller
{
   
  public function login()
  { //dd('hiii');
        return view('backend.login'); 
  }
  
  public function adminLogin(Request $request)
  {   
      
      if($the_user=User::select()->where('user_type','=','admin')->where('email','=',$request->email)->where('password','=',($request->password))->first())
      {

       Auth::login($the_user);
       return redirect('Dashboard');

      }else{ 
         $request->session()->flash('success','Wrong Username or Password');
         return redirect('masterAdmin');
      }
       
  }
  
  public function Dashboard()
  {
	   //dd('hiii');      
    if(Auth::user()->user_type == 4)
      {	
		if(Session::has('ro')){
			$customerInfo = RoCustomertManagement::where('Email',Auth::user()->email)->where('RO_code',Session::get('ro'))->first();//dd($customerInfo);
			if($customerInfo->confirm==0){
				return redirect('customerprofile/'.Auth::user()->id);
			}	
		}
		
       $email =Auth::user()->email;
       $customer_ro = RoCustomertManagement::join('tbl_ro_master', 'tbl_customer_master.RO_code', '=', 'tbl_ro_master.RO_code')
            ->where('tbl_customer_master.Email',$email)
            ->select('tbl_customer_master.*','tbl_ro_master.pump_legal_name as pump_name')->get();

         //dd('hiiiii');
         
          return view('backend.dashboard',compact('customer_ro')); 
      }
      /*$user_email =Auth::user()->email;//dd($user_email);
      $roCode = MastorRO::where('Email',$user_email)->value('RO_code');//dd($roCode);
      $data_price_specific = OutletConfigModel::where('rocode',$roCode)->where('field_name','price_specific')->first();
      if ($data_price_specific->value == "on") {
        //dd($data_price_specific->value);
            $TableItemPriceListModel=TableItemPriceListModel::where('is_active',1)->get();
            if (count($TableItemPriceListModel)>0) {//dd($TableItemPriceListModel);
                $date1 = $TableItemPriceListModel[0]->effective_date;
                $date_last_price =date('Y-m-d H:i:s',strtotime($date1));//dd($date_last_price);
                $fdate = Carbon::now();
                $date2 = new Carbon(str_replace('/', '-',$fdate));
                $form_date =date('Y-m-d H:i:s',strtotime($date2));//dd($date_last_price,$form_date);
                foreach ($TableItemPriceListModel as  $value) {
                    $item[]=$value->item_id;
                    $price[]=$value->price;
                }
               
                if ($date_last_price <= $form_date ) {
                   //dd('hii');
                    while ($date_last_price <= $form_date ) {
                        for($i=0;$i<count($item);$i++)
                        {
                            if(isset($price[$i]) && isset($item[$i])){
                                $is_active = TableItemPriceListModel::where('item_id',$item[$i])->where('is_active',1)->update(['is_active' =>0]);
                                $update_price = new TableItemPriceListModel();
                                $update_price->item_id = $item[$i];
                                $update_price->price = $price[$i];
                                $update_price->effective_date =  $date_last_price;
                                $update_price->save();
                            }
                        }    
                        $date_last_price = date('Y-m-d H:i:s',strtotime($date_last_price.'+ 1 Day'));
                    }      
                }
            
        }
      
      }*/
      //dd($roCode);
   return view('backend.dashboard'); 
  }
  

  /* Principal ID Management Start */


  public function principalCode()
   {
  //$getPrincipal = DB::table('tbl_principal_master')->orderBy('id', 'asc')->get();
  //$data=array('getPrincipal'=>$getPrincipal);
  //return view('backend.principal_code', $data);
    ///dd('hiii');
   return view('backend.principal_code'); 
    
   }
   
  public function add_principal(Request $request)
  {
      $url="";
      $validatedData = $request->validate([
        
         'company_code' => 'required|unique:tbl_company_principal_master',
         'company_name' => 'required',
      ]);
         
      try {

        if($request->hasFile('image'))
         {
            $file = Input::file('image');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $filename = $file->getClientOriginalName();

            $name = $timestamp . '-' . $filename;
            $url = 'images/' . $name;

            $file->move('images/', $name);
         }

        DB::table('tbl_company_principal_master')->insert([
          'company_code' =>$request->input('company_code'),
          'company_name' =>$request->input('company_name'),
             'image' =>$url,
          //'is_active'=>0
         ]);
       
        $request->session()->flash('success','Added Successfully !!');
          
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something Wrong!!');      
      }

      return redirect('principal_listing');
  }
       
  public function getcheck(Request $request)
  {
      $code=$request->input('company_code');
       
      $Customer= DB::table('tbl_company_principal_master')->where('company_code',$code)->get();

      if($Customer->count()>0)
          $Customer='true';

       return response()->json($Customer);

  }

  public function principalListing()
  {     
       
    $data['getPrincipal'] = DB::table('tbl_company_principal_master')->orderBy('id','desc')->get();    
    return view('backend.principal_listing',$data);  
  } 

  public function principalUpdate(Request $request ,$id)
  {   
     $data['principalData']=DB::table('tbl_company_principal_master')->where('id','=',$id)->get();
     
     return view('backend.principal_update', $data);
  }

  public function principalEdit(Request $request ,$id)
  {
       $url=$request->input('image1');
       $imagenew=$request->input('imagenew');  
      $validatedData = $request->validate([
          'company_name' => 'required',
      ]);



      $var=['company_name' =>$request->input('company_name'),'company_code' =>$request->input('company_code')];
      if($imagenew==null)
        $var['image']='';
       //$var['image']=$url;
      if($request->hasFile('image'))
      {
        $file = Input::file('image');
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $filename = $file->getClientOriginalName();

        $name = $timestamp . '-' . $filename;
        $url = 'images/' . $name;

        $file->move('images/', $name);
        
         $var['image']=$url;
      }
  
      DB::table('tbl_company_principal_master')->where('id',$id)->update($var); 
    
     $request->session()->flash('success','Data Updated Successfully!!');     
     return redirect('principal_listing');  

  }

    Public function principalDeleted(Request $request ,$id)
     {  
          try {
            DB::table('tbl_company_principal_master')->where('id','=',$id)->delete();
          $request->session()->flash('success','Data has been deleted Successfully!!');
              
          } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }

      
      return redirect('principal_listing');   
       }
     
  function principalActive(Request $request,$id)//sorry for Active means Deactive this
    { 
      
        DB::table('tbl_company_principal_master')
            ->where('id', $id)
            ->update(['is_active' => 1]);
             $request->session()->flash('success','Activated Successfully !!');
                return redirect('principal_listing');
      
        }   
     
  function principalDeactive(Request $request,$id)//sorry for DeActive means Active this
    {
      
        DB::table('tbl_company_principal_master')
            ->where('id', $id)
            ->update(['is_active' => 0]);
                $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('principal_listing');
                
                   
    }      
       
  /* Principal  ID Management End */
  
  
  
  /* Unit Of Measure Management Start*/  
      
  public function unitMeasure()
      {
    
    return view('backend.unit_of_measure'); 
  
      }
     
  public function add_unit_measure(Request $request){

         
          $validatedData = $request->validate([
            
             'Unit_Symbol' => 'required|unique:tbl_unit_of_measure',
             'Unit_Name' => 'required|unique:tbl_unit_of_measure',
             'No_of_Decimals' => 'required',
        ]);

        try{
            $input = $request->all();
            DB::table('tbl_unit_of_measure')->insert($input);
            $request->session()->flash('success','Added Successfully  !!');
         } catch(\Illuminate\Database\QueryException $e){
                
            $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
        }

        return redirect('unit_of_measure');

      }   
  public function unit_measure_listing()
        {     

       $data['getUnitMeasure'] = DB::table('tbl_unit_of_measure')->orderBy('id', 'desc')->get();  
           return view('backend.unit_of_measure',$data);  
        }    
  public function unit_measure_update(Request $request ,$id)
      {   
       $unitMeasureData=DB::table('tbl_unit_of_measure')->where('id','=',$id)->get();
        $getUnitMeasure = DB::table('tbl_unit_of_measure')->orderBy('Unit_Symbol', 'asc')->get();    
       return view('backend.unit_of_measure_update',compact('unitMeasureData','getUnitMeasure'));
      }

     public function unitMeasureEdit(Request $request ,$id)
        {
            $validatedData = $request->validate([
            
             'Unit_Symbol' => 'required|unique:tbl_unit_of_measure,Unit_Symbol,'.$id,
             'Unit_Name' => 'required|unique:tbl_unit_of_measure,Unit_Name,'.$id,
            
        ]);
       try{
           $input=$request->all();          
           DB::table('tbl_unit_of_measure')->where('id',$id)->update($input); 
           $request->session()->flash('success','Data Updated Successfully!!');     
           return redirect('unit_of_measure');  
         }
         catch(\Illuminate\Database\QueryException $e){
                
            $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
        }
         return bakc();

        }

    Public function unit_measure_delete(Request $request ,$id)
     {  
      DB::table('tbl_unit_of_measure')->where('id','=',$id)->delete();
      $request->session()->flash('success','Data has been deleted Successfully!!');
      return redirect('unit_of_measure');   
       } 
  function unitMeasureDeactive(Request $request,$id)
    { 
      
        DB::table('tbl_unit_of_measure')
            ->where('id', $id)
            ->update(['is_active' => 0]);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('unit_of_measure');
      
              }   
     
  function unitMeasureActive(Request $request,$id)
    {
      
        DB::table('tbl_unit_of_measure')
            ->where('id', $id)
            ->update(['is_active' => 1]);
                $request->session()->flash('success','Activated Successfully !!');
                return redirect('unit_of_measure');
                
                   
    }   


        public function nozalShow($id)
        { 
            if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
                  }
                 $rocode='';
                 if (Auth::user()->getRocode!=null) {
                    $rocode=Auth::user()->getRocode->RO_code;
                 }
            $pedestalData=DB::table('tbl_pedestal_nozzle_master')->where('id','=',$id)->get();
           $pedestal_master  =  DB::table('tbl_pedestal_master')
            ->where('is_active', '>=',1)->orderBy('id', 'asc')->get();
            $tank_master  =  DB::table('tbl_tank_master')
            ->where('is_active', '>=',1)->orderBy('id', 'asc')->get();

            $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->select('tbl_price_list_master.*')->get();
           
           return view('backend.nozzles_view_page', compact('pedestalData','fuel_type','tank_master','pedestal_master'));
           
         
        }
     
     
  /* Unit Of Measure Management End*/
  
  
  
  /* Stock Item Group Management Start */
  
  public function stock_item_group()
     { 
       $data['getStockItem'] = DB::table('tbl_stock_item_group')->orderBy('Group_Name', 'asc')->get(); 
          
     return view('backend.stock_item_group',$data);   
     }
     
  public function add_stock_item_group(Request $request){
     $validatedData = $request->validate([
        
        
         'Group_Name' => 'required|unique:tbl_stock_item_group',
        
         
    ]);
          $groupcode = mt_rand(100000, 999999);
    try {
         //   DB::table('tbl_stock_item_group')->insert([
         //  'Group_Code' =>$groupcode,
         //  'Group_Name' =>$request->input('Group_Name'),
         //  'parent' =>$request->input('category'),
         //  //'is_active'=>0
         // ]);
           $Stock_item_group = new Stock_item_group();
           $Stock_item_group->Group_Code=$groupcode;
           $Stock_item_group->Group_Name=trim($request->input('Group_Name'));
           $Stock_item_group->parent=$request->input('category');
           $Stock_item_group->save();       
           if($request->input('category')) {
            $LOBMasterModels = new LOBMasterModel();
            $LOBMasterModels->lob_name=trim($request->input('Group_Name'));
            $LOBMasterModels->stock_idg=$Stock_item_group->id;
            $LOBMasterModels->save();
           }
      $request->session()->flash('success','Added Successfully !!');
    }
    catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Somthing Wrong');
                
            }
      return redirect('stock_item_group');
      }   
  public function stock_item_group_listing()
        {     

       $data['getStockItem'] = Stock_item_group::orderBy('id', 'desc')->get(); 
       
           return view('backend.stock_item_group',$data);  
        }

  public function stock_item_group_update(Request $request ,$id)
      {   
      
       $getStockItem = DB::table('tbl_stock_item_group')->orderBy('Group_Name', 'asc')->get(); 
       $stockItemData=DB::table('tbl_stock_item_group')->where('id','=',$id)->get();
     
       return view('backend.stock_item_groupchange', compact('stockItemData','getStockItem'));

      }

     public function stock_item_group_edit(Request $request ,$id)
        {
                
           DB::table('tbl_stock_item_group')->where('id',$id)->update(['parent' =>$request->input('category'),
          'Group_Name' =>$request->input('Group_Name'),]);
           if($request->input('category')) {
            $LOBMasterModel = LOBMasterModel::where('stock_idg',$id)->first();
            $LOBMasterModel->lob_name=$request->input('Group_Name');
            $LOBMasterModel->save();
           }
           $request->session()->flash('success','Data Updated Successfully!!');     
           return redirect('stock_item_group');  
        }

    Public function stock_item_group_delete(Request $request ,$id)
     {  
      DB::table('tbl_stock_item_group')->where('id','=',$id)->delete();
      $request->session()->flash('success','Data has been deleted Successfully!!');
      return redirect('stock_item_group');   
       }
     
  function stockItemGroupDeactive(Request $request,$id)
    { 
      
        DB::table('tbl_stock_item_group')
            ->where('id', $id)
            ->update(['is_active' => 0]);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('stock_item_group');
      
        }   
     
  function stockItemGroupActive(Request $request,$id)
    {
      
        DB::table('tbl_stock_item_group')
            ->where('id', $id)
            ->update(['is_active' => 1]);
                $request->session()->flash('success','Activated Successfully !!');
                return redirect('stock_item_group');
                
                   
    }        
  
  /* Stock Item Group Management End */

  
  /* Designation Management Start */
  
  
  public function designation()
     {    
        $designation=TableDesignationMaster::orderBy('id', 'desc')->get();
         return view('backend.designation',compact('designation'));
     }
     
  public function add_designation(Request $request){

     $validatedData = $request->validate([
          'Designation_Name' => 'required|unique:tbl_designation_master'
         ]);
    
            
          /* $validatedData = $request->validate([
         
              'Designation_Name' => 'required|unique:tbl_designation_master'
          ]);*/
        
      try{
        $de=0;
        if($request->input('Reporting_To_Designation'))
          $de=$request->input('Reporting_To_Designation');

          DB::table('tbl_designation_master')->insert([
          'Designation_Name' =>$request->input('Designation_Name'),
          'Reporting_To_Designation' =>$de,
          //'is_active'=>0
         ]);
        $request->session()->flash('success','Successfully Added Designation!!');
        }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something Wrong!!');      
      }
        return redirect('designation');
      }   
  public function designation_listing()
        {     

           $designation=TableDesignationMaster::orderBy('Designation_Name', 'asc')->get();

           $getDesignation = TableDesignationMaster::orderBy('id', 'desc')->get();
          
       
           return view('backend.designation',compact('designation','getDesignation'));
        }    
  public function designation_update(Request $request ,$id)
     {   
           $designation=DB::table('tbl_designation_master')->orderBy('Designation_Name', 'asc')->get();

     $designationData=DB::table('tbl_designation_master')->where('id','=',$id)->orderBy('Designation_Name', 'asc')->get();
     
     return view('backend.designation_update', compact('designation','designationData'));
     }

    public function designation_edit(Request $request ,$id)
        {
          try
          {
              $validatedData = $request->validate([
                  'Designation_Name' => 'required|unique:tbl_designation_master,id,'.$id,
               ]);
              
             DB::table('tbl_designation_master')->where('id',$id)->update([
          
                'Designation_Name'=>$request->input('Designation_Name'),
                'Reporting_To_Designation'=>$request->input('Reporting_To_Designation'),
                

               ]); 
           $request->session()->flash('success','Data Updated Successfully!!');   
          }
          catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something Wrong!!');
                
            } 

            
           return redirect('designation');  
        }

    Public function designation_delete(Request $request ,$id)
     {  
      DB::table('tbl_designation_master')->where('id','=',$id)->delete();
      $request->session()->flash('success','Data has been deleted Successfully!!');
      return redirect('designation');   
       }
     
  function designationDeactive(Request $request,$id)
    { 
      
        DB::table('tbl_designation_master')
            ->where('id', $id)
            ->update(['is_active' => 0]);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('designation');
      
        }   
     
  function designationActive(Request $request,$id)
    {
      
        DB::table('tbl_designation_master')
            ->where('id', $id)
            ->update(['is_active' => 1]);
                $request->session()->flash('success','Activated Successfully !!');
                return redirect('designation');
                
                   
    }      
  
  /* Designation Management End */
  
  
  
  
  /* Dip Stick Management Start */
  
  
  public function dip_stick()
     {    
     return view('backend.dip_stick');  
     }
     
  public function add_dip_stick(Request $request){
    
      $input = $request->all();
          // dd($input);die;
      DB::table('tbl_dip_stick_master')->insert($input);
      $request->session()->flash('success','Added Successfully !!');
      return redirect('dip_stick');
      }   
  public function dip_stick_listing()
        {     

       $data['getDipStick'] = DB::table('tbl_dip_stick_master')->orderBy('id', 'asc')->get(); 
       
           return view('backend.dip_stick',$data);  
        }    
  public function dip_stick_update(Request $request ,$id)
      {   
       $data['dipSticData']=DB::table('tbl_dip_stick_master')->where('id','=',$id)->get();
       
       return view('backend.dip_stick_update', $data);
      }

    public function dip_stick_edit(Request $request ,$id)
        {
           
           try{
                $input=$request->all();           
               DB::table('tbl_dip_stick_master')->where('id',$id)->update($input); 
               $request->session()->flash('success','Data Updated Successfully!!'); 
            }catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something Wrong!!');
                
            }    
           return redirect('dip_stick');  
        }

    Public function dip_stick_delete(Request $request ,$id)
     {  
      DB::table('tbl_dip_stick_master')->where('id','=',$id)->delete();     
      $request->session()->flash('success','Data has been deleted Successfully!!');
      return redirect('dip_stick');   
       }
     
  function dipStickDeactive(Request $request,$id)
     { 
      
        DB::table('tbl_dip_stick_master')
            ->where('id', $id)
            ->update(['is_active' => 0]);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('dip_stick');
      
        }   
     
  function dipStickActive(Request $request,$id)
    {
      
        DB::table('tbl_dip_stick_master')
            ->where('id', $id)
            ->update(['is_active' => 1]);
                $request->session()->flash('success','Activated Successfully !!');
                return redirect('dip_stick');
                
                   
    }      
  
  
  /* Tax Management Start */
  
  
  
 public function tax()
     { 
          
    
     return view('backend.tax');

     }
     
  public function add_tax(Request $request){
   
    
     // $input = $request->all();
          
     $validatedData = $request->validate([
         'Tax_Code' => 'required|unique:tbl_tax_master',
         //'Tax_Type' => 'required|',
         //'Tax_percentage' => 'required',
         //'Description' => 'required|',
         //'strate_type' => 'required',
         
     ]);
     try{

        $tax=['Tax_Code' =>$request->input('Tax_Code'),
              'Tax_Type' =>$request->input('Tax_Type'),
              'Tax_percentage'=>$request->input('Tax_percentage'),
              'Description'=>$request->input('Description'),
              'row_columns'=>'row',
              'strate_type'=>$request->input('strate_type'),
              'position'=>$request->input('position'),
              'tax_name'=>$request->input('tax_rate')
            ];
         
         if(trim($request->input('Tax_Type'))!='GST'){
           $tax['tax_name']=$request->input('vat_tax_rate');
         }
        
        
           $tax['GST_Type']=$request->input('GST_Type');
       

        DB::table('tbl_tax_master')->insert($tax);

     
          $request->session()->flash('success','Added Successfully !!');
        }
        catch(\Illuminate\Database\QueryException $e){
                    
                    $request->session()->flash('success','Something wrong!!');  
          }
        return redirect('tax');
}
public function checkgst($gst)
{   
      
      $getTax = DB::table('tbl_tax_master')->where('Tax_Code', 'like', $gst.'%')->get();

      $taxt='false';
      if($getTax->count()>0)
       $taxt='true';
      else
        $taxt='false';
       
    return $taxt;  
}    

  public function tax_listing()
        {   
          $states  = DB::table('states')->get();  
          $state='';

           if (Auth::user()->getRocode!=null) {
              $scode=Auth::user()->getRocode->state;
           }

           $state  = DB::table('states')->where('id',$scode)->first();  

          $getTax = DB::table('tbl_tax_master')->where('Tax_Code','like',$state->statecode.'%')->orderBy('id','desc')->get(); 
          $type_list = TaxRatesModel::where('tax_type','GST')->get();
       
           return view('backend.tax',compact('getTax','states','type_list'));  
        }    
  public function tax_update(Request $request ,$id)
      {   
       $data['taxData']=DB::table('tbl_tax_master')->where('id','=',$id)->get();
       
       return view('backend.tax_update', $data);
      }

    public function tax_edit(Request $request ,$id)
        {
          // $input=$request->all();          
           DB::table('tbl_tax_master')->where('id',$id)->update([
          
          'Tax_Type' =>$request->input('Tax_Type'),
          'Tax_percentage'=>$request->input('Tax_percentage'),
         
         ]); 
           $request->session()->flash('success','Data Updated Successfully !!');     
           return redirect('tax');  
        }

    public function tax_delete(Request $request ,$id)
     {  
      DB::table('tbl_tax_master')->where('id','=',$id)->delete();     
      $request->session()->flash('success','Data has been deleted Successfully!!');
      return redirect('tax');   
       }
  
  function taxDeactive(Request $request,$id)
     { 
      
        DB::table('tbl_tax_master')
            ->where('id', $id)
            ->update(['is_active' => 0]);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('tax');
      
        }   
     
  function taxActive(Request $request,$id)
    {     
        DB::table('tbl_tax_master')
            ->where('id', $id)
            ->update(['is_active' => 1]);
                $request->session()->flash('success','Activated Successfully !!');
                return redirect('tax');
  }
  /* Tax Management Start */

  
  /* Accounting Management Start */
    
    
  public function accounting()
     {    
     return view('backend.accounting');   
     }
     
  public function add_accounting(Request $request){
    
      $validatedData = $request->validate([
         
         'Master_Code' => 'required|unique:tbl_accounting_ledger_master',
         'Master_Name' => 'required',
         
     ]);

      try{
          DB::table('tbl_accounting_ledger_master')->insert([
          'Master_Code' =>$request->input('Master_Code'),
          'Master_Name' =>$request->input('Master_Name'),
          //'is_active'=>0
         ]);
     
        $request->session()->flash('success',' Added Successfully !!');
        return redirect('accounting');
      }catch(\Illuminate\Database\QueryException $e){
                
              $request->session()->flash('success','Something Wrong!!');
              return back();
        }
      }   
  public function accounting_listing()
        {     

       $data['getAccounting'] = DB::table('tbl_accounting_ledger_master')->orderBy('id', 'desc')->get(); 
       
           return view('backend.accounting',$data);  
        }    
  public function accounting_update(Request $request ,$id)
      {   
       $data['accountingData']=DB::table('tbl_accounting_ledger_master')->where('id','=',$id)->get();
       
       return view('backend.accounting_update', $data);
      }

    public function accounting_edit(Request $request ,$id)
        {
          try{
               DB::table('tbl_accounting_ledger_master')->where('id',$id)->update([
              
                'Master_Code'=>$request->input('Master_Code'),
                'Master_Name'=>$request->input('Master_Name'),

               ]); 
           $request->session()->flash('success','Data Updated Successfully!!');     
           return redirect('accounting');

           }catch(\Illuminate\Database\QueryException $e){
                
              $request->session()->flash('success','Something Wrong!!');
              return back();
           }  
        }

    Public function accounting_delete(Request $request ,$id)
     {  
      DB::table('tbl_accounting_ledger_master')->where('id','=',$id)->delete();     
      $request->session()->flash('success','Data has been deleted Successfully!!');
      return redirect('accounting');   
       }
  function accountingDeactive(Request $request,$id)
     { 
      
        DB::table('tbl_accounting_ledger_master')
            ->where('id', $id)
            ->update(['is_active' => 1]);
             $request->session()->flash('success','Activated Successfully !!');
                return redirect('accounting');
      
        }   
  

  function accountingActive(Request $request,$id)
    {     
        DB::table('tbl_accounting_ledger_master')
            ->where('id', $id)
            ->update(['is_active' => 0]);
                $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('accounting');
  }
  
    /* Accounting Management Start */
    
    
  
    public function stock_item_management()
     {  
     
      $principle_company = DB::table('tbl_company_principal_master')->orderBy('company_name', 'asc')->get();
      $stock_group = DB::table('tbl_stock_item_group')->orderBy('Group_Name', 'asc')->get();
      $unit_measure = DB::table('tbl_unit_of_measure')->orderBy('Unit_Name', 'asc')->get();
      $stock_list = StockItemManagement::orderBy('id', 'asc')->get();

  
    return view('backend.stock_item_management',compact(['principle_company','stock_group','unit_measure','stock_list']));
     }
    
    public function addStockItem()
  {
    
  }
  public function retail_updatea(Request $request ,$id)
  {   
     $countries_list =DB::table('countries')->get();
       $retailData=DB::table('tbl_ro_master')->where('id','=',$id)->get();
       $row=RoMaster::where('id',$id)->first();
        $getPrincipal = DB::table('tbl_company_principal_master')->where('is_active', '>=',1)->orderBy('company_name', 'asc')->get();

       
       return view('backend.ro_viewpage',compact('retailData','countries_list','getPrincipal','row'));
   
   // return view('backend.ro_viewpage',compact('row','countries_list','getPrincipal'));

  }
  
    public function retail(){

      
    
    return view('backend.retail',compact('getPrincipal'));
    
  }
  
  public function add_retail(Request $request){

         $messages = [
                     
                      'PAN_No.unique' => 'The pan no. has already been taken.',
                    ];
           $validatedData = $request->validate([
         
             'pump_legal_name' => 'required',
             'pump_address' => 'required',
             
             //'mobile' => 'required|regex:/[0-9]{10}/',
          
             //'Email' => 'required|email',
             'pin_code'=> 'required',
              'Contact_Person_Name'=> 'required',
              'Contact_Person_Email'=> 'required',
              'Contact_Person_Mobile'=> 'required',
              'password'=> 'required',
              'Secondary_Person_Name'=> 'required',
              'Secondary_Person_Email'=> 'required',
              //'passwordSecondary'=> 'required',
              'Secondary_Person_Mobile'=> 'required',
              'GST_prefix'=> 'required',
              'GST_serial'=> 'required',
              'VAT_prefix'=> 'required',
              'VAT_serial'=> 'required',
              'Delivery_Slip_Prefix'=> 'required',
              'Delivery_serial'=> 'required',
              'GST_Slip_serial'=> 'required',
              'mobile' => 'required',
              'Email' => 'required|email|unique:tbl_ro_master,Email',
              'Email' => 'unique:users',
             
         ],$messages);
       
    //dd($request->all());
    try {
     
       $neusers=User::where('mobile',$request->input('mobile'))->get();

       if($neusers->count()>0){
           $request->session()->flash('success','Mobile No Already Exist !!');
           return back(); 
       }

      $company_code =$request->input('company_code');
      $length = 5;
      $randomBytes = openssl_random_pseudo_bytes($length);
      $characters = '0123456789';
      $charactersLength = strlen($characters);
      $result = '';

      for ($i = 0; $i < $length; $i++)
         $result .= $characters[ord($randomBytes[$i]) % $charactersLength];

      $MastorRO= new MastorRO();
      $MastorRO->company_code=$request->input('company_code');
      $MastorRO->RO_code='RO'.$result;
      $MastorRO->pump_legal_name=$request->input('pump_legal_name');
      $MastorRO->Ro_type=$request->input('Ro_type');
      $MastorRO->customer_care=$request->input('customer_care');
      $MastorRO->pump_address=$request->input('pump_address');
      $MastorRO->address_2=$request->input('address_2');
      $MastorRO->address_3=$request->input('address_3');
      $MastorRO->city=$request->input('city');
      $MastorRO->state=$request->input('state');
      $MastorRO->pin_code=$request->input('pin_code');
      $MastorRO->country=$request->input('country');
      $MastorRO->phone_no=$request->input('phone_no');
      $MastorRO->GST_prefix=$request->input('GST_prefix');
      $MastorRO->GST_serial=$request->input('GST_serial');
      $MastorRO->VAT_prefix=$request->input('VAT_prefix');
      $MastorRO->VAT_serial=$request->input('VAT_serial');
      
      
      //$MastorRO->GST_invoice_no=$request->input('GST_invoice_no');

      //$MastorRO->VAT_invoice_no=$request->input('VAT_invoice_no');
      $MastorRO->mobile=$request->input('mobile');
      $MastorRO->website=$request->input('website');
      $MastorRO->Email=$request->input('Email');
      $MastorRO->Lube_License=$request->input('Lube_License');
      $MastorRO->Pump_License=$request->input('Pump_License');
      $MastorRO->VAT_TIN=$request->input('VAT_TIN');
      $MastorRO->GST_TIN=$request->input('GST_TIN');
      $MastorRO->PAN_No=$request->input('PAN_No');
      $MastorRO->save();
      $AdminconfigtModel = AdminconfigtModel::get();
      //dd($AdminconfigtModel);
      foreach ($AdminconfigtModel as $config) {
      $OutletConfigModel = new  OutletConfigModel();
      $OutletConfigModel->field_lable = $config->field_lable;    
      $OutletConfigModel->field_name = $config->field_name; 
      $OutletConfigModel->value = $config->value;
      $OutletConfigModel->input_type = $config->input_type;
      $OutletConfigModel->rocode = $MastorRO->RO_code;
      $OutletConfigModel->save();
      }
      
      $VAT_image='images/VAT.png';
      $PAN_image='images/PAN.png';
      $GST_image='images/GST.png';

      if($request->hasFile('VAT_image'))
         {
            $file = Input::file('VAT_image');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $filename = $file->getClientOriginalName();

            $name = $timestamp . '-' . $filename;
            $VAT_image = 'images/' . $name;

            $file->move('images/', $name);
         }

         if($request->hasFile('GST_image'))
         {
            $file = Input::file('GST_image');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $filename = $file->getClientOriginalName();

            $name = $timestamp . '-' . $filename;
            $GST_image = 'images/' . $name;

            $file->move('images/', $name);
         }

         if($request->hasFile('PAN_image'))
         {
            $file = Input::file('PAN_image');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $filename = $file->getClientOriginalName();

            $name = $timestamp . '-' . $filename;
            $PAN_image = 'images/' . $name;

            $file->move('images/', $name);
         }

      $RoDetails = new RoDetails();
      $RoDetails->ro_id=$MastorRO->id;
      $RoDetails->Contact_Person_Name=$request->input('Contact_Person_Name');
      $RoDetails->D_no_fy=$request->input('D_no_fy');
      $RoDetails->GST_Slip_fy=$request->input('GST_Slip_fy');
      
      $RoDetails->VAT_serial_lenght=$request->input('VAT_serial_lenght');
      $RoDetails->Contact_Person_Email=$request->input('Contact_Person_Email');
      $RoDetails->VAT_serial_fy=$request->input('VAT_serial_fy');
      $RoDetails->Contact_Person_Mobile=$request->input('Contact_Person_Mobile');
      $RoDetails->Secondary_Person_Name=$request->input('Secondary_Person_Name');
      $RoDetails->Secondary_Person_Email=$request->input('Secondary_Person_Email');
      $RoDetails->Secondary_Person_Mobile=$request->input('Secondary_Person_Mobile');
      $RoDetails->Delivery_Slip_Prefix=$request->input('Delivery_Slip_Prefix');
      $RoDetails->Delivery_serial=$request->input('Delivery_serial');
      $RoDetails->GST_Slip_Prefix=$request->input('GST_Slip_Prefix');
      $RoDetails->GST_Slip_serial=$request->input('GST_Slip_serial');
      $RoDetails->SAP_Code=$request->input('SAP_Code');
      $RoDetails->VAT_image=$VAT_image;
      $RoDetails->PAN_image=$PAN_image;
      $RoDetails->GST_image=$GST_image;
      $RoDetails->Bank_Name=$request->input('Bank_Name');
      $RoDetails->Bank_Branch=$request->input('Bank_Branch');
      $RoDetails->Account_Name=$request->input('Account_Name');
      $RoDetails->Account_Number=$request->input('Account_Number');
      $RoDetails->IFSC_Code=$request->input('IFSC_Code');
      $RoDetails->TC_Delivery_Slip=$request->input('TC_Delivery_Slip');
      $RoDetails->TC_for_GST_Invoice_Slip=$request->input('TC_for_GST_Invoice_Slip');
      $RoDetails->TC_for_VAT_Invoice=$request->input('TC_for_VAT_Invoice');
      $RoDetails->TC_for_GST_Invoice=$request->input('TC_for_GST_Invoice');
      $RoDetails->TC_Delivery_Slip2=$request->input('TC_Delivery_Slip2');
      $RoDetails->TC_for_GST_Invoice_Slip2=$request->input('TC_for_GST_Invoice_Slip2');
      $RoDetails->TC_for_VAT_Invoice2=$request->input('TC_for_VAT_Invoice2');
      $RoDetails->TC_for_GST_Invoice2=$request->input('TC_for_GST_Invoice2');
      $RoDetails->D_serial_no_lenght=$request->input('D_serial_no_lenght');
      $RoDetails->GST_Slip_lenght=$request->input('GST_Slip_lenght');
      $RoDetails->GST_serial_lenght=$request->input('GST_serial_lenght');


      if($request->input('D_no_fy')==1)

      $RoDetails->D_no_fy=1;
      else
       $RoDetails->D_no_fy=0;

      if($request->input('GST_Slip_fy')==1)
         $RoDetails->GST_Slip_fy=1;
      else
        $RoDetails->GST_Slip_fy=0;

      if($request->input('GST_serial_fy')==1)
       $RoDetails->GST_serial_fy=1;
       else
         $RoDetails->GST_serial_fy=0;


      if($request->input('VAT_serial_fy')==1)
      $RoDetails->VAT_serial_fy=1;
      else
       $RoDetails->VAT_serial_fy=0;

      $RoDetails->save();
     
      $ro_code = 'RO'.$result;

     
      $neuser=User::where('Email',$request->input('Email'))->first();
            
                        if($neuser==null){

                            $user = new User();
                            $user->name=$request->input('pump_legal_name');
                            $user->email=$request->input('Email');
                            $user->username=$request->input('Email');
                            $user->user_type=3;
                            $user->mobile=$MastorRO->mobile;
                            $user->password=bcrypt($request->input('password'));
                            $user->save();
                            
                        }

          // dd($input);die;
    //  DB::table('tbl_ro_master')->insert($input);
      $request->session()->flash('success','Successfully Added Retail Outlet Details !!');
      return redirect('retail');

       }catch(\Illuminate\Database\QueryException $e){
                
              $request->session()->flash('success','Something Wrong!!');
              return back();
        }

      }   
  public function retail_listing()
        {     
      $countries_list =DB::table('countries')->get();
         
      $getPrincipal = DB::table('tbl_company_principal_master')->where('is_active', '>=',1)->orderBy('company_name', 'asc')->get(); 
            
       $getRetail = DB::table('tbl_ro_master')->join('tbl_company_principal_master', 'tbl_ro_master.company_code', '=', 'tbl_company_principal_master.company_code')
            ->select('tbl_ro_master.*', 'tbl_company_principal_master.company_name')->orderBy('id', 'desc')->get(); 
            
         
       return view('backend.newretails1',compact('getPrincipal','getRetail','countries_list'));
           //return view('backend.retail',$data);  
        }    
  public function retail_update(Request $request ,$id)
      {  
      Log::info('AdminController@retail_update'); 
       $countries_list =DB::table('countries')->get();
       $retailData=DB::table('tbl_ro_master')->where('id','=',$id)->get();
       $row=RoMaster::where('id',$id)->first();
       Log::info('$row   romaster ===== '.print_r($row,true));
       $getPrincipal = DB::table('tbl_company_principal_master')->where('is_active', '>=',1)->orderBy('company_name', 'asc')->get();

       
       return view('backend.retail_update',compact('retailData','countries_list','getPrincipal','row'));
      }

    public function retail_edit(Request $request ,$id)
        {
           $url='images/GST.png';
           if($request->hasFile('image'))
            {
                $file = Input::file('image');
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $filename = $file->getClientOriginalName();

                $name = $timestamp . '-' . $filename;
                $url = 'images/' . $name;

                $file->move('images/', $name);
              }
           $email = $request->input('Email');
           $messages = [
                     
                      'PAN_No.unique' => 'The pan no. has already been taken.',
                    ];
           $validatedData = $request->validate([
         
             'pump_legal_name' => 'required',
             'pump_address' => 'required',
             
             //'mobile' => 'required|regex:/[0-9]{10}/',
          
             //'Email' => 'required|email',
              'pin_code'=> 'required',
              'Contact_Person_Name'=> 'required',
              'Contact_Person_Email'=> 'required',
              'Contact_Person_Mobile'=> 'required',
            
              'Secondary_Person_Name'=> 'required',
              'Secondary_Person_Email'=> 'required',
          
              'Secondary_Person_Mobile'=> 'required',
              'GST_prefix'=> 'required',
              'GST_serial'=> 'required',
              'VAT_prefix'=> 'required',
              'VAT_serial'=> 'required',
              'Delivery_Slip_Prefix'=> 'required',
              'Delivery_serial'=> 'required',
              'GST_Slip_serial'=> 'required',
              'mobile' => 'required',
             'mobile' => 'required|unique:tbl_ro_master,mobile,'.$id,
             //'Email' => 'required|email|unique:tbl_ro_master,Email,'.$id,
            
         ],$messages);
         
           
          try{
                   $ro=DB::table('tbl_ro_master')->where('id',$id)->first();

                $neusers=User::where('email','<>',$ro->Email)->where('mobile',$request->input('mobile'))->get();

                 if($neusers->count()>0){
                     $request->session()->flash('success','Mobile No Already Exist !!');
                     return back(); 
                 }



                $ro=DB::table('tbl_ro_master')->where('id',$id)->first();
                $datupdate=[
                'pump_legal_name'=>$request->input('pump_legal_name'),
                'pump_address'=>$request->input('pump_address'),
                'Ro_type'=>$request->input('Ro_type'),
                'address_2'=>$request->input('address_2'),
                'address_3'=>$request->input('address_3'),
                'city'=>$request->input('city'),
                'state'=>$request->input('state'),
                'pin_code'=>$request->input('pin_code'),
                'phone_no'=>$request->input('phone_no'),
                'mobile'=>$request->input('mobile'),
                'GST_prefix'=>$request->input('GST_prefix'),
                'GST_serial'=>$request->input('GST_serial'),
                'VAT_prefix'=>$request->input('VAT_prefix'),
                'VAT_serial'=>$request->input('VAT_serial'),
                'website'=>$request->input('website'),
                'customer_care'=>$request->input('customer_care'),
                'IMEI_No'=>$request->input('IMEI_No'),
                //'Email'=>$request->input('Email'),
                'Lube_License'=>$request->input('Lube_License'),
                'Pump_License'=>$request->input('Pump_License'),
                'VAT_TIN'=>$request->input('VAT_TIN'),
                'GST_TIN'=>$request->input('GST_TIN'),
                'PAN_No'=>$request->input('PAN_No'),
                'image'=>$url,
               // 'add_retail' =>$request->input('add_retail'),

               ];

               if(Auth::user()->user_type==3){
                   $datupdate['confirm']=1;
               }

          DB::table('tbl_ro_master')->where('id',$id)->update($datupdate);
          $RoDetails =  RoDetails::where('ro_id',$id)->first();
          // dd($RoDetails->VAT_image);
          if($RoDetails!=null){
            $VAT_image=$RoDetails->VAT_image;
            $PAN_image=$RoDetails->PAN_image;
            $GST_image=$RoDetails->GST_image;
          }else{
              $request->session()->flash('success','Something Wrong !!');
            return back();
          }

          if($request->hasFile('VAT_image'))
             {
                $file = Input::file('VAT_image');
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $filename = $file->getClientOriginalName();

                $name = $timestamp . '-' . $filename;
                $VAT_image = 'images/' . $name;

                $file->move('images/', $name);
             }

             if($request->hasFile('GST_image'))
             {
                $file = Input::file('GST_image');
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $filename = $file->getClientOriginalName();

                $name = $timestamp . '-' . $filename;
                $GST_image = 'images/' . $name;

                $file->move('images/', $name);
             }

             if($request->hasFile('PAN_image'))
             {
                $file = Input::file('PAN_image');
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $filename = $file->getClientOriginalName();

                $name = $timestamp . '-' . $filename;
                $PAN_image = 'images/' . $name;

                $file->move('images/', $name);
             }

      
              

               $updatedata=[
                            'Contact_Person_Name'     =>$request->input('Contact_Person_Name'),
                            'Contact_Person_Email'    =>$request->input('Contact_Person_Email'),
                            'Contact_Person_Mobile'   =>$request->input('Contact_Person_Mobile'),
                            'Secondary_Person_Name'   =>$request->input('Secondary_Person_Name'),
                            'Secondary_Person_Email'  =>$request->input('Secondary_Person_Email'),
                            'Secondary_Person_Mobile' =>$request->input('Secondary_Person_Mobile'),
                            'Delivery_Slip_Prefix'    =>$request->input('Delivery_Slip_Prefix'),
                            'Delivery_serial'         =>$request->input('Delivery_serial'),
                            'GST_Slip_Prefix'         =>$request->input('GST_Slip_Prefix'),
                            'GST_Slip_serial'         =>$request->input('GST_Slip_serial'),
                            'SAP_Code'                =>$request->input('SAP_Code'),
                            'VAT_image'               =>$VAT_image,
                            'PAN_image'               =>$PAN_image,
                            'GST_image'               =>$GST_image,
                            'Bank_Name'               =>$request->input('Bank_Name'),
                            'Bank_Branch'             =>$request->input('Bank_Branch'),
                            'Account_Name'            =>$request->input('Account_Name'),
                            'Account_Number'          =>$request->input('Account_Number'),
                            'IFSC_Code'               =>$request->input('IFSC_Code'),
                            'TC_Delivery_Slip'        =>$request->input('TC_Delivery_Slip'),
                            'TC_for_GST_Invoice_Slip' =>$request->input('TC_for_GST_Invoice_Slip'),
                            'TC_for_VAT_Invoice'      =>$request->input('TC_for_VAT_Invoice'),
                            'TC_for_GST_Invoice'      =>$request->input('TC_for_GST_Invoice'),
                            'TC_Delivery_Slip2'       =>$request->input('TC_Delivery_Slip2'),
                            'TC_for_GST_Invoice_Slip2'=>$request->input('TC_for_GST_Invoice_Slip2'),
                            'TC_for_VAT_Invoice2'     =>$request->input('TC_for_VAT_Invoice2'),
                            'TC_for_GST_Invoice2'     =>$request->input('TC_for_GST_Invoice2'),
                            'D_serial_no_lenght'      =>$request->input('D_serial_no_lenght'),
                            'GST_Slip_lenght'         =>$request->input('GST_Slip_lenght'),
                            'GST_serial_lenght'       =>$request->input('GST_serial_lenght'),
                            'VAT_serial_lenght'       =>$request->input('VAT_serial_lenght'),

                           // 'add_retail' =>$request->input('add_retail'),
                           ];
 
                           
                           
                           
                    if($request->input('D_no_fy')==1)
                      $updatedata['D_no_fy']=1;
                    else
                       $updatedata['D_no_fy']=0;

                    if($request->input('GST_Slip_fy')==1)
                      $updatedata['GST_Slip_fy']=1;
                    else
                       $updatedata['GST_Slip_fy']=0;

                    if($request->input('GST_serial_fy')==1)
                      $updatedata['GST_serial_fy']=1;
                    else
                       $updatedata['GST_serial_fy']=0;

                    if($request->input('VAT_serial_fy')==1)
                      $updatedata['VAT_serial_fy']=1;
                    else
                      $updatedata['VAT_serial_fy']=0;

                  DB::table('ro_details')->where('ro_id',$id)->update($updatedata);
       
     
          

                  $neuser=User::where('email',$ro->Email)->first();
                   
                  if($neuser!=null){

                      $user =  User::find($neuser->id);
                      $user->name=$request->input('pump_legal_name');
                      //$user->email=$request->input('Email');
                      $user->mobile=$request->input('mobile');
                      //$user->username=$request->input('Email');

                      if($request->input('password')!=null || trim($request->input('password'))!='')
                         $user->password=bcrypt($request->input('password'));

                      $user->save();
                     
                  }

                 $request->session()->flash('success','Updated Successfully!!');

                if(Auth::user()->user_type===3){
                  return back();
                }else{
                  return redirect('retail');
                }    
                 
            }catch(\Illuminate\Database\QueryException $e){
                  
                $request->session()->flash('success','Something Wrong!!');
                return back();
          } 
        }

    public function retail_delete(Request $request ,$id)
     {  

        try {
            DB::table('tbl_ro_master')->where('id','=',$id)->delete();        
          $request->session()->flash('success','Data has been deleted Successfully!!');
        } 
       catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
      
      return redirect('retail');   
       }
  function retailActive(Request $request,$id)
     { 
      
        DB::table('tbl_ro_master')
            ->where('id', $id)
            ->update(['is_active' => 1]);
             $ro_email = DB::table('tbl_ro_master')->where('id', $id)->pluck('Email')->first();
             $this->roUseractive($ro_email);
             $request->session()->flash('success','Activated Successfully !!');
                return redirect('retail');
      
        }   
        public function roUseractive($ro_email)
        {
          DB::table('users')->where('email', $ro_email)->update(['is_active' => 1]);
        }
     
  function retailDeactive(Request $request,$id)
    {     
        DB::table('tbl_ro_master')->where('id', $id)->update(['is_active' => 0]);
                 $ro_code = DB::table('tbl_ro_master')->where('id', $id)->pluck('RO_code')->first();
                 $ro_email = DB::table('tbl_ro_master')->where('id', $id)->pluck('Email')->first();

                //$this->allRoDataDeactive($ro_code,$ro_email);
                $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('retail');
  }

public function allRoDataDeactive($ro_code,$ro_email)
{

          DB::table('users')->where('email', $ro_email)->update(['is_active' => 0]);
          DB::table('tbl_customer_limit')->where('Ro_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_customer_master')->where('RO_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_customer_request_lube')->where('RO_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_customer_request_petroldiesel')->where('RO_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_customer_vehicle_driver')->where('Ro_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_customer_vehicle_master')->where('Ro_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_pedestal_master')->where('RO_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_personnel_master')->where('RO_Code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_price_list_master')->where('RO_Code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_qrcode_manage')->where('Ro_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_ro_manager_master')->where('RO_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_ro_owner_master')->where('RO_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_ro_petroldiesel')->where('RO_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_ro_petroldiesel_price')->where('RO_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_tank_master')->where('RO_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_tank_reading')->where('Ro_code', $ro_code)->update(['is_active' => 0]);
          DB::table('tbl_tank_reading')->where('Ro_code', $ro_code)->update(['is_active' => 0]);
}

     /* Retail  Outlat Management Start */
public function pedestalc(){
  return view('backend.pedestal');
}
  public function add_pedestalc(Request $request)
  {
     
   try{
  
     $pedestal_id = $request->input('Pedestal_id');
     $pedestal = DB::table('tbl_pedestal_master')->where('id',$pedestal_id)->first();
       $count_record =  DB::table('tbl_pedestal_nozzle_master')->where('Pedestal_id',$pedestal_id)->where('is_active',1)->get();

     if($count_record->count() >= $pedestal->No_of_Nozzles)
     {

      $request->session()->flash('success','You are Already Use '.$pedestal->No_of_Nozzles.'  Nozzles');
      return back();
     }

      $nozzles=DB::table('tbl_pedestal_nozzle_master')->where('RO_code',$request->input('RO_code'))->where('Nozzle_Number',$request->input('Nozzle_Number'))->get();

    if($nozzles->count() >0)
     {

      $request->session()->flash('success','Nozzles No Already Exist');
      return back();
     }
   
  
  // $input = $request->all();
    $date = $request->input('Active_From_Date');
    $date1 = str_replace('/', '-', $date);
    $mysqltimea = date('Y-m-d', strtotime($date1));

    $date = $request->input('Opening_Date');
    $date1 = str_replace('/', '-', $date);
    $mysqltime = date('Y-m-d', strtotime($date1));
         
        
    DB::table('tbl_pedestal_nozzle_master')->insert([
      'RO_code' =>$request->input('RO_code'),
      'Tank_id' =>$request->input('Tank_id'),
      'Pedestal_id' =>$request->input('Pedestal_id'),
      'Nozzle_Number'=>$request->input('Nozzle_Number'),
      'fuel_type'=>$request->input('fuel_type'),
      'Opening_Reading'=>$request->input('Opening_Reading'),
      'Opening_Date'=> $mysqltime,
      'Remarks'=>$request->input('Remarks'),
      'Active_From_Date'=>$mysqltimea,
      //'is_active'=>1
     ]);
 
          $pedestal = DB::table('tbl_pedestal_master')->where('id',$pedestal_id)->first();

         $nozzles_reading = new nozzelsReadingModel();
         $nozzles_reading->RO_code=$pedestal->RO_code;
         $nozzles_reading->Nozzle_No=$request->input('Nozzle_Number');
         $nozzles_reading->Nozzle_Start=$request->input('Opening_Reading');
         //$nozzles_reading->Reading_by=1;
         $nozzles_reading->save();

     $request->session()->flash('success','Added Successfully !!');
     return redirect('pedestal');

     }catch(\Illuminate\Database\QueryException $e){
                
              $request->session()->flash('success','Something Wrong!!');
              return back();
    }
  }   
      public function pedestalc_listing()
        {     
             if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
                      }
                $rocode='';
                 if (Auth::user()->getRocode!=null) {
                    $rocode=Auth::user()->getRocode->RO_code;
                 }

             $getPedestals  =  DB::table('tbl_pedestal_master')->where('is_active', '>=',1)->orderBy('id', 'desc');
           
              
              if(Auth::user()->user_type!=1 && $rocode!='')
                $getPedestals=$getPedestals->where('tbl_pedestal_master.Ro_code',$rocode);

              $getPedestals=$getPedestals->orderBy('id', 'desc')->get();
         //  $getPedestals = DB::table('tbl_pedestal_master')->orderBy('id', 'asc')->get(); 
            $getPedestalsa  =  DB::table('tbl_tank_master')->where('is_active', '>=',1);
            

             if(Auth::user()->user_type!=1 && $rocode!='')
                $getPedestalsa=$getPedestalsa->where('tbl_tank_master.RO_code',$rocode);

              $getPedestalsa=$getPedestalsa->orderBy('id', 'desc')->get();
               $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
               $getpric=  $getpric->pluck('item_id')->toArray();
               $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->whereIn('tbl_price_list_master.id',$getpric)->select('tbl_price_list_master.*');

            if(Auth::user()->user_type!=1 && $rocode!='')
                $fuel_type=$fuel_type->where('tbl_price_list_master.RO_Code',$rocode);

              $fuel_type=$fuel_type->get();
           
            //$getPedestalsa = DB::table('tbl_tank_master')->orderBy('id', 'asc')->get(); 
    
     
       $getPedestal= Nozzle::join('tbl_pedestal_master', 'tbl_pedestal_nozzle_master.Pedestal_id', '=', 'tbl_pedestal_master.id')
            ->select('tbl_pedestal_nozzle_master.*', 'tbl_pedestal_master.Pedestal_Number')->orderBy('id', 'desc');


             if(Auth::user()->user_type!=1 && $rocode!='')
                $getPedestal=$getPedestal->where('tbl_pedestal_master.Ro_code',$rocode);

              $getPedestal=$getPedestal->get();
       
           return view('backend.pedestal',compact('getPedestals','getPedestal','getPedestalsa','fuel_type'));  
        } 

       
      function pedestlDeactive(Request $request,$id)
        { 
      
        DB::table('tbl_pedestal_nozzle_master')
            ->where('id', $id)
            ->update(['is_active' => 0]);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('pedestal');
    }  
    function pedestlActive(Request $request,$id)
    {  
       $nozz=DB::table('tbl_pedestal_nozzle_master')->where('id', $id)->first();
       $pedestal=DB::table('tbl_pedestal_master')->where('id',$nozz->Pedestal_id)->first();
       $count_record =  DB::table('tbl_pedestal_nozzle_master')->where('Pedestal_id',$nozz->Pedestal_id)->where('is_active',1)->get();

     if($count_record->count() >= $pedestal->No_of_Nozzles)
     {

      $request->session()->flash('success','You are Already Use '.$pedestal->No_of_Nozzles.'  Nozzles');
      return back();
     }
        DB::table('tbl_pedestal_nozzle_master')
            ->where('id', $id)
            ->update(['is_active' => 1]);
                $request->session()->flash('success','Activated Successfully !!');
                return redirect('pedestal');
  }
      public function pedestal_edits(Request $request ,$id)
        {


          Log::info('adminController@ pedestal_edits   input - '.print_r($request->all(),true));
           $rocode='';
                 if (Auth::user()->getRocode!=null) {
                    $rocode=Auth::user()->getRocode->RO_code;
                 }

          DB::beginTransaction();

          try {
          $input=$request->all();

          // $input=$request->all();  
           $date = $request->input('Active_From_Date');
           $date1 = str_replace('/', '-', $date);
           $mysqltimea = date('Y-m-d', strtotime($date1));
          // $datea =$request->input('Active_From_Date');
         //  $mysqltimea = date("Y-m-d", strtotime($datea));

          $date = $request->input('Opening_Date');
          $date1 = str_replace('/', '-', $date);
          $mysqltime = date('Y-m-d', strtotime($date1));
          $old_no=DB::table('tbl_pedestal_nozzle_master')->where('id',$id)->where('RO_code',$rocode)->value('Nozzle_Number');

         if($old_no!=$input['Nozzle_Number']){

          Log::info('Old nozzle no. -   '.$old_no);
          Log::info('new nozzle no. -   '.$input['Nozzle_Number']);

           DB::table('tbl_ro_nozzle_reading')->where('Nozzle_No',$old_no)->where('RO_code',$rocode)->update([
      
            'Nozzle_No'=>$input['Nozzle_Number'],
            //'is_active'=>1
           ]);      

         //Read to update nozzle reading table
         }
             
       DB::table('tbl_pedestal_nozzle_master')->where('id',$id)->update([
           
          'fuel_type'=>$request->input('fuel_type'),
          'Opening_Reading'=>$request->input('Opening_Reading'),
          'Opening_Date'=> $mysqltime,
          'Remarks'=>$request->input('Remarks'),
          'Active_From_Date'=>$mysqltimea,
          'Nozzle_Number'=>$input['Nozzle_Number'],
          //'is_active'=>1
         ]);  

         DB::commit();
      } catch (\Exception $e) {
          DB::rollback();
          // something went wrong
      }





         //  DB::table('tbl_pedestal_nozzle_master')->where('id',$id)->update($input); 
           $request->session()->flash('success','Data Updated Successfully!!');     
           return redirect('pedestal');  
        }



         public function pestal_update(Request $request ,$id)
      { 

Log::info('AdminController@pertal_update ready to update nozzle !!!!');

         if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                            return redirect('Dashboard');
                    }
              $rocode='';
                   if (Auth::user()->getRocode!=null) {
                      $rocode=Auth::user()->getRocode->RO_code;
                   }
               $pedestalData=DB::table('tbl_pedestal_nozzle_master')->where('id','=',$id)->get();
                   $pedestal_master  =  DB::table('tbl_pedestal_master')->where('is_active', '>=',1);

               if(Auth::user()->user_type!=1 && $rocode!='')
                $pedestal_master=$pedestal_master->where('tbl_pedestal_master.Ro_code',$rocode);

              $pedestal_master=$pedestal_master->orderBy('id', 'asc')->get();
                       
              $tank_master  =  DB::table('tbl_tank_master')->where('is_active', '>=',1);
                

             if(Auth::user()->user_type!=1 && $rocode!='')
                $tank_master=$tank_master->where('tbl_tank_master.RO_code',$rocode);

              $tank_master=$tank_master->orderBy('id', 'asc')->get();
             $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
             $getpric=  $getpric->pluck('item_id')->toArray();
            $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->select('tbl_price_list_master.*');

            if(Auth::user()->user_type!=1 && $rocode!='')
                $fuel_type=$fuel_type->where('tbl_price_list_master.RO_Code',$rocode);

              $fuel_type=$fuel_type->get();
              
       
       return view('backend.pestal_update', compact('pedestalData','fuel_type','tank_master','pedestal_master'));
      }
        Public function pestal_delete(Request $request ,$id)
     {  


      DB::table('tbl_pedestal_nozzle_master')->where('id','=',$id)->delete();     
      $request->session()->flash('success','Data has been deleted Successfully!!');
      return redirect('pedestal');   
       }
      public function nozzlesc(){
         
       
     return view('backend.nozzles'); 
      }
      public function add_pedestalnews(Request $request){
       
       try{

           /* $validatedData = $request->validate([
             'Pedestal_Number' => 'required|unique:tbl_pedestal_master',
             
                ]);*/
            $tbl_pedestal_masters=DB::table('tbl_pedestal_master')->where('RO_code',$request->input('RO_code'))->where('Pedestal_Number',strtoupper($request->input('Pedestal_Number')))->get();
            
            if($tbl_pedestal_masters->count()>0){
               $request->session()->flash('success',' Pedestal Number already exists Enter Other!'); 
               return back();
           
            }


            DB::table('tbl_pedestal_master')->insert([
                  ['RO_code' =>$request->input('RO_code'),'Pedestal_Number'=>strtoupper($request->input('Pedestal_Number')),'No_of_Nozzles'=> strtoupper($request->input('No_of_Nozzles')) ]
            ]);
            //DB::table('tbl_pedestal_master')->insert($input);
            $request->session()->flash('success','Successfully Added Pedestal Management Details !!');

        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');  
            }

        return redirect('nozzles');

      }
      public function pedestalcnew_listing()
      {  
      Log::info('pedestal edit call !!!!!!'); 
          if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
          }
          $rocode='';
         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
         }
          
           $getPedestalnew = DB::table('tbl_pedestal_master')
            ->join('tbl_ro_master', 'tbl_pedestal_master.Ro_code', '=', 'tbl_ro_master.RO_code')
            ->select('tbl_pedestal_master.*','tbl_ro_master.pump_legal_name as pump_name')->orderby('id','desc');

             if(Auth::user()->user_type!=1 && $rocode!='')
                $getPedestalnew=$getPedestalnew->where('tbl_pedestal_master.Ro_code',$rocode);

              $getPedestalnew=$getPedestalnew->get();
               // $getPedestalnew = DB::table('tbl_pedestal_master')->orderBy('id', 'asc')->get(); 
                   //$news = News::where('category_id',$categoryID)->where('is_active', '>=',0)->get();
            $getPedesta =  DB::table('tbl_ro_master')->where('is_active', '>=',1);

            if(Auth::user()->user_type!=1 && $rocode!='')
               $getPedesta= $getPedesta->where('tbl_ro_master.RO_code',$rocode);
            
               $getPedesta= $getPedesta->get();
            // $data['getPedesta'] = DB::table('tbl_ro_master')->orderBy('id', 'asc')->get();
       
           return view('backend.nozzles',compact('getPedestalnew','getPedesta'));  
        } 
    public function pestalnew_deletes(Request $request ,$id)
     {  

          try {
                 DB::table('tbl_pedestal_master')->where('id','=',$id)->delete();         
               $request->session()->flash('success','Data has been deleted Successfully!!');
        
                    
            } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
     
      return redirect('nozzles');   
       }
     function pedestlDeactivenews(Request $request,$id)
        { 
      
        DB::table('tbl_pedestal_master')->where('id', $id)->update(['is_active' => 0]);
            $pedestal_id = DB::table('tbl_pedestal_master')->where('id', $id)->pluck('id')->first();

            $this->allPedestalDataDeactive($pedestal_id);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('nozzles');
    }  

    public function allPedestalDataDeactive($pedestal_id)
    {
     
      DB::table('tbl_pedestal_nozzle_master')->where('Pedestal_id', $pedestal_id)->update(['is_active' => 0]);
    }
    function pedestlnewsActivenews(Request $request,$id)
    {     
        DB::table('tbl_pedestal_master')
            ->where('id', $id)
            ->update(['is_active' => 1]);
                $request->session()->flash('success','Activated Successfully !!');
                return redirect('nozzles');
  }
     public function pestalnew_updateabh($id)
        {     
          if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
         $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $data['getpess'] = DB::table('tbl_pedestal_master')->orderBy('id', 'asc')->where('id',$id)->first();

        $data['getPedesta'] = DB::table('tbl_ro_master')->where('is_active',1);

         if(Auth::user()->user_type!=1 && $rocode!='')
                $data['getPedesta']=$data['getPedesta']->where('tbl_ro_master.RO_code',$rocode);


           $data['getPedesta']= $data['getPedesta']->orderBy('id', 'asc')->get();

           return view('backend.pestalnew_update',$data);  
        } 
        public function pestalnew_edits(Request $request ,$id)
        {


            
          
          // $input=$request->all();  
          $checkUnique =  DB::table('tbl_pedestal_master')
                    ->where('RO_code' ,$request->input('RO_code') )  
                    ->where('Pedestal_Number' ,$request->input('Pedestal_Number')) 
                    ->first();  

          if(!$checkUnique){

           DB::table('tbl_pedestal_master')->where('id',$id)->update( ['RO_code' =>$request->input('RO_code'),'Pedestal_Number'=>strtoupper($request->input('Pedestal_Number')),'No_of_Nozzles'=>strtoupper($request->input('No_of_Nozzles'))]); 
           $request->session()->flash('success','Data Updated Successfully!!');     
            }else{

            $request->session()->flash('success','Pedestal  already Exist!!'); 

            }
           return redirect('nozzles');  
        }
      public function tankc(){

           $unit_measure = DB::table('tbl_unit_of_measure')->where('is_active', '>=',1)->orderBy('Unit_Name', 'asc')->get();

           if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
                }
                 $rocode='';
               if (Auth::user()->getRocode!=null) {
                  $rocode=Auth::user()->getRocode->RO_code;
               }

             $tank_list = TableTankMasterModel::join('tbl_ro_master', 'tbl_tank_master.RO_code', '=', 'tbl_ro_master.RO_code')
             ->select('tbl_tank_master.*','tbl_ro_master.pump_legal_name as pump_name')->orderby('id','desc');

               if(Auth::user()->user_type!=1 && $rocode!='')
                $tank_list=$tank_list->where('tbl_tank_master.RO_code',$rocode);

               $tank_list=$tank_list->get();


               $datas= RoMaster::where('is_active','1');

                 if(Auth::user()->user_type!=1 && $rocode!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

               $datas = $datas->get();
                  $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();

                    $getpric=  $getpric->pluck('item_id')->toArray();
                  
                  $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->select('tbl_price_list_master.*');

                  if(Auth::user()->user_type!=1 && $rocode!='')
                $fuel_type=$fuel_type->where('tbl_price_list_master.RO_Code',$rocode);

              $fuel_type=$fuel_type->get();

                  

               $dipchart=Dipchart::where('is_active','1')->get();

            
        return view('backend.tank',compact('tank_list','datas','fuel_type','dipchart','unit_measure'));


     }
      public function ownerc(){
      
            $datas= RoMaster::where('is_active','1')->get();
            $owner_list = RoOwnerManger::all();  
           return view('backend.owner',compact(['datas','owner_list']));
  
     }
      
    public function personalc(){

        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }

       $datas= RoMaster::where('is_active','1');
       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

       
      /** check admistrater */
       if(Auth::user()->user_type!=1 && $rocode!='')
         $datas=$datas->where('RO_code',$rocode);
        
       $datas=$datas->get();

       $designation= TableDesignationMaster::orderBy('Designation_Name', 'asc')->get();
        // $personal_list = RoPersonalManagement::All();
        $personal_list = DB::table('tbl_personnel_master')->join('tbl_designation_master','tbl_personnel_master.Designation', '=', 'tbl_designation_master.id')
        ->join('tbl_ro_master', 'tbl_personnel_master.RO_Code', '=', 'tbl_ro_master.RO_code')
        ->select('tbl_personnel_master.*','tbl_designation_master.Designation_Name','tbl_ro_master.pump_legal_name as pump_name');
       
        if(Auth::user()->user_type!=1)
         $personal_list=$personal_list->where('tbl_personnel_master.RO_code',Auth::user()->getRocode->RO_code);
        

        $personal_list=$personal_list->orderBy('id', 'desc')->get();

        return view('backend.persoonel',compact(['datas','personal_list','designation']));
     }
      public function price_listc(){
       $datas= RoMaster::where('is_active','1')->get();
       $item_master_list= TableStockItemMaster::where('is_active','1')->get();


     $price_list =RoPieceListManagement::join('tbl_ro_master', 'tbl_price_list_master.RO_Code', '=', 'tbl_ro_master.RO_code')
         ->select('tbl_price_list_master.*','tbl_ro_master.pump_legal_name as pump_name')->get();
   return view('backend.price_list',compact(['datas','price_list','item_master_list']));
   
     }

    public function saveTankMangementData(Request $request)
     {
        $unit = $request->input('Unit_of_Measure');
        //dd($unit);

      $validatedData = $request->validate([

           // 'Tank_Number' => 'required|unique:tbl_tank_master',
             
             'Capacity' => 'required|numeric',
             
        ]);

      $TableTankMaste=TableTankMasterModel::where('RO_code',$request->input('RO_code'))->where('Tank_Number',$request->input('Tank_Number'))->get();
      
      if($TableTankMaste->count()>0){
           $request->session()->flash('success','Tank Number already exists Enter Other!');
          return back();
      }

      $measureunit = DB::table('tbl_unit_of_measure')->where('id',$unit)->first();
         $as=$measureunit->No_of_Decimals;
        //dd($measureunit->No_of_Decimals);
           $Opening_Reading = $request->input('Opening_Reading');

           $asbd=explode('.',$Opening_Reading);
           
           if(isset($asbd[1])){
              if($as<strlen($asbd[1])){
                 $request->session()->flash('success','Please Enter Correct Value of Opening Stock !!');
                return back();
              }
           }
     try{
          $date = $request->input('opening_date');
          $date1 = str_replace('/', '-', $date);
          $mysqltime = date('Y-m-d', strtotime($date1));

          $date = $request->input('active_from_date');
          $date1 = str_replace('/', '-', $date);
          $active_from_date = date('Y-m-d', strtotime($date1));

          $tank_mangement = new TableTankMasterModel();
          $tank_mangement->RO_code = $request->input('RO_code');
          $tank_mangement->Tank_Number = $request->input('Tank_Number');
          $tank_mangement->Capacity = $request->input('Capacity');
          $tank_mangement->fuel_type = $request->input('fuel_type');
          $tank_mangement->Unit_of_Measure=$unit;
          $tank_mangement->Opening_Reading = $Opening_Reading;
          $tank_mangement->Opening_Date = $mysqltime;
          $tank_mangement->Remarks = $request->input('remarks');
          $tank_mangement->dipchart_id = $request->input('dipchartd_dis');
          $tank_mangement->Active_From_Date = $active_from_date;
          $tank_mangement->save();
          $request->session()->flash('success','Added Successfully!!');

          $tank_list = TableTankMasterModel::All();
          $datas= RoMaster::All();
          return redirect('tank');
    }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something Wrong!!');
          return back();
    }  
      
  // return view('backend.tank',['datas'=> $datas,'tank_list'=> $tank_list,]);

    
    }

      public function editTankPage(Request $request ,$id)
        {
             $unit_measure = DB::table('tbl_unit_of_measure')->where('is_active', '>=',1)->orderBy('Unit_Name', 'asc')->get();
             
          if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
          }
             $rocode='';
             if (Auth::user()->getRocode!=null) {
                $rocode=Auth::user()->getRocode->RO_code;
             }

         $tank_mangement_data=DB::table('tbl_tank_master')->where('id','=',$id)->first();

         $datas= RoMaster::where('is_active',1);

            if(Auth::user()->user_type!=1 && $rocode!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();
             $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
             $getpric=  $getpric->pluck('item_id')->toArray();
  
                  $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->select('tbl_price_list_master.*');

                  if(Auth::user()->user_type!=1 && $rocode!='')
                $fuel_type=$fuel_type->where('tbl_price_list_master.RO_Code',$rocode);

              $fuel_type=$fuel_type->get();
             // dd($fuel_type);     
           return view('backend.tank_update',compact(['tank_mangement_data','datas','fuel_type','unit_measure']));
          }

          public function tankViewPage(Request $request ,$id)
          {
              $unit_measure = DB::table('tbl_unit_of_measure')->where('is_active', '>=',1)->orderBy('Unit_Name', 'asc')->get();
             if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
              }

                 $rocode='';
             if (Auth::user()->getRocode!=null) {
                $rocode=Auth::user()->getRocode->RO_code;
             }

               $tank_mangement_data=DB::table('tbl_tank_master')->where('id','=',$id)->first();
           $datas= RoMaster::where('is_active',1);

             if(Auth::user()->user_type!=1 && $rocode!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();

           $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->select('tbl_price_list_master.*')->get();
           
           return view('backend.tank_page_view',compact(['tank_mangement_data','datas','fuel_type','unit_measure']));
          }

         

        public function tankEditData(Request $request ,$id)
        {  
                 $input=$request->all();
          Log::info('AdminController@tankEditData input - '.print_r($request->all(),true));

           Log::info('AdminController@tankEditData input  id - '.$id);
          $tank_mangement = TableTankMasterModel::find($id);
           Log::info('AdminController@tankEditData tank_mangement '.print_r($tank_mangement,true));


            $rocode='';
             if (Auth::user()->getRocode!=null) {
                $rocode=Auth::user()->getRocode->RO_code;
             }



            $Tank_Number=$input['Tank_Number'];

             //check tank with ro 

             Log::info('AdminController@tankEditData rocode   - '.$rocode);
             $tank_check= TableTankMasterModel::where('Tank_Number',$Tank_Number)->where('RO_code',$rocode)->first();
             Log::info('AdminController@tankEditData tank_check '.print_r($tank_check,true));
            
             if($tank_check){

              if($tank_check->id!=$tank_mangement->id){

                  Log::info('AdminController@tankEditData tank_mangement     Tank Already Exist!!!!!!!!!!!!!!!!!!!!');


                  $request->session()->flash('success','Tank Already Exist!!');
                return back();

              }


             }



   // dd($tank_mangement->Unit_of_Measure);
        $measureunit = DB::table('tbl_unit_of_measure')->where('id',$tank_mangement->Unit_of_Measure)->first();
         $as=$measureunit->No_of_Decimals;
        //dd($measureunit->No_of_Decimals);
           $opening_reading = $request->input('opening_reading');

           $asbd=explode('.',$opening_reading);
           
           if(isset($asbd[1])){
              if($as<strlen($asbd[1])){
                 $request->session()->flash('success','Please Enter Correct Value of Opening Stock !!');
                return back();
              }
           }  


         
 try {
       $date = $request->input('opening_date');
              $date1 = str_replace('/', '-', $date);
             $mysqltime = date('Y-m-d', strtotime($date1));

           

              $date = $request->input('active_from_date');
              $date1 = str_replace('/', '-', $date);
             $active_from_date = date('Y-m-d', strtotime($date1));
             $tank_mangement = TableTankMasterModel::find($id);
            $tank_mangement->Capacity = $request->input('Capacity');
            $tank_mangement->fuel_type = $request->input('fuel_type');
            $tank_mangement->Unit_of_Measure = $request->input('Unit_of_Measure');
            $tank_mangement->Opening_Reading = $request->input('opening_reading');
            $tank_mangement->Tank_Number = $request->input('Tank_Number');
            $tank_mangement->Opening_Date = $mysqltime;
            $tank_mangement->Remarks = $request->input('remarks');
            $tank_mangement->Active_From_Date = $active_from_date;
            $tank_mangement->save();


           
                 $request->session()->flash('success','Data Updated Successfully!!');
          } catch(\Illuminate\Database\QueryException $e){

                      $request->session()->flash('success','Something Wrong May be Tank No. Already Exist!!');
                return back();
                
            }

          
           return redirect('tank');
          }

           public function DeletetankData(Request $request ,$id)
          {  

              try {
                      $tank_mangement = TableTankMasterModel::find($id);

                       $tank_mangement->delete();
              
                          
                  } catch(\Illuminate\Database\QueryException $e){

                      $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                      
                  }
                   return redirect('tank');

               

        

   }

     public function tankDeactive(Request $request,$id)
{
   
   $getNozzel = DB::table('tbl_pedestal_nozzle_master')->where('is_active',1)->where('Tank_id', $id)->first();

     if($getNozzel!=null){
       $request->session()->flash('success',' Not Allows,Tank Associated with Nozzel  !!');
       return redirect('tank');
       
     }else{
        DB::table('tbl_tank_master')->where('id', $id)->update(['is_active' =>0]);
        //$tank_id=  DB::table('tbl_tank_master')->where('id', $id)->pluck('id')->first();
        //$this->allTankDataDeactive($tank_id);
              $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('tank');
      }
}

public function allTankDataDeactive($tank_id)
{
  DB::table('tbl_pedestal_nozzle_master')->where('Tank_id', $tank_id)->update(['is_active' =>0]);
}
public function tankActive(Request $request,$id)
{
  DB::table('tbl_tank_master')
            ->where('id', $id)
            ->update(['is_active' =>1]);
             $request->session()->flash('success','Activated Successfully !!');
                return redirect('tank');
}
     
     public function customerc(){

       if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
         $datas= RoMaster::where('is_active','1');

          if(Auth::user()->user_type!=1 && $rocode!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();

            $Industry=IndustryDepartment::where('type','Industry Vertical')->get();
            $Department=IndustryDepartment::where('type','Department')->get();
        $customer_list = RoCustomertManagement::join('tbl_ro_master', 'tbl_customer_master.Ro_code', '=', 'tbl_ro_master.RO_code')
            ->select('tbl_customer_master.*','tbl_ro_master.pump_legal_name as pump_name')->orderBy('id','desc');

            if(Auth::user()->user_type!=1 && $rocode!='')
                $customer_list=$customer_list->where('tbl_customer_master.RO_code',$rocode);

              $customer_list=$customer_list->get();

              //dd($customer_list);

         $countries_list =DB::table('countries')->get();
    return view('backend.customer_page_n',compact(['datas','customer_list','countries_list','Industry','Department']));
     }

       public function driverc(){
    return view('backend.driver');
      }
       
      public function qr_codec(){
    return view('backend.qr_code');
      }
      public function credit_limitc(){
    return view('backend.credit_limit');
      }
        
         public function getrocode(Request $request){
             
             $code=$request->input('rocode');
             //dd($code);
             $Customer= DB::table('tbl_customer_master')->where('RO_code',$code)
               ->where('tbl_customer_master.is_active','=',1)->orderBy('company_name', 'asc')->get();
             
            $Customer=$Customer->pluck('company_name','Customer_Code')->toArray();
            //Log::info('AdminController@getrocode - Customer'.print_r($Customer,true));
            
            return response()->json($Customer);

          }
           public function getregierid(Request $request){
            $code=$request->input('rocode');
            $Customer= DB::table('tbl_customer_vehicle_driver')->where('tbl_customer_vehicle_driver.is_active','=',1)->where('customer_code',$code)->first();
             
             if($Customer!=null)
             return $Customer->Registration_Number;
             else 
              return array();

          }


      public function getrocodes(Request $request){
            $code=$request->input('rocode');
        $Customer= DB::table('tbl_customer_master')->where('RO_code',$code)->where('is_active',1)->orderBy('company_name', 'asc')->get();
            
            $Customer=$Customer->pluck('company_name','Customer_Code')->toArray();
             //Log::info('AdminController@getrocodes getrocodes111 - '.print_r($Customer,true));
            
            return response()->json($Customer);

          }
           public function getregierids(Request $request){
            $code=$request->input('rocode');
             $Customer= DB::table('tbl_customer_vehicle_master')->where('customer_code',$code)->where('is_active',1)->get();


             $Customer=$Customer->pluck('Registration_Number','id')->toArray();

             return response()->json($Customer);

          }
          public function getRegistration_Number(Request $request){

            $Registration_Number=$request->input('registration_number');
            $RO_code=$request->input('RO_code');
            $customer_code=$request->input('customer_code');

             $Customer= DB::table('tbl_customer_vehicle_driver')->where('Ro_code',$RO_code)->where('customer_code',$customer_code)->where('Registration_Number',$Registration_Number)->first();
             
             if($Customer != null)
                echo $Customer->Driver_Mobile;
             else
              echo " ";

          }
          public function getcapcitys(Request $request){
            $registration_number=$request->input('registration_number');
             $Customer= DB::table('tbl_customer_vehicle_master')->where('Registration_Number',$registration_number)->first();

             $data=$Customer->capacity;

             return response()->json($data);

          }

          public function getTaxOfTax(Request $request)
          {

             $statecode = $request->input('statecode');
              
             $tax_of_tax = DB::table('tbl_tax_master')->where('Tax_Code', 'like', $statecode.'%')->get();
             $tax_of_tax=$tax_of_tax->pluck('Tax_Code','id')->toArray();

             return response()->json($tax_of_tax);
          }

          public function getFuelType(Request $request,$ro,$cus,$type='FUEL')
          {
             
              $registration_number=$request->input('registration_number');
              // $cus=$request->input('cus');
             

              $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
              $getpric=  $getpric->pluck('item_id')->toArray();
          
             $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id');
             
             if($type=='FUEL')
             {
                $fuel_type= $fuel_type->where('tbl_stock_item_group.Group_Name',$type);

                 if($registration_number!=null){

                    $VechilManage=VechilManage::where('Registration_Number',$registration_number)->where('customer_code',$cus)->first();
                    
                    if($VechilManage->getitem!=null)
                    $fuel_type= $fuel_type->where('tbl_price_list_master.Sub_Stock_Group',$VechilManage->getitem->Sub_Stock_Group);
                   

                  
                }
             }
             else
             {
                $fuel_type= $fuel_type->where('tbl_stock_item_group.Group_Name','!=','FUEL');
             }

             

              $fuel_type= $fuel_type->where('tbl_price_list_master.RO_Code',$ro)
              ->where('tbl_price_list_master.is_active','=',1)
              ->whereIn('tbl_price_list_master.id',$getpric)->select('tbl_price_list_master.*');

              $fuel_type=$fuel_type->pluck('Item_Name','id')->toArray();

Log::info('AdminController@getFuelType  '.print_r($fuel_type,true));
             return response()->json($fuel_type);


          }


        public function getState(Request $request){
            $code=$request->input('rocode');
            $Customer= DB::table('states')->where('country_id',$code)->get();
            
            $Customer=$Customer->pluck('name','id')->toArray();
            
            return response()->json($Customer);

          }
          public function getstatecoded(Request $request){
            Log::info('input -  '.print_r($request->all(),true));
            $code=$request->input('state');
            $Customer= DB::table('states')->where('id',$code)->first();
            
            //$Customer=$Customer->pluck('statecode','id')->toArray();
             //$member = json_decode($Customer); 
            return $Customer->gstcode;

          }

           public function getstategstcoded(Request $request){
            Log::info(' getstategstcoded   input -  '.print_r($request->all(),true));
            $code=$request->input('state');
            $Customer= DB::table('states')->where('id',$code)->first();
            
            //$Customer=$Customer->pluck('statecode','id')->toArray();
             //$member = json_decode($Customer); 
            return $Customer->gstcode;

          }



          

           public function getCity(Request $request){
            $code=$request->input('rocode');
             
            $Customer= DB::table('cities')->where('state_id',$code)->get();

             $Customer=$Customer->pluck('name','id')->toArray();

             return response()->json($Customer);

          }
          
      public function ro_nozzel_reading()
        {        
            if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
               }
               $rocode='';
               if (Auth::user()->getRocode!=null) {
                  $rocode=Auth::user()->getRocode->RO_code;
               }

           $data = DB::table('tbl_ro_nozzle_reading')->join('tbl_personnel_master','tbl_personnel_master.id','=','tbl_ro_nozzle_reading.Reading_by')->join('tbl_ro_master', 'tbl_ro_nozzle_reading.RO_code', '=', 'tbl_ro_master.RO_code')->select('tbl_ro_nozzle_reading.*','tbl_personnel_master.Personnel_Name','tbl_ro_master.pump_legal_name as pump_name'); 

             if(Auth::user()->user_type!=1 && $rocode!='')
                $data=$data->where('tbl_ro_nozzle_reading.RO_code',$rocode);

              $data=$data->get();
               
               $getPedestals= DB::table('tbl_pedestal_master')->where('Ro_code',$rocode)->get();
               $personnel = DB::table('tbl_personnel_master')->where('Ro_code',$rocode)->get();
          
          

         return view('backend.ro_nozzelread',compact(['data','getPedestals','personnel']));  
        }  
   
       public function getnozzelno(Request $request){
           
            $pedestalnum=$request->input('pedestalnum');
             
            $pedestalnum= DB::table('tbl_pedestal_nozzle_master')->where('Pedestal_id',$pedestalnum)->get();
            
             $pedestalnum=$pedestalnum->pluck('Nozzle_Number','id')->toArray();
             
             return response()->json($pedestalnum);

          }
           public function getstartingend(Request $request){
           
            $nuzzellnum=$request->input('nuzzellnum');
             
            $nuzzellnum= DB::table('tbl_ro_nozzle_reading')->where('Nozzle_No',$nuzzellnum)->get();
        
             $nuzzellnum=$nuzzellnum->pluck('Nozzle_End','id')->toArray();
             
             return response()->json($nuzzellnum);

          }

          public function nozzelReadiingdata (Request $request)
          {
               
               $val=$request->input('Nozzle_End');

               $rocode='';
               if (Auth::user()->getRocode!=null) {
                  $rocode=Auth::user()->getRocode->RO_code;
               }
                $ddd = nozzelsReadingModel::where('Nozzle_No',$request->input('Nozzle_Number'))->where('Nozzle_End',null)->first();
                 if($ddd!=null){
                   

                   if($ddd->Nozzle_Start>$val){

                     $request->session()->flash('success','Greater then old CMR Nozzel Reading Add !!');
                     return Back(); 
                   }

                   nozzelsReadingModel::where('id',$ddd->id)->update(['Nozzle_End' =>$request->input('Nozzle_End'),'Reading_by' =>$request->input('Reading_by')]);

              }
              
              $nozzelsReading = new nozzelsReadingModel();
              $nozzelsReading->RO_code=$rocode;
              $nozzelsReading->Nozzle_No=$request->input('Nozzle_Number');
              $nozzelsReading->Nozzle_Start=$request->input('Nozzle_End');
              
              $nozzelsReading->Reading_by=$request->input('Reading_by');
              $nozzelsReading->save();
              $request->session()->flash('success','Successfully Nozzel Reading Add !!');
             
             return Back();  
          }
        public function csvfileunitofmeasur()
          {
             $unitofmeasurs= UnitModel::where('is_active',1)->orderBy('id','DSC')->get();
          

           $CsvData=array('Unit Symbol,Unit Name,No.Of Decimals,Simple/Compound');  
              //$CsvData=array('Item Name,Price,Effective Date');         

              foreach($unitofmeasurs as $unit){
                    
                  

                              $CsvData[]=$unit->Unit_Symbol.','.$unit->Unit_Name.','.$unit->No_of_Decimals.','.$unit->Simple_Compound;
                     
              }

             
               
              $filename=date('Y-m-d')."-UnitOfMeasures.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers );
          }
       public function csvfilestockitemgroup()
          {
             $stockitem= Stock_item_group::where('is_active',1)->orderBy('id','DSC')->get();
          

           $CsvData=array('Group Name,Parent');  
              //$CsvData=array('Item Name,Price,Effective Date');         
             
              foreach($stockitem as $item){
                    
                  $parentName = ($item->parentName!=null) ? $item->parentName->Group_Name : "";

                  $CsvData[]=$item->Group_Name.','.$parentName;
                     
              }

             
               
              $filename=date('Y-m-d')."-Stockitemgroups.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers );
          }
      
      public function csvfileprincipallisting()
    {

       $Principal= Principal::where('is_active',1)->orderBy('id','DSC')->get();
          

           $CsvData=array('Principal Company Code,Principal Company Name');  
              //$CsvData=array('Item Name,Price,Effective Date');         
             
              foreach($Principal as $Principal){
                    
                 

                  $CsvData[]=$Principal->company_code.','.$Principal->company_name;
                     
              }

             
               
              $filename=date('Y-m-d')."-PrincipalCompanies.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers );

    }
  public function csvfilenewretails()
    {
     $romaster=RoMaster::where('is_active',1)->orderBy('id','DSC')->get();
       $str='';
        
       $str='Principal Company Name';
       $str.=',Outlet Name'; 
       $str.=',Select Outlet Type';
       $str.=',Outlet Address 1';
       $str.=',Outlet Address 2';
       $str.=',Outlet Address 3';
       $str.=',Country';
       $str.=',State';
       $str.=',City';
       $str.=',PIN Code';
       $str.=',Phone Number';
       $str.=',Mobile(+91)';
       $str.=',Email as login ID';
       $str.=',Name';
       $str.=',Email';
       $str.=',Mobile';
       $str.=',Name';
       $str.=',Email';
       $str.=',Mobile';
       $str.=',Customer Care No.';
       $str.=',Website';
       $str.=',Lube License';
       $str.=',Outlet License';
       $str.=',GST Invoice Prefix';
       $str.=',Start from';
       $str.=',Serial no. Lg.';
       $str.=',Reset FY in';
       $str.=',VAT Invoice Prefix';
       $str.=',Start from';
       $str.=',Serial no. Lg.';
       $str.=',Reset FY in';
       $str.=',Principle ERP code';
       $str.=',VAT No.';
       $str.=',PAN';
       $str.=',GSTIN';
       $str.=',Delivery Slip Prefix';
       $str.=',Start from';
       $str.=',Serial no. Lg.';
       $str.=',Reset FY in';
       $str.=',GST Invoice Slip Prefix';
       $str.=',Start from';
       $str.=',Serial no. Lg.';
       $str.=',Reset FYin';
       $str.=',Bank Name';
       $str.=',Bank Branch';
       $str.=',Account Name';
       $str.=',Account Number';
       $str.=',IFSC Code';
       $str.=',T&C for Delivery Slip 1';
       $str.=',T&C for Delivery Slip 2';
       $str.=',T&C for GST Invoice Slip 1';
       $str.=',T&C for GST Invoice Slip 2';
       $str.=',T&C for VAT Invoice 1';
       $str.=',T&C for VAT Invoice 2';
       $str.=',T&C for GST Invoice 1';
       $str.=',T&C for GST Invoice 2';

       $CsvData[]=$str;

      foreach($romaster as $master){

           if($master->Ro_type == 1) {

              $Ro_type="Petrol Pump";

           }elseif($master->Ro_type==2){
              $Ro_type="Other Retail";
           } 
               
            
         

        $str='';
        $str=     $master->principal->company_name;
        $str.=','.$master->pump_legal_name;
        $str.=','.$Ro_type;
        $str.=','.str_replace(',',' ',$master->pump_address);
        $str.=','.str_replace(',',' ',$master->address_2);
        $str.=','.str_replace(',',' ',$master->address_3);

        $str.=','.$master->Country;
        $str.=','.$master->getstate->name;
        $str.=','.$master->getcity->name;
        $str.=','.$master->pin_code;
        $str.=','.$master->phone_no;
        $str.=','.$master->mobile;
        $str.=','.$master->Email;

        $str.=','.$master->getdetails->Contact_Person_Name;
        $str.=','.$master->getdetails->Contact_Person_Email;
        $str.=','.$master->getdetails->Contact_Person_Mobile;
        $str.=','.$master->getdetails->Secondary_Person_Name;
        $str.=','.$master->getdetails->Secondary_Person_Email;

        $str.=','.$master->getdetails->Secondary_Person_Mobile;
        $str.=','.$master->customer_care;
        $str.=','.$master->website;
        $str.=','.$master->Lube_License;
        $str.=','.$master->Pump_License;
        $str.=','.$master->GST_prefix;
        $str.=','.$master->GST_serial;
        $str.=','.$master->getdetails->GST_serial_lenght;
        $str.=','.$master->getdetails->GST_serial_fy;

        $str.=','.$master->VAT_prefix;
        $str.=','.$master->VAT_serial;
        
        $str.=','.$master->getdetails->VAT_serial_lenght;
        $str.=','.$master->getdetails->VAT_serial_fy;

        $str.=','.$master->getdetails->SAP_Code;
        $str.=','.$master->VAT_TIN;
        $str.=','.$master->PAN_No;
        $str.=','.$master->GST_TIN;

        $str.=','.$master->getdetails->Delivery_Slip_Prefix;
        $str.=','.$master->getdetails->Delivery_serial;
        $str.=','.$master->getdetails->D_serial_no_lenght;
        $str.=','.$master->getdetails->D_no_fy;

        $str.=','.$master->getdetails->GST_Slip_Prefix;
        $str.=','.$master->getdetails->GST_Slip_serial;
        $str.=','.$master->getdetails->GST_Slip_lenght;
        $str.=','.$master->getdetails->GST_Slip_fy;

        $str.=','.$master->getdetails->Bank_Name;
        $str.=','.str_replace(',',' ',$master->getdetails->Bank_Branch);
        $str.=','.$master->getdetails->Account_Name;
        $str.=','.$master->getdetails->Account_Number;
        $str.=','.$master->getdetails->IFSC_Code;

        $str.=','.$master->getdetails->TC_Delivery_Slip;
        $str.=','.$master->getdetails->TC_Delivery_Slip2;
        $str.=','.$master->getdetails->TC_for_GST_Invoice_Slip;
       
        $str.=','.$master->getdetails->TC_for_GST_Invoice_Slip2;
        $str.=','.$master->getdetails->TC_for_VAT_Invoice;
        $str.=','.$master->getdetails->TC_for_VAT_Invoice2;
        $str.=','.$master->getdetails->TC_for_GST_Invoice;
        $str.=','.$master->getdetails->TC_for_GST_Invoice2;




        $CsvData[]=$str;
         }
          $filename=date('Y-m-d')."-newretails1.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));


    }

           fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers );
    }

    
  
}