<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\TaxRatesModel;
use DB;
class TaxRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $TaxRatesModel = TaxRatesModel::get();
         //  dd($StateManagementModel);
        return view('backend.taxrates',compact('TaxRatesModel')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create(Request $request)
    {
         
          $vehicle=$request->input('name');  
         
          $validatedData = $request->validate([
          
          'name' => 'required|unique:tax_rates',

         ]);
          try {
                  $VehicleMakeModel = new VehicleMakeModel();
                  $VehicleMakeModel->name =$vehicle;
              
                  $VehicleMakeModel->save();
                  $request->session()->flash('success','Added Successfully!!'); 

            }catch(\Illuminate\Database\QueryException $e){
                        
                $request->session()->flash('success','Something is wrong');      
            }

            return back();
 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rate = $request->input('rate');
        $tax_type = $request->input('tax_type');
        $check_unique = TaxRatesModel::where('rate',$rate)->where('tax_type',$tax_type)->first();
        if(count($check_unique) > 0)
        {
            $request->session()->flash('success','Tax Rate Already Exist');
            return back();
        }
        

        try{
           $tax_rate_save = new TaxRatesModel();
           $tax_rate_save->rate = $request->input('rate');
           $tax_rate_save->tax_type = $request->input('tax_type');
           $tax_rate_save->save();
        }
        catch(\Illuminate\Database\QueryException $e){
                        
                $request->session()->flash('success','Something is wrong');      
            }
         

        
         //  $validatedData = $request->validate([
         //  'name' => 'required|unique:tpl_vechile_make',
         //  'model_name' => 'required|unique:tpl_vechile_make',

         // ]);
         
          $request->session()->flash('success','Data Save Successfully!!'); 
          return redirect('taxrates');
  


    }
      public function getTaxRateAccordingType(Request $request)
          {

             $taxtype = $request->input('taxtype');
              
             $rate_of_type = TaxRatesModel::where('tax_type', $taxtype)->get();
             $rate_of_type=$rate_of_type->pluck('rate','id')->toArray();

             return response()->json($rate_of_type);
          }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function show(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function edit(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContributionModel $contributionModel)
    {
        //
    }
}
