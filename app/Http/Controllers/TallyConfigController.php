<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MastorRO;
use Auth;
use App\User;
use Log;
use DB;
use Carbon\Carbon;


class TallyConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info('tally confing genral  !!!!!!');
        $user_email = Auth::user()->email;
        $roCode = MastorRO::where('Email',$user_email)->first();
        return view('backend.tallyConfig',compact('roCode'));
    }

   
    public function store(Request $request)
    {
        

      Log::info('TallyConfigController@store - input -  '.print_r($request->all(),true));
        $tally_update_status = $request->input('tally_update_status');
        $tally_serial_number = $request->input('tally_serial_number');
        $sync_start_date = $request->input('sync_start_date');
          $sync_end_date = $request->input('sync_end_date');
        $sync_last_date = $request->input('sync_last_date');
        $secret_key = $request->input('secret_key');
if($sync_start_date){

        $sync_start_date = str_replace('/','-',str_replace('AM','',str_replace('PM','', $sync_start_date)));
        $sync_start_date = date('Y-m-d ', strtotime(trim($sync_start_date)));
}

if($sync_last_date){
        $sync_last_date = str_replace('/','-',str_replace('AM','',str_replace('PM','', $sync_last_date)));
        $sync_last_date = date('Y-m-d ', strtotime(trim($sync_last_date)));
      }

if($sync_end_date){
        $sync_end_date = str_replace('/','-',str_replace('AM','',str_replace('PM','', $sync_end_date)));
        $sync_end_date = date('Y-m-d ', strtotime(trim($sync_end_date)));

      }


        Log::info('sync_start_date - '.  $sync_start_date);
        Log::info('sync_end_date - '.  $sync_end_date);
        Log::info('sync_last_date - '.  $sync_last_date);
                
        // dd($sync_start_date);
        $save_key = MastorRO::where('RO_code',$request->input('RO_code'))->first();
      
        $save_key->secret_key = $secret_key ;
        $save_key->tally_update_status = $tally_update_status ;
        $save_key->tally_serial_number = $tally_serial_number ;
        $save_key->sync_start_date = $sync_start_date ;

         $save_key->sync_end_date = $sync_end_date ;
        $save_key->sync_last_date = $sync_last_date ;
        $save_key->save();
        return redirect()->route('tallyConfig');
    }


    public function changekey(Request $request)
    {

        $tally = bin2hex(openssl_random_pseudo_bytes(16));
        $save_key = MastorRO::where('RO_code',$request->input('RO_code'))->first();
// dd($save_key);
      
        $save_key->secret_key = $tally ;
        $save_key->save();

        // return redirect()->route('tallyConfig');

    return response()->json(array("success"=>true,"tally"=>$tally));

  }



 public function showMaster(Request $request)
    {
        Log::info('tally confing  master  show  !!!!!!');
       
        return view('backend.tallyConfig.tallyConfigMaster');
    }

    public function tallyConfigMasterPersonnel(Request $request)
    {
        Log::info('ReportController@tallyConfigMasterPersonnel  input - '.print_r($request->all(),true));
         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }

        $personalData=DB::select( DB::raw("select t2.Personnel_Code as code ,t2.Personnel_Name as name,t1.Personnel_Name as reporting_to,t3.Designation_Name as role,t2.mobile from tbl_personnel_master t1 inner join tbl_personnel_master t2 on t1.id= t2.Reporting_to left join tbl_designation_master t3 on t2.Designation=t3.id where t1.RO_Code='$rocode'") );


        Log::info('personalData === '.print_r($personalData,true));
    
            $str='Code, Employee Name, Role, Reporting To, Mobile Number';
            $CsvData=array($str);
                    
               $i=1;
                 $tval=[];
           
             foreach($personalData as $data){
                 $str='';

                     
                            $str.=$data->code.',';
                            $str.=$data->name.',';
                            $str.=$data->role.',';
                            $str.=$data->reporting_to.',';
                            $str.=$data->mobile.',';
                            
                         $CsvData[]=$str;
                         $i++;
                }  

               
              $filename=date('Y-m-d')."-tallyConfig-personal-master.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
    }

    public function tallyConfigMasterInventoryMaster(Request $request)
    {
        Log::info('tally confing  tallyConfigMasterInventoryMaster  show  !!!!!!');
       
       Log::info('ReportController@tallyConfigMasterPersonnel  input - '.print_r($request->all(),true));
         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }
       
          $customerData= DB::select( DB::raw("select t1.Item_Code,t1.Item_Name,U1.Group_Name as group1,U2.Group_Name as sub_group,t2.price 
             from tbl_price_list_master t1  inner join tbl_item_price_list t2 on t1.id=t2.item_id 
             inner join tbl_stock_item_group AS U1 on U1.id=t1.Stock_Group 
             inner join tbl_stock_item_group AS U2 on U2.id=t1.Sub_Stock_Group 
             where t2.is_active=1 and t1.RO_code='$rocode'") );   



        Log::info('customerData === '.print_r($customerData,true));
    
            $str='Code, Item Name, Group, Sub-Group, Current Price';
            $CsvData=array($str);
                    
               $i=1;
                 $tval=[];
           
             foreach($customerData as $data){
                 $str='';
                           
                            $str.=$data->Item_Code.',';
                            $str.=$data->Item_Name.',';
                            $str.=$data->group1.',';
                            $str.=$data->sub_group.',';
                            $str.=$data->price.',';
                           
                           
                            
                         $CsvData[]=$str;
                         $i++;
                }  

               
              $filename=date('Y-m-d')."-tallyConfigMasterInventoryMaster.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
    }

    public function tallyConfigMasterCustomerMaster(Request $request)
    {
       Log::info('ReportController@tallyConfigMasterPersonnel  input - '.print_r($request->all(),true));
         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }
    
              $customerData=DB::table('tbl_customer_master')->leftJoin('cities','cities.id','=','tbl_customer_master.city')
              ->leftJoin('states','states.id','=','tbl_customer_master.state')
              ->selectRaw('tbl_customer_master.*,cities.name as cname,states.name as sname')
              ->where('RO_code',$rocode)->where('is_active',1)->get();



        Log::info('customerData === '.print_r($customerData,true));
    
            $str='Code,Company Name,Contact Name, Admin mobile, Admin email_id, City, State';
            $CsvData=array($str);
                    
               $i=1;
                 $tval=[];
           
             foreach($customerData as $data){
                 $str='';
                           
                            $str.=$data->Customer_Code.',';
                            $str.=$data->company_name.',';
                             $str.=$data->Customer_Name.',';
                            $str.=$data->Mobile.',';
                            $str.=$data->Email.',';
                            $str.=$data->cname.',';
                            $str.=$data->sname.',';
                           
                            
                         $CsvData[]=$str;
                         $i++;
                }  

               
              $filename=date('Y-m-d')."-tallyConfigMasterCustomerMaster.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
    }


    public function tallyConfigRestView(Request $request){

          Log::info('TallyConfigController@tallyConfigRestView');
          return view('backend.tallyConfig.tallyConfigReset');

    }
    public function tallyConfigRest(Request $request){

        Log::info('TallyConfigController@tallyConfigRest input - '.print_r($request->all(),true));



         $input=$request->all();
         $rocode="";

         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }
          Log::info('TallyConfigController@tallyConfigRest rocode - '.print_r($rocode,true));

        $selected_reset_value=$input['selected_reset_value'];

           $from_date = $request->input('fromdate');
           $to_date = $request->input('to_date');
  

        if($from_date!=null && $to_date!=null){
          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
      

                if($selected_reset_value=="GST SLIP INVOICES"){

                  Log::info('ready to update --  '.$selected_reset_value);
                  $update=DB::table("tbl_customer_transaction")
                  ->where('RO_code',$rocode)
                  ->whereDate('trans_date','>=',$from_dates)
                  ->whereDate('trans_date','<=',$to_dates)
        
                  ->update(['tally_update_status' => 0]);

                   //update tbl_customer_transaction set tally_update_status=0  where on date range



                }else if($selected_reset_value=="GST BILL"){


                   $update=DB::table("invoice")
                   ->where('tax_type','=',2)
                  ->where('invoice_date','>=',$from_dates)
                  ->where('invoice_date','<=',$to_dates)
                   ->where('RO_code',$rocode)
                  ->update(['tally_update_status' => 0]);
                     $select=DB::table("invoice")
                   ->where('tax_type','=',2)
                  ->where('invoice_date','>=',$from_dates)
                  ->where('invoice_date','<=',$to_dates)
        
                  ->select('*')
                  ->get();

                  Log::info(' GST BILL update ---'.print_r($update,true));


                  //update invoice set tally_update_status=0  where tax_type=2 and date range

                  
                }else if($selected_reset_value=="VAT BILL"){

                   $update=DB::table("invoice")
                   ->where('tax_type','=',1)
                    ->where('RO_code',$rocode)
                  ->whereDate('invoice_date','>=',$from_dates)
                  ->whereDate('invoice_date','<=',$to_dates)
        
                  ->update(['tally_update_status_vat' => 0]);

                  Log::info(' Vat BILL update ---'.print_r($update,true));

                   //update invoice set tally_update_status=0  where tax_type=1 and date range

                  
                }else if($selected_reset_value=="FUEL SALES"){

                   $update=DB::table("shifts")
                  ->whereDate('created_at','>=',$from_dates)
                  ->whereDate('created_at','<=',$to_dates)
                   ->where('ro_code',$rocode)
        
                  ->update(['tally_update_status_fuel' => 0]);

                  Log::info('Fuel sales update ---'.print_r($update,true));

                 //  update shifts set tally_update_status_fuel=0 where created date range

                  
                }else if($selected_reset_value=="SHIFT SETTLEMENT"){


                   $update=DB::table("shifts")
                  ->whereDate('created_at','>=',$from_dates)
                  ->whereDate('created_at','<=',$to_dates)
                    ->where('ro_code',$rocode)
        
                  ->update(['tally_update_status' => 0]);


                  Log::info(' shift SETTLEMENT update ---'.print_r($update,true));

                  // Update shifts set tally_update_status=0  where created date range

                  
                }


        }  



        $request->session()->flash('success','Record Update Successfully !!!!!!');

        return redirect('tallyConfigRestView');       
        


    }

    

    

    
}
