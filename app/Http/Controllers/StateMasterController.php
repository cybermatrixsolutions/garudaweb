<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\RoPieceListManagement;
use App\TableStockItemMaster;
use App\RoMaster;
use App\RoLOBItemSelectionModel;
use App\RoLobSelectionModel;
use App\StockItemManagement;
use App\StateManagementModel;

use DB;
use Illuminate\Support\Facades\Redirect;

use App\Http\Controllers\Controller;

class StateMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          
          $state_list = StateManagementModel::All();
   
        return view('backend/state.state_master_page',compact('state_list'));

       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($ro_code)
    {

       
       $pump_legal_name_list= RoMaster::where('is_active','1')->get();
        $lob_item_list= RoLOBItemSelectionModel::where('is_active','1')->get();
        $get_lob= RoLobSelectionModel::where('RO_code',$ro_code)->get();

     return view('backend.lob_item_page',compact('pump_legal_name_list','lob_item_list','get_lob','ro_code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $save_state =  new StateManagementModel();
            $save_state->name = $request->input('name');
            $save_state->statecode = $request->input('statecode');
            $save_state->gstcode = $request->input('gstcode');
            $save_state->country_id = 101;
            $save_state->save();
            $request->session()->flash('success','Added Successfully!!');
            
        } catch(\Illuminate\Database\QueryException $e){
                
                 $request->session()->flash('success','Something wrong!!');
                
            }
       

            
                
                

        return redirect('add_state_page'); 
         
    }


        

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $state_data = StateManagementModel::where('id',$id)->get();
       
         return view('backend/state.state_master_edit_page',compact('state_data'));

    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        

        try {
           
            $save_state = StateManagementModel::find($id);
            $save_state->name = $request->input('name');
            $save_state->statecode = $request->input('statecode');
            $save_state->gstcode = $request->input('gstcode');
           
            $save_state->save();


            $request->session()->flash('success','Data Updated Successfully!!'); 
       } catch(\Illuminate\Database\QueryException $e){
                
                 $request->session()->flash('success','Something wrong!!');
                
            }
       
       
            return redirect('add_state_page'); 
    }

    function active(Request $request,$id, $actt)
    {
        
     $ac = RoPieceListManagement::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $price_list = RoPieceListManagement::find($id);

        $price_list->delete();
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
        return back();
    }
}
