<?php

namespace App\Http\Controllers;

use Auth;
use App\Shift;
use App\PedestalMaster;
use App\RoPersonalManagement;
use App\nozzelsReadingModel;
use App\TransactionModel;
use App\ShiftPedestal;
use App\PaymentModel;
use App\ShiftSettelementModel;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\TableTankMasterModel;
use App\TankReading;

class ShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
     
        //
        $rocode='';
        $id=0;
        $person=[];
        $Padestal=[];
		$tank = [];
        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $rocode=Auth::user()->getPersonel->RO_Code;

        }else{
            
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }
        }
       
        $allshifts=Shift::where('ro_code',$rocode)->where('is_active',1)->get();
        $openshift=Shift::where('ro_code',$rocode)->where('closer_date',null)->get();
       
        foreach ($openshift as $value) {

          
          $shift = ShiftPedestal::where('shift_id',$value->id)->first();
          if (count($shift)>0) {

            $list[] = ShiftPedestal::where('shift_id',$value->id)->first();
           
          }

          # code...
        }
          

        $Shift=Shift::where('ro_code',$rocode)->where('shift_manager',$id)->where('is_active',1)->first();

        $salesmanger=[];

        if($allshifts!=null){
            foreach ($allshifts as $allshift) {
                array_push($salesmanger,$allshift->shift_manager);
                
                if($allshift!=null && $allshift->getPersonnel!=null)
                $person=array_merge($person,$allshift->getPersonnel->pluck('id')->toArray());

                if($allshift!=null && $allshift->getPadestal!=null)
                $Padestal=array_merge($Padestal,$allshift->getPadestal->pluck('id')->toArray());
			
				if($allshift!=null && $allshift->getTanks!=null)
                $tank=array_merge($tank,$allshift->getTanks->pluck('tank_id')->toArray());
            }
            
        }
        
		
		
        $personals=PedestalMaster::where('RO_code',$rocode)->where('is_Active',1)->whereNotIn('id',$Padestal)->get();
        $per=array_merge($person,$salesmanger);
        $pedestals=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->whereIn('Designation',[30,29])->whereNotIn('id',$per)->get();

        $shiftmanager=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->where('Designation',29)->whereNotIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();
		
		$tanks=TableTankMasterModel::where('RO_code',$rocode)->where('is_active',1)->whereNotIn('id',$tank)->get();
		
        
       return view('backend.shiftmanagement.create',compact('pedestals','openshift','personals','Shift','shiftmanager','shifts','tanks'));
    }
        
    //     $Shift=Shift::where('shift_manager',$id)->where('ro_code',$RO_Code)->where('is_active',1)->first();
        

    //     $personals=PedestalMaster::where('RO_code',$RO_Code)->get();
    //     $pedestals=RoPersonalManagement::where('RO_Code',$RO_Code)->where('Designation',34)->get();

    //    return view('backend.shiftmanagement.create',compact('pedestals','personals','Shift'));
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function shiftCloder(Request $request)
    {
          //
        $id=0;
        $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
           if (Auth::user()->getRocode!=null) {
              $RO_Code=Auth::user()->getRocode->RO_code;
           }
        } 

        $shift_manager=$request->input('shift_manager');

        if(isset($shift_manager) && $shift_manager!='')
            $id=$shift_manager;

        $allshifts=Shift::where('ro_code',$RO_Code)->where('is_active',1)->get();
        $salesmanger=[];

        if($allshifts!=null)
          foreach ($allshifts as $allshift) {
               array_push($salesmanger,$allshift->shift_manager);
           }
                    
              

        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('is_Active',1)->where('Designation',29)->whereIn('id',$salesmanger)->get();

        $Shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',1)->first();
       
       return view('backend.shiftmanagement.closer1',compact('Shift','shiftmanager','id'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function shiftCloderStore(Request $request)
    {   
       
       try{
        $id=0;

        if (Auth::user()->getPersonel!=null) {

          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }
        }  
        
        $shift_manager=$request->input('shift_manager');

        if(isset($shift_manager) && $shift_manager!='')
            $id=$shift_manager;

        $padestal=$request->input('padestal');
        $cshift=$request->input('Shift');
        $padestals= explode(',',$padestal);
        $ro_code=$request->input('ro_code');

		
		// Getting tanks reading info
		
		$tankArray = $request->input('tank');
		$capacityArray = $request->input('capacity');
		$unitMeasureArray = $request->input('unit_measure');
		$fuelTypeArray = $request->input('fuel_type');
		$readingCMArray = $request->input('reading_cm');
		$readingArray = $request->input('reading');
		$sumvalueArray = $request->input('sumvalue');
        
		
		//dd($request->all());
		
        foreach ($padestals as $padesno) {
          if(trim($padesno)!=''){
              $Nozzle_Nos=$request->input($padesno.'_Nozzle_No');
              
              foreach ($Nozzle_Nos as $Nozzle_No) {

                $nozzle_start=$request->input($padesno.'_Nozzle_End_'.$Nozzle_No);
                $test=$request->input($padesno.'_test_'.$Nozzle_No);
                $reading=$request->input($padesno.'_reading_'.$Nozzle_No);
                
                nozzelsReadingModel::where('RO_code',$ro_code)->where('Nozzle_No',$Nozzle_No)->where('Nozzle_End',null)->update(['Nozzle_End' =>$nozzle_start,'shift_id'=>$cshift,'test'=>$test,'reading'=>$reading]);
                
                $nozzelsReading = new nozzelsReadingModel();
                $nozzelsReading->RO_code=$ro_code;
                /*$nozzelsReading->test=$test;
                $nozzelsReading->reading=$reading;*/
                $nozzelsReading->Nozzle_No=trim($Nozzle_No);
                $nozzelsReading->Nozzle_Start=$nozzle_start;
                $nozzelsReading->Reading_by=$id;
                $nozzelsReading->save();

              }

          }
            
        }
		
    		// Inserting Tank reading values
    		if(isset($tankArray) && is_array($tankArray) && count($tankArray)>0){
        
    			for($i=0;$i<count($tankArray);$i++){

             $totalesInwards=DB::table('tank_tankinwart')->where('tank_id',$tankArray[$i])->where('status',1)->get();
             $TankReading=TankReading::where('Tank_code',$tankArray[$i])->latest()->first();
             
             $tank_stack=0;
             $salesvalue=0;

             if($TankReading!=null){
                $tank_stack=$TankReading->value;
             }

             foreach ($totalesInwards as $totalesInward) {
               $tank_stack=$totalesInward->value+$tank_stack;
             }
            
             if($totalesInwards->count()>0)
             DB::table('tank_tankinwart')->where('tank_id',$tankArray[$i])->update(['status'=>0]);

             $salesvalue=$tank_stack-$sumvalueArray[$i];

    				$modelTankReading = new TankReading();
    				$modelTankReading->shift_id=$cshift;
    				$modelTankReading->Ro_code=$ro_code;
    				$modelTankReading->Tank_code=$tankArray[$i];
    				$modelTankReading->fuel_type=$fuelTypeArray[$i];
    				$modelTankReading->capacity=$capacityArray[$i];
    				$modelTankReading->unit_measure=$unitMeasureArray[$i];
    				$modelTankReading->Reading=$readingCMArray[$i];
    				$modelTankReading->dip_mm=$readingArray[$i];
    				$modelTankReading->tank_stack=$tank_stack;
            $modelTankReading->sale_value=$salesvalue;
            $modelTankReading->is_active=1;
    				$modelTankReading->value=$sumvalueArray[$i];
    				$modelTankReading->reading_date  = date("Y-m-d");
    				$modelTankReading->save();

           

    			}

    		}

        $ldate = date('Y-m-d H:i:s');
        Shift::where('id',$cshift)->update(['closer_date'=>$ldate,'is_active' =>0]);

        $request->session()->flash('success','Shift Successfully Close ');
        return redirect()->route('shiftAllocation');

        }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return back();
       
    }



    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getShiftOfManager(Request $request,$id)
    {
        $id=0;
        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }
        }

       $Shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('status',0)->get();
       $data=[];
       $Shift->pluck('closer_date','id')->toArray();
       foreach ($Shift as $value) {
           $data[$value->id]=date('d/m/Y',strtotime($value->closer_date));
       }

       return $data;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  

		//dd($request->all());
        try{
         $id=0;
         $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;
        }elseif (Auth::user()->getRocode!=null) {

              $RO_Code=Auth::user()->getRocode->RO_code;
          }
         

        $pes=$request->input('Padastal');
        $salesman=$request->input('salesman');
		$tank=$request->input('tank');

        $shift_manager=$request->input('shift_manager');
        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;

        $shift=new Shift();
        $shift->shift_manager=$id;
        $shift->ro_code=$RO_Code;
        $shift->save();
        $psdata=[];
        $saldata=[];
        $tankdata=[];

        $i=0;
        foreach ($pes as $pe){
           $psdata[$i]=array('shift_id'=>$shift->id,'pedestal_id'=>$pe);
           $i++;
        }

        $i=0;
        foreach ($salesman as $salesm){
           $saldata[$i]=array('shift_id'=>$shift->id,'personnel_id'=>$salesm);
           $i++;
        }
		
		$i=0;
        foreach ($tank as $t){
           $tankdata[$i]=array('shift_id'=>$shift->id,'tank_id'=>$t);
           $i++;
        }

        DB::table('shift_pedestal')->insert($psdata);
        DB::table('shift_personnel')->insert($saldata);
        DB::table('shift_tank')->insert($tankdata);
       }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function show(Shift $shift)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request , $ids)
    {
       $rocode='';
        $id=0;
        $person=[];
        $Padestal=[];
        $tank=[];
        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $rocode=Auth::user()->getPersonel->RO_Code;

        }else{
            
               if (Auth::user()->getRocode!=null) {
                  $rocode=Auth::user()->getRocode->RO_code;
               }
        }
         $allshifts=Shift::where('is_active',1)->get();

         $salesmanger=[];
        if($allshifts!=null){
          foreach ($allshifts as $allshift) {
              array_push($salesmanger,$allshift->shift_manager);
              
              if($allshift!=null && $allshift->getPersonnel!=null)
              $person=array_merge($person,$allshift->getPersonnel->pluck('id')->toArray());

              if($allshift!=null && $allshift->getPadestal!=null)
              $Padestal=array_merge($Padestal,$allshift->getPadestal->pluck('id')->toArray());
		  
			if($allshift!=null && $allshift->getTanks!=null)
			$tank=array_merge($tank,$allshift->getTanks->pluck('id')->toArray());
          }
            
        }

        $shiftmanager=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->where('Designation',29)->whereIn('id',$salesmanger)->get();

        $pedestals=PedestalMaster::where('RO_code',$rocode)->where('is_Active',1)->whereNotIn('id',$Padestal)->get();
        $per=array_merge($person,$salesmanger);
        $personals =RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->whereIn('Designation',[30,29])->whereNotIn('id',$per)->get();
       
        $shiftmanager=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->where('Designation',29)->whereNotIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();
		
		$tanks=TableTankMasterModel::where('RO_code',$rocode)->where('is_active',1)->whereNotIn('id',$tank)->get();
         
        $shift = shift::find($ids);
     
      return view('backend.shiftmanagement.editshift',compact('shift','shiftmanager',
        'personals','pedestals','tanks'));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

   try{
         
         $pedestals=$request->input('Padastal');
         $salemans=$request->input('salesman');
         $shift_manager=$request->input('shift_manager');
         $tanks=$request->input('tank');
         
        if(count($salemans)>0 && count($pedestals)>0 && count($tanks)>0){

            $shift=Shift::find($id);
            $shift->shift_manager=$shift_manager;
            $shift->save();

            $shift->getPadestal()->detach();
            $shift->getPersonnel()->detach();
            $shift->getTanks()->detach();

            $shift->getPadestal()->attach($pedestals);
            $shift->getPersonnel()->attach($salemans);
			
            $shift->getTanks()->attach($tanks);
			
            $request->session()->flash('success','Update Successfully !!');

        }else{
          
            $request->session()->flash('success','Something Wrong !!');

        }

       }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return back();
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shift $shift)
    {
        //
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function settlement(Request $request)
    {   


         $id=0;
        $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }

        }

        $Shiftid=0;

        if($request->input('siftname')!='')
            $Shiftid=$request->input('siftname');
        
        $shift_manager=$request->input('shift_manager');

        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;


        $ather=null;
        $fuels=null;
        $pendin=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('fuel',0)->get();
        
        if($Shiftid!=0)
            $Shift=Shift::where('ro_code',$RO_Code)->where('id',$Shiftid)->where('fuel',0)->first();
        else
            $Shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('fuel',0)->first();
          
       

        if($Shift!=null){

            $ather=TransactionModel::where('shift_id',$Shift->id)->where('petrol_or_lube',2)->get();
            $fuels=TransactionModel::where('shift_id',$Shift->id)->where('petrol_or_lube',1)->get();
        }
        
        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('Designation',29)->orderBy('Personnel_Name', 'asc')->get();
        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->where('IsActive','1')->orderby('order_number','ASC')->orderby('name','ASC')->get();
       
       return view('backend.shiftmanagement.settlement1',compact('Shift','pendin','ather','fuels','PaymentModel','Shiftid','shiftmanager','id'));
    }
    
        /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function settlementLueb(Request $request)
    {   


        $id=0;
        $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }
        }

        $Shiftid=0;
        if($request->input('siftname')!='')
            $Shiftid=$request->input('siftname');
        
        $shift_manager=$request->input('shift_manager');
        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;

        $creditsale=null;
        $ather=null;
        $fuels=null;
        $pendin=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('lueb',0)->get();
        
        if($Shiftid!=0)
            $Shift=Shift::where('ro_code',$RO_Code)->where('id',$Shiftid)->first();
        else
            $Shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('lueb',0)->first();
          


        if($Shift!=null){
           
            $ather=TransactionModel::where('shift_id',$Shift->id)->where('cust_name','!=','credit')->where('petrol_or_lube',2)->get();

            $creditsale=TransactionModel::where('shift_id',$Shift->id)->where('petrol_or_lube',2)->get();
           
        }

        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('Designation',29)->orderBy('Personnel_Name', 'asc')->get();
      
        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->where('IsActive','1')->orderby('order_number','ASC')->orderby('name','ASC')->get();

       
       return view('backend.shiftmanagement.settlementLube',compact('Shift','pendin','ather','PaymentModel','Shiftid','shiftmanager','id','creditsale'));
    }
  

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    
    public function settlementstore(Request $request)
    {    
      try{

         $id=0;
         $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
            
               if (Auth::user()->getRocode!=null) {
                  $RO_Code=Auth::user()->getRocode->RO_code;
                 
               }
        }

        $shift_manager=$request->input('shift_manager');
        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;

        $Nozzle_Amount=$request->input('Nozzle_Amount');
        $Nozzle_grass=$request->input('Nozzle_grass');
        $Nozzle_test=$request->input('Nozzle_test');
        $type=$request->input('type');
        $pes=explode(',',$request->input('paymentmode'));
        $Credit_fuel=$request->input('Credit_fuel');
        $Credit_lube=$request->input('Credit_lube');
        $trans_date=date('Y-m-d H:i:s');
        $trans_by=$id;
        $Diff=$request->input('Diff');
        $shift_id=$request->input('shift_id');
        
        $shift=new ShiftSettelementModel();
        $shift->Nozzle_Amount=$Nozzle_Amount;
        $shift->Nozzle_test=$Nozzle_test;
        $shift->Nozzle_grass=$Nozzle_grass;

        if($type==1){
           $shift->Credit_fuel=$Credit_fuel;
         }else{
            $shift->Credit_lube=$Credit_lube;
         }

        $shift->trans_date=$trans_date;
        $shift->trans_by=$trans_by;
        $shift->Diff=$Diff;
        $shift->type=$type;
        $shift->shift_id=$shift_id;
        $shift->save();

        $psdata=[];

        $i=0;
        foreach ($pes as $pe){
            if(trim($pe)!=''){
                $paymentmode_id=$request->input('paymentmode_'.$pe);
                
                $ref_no=$request->input('ref_nom_'.$pe);

                if($request->input('payment_'.$pe)!=null)
                    $paymen=$request->input('payment_'.$pe);
                else
                     $paymen=0;
               
               if($paymen!=0){

                $psdata[$i]=array('amount'=>$paymen,'type'=>$shift->type,'paymentmode_id'=>$pe,'sattlement_id'=>$shift->id,'ref_no'=>$ref_no);
                $i++;
                
               }
            }
            
        }

        DB::table('paymentmode_sattlement')->insert($psdata);

        if($type==1){
           Shift::where('id',$shift_id)->update(['status' =>1,'fuel'=>1]);
           
        }else{
           Shift::where('id',$shift_id)->update(['status' =>1,'lueb'=>1]);
        }

         //return redirect()->route('settlement');
       
    }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return back();

    }


    public function getShift(Request $request){

       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $from_date=$request->input('request_date');

        $from_dates = new Carbon(str_replace('/', '-',$from_date));
       
        $from_dates = date('Y-m-d', strtotime($from_dates));
         
        $Shifts=Shift::where('ro_code',$rocode)->whereDate('created_at','>=',$from_dates.' 00:00:00')->whereDate('created_at','<=',$from_dates.' 23:59:59')->get();
       

       $data=[];
       foreach ($Shifts as $Shift) {
        $data[$Shift->id]=date('d/m/Y h:m:s a',strtotime($Shift->created_at));
       }
       
     return response()->json($data);
           
        
    }

    public function getPreShift($shiftManager){
       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
        
       $Shift=Shift::where('ro_code',$rocode)->where('shift_manager',$shiftManager)->latest()->first();
       
       $data=['pedestals'=>[],'salesman'=>[]];

       if($Shift!=null){

           foreach ($Shift->getPadestal as $getPadestal) {

            $data['pedestals'][]= $getPadestal->id;

           }

           foreach ($Shift->getPersonnel as $getPersonnel) {

            $data['salesman'][]= $getPersonnel->id;
            
           }
       }
       
      return response()->json($data);
           
        
    }

    
    
}
