<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoPersonalManagement;
use App\PaymentModel;
use App\Shift;
use Auth;
use DB;
use Carbon\Carbon;
use App\QR_Code;
use App\MastorRO;
use App\RoMaster;
use App\CityModel;
use App\StateManagementModel;
use App\Principal;
use App\TransactionModel;
use Log;

class ReprintController extends Controller
{
    //

    /**
    * Reprint print settlement
    */
    public function settlement(){
      
         $id=0;
        $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }

        }
        $allshifts=Shift::where('ro_code',$RO_Code)->where('fuel',1)->whereNotNull('closer_date')->get();

        $salesmanger=[];
        if($allshifts!=null)
          foreach ($allshifts as $allshift) {
               array_push($salesmanger,$allshift->shift_manager);
           }
        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('Designation',29)->whereIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();

// dd($shiftmanager);
        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->where('IsActive','1')->orderby('order_number','ASC')->orderby('name','ASC')->get();
      return view('backend.reprint.settlement.settlement',compact('PaymentModel','shiftmanager'));
    }

     /**
    * Reprint print settlement
    */
    public function settlementReprint(Request $request){

      Log::info('ReprintController@settlement input - '.print_r($request->all(),true));
      
         $id=$request->siftname;
         $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }

        }

        // $Customer= QR_Code::where('id',$id)->first();
        $detail = MastorRO::leftJoin('tbl_customer_master','tbl_ro_master.RO_code','=','tbl_customer_master.RO_code')
        ->where('tbl_ro_master.RO_code',$RO_Code)
        ->select('tbl_ro_master.pump_legal_name as pump_name','tbl_ro_master.pump_address as pump_addetail','tbl_ro_master.city as citys','tbl_ro_master.state as states','tbl_ro_master.image as images','tbl_ro_master.customer_care','tbl_customer_master.Company_name as company_name','tbl_customer_master.Department as department','tbl_ro_master.RO_code as Ro_code')
        ->first();
        Log::info('detail   -  '.print_r($detail,true));
        
        // dd($detail);
         $CityModel=CityModel::where('id',$detail->citys)->first();
         $StateManagementModel=StateManagementModel::where('id',$detail->states)->first();
         $MastorRO=MastorRO::where('RO_code',$detail->Ro_code)->first();
         $Principal=Principal::where('company_code',$MastorRO->company_code)->first();

        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->where('IsActive','1')->orderby('order_number','ASC')->orderby('name','ASC')->get();
        $shift=Shift::find($id);

         $transactionList = TransactionModel::where('RO_code',$RO_Code)->where('shift_id',$shift->id)->where('petrol_or_lube',1)->where('cust_name','credit')->get();

      return view('backend.reprint.settlement.reprint',compact('shift','PaymentModel','Customer','detail','CityModel','StateManagementModel','Principal','MastorRO','transactionList'));
    }



    

    function getshiftbyshiftmanager(Request $request){
       $id=$request->id;
       $start_date=$request->start_date;
       $end_date=$request->end_date;
       // dd($start_date);

        $start_dates = new Carbon(str_replace('/', '-',$start_date));
        $start_dates = date('Y-m-d', strtotime($start_dates));

        $end_dates = new Carbon(str_replace('/', '-',$end_date));
        $end_dates = date('Y-m-d', strtotime($end_dates));
     
       $data=[];
       $shifts=DB::table('shifts')->where('shift_manager',$id)->where('fuel',1)

            ->whereDate('created_at','>=',$start_dates.' 00:00:00')
            ->whereDate('created_at','<=',$end_dates.' 23:59:59')

       ->orderBy('created_at', 'desc')->get();

// log::info('shifts = = = '.print_r($shifts,true));
       foreach ($shifts as $shift) {
        $data[$shift->id]=date('d/m/Y  h:i:s A',strtotime($shift->created_at))." To ".date('d/m/Y  h:i:s A',strtotime($shift->closer_date));
       }
        // if($data){
        //         print_r( $data);
        //     }
      return response()->json($data);
    }

      function deliverySlipFuel(Request $request){
    
    //echo $request->url();
    //echo $request->fullUrlWithQuery(['t'=>'123']);
    //echo $request->path();
    //echo $request->route()->getName();
    //exit;
     $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');
   
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));

         $from_dates = $from_dates." 00:00:00";
         $to_dates = $to_dates." 11:59:59";

      Log::info('GstReprintController@gstSlipFuel -  from_dates -  '.$from_dates  .'   to_dates  - '.$to_dates);


        $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
           $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }
        }
    $roInfo = RoMaster::where('RO_code',$RO_Code)->get()->first();
    $transactionList = null;
    
      $transactionList = TransactionModel::where('RO_code',$RO_Code)->where('petrol_or_lube',1)->where('cust_name','credit')
      ->where(DB::raw("(DATE_FORMAT(trans_date,'%Y-%m-%d'))"),'>=',$from_dates)
      ->where(DB::raw("(DATE_FORMAT(trans_date,'%Y-%m-%d'))"),'<=',$to_dates)
      ->groupBy('invoice_number')->get();

       Log::info('GstReprintController@gstSlipFuel -  transactionList count  -  '.print_r(count($transactionList),true));

    
    return view('backend.reprint.delivery-slip-fuel.index',compact('urlVars','roInfo','transactionList'));
  }
  function deliverySlipFuelPrint(Request $request){
    
    $transId = $request->input('id');
        $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
           $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }
        }
    $roInfo = RoMaster::where('RO_code',$RO_Code)->get()->first();

         $Principal=Principal::where('company_code',$roInfo->company_code)->first();
    $transactionInfo = TransactionModel::where('RO_code',$RO_Code)->where('cust_name','credit')->where('petrol_or_lube',1)->findorfail($transId);
    
  
    return view('backend.reprint.delivery-slip-fuel.print',compact('roInfo','transactionInfo','Principal'));
  }
}
