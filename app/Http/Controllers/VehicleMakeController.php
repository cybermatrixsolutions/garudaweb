<?php

namespace App\Http\Controllers;

//use App\CityController;
use Illuminate\Http\Request;
use App\VehicleMakeModel;
use App\VehicleModel;
use DB;
class VehicleMakeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $VehicleMake = VehicleMakeModel::orderby('id','desc')->get();
         //  dd($StateManagementModel);
        return view('backend.vehicle_make',compact('VehicleMake')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create(Request $request)
    {
         
          $vehicle=$request->input('name');  
         
          $validatedData = $request->validate([
          
          'name' => 'required|unique:tpl_vechile_make',

         ]);
          try {
                  $VehicleMakeModel = new VehicleMakeModel();
                  $VehicleMakeModel->name =$vehicle;
              
                  $VehicleMakeModel->save();
                  $request->session()->flash('success','Added Successfully!!'); 

            }catch(\Illuminate\Database\QueryException $e){
                        
                $request->session()->flash('success','Something is wrong');      
            }

            return back();
 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeupdate(Request $request , $id)
    {
           $vehicle=$request->input('name');
        
          $validatedData = $request->validate([
          'name' => 'required|unique:tpl_vechile_make,name,'.$id,
          

         ]);
          VehicleMakeModel::where('id', $id)->update(['name' =>$vehicle]);
          $request->session()->flash('success','Data Updated Successfully!!'); 
          return back();
  


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function show(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function edit(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContributionModel $contributionModel)
    {
        //
    }
	 public function csvfilevehicle()
    {


        $vihiclemake= VehicleMakeModel::where('IsActive',1)->orderBy('id','DSC')->get();
          

           $CsvData=array('Vehicle Make Name');  
              //$CsvData=array('Item Name,Price,Effective Date');         
             
              foreach($vihiclemake as $vehicle){
                    
                  

                  $CsvData[]=$vehicle->name;
                     
              }

             
               
              $filename=date('Y-m-d')."-Vehiclemakes.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers );
    } 
}
