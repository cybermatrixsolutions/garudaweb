<?php

namespace App\Http\Controllers;

use App\CustomerLubeRequest;
use Illuminate\Http\Request;
use App\RoMaster;
use App\RoPieceListManagement;
use App\TransactionModel;
use Session;
use DB;
use Carbon\Carbon;
use Gate;
use Auth;
use App\CustomerTransactionInvoice;
class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('ro-admin',Auth::user()) || Gate::allows('admin',Auth::user())) {
            return redirect('Dashboard');
        }
        $Customer_Code='';
        if (Auth::user()->customer!=null) {
            $Customer_Code=Auth::user()->customer->Customer_Code;
        }
        $rocode = Session::get('ro');
       
        // $item_id= RoPieceListManagement::All();
        $transaction_list= CustomerTransactionInvoice::join('tbl_ro_master','tbl_customer_transaction_invoice.RO_code','=','tbl_ro_master.RO_code')->join('tbl_customer_master','tbl_customer_transaction_invoice.customer_code', '=' , 'tbl_customer_master.Customer_Code')->select('tbl_customer_transaction_invoice.*','tbl_ro_master.pump_legal_name','tbl_customer_master.Customer_Name');

         if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $transaction_list=$transaction_list->where('tbl_customer_transaction_invoice.RO_code',$rocode)->where('tbl_customer_transaction_invoice.customer_code',$Customer_Code);

              $transaction_list=$transaction_list->get();

        
         return view('backend.transaction_page',compact(['transaction_list']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }
   
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exportCSV(Request $request)
    {
    
        try{  

            $Customer_Code='';

            if (Auth::user()->customer!=null) {
                $Customer_Code=Auth::user()->customer->Customer_Code;
            }

            $rocode = Session::get('ro');

            //$transaction_list=TransactionModel::join('tbl_price_list_master','tbl_customer_transaction.item_code','=', 'tbl_price_list_master.id')->select('tbl_customer_transaction.*','tbl_price_list_master.Item_Code as Item');
           
           //$transaction_list=TransactionModel::join('tbl_price_list_master','tbl_customer_transaction.item_code','=', 'tbl_price_list_master.id')->select('tbl_customer_transaction.*','tbl_price_list_master.Item_Code as Item');
           
             $transaction_list= CustomerTransactionInvoice::join('tbl_ro_master','tbl_customer_transaction_invoice.RO_code','=','tbl_ro_master.RO_code')->join('tbl_customer_master','tbl_customer_transaction_invoice.customer_code', '=' , 'tbl_customer_master.Customer_Code')->select('tbl_customer_transaction_invoice.*','tbl_ro_master.pump_legal_name','tbl_customer_master.Customer_Name');

            if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $transaction_list=$transaction_list->where('tbl_customer_transaction_invoice.RO_code',$rocode)->where('tbl_customer_transaction_invoice.customer_code',$Customer_Code);

            $transaction_list=$transaction_list->get();

            
        if(count($transaction_list)>0){
            
            $CsvData=array('Customer Name,Pump Legal Name,Item Code,Item Name,Item Price,Type,Petrol or Diesel Type,Petrol Or Diesel Qty,Transaction Id,Request ID,Transaction Date,Transaction Amount');          
            foreach($transaction_list as $list){
                 $traData='';
               //dd($list->getTrac->getItemName);

                if($list->getCustomerMaster->Customer_Name != 'Walkin'){
                    $traData=$list->getCustomerMaster->Customer_Name;
                 }else{
                    $traData='Walkin';
                 }

                  $traData.=','.$list->pump_legal_name;
                  $traData.=','.$list->getTrac->getItemName->Item_Code;
                  $traData.=','.$list->getTrac->getItemName->Item_Name;
                  $traData.=','.$list->getTrac->item_price;

                  if($list->petrol_or_lube==1){
                    $traData.=',Petrol';
                  }else{
                    $traData.=',Lube';
                  } 

                  $traData.=','.$list->getTrac->petroldiesel_type;
                  $traData.=','.$list->getTrac->petroldiesel_qty;
                  $traData.=','.$list->getTrac->trans_id;
                  $traData.=','.$list->getTrac->Request_id;
                  $trans_date=Carbon::parse($list->getTrac->trans_date)->format('d/m/Y');
                  $traData.=','.$trans_date;
                  $traData.=','.$list->trans_amount;
                  
                 
                $CsvData[]=$traData;
            }
             
            $filename=date('Y-m-d').".csv";
            $file_path=base_path().'/'.$filename;   
            $file = fopen($file_path,"w+");

            foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
            }

            fclose($file);          

            $headers = ['Content-Type' => 'application/csv'];
            return response()->download($file_path,$filename,$headers );

            }

        }
        catch(\Illuminate\Database\QueryException $e)
        {

        $request->session()->flash('success','No Transaction Available');

        }

        return redirect('transaction_list');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
           // $customer_code = TransactionModel::where('trans_id',$id)->pluck('customer_code')->first();
          
             $transaction_data=TransactionModel::join('tbl_price_list_master','tbl_customer_transaction.item_code','=', 'tbl_price_list_master.id')->where('tbl_customer_transaction.trans_id',$id)->select('tbl_customer_transaction.*','tbl_price_list_master.Item_Code as Item')->get();
           
  
      /*  $transaction_data=TransactionModel::join('tbl_customer_master', 'tbl_customer_transaction.customer_code', '=', 'tbl_customer_master.Customer_Code')
        ->join('tbl_ro_master', 'tbl_customer_transaction.RO_code', '=', 'tbl_ro_master.RO_code')
        ->join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.Item_Code')
        ->select('tbl_customer_transaction.*','tbl_customer_master.Customer_Name','tbl_ro_master.pump_legal_name','tbl_price_list_master.Item_Name as item_name')->where('tbl_customer_transaction.trans_id',$id)->get();*/
        
     
              
                   
        return view('backend.transaction_view_page',compact('transaction_data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerLubeRequest $customerLubeRequest)
    {
        
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerLubeRequest $customerLubeRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        try {
            $personal_mangement = CustomerLubeRequest::find($id);

        $personal_mangement->delete();
        $request->session()->flash('success','Data has been deleted Successfully!!');
        }  catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
         

        return back();
    }
}
