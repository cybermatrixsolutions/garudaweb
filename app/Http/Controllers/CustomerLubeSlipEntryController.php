<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\RoPieceListManagement;
use App\TableStockItemMaster;
use App\RoMaster;
use App\MastorRO;
use App\RoDetails;
use App\RoLOBItemSelectionModel;
use App\RoLobSelectionModel;
use App\StockItemManagement;
use App\LOBMasterModel;
use App\AddPetrolDieselModel;
use App\TableItemPriceListModel;
use App\TransactionModel;
use App\TransTaxModel;
use App\TransactionInvoiceModel;
use App\Shift;
use Auth;
use Gate;
use App\Credit_limit;
use App\RoCustomertManagement;
use App\invoiceCustomer;
use App\customer_slips;
use DB;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\SlipBook;
use App\OutletConfigModel;

class CustomerLubeSlipEntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $datas= RoMaster::where('is_active',1);

        if(Auth::user()->user_type!=1 && $rocode!='')
            $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

        $datas=$datas->get();
        // $item_id = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','OIL')->select('tbl_price_list_master.*')->get();
        // $item_id= RoPieceListManagement::All();
          $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
          $getpric=  $getpric->pluck('item_id')->toArray();
        $item_id = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
            ->where('tbl_stock_item_group.Group_Name','!=', 'FUEL')->where('tbl_price_list_master.RO_Code',$rocode)->whereIn('tbl_price_list_master.id',$getpric)->select('tbl_price_list_master.*');

        if(Auth::user()->user_type!=1 && $rocode!='')
            $item_id=$item_id->where('tbl_price_list_master.RO_Code',$rocode);

        $item_id=$item_id->get();
       
    
         return view('backend.customer_lube_slip_page',compact(['datas','item_id']));



       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($ro_code)
    {

       
       $pump_legal_name_list= RoMaster::where('is_active','1')->get();
        $lob_item_list= RoLOBItemSelectionModel::where('is_active','1')->get();
        $get_lob= RoLobSelectionModel::where('RO_code',$ro_code)->get();

     return view('backend.lob_item_page',compact('pump_legal_name_list','lob_item_list','get_lob','ro_code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        $rocode = $request->input('RO_code');
        $itemprice = $request->input('itemprice');
        $itempriceDefault = $request->input('itempriceDefault');
        $slip_no = $request->input('slip_detail');
        $customer_codes = $request->input('customer_code');
        $item_code = $request->input('item_code');  
        $vehicle_no= $request->input('Vehicle_Reg_No');
        $petroldiesel_qty = $request->input('petroldiesel_qty');
        $request_datess = $request->input('request_date');
        $shift = $request->input('shift');
         if($slip_no==null || count($slip_no)==0 || $vehicle_no==null || count($vehicle_no)==0 || $customer_codes==null || count($customer_codes)==0){
             $request->session()->flash('success','Something wrong ');
            return back();
        }
         if (date('m') <= 3) {
          $year = date('y')-1;
        } else {
              $year = date('y');
        }

        $j=0;
        $Delivery_Slip_Prefix='dev';
        $Delivery_serial=0;
        $nom=2;
        $Delivery_serial=0;
        $Delivery_invoice='';
        $rostate='';
        $custate='';
        $userid=0;
        $j=0;

       foreach ($customer_codes as $customer_code) {

        $check_slip_no= TransactionModel::where('RO_code',$rocode)->where('slip_detail',$slip_no[$j])->first();
        if(!empty($check_slip_no))
        {
            $request->session()->flash('success','Slip No. Alread Exist');
            return back();
        }
          if (Auth::user()->getRocode!=null) {

              $rocode=Auth::user()->getRocode->RO_code;
              $roid=Auth::user()->getRocode->id;
              $userid=Auth::user()->id;
              $rostate=Auth::user()->getRocode->getstate->statecode;

            if(Auth::user()->getRocode->getdetails!=null){
                 $Delivery_Slip_Prefix=Auth::user()->getRocode->getdetails->Delivery_Slip_Prefix;
                 $Delivery_serial=Auth::user()->getRocode->getdetails->Delivery_serial;
                 $nom=Auth::user()->getRocode->getdetails->D_serial_no_lenght;
            }

              $Delivery_serial= $Delivery_serial+1;
              $Delivery_invoice= $Delivery_serial;

         }

          $custo=RoCustomertManagement::where('RO_code',$rocode)->where('Customer_Code',$customer_code)->first();
          $custate=$custo->getstate->statecode;
       
         $l=mb_strlen($Delivery_serial);
         $nom=$nom-$l;
         for ($i=1; $i <=$nom ; $i++) { 
              $Delivery_invoice='0'.$Delivery_invoice;
         }

          $Delivery_invoice=$Delivery_Slip_Prefix.'/'.$Delivery_invoice.'/'.$year;

         $Delivery_invoice=$slip_no[$j];

          RoDetails::where('ro_id',$roid)->update(['Delivery_serial' => $Delivery_serial]); 
          $fuel_id = DB::table('tbl_customer_vehicle_master')->where('Registration_Number',$vehicle_no[$j])->first();
          $fuel_type_id =$item_code[$j];
          //$itemprice[$j] = TableItemPriceListModel::where('item_id',$item_code[$j])->where('is_active',1)->pluck('price')->first();
          $RoPieceListManagement=RoPieceListManagement::where('id',$item_code[$j])->first();
            
          $date1 = str_replace('/', '-', $request_datess[$j]);
          $request_date = date('Y-m-d', strtotime($date1));
            
          $effdate=$request_date;
        
         $tbl_itemprice_tax = DB::table('tbl_itemprice_tax')->where('item_id',$item_code[$j])->where('effective_date','<=',$request_date)->orderBy('effective_date', 'DESC')->first();
        
        if($tbl_itemprice_tax!=null)
            $effdate=$tbl_itemprice_tax->effective_date;

         $taxsdsadas=null;
         if($RoPieceListManagement->gettex()!=null)
            $taxsdsadas=$RoPieceListManagement->gettex()->where('effective_date',$effdate)->get();


         
          $length = 5;
          $randomBytes = openssl_random_pseudo_bytes($length);
          $characters = '0123456789';
          $charactersLength = strlen($characters);
          $result = '';
          for ($i = 0; $i < $length; $i++)
          $result .= $characters[ord($randomBytes[$i]) % $charactersLength];
          $length = 6;
          $randomBytes = openssl_random_pseudo_bytes($length);
          $characters = '0123456789';
          $charactersLength = strlen($characters);
          $trans_id_no = '';
          for ($i = 0; $i < $length; $i++)
          $trans_id_no .= $characters[ord($randomBytes[$i]) % $charactersLength];
          $trans_id="GARTRANS".$trans_id_no;

       // try {
        
         $transaction = new TransactionModel();
         $transaction->RO_code = $rocode;
         $transaction->item_price_act = $itempriceDefault[$j];

         if($itempriceDefault[$j]!="" && $itempriceDefault[$j] != $itemprice[$j]){
            $transaction->item_price = $itemprice[$j];
         }else{
            $transaction->item_price = $itempriceDefault[$j];
         }
         
      
         $transaction->customer_code = $customer_code;
         $transaction->petroldiesel_type = 0;
         $transaction->petrol_or_lube = 2;
         $transaction->petroldiesel_qty = $petroldiesel_qty[$j];            
         $transaction->Vehicle_Reg_No = $vehicle_no[$j];
         $transaction->slip_detail = $slip_no[$j];
         $transaction->item_code = $fuel_type_id;
         $transaction->Trans_By = $userid;
         //$transaction->request_date = $request_date;
         //$transaction->trans_date = $request_date;
         //$transaction->Trans_Mobile = 8548546985;
         $transaction->cust_name = 'credit';
         $transaction->Request_id = $slip_no[$j];
         $transaction->trans_id = $trans_id;
         //$transaction->Request_id = $slip_no[$j];
         $transaction->trans_mode = 'SLIPS';
         $transaction->invoice_number = $slip_no[$j];

          if($shift!=null && isset($shift[$j])){

              $shiftCal=Shift::find($shift[$j]);

               $transaction->shift_id=$shift[$j];


               $transaction->request_date = date('Y-m-d',strtotime($shiftCal->transation_date));
               $transaction->trans_date = $shiftCal->transation_date;
          }
         
        /* if($shift!=null && isset($shift[$j]))
                $transaction->shift_id=$shift[$j];*/

         $transaction->save();
          $id=$transaction->id;

         if($RoPieceListManagement->gettex!=null && $taxsdsadas!=null)

		$taxnew=0;
		    foreach ($taxsdsadas as $geds112) 
			{
				
				if($rostate==$custate){
                    
                    if($geds112->strate_type=='intrastate'){

                        $taxnew=$taxnew+$geds112->Tax_percentage;
                    }

                 }else{

                    if($geds112->strate_type=='interstate'){

                       $taxnew=$taxnew+$geds112->Tax_percentage;
                    }
                 }
				
				
				
				
			}


            $taxsumvalue=0;
				$pricebefore=0;
				$pricebeforewithqty=0;
				$Pval=0;


            foreach ($taxsdsadas as $geds) {


		$pricebefore=$transaction->item_price/(1+$taxnew/100);
		
		$pricebefore=round($pricebefore,2);
		
		$pricebeforewithqty=$pricebefore*$transaction->petroldiesel_qty;

                //$Pval=round($transaction->petroldiesel_qty*$transaction->item_price*$geds->Tax_percentage/100,2);

		$Pval=$pricebeforewithqty*$geds->Tax_percentage/100;
		
		$Pval=round($Pval,2);
		
		



                if($rostate==$custate){
                    
                    if($geds->strate_type=='intrastate'){
						
						$taxsumvalue=$taxsumvalue+$Pval;

                        $this->addtax($geds,$Pval,$id,$pricebefore);
						
                    }

                 }else{

                    if($geds->strate_type=='interstate'){
$taxsumvalue=$taxsumvalue+$Pval;
                        $this->addtax($geds,$Pval,$id,$pricebefore);
						
                    }
                 }
            }
			
			
			$totalinvamount=0;
			$n=0;
			$whole=0;
			$fraction=0;			
			$totalinvamount=$taxsumvalue+$pricebeforewithqty;
			
			
			$n = $totalinvamount;
	$whole = floor($n);
	$fraction = $n - $whole;
	
	if ($fraction<=0.50)
	{
		$rvalue=$fraction;
		$totalinvamount=floor($totalinvamount);
		
		$rvalue=round($rvalue,2);
		
		$transactionupdate = TransactionModel::where('invoice_number',$slip_no[$j])->update(['invoice_amount'=>$totalinvamount,'invoice_roundoff'=>-$rvalue,'taxbefore_price'=>$pricebefore,'taxable_value'=>$pricebeforewithqty]);
      
		
	}
	else
	{
		$rvalue=1- $fraction;
		$totalinvamount=round($totalinvamount);
		$rvalue=round($rvalue,2);
		
	
		
		$transactionupdate = TransactionModel::where('invoice_number',$slip_no[$j])->update(['invoice_amount'=>$totalinvamount,'invoice_roundoff'=>$rvalue,'taxbefore_price'=>$pricebefore,'taxable_value'=>$pricebeforewithqty]);
		
	}
			

         $price =0;
        
        $length = 6;
        $randomBytes = openssl_random_pseudo_bytes($length);
        $characters = '0123456789';
       $charactersLength = strlen($characters);
        $trans_invoice = '';
          for ($i = 0; $i < $length; $i++)
             $trans_invoice .= $characters[ord($randomBytes[$i]) % $charactersLength];

        $invoice_number = "GARINV".$trans_invoice;
       $trans_amount = $itemprice[$j] * $petroldiesel_qty[$j];
       
      /* $invoice_data = new TransactionInvoiceModel();
       $invoice_data->trans_id=$trans_id;
       $invoice_data->trans_amount=$trans_amount;
       $invoice_data->invoice_number=$invoice_number;
       $invoice_data->Trans_By=11;
       $invoice_data->cust_type='credit';
       $invoice_data->petrol_or_lube=2;
       $invoice_data->tax_amount=0;
       $invoice_data->customer_code=$customer_code;
       $invoice_data->RO_code=$rocode;
       $invoice_data->save();*/

          $fule_type=2;

           $Credit_limit=Credit_limit::where('Customer_Code',$transaction->customer_code)->first();
           $transactionModels =   TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
            ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
            ->where('tbl_customer_transaction.customer_code',$transaction->customer_code)
            ->where('tbl_customer_transaction.status',0)
             ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode')
            ->get();

            $amountTrajection=0;
            $amountTrajection=$price;

            foreach ($transactionModels as $transactionModel) {
                //dd($transactionModel->item_price);
                if($transactionModel->petrol_or_lube==1){
                  $amountTrajection=$amountTrajection+($transactionModel->item_price*$transactionModel->petroldiesel_qty);
                }else{
                  $amountTrajection=$amountTrajection+$transactionModel->item_price;
                }
               
            }
         
            $invoiceCustomers=invoiceCustomer::where('settlement_status',0)->where('Customer_name',$transaction->customer_code)->get();
            
            foreach ($invoiceCustomers as $invoiceCustomer) {
                $amountTrajection=$amountTrajection+$invoiceCustomer->amount;
            }

            if($Credit_limit!=null){
               if ($Credit_limit->Limit_Value<=$amountTrajection) {
                   $request->session()->flash('success','Credit Limit Exceed !!');
               }else{
                $request->session()->flash('success','Add Successfully!!');
               }
               
            }else{
               $request->session()->flash('success','Add Successfully!!');
            }

        // } catch(\Illuminate\Database\QueryException $e){
                
        //         $request->session()->flash('success','Something wrong!!');  
        //     }
            
          $j++;
    }
            return back();
  }

 /**
 *  add tax
 */
  public function addtax($geds,$value,$id,$pricebefore){
         $TransTaxModel = new TransTaxModel();
         $TransTaxModel->Tax_Code=$geds->Tax_Code;
         $TransTaxModel->Tax_Type=$geds->Tax_Type;
         $TransTaxModel->Tax_percentage=$geds->Tax_percentage;
         $TransTaxModel->Description=$geds->Description;
         $TransTaxModel->GST_Type=$geds->GST_Type;
         $TransTaxModel->tax_name=$geds->tax_name;
         $TransTaxModel->row_columns=$geds->row_columns;
         $TransTaxModel->position=$geds->position;
         $TransTaxModel->strate_type=$geds->strate_type;
         $TransTaxModel->transaction_id=$id;
         $TransTaxModel->is_active=1;
         $TransTaxModel->Tax_Value=$value;
		 $TransTaxModel->item_taxbefore_price=$pricebefore;
         $TransTaxModel->save();
    }   
        

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $price_list = DB::table('tbl_price_list_master')->where('id', '=', $id)->first();
         $stock_group = DB::table('tbl_stock_item_group')->where('is_active', '>=',1)->get();
         $datas= RoMaster::where('is_active','1')->get();
          $lob = DB::table('tbl_lob_master')->get();
         $unit_measure = DB::table('tbl_unit_of_measure')->where('is_active', '>=',1)->get();
         $item_master_list= TableStockItemMaster::All();
         $taxmaster = DB::table('tbl_tax_master')->get();
        return view('backend.priceEdit', compact('price_list', 'datas','item_master_list','stock_group','unit_measure','taxmaster','lob'));
    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $price_list = DB::table('tbl_price_list_master')->where('id', '=', $id)->first();
          $stock_group = DB::table('tbl_stock_item_group')->where('is_active', '>=',1)->get();
         $datas= RoMaster::where('is_active','1')->get();
         $unit_measure = DB::table('tbl_unit_of_measure')->where('is_active', '>=',1)->get();
         $item_master_list= TableStockItemMaster::All();
        return view('backend.price_view', compact(['price_list', 'datas','item_master_list','stock_group','unit_measure']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        
        $validatedData = $request->validate([
         
        
         'price_ro_code' => 'required',
         'price_item_list' => 'required',
         
         'volume_liter' => 'required',
         'item_name' => 'required',
    ]);


        try {
           
            $personal_mangement = RoPieceListManagement::find($id);
            $personal_mangement->RO_Code = $request->input('price_ro_code');
            $personal_mangement->Item_id = $request->input('price_item_list');
            // $personal_mangement->Price = $request->input('price');
            
            $personal_mangement->Volume_ltr = $request->input('volume_liter');            
            $personal_mangement->Item_Name = $request->input('item_name');
            $date =$request->input('Active_From_Date');
            $Active_From_Date = date("Y-m-d", strtotime($date));
            
            $personal_mangement->Stock_Group = $request->input('Stock_Group');
            $personal_mangement->Unit_of_Measure = $request->input('Unit_of_Measure');
            $personal_mangement->Active_From_Date= $Active_From_Date;
            $personal_mangement->Tail_Unit = $request->input('Tail_Unit');
            $personal_mangement->Conversion = $request->input('Conversion');
            $personal_mangement->Alternate_Unit = $request->input('Alternate_Unit');
            $personal_mangement->LOB = $request->input('LOB');
            $personal_mangement->brand = $request->input('brand'); 
            $personal_mangement->tax = $request->input('tax');
            $personal_mangement->hsncode = $request->input('hsncode');    
            $personal_mangement->save();


            $request->session()->flash('success','Data Updated Successfully!!'); 
       } catch(\Illuminate\Database\QueryException $e){
                
                 $request->session()->flash('success','Something wrong!!');
                
            }
       
       
            return redirect('price_list');
    }
    function active(Request $request,$id, $actt)
    {
        
     $ac = LOBMasterModel::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $price_list = RoPieceListManagement::find($id);

        $price_list->delete();
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
        return back();
    }
      //made by vijay
     public function checkslipdetailunique(Request $request)
    {  

        $slip_detail= $request->input('slip_detail');
        $customer= $request->input('customer');
        $RO_code=$request->input('RO_code');
        $SlipBook1=SlipBook::where('RO_code',$RO_code)->where('Customer_Code',$customer)->first();

        $SlipBook=SlipBook::where('RO_code',$RO_code)->where('Customer_Code',$customer)->where('Slip_start','<=',$slip_detail)->where('Slip_end','>=',$slip_detail)->get();
        
        $RoCon=OutletConfigModel::where('rocode',$RO_code)->where('field_name','slip_book')->first();
        
        if($RoCon!=null && $RoCon->value=='on')
          if($SlipBook->count()<=0){

            return "This no. not exist in allocated slip book";
          }

        $get_data =  TransactionModel::where('RO_code',$RO_code)->where('slip_detail',$slip_detail)->get();

        if($get_data->count()>0)
        {
           return "Slip no  Already Exist";
        }
        else
        {
           return "OK";
        }
    }
}
