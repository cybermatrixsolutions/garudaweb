<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\RoMaster;
use DB;
use Illuminate\Support\Facades\Input;
use File;
use Storage;
use Carbon\Carbon;
use App\RoCustomertManagement;
use App\CustomerDriverManager;
use App\Http\Controllers\Controller;
use Session;
use Gate;
use Auth;

class CustomerDriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
         if (Gate::allows('ro-admin',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $Customer_Code='';
       if (Auth::user()->customer!=null) {
          $Customer_Code=Session::get('custm');
       }
       $rocode = Session::get('ro');

        $datas= RoMaster::where('is_active','1');

       if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();


        $customer_driver_list = CustomerDriverManager::join('tbl_ro_master', 'tbl_customer_vehicle_driver.Ro_code', '=', 'tbl_ro_master.RO_code')
            ->join('tbl_customer_master', 'tbl_customer_vehicle_driver.customer_code', '=', 'tbl_customer_master.Customer_Code')
            ->select('tbl_customer_vehicle_driver.*','tbl_ro_master.pump_legal_name as pump_name','tbl_customer_master.Customer_Name as customer_name');
          /**
           *  Abhishek Chaudahry
          */
            if(Auth::user()->user_type!=1 && $Customer_Code!='')
            $customer_driver_list=$customer_driver_list->where('tbl_customer_vehicle_driver.Ro_code',$rocode)->where('tbl_customer_vehicle_driver.customer_code',$Customer_Code)->orderby('id','desc');

              $customer_driver_list=$customer_driver_list->get();

        $customer_code = RoCustomertManagement::where('is_active',1);

        if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $customer_code=$customer_code->where('tbl_customer_master.RO_code',$rocode);

              $customer_code=$customer_code->get();


        return view('backend/customer.right_customer_driver',compact(['datas','customer_driver_list','customer_code']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $validatedData = $request->validate([
        
         'Registration_Number' => 'required|unique:tbl_customer_vehicle_driver',
         
         ]);*/
		 
		$valid_upto = $request->input('valid_upto');
		if($valid_upto && $valid_upto!=""){
			$date1 = str_replace('/', '-', $valid_upto);
			$valid_upto = date('Y-m-d', strtotime($date1));
		}	
        
          if(Auth::User()->customer!=null){
               $customer=Session::get('custm');
            }else{
              $customer=$request->input('customer_code');
            }
            $url="images/driver.png";
          if($request->hasFile('licencephoto'))
         {
            $file = Input::file('licencephoto');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $filename = $file->getClientOriginalName();

            $name = $timestamp . '-' . $filename;
            $url = 'images/' . $name;

            $file->move('images/', $name);
         }

        try {
            $customer_driver_mangement = new CustomerDriverManager();
            $customer_driver_mangement->Ro_code = $request->input('customer_driver_ro_code');
            $customer_driver_mangement->customer_code =$customer;
            $customer_driver_mangement->Driver_Name = $request->input('driver_name');
            $customer_driver_mangement->Driver_Mobile = $request->input('Driver_Mobile');
            $customer_driver_mangement->Registration_Number = $request->input('Registration_Number');            
            $customer_driver_mangement->email = $request->input('email');
            $customer_driver_mangement->Driver_Licence_No = $request->input('licence_no');
            $customer_driver_mangement->valid_upto = $valid_upto;
            $customer_driver_mangement->photo = $url;
        // $date =$request->input('Operating_date');
        // $Operating_date=date('Y-m-d', strtotime(str_replace('-','/', $date)));
        // $customer_driver_mangement->Operating_date = $Operating_date;
        $customer_driver_mangement->save(); 
        $request->session()->flash('success','Add Successfully!!');
        } catch(\Illuminate\Database\QueryException $e){
              
             
                $request->session()->flash('success','Something wrong!!');  
            }
        

        
        return redirect('customer_driver');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       if (Gate::allows('ro-admin',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }

        $Customer_Code='';
       if (Auth::user()->customer!=null) {
          $Customer_Code=Auth::user()->customer->Customer_Code;
       }
       $rocode = Session::get('ro');

        $customber_driver_data = DB::table('tbl_customer_vehicle_driver')->where('id', '=', $id)->first();
         $datas = RoMaster::where('is_active',1);

         if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();

         $customer_code = RoCustomertManagement::where('is_active',1);

         if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $customer_code=$customer_code->where('tbl_customer_master.RO_code',$rocode);

              $customer_code=$customer_code->get();
        
        return view('backend/customer.right_customer_driver_view', compact(['customber_driver_data', 'datas','customer_code']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 

        if (Gate::allows('ro-admin',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $Customer_Code='';
       if (Auth::user()->customer!=null) {
          $Customer_Code=Session::get('custm');
       }

        $customber_driver_data = DB::table('tbl_customer_vehicle_driver')->where('id', '=', $id)->first();
         $datas = RoMaster::where('is_active',1);

         if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $datas=$datas->where('tbl_ro_master.RO_code',Auth::user()->customer->RO_code);

              $datas=$datas->get();

         $customer_code = RoCustomertManagement::where('is_active',1);

         if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $customer_code=$customer_code->where('tbl_customer_master.RO_code',Auth::user()->customer->RO_code);

              $customer_code=$customer_code->get();
        
        return view('backend/customer.right_customer_driver_edit', compact(['customber_driver_data', 'datas','customer_code']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     
		$valid_upto = $request->input('valid_upto');
		if($valid_upto && $valid_upto!=""){
			$date1 = str_replace('/', '-', $valid_upto);
			$valid_upto = date('Y-m-d', strtotime($date1));
		}

		if(Auth::User()->customer!=null){
			$customer=Auth::User()->customer->Customer_Code;
		}
		else{
			$customer=$request->input('customer_code');
		}
		$url="images/driver.png";
		if($request->hasFile('licencephoto'))
		{
			$file = Input::file('licencephoto');
			$timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
			$filename = $file->getClientOriginalName();
			$name = $timestamp . '-' . $filename;
			$url = 'images/' . $name;
			$file->move('images/', $name);
		}
		else
		{
			$url= $request->input('lince_logo');
		}

        try {
			$customer_driver_mangement = CustomerDriverManager::find($id);
			$customer_driver_mangement->Ro_code = $request->input('customer_driver_ro_code');
			$customer_driver_mangement->customer_code =$customer;
			$customer_driver_mangement->Driver_Name = $request->input('driver_name');
			$customer_driver_mangement->Driver_Mobile = $request->input('Driver_Mobile');
			$customer_driver_mangement->valid_upto = $valid_upto;
			$customer_driver_mangement->email = $request->input('email');
			$customer_driver_mangement->Registration_Number = $request->input('Registration_Number');
			$customer_driver_mangement->Driver_Licence_No = $request->input('licence_no');
			$customer_driver_mangement->photo = $url;

			$customer_driver_mangement->save();
			$request->session()->flash('success','Data Updated Successfully!!');
		} 
		catch(\Illuminate\Database\QueryException $e){
			$request->session()->flash('success','Something wrong!!');  
		}

		return redirect('customer_driver');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
           $personal_mangement = CustomerDriverManager::find($id);

        $personal_mangement->delete(); 
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
        

        return back();
    }
    function active(Request $request,$id, $actt)
    {
        
        $ac = CustomerDriverManager::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }
}
