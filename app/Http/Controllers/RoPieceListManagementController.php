<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoPieceListManagement;
use App\TableStockItemMaster;
use App\RoMaster;
use App\Pitem_tax;
use App\LOBMasterModel;
use App\TransactionModel;
use App\TransTaxModel;
use App\TableItemPriceListModel;
use App\OutletConfigModel;
use Carbon\Carbon;
use DB;
use Gate;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use File;


class RoPieceListManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
       
       if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $datas= RoMaster::where('is_active','1');

         if(Auth::user()->user_type!=1 && $rocode!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();

       $item_master_list= TableStockItemMaster::where('is_active','1')->get();


       $taxmaster = DB::table('tbl_tax_master')->get();

       $lob = DB::table('tbl_lob_selection')->where('RO_code',$rocode)->orderBy('lob_name', 'asc')->get();

       $principle_company = DB::table('tbl_company_principal_master')->where('is_active', '>=',1)->get();

      $stock_group = DB::table('tbl_stock_item_group')->where('is_active', '>=',1)->where('parent',0)->orderBy('Group_Name', 'asc')->get();

      $unit_measure = DB::table('tbl_unit_of_measure')->where('is_active', '>=',1)->orderBy('Unit_Name', 'asc')->get();
     
      $price_list =RoPieceListManagement::join('tbl_ro_master', 'tbl_price_list_master.RO_Code', '=', 'tbl_ro_master.RO_code')
         ->select('tbl_price_list_master.*','tbl_ro_master.pump_legal_name as pump_name')->orderBy('id','desc');

         if(Auth::user()->user_type!=1 && $rocode!='')
                $price_list=$price_list->where('tbl_price_list_master.RO_Code',$rocode);

              $price_list=$price_list->get();


             

     return view('backend.priceitempage',compact('datas','price_list','item_master_list','unit_measure','stock_group','principle_company','taxmaster','lob'));
    }
    
    public function itemshow() {

        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
            }
           $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }

        
        $itemprice = RoMaster::where('is_active','1');

        if(Auth::user()->user_type!=1 && $rocode!='')
                $itemprice=$itemprice->where('tbl_ro_master.RO_code',$rocode);

              $itemprice=$itemprice->get();

         return  view('backend.item_tax_alloc',compact(['itemprice']));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getstockgr(Request $request)
    {
            $rcode = $request->input('rocode');
            $rodode = DB::table('tbl_ro_master')->where('id',$rcode)->first();
            //dd($rodode);
            $stockgroupa = DB::table('tbl_price_list_master')->where('RO_Code', $rodode->RO_code)->get();
             //dd($stockgroupa);
             $Stock_Group=[];
           foreach ($stockgroupa as $stockgroup) {
               $Stock_Group[$stockgroup->Stock_Group]=$stockgroup->Stock_Group;
           }
             $tax = DB::table('tbl_stock_item_group');
             if($stockgroupa!=null)
              $tax=$tax->whereIn('id',$Stock_Group);

             //$tax=$tax->where('Group_Name','!=','FUEL')->get();
              
             $rodode=$tax->pluck('Group_Name','id')->toArray();
            // dd($rodode);
            return response()->json($rodode);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function store(Request $request)
    {
  


     try {

         $validatedData = $request->validate([
         'price_ro_code' => 'required',
         'price_item_list' => 'required',
         'item_name' => 'required',
         ]);
          $getlob = $request->input('Sub_Stock_Group');
          $LOBMasterModel= LOBMasterModel::where('stock_idg',$getlob)->first();

          $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }

       

            $RoPieceListManagement=RoPieceListManagement::where('RO_Code',$request->input('price_ro_code'))->where('Item_Code',$request->input('price_item_list'))->get();

           if($RoPieceListManagement->count()>0){

                $request->session()->flash('success','Item Code Already Exists !!');

            }else{
                 $time='00:00:00';

                 $OutletConfigModel=OutletConfigModel::where('rocode',$rocode)->where('field_name','FUEL_PRICE_Date_time')->first();
        
                 if($OutletConfigModel!=null)
                    $time=date('h:i:s',strtotime($OutletConfigModel->value));

                 
                $item_price=$request->input('item_price');

                $date3 = $request->input('effected_date');
               
               if($date3!=null){
                     $date2 = str_replace('/', '-', $date3);
                    $price_effected_date = date('Y-m-d', strtotime($date2));
                }else{
                    $price_effected_date = date('Y-m-d');
                }
                $personal_mangement = new RoPieceListManagement();
                $personal_mangement->RO_Code = $request->input('price_ro_code');
                $personal_mangement->Item_Code = $request->input('price_item_list');
                // $personal_mangement->Price = $request->input('price');
                if($request->input('volume_liter') == null)
                    $personal_mangement->Volume_ltr = 0; 
                else
                $personal_mangement->Volume_ltr = $request->input('volume_liter');

                $personal_mangement->Item_Name = $request->input('item_name');

                $date = $request->input('Active_From_Date');

                

                    $date1 = str_replace('/', '-', $date);
                    $Active_From_Date = date('Y-m-d', strtotime($date1));
               
               

                
                $personal_mangement->Stock_Group = $request->input('Stock_Group');
                $personal_mangement->Sub_Stock_Group = $getlob;
                $personal_mangement->Unit_of_Measure = $request->input('Unit_of_Measure');
                $personal_mangement->Active_From_Date= $Active_From_Date;
                $personal_mangement->Tail_Unit = $request->input('Tail_Unit');
                $personal_mangement->Conversion = $request->input('Conversion');
                $personal_mangement->Alternate_Unit = $request->input('Alternate_Unit');
                $personal_mangement->LOB = $LOBMasterModel->lob_name;
                $personal_mangement->brand = $request->input('brand');
               // $personal_mangement->tax = $request->input('tax');
                $personal_mangement->hsncode = $request->input('hsncode');
                $personal_mangement->gst_vat = $request->input('gst_vat');
                
                $personal_mangement->save();

                $daat=$request->input('tax');
                 if($item_price!=null && $item_price>0){
                    
                  $update_price = new TableItemPriceListModel();
                    $update_price->item_id = $personal_mangement->id;
                    $update_price->price = $item_price;
                    $update_price->effective_date = $price_effected_date.' '.$time;
                    $update_price->is_active=1;
                    $update_price->save();
                 }
               
                foreach ($daat as $key => $value) {
                $itemprice = new Pitem_tax();
                $itemprice->tax_id = $value;
                $itemprice->item_id = $personal_mangement->id;
                $itemprice->save();
                }


               
                 $request->session()->flash('success','Added Successfully!!');
              
               }  
             
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');
                
} 
            return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

         $price_list = DB::table('tbl_price_list_master')->where('id', '=', $id)->first();

          $rod= Pitem_tax::join('tbl_tax_master', 'tbl_tax_master.id', '=', 'tbl_itemprice_tax.tax_id')
         ->select('tbl_itemprice_tax.*','tbl_tax_master.Tax_Code')->where('item_id', '=', $id)->get();

         $stock_group = DB::table('tbl_stock_item_group')->where('parent',0)->where('is_active', '>=',1)->orderBy('Group_Name', 'asc')->get();

         $datas= RoMaster::where('is_active','1');
            
            if(Auth::user()->user_type!=1 && $rocode!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();

          $lob = DB::table('tbl_lob_master')->orderBy('lob_name', 'asc')->get();

         $unit_measure = DB::table('tbl_unit_of_measure')->where('is_active', '>=',1)->orderBy('Unit_Name', 'asc')->get();

         $item_master_list= TableStockItemMaster::All();
         $taxmaster = DB::table('tbl_tax_master')->get();
        return view('backend.priceEdit', compact('price_list', 'datas','item_master_list','stock_group','unit_measure','taxmaster','lob','rod'));
    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
            if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                    return redirect('Dashboard');
            }
            $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }
          $price_list = DB::table('tbl_price_list_master')->where('id', '=', $id)->first();
          $stock_group = DB::table('tbl_stock_item_group')->where('is_active', '>=',1)->get();
          $rod= Pitem_tax::join('tbl_tax_master', 'tbl_tax_master.id', '=', 'tbl_itemprice_tax.tax_id')
         ->select('tbl_itemprice_tax.*','tbl_tax_master.Tax_Code')->where('item_id', '=', $id)->get();
         
          $datas= RoMaster::where('is_active','1')->get();

         $unit_measure = DB::table('tbl_unit_of_measure')->where('is_active', '>=',1)->get();
         $item_master_list= TableStockItemMaster::All();
         
        return view('backend.price_view', compact(['price_list', 'datas','item_master_list','stock_group','unit_measure','rod']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        
        $validatedData = $request->validate([
         
        
         'price_ro_code' => 'required',
         'price_item_list' => 'required',
         
        
         'item_name' => 'required',
    ]);

            $getlob = $request->input('Sub_Stock_Group');
           $LOBMasterModel= LOBMasterModel::where('stock_idg',$getlob)->first();
       try {
            
        $RoPieceListManagement=RoPieceListManagement::where('RO_Code',$request->input('price_ro_code'))->where('Item_Code',$request->input('price_item_list'))->where('id','!=',$id)->get();
            
            if($RoPieceListManagement->count()>0){
              $request->session()->flash('success','Item Code Already Exists !!');

             }else{
                 
            $personal_mangement = RoPieceListManagement::find($id);
            $personal_mangement->RO_Code = $request->input('price_ro_code');
            $personal_mangement->Item_Code = $request->input('price_item_list');
            // $personal_mangement->Price = $request->input('price');
            
            if($request->input('volume_liter') == null)
                $personal_mangement->Volume_ltr = 0; 
            else
            $personal_mangement->Volume_ltr = $request->input('volume_liter');
                        
            $personal_mangement->Item_Name = $request->input('item_name');

             $date = $request->input('Active_From_Date');
            $date1 = str_replace('/', '-', $date);
            $Active_From_Date = date('Y-m-d', strtotime($date1));
           
            
            $personal_mangement->Stock_Group = $request->input('Stock_Group');
            $personal_mangement->Sub_Stock_Group = $request->input('Sub_Stock_Group');
            $personal_mangement->Unit_of_Measure = $request->input('Unit_of_Measure');
            $personal_mangement->Active_From_Date= $Active_From_Date;
            $personal_mangement->Tail_Unit = $request->input('Tail_Unit');
            $personal_mangement->Conversion = $request->input('Conversion');
            $personal_mangement->Alternate_Unit = $request->input('Alternate_Unit');
            $personal_mangement->LOB = $LOBMasterModel->lob_name;
            $personal_mangement->brand = $request->input('brand'); 
           // $personal_mangement->tax = $request->input('tax');
            $personal_mangement->hsncode = $request->input('hsncode');
            $personal_mangement->gst_vat = $request->input('gst_vat');
            $personal_mangement->tally_update_status=0;
                
            $personal_mangement->save();
            //$user = Pitem_tax::where('item_id',$id)->delete(); 
            
            //$daat=$request->input('tax');
            
           /* if($daat!=null)
            foreach ($daat as $key => $value) {
            $itemprice = new Pitem_tax();
            $itemprice->tax_id = $value;
            $itemprice->item_id = $personal_mangement->id;
            $itemprice->save();

           }*/
            $request->session()->flash('success','Data Updated Successfully!!');

        
        } 

      } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');
                
           }
       
       
            return redirect('price_list');
    }
  public function itemtaxsavetable(Request $request)
    {
         $rocode='';
         $roid='';
         $rostate='';
         if (Auth::user()->getRocode!=null) {

             $rocode=Auth::user()->getRocode->RO_code;
             $roid=Auth::user()->getRocode->id;
              $rostate=Auth::user()->getRocode->getstate->statecode;
         }

        $itwms=$request->input('item');
        $typetax=$request->input('typetax');
       
        if($typetax=='GST'){
            $typetax=2;
        }else{
            $typetax=1;
        }


        $hsn_code=$request->input('hsn_code');
        if($itwms==Null && count($itwms)<=0){
        
            $request->session()->flash('success','Please select Item !!');
            return back();

        }

        $taxes=$request->input('tax');
        $effective_date=$request->input('effective_date');
        $effective_date = new Carbon(str_replace('/', '-',$effective_date));
        $currentdate=date('Y-m-d');

        $flg=false;
        if($currentdate<date('Y-m-d',strtotime($effective_date))){
            $flg=true;
            
        }
        

       $TransactionModels=TransactionModel::where('RO_code',$rocode)->where('status',1);

       if($typetax==2)
            $TransactionModels=$TransactionModels->where('trans_mode','SLIPS');

        $TransactionModels=$TransactionModels->whereDate('trans_date','>=',date('Y-m-d H:i:s', strtotime($effective_date)))->get();
       
      
       if($TransactionModels->count()>0){

            $request->session()->flash('success','Bills already done so con not apply taxes !!');
            return back();
       }

       if(count($taxes)>0){

        foreach ($itwms as $item) {
           
                //$user1 = Pitem_tax::where('item_id',$item)->delete(); 
                // $itemprice->item_id = $personal_mangement->id;
                //Pitem_tax::where('item_id',$item)->delete();
                if($flg==false)
                 $texta=Pitem_tax::where('item_id',$item)->where('is_active','<>',2)->update(['is_active'=>0]);

          
                foreach ($taxes as $tax) {
                        
                        $itemprice = new Pitem_tax();
                        $itemprice->tax_id = $tax;
                        $itemprice->item_id = $item;

                        if($flg)
                            $itemprice->is_active =2;

                        $itemprice->effective_date = date('Y-m-d H:i:s', strtotime($effective_date));
                        $itemprice->save();
                    }
                   

                     $RoPieceListManagementss = RoPieceListManagement::find($item);

                    if($hsn_code!=null){
                      
                       $RoPieceListManagementss->hsncode = $request->input('hsn_code');
                       $RoPieceListManagementss->save();
                       }

                        //apply all tex in trajections
                       $this->applyTaxOnTransaction($typetax,$item,$itemprice->effective_date,$RoPieceListManagementss,$rostate);
                       
                }

                $request->session()->flash('success','Added Successfully !!');

            }

            return back();
          
      }
	  
	function truncate_number( $number, $precision = 2) {
    // Zero causes issues, and no need to truncate
    if ( 0 == (int)$number ) {
        return $number;
    }
    // Are we negative?
    $negative = $number / abs($number);
    // Cast the number to a positive to solve rounding
    $number = abs($number);
    // Calculate precision number for dividing / multiplying
    $precision = pow(10, $precision);
    // Run the math, re-applying the negative value to ensure returns correctly negative / positive
    return floor( $number * $precision ) / $precision * $negative;
}  

    //prev tran tax remove and app
    public function applyTaxOnTransaction($typetax,$item,$effective_date,$RoPieceListManagementss,$rostate)
	{
		
		

       $TransactionModels=TransactionModel::where('petrol_or_lube',$typetax)->where('item_code',$item)->where('status',0);

       if($typetax==2)
            $TransactionModels=$TransactionModels->where('trans_mode','SLIPS');

        $TransactionModels=$TransactionModels->where('trans_date','>',$effective_date)->get();
        
        $custate='';
        if($TransactionModels!=null)
            foreach ($TransactionModels as $TransactionModel) {

                    TransTaxModel::where('transaction_id',$TransactionModel->id)->delete();
                    
                        if($TransactionModel->getCustomerMaster!=null)
                            $custate=$TransactionModel->getCustomerMaster->getstate->statecode;
                    $taxsdsadas=null;
                     if($RoPieceListManagementss->gettex()!=null)
                        $taxsdsadas=$RoPieceListManagementss->gettex()->wherePivot('is_active',1)->get();
                   

                    if($RoPieceListManagementss->gettex!=null){
						
						
						$taxnew=0;
		    foreach ($taxsdsadas as $geds112) 
			{
				
				if($rostate==$custate){
                    
                    if($geds112->strate_type=='intrastate'){

                        $taxnew=$taxnew+$geds112->Tax_percentage;
                    }

                 }else{

                    if($geds112->strate_type=='interstate'){

                       $taxnew=$taxnew+$geds112->Tax_percentage;
                    }
                 }
				
				
				
				
			}
						
						
                       
                        foreach ($taxsdsadas as $geds) {
                            $Pval=0;
                            //$Pval=round($TransactionModel->petroldiesel_qty*$TransactionModel->item_price*$geds->Tax_percentage/100,2);
							
							$pricebefore=($TransactionModel->item_price)/(1+$taxnew/100);
							
							$pricebefore=round($pricebefore,2);
							
							$pricewithqty=$TransactionModel->petroldiesel_qty*$pricebefore;
							
							//$pcqty=$TransactionModel->petroldiesel_qty*$pricebefore;
		
		
		
							//$pricebefore=$pcqty/(1+$taxnew/100);
		
							//$pricebefore=$this->truncate_number($pricebefore,2);

							//$Pval=round($transaction->petroldiesel_qty*$transaction->item_price*$geds->Tax_percentage/100,2);

							$Pval=$pricewithqty*$geds->Tax_percentage/100;
		
							$Pval=$this->round($Pval,2);
							
							
							
							

                            if($geds->Tax_Type=='GST'){
                                 
                                 if($rostate==$custate){
                                    
                                    if($geds->strate_type=='intrastate'){

                                        $this->addtax($geds,$Pval,$TransactionModel->id,$pricebefore);
                                    }

                                 }else{

                                    if($geds->strate_type=='interstate'){

                                        $this->addtax($geds,$Pval,$TransactionModel->id,$pricebefore);
                                    }
                                 }

                            }else{

                                  $this->addtax($geds,$Pval,$TransactionModel->id,$pricebefore);
                            }
                        }
                    }
            }

    }
    public function gettax(Request $request){
            $rcode=$request->input('rocode');
            $taxttype=$request->input('taxttype');
            $rodode= DB::table('tbl_ro_master')->where('RO_code',$rcode)->first();

            $state = DB::table('states')->where('id',$rodode->state)->first();
           
            $tax=DB::table('tbl_tax_master')->where('Tax_Type',$taxttype)->where('Tax_Code','Like', $state->statecode.'%');
            // dd($tax);
            $rodode=$tax->pluck('Tax_Code','id')->toArray();
            // dd($rodode);
            return response()->json($rodode);

          }
    function active(Request $request,$id, $actt)
    {
        
     $ac = RoPieceListManagement::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $price_list = RoPieceListManagement::find($id);

        $price_list->delete();
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
     public function getItemBYajex($pump,$tags)
    {    

         $itemnameprice = DB::table('tbl_price_list_master')
          ->join('tbl_ro_master', 'tbl_price_list_master.RO_code', '=', 'tbl_ro_master.RO_code')
          ->where('tbl_price_list_master.Sub_Stock_Group',$tags)
          ->where('tbl_ro_master.id',$pump)
          ->select('tbl_price_list_master.*')->get();

         

            $str='<ul class="columns" data-columns="2">';
            foreach ($itemnameprice as $ChartVolume) {
                $str.='<li>
                <input type="checkbox" name="item[]" value="'.$ChartVolume->id.'">'.$ChartVolume->Item_Name.'</li>
                
                <li>' ;
            }
            $str.='</ul>';
            

             return $str;

    

}
public function getrotax(Request $request){
            $rcode=$request->input('rocode');
            $typetax=$request->input('typetax');
            $rodode= DB::table('tbl_ro_master')->where('id',$rcode)->first();

            $state = DB::table('states')->where('id',$rodode->state)->first();
           
            $tax=DB::table('tbl_tax_master')->where('is_active',1)->where('Tax_Type',$typetax)->where('Tax_Code','Like', $state->statecode.'%');
            // dd($tax);
            $rodode=$tax->pluck('Tax_Code','id')->toArray();
            // dd($rodode);
            return response()->json($rodode);

          }

   public function export(Request $request){

  
      try{
                $rocode='';
                if (Auth::user()->getRocode!=null) {
                   $rocode=Auth::user()->getRocode->RO_code;
                }



              $rodode= DB::table('tbl_ro_master')->where('RO_Code',$rocode)->first();
              $state = DB::table('states')->where('id',$rodode->state)->first();
              $tax=DB::table('tbl_tax_master')->where('Tax_Code','Like', $state->statecode.'%');
              $rtax=$tax->pluck('Tax_Code','id')->toArray();
              if(count($rtax)<= Null){
             
               $request->session()->flash('success','Please Create  Tax !!');

               return back();

              }
              foreach ($rtax as $taxq) {
              $taxss[] = $taxq;
              $taxro = implode(',', $taxss);
             }
            $table = RoPieceListManagement::all();
            $filename = "stock-item-master.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('RO_Code', 'Item_Code','Item_Name','Stock_Group','Sub_Stock_Group','Unit_of_Measure','Active_From_Date','Tail_Unit','Conversion','Alternate_Unit','brand',
            'LOB','tax','hsncode','gst_vat','Volume_ltr','price','Effected Date'));
            fputcsv($handle, array($rodode->RO_code, 'RLIANCEPETROLEUM
        ','RLIANCE','FUEL','Petrol','LTR','29/11/2017','23','23','12','Suziki',
            'Primary',$taxro,'12','gst or vat','10','52.50','29/11/2017'));

             // foreach($table as $row) {
             //     fputcsv($handle, array($row['RO_Code'], $row['Item_Code'],$row['Stock_Group'],$row['Unit_of_Measure'],$row['Active_From_Date'],$row['Tail_Unit'],$row['Conversion'],$row['Alternate_Unit'],$row['brand'],$row['LOB'],$row['tax'],$row['hsncode'],$row['Volume_ltr']));
             // }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );


        return Response()->download($filename, 'ro_item.csv', $headers);
    }
    catch(\Illuminate\Database\QueryException $e){
                    
                    $request->session()->flash('success','Please  Master For Add Tax !!');
                    
    } 
                return redirect('price_list');
   }

public function saveexport(Request $request){


    if (Input::hasFile('file'))
     {
       try{

            $rocode='';
                if (Auth::user()->getRocode!=null) {
                   $rocode=Auth::user()->getRocode->RO_code;
                }


            $file = Input::file('file');
            $name = time() . '-' . $file->getClientOriginalName();
            $path = storage_path('documents');
            $file->move($path, $name);
            $filename=base_path('/storage/documents/'.$name);
            //$file = fopen($filename, "r");
            $file=fopen($filename, 'r');
            while(! feof($file))
            {
            $row= fgetcsv($file);

            if($row){
                $datas[]=$row;
            }

            }
             $time='00:00:00';

               $OutletConfigModel=OutletConfigModel::where('rocode',$rocode)->where('field_name','FUEL_PRICE_Date_time')->first();
             
             if($OutletConfigModel!=null)
                $time=date('h:i:s',strtotime($OutletConfigModel->value));
           
            unset($datas[0]);
            $cnt=count($datas);

            foreach ($datas as $data) {
                $taxcode =explode(',', trim(str_replace('\r','',str_replace('\n','',$data[12]))));
                $item = trim(str_replace('\r','',str_replace('\n','',$data[1]))); 
               
                
                if($cnt>0 && trim(str_replace('\r','',str_replace('\n','',$data[0])))!='' && $data[1]!=''){

                    $rocodecheck = DB::table('tbl_ro_master')->where('RO_code', '=', trim(str_replace('\r','',str_replace('\n','',$data[0]))))->first();
                    if($rocodecheck == null)
                    {
                       $request->session()->flash('success','Your RO Code Not  Exit,Please Contact Admin'); 
                       return back();
                    }
                    
                    $itemcheckexit = DB::table('tbl_price_list_master')->where('Item_Code',$item)->get();

                    if($itemcheckexit->count()>0 || $item=='')
                    {
                       $request->session()->flash('success','Your Item Code Exit or Please Enter Item Code'); 
                       return back();
                    }
                   
                    $groupstock = DB::table('tbl_stock_item_group')->where('Group_Name', '=', trim(str_replace('\r','',str_replace('\n','',$data[3]))))->first();
                    if($groupstock == null)
                    {
                       $request->session()->flash('success','Group Name Not Exit in Database'); 
                       return back();
                    }


                    $subgroupstock = DB::table('tbl_stock_item_group')->where('Group_Name', trim(str_replace('\r','',str_replace('\n','',$data[4]))))->first();
                    if($subgroupstock == null)
                    {
                        $request->session()->flash('success','Sub Group Name Not Exit in Database'); 
                        return back();
                    }

                    if($groupstock!=null && $subgroupstock!=null ){
                        if($groupstock->id!=$subgroupstock->parent){
                          $request->session()->flash('success','Parent Group is not match of Sub Group ');
                          return back();
                        }
                      }

                    $unitofmasurcheck = DB::table('tbl_unit_of_measure')->where('Unit_Symbol',trim(str_replace('\r','',str_replace('\n','',$data[5]))))->first();
                   

                    if($unitofmasurcheck == null)
                    {
                       $request->session()->flash('success','Unit of Measure Not Exit in Database'); 
                       return back();
                    }
                   
                    $lobmastercheck = DB::table('tbl_lob_master')->where('lob_name',trim(str_replace('\r','',str_replace('\n','',$data[11]))))->first();
                    if($lobmastercheck == null)
                    {
                       $request->session()->flash('success','LOB  Not Exits in Database'); 
                       return back();
                    }

                    $item_price=floatval(trim(str_replace('\r','',str_replace('\n','',$data[16]))));
                   
                   

                      $Roitem= new RoPieceListManagement();
                      $Roitem->RO_Code= trim(str_replace('\r','',str_replace('\n','',$data[0])));
                      $Roitem->Item_Code= $item;
                      $Roitem->Item_Name= trim(str_replace('\r','',str_replace('\n','',$data[2])));
                      $Roitem->Stock_Group= $groupstock->id;
                      $Roitem->Sub_Stock_Group=$subgroupstock->id;
                      $Roitem->Unit_of_Measure= $unitofmasurcheck->id;
                      
                      $date1 = str_replace('/', '-', trim(str_replace('\r','',str_replace('\n','',$data[6]))));
                      $Active_From_Date = date('Y-m-d', strtotime($date1));
                      
                      $Roitem->Active_From_Date=  $Active_From_Date;
                      $Roitem->Tail_Unit= trim(str_replace('\r','',str_replace('\n','',$data[7])));
                      $Roitem->Conversion= trim(str_replace('\r','',str_replace('\n','',$data[8])));
                      $Roitem->Alternate_Unit= trim(str_replace('\r','',str_replace('\n','',$data[9])));
                      $Roitem->brand= trim(str_replace('\r','',str_replace('\n','',$data[10])));
                    
                      $Roitem->LOB= $lobmastercheck->lob_name;
                      $Roitem->hsncode= trim(str_replace('\r','',str_replace('\n','',$data[13])));
                      $Roitem->gst_vat= trim(str_replace('\r','',str_replace('\n','',$data[14])));
                      $Roitem->Volume_ltr= trim(str_replace('\r','',str_replace('\n','',$data[15])));
                      $Roitem->save();
                      
                       
                       
                       $date1 = str_replace('/', '-', trim(str_replace('\r','',str_replace('\n','',$data[17]))));

                        if($date1!=null && trim($date1)!='')
                            $price_effected_date = date('Y-m-d', strtotime($date1));
                        else
                            $price_effected_date = date('Y-m-d');

                       if($item_price != null && $item_price>0)
                        {
                            $update_price = new TableItemPriceListModel();
                            $update_price->item_id = $Roitem->id;
                            $update_price->price = $item_price;
                            $update_price->effective_date = $price_effected_date.' '.$time;
                            $update_price->is_active=1;
                            $update_price->save();
                         }

                      foreach ($taxcode as $key => $value) {

                          $dat= DB::table('tbl_tax_master')->where('Tax_Code',$value)->first();
                          if($dat!==null){

                              $pritat=new Pitem_tax();
                              $pritat->tax_id = $dat->id;
                              $pritat->item_id = $Roitem->id;
                              $pritat->save();
                           }else{
                             $request->session()->flash('success','Tax Not Exits in Database'); 
                               return back();
                            }

                       }
                   }
                         
                   }

                    $request->session()->flash('success','Data Import Csv  Successfully!!');

                }catch(\Illuminate\Database\QueryException $e){
                    
                    $request->session()->flash('success','Something is wrong');
                    
                }  
        }

        return back();
   }
    public function exportroall(Request $request){
     
        $table = RoPieceListManagement::all();
        $filename = "stock-item-master.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('RO_Code', 'Item_Code','Item_Name','Stock_Group','Unit_of_Measure','Active_From_Date','Tail_Unit','Conversion','Alternate_Unit','brand',
        'LOB','tax','hsncode','Volume_ltr'));
        fputcsv($handle, array('RO_CODE', 'RLIANCEPETROLEUM
    ','RLIANCE','OIL','Liter','11/29/2017','23','23','12','Suziki',
        'Primary','taxh,taksk2','12','10'));
        /* foreach($table as $row) {
             fputcsv($handle, array($row['RO_Code'], $row['Item_Code'],$row['Stock_Group'],$row['Unit_of_Measure'],$row['Active_From_Date'],$row['Tail_Unit'],$row['Conversion'],$row['Alternate_Unit'],$row['brand'],$row['LOB'],$row['tax'],$row['hsncode'],$row['Volume_ltr']));
         }*/

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );


        return Response()->download($filename, 'ro_item.csv', $headers);

            return redirect('price_list');
   }

   /// sub gro
   public function getsubgroup(Request $request){
 
       $stockgroup=$request->input('subgroup');  
       $substock = DB::table('tbl_stock_item_group')->where('parent',$stockgroup)->get();
       $substock=$substock->pluck('Group_Name','id')->toArray();
            
            return response()->json($substock);
  }
    public function getstocksubprice(Request $request){
 
       $stockgroup=$request->input('stockgroup');  
       $substock = DB::table('tbl_stock_item_group')->where('parent',$stockgroup)->orderBy('Group_Name','asc')->get();
       $substock=$substock->pluck('Group_Name','id')->toArray();
            
            return response()->json($substock);
  }

   public function getstockupdategroup(Request $request){
 
       $stockgroup=$request->input('stockgroup');  
       $substock = DB::table('tbl_stock_item_group')->where('parent',$stockgroup)->get();
       $substock=$substock->pluck('Group_Name','id')->toArray();
            
            return response()->json($substock);
  }


public function addtax($geds,$value,$id,$pricebefore){
         $TransTaxModel = new TransTaxModel();
         $TransTaxModel->Tax_Code=$geds->Tax_Code;
         $TransTaxModel->Tax_Type=$geds->Tax_Type;
         $TransTaxModel->Tax_percentage=$geds->Tax_percentage;
         $TransTaxModel->Description=$geds->Description;
         $TransTaxModel->GST_Type=$geds->GST_Type;
         $TransTaxModel->tax_name=$geds->tax_name;
         $TransTaxModel->row_columns=$geds->row_columns;
         $TransTaxModel->position=$geds->position;
         $TransTaxModel->strate_type=$geds->strate_type;
         $TransTaxModel->transaction_id=$id;
         $TransTaxModel->is_active=1;
         $TransTaxModel->Tax_Value=$value;
		 $TransTaxModel->item_taxbefore_price=$pricebefore;
         $TransTaxModel->save();
    }
}