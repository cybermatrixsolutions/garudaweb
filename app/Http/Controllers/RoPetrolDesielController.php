<?php

namespace App\Http\Controllers;

use App\RoPetrolDesiel;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\MastorRO;

class RoPetrolDesielController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $data = $RoPetrolDesiel=RoPetrolDesiel::all(); 
         $data1 = $MastorRO=MastorRO::where('is_active','1')->get();
        return view('backend.petroldiesel',compact('data','data1'));

    }

       public function checkPedestalNumberUnique(Request $request)
       {

           $rocode='';
             if (Auth::user()->getRocode!=null) {
                $rocode=Auth::user()->getRocode->RO_code;
             }


            $id=0;
            $table=$request->input('table');
            $colum=$request->input('colum');
            $unik=$request->input('unik');
            if($request->input('id') && $request->input('id')!='')
             $id=$request->input('id');
        
            $Customer= DB::table($table)->where($colum,$unik)->where('RO_code',$rocode);
            if($request->input('colum2')!=null && trim($request->input('colum2'))!=''){
                
                $colum2=$request->input('colum2');
                $colum12val=$request->input('colum12val');
                $Customer=$Customer->where($colum2,$colum12val);
            }


            $Customer=$Customer->get();


            $data= $Customer->pluck('id','id')->toArray();
            if($Customer->count()>0){
                  
                  if(in_array($id,$data)){

                     
                      return 'false';
                   }else{

                     return 'true';

                   }
            }else{
                return 'false';   
            }
  
       }

       public function checkPedestalNumberUniqueCus(Request $request)
       {
            $id=0;
            $table=$request->input('table');
            $colum=$request->input('colum');
            $customer=$request->input('customer');
            $customer_col=$request->input('customercol');
            $unik=$request->input('unik');
            if($request->input('id') && $request->input('id')!='')
             $id=$request->input('id');
        
            $Customer= DB::table($table)->where($customer_col,$customer)->where($colum,$unik)->get();
            $data= $Customer->pluck('id','id')->toArray();
            if($Customer->count()>0){
                  
                  if(in_array($id,$data)){

                     
                      return 'false';
                   }else{

                     return 'true';

                   }
            }else{
                return 'false';   
            }
  
       }
       public function checkCustomerEmailWithRo(Request $request)
       {

         $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
              
           }
         $id=0;
            $table=$request->input('table');
            $colum=$request->input('colum');
            $unik=$request->input('unik');
            if($request->input('id') && $request->input('id')!='')
             $id=$request->input('id');
        
            $Customer= DB::table($table)->where($colum,$unik)->where('RO_code',$rocode)->get();
            $data= $Customer->pluck('id','id')->toArray();
            if($Customer->count()>0){
                  
                  if(in_array($id,$data)){

                     
                      return 'false';
                   }else{

                     return 'true';

                   }
            }else{
                return 'false';   
            }

       }

       
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
         $validatedData = $request->validate([
         'RO_code' => 'required',
         'Petrol_Diesel_Type' => 'required|unique:tbl_ro_petroldiesel',
        ]);
         try {

        $RoPetrolDesiel = new RoPetrolDesiel();
         $RoPetrolDesiel->RO_code=$request->input('RO_code');
           $RoPetrolDesiel->Petrol_Diesel_Type=$request->input('Petrol_Diesel_Type');
        
        $RoPetrolDesiel->is_active=1;
        $RoPetrolDesiel->save();
        $request->session()->flash('success','Added Successfully!!');
        
           } catch(\Illuminate\Database\QueryException $e){
               
             $request->session()->flash('success',' Sonthing is Wrong !!');
        }
       return back();



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RoPetrolDesiel  $roPetrolDesiel
     * @return \Illuminate\Http\Response
     */
    public function show(RoPetrolDesiel $roPetrolDesiel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RoPetrolDesiel  $roPetrolDesiel
     * @return \Illuminate\Http\Response
     */
    public function edit(RoPetrolDesiel $roPetrolDesiel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RoPetrolDesiel  $roPetrolDesiel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RoPetrolDesiel $roPetrolDesiel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RoPetrolDesiel  $roPetrolDesiel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      try {
            $ac = RoPetrolDesiel::where('id',$id)->delete();
             $request->session()->flash('success','Delete Successfully !!');   
            } catch(\Illuminate\Database\QueryException $ac){
                
            $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }


        
        
        return back();
    }
    function active(Request $request,$id, $actt)
    {
        
        $ac = RoPetrolDesiel::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }
}
