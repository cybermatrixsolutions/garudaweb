<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\QR_Code;
use App\MastorRO;
use App\CityModel;
use App\StateManagementModel;
use App\Principal;
use App\VechilManage;
use App\VehicleMakeModel;
use App\VehicleModel;
use Auth;
use Gate;

class Qr_codeController extends Controller
{
  public function add_qr_code(Request $request)
    { 


         $validatedData = $request->validate([
         'Ro_code' => 'required',
         'Customer_Code' => 'required',
         'Vehicle_Reg_no' => 'required|unique:tbl_qrcode_manage',
        
    ]);
         try {
             $num1='-'.$request->input('Ro_code').'-'.$request->input('Customer_Code').'-'.$request->input('Vehicle_Reg_no');
           $num2  = uniqid();
         //= (rand(100,1000));
        $QR_Code = new QR_Code();
        $QR_Code->Ro_code=$request->input('Ro_code'); 
        $QR_Code->Customer_Code=$request->input('Customer_Code');
        $QR_Code->Vehicle_Reg_no=$request->input('Vehicle_Reg_no');
         $QR_Code->qrcode=$num2.$num1;
       
        $QR_Code->save();
        $request->session()->flash('success','QR Code Generate Successfully!!');
         } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');
                
            }
    	
        
       return back();
    }
      public function index()
    {
        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $data =QR_Code::join('tbl_ro_master', 'tbl_qrcode_manage.Ro_code', '=', 'tbl_ro_master.RO_code')
            ->join('tbl_customer_master', 'tbl_qrcode_manage.Customer_Code', '=', 'tbl_customer_master.Customer_Code')
            ->select('tbl_qrcode_manage.*','tbl_ro_master.pump_legal_name as pump_name','tbl_customer_master.Customer_Name as Customer_Name')->orderby('id','desc');

        if(Auth::user()->user_type!=1 && $rocode!='')
                $data=$data->where('tbl_qrcode_manage.Ro_code',$rocode);

              $data=$data->get();

         $data1 = $MastorRO=MastorRO::where('is_active','1');

         if(Auth::user()->user_type!=1 && $rocode!='')
                $data1=$data1->where('tbl_ro_master.RO_code',$rocode);

              $data1=$data1->get();


        return view('backend.qr_code',compact('data','data1'));


    }
     public function delete(Request $request, $id)
    {
        try {
            $ac = QR_Code::where('id',$id)->delete();
        $request->session()->flash('success','Delete Successfully !!');
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
       
        return back();


    }
     function active(Request $request,$id, $actt)
    {
        
    	$ac = QR_Code::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }

     function print_val(Request $request,$id){

        $Customer= QR_Code::where('id',$id)->first();
        $detail = QR_Code::join('tbl_ro_master','tbl_qrcode_manage.Ro_code','=','tbl_ro_master.RO_code')->join('tbl_customer_master','tbl_qrcode_manage.Customer_Code','=','tbl_customer_master.Customer_Code')->where('tbl_qrcode_manage.id',$id)->select('tbl_qrcode_manage.*','tbl_ro_master.pump_legal_name as pump_name','tbl_ro_master.pump_address as pump_addetail','tbl_ro_master.city as citys','tbl_ro_master.state as states','tbl_ro_master.image as images','tbl_ro_master.customer_care','tbl_customer_master.Company_name as company_name','tbl_customer_master.Department as department')->first();
       
         $CityModel=CityModel::where('id',$detail->citys)->first();
         $StateManagementModel=StateManagementModel::where('id',$detail->states)->first();
         $MastorRO=MastorRO::where('RO_code',$detail->Ro_code)->first();
         $Principal=Principal::where('company_code',$MastorRO->company_code)->first();
         $Vechilm = VechilManage::where('Registration_Number',$Customer->Vehicle_Reg_no)->first();
         $VehicleMakeModel = VehicleMakeModel::where('id',$Vechilm->Make)->first();
         $VehicleModel = VehicleModel::where('id',$Vechilm->Model)->first();

        return view('backend.print_val',compact('Vechilm','Customer','detail','CityModel','StateManagementModel','Principal','MastorRO','VehicleMakeModel','VehicleModel'));
    }

    function QrcodeReganaret(Request $request,$id){


       
        $Customer= QR_Code::find($id);
        $num1='-'.$Customer->Ro_code.'-'.$Customer->Customer_Code.'-'.$Customer->Vehicle_Reg_no;
       // $num2 = (rand(100,1000));
        $num2  = uniqid();
        $Customer->qrcode=$num2.$num1;
        $Customer->is_active=1;
        $Customer->save();
        $request->session()->flash('success',' Re-Generate QR Code  Successfully!!');
        return back();
    }

    
  


}
