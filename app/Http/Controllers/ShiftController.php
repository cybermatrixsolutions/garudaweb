<?php

namespace App\Http\Controllers;

use Auth;
use App\Shift;
use App\PedestalMaster;
use App\RoPersonalManagement;
use App\nozzelsReadingModel;
use App\TransactionModel;
use App\ShiftPedestal;
use App\PaymentModel;
use App\ShiftSettelementModel;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\TableTankMasterModel;
use App\TankReading;
use App\ShiftType;
use App\PaymentmodesattlementModel;
use Illuminate\Support\Facades\Validator;
use Log;
use App\OutletConfigModel;

class ShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 

     
        //
        $rocode='';
        $id=0;
        $person=[];
        $Padestal=[];
		    $tank = [];
        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $rocode=Auth::user()->getPersonel->RO_Code;

        }else{
            
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }
        }
       
        $allshifts=Shift::where('ro_code',$rocode)->where('is_active',1)->get();
        $openshift=Shift::where('ro_code',$rocode)->where('closer_date',null)->get();
       
        foreach ($openshift as $value) {

          
          $shift = ShiftPedestal::where('shift_id',$value->id)->first();
          if (!empty($shift)) {

            $list[] = ShiftPedestal::where('shift_id',$value->id)->first();
           
          }

        }
          

        $Shift=Shift::where('ro_code',$rocode)->where('shift_manager',$id)->where('is_active',1)->first();

        $salesmanger=[];

        if($allshifts!=null){
            foreach ($allshifts as $allshift) {
                array_push($salesmanger,$allshift->shift_manager);
                
                if($allshift!=null && $allshift->getPersonnel!=null)
                $person=array_merge($person,$allshift->getPersonnel->pluck('id')->toArray());

                if($allshift!=null && $allshift->getPadestal!=null)
                $Padestal=array_merge($Padestal,$allshift->getPadestal->pluck('id')->toArray());
			
				if($allshift!=null && $allshift->getTanks!=null)
                $tank=array_merge($tank,$allshift->getTanks->pluck('tank_id')->toArray());
            }
            
        }

        $ShiftType_old = array('1'=>'Fuel','2'=>'Other','3'=>'Fuel & Other(Both)');

        $Shifttype=ShiftType::where('RO_Code',$rocode)->where('shift_type','regular')->get();
        $flexiShifttype=ShiftType::where('RO_Code',$rocode)->where('shift_type','flexi')->get();

        Log::info('$Shifttype   -   '.print_r($Shifttype,true));
        Log::info('$flexuShifttype   -   '.print_r($flexiShifttype,true));
        $flexishift = OutletConfigModel::where('rocode',$rocode)->where('field_name',"flexi_shift")->first();
        Log::info('$flexishift status   -   '.print_r($flexishift,true));
        if($flexishift){
        if($flexishift->value=="on"){
           $flexishiftstatus="flexi";
         }else{
         $flexishiftstatus="regular";

         }
        }else{
              $flexishiftstatus="regular";
        }
		
		
        $personals=PedestalMaster::where('RO_code',$rocode)->where('is_Active',1)->whereNotIn('id',$Padestal)->get();
        $per=array_merge($person,$salesmanger);
        $pedestals=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->whereIn('Designation',[30,29])->whereNotIn('id',$per)->get();

        $shiftmanager=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->where('Designation',29)->whereNotIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();
		
		    $tanks=TableTankMasterModel::where('RO_code',$rocode)->where('is_active',1)->whereNotIn('id',$tank)->get();
		
        
       return view('backend.shiftmanagement.create',compact('pedestals','openshift','personals','Shift','shiftmanager','shifts','tanks','Shifttype','ShiftType_old','flexishiftstatus','flexiShifttype'));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function store(Request $request)
    {  

      Log::info('ShiftController@store here - input  -  '.print_r($request->all(),true));



    // dd($request->all());
 try{
         $id=0;
         $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;
        }elseif (Auth::user()->getRocode!=null) {

              $RO_Code=Auth::user()->getRocode->RO_code;
          }

           $flexishift = OutletConfigModel::where('rocode',$RO_Code)->where('field_name',"flexi_shift")->first();
       // Log::info('$flexishift status   -   '.print_r($flexishift,true));
        if($flexishift){
        if($flexishift->value=="on"){
           $flexishiftstatus="flexi";
         }else{
            $flexishiftstatus="regular";

         }
        }else{
              $flexishiftstatus="regular";
        }
    
        $flexishiftstatus=$request->input('flexishiftstatus');
        $pes=$request->input('Padastal');
        $type_shift=$request->input('type');
        $data_created = $request->input('created_at');
        $shift_type=$request->input('shift_type');
        $type = explode(',', $shift_type);
        Log::info('pes  array --  '.print_r($pes,true));
       
        $data_created = str_replace('/','-',str_replace('AM','',str_replace('PM','', $data_created)));
        $created_ats = date('Y-m-d', strtotime(trim($data_created)));
        $type2 = $type[0];

        if($flexishiftstatus=="flexi"){
              $type2=date('H:i:s');

        }
        $created_at = date('Y-m-d H:i:s', strtotime("$created_ats $type2"));

            
        $shift_type = $type[1];
        

         if (!empty($Shifttype)) {
         
          $shift_type = $request->validate([
          'shift_type' => 'unique:shifts',
         ]);
        }

        $salesman=$request->input('salesman');
        if (!empty($request->input('tank'))&&($type_shift)!=2) {
                $tank=$request->input('tank');
        }
        
// dd($pes, $type_shift, $shift_type);

        $shift_manager=$request->input('shift_manager');
        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;

      
      if($flexishiftstatus=="flexi"){
        Log::info('my shift if flexi check closer date ');

           $Shift_search=Shift::where('shift_manager',$id)->where('created_at',$created_at)->where('shift_type',$shift_type)->where('closer_date','!=','NULL')->first();
      }else{
         Log::info('old funcationlaty work')  ;
     $Shift_search=Shift::where('shift_manager',$id)->where('created_at',$created_at)->where('shift_type',$shift_type)->first();

      }


     if(!empty($Shift_search)){

        $request->session()->flash('success','Shift Already Exist!!');
        return back();
      }

    

// dd($id, $created_at, $shift_type, $Shift_search);
 
        $shift=new Shift();
        $shift->shift_manager=$id;
        $shift->shift_type=$shift_type;
        $shift->type=$type_shift;
        $shift->created_at=$created_at;
        $shift->transation_date=$created_at;
        $shift->shifttype=$flexishiftstatus;
        $shift->ro_code=$RO_Code;
        $shift->save();

        $psdata=[];
        $saldata=[];

        $tankdata=[];

        if (!empty($pes) && $type_shift!=2){
            $i=0;
            foreach ($pes as $pe){
               $psdata[$i]=array('shift_id'=>$shift->id,'pedestal_id'=>$pe);
               $i++;
            }
        }

        if (!empty($salesman)) {
            $i=0;
            foreach ($salesman as $salesm){
               $saldata[$i]=array('shift_id'=>$shift->id,'personnel_id'=>$salesm);
               $i++;
            }
        }
            
        if (!empty($tank) && $type_shift!=2) {
          
            $i=0;
            foreach ($tank as $t){
               $tankdata[$i]=array('shift_id'=>$shift->id,'tank_id'=>$t);
               $i++;
            }
        }

        DB::table('shift_pedestal')->insert($psdata);
        DB::table('shift_personnel')->insert($saldata);
        DB::table('shift_tank')->insert($tankdata);

         $previous_cmr=$this->previous_cmr_show($pes);
       if(count($previous_cmr)>0){

        
        $reading=$previous_cmr['reading'];
         return view('backend.shiftmanagement.previous_cmr',compact('reading'));

       }

          
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return back();

    }

    public function previous_cmr_show($pes){

      Log::info('ShiftController@previous_cmr_show  $pedestial - '.print_r($pes,true));

       $id=0;
        $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
           if (Auth::user()->getRocode!=null) {
              $RO_Code=Auth::user()->getRocode->RO_code;
           }
        } 



       $Shift=Shift::join('shift_pedestal','shifts.id','=','shift_pedestal.shift_id')
          ->where('shifts.ro_code',$RO_Code)
         ->where('shift_pedestal.pedestal_id',$pes)
         ->whereNotNull('closer_date')
         ->orderBy('created_at','desc')
         ->select('shifts.id as id','shifts.*')
         ->limit(1)
         //->value('shifts.id');

         ->get();

          Log::info('Shift   --  '.print_r($Shift,true)); 
$shiftid=0;
$shift_manager_id=0;
$shifttype="";
         foreach($Shift as $shiftdata){
            $shiftid=$shiftdata->id;
            $shifttype=$shiftdata->shifttype;
            $shift_manager_id =$shiftdata->shift_manager; 

         }

          if($shiftid&&$shiftid!=0){

           Log::info('Shift record found.ready to get reading !!!!!!!!!!!!!!!!!!!');
           
          $reading=DB::select( DB::raw("select distinct t4.RO_code,t4.id,Nozzle_Number,Nozzle_Start as Nozzle_Start,t5.Pedestal_Number,max(Nozzle_End) as Nozzle_End,test,reading,t9.item_name from tbl_pedestal_nozzle_master t1 inner join shift_pedestal t2 on t1.Pedestal_id=t2.pedestal_id inner join shifts t3 on t3.id=t2.shift_id inner join tbl_pedestal_master t5 on t5.id=t2.pedestal_id inner join tbl_ro_nozzle_reading t4 on trim(t4.Nozzle_No)=trim(t1.Nozzle_Number)
            inner join tbl_price_list_master t9 on t9.id=t1.fuel_type

            where t4.Nozzle_End<>'' and t3.fuel=0 and t4.RO_code='$RO_Code'  and t5.RO_code='$RO_Code' and closer_date is not null and t1.is_active=1 and t4.shift_id ='$shiftid' group by Nozzle_Number
            order by t5.Pedestal_Number,t1.Nozzle_Number"));
            Log::info('ShiftController@store   reading  -  '.print_r($reading,true));
        
        
        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('is_Active',1)->where('Designation',29)->where('id',$shift_manager_id)->orderBy('Personnel_Name', 'asc')->get();
       
        Log::info('ShiftController@shiftCoder reading - '.print_r($reading,true));
        //Log::info('ShiftController@shiftCoder Shift - '.print_r($Shift,true));
        $flexishiftstatus="";
          if($shifttype){

            $flexishiftstatus=$shifttype;
          }

                if($reading){

                   return array('reading'=>$reading);
                }else{
                    Log::info('Reading  record not found !!!!!!!!!!!!!!!!!!!');
                  return array();

                }

         
    
     
          }else{


            Log::info('Shift record not found !!!!!!!!!!!!!!!!!!!');
            return array();
          }



    }

    public function previouscmr_update(Request $request){

      Log::info('ShiftControler@previouscmr_update     input -'.print_r($request->all(),true));
       $input=$request->all();
       $nozzle_nosarr=$input['Nozzle_Number'];
       $id_arr=$input['id'];
       $nozzle_readarr=$input['Nozzle_End'];
       $test=$input['test'];
       $reading=$input['reading'];

       $id=0;
         $Ro_Code='';

        if (Auth::user()->getRocode!=null) {
          $Ro_Code=Auth::user()->getRocode->RO_code;
       }

     /*  try{*/
          for ($j = 0; $j < count($nozzle_nosarr); $j++) {

      /* DB::update("update tbl_ro_nozzle_reading set Nozzle_End='".trim($nozzle_readarr[$j])."',
        test= '".$id_arr[$j]."',reading='".trim($reading[$j])."'
        where id='".trim($id_arr[$j])."' and RO_code='".$Ro_Code."' and Nozzle_End is not null");


              DB::update("update tbl_ro_nozzle_reading set Nozzle_Start='".trim($nozzle_readarr[$j])."'
        
        where Nozzle_No='".trim($nozzle_nosarr[$j])."' and RO_code='".$Ro_Code."' and Nozzle_End is null");*/

              $firstrecord=DB::table('tbl_ro_nozzle_reading')->where('id',$id_arr[$j])
        ->where('RO_code',$Ro_Code)
        ->where('Nozzle_End','<>',"")
        ->get();

        /*$firstrecord1=DB::table('tbl_ro_nozzle_reading')->where('id',$id_arr[$j])
        ->where('RO_code',$Ro_Code)
        ->where('Nozzle_End','<>',"")
        ->get();*/

        Log::info('firstrecord ==== '.print_r($firstrecord,true));
        Log::info('$id_arr[$j]==== '.print_r($id_arr[$j],true));
        Log::info('$test[$j]==== '.print_r($test[$j],true));
        Log::info('$reading[$j]==== '.print_r($reading[$j],true));
        Log::info('$Ro_Code==== '.print_r($Ro_Code,true));

   


      $firstrecord=DB::table('tbl_ro_nozzle_reading')->where('id',$id_arr[$j])
        ->where('RO_code',$Ro_Code)
        ->where('Nozzle_End','<>',"")
       ->update([
        'Nozzle_End'=>trim($nozzle_readarr[$j]),
          'test'=>$test[$j],
          'reading'=>trim($reading[$j])
          ]);
   



    $secondrecord=DB::table('tbl_ro_nozzle_reading')->where('Nozzle_No',trim($nozzle_nosarr[$j]))
        ->where('RO_code',$Ro_Code)
        ->whereNull('Nozzle_End')
       ->update([
        'Nozzle_Start'=>trim($nozzle_readarr[$j])
          
          ]);

          }

   

          $request->session()->flash('success','Previous CMR update Successfully ');
  
         /*  }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
         }
*/
      return redirect('/shift/create');
     



  }
    public function shiftCloder(Request $request)
    {
          //
      Log::info('ShiftController@shiftcloder - input '.print_r($request->all(),true));
        $id=0;
        $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
           if (Auth::user()->getRocode!=null) {
              $RO_Code=Auth::user()->getRocode->RO_code;
           }
        } 

        $shift_manager=$request->input('shift_manager');

        if(isset($shift_manager) && $shift_manager!='')
            $id=$shift_manager;

        $allshifts=Shift::where('ro_code',$RO_Code)->where('is_active',1)->get();

        $salesmanger=[];

        if($allshifts!=null)
          foreach ($allshifts as $allshift) {
               array_push($salesmanger,$allshift->shift_manager);
           }
                    
              

        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('is_Active',1)->where('Designation',29)->whereIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();
         
        $Shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',1)->first();

        // Log::info('ShiftController@shiftCoder shiftmanager - '.print_r($shiftmanager,true));
        Log::info('ShiftController@shiftCoder shiftmanager - '.print_r($shiftmanager,true));
        Log::info('ShiftController@shiftCoder Shift - '.print_r($Shift,true));
        $flexishiftstatus="";
          if($Shift){

            $flexishiftstatus=$Shift->shifttype;
          }
    
       return view('backend.shiftmanagement.closer1',compact('Shift','shiftmanager','id','flexishiftstatus'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function shiftCloderStore(Request $request)
    { 
       Log::info('ShiftController@shiftcloder - input '.print_r($request->all(),true));
       try{
        $id=0;
         $input=$request->all();

        if (Auth::user()->getPersonel!=null) {

          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }
        }  
        
        $shift_manager=$request->input('shift_manager');

        if(isset($shift_manager) && $shift_manager!='')


            $id=$shift_manager;

          $cshift=$request->input('Shift');
        $padestal=$request->input('padestal');
        if (!empty($padestal)) 
        {
         
        
          $Reading_by=$request->input('Reading_by');
          
          $padestals= explode(',',$padestal);
          $ro_code=$request->input('ro_code');

          // Getting tanks reading info
          
          $tankArray = $request->input('tank');
          $capacityArray = $request->input('capacity');
          $unitMeasureArray = $request->input('unit_measure');
          $fuelTypeArray = $request->input('fuel_type');
          $readingCMArray = $request->input('reading_cm');
          $readingArray = $request->input('reading');
          $sumvalueArray = $request->input('sumvalue');
              
          $padestals=array_filter($padestals);  

          Log::info('padestals   -   '.print_r($padestals,true));
                foreach ($padestals as $padesno) {
                $Nozzle_Nos=$request->input($padesno.'_Nozzle_No');

                 Log::info('Nozzle_Nos   -   '.print_r($Nozzle_Nos,true));



                 if($Nozzle_Nos){
                
                foreach ($Nozzle_Nos as $Nozzle_No) {

                  $priceKey=$padesno.'_price_'.$Nozzle_No;
                  $Nozzle_EndKey=$padesno.'_Nozzle_End_'.$Nozzle_No;
                  $testKey=$padesno.'_test_'.$Nozzle_No;
                  $readingKey=$padesno.'_reading_'.$Nozzle_No;

                  $price=$input[$priceKey];
                  $nozzle_start=$input[$Nozzle_EndKey];
                  $test=$input[$testKey];
                  $reading=$input[$readingKey];

                  $nozelName=DB::table('tbl_pedestal_nozzle_master')->where('RO_code',$RO_Code)->where('id',$Nozzle_No)->value('Nozzle_Number');
              

                   $nozelDbData=nozzelsReadingModel::where('RO_code',$ro_code)->where('Nozzle_No',$nozelName)->where('Nozzle_End',null)->select('*')->get(); 

                  $nozelupdateData=nozzelsReadingModel::where('RO_code',$ro_code)->where('Nozzle_No',$nozelName)->where('Nozzle_End',null)->update(['Nozzle_End' =>$nozzle_start,'shift_id'=>$cshift,'test'=>$test,'reading'=>$reading,'price'=>$price,'Reading_by'=>$Reading_by]);

                  Log::info('nozelupdateData  --  '.print_r($nozelupdateData,true));
                  $nozzelsReading = new nozzelsReadingModel();
                  $nozzelsReading->RO_code=$ro_code;
                  $nozzelsReading->Nozzle_No=$nozelName;
                  $nozzelsReading->Nozzle_Start=$nozzle_start;
                  $nozzelsReading->Reading_by=$Reading_by;
                $nozzelsReading->save();

                }
              }else{

                Log::info('Nozel Record not found of this padesno - '.$padesno);
              }
              
          }
  		
      		// Inserting Tank reading values
      		if(isset($tankArray) && is_array($tankArray) && count($tankArray)>0){
          
            for($i=0;$i<count($tankArray);$i++){

               $totalesInwards=DB::table('tank_tankinwart')->where('tank_id',$tankArray[$i])->where('status',1)->get();
               $TankReading=TankReading::where('Tank_code',$tankArray[$i])->latest()->first();
               
               $tank_stack=0;
               $salesvalue=0;
               if($TankReading!=null){
                  $tank_stack=$TankReading->value;
               }

               foreach ($totalesInwards as $totalesInward) {
                 $tank_stack=$totalesInward->value+$tank_stack;
               }

               if($totalesInwards->count()>0)
               {
              DB::table('tank_tankinwart')->where('tank_id',$tankArray[$i])->update(['status'=>0]);

               $salesvalue=$tank_stack-$sumvalueArray[$i];

              $modelTankReading = new TankReading();
              $modelTankReading->shift_id=$cshift;
              $modelTankReading->Ro_code=$ro_code;
              $modelTankReading->Tank_code=$tankArray[$i];
              $modelTankReading->fuel_type=$fuelTypeArray[$i];
              $modelTankReading->capacity=$capacityArray[$i];
              $modelTankReading->unit_measure=$unitMeasureArray[$i];
              $modelTankReading->Reading=$readingCMArray[$i];
              $modelTankReading->dip_mm=$readingArray[$i];
              $modelTankReading->tank_stack=$tank_stack;
              $modelTankReading->sale_value=$salesvalue;
              $modelTankReading->is_active=1;
              $modelTankReading->value=$sumvalueArray[$i];
              $modelTankReading->reading_date  = date("Y-m-d");
             $modelTankReading->save();
            } 

            }

          }
        }  
        

        $ldate = $request->input('closer_date');

        $data_created = str_replace('/','-',str_replace('AM','',str_replace('PM','', $ldate)));
        $closer_date = date('Y-m-d ', strtotime(trim($data_created)));
        Log::info('closer_date  after trim- '.$closer_date);

				
			$allshifts=Shift::where('id',$cshift)->first();


      $Shifttype=ShiftType::where('id',$allshifts->shift_type)->first();
      $time=date('H:i:s');
        if($flexishiftstatus="flexi"){
             $closer_date = date('Y-m-d H:i:s', strtotime("$closer_date $time"));

        }else{

         $closer_date = date('Y-m-d H:i:s', strtotime("$closer_date $Shifttype->end_time"));

        }
        
       
         Log::info('closer_date  final- '.$closer_date);
         Log::info('allshifts - '.print_r($allshifts,true));
         Log::info('Shifttype - '.print_r($Shifttype,true));
        


// dd( $closer_date);



     Shift::where('id',$cshift)->update(['closer_date'=>$closer_date,'is_active' =>0]);

        $request->session()->flash('success','Shift Successfully Close ');
        return redirect()->route('shiftAllocation');

        }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return back();
       
    }



    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getShiftOfManager(Request $request,$id)
    {
        $id=0;
        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }
        }

       $Shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('status',0)->get();
       $data=[];
       $Shift->pluck('closer_date','id')->toArray();
       foreach ($Shift as $value) {
           $data[$value->id]=date('d/m/Y',strtotime($value->closer_date));
       }

       return $data;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Display the specified resource.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function show(Shift $shift)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request , $ids)
    {

      
      Log::info('ShiftController@edit   id  --   '.print_r($ids,true));
       $rocode='';
        $id=0;
        $person=[];
        $Padestal=[];
        $tank=[];
        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $rocode=Auth::user()->getPersonel->RO_Code;

        }else{
            
               if (Auth::user()->getRocode!=null) {
                  $rocode=Auth::user()->getRocode->RO_code;
               }
        }

        
         $allshifts=Shift::where('is_active',1)->get();

         $salesmanger=[];
        if($allshifts!=null){
          foreach ($allshifts as $allshift) {
              array_push($salesmanger,$allshift->shift_manager);
              
              if($allshift!=null && $allshift->getPersonnel!=null)
              $person=array_merge($person,$allshift->getPersonnel->pluck('id')->toArray());

              if($allshift!=null && $allshift->getPadestal!=null)
              $Padestal=array_merge($Padestal,$allshift->getPadestal->pluck('id')->toArray());
		  
			if($allshift!=null && $allshift->getTanks!=null)
			$tank=array_merge($tank,$allshift->getTanks->pluck('id')->toArray());
          }
            
        }

        $Shifttype=ShiftType::where('RO_Code',$rocode)->get();
         $ShiftType_old = array('1'=>'Fuel','2'=>'Other','3'=>'Fuel & Other(Both)');

         log::info('salesmanger  first after push - '.print_r($salesmanger,true));

      //  $shiftmanager=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->where('Designation',29)->whereIn('id',$salesmanger)->get();

        $pedestals=PedestalMaster::where('RO_code',$rocode)->where('is_Active',1)->whereNotIn('id',$Padestal)->get();
        $per=array_merge($person,$salesmanger);

        $personals =RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->whereIn('Designation',[30,29])->whereNotIn('id',$per)->get();
       
        $shiftmanager=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->where('Designation',29)->whereNotIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();

          //log::info('shiftmanager first option  - '.print_r($shiftmanager,true));
          log::info('personals  salesman   - '.print_r(collect($personals)->pluck('Personnel_Name'),true));
		
		$tanks=TableTankMasterModel::where('RO_code',$rocode)->where('is_active',1)->whereNotIn('id',$tank)->get();
         
        $shift = shift::find($ids);
        Log::info('shift record -  detail -  '.print_r($shift,true));
        $Shifttype=ShiftType::where('RO_Code',$rocode)->where('shift_type',$shift->shifttype)->get();
        $flexishiftstatus=$shift->shifttype;
     
      return view('backend.shiftmanagement.editshift',compact('shift','shiftmanager',
        'personals','pedestals','tanks','Shifttype','ShiftType_old','flexishiftstatus'));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

      Log::info('ShiftController@update   input  --   '.print_r($request->all(),true));

   try{
         // dd($request->all());
         
         $salemans=$request->input('salesman');
         $shift_manager=$request->input('shift_manager');
         $type_shift=$request->input('type');
         $shift_type=$request->input('shift_type');
         $data_created = $request->input('created_at');
         $flexishiftstatus=$request->input('flexishiftstatus');

         $type = explode(',', $shift_type);
                //$date2 = new Carbon(str_replace('/', '-',$data_created));

                //$created_at =date('Y-m-d H:i:s',strtotime($date2));
        
        $data_created = str_replace('/','-',str_replace('AM','',str_replace('PM','', $data_created)));

                $created_ats = date('Y-m-d', strtotime(trim($data_created)));
        $type2 = $type[0];
           if($flexishiftstatus=="flexi"){
              $type2=date('H:i:s');

        }
        $created_at = date('Y-m-d H:i:s', strtotime("$created_ats $type2"));

        
              
        $shift_type = $type[1];

//         $shift = shift::find($id);
//          if ($shift->shift_type != $shift_type2) {
         
//           $shift_type = Validator::make($shift->shift_type, [
//           'shift_type' => 'unique:shifts',
//          ]);
//         }
// dd($shift_type);

        $pedestals=$request->input('Padastal');

         if (!empty($request->input('tank')) && $type_shift!=2) {
                $tank=$request->input('tank');
        }

        if(count($salemans)>0 || count($pedestals)>0 || count($tanks)>0)
        {

            $shift=Shift::find($id);
            $shift->shift_manager=$shift_manager;
            $shift->created_at=$created_at;
            $shift->transation_date=$created_at;
            $shift->type=$type_shift;
            $shift->shift_type=$shift_type;
            $shift->save();

            $shift->getPadestal()->detach();
            $shift->getPersonnel()->detach();
            $shift->getTanks()->detach();

      if (!empty($request->input('Padastal')) && $type_shift!=2) {
            $shift->getPadestal()->attach($pedestals);
      }
      $shift->getPersonnel()->attach($salemans);

			if (!empty($request->input('tank')) && $type_shift!=2) {
            $shift->getTanks()->attach($tank);
			}
            $request->session()->flash('success','Update Successfully !!');

        }else{
          
            $request->session()->flash('success','Something Wrong !!');

        }

       }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return back();
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shift $shift)
    {
        //
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function settlement(Request $request)
    {   


         $id=0;
        $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }

        }

        $Shiftid=0;
        $Shift=null;

        if($request->input('siftname')!='')
            $Shiftid=$request->input('siftname');
        
        $shift_manager=$request->input('shift_manager');

        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;


        $ather=null;
        $fuels=null;
        $pendin=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('fuel',0)->orderBy('created_at', 'desc')->get();
        
        if($Shiftid!=0)
            $Shift=Shift::where('ro_code',$RO_Code)->where('id',$Shiftid)->where('fuel',0)->first();
        // else
        //     $Shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('fuel',0)->first();
          
       

        if($Shift!=null){

            $ather=TransactionModel::where('shift_id',$Shift->id)->where('petrol_or_lube',2)->get();
            $fuels=TransactionModel::where('shift_id',$Shift->id)->where('petrol_or_lube',1)->get();


            $getnozzleData=PedestalMaster::join('shift_pedestal','shift_pedestal.pedestal_id','tbl_pedestal_master.id')
            ->join('tbl_pedestal_nozzle_master','tbl_pedestal_nozzle_master.Pedestal_id','=','tbl_pedestal_master.id')
            ->where('tbl_pedestal_nozzle_master.is_active',1)
             ->where('tbl_pedestal_nozzle_master.RO_code',$RO_Code)
           // ->groupBy('tbl_pedestal_nozzle_master.fuel_type')

            ->where('shift_pedestal.shift_id',$Shift->id)
          
            ->get();
             $getnozzleData=$getnozzleData->groupBy('fuel_type');
             $padDataFuelWise=$getnozzleData;

            //dd($Shift->id,$padDataFuelWise,$Shift->id,$getnozzleData);

            //nozzelsReadingModel::join('')
        }

          $allshifts=Shift::where('ro_code',$RO_Code)->where('is_active',0)->where('fuel',0)->get();
        $salesmanger=[];
// dd($ather);
        if($allshifts!=null)
          foreach ($allshifts as $allshift) {
               array_push($salesmanger,$allshift->shift_manager);
           }
        
        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('Designation',29)->whereIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();

        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->where('IsActive','1')->orderby('order_number','ASC')->orderby('name','ASC')->get();

        $cash_id=0;
       
       return view('backend.shiftmanagement.settlement1',compact('Shift','pendin','ather','fuels','PaymentModel','Shiftid','shiftmanager','id','padDataFuelWise','RO_Code','cash_id'));
    }
    
        /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */


    public function settlementconfirm(Request $request)
    {   
Log::info('ShiftController@settlement_confirm input - '.print_r($request->all(),true));
         $id=0;
        $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }

        }





        $Shiftid=0;

        if($request->input('siftname')!='')
                    $Shiftid=$request->input('siftname');
                
        $shift_manager=$request->input('shift_manager');

        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;


        $ather=null;
        $fuels=null;
        $shift=null;
        $paymentmode_sattlement_new=null;
        $pendin=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('fuel',1)->where('confirm',0)->orderBy('created_at', 'desc')->get();

        //Log::info('ShiftController pendin   -'.print_r($pendin,true));
        
        if($Shiftid!=0){
            $shift=Shift::where('ro_code',$RO_Code)->where('id',$Shiftid)->where('confirm',0)->where('fuel',1)->first();
          }
        // else

            // $shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('confirm',0)->where('fuel',1)->first();
          
       

        if($shift!=null){

            $ather=TransactionModel::where('shift_id',$shift->id)->where('petrol_or_lube',2)->get();
            $fuels=TransactionModel::where('shift_id',$shift->id)->where('petrol_or_lube',1)->get();

            $myshiftId=$shift->id;
            $mySatmntId=Db::table('tbl_shift_settlement')->where('shift_id',$myshiftId)->value('id');

            $paymentmode_sattlement_new = DB::table('tpl_payment_mode') 
          //->leftjoin('paymentmode_sattlement','paymentmode_sattlement.paymentmode_id','tpl_payment_mode.id')
          ->leftJoin('paymentmode_sattlement',  function($join) use ($mySatmntId)
                            {
                              $join->on('paymentmode_sattlement.paymentmode_id','tpl_payment_mode.id')
                                    ->where('paymentmode_sattlement.sattlement_id','=',$mySatmntId)
                                     ;

                                    
                            })
          
          ->select('tpl_payment_mode.id','tpl_payment_mode.name','paymentmode_sattlement.garruda_receipt','paymentmode_sattlement.manual_receipt','paymentmode_sattlement.amount','paymentmode_sattlement.ref_no' ,
            'paymentmode_sattlement.sattlement_id')
        
          ->where('tpl_payment_mode.IsActive',1)
           ->where('tpl_payment_mode.ro_code',$RO_Code)
          ->groupBy('tpl_payment_mode.name')
          ->orderBy('order_number','asc')
          ->get();

     Log::info('paymentmode_sattlement_new ---- '.print_r($paymentmode_sattlement_new,true));
        }



// dd($Shiftid, $shift_manager, $shift);


         





        $allshifts=Shift::where('ro_code',$RO_Code)->where('fuel',1)->whereNotNull('closer_date')->where('confirm',0)->get();

        $salesmanger=[];
        if($allshifts!=null)
          foreach ($allshifts as $allshift) {
               array_push($salesmanger,$allshift->shift_manager);
           }
        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('Designation',29)->whereIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();

// dd($Shift);
        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->orderby('order_number','ASC')->orderby('name','ASC')->get();

          

        

          // Log::info('paymentmode_sattlement_new   ====='.print_r($paymentmode_sattlement_new,true));

          // $Shift=Shift::where('ro_code',$RO_Code)->where('fuel',1)->where('confirm',0)->get();
          // $shift=Shift::find($id);


        
       
       return view('backend.shiftmanagement.settlement_confirm',compact('pendin','Shiftid','shift','PaymentModel','shiftmanager','id','paymentmode_sattlement_new'));
    }
    
        /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */    


    public function settlementconfirmstore(Request $request)
    {    

      Log::info('ShiftController@settlementconfirmstore input '.print_r($request->all(),true));
      try{

         $id=0;
         $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
            
               if (Auth::user()->getRocode!=null) {
                  $RO_Code=Auth::user()->getRocode->RO_code;
                 
               }
        }

        $shift_manager=$request->input('shift_manager');
        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;

        $garruda_receipt=$request->input('garruda_receipt');
        $manual_receipt=$request->input('manual_receipt');
        $type=$request->input('type');
        
        $pes=explode(',',$request->input('paymentmode_new'));

        $Diff=$request->input('Diff');
        $shift_id=$request->input('shift_id');
        
        $shift = ShiftSettelementModel::where('shift_id',$shift_id)->first();
       
        $shift->manual_receipt=$manual_receipt;
        // dd($manual_receipt);
        $shift->Diff=$Diff;
   
        $shift->save();


    $delete_payment = DB::table('paymentmode_sattlement')->where('sattlement_id', $shift->id)->delete();

        $psdata=[];

        $i=0;
        foreach ($pes as $pe){
            if(trim($pe)!=''){
                $paymentmode_id=$request->input('paymentmode_'.$pe);
                
                $ref_no=$request->input('ref_nom_'.$pe);

                // if($request->input('payment_'.$pe)!=null)
                    $paymen=$request->input('payment_'.$pe);
                    $garruda_receipt1_=$request->input('garruda_receipt1_'.$pe);
                $manual_receipt1_=$request->input('manual_receipt1_'.$pe);
                // else
                //      $paymen=0;
               
               // if($paymen!=0){
 // dd( $manual_receipt1_);
                $psdata[$i]=array('amount'=>$paymen,'type'=>$shift->type,'paymentmode_id'=>$pe,'sattlement_id'=>$shift->id,'ref_no'=>$ref_no,'garruda_receipt'=>$garruda_receipt1_,'manual_receipt'=>$manual_receipt1_);
                $i++;
                
               // }
            }
            
        }

        DB::table('paymentmode_sattlement')->insert($psdata);

        // if($type==1){
           Shift::where('id',$shift_id)->update(['confirm' =>1]);
           
        // }else{
        //    Shift::where('id',$shift_id)->update(['confirm' =>1]);
        // }

         //return redirect()->route('settlement');
       
    }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return back();

    }


    public function settlementLueb(Request $request)
    {   


        $id=0;
        $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }
        }

        $Shiftid=0;
        if($request->input('siftname')!='')
            $Shiftid=$request->input('siftname');
        
        $shift_manager=$request->input('shift_manager');
        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;

        $creditsale=null;
        $ather=null;
        $fuels=null;
        $pendin=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('lueb',0)->get();
        
        if($Shiftid!=0)
            $Shift=Shift::where('ro_code',$RO_Code)->where('id',$Shiftid)->first();
        else
            $Shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('lueb',0)->first();
          


        if($Shift!=null){
           
            $ather=TransactionModel::where('shift_id',$Shift->id)->where('cust_name','!=','credit')->where('petrol_or_lube',2)->get();

            $creditsale=TransactionModel::where('shift_id',$Shift->id)->where('petrol_or_lube',2)->get();
           
        }

        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('Designation',29)->orderBy('Personnel_Name', 'asc')->get();
      
        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->where('IsActive','1')->orderby('order_number','ASC')->orderby('name','ASC')->get();

       
       return view('backend.shiftmanagement.settlementLube',compact('Shift','pendin','ather','PaymentModel','Shiftid','shiftmanager','id','creditsale'));
    }
  

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    
    public function settlementstore(Request $request)
    { 


    Log::info('ShiftController@settlementstore  store sattelement input - ' .print_r($request->all(),true));    
      try{
        

         $id=0;
         $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
            
               if (Auth::user()->getRocode!=null) {
                  $RO_Code=Auth::user()->getRocode->RO_code;
                 
               }
        }

        $shift_manager=$request->input('shift_manager');
        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;
        $Nozzle_Amount=0;
        $Nozzle_grass=0;
        $Nozzle_test=0;
        $Credit_lube=0;
        
        if(null !==($request->input('Nozzle_Amount')) && $request->input('Nozzle_Amount')!=''){
                $Nozzle_Amount=$request->input('Nozzle_Amount');
                $Nozzle_grass=$request->input('Nozzle_grass');
                $Nozzle_test=$request->input('Nozzle_test');
              }
       
              
        if(null !==($request->input('Credit_lube')) && $request->input('Credit_lube')!=''){      
        $Credit_lube=$request->input('Credit_lube');
              }
        $garruda_receipt=$request->input('garruda_receipt');
        $manual_receipt=$request->input('manual_receipt');
        $type=$request->input('type');
        
        $pes=explode(',',$request->input('paymentmode'));
        $Credit_fuel='';
        
        $trans_date=date('Y-m-d H:i:s');
        $trans_by=$id;
        $Diff=$request->input('Diff');
        $shift_id=$request->input('shift_id');
        $total_exp_amount=$request->input('total_exp_amount');
        $deposit_amount_total=$request->input('deposit_amount_total');
        $input=$request->all();

        $exp_amount_arr=array();
        if(isset($input['exp_amount'])){


           $exp_amount_arr=$request->input('exp_amount');
        }

        $references_arr=array();
        if(isset($input['references'])){

         $references_arr=$request->input('references');
        }


        $references_deposit_arr=array();
        if(isset($input['references_deposit'])){


            $references_deposit_arr=$request->input('references_deposit');
        }

        $deposit_amount_arr=array();
        if(isset($input['deposit_amount'])){


         $deposit_amount_arr=$request->input('deposit_amount');
        }
       
   
     
        
        
        $shift=new ShiftSettelementModel();
        
          
        $shift->Nozzle_Amount=$Nozzle_Amount;
        $shift->Nozzle_test=$Nozzle_test;
        $shift->Nozzle_grass=$Nozzle_grass;
        

        if($type==1 || $type==3){
           $shift->Credit_fuel=$Credit_fuel;
         }else{
            $shift->Credit_lube=$Credit_lube;
         }
        $shift->trans_date=$trans_date;
        $shift->trans_by=$trans_by;
        $shift->Credit_receipt=$garruda_receipt;
        $shift->manual_receipt=$manual_receipt;
        $shift->Diff=$Diff;
        $shift->type=$type;
        $shift->shift_id=$shift_id;
        $shift->exp_amount=$total_exp_amount;
        $shift->deposit_amount= $deposit_amount_total;

         
       
        $shift->save();

        $psdata=[];

        $i=0;
        foreach ($pes as $pe){
            if(trim($pe)!=''){
                $paymentmode_id=$request->input('paymentmode_'.$pe);
                
                $ref_no=$request->input('ref_nom_'.$pe);
                $garruda_receipt1_=$request->input('garruda_receipt1_'.$pe);
                $manual_receipt1_=$request->input('manual_receipt1_'.$pe);

                    $paymen=$request->input('payment_'.$pe);
              
// dd($manual_receipt1_, $ref_no, $garruda_receipt1_, $paymentmode_id , $paymen);
                $psdata[$i]=array('amount'=>$paymen,'type'=>$shift->type,'paymentmode_id'=>$pe,'sattlement_id'=>$shift->id,'ref_no'=>$ref_no,'garruda_receipt'=>$garruda_receipt1_,'manual_receipt'=>$manual_receipt1_);
                $i++;
                
               // }
            }
            
        }



        DB::table('paymentmode_sattlement')->insert($psdata);

        // if($type==1){
           Shift::where('id',$shift_id)->update(['status' =>1,'fuel'=>1]);
           
        // }else{
           // Shift::where('id',$shift_id)->update(['status' =>1,'lueb'=>1]);
        // }

         //return redirect()->route('settlement');

         

            if(count($references_deposit_arr)){

              foreach($references_deposit_arr as $key   => $deprefarr){



                   DB::table('tbl_shift_depoist')->insert(
                    ['Ro_Code' => $RO_Code, 
                    'shift_id' => $shift_id,
                    'reference' => $deprefarr,
                    'amount' =>  $deposit_amount_arr[$key],
                   
                  ]
                );
             }

           }


            if(count($references_arr)){

              foreach($references_arr as $key  =>$deprefarr){

                   DB::table('tbl_shift_expense')->insert(
                    ['Ro_Code' => $RO_Code, 
                    'shift_id' => $shift_id,
                    'reference' => $deprefarr,
                    'amount' =>  $exp_amount_arr[$key],
                   
                  ]
                );
             }

           }


            
       
    }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return back();

    }


    public function getShift(Request $request){

      Log::info('shiftController@getshift - input  - '.print_r($request->all(),true));

       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $from_date=$request->input('request_date');

        $from_dates = new Carbon(str_replace('/', '-',$from_date));
       
        $from_dates = date('Y-m-d', strtotime($from_dates));
         
        // $Shifts=Shift::where('ro_code',$rocode)->whereDate('created_at','>=',$from_dates.' 00:00:00')->whereDate('created_at','<=',$from_dates.' 23:59:59')->get();

   $Shifts = DB::table('shifts')
            ->join('tbl_personnel_master', 'tbl_personnel_master.id', 'shifts.shift_manager')
            ->select('tbl_personnel_master.Personnel_Name as name','shifts.created_at','shifts.id','shifts.transation_date','shifts.shifttype')
            ->where('shifts.ro_code',$rocode)
            ->where('shifts.confirm',0)
            ->whereDate('shifts.transation_date','>=',$from_dates.' 00:00:00')
            ->whereDate('shifts.transation_date','<=',$from_dates.' 23:59:59')
            ->get();   

        Log::info('shiftController@getshift - Shifts  - '.print_r($Shifts,true));     

       $data=[];
       foreach ($Shifts as $Shift) {

        $data[$Shift->id]=($Shift->name).'-'.(date('d/m/Y h:m:s a',strtotime($Shift->transation_date)));
       }

       Log::info('shiftController@getshift - data  after - '.print_r($data,true));
       
     return response()->json($data);
           
        
    }

    public function getPreShift($shiftManager){
       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
        
       $Shift=Shift::where('ro_code',$rocode)->where('shift_manager',$shiftManager)->latest()->first();
       
       $data=['pedestals'=>[],'salesman'=>[]];

       if($Shift!=null){

           foreach ($Shift->getPadestal as $getPadestal) {

            $data['pedestals'][]= $getPadestal->id;

           }

           foreach ($Shift->getPersonnel as $getPersonnel) {

            $data['salesman'][]= $getPersonnel->id;
            
           }
       }
       
      return response()->json($data);
           
        
    }

    public function shifttype(Request $request)
    {  
       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

       $Shifttype=ShiftType::where('RO_Code',$rocode)->get();

      return view('backend.shifttypes',compact('Shifttype')); 
    } 

    public function shifttypestore(Request $request)
    {  
       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $status=1;
        $RO_Code=$request->input('RO_Code');
        $type=$request->input('type');
        $start_time=$request->input('start_time');
        $end_time=$request->input('end_time');
        $shift_type=$request->input('shift_type');
        if($shift_type=="flexi"){
          //check flex shift of ro
          $flexishift=DB::table('ro_shift_config')->where('RO_Code',$RO_Code)->where('shift_type',"flexi")->first();
          if($flexishift){
             $status=0;
             $request->session()->flash('success','Flexi shift Already Exist!!');

            return Back();

          }else{

            $status=1;
          }





        }
        // dd($type);
        if($status==1){


        $ShiftType=new ShiftType();
        $ShiftType->RO_Code=$RO_Code;
        $ShiftType->shift_type=$shift_type;
        $ShiftType->type=$type;
        $ShiftType->start_time=$start_time;
        $ShiftType->end_time=$end_time;
       
        $ShiftType->save();
        }

      return Back(); 
    } 


    public function shifttypeedit(Request $request, $id)
    {  
       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

       $Shifttype=ShiftType::where('RO_Code',$rocode)->where('id',$id)->first();


      return view('backend.shifttypeedit',compact('Shifttype')); 
    } 

    public function shifttypeupdate(Request $request, $id)
    {
      Log::info('ShiftController@shifttypeupdate   input -  '.print_r($request->all(),true));
      Log::info('ShiftController@shifttypeupdate   id -  '.print_r($id,true));

    try{
           $rocode='';
         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
         }
         $type=$request->input('type');
         $start_time=$request->input('start_time');
         $end_time=$request->input('end_time');
         $shift_type=$request->input('shift_type');

         $Shifttype=ShiftType::where('RO_Code',$rocode)->where('id',$id)->first();
         $Shifttype1 = $Shifttype->type;


        if ($type != $Shifttype1) {
         
          $type = $request->validate([
          'type' => 'unique:ro_shift_config',
         ]);
        }

         if($shift_type=="flexi"){
          //check flex shift of ro
          $flexishift=DB::table('ro_shift_config')->where('RO_Code',$rocode)->where('shift_type',"flexi")->first();
             Log::info('ShiftController@shifttypeupdate   flexishift -  '.print_r($flexishift,true));
          if($flexishift){
            if($id!=$flexishift->id){


             $status=0;
             $request->session()->flash('success','Flexi shift Already Exist!!');

            return Back();
            }

          }

        }


        if(!empty($type) && !empty($start_time) || !empty($end_time)){

            $Shifttypess=ShiftType::find($id);
            $Shifttypess->type=$type;
            $Shifttypess->start_time=$start_time;
            $Shifttypess->end_time=$end_time;
            $Shifttypess->shift_type=$shift_type;
            $Shifttypess->save();

            $request->session()->flash('success','Update Successfully !!');
        }
            $Shifttype=ShiftType::where('RO_Code',$rocode)->get();

       }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
   
      }
        
      return redirect('/shifttype')->with('Shifttype');
    }


    function getshiftbyshiftmanager(Request $request){
       $id=$request->id;
       $data=[];
       $shifts=Shift::where('shift_manager',$id)->where('status',1)->get();

       foreach ($shifts as $shift) {
        $data[$shift->id]=date('d/m/Y  h:i:s A',strtotime($shift->transation_date))." To ".date('d/m/Y  h:i:s A',strtotime($shift->closer_date));
       }
       
      return response()->json($data);
    }
    
}
