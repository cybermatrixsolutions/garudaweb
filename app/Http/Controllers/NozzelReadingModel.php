<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NozzelReadingModel extends Model
{
    protected $table ='tbl_ro_nozzle_reading';
}
