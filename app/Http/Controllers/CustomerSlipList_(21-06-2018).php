<?php

namespace App\Http\Controllers;

//use App\CityController;
use Illuminate\Http\Request;
use App\RoPieceListManagement;
use App\TableStockItemMaster;
use App\RoMaster;
use App\RoCustomertManagement;
use App\RoLobSelectionModel;
use App\StockItemManagement;
use App\LOBMasterModel;
use App\AddPetrolDieselModel;
use App\TableItemPriceListModel; 
use App\TransactionModel;
use App\TransactionInvoiceModel;
use App\invoiceCustomer;
use App\customer_slips;
use Auth;
use Gate;
use App\Credit_limit;
use Carbon\Carbon;
use DB;
use App\OutletConfigModel;
class CustomerSlipList extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {      $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
          $customer_codedetals = customer_slips::where('item_type',1)->where('rocode',$rocode)->distinct()->get();
         
          $RoCustomertManagement= RoCustomertManagement::where('RO_code',$rocode)->get();
         
          
        return view('backend.slipcustomerlist',compact('customer_codedetals')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function requestFillter(Request $request)
{
      $customer_slip = customer_slips::get();
      $rocode='';
     if (Auth::user()->getRocode!=null) {
    $rocode=Auth::user()->getRocode->RO_code;
     }

  $customer_code=$request->input('customer_code');
  $from_date = $request->input('fromdate'); 
  $to_date = $request->input('to_dae'); 
  
  $from_dates = new Carbon(str_replace('/', '-',$from_date));
  $to_dates = new Carbon(str_replace('/', '-',$to_date));
  $from_dates = date('Y-m-d', strtotime($from_dates));
  $to_dates = date('Y-m-d', strtotime($to_dates));

    $customer_codedetals = customer_slips::where('customer_code',$customer_code);
  
   if(!empty($from_date)){
     $customer_codedetals = $customer_codedetals->where('request_date','>=',$from_dates);
   }
    
  if($to_dates!==null){
     $customer_codedetals = $customer_codedetals->where('request_date','<=',$to_dates);
    }
    $customer_codedetals->get();
  
     return view('backend.slipcustomerlist',compact('customer_codedetals','customer_slip')); 

  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Display the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function show(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,$id)
    {       $rocode='';
             if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
             }
           
           $RoPieceListManagement= RoPieceListManagement::where('RO_code',$rocode)->get();
           $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
        $getpric=  $getpric->pluck('item_id')->toArray();

        $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->whereIn('tbl_price_list_master.id',$getpric)->select('tbl_price_list_master.*');

        if(Auth::user()->user_type!=1 && $rocode!='')
                $fuel_type=$fuel_type->where('tbl_price_list_master.RO_Code',$rocode);

              $fuel_type=$fuel_type->get();


           $customer_slip = TransactionModel::find($id);

           return view('backend.transfueledit',compact('customer_slip','RoPieceListManagement','fuel_type')); 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
        $from_date=$request->input('request_date');
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));

        $customer_slip = TransactionModel::find($id);
        $customer_slip->item_code = $request->input('Fuel_Type');
        $customer_slip->customer_code = $request->input('customer_code');
        $customer_slip->Vehicle_Reg_No = $request->input('Vehicle_Reg_No');
        $customer_slip->item_price = $request->input('price');
        $customer_slip->request_date =$from_dates;
        $customer_slip->trans_date =$from_dates;
        $customer_slip->petroldiesel_qty=$request->input('petroldiesel_qty');
        $customer_slip->save();
          
        $request->session()->flash('success','Updated Successfully!!');
       


      return back();
    }  
    public function lubeedit(Request $request ,$id)
    {      
      


      $rocode='';
      if (Auth::user()->getRocode!=null) {
        $rocode=Auth::user()->getRocode->RO_code;
      }

      $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
      $getpric=  $getpric->pluck('item_id')->toArray();
      $item_id = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
      ->where('tbl_stock_item_group.Group_Name','!=', 'FUEL')->whereIn('tbl_price_list_master.id',$getpric)->select('tbl_price_list_master.*');

      if(Auth::user()->user_type!=1 && $rocode!='')
        $item_id=$item_id->where('tbl_price_list_master.RO_Code',$rocode);

      $item_id=$item_id->get();
      $customer_slip = TransactionModel::find($id);

      $request->session()->flash('success','Updated Successfully!!');

       return view('backend.lubeslipedit',compact('customer_slip','RoPieceListManagement','item_id')); 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function lubeupdate(Request $request, $id)
    {  
        $from_date=$request->input('request_date');

     $from_dates = new Carbon(str_replace('/', '-',$from_date));
  
       $from_dates = date('Y-m-d', strtotime($from_dates));
 
        $customer_slip = TransactionModel::find($id);
        $customer_slip->item_code = $request->input('Fuel_Type');
        $customer_slip->customer_code = $request->input('customer_code');
        $customer_slip->Vehicle_Reg_No = $request->input('Vehicle_Reg_No');
        $customer_slip->item_price = $request->input('price');
        $customer_slip->request_date =$from_dates;
        $customer_slip->petroldiesel_qty=$request->input('petroldiesel_qty');
        $customer_slip->save();
        $fuel_price = TableItemPriceListModel::where('item_id',$customer_slip->Fuel_Type)->where('is_active',1)->pluck('price')->first();
       
          $TransactionModel = TransactionModel::where('slip_id',$customer_slip->id)->update(['item_price'=>$fuel_price,
          'petroldiesel_qty'=>$customer_slip->petroldiesel_qty,'item_code'=>$customer_slip->Fuel_Type]);
            //$petroldiesel_qty =  $TransactionModel->petroldiesel_qty;
          
          



        
        $request->session()->flash('success','Updated Successfully!!');
       


      return back();
    }    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function getItemPrice(Request $request, $itemId)
    {

       $rocode='';

       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
        $from_date=$request->input('request_date');

        $from_dates = new Carbon(str_replace('/', '-',$from_date));
       
         $from_dates = date('Y-m-d', strtotime($from_dates));
         $to=$from_dates;
      
        $OutletConfigModel=OutletConfigModel::where('rocode',$rocode)->where('field_name','FUEL_PRICE_Date_time')->first();
        
        if($OutletConfigModel!=null)
        $roconfigTime=$OutletConfigModel->value;

       $TableItemPriceList=TableItemPriceListModel::where('item_id',$itemId)->whereDate('effective_date','>=',$from_dates.' 00:00:00')->whereDate('effective_date','<=',$from_dates.' 23:59:59')->get();
      
       if($TableItemPriceList->count()<=0 && $from_date!=null){
          return 0;
       }

        $TableItemPriceListModel=TableItemPriceListModel::where('item_id',$itemId);

        if($from_date!=null)
          $TableItemPriceListModel=$TableItemPriceListModel->whereDate('effective_date','>=',$to)->whereDate('effective_date','<=',date('Y-m-d h:i:s',strtotime($from_dates)+86390))->first();
        else
          $TableItemPriceListModel=$TableItemPriceListModel->where('is_active',1)->first();

        


        if($TableItemPriceListModel!=null){
              return $TableItemPriceListModel->price;
        }else{

          return 0;
        }
    }

    
}
