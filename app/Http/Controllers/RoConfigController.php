<?php

namespace App\Http\Controllers;

//use App\CityController;
use Illuminate\Http\Request;
use App\AdminconfigtModel;
use DB;
use App\User;
use Carbon\Carbon;
use Auth;
class RoConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
       
        $AdminconfigtModels = AdminconfigtModel::get();
       
     
        return view('backend.roconfig',compact('AdminconfigtModels')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
     public function create(Request $request)
    {
         $fields = $request->input('field');          
         foreach ($fields as $field) {
             $v=$request->input($field);
             
             $AdminconfigtModel = AdminconfigtModel::where('field_name',$field)->first();

            if($field=='FUEL_PRICE_Date_time'){
                   $date = new Carbon(str_replace('/', '-',$v));
                   $date =date('Y-m-d H:i:s',strtotime($date));
                   
                   $AdminconfigtModel->value =$date;
             }else{
                   $AdminconfigtModel->value =$v;
            }
           
             $AdminconfigtModel->save();
            }
          
          
          $request->session()->flash('success','Update Successfully!!'); 
        
         
      
          return back();
 
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function show(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function edit(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContributionModel $contributionModel)
    {
        //
    }
}
