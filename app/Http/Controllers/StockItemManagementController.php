<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockItemManagement;
use App\RoCustomertManagement;
use App\RoPieceListManagement;
use App\RoMaster;
use App\LOBMasterModel;
use App\Stock_item_group; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use File;
use DB;

class StockItemManagementController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
      $taxmaster = DB::table('tbl_tax_master')->get();
      $lob = DB::table('tbl_lob_master')->whereNotIn('lob_name',['FUELS','LUBES'])->orderBy('lob_name','asc')->get();

      $principle_company = DB::table('tbl_company_principal_master')->where('is_active', '>=',1)->orderBy('company_name', 'asc')->get();
      $stock_group = DB::table('tbl_stock_item_group')->whereNotIn('Group_Name',['FUEL','LUBES'])->where('is_active', '>=',1)->where('parent',0)->orderBy('Group_Name', 'asc')->get();
      $tock_groupfuel = DB::table('tbl_stock_item_group')->whereIn('Group_Name',['FUEL','LUBES'])->where('is_active', '>=',1)->where('parent',0)->orderBy('Group_Name', 'asc')->get();
      
       $unit_measure = DB::table('tbl_unit_of_measure')->where('is_active', '>=',1)->orderBy('Unit_Name', 'asc')->get();
      
      $stock_list = StockItemManagement::join('tbl_stock_item_group', 'tbl_stock_item_master.Stock_Group', '=', 'tbl_stock_item_group.id')
            ->join('tbl_unit_of_measure', 'tbl_stock_item_master.Unit_of_Measure', '=', 'tbl_unit_of_measure.id')
            ->join('tbl_company_principal_master', 'tbl_stock_item_master.Company_Code', '=', 'tbl_company_principal_master.company_code')
            ->select('tbl_stock_item_master.*', 'tbl_stock_item_group.Group_Name as Group_Name', 'tbl_unit_of_measure.Unit_Symbol as  Unit_Symbol','tbl_company_principal_master.company_name as company_name')->orderByRaw('tbl_stock_item_master.id desc')->get();

  //return view('backend.stockItemManagement');
    return view('backend.StockItemManagement',compact(['principle_company','stock_group','unit_measure','stock_list','taxmaster','lob','tock_groupfuel']));
     
    }

  public function getstocksub(Request $request){
 
       $stockgroup=$request->input('stockgroup');  
       $substock = DB::table('tbl_stock_item_group')->where('parent',$stockgroup)->orderBy('Group_Name', 'asc')->get();
       $substock=$substock->pluck('Group_Name','id')->toArray();
            
            return response()->json($substock);
  }

  public function getstocksubedit(Request $request){
 
       $stockgroup=$request->input('dtockgrpuco');  
       $substocks = DB::table('tbl_stock_item_group')->where('parent',$stockgroup)->orderBy('Group_Name', 'asc')->get();
       $substocks=$substocks->pluck('Group_Name','id')->toArray();
            
            return response()->json($substocks);
  }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $getlob = $request->input('Stock_Group');
        $LOBMasterModel= LOBMasterModel::where('stock_idg',$getlob)->first();
        $Company_Code = $request->input('Company_Code');

        $validatedData = $request->validate([

                 'Stock_Item_Code' => 'required|unique:tbl_stock_item_master',
            ]);

        try {
                if($LOBMasterModel!=null){

                $date                               = $request->input('Active_From_Date');
                $date1                              = str_replace('/', '-', $date);
                $Active_From_Date                   = date('Y-m-d', strtotime($date1));

                $stock_mangement                    = new StockItemManagement();
                $stock_mangement->Company_Code      = $request->input('Company_Code');
                $stock_mangement->Stock_Item_Code   = $request->input('Stock_Item_Code');
                $stock_mangement->Stock_Item_Name   = $request->input('Stock_Item_Name');
                $stock_mangement->Stock_Group       = $request->input('Sub_Stock_Group');
                $stock_mangement->Sub_Stock_Group   = $getlob;
                $stock_mangement->Unit_of_Measure   = $request->input('Unit_of_Measure');
                $stock_mangement->Active_From_Date  = $Active_From_Date;
                $stock_mangement->Tail_Unit         = $request->input('Tail_Unit');
                $stock_mangement->Conversion        = $request->input('Conversion');
                $stock_mangement->Alternate_Unit    = $request->input('Alternate_Unit');
                $stock_mangement->LOB               = $LOBMasterModel->lob_name;
                $stock_mangement->brand             = $request->input('brand');
                $stock_mangement->tax               = $request->input('tax');
                $stock_mangement->hsncode           = $request->input('hsncode');
                $stock_mangement->gst_vat           = $request->input('gst_vat');
                $stock_mangement->save();
                
                /**
                * check stock Company is GEN 
                */
                if($stock_mangement->Company_Code=='GEN')
                    $RoMasters = RoMaster::get();
                else
                    $RoMasters = RoMaster::where('company_code',$stock_mangement->Company_Code)->get();
  
                $item = $stock_mangement->Stock_Item_Code;

                foreach($RoMasters as $RoMaster)
                {
                    $lob = DB::table('tbl_lob_selection')->where('RO_code',$RoMaster->RO_code)->where('lob_name',$stock_mangement->LOB)->first();
                    
                    $RoPiecess=RoPieceListManagement::where('Item_Code',$stock_mangement->Stock_Item_Code)->where('RO_Code',$RoMaster->RO_code)->first();

                    if($lob!=null && $RoPiecess==null){

                        $personal_mangement                     = new RoPieceListManagement();
                        $personal_mangement->RO_Code            = $RoMaster->RO_code;
                        $personal_mangement->Item_code          = $stock_mangement->Stock_Item_Code;
                        $personal_mangement->Volume_ltr         = 1;            
                        $personal_mangement->Item_Name          = $stock_mangement->Stock_Item_Name;
                        $personal_mangement->Active_From_Date   = $Active_From_Date;
                        $personal_mangement->Stock_Group        = $stock_mangement->Stock_Group;
                        $personal_mangement->Sub_Stock_Group    = $stock_mangement->Sub_Stock_Group;
                        $personal_mangement->Unit_of_Measure    = $stock_mangement->Unit_of_Measure;
                        $personal_mangement->Tail_Unit          = $stock_mangement->Tail_Unit;
                        $personal_mangement->Conversion         = $stock_mangement->Conversion;
                        $personal_mangement->Alternate_Unit     = $stock_mangement->Alternate_Unit;
                        $personal_mangement->LOB                = $stock_mangement->LOB;
                        $personal_mangement->brand              = $stock_mangement->brand;
                        $personal_mangement->tax                   = $stock_mangement->tax;
                        $personal_mangement->hsncode               = $stock_mangement->hsncode;
                        $personal_mangement->gst_vat               = $stock_mangement->gst_vat;
                        $personal_mangement->save();
                    
                    }

                     
      
                 }
                 }else{
                    $request->session()->flash('success','Please Select Sub Group !!');
                    return back();
                  }
               
                $request->session()->flash('success','Added Successfully !!');
            }catch(\Illuminate\Database\QueryException $e){
                $request->session()->flash('success','Something wrong!!');  
            } 

            return redirect('stock_item_management');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $principle_company       = DB::table('tbl_company_principal_master')->orderBy('company_name', 'asc')->get();
        $stock_group             = DB::table('tbl_stock_item_group')->orderBy('Group_Name', 'asc')->get();
        $unit_measure            = DB::table('tbl_unit_of_measure')->get();
        $lob                     = DB::table('tbl_lob_master')->orderBy('lob_name', 'asc')->get();
        $personal_mangement_data = DB::table('tbl_stock_item_master')->where('id', '=', $id)->first();
        
        return view('backend.stock_item_management_view_page',compact(['principle_company','stock_group','unit_measure','personal_mangement_data','lob']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $principle_company       = DB::table('tbl_company_principal_master')->orderBy('company_name', 'asc')->get();
        $stock_group             = DB::table('tbl_stock_item_group')->where('parent',0)->orderBy('Group_Name', 'asc')->get();
        $unit_measure            = DB::table('tbl_unit_of_measure')->orderBy('Unit_Name', 'asc')->get();
        $taxmaster               = DB::table('tbl_tax_master')->get();
        $lob                     = DB::table('tbl_lob_master')->orderBy('lob_name', 'asc')->get();

        $personal_mangement_data = DB::table('tbl_stock_item_master')->where('id', '=', $id)->first();
        
        return view('backend.stock_item_management_edti_page',compact(['principle_company','stock_group','unit_measure','personal_mangement_data','taxmaster','lob']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
             $getlob = $request->input('Stock_Group');
            $LOBMasterModel= LOBMasterModel::where('stock_idg',$getlob)->first();
        try {

            $date                                  = $request->input('Active_From_Date');
            $date1                                 = str_replace('/', '-', $date);
            $Active_From_Date                      = date('Y-m-d', strtotime($date1));

            $stock_mangement                       = StockItemManagement::find($id);

            $stock_mangement->Stock_Group          = $request->input('Sub_Stock_Group');
            $stock_mangement->Sub_Stock_Group      = $getlob;
            $stock_mangement->Unit_of_Measure      = $request->input('Unit_of_Measure');
            $stock_mangement->Stock_Item_Name      = $request->input('Stock_Item_Name');
            $stock_mangement->Active_From_Date     = $Active_From_Date;
            $stock_mangement->Tail_Unit            = $request->input('Tail_Unit');
            $stock_mangement->Conversion           = $request->input('Conversion');
            $stock_mangement->Alternate_Unit       = $request->input('Alternate_Unit');
            $stock_mangement->LOB                  = $LOBMasterModel->lob_name;
            $stock_mangement->brand                = $request->input('brand');
            $stock_mangement->tax                  = $request->input('tax');
            $stock_mangement->hsncode              = $request->input('hsncode');
            $stock_mangement->gst_vat              = $request->input('gst_vat');
            $stock_mangement->save();
           
            $RoPieceListManagement = RoPieceListManagement::where('Item_Code',$stock_mangement->Stock_Item_Code)->update([
                                        'Item_Name'         =>$stock_mangement->Stock_Item_Name,
                                        'Active_From_Date'  =>$Active_From_Date,
                                        'Stock_Group'       =>$stock_mangement->Stock_Group,
                                        'Sub_Stock_Group'   =>$stock_mangement->Sub_Stock_Group,
                                        'Unit_of_Measure'   =>$stock_mangement->Unit_of_Measure,
                                        'Tail_Unit'         =>$stock_mangement->Tail_Unit,
                                        'Conversion'        =>$stock_mangement->Conversion,
                                        'Alternate_Unit'    =>$stock_mangement->Alternate_Unit,
                                        'brand'             =>$stock_mangement->brand,
                                        'hsncode'           =>$stock_mangement->hsncode,
                                        'gst_vat'           =>$stock_mangement->gst_vat,
                                       ]);
    

            $request->session()->flash('success','Data Updated Successfully!!');

        }catch(\Illuminate\Database\QueryException $e){
                
            $request->session()->flash('success','Something wrong!!');  
          }
        

            return redirect('stock_item_management');
    }

    function active(Request $request,$id, $actt)
    {
      
        
     $ac = StockItemManagement::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        try {
            $customer_mangement = StockItemManagement::find($id);

        $customer_mangement->delete();
        $request->session()->flash('success','Data has been deleted Successfully!!');
        
        } catch(\Illuminate\Database\QueryException $e)
            {
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                 return back();
            }
        

        return back();
    }
    public function emportstock(Request $request){
    $table = StockItemManagement::all();
    $filename = "tweets.csv";
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('Company_Code','Stock_Item_Code','Stock_Item_Name','Stock_Group','Sub_Stock_Group','Unit_of_Measure','Active_From_Date','Tail_Unit','Conversion','Alternate_Unit','brand','hsncode','gst_vat'));
    fputcsv($handle, array('IOCL', 'RLIANCEPETROLEUM
','RLIANCE','FUEL','Petrol','Liter','1/1/2017','23','23','12','Suziki','12','VAT'));

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response()->download($filename, 'stockitem.csv', $headers);

   } 

    public function stocksaveexport(Request $request){

    if (Input::hasFile('file'))
    {
        try{
                $file = Input::file('file');
                $name = time() . '-' . $file->getClientOriginalName();
                $path = storage_path('documents');
                $file->move($path, $name);
                $filename=base_path('/storage/documents/'.$name);
                //$file = fopen($filename, "r");
                $file=fopen($filename, 'r');

                while(! feof($file))
                {
                    $row= fgetcsv($file);
                    if($row){
                        $datas[]=$row;
                    }
                }
                
                    
                unset($datas[0]);
                             
                foreach ($datas as $data) {

                    $principlecheck = DB::table('tbl_company_principal_master')->where('company_code', '=',trim(str_replace('\r','',str_replace('\n','',$data[0]))))->first();
                    
                    if($principlecheck == null)
                    {
                       $request->session()->flash('success','Your Company  Code Not  Exit,Please Contact Admin'); 
                       return back();
                    }
                   $stocknamecode = StockItemManagement::where('Stock_Item_Code', '=', trim(str_replace('\r','',str_replace('\n','',$data[1]))))->get();

                   if($stocknamecode->count()>0)
                    {
                       $request->session()->flash('success','Stock Item Code Already Exit in Database'); 
                       return back();
                    }

                    $groupstock = DB::table('tbl_stock_item_group')->where('Group_Name', '=',trim(str_replace('\r','',str_replace('\n','',$data[3]))))->first();

                    if($groupstock == null)
                    {
                       $request->session()->flash('success','Group Name Not Exit in Database'); 
                       return back();
                    }

                    $subgroupstock = DB::table('tbl_stock_item_group')->where('Group_Name',trim(str_replace('\r','',str_replace('\n','',$data[4]))))->first();

                    if($subgroupstock == null)
                    {
                        $request->session()->flash('success','Sub Group Name Not Exit in Database');
                        return back();
                    }

                    if($groupstock!=null && $subgroupstock!=null ){

                        if($groupstock->id!=$subgroupstock->parent){
                          $request->session()->flash('success','Parent Group is not match of Sub Group ');
                          return back();
                        }

                      }
                   
                    $unitofmasurcheck = DB::table('tbl_unit_of_measure')->where('Unit_Symbol',trim(str_replace('\r','',str_replace('\n','',$data[5]))))->first();

                    if($unitofmasurcheck == null)
                    {
                       $request->session()->flash('success','Unit of Measure Not Exit in Database'); 
                       return back();
                    }

                    $lobmastercheck = DB::table('tbl_lob_master')->where('lob_name',trim(str_replace('\r','',str_replace('\n','',$data[4]))))->first();
                    
                    if($lobmastercheck == null)
                    {
                       $request->session()->flash('success','LOB  Not Exit in Database'); 
                       return back();
                    }
                   
                    $Stock= new StockItemManagement();
                    $Stock->Company_Code= $principlecheck->company_code;
                    $Stock->Stock_Item_Code= trim(str_replace('\r','',str_replace('\n','',$data[1])));
                    $Stock->Stock_Item_Name= trim(str_replace('\r','',str_replace('\n','',$data[2])));
                    $Stock->Stock_Group= $groupstock->id;
                    $Stock->Sub_Stock_Group= $subgroupstock->id;
                    $Stock->Unit_of_Measure= $unitofmasurcheck->id;
                    $date = $request->input('Active_From_Date');
                    $date1 = str_replace('/', '-', trim(str_replace('\r','',str_replace('\n','',$data[6]))));
                    $Active_From_Date = date('Y-m-d', strtotime($date1));
                    $Stock->Active_From_Date=  $Active_From_Date;

                    if(floatval(trim(str_replace('\r','',str_replace('\n','',$data[7]))))!=0)
                        $Stock->Tail_Unit= trim(str_replace('\r','',str_replace('\n','',$data[7])));

                    if(floatval(trim(str_replace('\r','',str_replace('\n','',$data[8]))))!=0)
                        $Stock->Conversion= trim(str_replace('\r','',str_replace('\n','',$data[8])));

                    if(floatval(trim(str_replace('\r','',str_replace('\n','',$data[9]))))!=0)
                        $Stock->Alternate_Unit= trim(str_replace('\r','',str_replace('\n','',$data[9])));

                    $Stock->brand= trim(str_replace('\r','',str_replace('\n','',$data[10])));
                    $Stock->LOB= $lobmastercheck->lob_name;
                    $Stock->hsncode= trim(str_replace('\r','',str_replace('\n','',$data[11])));
                    $Stock->gst_vat= trim(str_replace('\r','',str_replace('\n','',$data[12])));
                    $Stock->save();

                     if($Stock->Company_Code=='GEN')
                       $RoMasters = RoMaster::get();
                     else
                       $RoMasters = RoMaster::where('company_code',$Stock->Company_Code)->get();

                   
                    $item=$Stock->Stock_Item_Code;

                    foreach($RoMasters as $RoMaster)
                    {
                        $lob = DB::table('tbl_lob_selection')->where('RO_code',$RoMaster->RO_code)->where('lob_name',$Stock->LOB)->first();
                        $RoPiecess=RoPieceListManagement::where('Item_Code',$item)->where('RO_Code',$RoMaster->RO_code)->first();

                        if($lob!=null && $RoPiecess==null){
                        
                            $personal_mangement = new RoPieceListManagement();
                            $personal_mangement->RO_Code = $RoMaster->RO_code;
                            $personal_mangement->Item_code = $item;
                            $personal_mangement->Volume_ltr = 1;            
                            $personal_mangement->Item_Name = $Stock->Stock_Item_Name;
                            $personal_mangement->Active_From_Date= $Stock->Active_From_Date;
                            $personal_mangement->Stock_Group = $Stock->Stock_Group;
                            $personal_mangement->Sub_Stock_Group = $Stock->Sub_Stock_Group;
                            $personal_mangement->Unit_of_Measure = $Stock->Unit_of_Measure;
                            $personal_mangement->Tail_Unit = $Stock->Tail_Unit;
                            $personal_mangement->Conversion = $Stock->Conversion;
                            $personal_mangement->Alternate_Unit = $Stock->Alternate_Unit;
                            $personal_mangement->LOB = $Stock->LOB;
                            $personal_mangement->brand = $Stock->brand;
                            $personal_mangement->hsncode = $Stock->hsncode;
                            $personal_mangement->gst_vat = $Stock->gst_vat;
                            $personal_mangement->save();
                        
                        }
                    }
                 }
                
                $request->session()->flash('success','Data Import CSV  Successfully!!'); 
       
            }catch(\Illuminate\Database\QueryException $e){
                    
                $request->session()->flash('success','Something wrong!!');  
            }
        }
        
        return back();
   } 
}
