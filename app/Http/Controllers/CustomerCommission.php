<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use App\TransactionInvoiceModel;
use Auth;
use App\RoCustomertManagement;
use App\RoPieceListManagement;
use App\TransactionModel;
use App\invoiceCustomer;
use App\ItemServicesModel;
use App\Credit_limit;
use App\CommissionInvoice;
use Carbon\Carbon;
use App\MastorRO;
use Log;
class CustomerCommission extends Controller
{
	/**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */



  public function index(Request $request)
    {

           $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $tbl_customers =  DB::table('tbl_customer_master')->where('RO_code',$rocode)->get();
        
        $commisioninvoices=CommissionInvoice::where('Ro_code',$rocode)->get();
       
        //dd($tbl_customer_transaction);
       return view('backend.commisionView',compact('commisioninvoices'));
        
    }

     public function craete(Request $request)
    {

           $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $tbl_customers =  DB::table('tbl_customer_master')->where('RO_code',$rocode)->get();
        
        $Customerinvoices=invoiceCustomer::where('RO_code',$rocode)->get();
        $CustomerCommission=ItemServicesModel::where('RO_code',$rocode)->get();
        //dd($tbl_customer_transaction);
       return view('backend.customer_commision',compact('Customerinvoices','tbl_customers','CustomerCommission'));
        
    }

    
    public function indexpdf(Request $request)
    {
      {   
         // the amount of ids
          
          if (date('m') <= 3) {
                $year = date('y')-1;
            } else {
                $year = date('y');
            }

   try{ 
        $com_invoice_no=1;
        $rocode='';
        $invoice_no='';
        $rocode='';
        $nom=5;
        $l=0;
        $invoice_prefix='';
         if (Auth::user()->getRocode!=null) {
            $com_invoice_no=Auth::user()->getRocode->com_invoice_no;
            $rocode=Auth::user()->getRocode->RO_code;
            $invoice_prefix=Auth::user()->getRocode->GST_prefix;
            $com_invoice_no=$com_invoice_no+1;
            $invoice_no=$com_invoice_no;

         }

         $l=mb_strlen($invoice_no);
         $nom=$nom-$l;
         for ($i=1; $i <=$nom ; $i++) { 
              $invoice_no='0'.$invoice_no;
         }

          $invoice_no=$invoice_prefix.'-'.$invoice_no.'-'.$year;
         
         $handling_fee=0;
         $customer_code=$request->input('customer_code');
         $invoice=$request->input('invoicecheck');
         $fule_type = $request->input('fule_type');

        if($fule_type==1)
         $sevice=ItemServicesModel::where('RO_code',$rocode)->where('tax_type','VAT')->first();
        else
          $sevice=ItemServicesModel::where('RO_code',$rocode)->where('tax_type','GST')->first();
         

         $credit_limit=Credit_limit::where('Ro_code',$rocode)->where('Customer_Code',$customer_code)->latest()->first();
        // dd($sevice->gettax);
         if($credit_limit!=null)
         $handling_fee=$credit_limit->handling_fee;

        Log::info('invoice===='.print_r($invoice,true));
         if(!$invoice)
         {

     
          return back();
         } else{
               if(!$invoice&&count($invoice)<=0)
                     return back();


         }

        $invoiceCustomers=invoiceCustomer::whereIn('id',$invoice)->where('status',0)->get();
        
        if($invoiceCustomers->count()==0){
           return back();
        }
        
        $tbl_invoice  =  RoCustomertManagement::where('Customer_Code',$customer_code)->first();
        
   
            $CommissionInvoice= new CommissionInvoice();
            $CommissionInvoice->customer_code=$customer_code;
            $CommissionInvoice->pfdpath='commission/'.$invoice_no;
            $CommissionInvoice->cus_invoice_no=$invoice_no;
            $CommissionInvoice->Ro_code=$rocode;
            $CommissionInvoice->tax_type=$fule_type;
            //$CommissionInvoice->amount=$amount;
            $CommissionInvoice->save();
            MastorRO::where('RO_code',$rocode)->update(['com_invoice_no' => $com_invoice_no]); 

           //invoiceCustomer::whereIn('id',$invoice)->update(['status'=>1]);

           PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
          
           $customPaper = array(0,0,900,900);
             //return View('backend.customerview',compact('handling_fee','sevice','invoice_no','tbl_invoice','invoiceCustomers','fule_type'));
            

            $pdf = PDF::loadView('backend.customerview',compact('handling_fee','sevice','invoice_no','tbl_invoice','invoiceCustomers','fule_type'))->setPaper($customPaper, 'landscape')->save(storage_path().'/commission/'.$invoice_no.'.pdf');
            return response()->download(storage_path().'/commission/'.$invoice_no.'.pdf');
           
      }
       catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something is wrong');
                
        } 


        return back();

    }

    }
   /* public function pdfviewcustomer(Request $request)
    {
        $users = DB::table("users")->get();
        view()->share('users',$users);

        if($request->has('download')){
          // Set extra option
          PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
          // pass view file
          $customPaper = array(0,0,960,960);

            //$pdf = PDF::loadView('backend.customerview')->setPaper($customPaper, 'landscape');
            // download pdf
            //return $pdf->download('pdfview.pdf');
        }
        return view('backend.customerview',compact(''));
    }*/
  

  /**
   *
   * get cuustomer invoice
   *
   */
  public function getinvoice($cutomer,$fule_type)
  {
    $str='';
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

       $Customerinvoices=invoiceCustomer::where('RO_code',$rocode)->where('Customer_name',$cutomer)->where('tax_type',$fule_type)->where('status',0)->get();
       $i=1;

        if($Customerinvoices->count()>0){

          foreach($Customerinvoices as $invoice){

               $str.='<tr>
                 
                  <td>
                    <input type="checkbox" name="invoicecheck[]" value="'.$invoice->id.'"/></td>
                  <td scope="row">'.$invoice->invoice_no.'</td>
                  <td scope="row">'.date('d/m/Y',strtotime($invoice->created_at)).'</td>
                  </tr>';
                $i++;

          }
        }else{
            $str.='<tr>
                    NO Record Found
                   </tr>';
                $i++;
        }
                
              
             
     return $str;
  }

   public function slipPendingPrinting()
    {
           $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
        $type=0;
         $tbl_customer_transaction =  DB::table('tbl_customer_master')->where('is_active',1)->where('RO_code',$rocode)->get();
         if(count($tbl_customer_transaction)==null){
   return back();
  }
         $cus=$tbl_customer_transaction[0]->Customer_Code;
        $tbl_customer_transaction =  DB::table('tbl_customer_master')->where('RO_code',$rocode)->get();
        $customer=$cus;
        $Customerinvoices=CommissionInvoice::where('RO_code',$rocode)->where('settlement_status',0)->get();
        //dd($tbl_customer_transaction);
       return view('backend.creditLimit.commition.settlement',compact('Customerinvoices','tbl_customer_transaction','type','customer'));
   
    }

   public function invoicePendingCollectionFillter(Request $request)
    {
           $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
        $customer_code=$request->input('RO_code');
        $from_date = $request->input('fromdate'); 
        $to_date = $request->input('to_dae'); 
        $Settlement=$request->input('Settlement');
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));
       $type=$Settlement;
       $customer=$customer_code;

        $tbl_customer_transaction =  DB::table('tbl_customer_master')->where('RO_code',$rocode)->get();
        $Customerinvoices=CommissionInvoice::where('RO_code',$rocode)
        ->where('settlement_status',$Settlement)
        ->where('customer_code',$customer_code)
        ->whereBetween('created_at', [$from_dates." 00:00:00", $to_dates." 23:59:59"])
        ->get();
        //dd($tbl_customer_transaction);
       return view('backend.creditLimit.commition.settlement',compact('Customerinvoices','tbl_customer_transaction','type','customer'));
   
    }


    public function settled($id)
    {   
        $invoiceCustomer=CommissionInvoice::where('id',$id)->first();
        return view('backend.creditLimit.commition.settled',compact('invoiceCustomer'));
    }

    
    public function settledUdate(Request $request, $id)
    {  
    
       $invoiceCustomer=CommissionInvoice::where('id',$id)->first();
       $payment_mode=$request->input('payment_mode');
       $bank_name=$request->input('bank_name');
       $checque_no=$request->input('checque_no');
       $settlement_date=new Carbon(str_replace('/', '-',$request->input('payment_date')));
       $checque_date=new Carbon(str_replace('/', '-',$request->input('checque_date')));

       $invoiceCustomer->settlement_date=$settlement_date;
       $invoiceCustomer->payment_mode=$payment_mode;
       $invoiceCustomer->settlement_status=1;
       
       if($payment_mode='NEFT'){
         $invoiceCustomer->bank_name=$bank_name;
       }

       if($payment_mode='Cheque'){
         $invoiceCustomer->checque_no=$checque_no;
         $invoiceCustomer->checque_date=$checque_date;
       }

       $invoiceCustomer->save();

      return redirect('slipPendingPrinting');
   
    }

    public function viewSettled($id)
    {
       $invoiceCustomer=CommissionInvoice::where('id',$id)->first();
       return view('backend.creditLimit.commition.viewSettled',compact('invoiceCustomer'));
   
    }

    
}