<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoPersonalManagement;
use App\PaymentModel;
use App\Shift;
use Auth;

use App\QR_Code;
use App\MastorRO;
use App\CityModel;
use App\StateManagementModel;
use App\Principal;

class ReprintController extends Controller
{
    //

    /**
    * Reprint print settlement
    */
    public function settlement(){
      
         $id=0;
        $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }

        }
        $allshifts=Shift::where('ro_code',$RO_Code)->where('is_active',0)->where('status',1)->where('fuel',1)->whereNotNull('closer_date')->get();
        $salesmanger=[];
        if($allshifts!=null)
          foreach ($allshifts as $allshift) {
               array_push($salesmanger,$allshift->shift_manager);
           }
        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('Designation',29)->whereIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();

// dd($shiftmanager);
        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->where('IsActive','1')->orderby('order_number','ASC')->orderby('name','ASC')->get();
      return view('backend.reprint.settlement.settlement',compact('PaymentModel','shiftmanager'));
    }

     /**
    * Reprint print settlement
    */
    public function settlementReprint(Request $request){
      
         $id=$request->siftname;
         $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }

        }

        // $Customer= QR_Code::where('id',$id)->first();
        $detail = MastorRO::join('tbl_customer_master','tbl_ro_master.RO_code','=','tbl_customer_master.RO_code')
        ->where('tbl_ro_master.RO_code',$RO_Code)
        ->select('tbl_ro_master.pump_legal_name as pump_name','tbl_ro_master.pump_address as pump_addetail','tbl_ro_master.city as citys','tbl_ro_master.state as states','tbl_ro_master.image as images','tbl_ro_master.customer_care','tbl_customer_master.Company_name as company_name','tbl_customer_master.Department as department','tbl_ro_master.RO_code as Ro_code')
        ->first();
        
        // dd($detail);
         $CityModel=CityModel::where('id',$detail->citys)->first();
         $StateManagementModel=StateManagementModel::where('id',$detail->states)->first();
         $MastorRO=MastorRO::where('RO_code',$detail->Ro_code)->first();
         $Principal=Principal::where('company_code',$MastorRO->company_code)->first();

        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->where('IsActive','1')->orderby('order_number','ASC')->orderby('name','ASC')->get();
        $shift=Shift::find($id);
      return view('backend.reprint.settlement.reprint',compact('shift','PaymentModel','Customer','detail','CityModel','StateManagementModel','Principal','MastorRO'));
    }



    

    function getshiftbyshiftmanager(Request $request){
       $id=$request->id;
       $data=[];
       $shifts=Shift::where('shift_manager',$id)->where('status',1)->get();

       foreach ($shifts as $shift) {
        $data[$shift->id]=date('d/m/Y  h:i:s A',strtotime($shift->created_at))." To ".date('d/m/Y  h:i:s A',strtotime($shift->closer_date));
       }
       
      return response()->json($data);
    }
}
