<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoPersonalManagement;
use App\PaymentModel;
use App\Shift;
use Auth;

class ReprintController extends Controller
{
    //

    /**
    * Reprint print settlement
    */
    public function settlement(){
      
         $id=0;
        $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }

        }
        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('Designation',29)->orderBy('Personnel_Name', 'asc')->get();
        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->where('IsActive','1')->orderby('order_number','ASC')->orderby('name','ASC')->get();
      return view('backend.reprint.settlement.settlement',compact('PaymentModel','shiftmanager'));
    }

     /**
    * Reprint print settlement
    */
    public function settlementReprint(Request $request){
      
         $id=$request->siftname;
         $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }

        }
        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->where('IsActive','1')->orderby('order_number','ASC')->orderby('name','ASC')->get();
        $shift=Shift::find($id);
      return view('backend.reprint.settlement.reprint',compact('shift','PaymentModel'));
    }



    

    function getshiftbyshiftmanager(Request $request){
       $id=$request->id;
       $data=[];
       $shifts=Shift::where('shift_manager',$id)->where('status',1)->get();

       foreach ($shifts as $shift) {
        $data[$shift->id]=date('d/m/Y  h:i:s A',strtotime($shift->created_at))." To ".date('d/m/Y  h:i:s A',strtotime($shift->closer_date));
       }
       
      return response()->json($data);
    }
}
