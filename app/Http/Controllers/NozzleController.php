<?php

namespace App\Http\Controllers;

use App\Nozzle;
use App\TableTankMasterModel;
use App\RoPieceListManagement;
use Illuminate\Http\Request;

class NozzleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Nozzle  $nozzle
     * @return \Illuminate\Http\Response
     */
    public function show(Nozzle $nozzle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Nozzle  $nozzle
     * @return \Illuminate\Http\Response
     */
    public function edit(Nozzle $nozzle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nozzle  $nozzle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Nozzle $nozzle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nozzle  $nozzle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nozzle $nozzle)
    {
        //
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nozzle  $nozzle
     * @return \Illuminate\Http\Response
     */
    public function getTankfuel($id)
    {
         $TableTankMasterModel=TableTankMasterModel::find($id);
         return $TableTankMasterModel->fuel_type;
    }
     public function getTankname($id)
    {
         $TableTankMasterModel=TableTankMasterModel::find($id);
         $RoPieceListManagement = RoPieceListManagement::where('id',$TableTankMasterModel->fuel_type)->first();
         return $RoPieceListManagement->Item_Name;
    }
    

}
