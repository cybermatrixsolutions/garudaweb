<?php

namespace App\Http\Controllers;

//use App\CityController;
use Illuminate\Http\Request;
use App\PaymentModel;
use App\MasterPaymentmode;
use App\Tpl_tally_config;
use DB;
use App\User;
use App\tax_master;
use App\Stock_item_group;
use Auth;
use Log;
class PaymentModeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
        $PaymentModel = PaymentModel::where('ro_code',$rocode)->orderby('order_number','ASC')->orderby('name','ASC')->get();
       // $checkpayment = PaymentModel::where('ro_code',$rocode)->first();

        if($PaymentModel->count()>0)
          $MasterPayment = MasterPaymentmode::whereNotIn('name',$PaymentModel->pluck('name')->toArray())->orderby('id','desc')->get();
        else
          $MasterPayment = MasterPaymentmode::orderby('id','desc')->get();



        $checkmaster = MasterPaymentmode::first();
         //  dd($StateManagementModel);
        return view('backend.paymentmood',compact('PaymentModel','MasterPayment','checkmaster','checkpayment')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create(Request $request)
    {
       try{
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
         
          $paymentmode=$request->input('name');  
          // $tally_config_name=$request->input('tally_config_name');  
          /*$validatedData = $request->validate([
          'name' => 'required|unique:tpl_payment_mode',

         ]);*/
         $checkpaymentmode=DB::table('tpl_payment_mode')->where('name',$paymentmode)->where('ro_code',$rocode)->first();
         if($checkpaymentmode){

          $request->session()->flash('success','Payment Mode Already Exist!!'); 
           return back();
         }

          $PaymentModel = new PaymentModel();
          $PaymentModel->name =$paymentmode;
          // $PaymentModel->tally_config_name =$tally_config_name;
          $PaymentModel->ro_code=$rocode;
          $PaymentModel->mode_type=$request->mode_type;
          $PaymentModel->save();
          $request->session()->flash('success','Added Successfully!!'); 
         }
         catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
 
    }
    public function masterpaymentsave(Request $request){

      try{
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
         
          $paymentmode=$request->input('names');  
           
           
          if(!isset($paymentmode) || $paymentmode==null || count($paymentmode)==0){
            $request->session()->flash('success','Please select payment mode');
            return back();
            }

      $paymentmodes=MasterPaymentmode::whereIn('name',$paymentmode)->get();
    
      foreach ($paymentmodes as $key => $value) {
      
        $PaymentModel = new PaymentModel();
        $PaymentModel->name =$value->name;
        $PaymentModel->ro_code=$rocode;
        $PaymentModel->mode_type=$value->mode_type;
        $PaymentModel->save();
        $request->session()->flash('success','Added Successfully!!'); 
      }
     }
         catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
    }

   public function activePaymentMode(Request $request, $id, $act){
     $paymentMode=PaymentModel::find($id);

     $paymentMode->IsActive=$act;
     $paymentMode->save();

     if($act==1)
      $request->session()->flash('success','Activated Successfully');
    else
      $request->session()->flash('success','DeActivated Successfully');

    return back();
   }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeupdate(Request $request , $id)
    {
       try{
          $paytypecreate=$request->input('name');  
          $validatedData = $request->validate([
          'name' => 'required|unique:tpl_payment_mode,id,'.$id,
         ]);
          PaymentModel::where('id', $id)->update(['name' =>$paytypecreate]);
          $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
      

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function show(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function editmethod(Request $request)
    {
         $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $PaymentModels = PaymentModel::where('ro_code',$rocode)->orderby('order_number','ASC')->orderby('name','ASC')->get();

        $Config_header_payment_check = Tpl_tally_config::where('Config_header','Payment Mode')->where('RO_code',$rocode)->where('Config_name','Daily Shift Settlement')->get();
       // dd($Config_header_payment_check);

        $Config_header_other_check = Tpl_tally_config::where('Config_header','Others')->where('RO_code',$rocode)->where('Config_name','Daily Shift Settlement')->get();

        $Other = array('Rounded Off','Voucher Type');


          $check_creditsales_record = Tpl_tally_config::where('Config_header','Payment Mode')
                                           ->where('RO_code',$rocode)
                                          ->where('Garruda_name', 'Credit-Sale')
                                           ->where('Config_name','Daily Shift Settlement')
                                           ->value('Garruda_name');
  

 if(!$check_creditsales_record){



                 $Tpl_tally_config = new Tpl_tally_config();
                $Tpl_tally_config->Config_header ="Payment Mode";
                $Tpl_tally_config->RO_code =$rocode;
                $Tpl_tally_config->Config_name="Daily Shift Settlement";
                $Tpl_tally_config->Garruda_name = "Credit-Sale";
                $Tpl_tally_config->save();





}
              



        if ($PaymentModels->count()>0) {
            foreach ($PaymentModels as $key => $PaymentModel) {
                if ($Config_header_payment_check->count()==0) {
                $Tpl_tally_config = new Tpl_tally_config();
                $Tpl_tally_config->Config_header ="Payment Mode";
                $Tpl_tally_config->RO_code =$rocode;
                $Tpl_tally_config->Config_name="Daily Shift Settlement";
                $Tpl_tally_config->Garruda_name = $PaymentModel->name;
                $Tpl_tally_config->save();
              }else{


foreach ($Config_header_payment_check as $key => $paymentcheck) {
 


                if ($PaymentModel->Group_name==$paymentcheck->Garruda_name) {
                     $Tpl_tally_config = Tpl_tally_config::where('Config_header','Payment Mode')->where('Config_name','Daily Shift Settlement')->where('RO_code',$rocode)->where('Garruda_name',$paymentcheck->Garruda_name)->first();
                  $Tpl_tally_config->Garruda_name = $PaymentModel->name;
                  $Tpl_tally_config->save();

                 }


              }
            }
          }
       }


          if ($Config_header_other_check->count()==0) {
            foreach ($Other as $value) {
           
              $Tpl_tally_config = new Tpl_tally_config();
              $Tpl_tally_config->Config_header ="Others";
              $Tpl_tally_config->RO_code =$rocode;
              $Tpl_tally_config->Config_name="Daily Shift Settlement";
              $Tpl_tally_config->Garruda_name = $value;
              $Tpl_tally_config->save();
            }
              
               }else{
                  foreach ($Other as $value) {
                  $Tpl_tally_config = Tpl_tally_config::where('Config_header', 'Others')->where('Config_name','Daily Shift Settlement')->where('RO_code',$rocode)->where('Garruda_name', $value)->first();
                  $Tpl_tally_config->Garruda_name = $value;
                  $Tpl_tally_config->save();
                     }
              }

         $Tally_Model_check = Tpl_tally_config::where('ro_code',$rocode)->get();
            
          $Tally_Model = Tpl_tally_config::where('ro_code',$rocode)->where('Config_name','Daily Shift Settlement')->orderByRaw('FIELD(Config_header, "Payment Mode", "Sales", "Taxes", "Others")')->get();

        return view('backend.tallyConfig.dailyshiftsettlement',compact('Tally_Model','Tally_Model_check','MasterPayment','checkpayment')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function updatemethod(Request $request)
    {
          try{
         $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }
      
          $paytypecreate=$request->input('Garruda_name');  
          $Tally_name=$request->input('Tally_name');  
          $id=$request->input('id');  

           foreach ($Tally_name as $key => $tally_names) {
          
           
         $paymentss = Tpl_tally_config::where('RO_code', $rocode)->where('id',$id[$key])->update(['Tally_name' =>$tally_names]);

          }


           
          $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContributionModel $contributionModel)
    {
        //
    }


    public function edittally(Request $request)
    {
      try{
         $rocode='';

       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;

         $state="";
         if(! empty(Auth::user()->customer->getro->getstate->statecode)){

          $state = Auth::user()->customer->getro->getstate->statecode;
       
           }
         }

         


        $Config_header_sales_check = Tpl_tally_config::where('Config_header','Sales')->where('Config_name','Daily Fuel Sales')->where('RO_code',$rocode)->get();
        $Config_header_tax_check = Tpl_tally_config::where('Config_header','Taxes')->where('Config_name','Daily Fuel Sales')->where('RO_code',$rocode)->get();
        $Config_header_customer_check = Tpl_tally_config::where('Config_header','Customer')->where('Config_name','Daily Fuel Sales')->where('RO_code',$rocode)->get();
        $Config_header_other_check = Tpl_tally_config::where('Config_header','Others')->where('Config_name','Daily Fuel Sales')->where('RO_code',$rocode)->get();
        $Other = array('Rounded Off','Voucher Type');
       Log::info('state  - '.$state);
         $tax_master_state = tax_master::where('Tax_Code', 'like', '%' . $state . '%')->where('GST_Type', 'VAT')->get();

          // $Sales_master_name = Stock_item_group::where('parent', '=', 68)->groupBy('Group_name')->get(['Group_name']);

        $Sales_master_name = Stock_item_group::join('tbl_price_list_master','tbl_price_list_master.Stock_Group','=','tbl_stock_item_group.id')
          ->where('tbl_stock_item_group.Group_Name','=','FUEL')
          ->where('tbl_price_list_master.RO_Code','=',$rocode)
          ->selectRaw('tbl_price_list_master.Item_Name')
          ->get();

          if ($Sales_master_name->count()>0) {
            foreach ($Sales_master_name as $key => $Sales_master_names) {
              if ($Config_header_sales_check->count()==0) {
                $PaymentModels = new Tpl_tally_config();
                $PaymentModels->Config_header ="Sales";
                $PaymentModels->RO_code =$rocode;
                $PaymentModels->Config_name="Daily Fuel Sales";
                $PaymentModels->Garruda_name = $Sales_master_names->Item_Name;
                $PaymentModels->save();
              }else{
                      foreach($Config_header_sales_check as $salescheck){


            
                        if ($Sales_master_names->Item_Name==$salescheck->Garruda_name) {
                        $PaymentModels = Tpl_tally_config::where('Config_header','Sales')->where('Config_name','Daily Fuel Sales')->where('RO_code',$rocode)->where('Garruda_name',$salescheck->Garruda_name)->first();
                        $PaymentModels->Garruda_name = $Sales_master_names->Item_Name;
                        $PaymentModels->save();
                        } 


                      }


              }
            }
          }
Log::info('PaymentModeController@edittally  - tax_master_state'.print_r($tax_master_state,true));


Log::info('PaymentModeController@edittally  - Config_header_tax_check'.print_r($Config_header_tax_check,true));
           if ($tax_master_state->count()>0) {
            foreach ($tax_master_state as $key => $tax_master_states) {
                if ($Config_header_tax_check->count()==0) {
                $PaymentModels = new Tpl_tally_config();
                $PaymentModels->Config_header ="Taxes";
                $PaymentModels->RO_code =$rocode;
                $PaymentModels->Config_name="Daily Fuel Sales";
                $PaymentModels->Garruda_name = $tax_master_states->Description;
                $PaymentModels->save();
              }else{

                 foreach($Config_header_tax_check as $taxcheck){



                if ($tax_master_states->Group_name==$taxcheck->Garruda_name) {
                     $PaymentModels = Tpl_tally_config::where('Config_header','Taxes')->where('Config_name','Daily Fuel Sales')->where('RO_code',$rocode)->where('Garruda_name',$taxcheck->Garruda_name)->first();
                  $PaymentModels->Garruda_name = $tax_master_states->Group_name;
                  $PaymentModels->save();

                }


                }


              }
            }
          }
          
          if ($Config_header_customer_check->count()==0) {
          $PaymentModels = new Tpl_tally_config();
          $PaymentModels->RO_code =$rocode;
          $PaymentModels->Config_name="Daily Fuel Sales";
          $PaymentModels->Config_header ="Customer";
          $PaymentModels->Garruda_name ="Fuel Sales Control A/c";
          $PaymentModels->save();
        }else{
                  $PaymentModels = Tpl_tally_config::where('Config_header', 'Customer')->where('Config_name','Daily Fuel Sales')->where('RO_code',$rocode)->where('Garruda_name', 'Fuel Sales Control A/c')->first();
                  $PaymentModels->Garruda_name = 'Fuel Sales Control A/c';
                  $PaymentModels->save();
              }
        if ($Config_header_other_check->count()==0) {
            foreach ($Other as $value) {
             
              $PaymentModels = new Tpl_tally_config();
              $PaymentModels->Config_header ="Others";
              $PaymentModels->RO_code =$rocode;
              $PaymentModels->Config_name="Daily Fuel Sales";
              $PaymentModels->Garruda_name = $value;
              $PaymentModels->save();
            }
              
               }else{
                  foreach ($Other as $value) {
                  $PaymentModels = Tpl_tally_config::where('Config_header', 'Others')->where('Config_name','Daily Fuel Sales')->where('RO_code',$rocode)->where('Garruda_name', $value)->first();
                  $PaymentModels->Garruda_name = $value;
                  $PaymentModels->save();
                 }
            }
        
            $Tally_Model_check = Tpl_tally_config::where('ro_code',$rocode)->get();
            
            $Tally_Model = Tpl_tally_config::where('ro_code',$rocode)->where('Config_name','Daily Fuel Sales')->orderByRaw('FIELD(Config_header, "Customer", "Sales", "Taxes", "Others")')->get();
     
     
            // $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return view('backend.tallyConfig.fuel',compact('Tally_Model','Tally_Model_check','MasterPayment','checkpayment')); 

    }


    public function updatetally(Request $request)
    {
          try{
         $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }

          $paytypecreate=$request->input('Garruda_name');  
          $Tally_name=$request->input('Tally_name');  
          $id=$request->input('id');  

           foreach ($Tally_name as $key => $tally_names) {
          
           
         $paymentss = Tpl_tally_config::where('RO_code', $rocode)->where('id',$id[$key])->update(['Tally_name' =>$tally_names]);

          }


           
          $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
      
    }
  
        /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function edittallyGST(Request $request)
    {
            try{
         $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
          
         $state="";
         if(! empty(Auth::user()->customer->getro->getstate->statecode)){


          $state = Auth::user()->customer->getro->getstate->statecode;
           }
       }
         $val  = array(73,67);
        $Config_header_sales_check = Tpl_tally_config::where('Config_header','Sales')->where('RO_code',$rocode)->where('Config_name','Gst Slip Invoices')->get();
        $Config_header_location_check = Tpl_tally_config::where('Config_header','Location')->where('RO_code',$rocode)->where('Config_name','Gst Slip Invoices')->get();
        $Config_header_tax_check = Tpl_tally_config::where('Config_header','Taxes')->where('RO_code',$rocode)->where('Config_name','Gst Slip Invoices')->get();
        $Config_header_customer_check = Tpl_tally_config::where('Config_header','WALKIN Customer')->where('RO_code',$rocode)->where('Config_name','Gst Slip Invoices')->get();
        $Config_header_other_check = Tpl_tally_config::where('Config_header','Others')->where('RO_code',$rocode)->where('Config_name','Gst Slip Invoices ')->get();
        $Other = array('Rounded Off','Voucher Type');
        $locations = array('Storage location lube','Storage location others');
       
         $tax_master_state = tax_master::where('Tax_Code', 'like', '%' . $state . '%')->where('GST_Type','!=', 'VAT')->get();

          $Sales_master_name = Stock_item_group::whereIn('parent', $val)->groupBy('Group_name')->get(['Group_name']);
        // dd($tax_master_state);

      
          if ($Sales_master_name->count()>0) {
            foreach ($Sales_master_name as $key => $Sales_master_names) {
              if ($Config_header_sales_check->count()==0) {
                $PaymentModels = new Tpl_tally_config();
                $PaymentModels->Config_header ="Sales";
                $PaymentModels->RO_code =$rocode;
                $PaymentModels->Config_name="Gst Slip Invoices";
                $PaymentModels->Garruda_name = $Sales_master_names->Group_name;
                $PaymentModels->save();
              }else{

                  foreach ($Config_header_sales_check as $key => $salescheck) {
                      if ($Sales_master_names->Group_name==$salescheck->Garruda_name) {
                      $PaymentModels = Tpl_tally_config::where('Config_header','Sales')->where('Config_name','Gst Slip Invoices')->where('RO_code',$rocode)->where('Garruda_name',$salescheck->Garruda_name)->first();
                      $PaymentModels->Garruda_name = $Sales_master_names->Group_name;
                      $PaymentModels->save();
                     } 
                  }
                }
              }
           }

           if ($tax_master_state->count()>0) {
            foreach ($tax_master_state as $key => $tax_master_states) {
                if ($Config_header_tax_check->count()==0) {
                $PaymentModels = new Tpl_tally_config();
                $PaymentModels->Config_header ="Taxes";
                $PaymentModels->RO_code =$rocode;
                $PaymentModels->Config_name="Gst Slip Invoices";
                $PaymentModels->Garruda_name = $tax_master_states->Description;
                $PaymentModels->save();
              }else{
                
                foreach ($Config_header_tax_check as $key => $taxcheck) {
                  if($tax_master_states->Group_name==$taxcheck->Garruda_name) {
                     $PaymentModels = Tpl_tally_config::where('Config_header','Taxes')->where('Config_name','Gst Slip Invoices')->where('RO_code',$rocode)->where('Garruda_name',$taxcheck->Garruda_name)
                     ->first();
                  $PaymentModels->Garruda_name = $tax_master_states->Group_name;
                  $PaymentModels->save();

                 }
                }
       
              }
            }
          }
          
          if ($Config_header_customer_check->count()==0) {
          $PaymentModels = new Tpl_tally_config();
          $PaymentModels->RO_code =$rocode;
          $PaymentModels->Config_name="Gst Slip Invoices";
          $PaymentModels->Config_header ="WALKIN Customer";
          $PaymentModels->Garruda_name ="Other Sales Control A/c";
          $PaymentModels->save();
        }else{
                  $PaymentModels = Tpl_tally_config::where('Config_header', 'WALKIN Customer')->where('Config_name','Gst Slip Invoices')->where('RO_code',$rocode)->where('Garruda_name', 'Other Sales Control A/c')->first();
                  $PaymentModels->Garruda_name = 'Other Sales Control A/c';
                  $PaymentModels->save();
              }

        if ($Config_header_location_check->count()==0) {
          foreach ($locations as $location) {
          $PaymentModels = new Tpl_tally_config();
          $PaymentModels->RO_code =$rocode;
          $PaymentModels->Config_name="Gst Slip Invoices";
          $PaymentModels->Config_header ="Location";
          $PaymentModels->Garruda_name = $location;
          $PaymentModels->save();
          }
        }else{
          foreach ($locations as $location) {
                  $PaymentModels = Tpl_tally_config::where('Config_header', 'Location')->where('Config_name','Gst Slip Invoices')->where('RO_code',$rocode)->where('Garruda_name', $location)->first();
                  $PaymentModels->Garruda_name = $location;
                  $PaymentModels->save();
                }
              }
        if ($Config_header_other_check->count()==0) {
            foreach ($Other as $value) {
              # code...
              $PaymentModels = new Tpl_tally_config();
              $PaymentModels->Config_header ="Others";
              $PaymentModels->RO_code =$rocode;
              $PaymentModels->Config_name="Gst Slip Invoices";
              $PaymentModels->Garruda_name = $value;
              $PaymentModels->save();
            }
              
               }else{
                  foreach ($Other as $value) {
                  $PaymentModels = Tpl_tally_config::where('Config_header', 'Others')->where('Config_name','Gst Slip Invoices')->where('RO_code',$rocode)->where('Garruda_name', $value)->first();
                  $PaymentModels->Garruda_name = $value;
                  $PaymentModels->save();
                     }
              }
        
            $Tally_Model_check = Tpl_tally_config::where('ro_code',$rocode)->get();
            
            $Tally_Model = Tpl_tally_config::where('ro_code',$rocode)->where('Config_name','Gst Slip Invoices')->orderByRaw('FIELD(Config_header, "WALKIN Customer", "Location", "Sales", "Taxes", "Others")')->get();
     
     
            // $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
      return view('backend.tallyConfig.gst',compact('Tally_Model','Tally_Model_check','MasterPayment','checkpayment'));  
    }

    public function updatetallyGST(Request $request)
    {
    
               try{
         $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }

          $paytypecreate=$request->input('Garruda_name');  
          $Tally_name=$request->input('Tally_name');  
          $id=$request->input('id');  
           foreach ($Tally_name as $key => $tally_names) {
          
           
         $paymentss = Tpl_tally_config::where('RO_code', $rocode)->where('id',$id[$key])->update(['Tally_name' =>$tally_names]);

          }
// dd($rocode ,$paytypecreate, $Tally_name, $id, $paymentss, $tally_names, $key );

          $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
      
    }

     public function edittallyDailyShift(Request $request)
    {
                  try{
         $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
          
         $state="";
         if(! empty(Auth::user()->customer->getro->getstate->statecode)){


          $state = Auth::user()->customer->getro->getstate->statecode;
           }
       }
         $val  = array(73,67);
        $Config_header_discount_check = Tpl_tally_config::where('Config_header','Discount')->where('RO_code',$rocode)->where('Config_name','Periodic VAT Bill (Fuel)')->get();

        $Config_header_billing_check = Tpl_tally_config::where('Config_header','Billing Charges')->where('RO_code',$rocode)->where('Config_name','Periodic VAT Bill (Fuel)')->get();
        $Config_header_tax_check = Tpl_tally_config::where('Config_header','Taxes')->where('RO_code',$rocode)->where('Config_name','Periodic VAT Bill (Fuel)')->get();
        $Config_header_customer_check = Tpl_tally_config::where('Config_header','Customer')->where('RO_code',$rocode)->where('Config_name','Periodic VAT Bill (Fuel)')->get();
        $Config_header_other_check = Tpl_tally_config::where('Config_header','Others')->where('RO_code',$rocode)->where('Config_name','Periodic VAT Bill (Fuel) ')->get();

        $Other = array('Rounded Off','Voucher Type');
       
         $tax_master_state = tax_master::where('Tax_Code', 'like', '%' . $state . '%')->get();

          $Sales_master_name = Stock_item_group::whereIn('parent', $val)->groupBy('Group_name')->get(['Group_name']);
    

           if ($tax_master_state->count()>0) {
            foreach ($tax_master_state as $key => $tax_master_states) {
                if ($Config_header_tax_check->count()==0) {
                $PaymentModels = new Tpl_tally_config();
                $PaymentModels->Config_header ="Taxes";
                $PaymentModels->RO_code =$rocode;
                $PaymentModels->Config_name="Periodic VAT Bill (Fuel)";
                $PaymentModels->Garruda_name = $tax_master_states->Description;
                $PaymentModels->save();
              }else{

                    foreach ($Config_header_tax_check as $key => $taxcheck) {
                   

                if ($tax_master_states->Group_name==$taxcheck->Garruda_name) {
                     $PaymentModels = Tpl_tally_config::where('Config_header','Taxes')->where('Config_name','Periodic VAT Bill (Fuel)')->where('RO_code',$rocode)->where('Garruda_name',$taxcheck->Garruda_name)->first();
                  $PaymentModels->Garruda_name = $tax_master_states->Group_name;
                  $PaymentModels->save();

                }
              }
            }
          }
       }
          
          if ($Config_header_customer_check->count()==0) {
                $PaymentModels = new Tpl_tally_config();
                $PaymentModels->RO_code =$rocode;
                $PaymentModels->Config_name="Periodic VAT Bill (Fuel)";
                $PaymentModels->Config_header ="Customer";
                $PaymentModels->Garruda_name ="Credit Customer Control A/c";
                $PaymentModels->save();
              }else{
                        $PaymentModels = Tpl_tally_config::where('Config_header', 'Customer')->where('Config_name','Periodic VAT Bill (Fuel)')->where('RO_code',$rocode)->where('Garruda_name', 'Credit Customer Control A/c')->first();
                        $PaymentModels->Garruda_name = 'Credit Customer Control A/c';
                        $PaymentModels->save();
                    }
              if ($Config_header_discount_check->count()==0) {
           
             
                $PaymentModels = new Tpl_tally_config();
                $PaymentModels->Config_header ="Discount";
                $PaymentModels->RO_code =$rocode;
                $PaymentModels->Config_name="Periodic VAT Bill (Fuel)";
                $PaymentModels->Garruda_name = "Discount";
                $PaymentModels->save();
             
              
               }else{

                  $PaymentModels = Tpl_tally_config::where('Config_header', 'Discount')->where('Config_name','Periodic VAT Bill (Fuel)')->where('RO_code',$rocode)->where('Garruda_name', 'Discount')->first();
                  $PaymentModels->Garruda_name = "Discount";
                  $PaymentModels->save();
                     
              }

            if ($Config_header_billing_check->count()==0) {
            
             
              $PaymentModels = new Tpl_tally_config();
              $PaymentModels->Config_header ="Billing Charges";
              $PaymentModels->RO_code =$rocode;
              $PaymentModels->Config_name="Periodic VAT Bill (Fuel)";
              $PaymentModels->Garruda_name = "Billing & Handling Charges";
              $PaymentModels->save();
            
              
               }else{
                  $PaymentModels = Tpl_tally_config::where('Config_header', 'Billing Charges')->where('Config_name','Periodic VAT Bill (Fuel)')->where('RO_code',$rocode)->where('Garruda_name', 'Billing & Handling Charges')->first();
                  $PaymentModels->Garruda_name = "Billing & Handling Charges";
                  $PaymentModels->save();
                  
              }

                  if ($Config_header_other_check->count()==0) {
            foreach ($Other as $value) {
             
              $PaymentModels = new Tpl_tally_config();
              $PaymentModels->Config_header ="Others";
              $PaymentModels->RO_code =$rocode;
              $PaymentModels->Config_name="Periodic VAT Bill (Fuel)";
              $PaymentModels->Garruda_name = $value;
              $PaymentModels->save();
            }
              
               }else{
                  foreach ($Other as $value) {
                  $PaymentModels = Tpl_tally_config::where('Config_header', 'Others')->where('Config_name','Periodic VAT Bill (Fuel)')->where('RO_code',$rocode)->where('Garruda_name', $value)->first();
                  $PaymentModels->Garruda_name = $value;
                  $PaymentModels->save();
                     }
              }
        
            $Tally_Model_check = Tpl_tally_config::where('ro_code',$rocode)->get();
            
            $Tally_Model = Tpl_tally_config::where('ro_code',$rocode)->where('Config_name','Periodic VAT Bill (Fuel)')->orderByRaw('FIELD(Config_header, "Customer", "Discount", "Billing Charges", "Taxes", "Others")')->get();
     
     
            // $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return view('backend.tallyConfig.dailyshift',compact('Tally_Model','Tally_Model_check','MasterPayment','checkpayment')); 
    }

     public function updatetallyDailyShift(Request $request)
    {
   
      try{
         $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }

          $paytypecreate=$request->input('Garruda_name');  
          $Tally_name=$request->input('Tally_name');  
          $id=$request->input('id');  

           foreach ($Tally_name as $key => $tally_names) {
          
           
         $paymentss = Tpl_tally_config::where('RO_code', $rocode)->where('id',$id[$key])->update(['Tally_name' =>$tally_names]);

          }

          $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
      
    }

    public function edittallyPeriodic(Request $request)
    {

                          try{
         $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
         
         $state="";
         if(! empty(Auth::user()->customer->getro->getstate->statecode)){


          $state = Auth::user()->customer->getro->getstate->statecode;
           }
       }
         $val  = array(73,67);
        $Config_header_discount_check = Tpl_tally_config::where('Config_header','Discount')->where('RO_code',$rocode)->where('Config_name','Periodic GST Bill (Others)')->get();
        $Config_header_billing_check = Tpl_tally_config::where('Config_header','Billing Charges')->where('RO_code',$rocode)->where('Config_name','Periodic GST Bill (Others)')->get();
        $Config_header_tax_check = Tpl_tally_config::where('Config_header','Taxes')->where('RO_code',$rocode)->where('Config_name','Periodic GST Bill (Others)')->get();
        // $Config_header_customer_check = Tpl_tally_config::where('Config_header','Customer')->where('Config_name','Periodic GST Bill (Others)')->get();
        $Config_header_other_check = Tpl_tally_config::where('Config_header','Others')->where('RO_code',$rocode)->where('Config_name','Periodic GST Bill (Others) ')->get();

        $Other = array('Rounded Off','Voucher Type');
       
         $tax_master_state = tax_master::where('Tax_Code', 'like', '%' . $state . '%')->where('Tax_Type','=','GST')->get();

          $Sales_master_name = Stock_item_group::whereIn('parent', $val)->groupBy('Group_name')->get(['Group_name']);
        // dd($tax_master_state);


           if ($tax_master_state->count()>0) {
            foreach ($tax_master_state as $key => $tax_master_states) {
                if ($Config_header_tax_check->count()==0) {
                $PaymentModels = new Tpl_tally_config();
                $PaymentModels->Config_header ="Taxes";
                $PaymentModels->RO_code =$rocode;
                $PaymentModels->Config_name="Periodic GST Bill (Others)";
                $PaymentModels->Garruda_name = $tax_master_states->Description;
                $PaymentModels->save();
              }else{
                  foreach ($Config_header_tax_check as $key => $taxcheck) {

                      if ($tax_master_states->Group_name==$taxcheck->Garruda_name) {
                           $PaymentModels = Tpl_tally_config::where('Config_header','Taxes')->where('Config_name','Periodic GST Bill (Others)')->where('RO_code',$rocode)->where('Garruda_name',$taxcheck->Garruda_name)->first();
                        $PaymentModels->Garruda_name = $tax_master_states->Group_name;
                        $PaymentModels->save();

                }

              }



              }
            }
          }
          
     
        if ($Config_header_discount_check->count()==0) {
           
             
              $PaymentModels = new Tpl_tally_config();
              $PaymentModels->Config_header ="Discount";
              $PaymentModels->RO_code =$rocode;
              $PaymentModels->Config_name="Periodic GST Bill (Others)";
              $PaymentModels->Garruda_name = "Discount";
              $PaymentModels->save();
           
              
               }else{
                  $PaymentModels = Tpl_tally_config::where('Config_header', 'Discount')->where('Config_name','Periodic GST Bill (Others)')->where('RO_code',$rocode)->where('Garruda_name', 'Discount')->first();
                  $PaymentModels->Garruda_name = "Discount";
                  $PaymentModels->save();
                     
              }

            if ($Config_header_billing_check->count()==0) {
            
             
              $PaymentModels = new Tpl_tally_config();
              $PaymentModels->Config_header ="Billing Charges";
              $PaymentModels->RO_code =$rocode;
              $PaymentModels->Config_name="Periodic GST Bill (Others)";
              $PaymentModels->Garruda_name = "Billing & Handling Charges";
              $PaymentModels->save();
            
              
               }else{
                  $PaymentModels = Tpl_tally_config::where('Config_header', 'Billing Charges')->where('Config_name','Periodic GST Bill (Others)')->where('RO_code',$rocode)->where('Garruda_name', 'Billing & Handling Charges')->first();
                  $PaymentModels->Garruda_name = "Billing & Handling Charges";
                  $PaymentModels->save();
                  
              }

                  if ($Config_header_other_check->count()==0) {
            foreach ($Other as $value) {
             
              $PaymentModels = new Tpl_tally_config();
              $PaymentModels->Config_header ="Others";
              $PaymentModels->RO_code =$rocode;
              $PaymentModels->Config_name="Periodic GST Bill (Others)";
              $PaymentModels->Garruda_name = $value;
              $PaymentModels->save();
            }
              
               }else{
                  foreach ($Other as $value) {
                  $PaymentModels = Tpl_tally_config::where('Config_header', 'Others')->where('Config_name','Periodic GST Bill (Others)')->where('RO_code',$rocode)->where('Garruda_name', $value)->first();
                  $PaymentModels->Garruda_name = $value;
                  $PaymentModels->save();
                     }
              }
        
            $Tally_Model_check = Tpl_tally_config::where('ro_code',$rocode)->get();
            
            $Tally_Model = Tpl_tally_config::where('ro_code',$rocode)->where('Config_name','Periodic GST Bill (Others)')->orderByRaw('FIELD(Config_header, "Discount", "Taxes", "Others")')->get();
     
            // $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }

        return view('backend.tallyConfig.periodic',compact('Tally_Model','Tally_Model_check','MasterPayment','checkpayment')); 
    }

     public function updatetallyPeriodic(Request $request)
    {
   
         try{
         $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }

          $paytypecreate=$request->input('Garruda_name');  
          $Tally_name=$request->input('Tally_name');  
          $id=$request->input('id');  

           foreach ($Tally_name as $key => $tally_names) {
          
           
         $paymentss = Tpl_tally_config::where('RO_code', $rocode)->where('id',$id[$key])->update(['Tally_name' =>$tally_names]);

          }

          $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
      
    }

    public function edittallyBillingCharges(Request $request)
    {

                        try{
         $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
         
         $state="";
         if(! empty(Auth::user()->customer->getro->getstate->statecode)){


          $state = Auth::user()->customer->getro->getstate->statecode;
           }
          // dd($state);
       }
         $val  = array(73,67);
      
        $Config_header_tax_check = Tpl_tally_config::where('Config_header','Taxes')->where('RO_code',$rocode)->where('Config_name','Periodic Separate Billing Charges')->get();
        $Config_header_customer_check = Tpl_tally_config::where('Config_header','Customer')->where('RO_code',$rocode)->where('Config_name','Periodic Separate Billing Charges')->get();
        $Config_header_other_check = Tpl_tally_config::where('Config_header','Others')->where('RO_code',$rocode)->where('Config_name','Periodic Separate Billing Charges ')->get();
        $Config_header_Discount_check = Tpl_tally_config::where('Config_header','Discount')->where('RO_code',$rocode)->where('Config_name','Periodic Separate Billing Charges ')->get();

          $Other = array('Credit Customer');
       
         $tax_master_state = tax_master::where('Tax_Code', 'like', '%' . $state . '%')->where('GST_Type','!=', 'VAT')->get();



           if ($tax_master_state->count()>0) {
            foreach ($tax_master_state as $key => $tax_master_states) {
                if ($Config_header_tax_check->count()==0) {
                $PaymentModels = new Tpl_tally_config();
                $PaymentModels->Config_header ="Taxes";
                $PaymentModels->RO_code =$rocode;
                $PaymentModels->Config_name="Periodic Separate Billing Charges";
                $PaymentModels->Garruda_name = $tax_master_states->Description;
                $PaymentModels->save();
              }else{
                foreach ($Config_header_tax_check as $key => $taxcheck) {
                if ($tax_master_states->Group_name==$taxcheck->Garruda_name) {
                     $PaymentModels = Tpl_tally_config::where('Config_header','Taxes')->where('Config_name','Periodic Separate Billing Charges')->where('RO_code',$rocode)->where('Garruda_name',$taxcheck->Garruda_name)->first();
                   $PaymentModels->Garruda_name = $tax_master_states->Group_name;
                   $PaymentModels->save();

                }
              }
            }
          }
        }
          
          if ($Config_header_customer_check->count()==0) {
              foreach ($Other as $value) {
            $PaymentModels = new Tpl_tally_config();
            $PaymentModels->RO_code =$rocode;
            $PaymentModels->Config_name="Periodic Separate Billing Charges";
            $PaymentModels->Config_header ="Customer";
            $PaymentModels->Garruda_name =$value;
            $PaymentModels->save();
          }
        }else{
          foreach ($Other as $value) {
                  $PaymentModels = Tpl_tally_config::where('Config_header', 'Customer')->where('Config_name','Periodic Separate Billing Charges')->where('RO_code',$rocode)->where('Garruda_name', $value)->first();
                  $PaymentModels->Garruda_name = $value;
                  $PaymentModels->save();

              }
            }
        if ($Config_header_other_check->count()==0) {
            // foreach ($Other as $value) {
             
              $PaymentModels = new Tpl_tally_config();
              $PaymentModels->Config_header ="Others";
              $PaymentModels->RO_code =$rocode;
              $PaymentModels->Config_name="Periodic Separate Billing Charges";
              $PaymentModels->Garruda_name = "Rounded Off";
              $PaymentModels->save();
            // }
              
               }else{
                  // foreach ($Other as $value) {
                  $PaymentModels = Tpl_tally_config::where('Config_header', 'Others')->where('Config_name','Periodic Separate Billing Charges')->where('RO_code',$rocode)->where('Garruda_name', 'Rounded Off')->first();
                  $PaymentModels->Garruda_name = "Rounded Off";
                  $PaymentModels->save();
                     // }
              }

               if ($Config_header_Discount_check->count()==0) {
            // foreach ($Other as $value) {
             
              $PaymentModels = new Tpl_tally_config();
              $PaymentModels->Config_header ="Discount";
              $PaymentModels->RO_code =$rocode;
              $PaymentModels->Config_name="Periodic Separate Billing Charges";
              $PaymentModels->Garruda_name = "Discount";
              $PaymentModels->save();
            // }
              
               }else{
                  // foreach ($Other as $value) {
                  $PaymentModels = Tpl_tally_config::where('Config_header', 'Discount')->where('Config_name','Periodic Separate Billing Charges')->where('RO_code',$rocode)->where('Garruda_name', 'Discount')->first();
                  $PaymentModels->Garruda_name = "Discount";
                  $PaymentModels->save();
                     // }
              }
        
            $Tally_Model_check = Tpl_tally_config::where('ro_code',$rocode)->get();
            
            $Tally_Model = Tpl_tally_config::where('ro_code',$rocode)->where('Config_name','Periodic Separate Billing Charges')->orderByRaw('FIELD(Config_header, "Customer", "Discount", "Sales", "Taxes", "Others")')->get();
     
     
            // $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return view('backend.tallyConfig.billingcharges',compact('Tally_Model','Tally_Model_check','MasterPayment','checkpayment')); 
    }

    public function updatetallyBillingCharges(Request $request)
    {
   

         try{
         $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }

          $paytypecreate=$request->input('Garruda_name');  
          $Tally_name=$request->input('Tally_name');  
          $id=$request->input('id');  

           foreach ($Tally_name as $key => $tally_names) {
          
           
         $paymentss = Tpl_tally_config::where('RO_code', $rocode)->where('id',$id[$key])->update(['Tally_name' =>$tally_names]);

          }
          $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
      
    }

  /**
     * Update payment mode order
     *
     * 
     */
  public function updatePaymentModeOrder(Request $request){
    
    $postData = $request->all();
    
    $rocode='';
    if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
    }
    
    //dd($postData);
    
    if($rocode!="" && isset($postData['item']) && count($postData['item']) > 0){
      $i = 0;
      foreach($postData['item'] as $id) {
        //$sql ="UPDATE tpl_payment_mode SET order_number=".$i." WHERE ro_code='".$rocode."' AND id=".$data;
        DB::table('tpl_payment_mode')->where('ro_code',$rocode)->where('id',$id)->update(['order_number'=>$i]);
        
        $i++;
      }
      
    }
    return response()->json(array("success"=>true,"item"=>$postData));
  }
}
