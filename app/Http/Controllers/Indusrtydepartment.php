<?php

namespace App\Http\Controllers;

//use App\CityController;
use Illuminate\Http\Request;
use App\IndustryDepartment;
use DB;
class Indusrtydepartment extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $Industrydepartment = IndustryDepartment::All();
           
        return view('backend.industry',compact('Industrydepartment')); 
    }

    
    public function indexs(Request $request)
    {

        $Industrydepartment = IndustryDepartment::All();
           
        return view('backend.department',compact('Industrydepartment')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create(Request $request)
    {
         
          $value=$request->input('value');  
          $type=$request->input('type'); 
         
          $validatedData = $request->validate([
          
          'value' => 'required|unique:tpl_industry_depart',

         ]);
          try {
                  $IndustryDepartment = new IndustryDepartment();
                  $IndustryDepartment->value =$value;
                  $IndustryDepartment->type =$type;
                  $IndustryDepartment->save();
                  $request->session()->flash('success','Added Successfully!!'); 

            }catch(\Illuminate\Database\QueryException $e){
                        
                $request->session()->flash('success','Something is wrong');      
            }

            return back();
 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function industryupdate(Request $request , $id)
    {
           $type=$request->input('type');
           $value=$request->input('value');  
        
          $validatedData = $request->validate([
          'value' => 'required|unique:tpl_industry_depart,value,'.$id,
          

         ]);
          IndustryDepartment::where('id', $id)->update(['type' =>$type,'value' =>$value]);
          $request->session()->flash('success','Data Updated Successfully!!'); 
          return back();
  


    }
      public function get_vertical(Request $request){
            $industry=$request->input('industry');
             
            $type= IndustryDepartment::where('type',$industry)->get();

             $type=$type->pluck('value','value')->toArray();

             return response()->json($type);

          }
    /**
     * Display the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function show(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function edit(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContributionModel $contributionModel)
    {
        //
    }
}
