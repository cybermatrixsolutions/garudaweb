<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TableTankMasterModel;
use App\TankInward;
use App\RoPieceListManagement;
use Carbon\Carbon;
use App\Shift;
use Auth;
use Log;
use DB;
class TankInwardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  

        $rocode='';
        if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
        }
         
            //$shifts=Shift::where('ro_code',$rocode)->where('is_active',1)->get();
            $fuel_types = RoPieceListManagement::join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
            ->where('tbl_stock_item_group.Group_Name','FUEL')
            ->where('tbl_price_list_master.RO_Code',$rocode)
            ->select('tbl_price_list_master.*')->get();

        $TankMasterModels=TableTankMasterModel::where('RO_code',$rocode)->where('is_active',1)->get();
        $TankInwards=TankInward::where('ro_code',$rocode)->where('status',1)->get();
       return view('backend.tank.inward.index',compact('TankMasterModels','rocode','TankInwards','fuel_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }




     public function store(Request $request)
    {  

         // dd($request->all())  ;
        Log::info('TankinwardController@store   input - '.print_r($request->all(),true));

         $input=$request->all();
         $rocode=$input['Ro_code'];
         $invoiceno=$input['invoiceno'];
       

      //  volume_
        // tankvolume

         //tankname

         $invoicedate=$input['invoicedate'];
         $invoicedate = new Carbon(str_replace('/', '-',$invoicedate));
         $invoicedate =date('Y-m-d',strtotime($invoicedate));

          $inwarddate=$input['inwarddate'];
         $inwarddate = new Carbon(str_replace('/', '-',$inwarddate));
         $inwarddate =date('Y-m-d',strtotime($inwarddate));

           $fuel_types = RoPieceListManagement::join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
                            ->where('tbl_stock_item_group.Group_Name','FUEL')
                            ->where('tbl_price_list_master.RO_Code',$rocode)
                            ->select('tbl_price_list_master.id','tbl_price_list_master.Item_Name')->get();

            foreach($fuel_types as $fuel){

                Log::info('ready to create      fuel_id record  - '.print_r($fuel->id,true));
               $volume=0;

               if(isset($input['volume_'.$fuel->id])){


                $volume=$input['volume_'.$fuel->id];
               }
                                               Log::info('volume    - '.$volume);


               $tankvolume=[];
               if(isset($input['tankvolume_'.$fuel->id])){


                        $tankvolume=$input['tankvolume_'.$fuel->id];
               }

                                            Log::info(' tankvolume  -   '.print_r($tankvolume,true));


                $tankname=[];
               if(isset($input['tankname_'.$fuel->id])){


                        $tankname=$input['tankname_'.$fuel->id];
               }

                                             Log::info(' tankname  -   '.print_r($tankname,true));




                 //ready to create tank inward data 
                 if($volume>0){



                        $TankInward=new TankInward();
                        $TankInward->value=$volume;
                        $TankInward->inward_date=$inwarddate;
                        $TankInward->invoice_date=$invoicedate;
                        $TankInward->invoice_no=$invoiceno;
                        $TankInward->fuel_type=$fuel->id;
                        $TankInward->user_id=Auth::user()->id;
                       // $TankInward->shift_id=$request->shift;
                        $TankInward->ro_code=$rocode;
                        $TankInward->save();



                        // create data tank_tankinvert relational data


                         $data=[];
                        $tankvaluem=$tankvolume;
                        $tanids=$tankname;
                        $i=0;
                        foreach ($tanids as $tanid) {
                           $data[$tanid]=['value'=>$tankvaluem[$i]];
                           $i++;
                        }

                        $TankInward->tanks()->attach($data);



                 }

                        
     


            }




/*
        $TankInward=new TankInward();
        $TankInward->value=$request->volume;
        $TankInward->inward_date=$inwarddate;
        $TankInward->invoice_date=$date;
        $TankInward->invoice_no=$request->invoiceno;
        $TankInward->fuel_type=$request->fuel_type;
        $TankInward->user_id=Auth::user()->id;
       // $TankInward->shift_id=$request->shift;
        $TankInward->ro_code=$request->Ro_code;
        $TankInward->save();

        $data=[];
        $tankvaluem=$request->tankvaluem;
        $tanids=$request->tanname;
        $i=0;
        foreach ($tanids as $tanid) {
           $data[$tanid]=['value'=>$tankvaluem[$i]];
           $i++;
        }

        $TankInward->tanks()->attach($data);
       */
       return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   /* public function store(Request $request)
    {  

   // dd($request->all())  ;
Log::info('TankinwardController@store   input - '.print_r($request->all(),true));

         $input=$request->all();

        $date=$request->invoicedate;
         $date = new Carbon(str_replace('/', '-',$date));
         $date =date('Y-m-d',strtotime($date));

          $inwarddate=$request->inwarddate;
         $inwarddate = new Carbon(str_replace('/', '-',$inwarddate));
         $inwarddate =date('Y-m-d',strtotime($inwarddate));

        $TankInward=new TankInward();
        $TankInward->value=$request->volume;
        $TankInward->inward_date=$inwarddate;
        $TankInward->invoice_date=$date;
        $TankInward->invoice_no=$request->invoiceno;
        $TankInward->fuel_type=$request->fuel_type;
        $TankInward->user_id=Auth::user()->id;
       // $TankInward->shift_id=$request->shift;
        $TankInward->ro_code=$request->Ro_code;
        $TankInward->save();

        $data=[];
        $tankvaluem=$request->tankvaluem;
        $tanids=$request->tanname;
        $i=0;
        foreach ($tanids as $tanid) {
           $data[$tanid]=['value'=>$tankvaluem[$i]];
           $i++;
        }

        $TankInward->tanks()->attach($data);
       
       return back();
    }*/

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 public function edit(Request $request)
    {
        
          Log::info('Get tankinward input TankInwardController@edit'.print_r($request->all(),true));

        $input=$request->all();
       DB::beginTransaction();
       try{

             $rocode=$input['Ro_code'];
             $tankInwardId=$input['tankInwardId'];
             $invoiceno=$input['invoiceno'];
             $fuel_id= $input['fuel_type']; 
             $invoicedate=$input['invoicedate'];
             $invoicedate = new Carbon(str_replace('/', '-',$invoicedate));
             $invoicedate =date('Y-m-d',strtotime($invoicedate));

              $inwarddate=$input['inwarddate'];
             $inwarddate = new Carbon(str_replace('/', '-',$inwarddate));
             $inwarddate =date('Y-m-d',strtotime($inwarddate));

               $fuel_types = RoPieceListManagement::join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
                                ->where('tbl_stock_item_group.Group_Name','FUEL')
                                ->where('tbl_price_list_master.RO_Code',$rocode)
                                ->select('tbl_price_list_master.id','tbl_price_list_master.Item_Name')->get();



                        if(isset($input['volume'])){


                            $volume=$input['volume'];

                            Log::info('volume'.print_r( $volume,true));

                             }

                foreach($fuel_types as $fuel){
                    
                    Log::info('ready to create      fuel_id record  - '.print_r($fuel->id,true));
                   $tankvolume=[];
                   if(isset($input['tankvolume'])){
                            $tankvolume=$input['tankvolume'];

                   }

              
               Log::info(' tankvolume isset-   '.print_r($tankvolume,true));

                $tankname=[];
                   if(isset($input['tankname'])){


                            $tankname=$input['tankname'];
                             //dd( $input['tankvolume_'.$fuel->id],$input['tankname_'.$fuel->id]);
                   }

                                                 Log::info(' tankname2  -   '.print_r($tankname,true));




                     if($volume>0){
                       

                            $TankInward=TankInward::where('id',$tankInwardId)->first();

                            $TankInward->value=$volume;
                            $TankInward->inward_date=$inwarddate;
                            $TankInward->invoice_date=$invoicedate;
                            $TankInward->invoice_no=$invoiceno;
                            $TankInward->fuel_type= $fuel_id;
                            $TankInward->user_id=Auth::user()->id;
                            $TankInward->ro_code=$rocode;
                            $TankInward->save();


                            Log::info('TankInward -- --  '.print_r($TankInward,true));
                         
                         
                            if(isset($tankInwardId))
                            {
                                $getAllTankValue=DB::table('tank_tankinwart')->where('tankinwart_id',$tankInwardId)->get();

                                foreach ($getAllTankValue as $key => $value) {
                                    DB::table('tank_tankinwart')->where('id',$value->id)->delete();

                                }
                          }

                             $data=[];
                            $tankvaluem=$tankvolume;

                            $tanids=$tankname;
                          
                           Log::info('tankvaluem='.print_r( $tankvaluem,true));
                           Log::info('tanids='.print_r( $tanids,true));
                            
                            $i=0;
                            foreach ($tanids as $tanid) {
                               $data[$tanid]=['value'=>$tankvaluem[$i]];
                               $i++;
                            }

                            $TankInward->tanks()->attach($data);
                     }

                }
              }catch(\Exception $e)
              {
                DB::rollback();
                 $request->session()->flash('danger','Something wrong!!');
                 return back();
              }
               $request->session()->flash('success','Info created successfully!!');
       return back();
    }


     
   public function getTankbyAjex(Request $request,$item)
    {   

         $rocode='';
       
          $rocode=$request->RO_code;
        
          
        $TankMasterModels=TableTankMasterModel::distinct()->where('RO_code',$rocode)->where('is_active',1)->where('fuel_type',$item)->get();
        
        $TankMasterModels=$TankMasterModels->pluck('Tank_Number','id');

        //dd($TankMasterModels);

  
        return response()->json($TankMasterModels);
    }

     public function getTankValueByAjax(Request $request,$item)
    {   



        Log::info('Get tankinward Id TankInwardController@getTankValueByAjax'.print_r($request->all(),true));

         $tankinward='';
       
          $tankinward_id=$request->tankInwardId;
        
          
        $TankValueModels=DB::table('tank_tankinwart')->where('tankinwart_id',$tankinward_id)->get();
        
        
  
        return response()->json($TankValueModels);
    }
    


    public function delete(Request $request,$id)
    {

         
         Log::info('TankInward delete Id=========TankInwardController@delete=='.print_r($id,true));

         $tankinward_id=$id;
            DB::beginTransaction();
            try{
       
                 $getAllTank=DB::table('tank_tankinwart')->where('tankinwart_id','=',$tankinward_id)->delete();
                
                 $getTankInward=DB::table('tbl_tank_inward')->where('id','=',$tankinward_id)->delete();

                 DB::commit();
            
            } catch (\Exception $e) {
                DB::rollback();

                 return back();
               }
   
           return back();       
  
      }
}
