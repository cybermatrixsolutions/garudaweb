<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\RoMaster;
use App\OtpModel;
use App\AddPetrolDieselModel;
use DB;
use App\RoCustomertManagement;
use App\CustomerDriverManager;
use App\Credit_limit;
use App\TransactionModel;
use App\RoPieceListManagement;
use App\CustomerLubeRequest;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use Gate;
use App\CityModel;
use Log;

class AddPetrolDieselController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        Log::info('AddPetrolDieselController@index  input - '.print_r($request->all(),true));

         
          if (Gate::allows('ro-admin',Auth::user()) || Gate::allows('admin',Auth::user())) {
            return redirect('Dashboard');
        }
          Log::info('AddPetrolDieselController@index  gate pass - ');
        
        $Customer_Codes='';
        if (Auth::user()->customer!=null) {
          $Customer_Code=Auth::user()->customer->Customer_Code;
        }
         $Customer_Mobile='';
        if (Auth::user()->customer!=null) {
          $Customer_Mobile=Auth::user()->customer->Mobile;
        }
         $Customer_Name='';
        if (Auth::user()->customer!=null) {
          $Customer_Name=Auth::user()->customer->Customer_Name;
        }


         
         //$Customer_Code = DB::table('tbl_customer_vehicle_driver')->where('customer_code',$Customer_Codes)->first();

           Log::info('AddPetrolDieselController@index  gate Customer_Code - '.$Customer_Code);
           Log::info('AddPetrolDieselController@index  gate Customer_Mobile - '.$Customer_Mobile);
           Log::info('AddPetrolDieselController@index  gate Customer_Name - '.$Customer_Name);
         if($Customer_Code==null)
         {
             $request->session()->flash('success','Sorry, Something Wrong');
              
            return redirect('Dashboard');
         }
         
       $rocode = Session::get('ro');
       

       $datas= RoMaster::where('is_active','1');
        if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();


       
        
        if(Auth::user()->user_type==4)
            {
            $add_petrol_diesel= AddPetrolDieselModel::join('tbl_ro_master', 'tbl_customer_request_petroldiesel.RO_code', '=', 'tbl_ro_master.RO_code')->join('tbl_customer_master', 'tbl_customer_request_petroldiesel.customer_code', '=', 'tbl_customer_master.Customer_Code')
            ->select('tbl_customer_request_petroldiesel.*','tbl_ro_master.pump_legal_name','tbl_customer_master.Customer_Name')
            ->where('tbl_customer_request_petroldiesel.customer_code',Auth::user()->customer->Customer_Code)->orderby('id','desc')->get();
            $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->select('tbl_price_list_master.*');

            if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $fuel_type=$fuel_type->where('tbl_price_list_master.RO_Code',$rocode);

              $fuel_type=$fuel_type->get();

            }else{

           $add_petrol_diesel= AddPetrolDieselModel::join('tbl_ro_master', 'tbl_customer_request_petroldiesel.RO_code', '=', 'tbl_ro_master.RO_code')->join('tbl_customer_master', 'tbl_customer_request_petroldiesel.customer_code', '=', 'tbl_customer_master.Customer_Code')
            ->select('tbl_customer_request_petroldiesel.*','tbl_ro_master.pump_legal_name','tbl_customer_master.Customer_Name')->orderby('id','desc')->get();
             $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->select('tbl_price_list_master.*')->get();
            
        }
         
       
        // $petrol_diesel_type= DB::table('tbl_ro_petroldiesel')->where('is_active', '>=',1)->get();

         $customer_driver_list = CustomerDriverManager::where('customer_code',$Customer_Code)->get();

     
        // $customer_code = RoCustomertManagement::All();


        // return view('backend.driver',compact(['datas','customer_driver_list','customer_code']));
    return view('backend.add_petrol_diesel',compact(['datas','fuel_type','add_petrol_diesel','Customer_Code','customer_driver_list','Customer_Mobile','Customer_Name']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
             $otppin = rand(1000, 9999);
             $vehicle_no= $request->input('Vehicle_Reg_No');

             $PetrolDiesel_Type=$request->input('PetrolDiesel_Type');
             $customer_code=$request->input('customer_code');
             $ro=$request->input('RO_code');
             $Request_Type=$request->input('Request_Type');
             $Request_Value=$request->input('Request_Value');
             $vehicle_capcitys=$request->input('vehicle_capcitys');
             
            $roInfo= RoMaster::where('RO_code',$ro)->get()->first();
            $cityInfo = CityModel::where('id',$roInfo->city)->get()->first();

            //echo $roInfo->pump_legal_name.", ".$cityInfo->name;
            //dd($cityInfo);
            //dd($request->all());
            
             
             $amountTrajection=0;
             $Credit_limitAmount=0;

              


             $Credit_limit=Credit_limit::where('Customer_Code',$customer_code)->first();

             if($Credit_limit!=null)
             $Credit_limitAmount=$Credit_limit->Limit_Value;

             $RoPieceListManagement=RoPieceListManagement::find($PetrolDiesel_Type);

             $CustomerLubeRequests=CustomerLubeRequest::where('customer_code',$customer_code)->where('Execution_date',null)->get();

             if($RoPieceListManagement!=null && $RoPieceListManagement->price!=null && $Request_Type!='Full Tank')
                 $amountTrajection=$RoPieceListManagement->price->price*$Request_Value;
              else
                $amountTrajection=$RoPieceListManagement->price->price*$vehicle_capcitys;
            

                foreach ($CustomerLubeRequests as $CustomerLubeRequest) {
                    
                    if($CustomerLubeRequest->priceLists!=null)
                     $amountTrajection=($CustomerLubeRequest->priceLists->price*$CustomerLubeRequest->quantity)+$amountTrajection;
                }
             
                $pemdingFuelRes=AddPetrolDieselModel::where('customer_code',$customer_code)->where('Execution_date',null)->get();
                
                foreach ($pemdingFuelRes as $pemdingFuelRe) {
                    
                     
                    if($pemdingFuelRe->priceLists!=null){
                      
                        if($pemdingFuelRe->Request_Type!='Full Tank'){

                            $amountTrajection=($pemdingFuelRe->priceLists->price*$pemdingFuelRe->quantity)+$amountTrajection;
                        }else{
                            
                            $amountTrajection=($pemdingFuelRe->priceLists->price*$pemdingFuelRe->getVehicle->capacity)+$amountTrajection;
                        }
                    }
                     
                   

                }
              
           
            
             $transactionModels =   TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
                ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
                ->where('tbl_customer_transaction.customer_code',$customer_code)
                ->where('tbl_customer_transaction.status','<>',2)
                 ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode')
                ->get();

                
                foreach ($transactionModels as $transactionModel) {
                  
                      $amountTrajection=$amountTrajection+($transactionModel->item_price*$transactionModel->petroldiesel_qty);
                   
                }
                

                if($amountTrajection>$Credit_limitAmount){
                     $request->session()->flash('success','Credit Limit Exhausted!!');
                     return back();
                }
         
          if($vehicle_no!=null){
                   $AddPetrolDieselModel = AddPetrolDieselModel::where('Vehicle_Reg_No',$vehicle_no)->where('Execution_date',Null)->get();

                 if($AddPetrolDieselModel!=null && $AddPetrolDieselModel->count()>0 ){
                    
                     $request->session()->flash('success','Sorry, This vehicle request already exists');
                     return redirect('add_petrol_diesel');
                 }
             }

         $fuel_id = DB::table('tbl_customer_vehicle_master')->where('Registration_Number',$vehicle_no)->first();
         $fuel_id =$fuel_id->Fuel_Type;

         $otppin = rand(1000, 9999);

            $length = 5;
            $randomBytes = openssl_random_pseudo_bytes($length);
            $characters = '0123456789';
            $charactersLength = strlen($characters);
            $result = '';

          for ($i = 0; $i < $length; $i++)
             $result .= $characters[ord($randomBytes[$i]) % $charactersLength];

       
        $add_petrol_diesel = new AddPetrolDieselModel();
        $add_petrol_diesel->Ro_code = $request->input('RO_code');
        $add_petrol_diesel->customer_code = $request->input('customer_code');
        $add_petrol_diesel->PetrolDiesel_Type = $request->input('PetrolDiesel_Type');
        $add_petrol_diesel->Item_id = $request->input('PetrolDiesel_Type');
        $add_petrol_diesel->Request_Type = $request->input('Request_Type');
        $add_petrol_diesel->Request_Value = $request->input('Request_Value');            
        $add_petrol_diesel->Vehicle_Reg_No = $request->input('Vehicle_Reg_No');
        //$add_petrol_diesel->current_driver_mobile = $request->input('mobile');
        $add_petrol_diesel->current_driver_mobile = $request->input('current_driver_mobile');

        // $date =$request->input('request_date');
        // $request_date = date("Y-m-d", strtotime($date));
        // $add_petrol_diesel->request_date = $request_date;
        $add_petrol_diesel->request_id = "RQ".mt_rand();
        $add_petrol_diesel->save();
        $OtpModel = new OtpModel();
        $OtpModel->mobile=$request->input('current_driver_mobile'); 
        $OtpModel->OTP=$otppin; 
        $OtpModel->request_id=$add_petrol_diesel->request_id;
       // dd($add_petrol_diesel->request_id);
        $OtpModel->save();

        //send sms in mobile
       // $message="Your One Time Password is: ".$OtpModel->OTP." for request #".$OtpModel->request_id.".Please do not share OTP to anyone.";
        
        $message ="Your OTP is ".$OtpModel->OTP." for #".$OtpModel->request_id." dtd. ".date('d/m/Y')." - Fuel - ".$vehicle_no." at ".$roInfo->pump_legal_name.", ".$cityInfo->name;
            
        $this->sendsms($message, $add_petrol_diesel->current_driver_mobile);
      //  dd($OtpModel);
        $request->session()->flash('success','Create Request  Successfully and Send OTP !!');
    
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');  
            }
              
             return back();
        // $add_petrol_diesel = new AddPetrolDieselModel();
        // $add_petrol_diesel->Ro_code = $request->input('RO_code');
        // $add_petrol_diesel->customer_code = $request->input('customer_code');
        // $add_petrol_diesel->PetrolDiesel_Type = $request->input('PetrolDiesel_Type');
        // $add_petrol_diesel->Request_Type = $request->input('Request_Type');
        // $add_petrol_diesel->Request_Value = $request->input('Request_Value');            
        // $add_petrol_diesel->Vehicle_Reg_No = $request->input('Vehicle_Reg_No');
        // $date =$request->input('request_date');
        // $request_date=date('Y-m-d', strtotime(str_replace('-','/', $date)));
        // $add_petrol_diesel->request_date = $request_date;
        // $add_petrol_diesel->save();
        // return redirect('add_petrol_diesel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

             if (Gate::allows('ro-admin',Auth::user()) || Gate::allows('admin',Auth::user())) {
                    return redirect('Dashboard');
            }
            $Customer_Code='';
           if (Auth::user()->customer!=null) {
              $Customer_Code=Auth::user()->customer->Customer_Code;
           }

         $datas= RoMaster::where('is_active','1');

         if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $datas=$datas->where('RO_code',Auth::user()->customer->RO_code);

              $datas=$datas->get();

         $request_petrol_diesel = AddPetrolDieselModel::where('id', '=', $id)->orderby('id','desc')->first();
        
        $add_petrol_diesel= AddPetrolDieselModel::All();

        $fuel_type = RoPieceListManagement::join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->select('tbl_price_list_master.*')->get();
        
        return view('backend.add_petrol_diesel_show',compact(['datas','fuel_type','add_petrol_diesel','request_petrol_diesel']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    function active(Request $request,$id, $actt)
    {
        
        $ac = AddPetrolDieselModel::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $personal_mangement = AddPetrolDieselModel::find($id);
           
            $OtpModel = OtpModel::where('request_id',$personal_mangement->request_id);
            $OtpModel->delete();
           $personal_mangement->delete();
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
        

        return back();
    }

    function sendsms($message,$mobile) { 
        $username="GARRUDA";
        $password ="9811078782";
        $number=$mobile;
        $sender="GARRUD";
        $message=$message;
        $url="login.bulksmsgateway.in/sendmessage.php?user=".urlencode($username)."&password=".urlencode($password)."&mobile=".urlencode($number)."&sender=".urlencode($sender)."&message=".urlencode($message)."&type=".urlencode('3');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch); 
        return $curl_scraped_page;

        }
}
