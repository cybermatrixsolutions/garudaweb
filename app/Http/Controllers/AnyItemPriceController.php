<?php

namespace App\Http\Controllers;

use App\CustomerLubeRequest;
use Illuminate\Http\Request;
use App\RoMaster;
use App\TableItemPriceListModel;
use App\RoPieceListManagement;
use DB;
use Carbon\Carbon;
use Auth;
use Gate;
use App\Stock_item_group;

class AnyItemPriceController extends Controller
{   
     protected $groupName;
      protected $groupsubgrup;
	

	public function __construct(){
        $tbl_stock_item_group=DB::table('tbl_stock_item_group')->where('Group_Name','!=','FUEL')->where('parent',0)->first();
        if($tbl_stock_item_group!=null)
		$this->groupName= $tbl_stock_item_group->id;

		if(isset($_GET['itemName'])){ $this->groupName=$_GET['itemName'];}

        if(isset($_GET['subgrup'])){ $this->groupsubgrup=$_GET['subgrup'];}
	}

   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
         $groupName=$this->groupName;
         if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }

        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
        $oldprice=[]; 
       //$price_item = TableItemPriceListModel::where('effective_date',$efective_date);
       
        $fuel_type= RoPieceListManagement::join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
            ->where('tbl_price_list_master.Stock_Group',$this->groupName);

            if(trim($this->groupsubgrup)!='')
            $fuel_type=$fuel_type->where('tbl_price_list_master.Sub_Stock_Group',$this->groupsubgrup);

            $fuel_type=$fuel_type->where('tbl_price_list_master.is_active',1)
            ->select('tbl_price_list_master.*','tbl_stock_item_group.Group_Name');

             if(Auth::user()->user_type!=1 && $rocode!='')
                $fuel_type=$fuel_type->where('tbl_price_list_master.RO_Code',$rocode);

              $fuel_type=$fuel_type->get();
            
         //dd($fuel_type);
            $search_date=date('d/m/Y');
   
            $group=DB::table('tbl_stock_item_group')->where('Group_Name','!=','FUEL')->where('is_active',1)->where('tbl_stock_item_group.parent',0)->get();
            $subgrup=DB::table('tbl_stock_item_group')->where('Group_Name','!=','FUEL')->where('is_active',1)->where('tbl_stock_item_group.parent','!=',0)->get();
           $olds=  DB::table('tbl_item_price_list')->where('is_active',4)->get();
             
             foreach ($olds as $old) {

                if(isset($fuel->getpricelist)){

                   $oldprice[$old->item_id]=['price'=>$old->price,'effective_date'=>$old->effective_date];
                }else{

                   $oldprice[$old->item_id]=['price'=>$old->price,'effective_date'=>$old->effective_date];
               
                }
                

             }

        return view('backend.any_item_price',compact('fuel_type','search_date','group','oldprice','groupName','subgrup'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getsubgroup($grup)
    {
         //$group=DB::table('tbl_stock_item_group')->where('Group_Name',$grup)->get();
          $Stock_item_group = Stock_item_group::where('id',$grup)->first();
          $data=[];

          if($Stock_item_group!=null)
           $data=$Stock_item_group->children()->pluck('Group_Name','id')->toArray();

         
        return response()->json($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
  
        $datas= RoMaster::where('is_active',1)->get();
        $item_id= RoPieceListManagement::get();
          $groupName=$this->groupName;
       
        $lube_list= CustomerLubeRequest::join('tbl_stock_item_master', 'tbl_customer_request_lube.item_id', '=', 'tbl_stock_item_master.id')
        ->select('tbl_customer_request_lube.*', 'tbl_stock_item_master.Stock_Item_Name')->get();
   
    if(Auth::User()->getRocode!=null)
            $lube_list=$lube_list->where('RO_Code',Auth::User()->getRocode->RO_code);

        // $designation=DB::table('tbl_designation_master')->get();
         // return view('backend.lube_page',compact('designation'));
         return view('backend.lube_page',compact(['datas','item_id','lube_list','groupName']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

          
          $date =$request->input('date');
          $groupName=$this->groupName;
          //$efective_date=date('Y-m-d', strtotime(str_replace('-','/', $date)));
          $efective_date = new Carbon(str_replace('/', '-',$date));
        
         $group=DB::table('tbl_stock_item_group')->where('Group_Name','!=','FUEL')->where('is_active',1)->get();

          $price_item = TableItemPriceListModel::join('tbl_price_list_master','tbl_item_price_list.item_id', '=', 'tbl_price_list_master.id')
          ->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
          ->where('tbl_stock_item_group.Group_Name',$this->groupName)
          ->whereDate('tbl_item_price_list.effective_date',$efective_date)
          ->select('tbl_item_price_list.*');
          
           $oldprice=[];
           $olds=  DB::table('tbl_item_price_list')->where('is_active',2)->get();

             foreach ($olds as $old) {

                if(isset($old->getpricelist)){
                   $oldprice[$old->item_id]=['price'=>$old->price,'effective_date'=>$old->effective_date];
                }else{
                   $oldprice[$old->item_id]=['price'=>$old->price,'effective_date'=>$old->effective_date];
               
                }
                

             }
         

          if(count($price_item->get()) > 0)
          {

             $fuel_type = $price_item->where('tbl_item_price_list.is_active',1)->get();
              
             $search_date = $date;
             //dd($fuel_type);
             return view('backend.any_item_price',compact('fuel_type','search_date','group','oldprice','groupName'));

          }
          else
          {

             $search_date = $date;
              $fuel_type = $price_item->where('tbl_item_price_list.is_active',1)->get();
              // dd($fuel_type);
              //$fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name',$this->groupName)->select('tbl_price_list_master.*')->get();
              return view('backend.any_item_price',compact('fuel_type','search_date','group','oldprice','groupName'));
          }

       
        
    }


    public function saveFuelPrice(Request $request)
    {
        
          $date =$request->input('effective_date');
         
          $groupName=$this->groupName;
          //$effective_date=date('Y-m-d H:i:s', strtotime(str_replace('-','/', $date)));
        $effective_date = new Carbon(str_replace('/', '-',$date));
        $currentdate=date('Y-m-d');

        $flg=true;
        if($currentdate!=date('Y-m-d',strtotime($effective_date))){
            $flg=false;
            
        }
       
        $item = $request->input('namenew');
       
        $price = $request->input('Price');
        
       for($i=0;$i<count($item);$i++)
        {

            if($price[$i]!='' && intval($price[$i])>0 && $flg==true){
                

                $is_active = TableItemPriceListModel::where('item_id',$item[$i])->where('is_active',1)->update(['is_active' =>0]);
                //$is_active = TableItemPriceListModel::where('item_id',$item[$i])->where('is_active',1)->update(['is_active' =>2]);
             } 
        }

        for($i=0;$i<count($item);$i++)
        {  
            $chk=true;
           if($price[$i]!='' && intval($price[$i])>0){

                if($flg===false){

                    $PriceListModel =TableItemPriceListModel::where('item_id',$item[$i])->where('is_active',4)->get();
                    
                    if($PriceListModel->count()>0)
                     $chk=false;

                }
                
                if($chk){
                  
                    $update_price = new TableItemPriceListModel();
                    $update_price->item_id = $item[$i];
                    $update_price->price = $price[$i];
                    $update_price->effective_date =  $effective_date;

                    if($flg)
                        $update_price->is_active=1;
                    else
                         $update_price->is_active=4;

                    $update_price->save();

                }else{
                    
                    TableItemPriceListModel::where('item_id',$item[$i])->where('is_active',4)->update(['price' =>$price[$i],'effective_date'=>$effective_date]);
                }
             }   
            
        }

        $request->session()->flash('success','Added Successfully!!');


         return redirect('Any_price_management?itemName='.$this->groupName);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerLubeRequest $customerLubeRequest,$id)
    {
         $lube_data = DB::table('tbl_customer_request_lube')->where('id', '=', $id)->first();
        
        
        return view('backend.lube_view_page', compact(['lube_data']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerLubeRequest $customerLubeRequest)
    {
        
    }
    function active(Request $request,$id, $actt)
    {
        
        $ac = CustomerLubeRequest::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerLubeRequest $customerLubeRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        try {
            $personal_mangement = CustomerLubeRequest::find($id);

        $personal_mangement->delete();
        $request->session()->flash('success','Data has been deleted Successfully!!');
        }  catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
         

        return back();
    }
}
