<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\RoPieceListManagement;
use App\TableStockItemMaster;
use App\TableItemPriceListModel;
use App\RoMaster;
use App\Pitem_tax;
use DB;
use Gate;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use File;
use Carbon\Carbon;

class CsvItemPriceUpdateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function itemshow() {

        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getstockgr(Request $request)
    {
            

    }
    public function index()
    {
       
   
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function store(Request $request)
    {
  

         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        
    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
           
             }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        
       
         
        
    }
      public function itemtaxsavetable(Request $request)
      {
         
            
         
   }
    public function gettax(Request $request){
            

          }
    function active(Request $request,$id, $actt)
    {
        
     

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
     public function getItemBYajex($pump,$tags)
    {
         
    
  }

  public function getrotax(Request $request)
  {
        
  }
  /**
  * dowload item prece 
  * @param $request
  */
  public function DownloadItem(Request $request)
  {
  


        try{  
             $item='LUBES';
             $itemName = $request->input('itemName');
             $subgrup = $request->input('subgrup');



             if($itemName!=null)
               $item=$itemName;

             if($subgrup!=null || $subgrup!=0)
              $item=$itemName;
            
              $rocode='';
              if(Auth::user()->getRocode!=null) {
                 $rocode=Auth::user()->getRocode->RO_code;
              }
           
            $getitem= RoPieceListManagement::join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
                ->where('tbl_price_list_master.is_active',1)
                ->select('tbl_price_list_master.*','tbl_stock_item_group.Group_Name');
              
               if($itemName!=null)
                  $getitem= $getitem->where('tbl_price_list_master.Stock_Group',$itemName);

                 if($subgrup!=null || $subgrup!=0)
                  $getitem= $getitem->where('tbl_price_list_master.Sub_Stock_Group',$subgrup);



              if(Auth::user()->user_type!=1 && $rocode!='')
                  $getitem=$getitem->where('tbl_price_list_master.RO_Code',$rocode);

              $getitem=$getitem->get();

               $search_date=date('d/m/Y');
             
           
              $tot_record_found=0;
         

          if($getitem->count()>0){
              $tot_record_found=1;
               

               $CsvData=array('Stock Item Code,Stock Group,Stock Sub Group,Stock Description,effective_date,Price');  
              //$CsvData=array('Item Name,Price,Effective Date');         

              foreach($getitem as $value){
                    
                  if(isset($value->getpricelist)){

                              $CsvData[]=$value->Item_Code.','.$value->getgroup->Group_Name.','.$value->getsubgroup->Group_Name.','.str_replace(',',' ', $value->Item_Name).','.date('d/m/Y',strtotime($value->effective_date)).','.$value->price;
                     }else{
                             if(isset($value->price)){
                             $CsvData[]=$value->Item_Code.','.$value->getgroup->Group_Name.','.$value->getsubgroup->Group_Name.','.str_replace(',',' ', $value->Item_Name).','.date('d/m/Y',strtotime($value->price->effective_date)).','.$value->price->price;
                             }else{
                               $CsvData[]=$value->Item_Code.','.$value->getgroup->Group_Name.','.$value->getsubgroup->Group_Name.','.str_replace(',',' ', $value->Item_Name).', ';
                             }
                  }          
              }

             
               
              $filename=date('Y-m-d').".csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers );
          }

     }
    catch(\Illuminate\Database\QueryException $e)
      {
                      
                     $request->session()->flash('success','Please  Master For Add Tax !!');
                      
      }

        return redirect('Any_price_management');
  }

  public function updateCsvData(Request $request)
  {
    $rocode='';
            if(Auth::user()->getRocode!=null) {
               $rocode=Auth::user()->getRocode->RO_code;
            }

     //dd($rocode);
       if (Input::hasFile('file'))
            {

                $file = Input::file('file');
                $name = time() . '-' . $file->getClientOriginalName();
                $path = storage_path('documents');
                $file->move($path, $name);
                $filename=base_path('/storage/documents/'.$name);
                //$file = fopen($filename, "r");
                 $file=fopen($filename, 'r');
                  while(! feof($file))
                  {
                    $row= fgetcsv($file);

                    if($row){
                        $datas[]=$row;
                    }

                }
                      
                    unset($datas[0]);
                               
                              foreach ($datas as $data)
                              {
                                $itemname = DB::table('tbl_price_list_master')->where('Item_Code', '=', $data[0])->first();
                                  if($itemname == null || trim($data[0]) =='')
                                
                                    {
                                   $request->session()->flash('success','Could not found this iten name '.$data[0]); 
                                   return back();
                                    } 
                                    // sp 11-06-2018 17:45
                                   /*$date_now = date("d/m/Y");
                                    if (strtotime($date_now) > strtotime($data[4])) {
                                  
                                       $request->session()->flash('success','Could not Add Post date'); 
                                       return back();
                                    } */
                                    

                                   /* if ($date_now > $data[4]) {
                                  
                                       $request->session()->flash('success','Could  not Add Past date'); 
                                       return back();
                                    }*/
                                   if($data[5] == 0 || trim($data[5]) =='')
                                   {
                                    
                                      $request->session()->flash('success','Please  Check Price Value!!'); 
                                      return back();
                                     
                                  }
                                  
                              }
                           
                         
                    
                            foreach ($datas as $data) {
                              $tbl_price_list_master= RoPieceListManagement::join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
                               
                                ->where('tbl_price_list_master.RO_Code',$rocode)
                                ->where('tbl_price_list_master.Item_Code',trim($data[0]))
                                 ->select('tbl_price_list_master.*')->first();
                               
              
                               
                                  if($tbl_price_list_master == null)
                                    {
                                   $request->session()->flash('success','Your RO Code Not  Exit,Please Contact Admin'); 
                                   return back();
                                   }


                             $dat= TableItemPriceListModel::where('item_id',$tbl_price_list_master->id)->first();
                            

                             if($dat!=null){
                                TableItemPriceListModel::where('item_id',$tbl_price_list_master->id)->where('is_active',2)->update(['is_active' => 0]);
                                TableItemPriceListModel::where('item_id',$tbl_price_list_master->id)->where('is_active',1)->update(['is_active' => 2]);
                             }

                              $Roitem= new TableItemPriceListModel();
                              $Roitem->item_id= $tbl_price_list_master->id;
                             
                               $Roitem->price= $data[5];
                                  $to_dates = new Carbon(str_replace('/', '-',$data[4]));
                                  $to_dates = date('Y-m-d', strtotime($to_dates));
                              $Roitem->effective_date = $to_dates;
          
                             // $Roitem->effective_date= date('Y-m-d H:i:s', strtotime(str_replace('-','/', $data[3])));
                              //($Roitem->effective_date);
                               $Roitem->is_active =1;
                               $Roitem->save();  
                                 
                           }
                      

                   
                $request->session()->flash('success','Data Import Csv  Successfully!!'); 
          
                  
            }
             return back();
    }
}
