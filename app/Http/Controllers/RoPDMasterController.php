<?php

namespace App\Http\Controllers;

use App\RoPDMaster;
use Illuminate\Http\Request;
use DB;
use App\MastorRO;

class RoPDMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $data = $RoPDMaster=RoPDMaster::join('tbl_ro_petroldiesel', 'tbl_ro_petroldiesel_price.petroldiesel_type', '=', 'tbl_ro_petroldiesel.id')
           ->select('tbl_ro_petroldiesel_price.*', 'tbl_ro_petroldiesel.Petrol_Diesel_Type as type')
            ->get(); 
         $data1 =  MastorRO::where('is_active','1')->get();
        return view('backend.petDmaster',compact('data','data1'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
         $validatedData = $request->validate([
         'RO_code' => 'required',
         'petroldiesel_type' => 'required',
         'Price' => 'required',
         'Price_Date'=>'required',
        ]);
         try {

        $RoPDMaster = new RoPDMaster();
        $RoPDMaster->RO_code=$request->input('RO_code');
        $RoPDMaster->petroldiesel_type=$request->input('petroldiesel_type');
        $RoPDMaster->Price=$request->input('Price');
        $date =$request->input('Price_Date');
       $mysqltime=date('Y-m-d', strtotime(str_replace('-','/', $date)));
        $RoPDMaster->Price_Date=$mysqltime;
        $RoPDMaster->is_active=1;
        $RoPDMaster->save();
        $request->session()->flash('success',' Added  Successfully!!');
        
           }
            catch(\Illuminate\Database\QueryException $e){
               
             $request->session()->flash('success',' Sonthing is Wrong !!');
        }
       return back();



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RoPetrolDesiel  $roPetrolDesiel
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RoPDMaster $RoPDMaster , $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RoPetrolDesiel  $roPetrolDesiel
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, RoPDMaster $RoPDMaster , $id)
    {   


         $validatedData = $request->validate([
         'RO_code' => 'required',
         'petroldiesel_type' => 'required',
         'Price' => 'required',
         'Price_Date'=>'required',
        ]);
        $RoPDMaster = $RoPDMaster->find($id);
         
       // $RoPDMaster = new RoPDMaster();
        $RoPDMaster->RO_code=$request->input('RO_code');
        $RoPDMaster->petroldiesel_type=$request->input('petroldiesel_type');
        $RoPDMaster->Price=$request->input('Price');
        $date =$request->input('Price_Date');
        $mysqltime=date('Y-m-d', strtotime(str_replace('-','/', $date)));
        $RoPDMaster->Price_Date=$mysqltime;
        
        $RoPDMaster->save();

       
    
         $request->session()->flash('success','Data Updated Successfully!!');   
         
         return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RoPetrolDesiel  $roPetrolDesiel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RoPDMaster $RoPDMaster , $id)
    {
         $data1 = $MastorRO=MastorRO::all();
        $RoPDMaster=$RoPDMaster->find($id);
         $data2= DB::table('tbl_ro_petroldiesel')->where('RO_code',$RoPDMaster->RO_code)->get();
        $RoPDMaster=$RoPDMaster->find($id);
       
        return view('backend.PdmasterUpdate',compact('RoPDMaster','data1','data2'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RoPetrolDesiel  $roPetrolDesiel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      try {
            $ac = RoPDMaster::where('id',$id)->delete();
             $request->session()->flash('success','Delete Successfully !!');   
            } catch(\Illuminate\Database\QueryException $ac){
                
            $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }


        
        
        return back();
    }
    function active(Request $request,$id, $actt)
    {
        
        $ac = RoPDMaster::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }
    public function getrocode(Request $request){
            $code=$request->input('rocode');
            $Customer= DB::table('tbl_ro_petroldiesel')->where('RO_code',$code)->get();
            
            $Customer=$Customer->pluck('Petrol_Diesel_Type','id')->toArray();
           
            return response()->json($Customer);

          }
           

}
