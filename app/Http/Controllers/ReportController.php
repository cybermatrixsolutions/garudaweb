<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InvoiceBilling;
use App\TransactionModel;
use App\tax_master;
use App\nozzelsReadingModel;
use App\RoCustomertManagement;
use App\AddPetrolDieselModel;
use App\CustomerLubeRequest;
use App\InvoiceVehicleSummary;
use App\Credit_limit;
use App\RoPersonalManagement;
use App\CustomerDriverManager;
use Carbon\Carbon;
use Auth;
use Log;
use DB;
use App\TransTaxModel;
use App\ItemServicesModel;
use stdClass;

class ReportController extends Controller
{
 //
     /**
      * salesRegisterBilled
      * @return \Illuminate\Http\Response


      */
    

      public function salesRegisterBilledSummary(Request $request){
         
         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }

      $fule_type=$request->input('fule_type');

      $from_date = $request->input('fromdate');
      $to_date = $request->input('to_date');


      if($fule_type=="GST Invoice"){

          Log::info('ReportController@Reportcontroller@salesRegisterBilledSummary  fule_type - '.$fule_type);
          $data= $this->salesRegisterBilledSummaryGSTIvoice($from_date,$to_date,$fule_type,$rocode);
           return $data;
         }elseif($fule_type=="2"){

         // Log::info('ReportController@Reportcontroller@salesRegisterBilledSummary  fule_type - '.$fule_type);
          $data= $this->salesRegisterBilledSummaryGSTBill($from_date,$to_date,$fule_type,$rocode);
           return $data;
         }

       $taxs=tax_master::get()->groupBy('GST_Type');
      // Log::info('Reportcontroller@salesRegisterBilledSummary taxs -'.print_r($taxs,true));
        if($fule_type==null){
           $fule_type=2;
         }



      if($from_date!=null && $to_date!=null){
        
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));

              
      //  $bills=InvoiceBilling::where('ro_code',$rocode)->where('type',$fule_type)->get();
         $bills=InvoiceBilling::join('invoice','invoice.invoice_no','=','invoice_billing.invoice_no')
                       ->where('invoice_billing.ro_code',$rocode)
                       ->where('type',$fule_type)
                       ->where('invoice.invoice_date','>=',$from_dates)
                       ->where('invoice.invoice_date','<=',$to_dates)
                       ->selectRaw('invoice_billing.*,DATE_FORMAT(invoice.invoice_date, \'%d-%m-%Y\') as invoiceDate')
                                ->distinct()
                       ->get();
     

       
        Log::info('ReportController@salesRegisterBilledSummary  bills - '.print_r($bills,true));

         $rostate="";
      if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
             $rostate=Auth::user()->getRocode->getstate->name;
        }
      

      }

       $services =ItemServicesModel::where('RO_code',$rocode)->orderby('id','desc')->get();
        Log::info('  Reportcontroller  $services ======'.print_r($services,true));
          $allTexByRo=array();
        foreach($services as $row){ 
      
             
              $allTexByRo=array();
                 foreach($row->gettax as $tax) {
                  $allTexByRo[]=array('Tax_Code'=>$tax->Tax_Code,'Tax_percentage'=>$tax->Tax_percentage,'GST_Type'=>$tax->GST_Type);

                 }
       
             }
         // Log::info('  Reportcontroller  $allTexByRo ====== '.print_r($allTexByRo,true)); 

      
       return view('backend.reports.salesRegisterBilledSummary',compact('taxs','fule_type','bills','rostate','allTexByRo'));

    }

    /**
    * salesRegisterBilledDownload
    * @return \Illuminate\Http\Response
    */

    public function  salesRegisterBilledSummaryGSTBill($from_date,$to_date,$fule_type,$rocode){

       $taxs=tax_master::get()->groupBy('GST_Type');
      Log::info('Reportcontroller@salesRegisterBilledSummaryGSTBill $fule_type -'.$fule_type);
       $rostate="";
      if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
             $rostate=Auth::user()->getRocode->getstate->name;
        }
        Log::info('Reportcontroller@salesRegisterBilledSummaryGSTBill $rostate - '.$rostate);
       $taxs=tax_master::get()->groupBy('GST_Type');
      // Log::info('Reportcontroller@salesRegisterBilledSummaryGSTBill -'.print_r($taxs,true));
        if($fule_type==null){
           $fule_type=2;
         }
      if($from_date!=null && $to_date!=null){
        
          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));

                
          $bills=InvoiceBilling::join('invoice','invoice.invoice_no','=','invoice_billing.invoice_no')
                       ->where('invoice_billing.ro_code',$rocode)
                       ->where('type',$fule_type)
                       ->selectRaw('invoice_billing.*,DATE_FORMAT(invoice.invoice_date, \'%d-%m-%Y\') as invoiceDate')
                       ->where('invoice.invoice_date','>=',$from_dates)->where('invoice.invoice_date','<=',$to_dates)

                       ->get();
       

         

                  
           
         // Log::info('ReportController@salesRegisterBilledSummaryGSTBill bills - '.print_r($bills,true));
         

      }

        $services =ItemServicesModel::where('RO_code',$rocode)->orderby('id','desc')->get();
        Log::info('  Reportcontroller  $services ======'.print_r($services,true));
          $allTexByRo=array();
        foreach($services as $row){ 
      
             
              $allTexByRo=array();
                 foreach($row->gettax as $tax) {
                  $allTexByRo[]=array('Tax_Code'=>$tax->Tax_Code,'Tax_percentage'=>$tax->Tax_percentage,'GST_Type'=>$tax->GST_Type);

                 }
       
             }
          //Log::info('  Reportcontroller  $allTexByRo ====== '.print_r($allTexByRo,true));      
      
       
        return view('backend.reports.salesRegisterBilledSummaryGSTBill',compact('rostate','taxs','fule_type','bills','allTexByRo'));




     }
 public function salesRegisterBilledDownloadSummaryGSTBill(Request $request,$fule_type){
        Log::info('ReportController@salesRegisterBilledDownloadSummaryGSTBill kkkkkk  input - '.print_r($request->all(),true));
        if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');
      
    
        if($from_date!=null && $to_date!=null){
        
          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));

                
           $bills=InvoiceBilling::join('invoice','invoice.invoice_no','=','invoice_billing.invoice_no')
                       ->where('invoice_billing.ro_code',$rocode)
                       ->where('type',$fule_type)
                       ->where('invoice.invoice_date','>=',$from_dates)->where('invoice.invoice_date','<=',$to_dates)
                       ->selectRaw('invoice_billing.*,DATE_FORMAT(invoice.invoice_date, \'%d-%m-%Y\') as invoiceDate')
                       ->get();
     
         Log::info('ReportController@salesRegisterBilledSummaryGSTBill bills - '.print_r($bills,true));
        

      }
       $rostate="";
        if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
               $rostate=Auth::user()->getRocode->getstate->name;
          }
        Log::info('Reportcontroller@salesRegisterBilledSummaryGSTBill $rostate - '.$rostate);
        $services =ItemServicesModel::where('RO_code',$rocode)->orderby('id','desc')->get();
       // Log::info('  Reportcontroller  $services ======'.print_r($services,true));
          $allTexByRo=array();
        foreach($services as $row){ 
      
             
              $allTexByRo=array();
                 foreach($row->gettax as $tax) {
                  $allTexByRo[]=array('Tax_Code'=>$tax->Tax_Code,'Tax_percentage'=>$tax->Tax_percentage,'GST_Type'=>$tax->GST_Type);

                 }
       
             }
         // Log::info('  Reportcontroller  $allTexByRo ====== '.print_r($allTexByRo,true));      
       

         $taxs=tax_master::orderBy('GST_Type')->get()->groupBy('GST_Type');
         $str='Inv.Type,Invoice No.,Invoice Date,Party Code,Party Name,GSTIN No.,Place of Supply,Invoice Amount,VAT Taxable Value,GST Taxable Value,CGST %,CGST Amount,UT/SGST  %,UT/SGST  Amount,IGST %,IGST Amount,VAT  %,VAT Amount,SPCESS %,SPCESS Amount,Handling Charges,Discount Amount,Round Off';

              $CsvData=array($str);
              $i=1;
                

                $totalFinalAmount=0;
                $totalHandlingCharges=0;
                $totalDiscount=0;
                $totalRoundOff=0;
                $totalTaxableValue=0;  

                 $totalCgstAmt=0;
                $totalSgstAmt=0;  
                $totalIgstAmt=0; 
                $totalSpcesstAmt=0;
                $totalVatAmt=0; 

                 $cgstPer="0.00";
               $cgstAmt="0.00";
               $sgstPer="0.00";
               $sgstAmt="0.00";
               $igstPer="0.00";
               $igstAmt="0.00";
               $spcessPer="0.00";
               $spcessAmt="0.00";
               $vatPer="0.00";
               $vatAmt="0.00";           
    foreach($bills as $bill){

               $totalFinalAmount=$totalFinalAmount+$bill->bill_amount;
             
              $totalHandlingCharges="0.00";


              $totalDiscount=$totalDiscount+$bill->discount;
              $totalRoundOff= $totalRoundOff+$bill->rounded_of;
              $totalTaxableValue=$totalTaxableValue+$bill->handling_amount;
              $tval=[];
               $handlingAmount=$bill->handling_amount;
               $cgstPer="0.00";
               $cgstAmt="0.00";
               $sgstPer="0.00";
               $sgstAmt="0.00";
               $igstPer="0.00";
               $igstAmt="0.00";
               $spcessPer="0.00";
               $spcessAmt="0.00";
               $vatPer="0.00";
               $vatAmt="0.00";
                  //dd($allTax);

               
                  $customerState=$bill->party->getstate->name;
                  Log::info('Customer state   ------ '.print_r($customerState,true));
                   foreach($allTexByRo as $allTax)
                   {

                        if($rostate== $customerState){
//calculate cgst and sgst

                          if($allTax['GST_Type']=="CGST"){
                             $cgstPer=$allTax['Tax_percentage'];
                             $cgstAmt=($handlingAmount* $cgstPer)/100;


                          } 
                           if($allTax['GST_Type']=="UT/SGST"){
                             $sgstPer=$allTax['Tax_percentage'];
                             $sgstAmt=($handlingAmount* $cgstPer)/100;


                          } 


                        }else{
//calculate igst

                            if($allTax['GST_Type']=="IGST"){
                               $igstPer=$allTax['Tax_percentage'];
                               $igstAmt=($handlingAmount* $cgstPer)/100;
                            } 
                        }


                   }
             
                   if($bill->customer_code =="Walkin"||$bill->customer_code =="walkin"){
                   /* dd($bill->customer_code);*/
                       $customerName=$bill->cust_name;
                       $gstin="";
                   }else{
                     /* dd($bill->getCustomername);*/

                         if(empty($bill->party->company_name))
                         {
                           $customerName="Not Found";
                         

                         }else{
                          $customerName=$bill->party->company_name;
                          

                         }


                         if(empty($bill->party->gst_no))
                         {
                          
                           $gstin="";

                         }else{
                          
                          $gstin=$bill->party->gst_no;

                         }
                    
                   }

                      $totalCgstAmt=$totalCgstAmt+$cgstAmt;
                      $totalSgstAmt=$totalSgstAmt+$sgstAmt;  
                      $totalIgstAmt=$totalIgstAmt+$igstAmt; 
                      $totalSpcesstAmt=$totalSpcesstAmt+$spcessAmt;  
             $totalVatAmt=$totalVatAmt+$vatAmt;  
                 $str='';
                          
                            $str.='GST';
                            $str.=','.$bill->invoice_no;
                            $str.=','.date('d/m/y',strtotime($bill->invoiceDate));
                         
                            $str.=','.$bill->customer_code;
                            $str.=','.$customerName;
                            $str.=','.$gstin;
                            $str.=','.$customerState;
                            $str.=','.bcadd($bill->bill_amount, 0, 2);
                            $str.=','.'0.00';
                            $str.=','.bcadd($bill->handling_amount, 0, 2);
                            $str.=','. $cgstPer;
                            $str.=','.bcadd(round($cgstAmt,2), 0, 2);
                             $str.=','.$sgstPer;
                            $str.=','.bcadd(round($sgstAmt,2), 0, 2);
                             $str.=','.$igstPer;
                            $str.=','.bcadd(round($igstAmt,2), 0, 2);
                             $str.=','.'0.00';
                            $str.=','.'0.00';
                             $str.=','.'0.00';
                            $str.=','.'0.00';
             
                            $str.=','.'0.00';
                            $str.=','.bcadd(round($bill->discount,2), 0, 2);
                            $str.=','.bcadd(round($bill->rounded_of,2), 0, 2);
                            $CsvData[]=$str;

                  }    
                    
                           
                    /*   $str=',,,,,,Grand Totals :,'. bcadd($totalFinalAmount, 0, 2).',0.00,'.bcadd($totalTaxableValue, 0, 2).',,,,,,,,,,,'.bcadd($totalHandlingCharges, 0, 2).','.bcadd($totalDiscount, 0, 2).','.bcadd($totalRoundOff, 0, 2);*/

                           $str=',,,,,,Grand Totals :,'. bcadd($totalFinalAmount, 0, 2).',0.00,'.bcadd($totalTaxableValue, 0, 2).',,'.bcadd($totalCgstAmt, 0, 2).',,'.bcadd($totalSgstAmt, 0, 2).',,'.bcadd($totalIgstAmt, 0, 2).',,'.bcadd($totalVatAmt, 0, 2).',,'.bcadd($totalSpcesstAmt, 0, 2).','.bcadd($totalHandlingCharges, 0, 2).','.bcadd($totalDiscount, 0, 2).','.bcadd($totalRoundOff, 0, 2);
                      
                      $CsvData[]=$str;
               
              $filename=date('Y-m-d')."-sales-register-summary-gst-bill-reports.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];

              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);



 }

     public function  salesRegisterBilledSummaryGSTIvoice($from_date,$to_date,$fule_type,$rocode){

       $taxs=tax_master::get()->groupBy('GST_Type');
      // Log::info('Reportcontroller@salesRegisterBilledSummary taxs -'.print_r($taxs,true));
       
      if($from_date!=null && $to_date!=null){
        
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));

        }
        $customerTransaction=TransactionModel::where('ro_code',$rocode)->where('petrol_or_lube',2)
          ->where('trans_date','>=',$from_dates)->where('trans_date','<=',$to_dates)
          ->groupby('invoice_number')
           ->orderby('invoice_number')
          ->selectRaw('tbl_customer_transaction.*,SUM(tbl_customer_transaction.taxable_value) as taxable_value')
          
          ->get();
   
        Log::info('ReportController@salesRegisterBilledSummaryGSTIvoice  customerTransaction record - '.print_r($customerTransaction,true));

          $rostate="";
      if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
             $rostate=Auth::user()->getRocode->getstate->name;
        }
      

    

       $services =ItemServicesModel::where('RO_code',$rocode)->orderby('id','desc')->get();
        Log::info('  Reportcontroller  $services ======'.print_r($services,true));
          $allTexByRo=array();
        foreach($services as $row){ 
      
             
              $allTexByRo=array();
                 foreach($row->gettax as $tax) {
                  $allTexByRo[]=array('Tax_Code'=>$tax->Tax_Code,'Tax_percentage'=>$tax->Tax_percentage,'GST_Type'=>$tax->GST_Type);

                 }
       
             }
          //Log::info('  Reportcontroller  $allTexByRo ====== '.print_r($allTexByRo,true)); 

        return view('backend.reports.salesRegisterBilledSummaryGSTIvoice',compact('taxs','fule_type','customerTransaction','rostate','allTexByRo'));




     }
 public function salesRegisterBilledDownloadSummaryGSTInvoice(Request $request,$fule_type){
        Log::info('ReportController@salesRegisterBilledDownloadSummaryGSTInvoice  input - '.print_r($request->all(),true));
        if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }
          $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');
      
      if($from_date!=null && $to_date!=null){
        
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));

        }
        $customerTransaction=TransactionModel::where('ro_code',$rocode)->where('petrol_or_lube',2)
      
          ->where('trans_date','>=',$from_dates)->where('trans_date','<=',$to_dates)
          ->groupby('invoice_number')
          ->orderby('invoice_number')
          ->selectRaw('tbl_customer_transaction.*,SUM(tbl_customer_transaction.taxable_value) as taxable_value')
          
          ->get();

         // Log::info('ReportController@salesRegisterBilledDownloadSummaryGSTInvoice  customerTransaction - '.print_r($customerTransaction,true));

         $taxs=tax_master::orderBy('GST_Type')->get()->groupBy('GST_Type');
         $str='Inv.Type,Invoice No.,Invoice Date,Party Code,Party Name,GSTIN No.,Place of Supply,Invoice Amount,VAT Taxable Value,GST Taxable Value,CGST %,CGST Amount,UT/SGST  %,UT/SGST  Amount,IGST %,IGST Amount,VAT  %,VAT Amount,SPCESS %,SPCESS Amount,Handling Charges,Discount Amount,Round Off';

              $CsvData=array($str);
                    
                    $i=1;

                $totalFinalAmount=0;
                $totalHandlingCharges=0;
                $totalDiscount=0;
                $totalRoundOff=0;
                $totalTaxableValue=0; 

                $totalCgstAmt=0;
                $totalSgstAmt=0;  
                $totalIgstAmt=0; 
                $totalSpcesstAmt=0;
                $totalVatAmt=0;  



                 $cgstPer="0.00";
                 $cgstAmt="0.00";
                 $sgstPer="0.00";
                 $sgstAmt="0.00";
                 $igstPer="0.00";
                 $igstAmt="0.00";
                 $spcessPer="0.00";
                 $spcessAmt="0.00";
                 $finalTaxable="0.00";

                 $customerName="";
      
    foreach($customerTransaction as $transaction){

    
               $getallTransactionid=TransactionModel::where('invoice_number',$transaction->invoice_number)->pluck('id');
               //dd($transaction,$getallTransactionid);
                 $getApplyTax=TransTaxModel::whereIn('transaction_id',$getallTransactionid)
                 ->groupby('Tax_percentage')
                  ->pluck('Tax_percentage');
                 
               $getApplyTaxBoth=TransTaxModel::whereIn('transaction_id',$getallTransactionid)
                 ->groupby('GST_Type')
                  ->groupby('Tax_percentage')
                  ->selectRaw('GST_Type,Tax_percentage,SUM(Tax_Value) as taxAmount')->get();

        
                $totalFinalAmount=$totalFinalAmount+$transaction->invoice_amount;
                $totalHandlingCharges=$totalHandlingCharges+ $transaction->handling_amount;
                $totalDiscount=$totalDiscount+$transaction->discount;
                $totalRoundOff= $totalRoundOff+$transaction->invoice_roundoff;
               /* $totalTaxableValue=$totalTaxableValue+$transaction->taxable_value;*/
               $tval=[];
               if($transaction->customer_code =="Walkin"||$transaction->customer_code =="walkin"){
               /* dd($transaction->customer_code);*/
                   $customerName=$transaction->cust_name;
                   $gstin="";
               }else{
               /* dd($transaction->getCustomername);*/
                   if(empty($transaction->getCustomername->company_name))
                   {
                     $customerName="Not Found";
                    }else{
                    $customerName=$transaction->getCustomername->company_name;
                    }
                   if(empty($transaction->getCustomername->gst_no))
                   {
                    
                     $gstin="";

                   }else{
                     $gstin=$transaction->getCustomername->gst_no;
                   }
               }
           

           $tratex=$getApplyTaxBoth->groupBy('taxAmount'); 

              if(!$tratex->isEmpty()){


               foreach($tratex as $key =>$tax){

                

                 $cgstPer="0.00";
                 $cgstAmt="0.00";
                 $sgstPer="0.00";
                 $sgstAmt="0.00";
                 $igstPer="0.00";
                 $igstAmt="0.00";
                 $spcessPer="0.00";
                 $spcessAmt="0.00";
                 $finalTaxable="0.00";


                          foreach($tax as $value){

                                  
                           if($value['GST_Type']=="CGST"){
                                              $cgstPer=$value['Tax_percentage'];
                                              $cgstAmt=$value['taxAmount'];
                                              $per=$value['Tax_percentage'];

 
                                              } 
                                               if($value['GST_Type']=="UT/SGST"){
                                                 $sgstPer=$value['Tax_percentage'];
                                                  $per=$value['Tax_percentage'];
                                                 $sgstAmt=$value['taxAmount'];


                                              } 
            
                    //calculate igst

                                              if($value['GST_Type']=="IGST"){
                                                 $igstPer=$value['Tax_percentage'];
                                                 $per=$value['Tax_percentage'];
                                                 $igstAmt=$value['taxAmount'];
                                               }

                                                if($value['GST_Type']=="SPCESS"){
                                                 $spcessPer=$value['Tax_percentage'];
                                                 $per=$value['Tax_percentage'];
                                                 $spcessAmt=$value['taxAmount'];

                                                 Log::info('$spcessPer '.$spcessPer.'   spcessAmt'.$spcessAmt.'    per '.$per);
                                               }


                               $gettransTaxableValue=DB::table('item_transaction_tax')->join('tbl_customer_transaction','tbl_customer_transaction.id','=','item_transaction_tax.transaction_id')
                                 ->select(DB::raw("item_transaction_tax.Tax_percentage,item_transaction_tax.Tax_percentage,SUM(tbl_customer_transaction.taxable_value_W) as taxableValue"))
                                 ->whereIn('transaction_id',$getallTransactionid)
                                 ->groupby('Tax_Code')
                               ->distinct('transaction_id')

                  
                                /* ->selectRaw('item_transaction_tax.Tax_percentage,SUM(tbl_customer_transaction.taxable_value) as taxableValue')*/
                           ->get()
                           ;
                             
                           //dd($gettransTaxableValue);
                              foreach($gettransTaxableValue as $taxvisevalue){
                                if($taxvisevalue->Tax_percentage==$per)
                                {


                                  $finalTaxable=$taxvisevalue->taxableValue;

                                    Log::info('$finalTaxable '.$finalTaxable.'   taxvisevalue->Tax_percentage'.$taxvisevalue->Tax_percentage.'    per '.$per);

                                }

                              }


                    


                     }

                      $totalCgstAmt=$totalCgstAmt+bcadd($cgstAmt, 0, 2);
                      $totalSgstAmt=$totalSgstAmt+bcadd($sgstAmt, 0, 2);  
                      $totalIgstAmt=$totalIgstAmt+bcadd($igstAmt, 0, 2); 
                      $totalSpcesstAmt=$totalSpcesstAmt+bcadd($spcessAmt, 0, 2);  
                       $totalTaxableValue=$totalTaxableValue+bcadd($finalTaxable, 0, 2);
                        $str='';
  
                             
                              $str.='GST';
                            
                            
                            $str.=','.$transaction->invoice_number;
                            $str.=','.date('d/m/y',strtotime($transaction->trans_date));
                         
                            $str.=','.$transaction->customer_code;
                            $str.=','.$customerName;
                            $str.=','.$gstin;

                            
                            $str.=','.$transaction->state;
                            $str.=','.bcadd($transaction->invoice_amount, 0, 2);
                         
                            $str.=','.'0.00';
                              $str.=','.bcadd($finalTaxable, 0, 2);

                            $str.=','. $cgstPer;
                            $str.=','.bcadd($cgstAmt, 0, 2);
                             $str.=','.$sgstPer;
                            $str.=','.bcadd($sgstAmt, 0, 2);
                             $str.=','.$igstPer;
                            $str.=','.bcadd($igstAmt, 0, 2);
                        
                            
                             $str.=','.'0.00';
                            $str.=','.'0.00';
                             $str.=','.$spcessPer;
                            $str.=','.bcadd($spcessAmt, 0, 2);


                           
                            $str.= ', 0.00, 0.00, 0.00';
                        
                        
                         $CsvData[]=$str;
                         $i++;
                 }


               }else{

// if tax record not found

                Log::info('tax record not found');
                $str='GST,'.$transaction->invoice_number.','.date('d/m/y',strtotime($transaction->trans_date)).','.$transaction->customer_code.','.$customerName.','.$gstin.','.$transaction->state.','.bcadd($transaction->invoice_amount, 0, 2).',0.00,0.00';

                      foreach($taxs as $key=>$tax){
                        $str.=',0.00,0.00';
                      }
                   
                     /* $str.=','.bcadd($bill->taxable_amt, 0, 2);*/
                      $str.=','.bcadd(round($transaction->handling_amount,2), 0, 2);
                      $str.=','.bcadd(round($transaction->discount,2), 0, 2);
                      $str.=','.bcadd(round($transaction->invoice_roundoff,2), 0, 2);
                      $CsvData[]=$str;




               }






                
                /* bcadd($bill->grand_total_amount, 0, 2).','.bcadd($bill->taxable_amt, 0, 2).',,';*/

                Log::info('$transaction->invoice_roundoff  ---- '.print_r($transaction->invoice_roundoff,true));
                if($transaction->invoice_roundoff!="0.00"||"0"){

                      $str='GST,'.$transaction->invoice_number.','.date('d/m/y',strtotime($transaction->trans_date)).','.$transaction->customer_code.','.$customerName.','.$gstin.','.$transaction->state.','.bcadd($transaction->invoice_amount, 0, 2).',0.00,0.00';

                      foreach($taxs as $key=>$tax){
                        $str.=',0.00,0.00';
                      }
                   
                     /* $str.=','.bcadd($bill->taxable_amt, 0, 2);*/
                      $str.=','.bcadd(round($transaction->handling_amount,2), 0, 2);
                      $str.=','.bcadd(round($transaction->discount,2), 0, 2);
                      $str.=','.bcadd(round($transaction->invoice_roundoff,2), 0, 2);
                      $CsvData[]=$str;
                    }

                  }    
                    
                           
                       $str=',,,,,,Grand Totals :,'. bcadd($totalFinalAmount, 0, 2).',0.00,'.bcadd($totalTaxableValue, 0, 2).',,'.bcadd($totalCgstAmt, 0, 2).',,'.bcadd($totalSgstAmt, 0, 2).',,'.bcadd($totalIgstAmt, 0, 2).',,'.bcadd($totalVatAmt, 0, 2).',,'.bcadd($totalSpcesstAmt, 0, 2).','.bcadd($totalHandlingCharges, 0, 2).','.bcadd($totalDiscount, 0, 2).','.bcadd($totalRoundOff, 0, 2);
                      
                      $CsvData[]=$str;
               
              $filename=date('Y-m-d')."-sales-register-summary-gst-invoice-reports.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];

              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);



 }
   
    public function salesRegisterBilledDownloadSummary(Request $request,$fule_type){

   Log::info('ReportController@salesRegisterBilledDownloadSummary  input - '.print_r($request->all(),true));


         
         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }

         $taxs=tax_master::orderBy('GST_Type')->get()->groupBy('GST_Type');
       
        
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');

         //$taxs=tax_master::get()->groupBy('GST_Type');
          if($fule_type==null){
             $fule_type=2;
           }

        // $bills=InvoiceBilling::where('ro_code',$rocode)->where('type',$fule_type)->get();
           
     

        if($from_date!=null && $to_date!=null){

          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
           $bills=InvoiceBilling::join('invoice','invoice.invoice_no','=','invoice_billing.invoice_no')
                       ->where('invoice_billing.ro_code',$rocode)
                       ->where('type',$fule_type)
                       ->where('invoice.invoice_date','>=',$from_dates)->where('invoice.invoice_date','<=',$to_dates)
                       ->distinct()
                       ->selectRaw('invoice_billing.*,DATE_FORMAT(invoice.invoice_date, \'%d-%m-%Y\') as invoiceDate')
                       ->get();

         
      //  Log::info('ReportController@salesRegisterBilledDownloadSummary  bills - '.print_r($bills,true));
        }
     

 $rostate="";
        if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
               $rostate=Auth::user()->getRocode->getstate->name;
          }
        Log::info('Reportcontroller@salesRegisterBilledSummaryGSTBill $rostate - '.$rostate);
        $services =ItemServicesModel::where('RO_code',$rocode)->orderby('id','desc')->get();
       // Log::info('  Reportcontroller  $services ======'.print_r($services,true));
          $allTexByRo=array();
        foreach($services as $row){ 
      
             
              $allTexByRo=array();
                 foreach($row->gettax as $tax) {
                  $allTexByRo[]=array('Tax_Code'=>$tax->Tax_Code,'Tax_percentage'=>$tax->Tax_percentage,'GST_Type'=>$tax->GST_Type);

                 }
       
             }


           $str='Inv.Type,Invoice No.,Invoice Date,Party Code,Party Name,GSTIN No.,Place of Supply,Invoice Amount,VAT Taxable Value,GST Taxable Value,CGST %,CGST Amount,UT/SGST  %,UT/SGST  Amount,IGST %,IGST Amount,SPCESS %,SPCESS Amount,VAT  %,VAT Amount,Handling Charges,Discount Amount,Round Off';

             
             

              $CsvData=array($str);
                    
                    $i=1;

                $totalFinalAmount=0;
                $totalHandlingCharges=0;
                $totalDiscount=0;
                $totalRoundOff=0;
                $totalTaxableValue=0; 

                $totalCgstAmt=0;
                $totalSgstAmt=0;  
                $totalIgstAmt=0; 
                $totalSpcesstAmt=0;
                $totalVatAmt=0; 

                 $cgstPer="0.00";
               $cgstAmt="0.00";
               $sgstPer="0.00";
               $sgstAmt="0.00";
               $igstPer="0.00";
               $igstAmt="0.00";
               $spcessPer="0.00";
               $spcessAmt="0.00";
               $vatPer="0.00";
               $vatAmt="0.00";

               $companyName="";

                   
             foreach($bills as $bill){
                //dd($bill,$bill->invoice_no);

                $getallTransactionid=TransactionModel::where('bill_no',$bill->invoice_no)->pluck('id');
              // dd($bill,$getallTransactionid);
                 $getApplyTax=TransTaxModel::whereIn('transaction_id',$getallTransactionid)
                 ->groupby('Tax_percentage')
                  ->pluck('Tax_percentage');
                 
               $getApplyTaxBoth=TransTaxModel::whereIn('transaction_id',$getallTransactionid)
                 ->groupby('GST_Type')
                  ->groupby('Tax_percentage')
                  ->selectRaw('GST_Type,Tax_percentage,SUM(Tax_Value_W) as taxAmount')->get();

        
                $totalFinalAmount=$totalFinalAmount+$bill->grand_total_amount;
                $totalHandlingCharges=$totalHandlingCharges+ $bill->handling_amount;
                $totalDiscount=$totalDiscount+$bill->discount;
                $totalRoundOff= $totalRoundOff+$bill->rounded_of;

                
                    Log::info('bill->rounded_of  =           '.$bill->rounded_of);
               
               $tval=[];
              
           

                $tratex=$getApplyTaxBoth->groupBy('taxAmount'); 


if(!$tratex->isEmpty()){
          foreach($tratex as $key =>$tax){

               $cgstPer="0.00";
               $cgstAmt="0.00";
               $sgstPer="0.00";
               $sgstAmt="0.00";
               $igstPer="0.00";
               $igstAmt="0.00";
               $spcessPer="0.00";
               $spcessAmt="0.00";
               $vatPer="0.00";
               $vatAmt="0.00";




                  foreach($tax as $value){
    
            //calculate igst
                     
                                         if($value['GST_Type']=="VAT"){

                                                 $vatPer=$value['Tax_percentage'];
                                                 $vatAmt=$value['taxAmount'];
                                               }

                            $gettransTaxableValue=TransTaxModel::join('tbl_customer_transaction','tbl_customer_transaction.id','=','item_transaction_tax.transaction_id')
                                 ->whereIn('transaction_id',$getallTransactionid)
                                 ->groupby('item_transaction_tax.Tax_percentage')
                  
                              ->selectRaw('item_transaction_tax.Tax_percentage,SUM(tbl_customer_transaction.taxable_value_W) as taxableValue')
                              ->get();
                              $finalTaxable="0.00";
                             // dd($gettransTaxableValue);
                              foreach($gettransTaxableValue as $taxvisevalue){
                                if($taxvisevalue->Tax_percentage==$vatPer)
                                {


                                  $finalTaxable=$taxvisevalue->taxableValue;
                                }

                              }


                    $totalTaxableValue=$totalTaxableValue+bcadd(round($finalTaxable,2), 0, 2);
                       $totalVatAmt=$totalVatAmt+bcadd(round($vatAmt,2), 0, 2);  



                     }
                      /*Log::info('vatAmt  -   '.$vatAmt);
                      Log::info('totalVatAmt  -   '.$totalVatAmt);
                    */


                            $str='';
  
                             if($bill->type==1){
                                $str.='VAT';
                             }else{
                              $str.='GST';
                             }
                            
                            $str.=','.$bill->invoice_no;
                            $str.=','.date('d/m/y',strtotime($bill->invoiceDate));
                         
                            $str.=','.$bill->customer_code;

                              $companyName= str_replace(',', '', $bill->party->company_name);
                            $str.=','.$companyName;
                            $str.=','.$bill->party->gst_no;

                            
                            $str.=','.$bill->party->getstate->name;
                            $str.=','.bcadd($bill->grand_total_amount, 0, 2);
                            $str.=','.bcadd(round($finalTaxable,2), 0, 2);
                            $str.=','.'0.00';
                        

                            $str.=','.'0.00';
                            $str.=','.'0.00';
                            $str.=','.'0.00';
                            $str.=','.'0.00';
                            $str.=','.'0.00';
                            $str.=','.'0.00';
                             $str.=','.'0.00';
                            $str.=','.'0.00';
                             $str.=','.$vatPer;
                            $str.=','.bcadd(round($vatAmt,2), 0, 2);

                          
                           
                            $str.= ',0.00 ,0.00 ,0.00 ';
                        
                           

                           
                        
                         $CsvData[]=$str;
                         $i++;
                 } 

                 }else{



                            $str='';
  
                             if($bill->type==1){
                                $str.='VAT';
                             }else{
                              $str.='GST';
                             }
                            
                            $str.=','.$bill->invoice_no;
                            $str.=','.date('d/m/y',strtotime($bill->invoiceDate));
                         
                            $str.=','.$bill->customer_code;

                              $companyName= str_replace(',', '', $bill->party->company_name);
                            $str.=','.$companyName;
                            $str.=','.$bill->party->gst_no;

                            
                            $str.=','.$bill->party->getstate->name;
                            $str.=','.bcadd($bill->grand_total_amount, 0, 2);
                            $str.=','.'0.00';;
                            $str.=','.'0.00';
                        

                            $str.=','.'0.00';
                            $str.=','.'0.00';
                            $str.=','.'0.00';
                            $str.=','.'0.00';
                            $str.=','.'0.00';
                            $str.=','.'0.00';
                             $str.=','.'0.00';
                            $str.=','.'0.00';
                             $str.=','.'0.00';
                            $str.=','.'0.00';

                          
                           
                            $str.= ',0.00 ,0.00 ,0.00 ';
                        
                           

                           
                        
                         $CsvData[]=$str;
                         $i++;




                 } 


                  $customerState=$bill->party->getstate->name;
                //  Log::info('Customer state   ------ '.print_r($customerState,true));
                  //   Log::info('rostate state   ------ '.print_r($rostate,true));
                   foreach($allTexByRo as $allTax)
                   {

                        if($rostate== $customerState){
//calculate cgst and sgst

                          if($allTax['GST_Type']=="CGST"){
                             $cgstPer=$allTax['Tax_percentage'];
                             $cgstAmt=($bill->handling_amount* $cgstPer)/100;


                          } 
                           if($allTax['GST_Type']=="UT/SGST"){
                             $sgstPer=$allTax['Tax_percentage'];
                             $sgstAmt=($bill->handling_amount* $sgstPer)/100;


                          } 


                        }else{
//calculate igst

                            if($allTax['GST_Type']=="IGST"){
                               $igstPer=$allTax['Tax_percentage'];
                               $igstAmt=($bill->handling_amount* $igstPer)/100;
                            } 
                        }


                   }
                      $totalCgstAmt=$totalCgstAmt+$cgstAmt;
                      $totalSgstAmt=$totalSgstAmt+$sgstAmt;  
                      $totalIgstAmt=$totalIgstAmt+$igstAmt; 
                      $totalSpcesstAmt=$totalSpcesstAmt+$spcessAmt;  
                                   /* bcadd($bill->grand_total_amount, 0, 2).','.bcadd($bill->taxable_amt, 0, 2).',,';*/
                   if($bill->handling_amount!="0.00"||$bill->handling_amount!="0"){
                                $str='VAT,'.$bill->invoice_no.','.date('d/m/y',strtotime($bill->invoiceDate)).','.$bill->customer_code.','.$companyName.','.$bill->party->gst_no.','.$bill->party->getstate->name.','.$bill->grand_total_amount.',0.00,0.00';

                                $str.=','. $cgstPer;
                            $str.=','.bcadd(round($cgstAmt,2), 0, 2);
                             $str.=','.$sgstPer;
                            $str.=','.bcadd(round($sgstAmt,2), 0, 2);
                             $str.=','.$igstPer;
                            $str.=','.bcadd(round($igstAmt,2), 0, 2);
                             $str.=','.'0.00';
                            $str.=','.'0.00';
                             $str.=','.'0.00';
                            $str.=','.'0.00';
                             
                               /* $str.=','.bcadd($bill->taxable_amt, 0, 2);*/
                                $str.=','.bcadd(round($bill->handling_amount,2), 0, 2);
                                $str.=','.bcadd(round($bill->discount,2), 0, 2);
                                $str.=','.bcadd(round($bill->rounded_of,2), 0, 2);
                                $CsvData[]=$str;


                    }

              }
                           
              $str=',,,,,,Grand Totals :,'. bcadd($totalFinalAmount, 0, 2).','.bcadd($totalTaxableValue, 0, 2).',0.00,,'.bcadd($totalCgstAmt, 0, 2).',,'.bcadd(round($totalSgstAmt,2), 0, 2).',,'.bcadd($totalIgstAmt, 0, 2).',,'.bcadd($totalSpcesstAmt, 0, 2).',,'.bcadd($totalVatAmt, 0, 2).','.bcadd($totalHandlingCharges, 0, 2).','.bcadd($totalDiscount, 0, 2).','.bcadd($totalRoundOff, 0, 2);
               Log::info('totalRoundOff  -- '.$totalRoundOff);
                      
              
              $CsvData[]=$str;
               
              $filename=date('Y-m-d')."-sales-register-summary-reports.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];

              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
    }

 
    public function salesRegisterBilled(Request $request){
         
         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }

      $fule_type=$request->input('fule_type');
      $from_date = $request->input('fromdate');
      $to_date = $request->input('to_date');

       $taxs=tax_master::get()->groupBy('GST_Type');
        if($fule_type==null){
           $fule_type=2;
         }
   
       
   

      if($from_date!=null && $to_date!=null){
        
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));

              
        $bills=InvoiceBilling::where('ro_code',$rocode)->where('type',$fule_type)->where('billing_from','>=',$from_dates)->where('billing_from','<=',$to_dates)->get();
        Log::info('ReportController@salesRegisterBilled  bills - '.print_r($bills,true));

      }


       return view('backend.reports.salesRegisterBilled',compact('taxs','fule_type','bills'));

    }

    /**
    * salesRegisterBilledDownload
    * @return \Illuminate\Http\Response
    */
    public function salesRegisterBilledDownload(Request $request,$fule_type){

   Log::info('ReportController@salesRegisterBilledDownload  input - '.print_r($request->all(),true));
         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }

         $taxs=tax_master::get()->groupBy('GST_Type');
       
        
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');

         $taxs=tax_master::get()->groupBy('GST_Type');
          if($fule_type==null){
             $fule_type=2;
           }

         $bills=InvoiceBilling::where('ro_code',$rocode)->where('type',$fule_type)->get();
     

        if($from_date!=null && $to_date!=null){

          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));

                
          $bills=$bills->where('billing_from','>=',$from_dates)->where('billing_from','<=',$to_dates);
        }



                
            $str='Inv.Type,Invoice No.,Invoice Date,Invoice Amount,Party Code,Party Name,Item Group,Item Sub-Group,Item Code,Item Name,HSN Code,Item Qty Sold,Item Rate';
             
              foreach($taxs as $key=>$tax){
                $str.=','.$key.'%,'.$key.' Amount';
              }
             
              $str.= ',Handling Charges,Discount Amount,Round Off';

              $CsvData=array($str);
                    
                    $i=1;
             foreach($bills as $bill){

                foreach($bill->trangection as $TransactionModel){
                            $str='';
  
                             if($bill->type==1){
                                $str.='VAT';
                             }else{
                              $str.='GST';
                             }
                            
                            $str.=','.$bill->invoice_no;
                            $str.=','.date('d/m/y',strtotime($bill->created_at));
                            $str.=','.bcadd($bill->grand_total_amount, 0, 2);
                            $str.=','.$bill->customer_code;
                            $str.=','.$bill->party->company_name;
                            $str.=','.$TransactionModel->getItemName->getgroup->Group_Name;
                            $str.=','.$TransactionModel->getItemName->getsubgroup->Group_Name;
                            $str.=','.$TransactionModel->getItemName->Item_Code;
                            $str.=','.$TransactionModel->getItemName->Item_Name;
                            $str.=','.$TransactionModel->getItemName->hsncode;
                            $str.=','.$TransactionModel->petroldiesel_qty;
                            $str.=','.bcadd($TransactionModel->item_price, 0, 2);

                            $tratex=$TransactionModel->gettex()->get()->groupby('GST_Type');
                            foreach($taxs as $key=>$tax){

                              if(isset($tratex[$key])){
                                  $getaxss=$tratex[$key]->first();

                                      $str.= ','.$getaxss->Tax_percentage.'%,'.bcadd($getaxss->Tax_Value, 0, 2);
                                 }else{

                                       $str.= ', , ';
                                   }
                              }
                           
                            $str.= ', , , ';
                        
                           

                           
                        
                         $CsvData[]=$str;
                         $i++;
                 }  

                      $str=',,,,,,,,,,,Totals,';

                      foreach($taxs as $key=>$tax){
                        $str.=',,';
                      }

                      $str.=','.bcadd($bill->handling_amount, 0, 2);
                      $str.=','.bcadd($bill->discount, 0, 2);
                      $str.=','.bcadd($bill->rounded_of, 0, 2);
                      $CsvData[]=$str;
              }
               
              $filename=date('Y-m-d')."-sales-register-billed-reports.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
    }
    /**
    * ItemwiseNozzleWiseReadingReport
    * @return \Illuminate\Http\Response
    */
   /* public function ItemwiseNozzleWiseReadingReport(Request $request){

        if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }
       
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');
          
        

        if($from_date!=null && $to_date!=null){

          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
          $reading=nozzelsReadingModel::where('RO_code',$rocode)->where('Nozzle_End','<>',null)->where('Reading_Date','>=',$from_dates." 00:00:00")->where('Reading_Date','<=',$to_dates." 23:59:59")->get();
           $reading=$reading->groupby('shift_id');
        }

       

        return view('backend.reports.ItemwiseNozzleWiseReadingReport',compact('reading'));
    }*/
    //Daily Total Shift Sales & Settlement Report

      public function DailyTotalShiftSalesAndSettlementReport(Request $request){

        if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }
       
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');
      /*   $readings=collect();
        $reading1=collect();*/

        if($from_date!=null && $to_date!=null){
             $readings=collect();
        $reading1=collect();


          //get default payment mode


          


        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));

        $dateArr=array();
        $date_from = strtotime($from_dates); 
        $date_to = strtotime($to_dates);          
        //make date range array
        for ($i=$date_from; $i<=$date_to; $i+=86400) {  
          $dateArr[]=date("Y-m-d", $i);
             
        } 

       
      //  $reading=collect();
       

        foreach($dateArr as $date){
             $totalSalesSum=0;
             $paymentDataFinal=array();
             $totalPaymentSum=0;
             $totalDiff=0;



        $paymentDataDefault = DB::table('tpl_payment_mode') 
                              
                      ->selectRaw( 'tpl_payment_mode.name,tpl_payment_mode.id,0 as total_sales,"" as totalAmount,"" as totalDiff')
                         ->where('tpl_payment_mode.IsActive',1)
                          ->where('tpl_payment_mode.ro_code',$rocode)
                          //->where('tbl_shift_settlement.shift_id','=',$shift_id)
                         // ->groupBy('paymentmode_sattlement.paymentmode_id')
                          ->orderBy('tpl_payment_mode.order_number')
                          ->get();

                    foreach($paymentDataDefault as $defaultdata) {
                                $default=new stdClass();
                                $default->total_sales=0.00;
                                $default->name=$defaultdata->name;
                                $default->totalAmount=0.00;
                                $default->totalDiff=0.00;

                              
                                //$paymentDataDefault[]= $default;

                      } 

            $paymentDataDefault2 = DB::table('tpl_payment_mode') 
                              
                      ->selectRaw( 'tpl_payment_mode.name,tpl_payment_mode.id,0 as total_sales,"" as totalAmount,"" as totalDiff')
                         ->where('tpl_payment_mode.IsActive',1)
                          ->where('tpl_payment_mode.ro_code',$rocode)
                          //->where('tbl_shift_settlement.shift_id','=',$shift_id)
                         // ->groupBy('paymentmode_sattlement.paymentmode_id')
                          ->orderBy('tpl_payment_mode.order_number')
                          ->get();

                    foreach($paymentDataDefault2 as $defaultdata2) {
                                $default2=new stdClass();
                                $default2->total_sales=0.00;
                                $default2->name=$defaultdata2->name;
                                $default2->totalAmount=0.00;
                                $default2->totalDiff=0.00;
                      } 
           
        


            $reading=nozzelsReadingModel::join('shifts','tbl_ro_nozzle_reading.shift_id','=','shifts.id')
                          ->join('tbl_pedestal_nozzle_master', function($join)
                       {
                         $join->on('tbl_pedestal_nozzle_master.Nozzle_Number','=','tbl_ro_nozzle_reading.Nozzle_No');
                         $join->on('tbl_pedestal_nozzle_master.RO_code','=','tbl_ro_nozzle_reading.RO_code');

                       })


           // ->Join('tbl_pedestal_nozzle_master','tbl_pedestal_nozzle_master.Nozzle_Number','=','tbl_ro_nozzle_reading.Nozzle_No')


              ->Join('tbl_price_list_master','tbl_pedestal_nozzle_master.fuel_type','=','tbl_price_list_master.id')
              ->where('tbl_ro_nozzle_reading.RO_code',$rocode)
              ->where('Nozzle_End','<>',null)
              ->whereDate('shifts.transation_date','=',$date)
             // ->where('shifts.created_at','<=',$to_dates." 23:59:59")
                 ->where('shifts.fuel','=',1)
              ->groupBy('date')
              ->groupBy('itemName')
             ->selectRaw('tbl_ro_nozzle_reading.*,
                SUM(tbl_ro_nozzle_reading.test) as test,
                SUM(tbl_ro_nozzle_reading.reading) as reading,
                SUM(tbl_ro_nozzle_reading.Nozzle_Start) as Nozzle_Start,
                SUM(tbl_ro_nozzle_reading.Nozzle_End) as Nozzle_End,
                DATE_FORMAT(shifts.transation_date, \'%d-%m-%Y\') as created_date,DATE_FORMAT(shifts.transation_date, \'%H:%i:%s\') as created_time,tbl_price_list_master.Item_Name as itemName,DATE(shifts.transation_date) as date,SUM((tbl_ro_nozzle_reading.reading)*tbl_ro_nozzle_reading.price)  as totalSales,(reading-test) as finalQty,"" as payment_sales,"0.00" as totalAmount,"0.00" as totalDiff,shifts.transation_date as shiftDate')

             ->get();

             $paymentData=DB::select( DB::raw("select t2.paymentmode_id,sum(amount) as total_sales,t1.name from tpl_payment_mode t1 left join paymentmode_sattlement t2 on t1.id=t2.paymentmode_id left join tbl_shift_settlement t3 on t3.id=t2.sattlement_id inner join shifts t4 on t4.id=t3.shift_id where date(t4.transation_date)='$date' and t1.ro_code='$rocode' group by t2.paymentmode_id order by order_number") );


           $otherArr=DB::select( DB::raw("select sum(Credit_fuel)  as total_sales_fuel from tbl_shift_settlement t1  inner join shifts t2 on t1.shift_id=t2.id where date(t2.transation_date) = '$date' and t2.Ro_Code='$rocode'") );

           $lubeArr=DB::select( DB::raw("select sum(Credit_lube)  as total_sales_fuel from tbl_shift_settlement t1  inner join shifts t2 on t1.shift_id=t2.id where date(t2.transation_date) = '$date' and t2.Ro_Code='$rocode'") );


           $salesData = DB::table('tbl_shift_settlement')
                        ->join('shifts','tbl_shift_settlement.shift_id','=','shifts.id')
                        ->selectRaw('SUM(Credit_receipt) AS credit_sales, SUM(manual_receipt) AS manual_sales,shifts.id,shifts.transation_date,shifts.ro_code')
                       ->whereDate('shifts.transation_date','=',$date)
                       ->where('shifts.ro_code',$rocode)
                         ->first(); 

                                //dd($salesData);   

             //getcredit sales   

             $shift_id=0;
             $creditsales=0;

               $creditsales=$salesData->credit_sales+$salesData->manual_sales; 
               $totalSalesData=array();

               //dd($date,$reading);
           
             foreach($reading as $data){
             // $mycollect=collect($data);
             
                       $totalSalesData=$reading->where('shift_id',$data->shift_id)->pluck('totalSales');
                        $data->payment_sales=$paymentDataDefault2;
                     //   $reading->put($data);

                       
             }  
             //dd($reading);  
                       

         foreach($totalSalesData as $totalSalesSumVal){

             $totalSalesSum=$totalSalesSum+bcadd($totalSalesSumVal, 0,2);
           }

           $other=0.00;
           $lube=0.00;
           if($otherArr['0']){

            $other=$otherArr['0']->total_sales_fuel;
           }
           if($lubeArr['0']){
            $lube=$lubeArr['0']->total_sales_fuel;
            
           }  

        $totalSalesSum=$totalSalesSum+$lube+$other;



        //update payment mode
         foreach($paymentDataDefault  as $defaultPayValue){

                   foreach($paymentData  as $payValue){
                             if($defaultPayValue->name==$payValue->name){
                                 $defaultPayValue->total_sales=$payValue->total_sales;

                              }

                    
                    }


         }
                        Log::info('paymentDataDefault - '.print_r($paymentDataDefault,true));
                          $paymentDataFinal=$paymentDataDefault;

                           $creditsalesObj = collect();
                           $creditsalesObj->total_sales= $creditsales;
                           $creditsalesObj->id="";
                           $creditsalesObj->name="Credit Sale Value";
                           $creditsalesObj->Nozzle_Start="0.00";
                           $creditsalesObj->Nozzle_End="0.00";

                          $creditsalesObj->totalAmount="0.00";
                          $creditsalesObj->totalDiff="0.00";

                          $paymentDataFinal->prepend($creditsalesObj);


                           $creditsalesObjDefault = collect();
                           $creditsalesObjDefault->total_sales= "0.00";
                           $creditsalesObjDefault->id="";
                           $creditsalesObjDefault->name="Credit Sale Value";
                           $creditsalesObjDefault->Nozzle_Start="0.00";
                           $creditsalesObjDefault->Nozzle_End="0.00";

                          $creditsalesObjDefault->totalAmount="0.00";
                          $creditsalesObjDefault->totalDiff="0.00";


                        

                          $paymentDataDefault2->prepend($creditsalesObjDefault); 


                         // $data->payment_sales=$paymentDataDefault2;


                         /* if(!$reading->isEmpty()){

                            $reading1=$reading->push($reading);
                            }*/
  $datesForView = date('d-m-Y', strtotime($date));
                          // dd($datesForView);

                          if($lube){

                                $lubeCollect = new \Illuminate\Database\Eloquent\Collection;
                                 $lubeCollect->created_date=$datesForView;
                                 $lubeCollect->itemName="Lubes";
                                 $lubeCollect->reading="0.00";
                                 $lubeCollect->test="0.00";
                                 $lubeCollect->Nozzle_Start="0.00";
                                 $lubeCollect->Nozzle_End="0.00";
                                 $lubeCollect->finalQty="0.00";
                                 $lubeCollect->price="0.00";
                               
                                 $lubeCollect->totalSales=$lube;
                                 $lubeCollect->payment_sales=$paymentDataDefault2;
                                  $lubeCollect->totalAmount="0.00";
                                 $lubeCollect->totalDiff="0.00";
                                 
                                 //Log::info(' lubeCollect  caLL !!!!!!');
                                $reading1=$reading->push($lubeCollect);


                          }

                       
// get other data


                        if($lube){

                               $otherCollect = new \Illuminate\Database\Eloquent\Collection;
                               $otherCollect->created_date=$datesForView;
                               $otherCollect->itemName="Others";
                               $otherCollect->reading="0.00";
                               $otherCollect->test="0.00";
                                $otherCollect->Nozzle_Start="0.00";
                               $otherCollect->Nozzle_End="0.00";
                               $otherCollect->finalQty="0.00";
                               $otherCollect->price="0.00";
                               $otherCollect->totalSales=$other;
                               $otherCollect->payment_sales=$paymentDataDefault2;
                                $otherCollect->totalAmount="0.00";
                               $otherCollect->totalDiff="0.00";
                             

                              // Log::info(' otherCollect  caLL !!!!!!');
                              $reading1=$reading->push($otherCollect);

                         }  

                  foreach($paymentDataFinal as $paymentValue)
                    {
                      $totalPaymentSum=$totalPaymentSum+$paymentValue->total_sales;

                    }
                    $totalDiff=  $totalPaymentSum-$totalSalesSum;   


                     if(!$reading->isEmpty()){

                 $settlmntCollect = new \Illuminate\Database\Eloquent\Collection;
                         $settlmntCollect->created_date=$datesForView;
                         $settlmntCollect->itemName="Settlements";
                         $settlmntCollect->reading="0.00";
                         $settlmntCollect->test="0.00";
                          $settlmntCollect->Nozzle_Start="0.00";
                         $settlmntCollect->Nozzle_End="0.00";
                         $settlmntCollect->finalQty="0.00";
                         $settlmntCollect->price="0.00";
                         $settlmntCollect->totalSales="0.00";

                         
                         $settlmntCollect->payment_sales=$paymentDataFinal;
                         $settlmntCollect->totalAmount=$totalPaymentSum;
                         $settlmntCollect->totalDiff=$totalDiff;

                         //dd($settlmntCollect);

                 $reading1=$reading->push($settlmntCollect);


                   $totalCollect = new \Illuminate\Database\Eloquent\Collection;
                         $totalCollect->created_date=$datesForView;
                         $totalCollect->itemName="Totals";
                         $totalCollect->reading="0";
                         $totalCollect->test="0";
                         $totalCollect->finalQty="";
                         $totalCollect->price="";
                         $totalCollect->Nozzle_Start="0.00";
                                 $totalCollect->Nozzle_End="0.00";
                         $totalCollect->totalSales=$totalSalesSum;
                         $totalCollect->payment_sales=$paymentDataFinal;
                         $totalCollect->totalAmount=$totalPaymentSum;
                         $totalCollect->totalDiff=$totalDiff;

                 $reading1=$reading->push($totalCollect);   

               }

  
$readings->push($reading);




        }//end date foreach


         
        /* $merged_collection = collect();

               foreach($readings as $collection) {

              $merged_collection->merge($collection);

           }
           dd($merged_collection,$readings,$reading);*/


        log::info('date array - '.print_r($dateArr,true));

      }//date check





//dd($readings);


       
//dd($readings);
        return view('backend.reports.DailyTotalShiftSalesAndSettlementReport',compact('readings','paymentDataFinal'));
    }


     public function ItemwiseNozzleWiseReadingReport(Request $request){


      Log::info('Reportcontroller@ItemwiseNozzleWiseReadingReport input - '.print_r($request->all(),true));

        if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }
        Log::info('Reportcontroller@ItemwiseNozzleWiseReadingReport rocode - '.print_r($rocode,true));
       
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');
          
        

        if($from_date!=null && $to_date!=null){

          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
          $readings=nozzelsReadingModel::join('shifts','tbl_ro_nozzle_reading.shift_id','=','shifts.id')
          ->where('tbl_ro_nozzle_reading.RO_code',$rocode)
          ->where('Nozzle_End','<>',null)
          ->where('shifts.transation_date','>=',$from_dates." 00:00:00")
          ->where('shifts.transation_date','<=',$to_dates." 23:59:59")
          ->orderBy('shifts.transation_date')
         ->selectRaw('tbl_ro_nozzle_reading.*,DATE_FORMAT(shifts.transation_date, \'%d-%m-%Y\') as created_date,DATE_FORMAT(shifts.transation_date, \'%H:%i:%s\') as created_time')
          ->get();
           $readings=$readings->groupby('shift_id');

           //dd($readings,$rocode);

          Log::info('reading   count  - '.print_r(count($readings),true));
        }

        return view('backend.reports.ItemwiseNozzleWiseReadingReport',compact('readings','rocode'));
    }

     /**
      * downloadNozzleReadingReport
      * @return \Illuminate\Http\Response
      */
    public function downloadNozzleReadingReport(Request $request){

      Log::info('Reportcontroller@downloadNozzleReadingReport input - '.print_r($request->all(),true));

         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }
       
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');
          
        

        if($from_date!=null && $to_date!=null){

          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
          $reading=nozzelsReadingModel::where('RO_code',$rocode)->where('Nozzle_End','<>',null)->where('Reading_Date','>=',$from_dates." 00:00:00")->where('Reading_Date','<=',$to_dates." 23:59:59")->get();
           $reading=$reading->groupby('shift_id');
        }

                
              $str='S No.,Shift,Item,Tank No.,Totalizer Number,OMR,CMR,Totalizer Out Qty,Totalizer Test Qty,Nett Sales Qty,Credit Sales Qty,Other Sales Qty';
             
              $CsvData=array($str);
                    
                    $i=1;
                     $j=1;
             foreach($reading as $shifts){
                $creditsales=0; $i=0; $natesales=0;
                $GtalizerOutQty=0;
                $GnettSalesQty=0;
                $testsales=0;

                foreach($shifts as $reading){
                  $str=$j;

                  $totalizerOutQty=0;
                  $nettSalesQty=0;
                  $otherSalesQty=0;
                 

                  if($i==0){
                  
                    foreach ($reading->getCreditSales as $getCreditSale) {
                      $creditsales=$getCreditSale->petroldiesel_qty+$creditsales;
                    }
                  
                  }
                  $natesales=($reading->Nozzle_End-$reading->Nozzle_Start)+$natesales;
                  $totalizerOutQty=$reading->Nozzle_End-$reading->Nozzle_Start;
                  $nettSalesQty=$reading->Nozzle_End-$reading->Nozzle_Start-$reading->test;

                  $GtalizerOutQty=$totalizerOutQty+$GtalizerOutQty;
                  $GnettSalesQty=$nettSalesQty+$GnettSalesQty;
                  $testsales=$reading->test+$testsales;

                  if($reading->getShift!=null){

                    $str.=','.date('d/m/Y  h:i:s A',strtotime($reading->getShift->created_at)).'  To  '. date('d/m/Y  h:i:s A',strtotime($reading->getShift->closer_date));
                  }else{
                     $str.=',';
                  }
                 
                  $str.=','.$reading->getNozzle->getItem->Item_Name;
                  $str.=','.$reading->getNozzle->getTank->Tank_Number;
                  $str.=','.$reading->Nozzle_No;
                  $str.=','.$reading->Nozzle_Start;
                  $str.=','.$reading->Nozzle_End;
                  $str.=','.$totalizerOutQty;
                  $str.=','.$reading->test;
                  $str.=','.$nettSalesQty;
                  $str.=', ';
                  $str.=', ';
                  $CsvData[]=$str;
                  $j++;
                 }
                 $str='';
                  $str.=', ';
                  $otherSalesQty=$GnettSalesQty-$creditsales;
                 
                  $str.=', ';
                  $str.=', ';
                  $str.=', ';
                  $str.=', ';
                  $str.=', Totals';
                  $str.=','.$GtalizerOutQty;
                  $str.=','.$testsales;
                  $str.=','.$GnettSalesQty;
                  $str.=','.$creditsales;
                  $str.=','.$otherSalesQty;
                  $CsvData[]=$str;
              }
               
              $filename=date('Y-m-d')."-item-wise-nozzle-wise-reading-report.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
  }
  
  /**
  * partywiseItemwiseDatewiseDeliveriesPending
  * @return \Illuminate\Http\Response
  */
   public function partywiseItemwiseDatewiseDeliveriesPending(Request $request){


Log::info('ReportController@partywiseItemwiseDatewiseDeliveriesPending - input -'.print_r($request->all(),true));
        if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');
        $request_type=$request->input('request_type');

        $parties=RoCustomertManagement::where('RO_code',$rocode)->get();
          
        if($from_date!=null && $to_date!=null){

          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
          if($request_type==1){

            $requests=AddPetrolDieselModel::where('RO_code',$rocode)->where('Execution_date',null)
            ->where('request_date','>=',$from_dates." 00:00:00")->where('request_date','<=',$to_dates." 23:59:59")
            ->orderBy('request_date')
            ->get();
          }elseif($request_type==2){
            $requests=CustomerLubeRequest::where('RO_code',$rocode)->where('Execution_date',null)
                   ->where('request_date','>=',$from_dates." 00:00:00")->where('request_date','<=',$to_dates." 23:59:59")
                   ->orderBy('request_date')
                  ->select('tbl_customer_request_lube.*','tbl_customer_request_lube.quantity as Request_Value') 
            ->get();

          }else{
            Log::info('Request_Type  =   '.$request_type);
              $requests1=AddPetrolDieselModel::where('RO_code',$rocode)->where('Execution_date',null)
              ->where('request_date','>=',$from_dates." 00:00:00")->where('request_date','<=',$to_dates." 23:59:59")
              ->orderBy('request_date')
              ->get();
              $requests2=CustomerLubeRequest::where('RO_code',$rocode)->where('Execution_date',null)
                  ->where('request_date','>=',$from_dates." 00:00:00")->where('request_date','<=',$to_dates." 23:59:59")
                    ->select('tbl_customer_request_lube.*','tbl_customer_request_lube.quantity as Request_Value')
                  ->orderBy('request_date')
              ->get();
              $requests= $requests1->merge($requests2); 
              //dd($requests1,$requests2,$requests);

          }
          
          if($request->input('party')!=null && trim($request->input('party'))!='')
            $requests=$requests->where('customer_code',$request->input('party'));




          /*$requests=$requests->where('request_date','>=',$from_dates." 00:00:00")->where('request_date','<=',$to_dates." 23:59:59");*/
        
        }


      return view('backend.reports.partywiseItemwiseDatewiseDeliveriesPending',compact('request_type','parties','requests'));
    }

   /**
    * downloadPartywiseItemwiseDatewiseDeliveriesPendingReport
    * @return \Illuminate\Http\Response
    */
    public function downloadPartywiseItemwiseDatewiseDeliveriesPendingReport(Request $request){


      Log::info('ReportController@downloadPartywiseItemwiseDatewiseDeliveriesPendingReport - input -'.print_r($request->all(),true));

        if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }


         $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');
        $request_type=$request->input('request_type');

        $parties=RoCustomertManagement::where('RO_code',$rocode)->get();
          
        if($from_date!=null && $to_date!=null){

          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
          if($request_type==1){

            $requests=AddPetrolDieselModel::where('RO_code',$rocode)->where('Execution_date',null)
            ->where('request_date','>=',$from_dates." 00:00:00")->where('request_date','<=',$to_dates." 23:59:59")
            ->orderBy('request_date')
            ->get();
          }elseif($request_type==2){
            $requests=CustomerLubeRequest::where('RO_code',$rocode)->where('Execution_date',null)
                   ->where('request_date','>=',$from_dates." 00:00:00")->where('request_date','<=',$to_dates." 23:59:59")
                   ->orderBy('request_date')
                  ->select('tbl_customer_request_lube.*','tbl_customer_request_lube.quantity as Request_Value') 
            ->get();

          }else{
            Log::info('Request_Type  =   '.$request_type);
              $requests1=AddPetrolDieselModel::where('RO_code',$rocode)->where('Execution_date',null)
              ->where('request_date','>=',$from_dates." 00:00:00")->where('request_date','<=',$to_dates." 23:59:59")
              ->orderBy('request_date')
              ->get();
              $requests2=CustomerLubeRequest::where('RO_code',$rocode)->where('Execution_date',null)
                  ->where('request_date','>=',$from_dates." 00:00:00")->where('request_date','<=',$to_dates." 23:59:59")
                    ->select('tbl_customer_request_lube.*','tbl_customer_request_lube.quantity as Request_Value')
                  ->orderBy('request_date')
              ->get();
              $requests= $requests1->merge($requests2); 
              //dd($requests1,$requests2,$requests);

          }
          
          if($request->input('party')!=null && trim($request->input('party'))!='')
            $requests=$requests->where('customer_code',$request->input('party'));




          /*$requests=$requests->where('request_date','>=',$from_dates." 00:00:00")->where('request_date','<=',$to_dates." 23:59:59");*/
        
        }

                
              $str='Slip Date,Requisition / Slip No.,Party Code,Party Name,Vehicle Number,Product Ordered,Request Type,Qty.Order';

                          
              $CsvData=array($str);
                    
                    
             foreach($requests as $requ){
             
                  $str=date('d/m/y',strtotime($requ->request_date));
                  $str.=','.$requ->request_id;
                  $str.=','.$requ->customer_code;

                  if($requ->getCustomer!=null)
                    $str.=','.$requ->getCustomer->company_name;
                  else
                     $str.=', ';

                   $str.=','.$requ->Vehicle_Reg_No;

                 

                       if($requ->itemname!=null)
                          $str.=','.$requ->itemname->Item_Name;
                        else
                           $str.=', ';

                         $str.=','.$requ->Request_Type;
                         $str.=','.$requ->Request_Value;

                 
                     
                  $CsvData[]=$str;
              }
               
              $filename=date('Y-m-d')."-item-wise-nozzle-wise-reading-report.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
  }

  /**
  * Partywise - Credit Limits & Status Report
  * @return \Illuminate\Http\Response
  */
  public function partywiseCreditLimitsandStatusReport(){

        if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }
        
        $Credit_limits=Credit_limit::where('tbl_customer_limit.RO_code',$rocode)
        ->join('tbl_customer_master', 'tbl_customer_master.Customer_Code', '=', 'tbl_customer_limit.Customer_Code')
        ->select('tbl_customer_limit.*','tbl_customer_master.Customer_Name','tbl_customer_master.is_active')->get();

          //Log::info('data   ======'.print_r($Credit_limits,true));
       
        return view('backend.reports.partywise-Credit-Limits-Report',compact('Credit_limits'));
 
  }

  /**
  * Partywise - Credit Limits & Status Report
  * @return \Illuminate\Http\Response
  */
  public function downloadPartywiseCreditLimitsandStatusReport(){

      if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
      }
      
      $Credit_limits=Credit_limit::where('tbl_customer_limit.RO_code',$rocode)
        ->join('tbl_customer_master', 'tbl_customer_master.Customer_Code', '=', 'tbl_customer_limit.Customer_Code')
        ->select('tbl_customer_limit.*','tbl_customer_master.Customer_Name','tbl_customer_master.is_active')->get();


              $str='S.no.,Party Code,Party Name,Status,Last Sale Date,Last Sale Value,Total Sale Value Yr. to Date,Current Unbilled Value,Current O/S Value,Last Bill O/s Date,Credit Limit Rs.,Credit Limit Days,Billing Cycle,Pymt Performance';

              $CsvData=array($str);
              
              $i=1;    
             foreach($Credit_limits as $CreditLimit){
             
                  $str=$i;
                  $str.=','.$CreditLimit->Customer_Code;
                  $str.=','.$CreditLimit->getcustomer->company_name;

                  if($CreditLimit->getcustomer->is_active==1)
                    $str.=',Active';
                  else
                     $str.=',Inactive';


                 if($CreditLimit->last_sale_date!=null){
                   $str.=','.date('d/m/Y',strtotime($CreditLimit->last_sale_date));
                 }else{
                   $str.=', ';
                 }

                 $str.=','.bcadd($CreditLimit->last_sale,0,2);
                 $str.=','.bcadd($CreditLimit->total_sale,0,2);
                 $str.=','.bcadd($CreditLimit->current_unbilled,0,2);
                 $str.=','.bcadd($CreditLimit->available_creadit_limit,0,2);
                
                if($CreditLimit->last_bill_Os_date!=null)
                    $str.=','.date('d/m/Y',strtotime($CreditLimit->last_bill_Os_date));
                else
                    $str.=', ';

                  $str.=','.bcadd($CreditLimit->Limit_Value,0,2);
                  $str.=','.$CreditLimit->no_of_days;
                  $str.=', ';
                  $str.=', ';
                     
                  $CsvData[]=$str;
                  $i++;
              }
               
              $filename=date('Y-m-d')."-Partywise-Credit-Limits-and-Status-Report.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);

  }

       /**
      * salesRegisterBilled
      * @return \Illuminate\Http\Response
      */
    public function partywiseVehiclewiseSalesReport(Request $request){
         
       if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }

      $fule_type=$request->input('fule_type');
      $from_date = $request->input('fromdate');
      $to_date = $request->input('to_date');
      $customer_code = $request->input('customer_code');

       $taxs=tax_master::get()->groupBy('GST_Type');
        if($fule_type==null){
           $fule_type=2;
         }
   
       
      $parties=RoCustomertManagement::where('RO_code',$rocode)->get();

      if($from_date!=null && $to_date!=null){
        
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));


             $transactionModels =TransactionModel::where('RO_code',$rocode)
                              ->where('customer_code',$customer_code)
                              ->whereBetween('trans_date', [date('Y-m-d',strtotime($from_dates))." 00:00:00", date('Y-m-d',strtotime($to_dates))." 23:59:59"])
                              ->orderBy('Vehicle_Reg_No', 'ASC')
                              ->get();
      }


       return view('backend.reports.Partywise-Vehiclewise-Sales-Report',compact('taxs','fule_type','transactionModels','parties'));

    }

    /**
    * salesRegisterBilledDownload
    * @return \Illuminate\Http\Response
    */
    public function partywiseVehiclewiseSalesReportDownload(Request $request){


        if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }

      $fule_type=$request->input('fule_type');
      $from_date = $request->input('fromdate');
      $to_date = $request->input('to_date');
      $customer_code = $request->input('customer_code');

       $taxs=tax_master::get()->groupBy('GST_Type');
        if($fule_type==null){
           $fule_type=2;
         }
   
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));


             $transactionModels =TransactionModel::where('RO_code',$rocode)
                              ->where('customer_code',$customer_code)
                              ->whereBetween('trans_date', [date('Y-m-d',strtotime($from_dates))." 00:00:00", date('Y-m-d',strtotime($to_dates))." 23:59:59"])
                              ->orderBy('Vehicle_Reg_No', 'ASC')
                              ->get();
      

                
              $str='S.No.,Invoice Date,Invoice No.,Party Code,Party Name,Vehicle Regn.No.,Item Sold Name,Item Sold Qty,Item Rate,Item Sold Value';
              $CsvData=array($str);
                    
                    $i=1;
           
                foreach($transactionModels as $TransactionModel){
                            $str=$i;
                            $str.=','.date('d/m/y',strtotime($TransactionModel->trans_date));
                            $str.=','.$TransactionModel->bill_no;
                            $str.=','.$TransactionModel->customer_code;
                            $str.=','.$TransactionModel->getCustomername->company_name;
                            $str.=','.$TransactionModel->Vehicle_Reg_No;
                            $str.=','.$TransactionModel->getItemName->Item_Name;
                            $str.=','.bcadd(round($TransactionModel->petroldiesel_qty,2),0,2);
                            $str.=','.bcadd(round($TransactionModel->item_price,2),0,2);
                            $str.=','.bcadd(round($TransactionModel->item_price*$TransactionModel->petroldiesel_qty,2),0,2);

                         $CsvData[]=$str;
                         $i++;
                

              }
               
              $filename=date('Y-m-d')."-partywise-Vehiclewise-Sales-Report.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");

              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
    }


       /**
      * salesRegisterBilled
      * @return \Illuminate\Http\Response
      */
    public function salesmanWiseSalesReport(Request $request){
         
         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }

      $fule_type=$request->input('fule_type');
      $from_date = $request->input('fromdate');
      $to_date = $request->input('to_date');


       $taxs=tax_master::get()->groupBy('GST_Type');
        if($fule_type==null){
           $fule_type=2;
         }
   
       
      

      if($from_date!=null && $to_date!=null){
        
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));

             $RoPersonalManagements=RoPersonalManagement::with(['getTrangections' => function ($query) use ($from_dates,$to_dates) {

                  $query=$query->whereBetween('trans_date', [date('Y-m-d',strtotime($from_dates))." 00:00:00", date('Y-m-d',strtotime($to_dates))." 23:59:59"]);
           
            }])->where('RO_Code',$rocode)->whereIn('Designation',[29,30])->get();
            
      }


       return view('backend.reports.SalesmanWise-Sales-Report',compact('RoPersonalManagements'));

    }

    /**
    * salesRegisterBilledDownload
    * @return \Illuminate\Http\Response
    */
    public function salesmanWiseSalesReportDownload(Request $request){


        if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }

      $fule_type=$request->input('fule_type');
      $from_date = $request->input('fromdate');
      $to_date = $request->input('to_date');
      $customer_code = $request->input('customer_code');

       $taxs=tax_master::get()->groupBy('GST_Type');
        if($fule_type==null){
           $fule_type=2;
         }
   
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));


            $RoPersonalManagements=RoPersonalManagement::with(['getTrangections' => function ($query) use ($from_dates,$to_dates) {

                  $query=$query->whereBetween('trans_date', [date('Y-m-d',strtotime($from_dates))." 00:00:00", date('Y-m-d',strtotime($to_dates))." 23:59:59"]);
           
            }])->where('RO_Code',$rocode)->whereIn('Designation',[29,30])->get();
      

                
              $str='S.No.,Invoice Date,Invoice No.,Salesman Name,Shift,Party Code,Party Name,Item Sold Name,Item Sold Qty,Item Rate,Item Sold Value';
              $CsvData=array($str);
                    
                    $i=1;
           foreach($RoPersonalManagements as $RoPersonalManagement){
                $amount=0;
                if($RoPersonalManagement->getTrangections->count()>0){
                  
                      foreach($RoPersonalManagement->getTrangections as $TransactionModel){

                              $amount=bcadd(round($TransactionModel->item_price*$TransactionModel->petroldiesel_qty,2),0,2)+$amount;
                                  $str=$i;
                                  $str.=','.date('d/m/y',strtotime($TransactionModel->trans_date));
                                  $str.=','.$TransactionModel->bill_no;
                                  $str.=','.$RoPersonalManagement->Personnel_Name;
                                  
                                  if($TransactionModel->getShift!=null)        
                                    $str.=','.date('d/m/Y  h:i:s A',strtotime($TransactionModel->getShift->created_at)).' To '.date('d/m/Y  h:i:s A',strtotime($TransactionModel->getShift->closer_date));
                                  else
                                    $str.=', ';

                                  $str.=','.$TransactionModel->customer_code;
                                  $str.=','.$TransactionModel->getCustomername->company_name;
                                  $str.=','.$TransactionModel->getItemName->Item_Name;
                                  $str.=','.bcadd(round($TransactionModel->petroldiesel_qty,2),0,2);
                                  $str.=','.bcadd(round($TransactionModel->item_price,2),0,2);
                                  $str.=','.bcadd(round($TransactionModel->item_price*$TransactionModel->petroldiesel_qty,2),0,2);

                               $CsvData[]=$str;
                               $i++;
                      

                    }
                    $str=', , , , , , , ,Total, ,'.$amount;
                    $CsvData[]=$str;
                }
           }
               
              $filename=date('Y-m-d')."-salesman-Wise-Sales-Report.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");

              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
    }

       /**
      * salesRegisterBilled
      * @return \Illuminate\Http\Response
      */
    public function partyDriverVehicleListings(Request $request){
         
        if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
            
        }

        $customer_code=$request->input('customer_code');

        $CustomerDrivers=CustomerDriverManager::where('Ro_code',$rocode);

       if($customer_code!=null && $customer_code!='all')
        $CustomerDrivers=$CustomerDrivers->where('customer_code',$customer_code);

       if(Auth::user()->user_type==4 && $request->session()->has('custm'))
         $CustomerDrivers=$CustomerDrivers->where('customer_code',$request->session()->get('custm'));

        $CustomerDrivers=$CustomerDrivers->get();

        $parties=RoCustomertManagement::where('RO_code',$rocode)->get();
       


       return view('backend.reports.Party-Driver-Vehicle-Listings',compact('CustomerDrivers','parties'));

    }

    /**
    * salesRegisterBilledDownload
    * @return \Illuminate\Http\Response
    */
    public function partyDriverVehicleListingsDownload(Request $request){


        if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }

        $customer_code=$request->input('customer_code');
        $CustomerDrivers=CustomerDriverManager::where('Ro_code',$rocode);

       if($customer_code!=null && $customer_code!='all')
        $CustomerDrivers=$CustomerDrivers->where('customer_code',$customer_code);

      if(Auth::user()->user_type==4 && $request->session()->has('custm'))
         $CustomerDrivers=$CustomerDrivers->where('customer_code',$request->session()->get('custm'));

        $CustomerDrivers=$CustomerDrivers->get();

        $parties=RoCustomertManagement::where('RO_code',$rocode)->get();
                    
            $i=1;

               
                
              $str='S.No.,Party Code,Party Name,Vehicle Regn.No.,Vehicle Make,Vehicle Model,Vehicle Color,Vehicle Fuel,Vehicle Insurance Co.,Vehicle Insurance Due On,Vehicle PUC Due On,Vehicle Regn.Valid upto,Driver Name,Driver Mobile,Driver License Validity';
              $CsvData=array($str);
                    
                    
          foreach($CustomerDrivers as $CustomerDriver){

              $str=$i;
              $str.=','.$CustomerDriver->customer_code;
               if($CustomerDriver->getCustomer!=null)
                  $str.=','.$CustomerDriver->getCustomer->company_name;
                else
                 $str.=',';
               
             
              $str.=','.$CustomerDriver->Registration_Number;

              if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->getmake!=null)
                $str.=','.$CustomerDriver->getVihicle->getmake->name;
              else
                $str.=',';
              
              if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->getmmodel!=null)
                  $str.=','.$CustomerDriver->getVihicle->getmmodel->name;
              else
                 $str.=',';

               if($CustomerDriver->getVihicle!=null)
                $str.=','.$CustomerDriver->getVihicle->van_color;
              else
                 $str.=',';


                if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->getitem!=null)
                 $str.=','.$CustomerDriver->getVihicle->getitem->Item_Name;
                else
                 $str.=',';


                if($CustomerDriver->getVihicle!=null)
                  $str.=','.$CustomerDriver->getVihicle->industry_company;
                else
                 $str.=',';

             

              if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->insurance_due_date!=null)
                $str.=','.date('d/m/Y',strtotime($CustomerDriver->getVihicle->insurance_due_date));
              else
                 $str.=',';

                if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->puc_date!=null)
                $str.=','.date('d/m/Y',strtotime($CustomerDriver->getVihicle->puc_date));
              else
                 $str.=',';

                if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->rc_valide!=null)
                $str.=','.date('d/m/Y',strtotime($CustomerDriver->getVihicle->rc_valide));
              else
                 $str.=',';


              $str.=','.$CustomerDriver->Driver_Name;
              $str.=','.$CustomerDriver->Driver_Mobile;
              if($CustomerDriver->valid_upto!=null)
                $str.=','.date('d/m/Y',strtotime($CustomerDriver->valid_upto));
              else
                 $str.=',';


             $CsvData[]=$str;
             $i++;
          

          }
            
               
           
               
              $filename=date('Y-m-d')."-Party-Driver-Vehicle-Listings.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");

              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
    }

  
  /**
  * PartywiseItemwiseDatewiseDeliveriesPendingforBilling
  * @return \Illuminate\Http\Response
  */
   public function PartywiseItemwiseDatewiseDeliveriesPendingforBilling(Request $request){

        if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }

        $request_type=1;
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');

        if($request->input('request_type')!=null)
           $request_type=$request->input('request_type');

        
        $parties=RoCustomertManagement::where('RO_code',$rocode)->get();
          
        if($from_date!=null && $to_date!=null){
          
       
          $Transactions=TransactionModel::where('RO_code',$rocode)->where('petrol_or_lube',$request_type)->where('status',0)->get();
       
        if($request->input('party')!=null && trim($request->input('party'))!='')
            $Transactions=$Transactions->where('customer_code',$request->input('party'));



          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
          $Transactions=$Transactions->where('trans_date','>=',$from_dates." 00:00:00")->where('trans_date','<=',$to_dates." 23:59:59");
        
        }
       
    
        return view('backend.reports.Partywise-Itemwise-Datewise-Deliveries-Pending-Billing',compact('request_type','parties','Transactions'));
    }

   /**
    * downloadPartywiseItemwiseDatewiseDeliveriesPendingforBillingReport
    * @return \Illuminate\Http\Response
    */
    public function partywiseItemwiseDatewiseDeliveriesPendingforBillingDownload(Request $request){

         if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }

        $request_type=1;
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');

        if($request->input('request_type')!=null)
           $request_type=$request->input('request_type');

        
        $parties=RoCustomertManagement::where('RO_code',$rocode)->get();
          
        if($from_date!=null && $to_date!=null){
          
       
          $Transactions=TransactionModel::where('RO_code',$rocode)->where('petrol_or_lube',$request_type)->where('status',0)->get();
       
        if($request->input('party')!=null && trim($request->input('party'))!='')
            $Transactions=$Transactions->where('customer_code',$request->input('party'));



          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
          $Transactions=$Transactions->where('trans_date','>=',$from_dates." 00:00:00")->where('trans_date','<=',$to_dates." 23:59:59");
        
        }
              
              $str='S.No.,Slip Date,Requisition / Slip No,Transaction Type,Party Code,Party Name,Product Ordered,Product Delivered,Qty. Order,Qty Deld.,Rate,Value';

              

              
              $CsvData=array($str);
                    
                  $i=1;  
             foreach($Transactions as $Transaction){
                   $str=$i;
                    $str.=','.date('d/m/Y',strtotime($Transaction->trans_date));
                    $str.=','.$Transaction->Request_id;

                    if($Transaction->trans_mode=='SLIPS')
                      $str.=',SLIPS';
                    else
                      $str.=',GARRUDA';
                    

                     $str.=','.$Transaction->customer_code;
                     $str.=','.$Transaction->getCustomerMaster->company_name;
                    
                    if($Transaction->trans_mode=='SLIPS'){
                       $str.=','.$Transaction->getItemName->Item_Name;
                    }else{
                      
                      if($Transaction->petrol_or_lube==1){

                        if($Transaction->getfuelRequest!=null && $Transaction->getfuelRequest->itemname!=null){
                          $str.=','.$Transaction->getfuelRequest->itemname->Item_Name;
                        }

                      }else{
                        $str.=','.$Transaction->getItemName->Item_Name;
                      }

                    }
                  
                  $str.=','.$Transaction->getItemName->Item_Name;

                  if($Transaction->trans_mode=='SLIPS'){

                      $str.=','.$Transaction->petroldiesel_qty;

                  }else{
                       if($Transaction->petrol_or_lube==1){

                         if($Transaction->getfuelRequest!=null){

                              if($Transaction->getfuelRequest->Request_Type=='Full Tank'){

                                 $str.=','.$Transaction->getfuelRequest->Request_Type;

                              }else{

                                $str.=','.$Transaction->getfuelRequest->Request_Value;

                              }
                              
                         }

                       }else{

                        $str.=','.$Transaction->getlubeRequest()->where('item_id',$Transaction->item_code)->first()->quantity;
                       
                       }
                  }
               

                 $str.=','.$Transaction->petroldiesel_qty;
                 $str.=','.bcadd(round($Transaction->item_price,2),0,2);
                 $str.=','.bcadd(round($Transaction->item_price*$Transaction->petroldiesel_qty,2),0,2);
 
                     
                  $CsvData[]=$str;
                  $i++;
              }
               
              $filename=date('Y-m-d')."-item-wise-nozzle-wise-reading-report.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
  }

}
