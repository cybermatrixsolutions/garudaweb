<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShiftSettelementModel;
use App\MastorRO;
use App\RoPersonalManagement;
use Auth;
use Gate;
use DB;
class ShiftSettelmentController extends Controller
{    

   public function index()
    {
   
    	if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
            }
            $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
          }

         // $shiftsettelemt =  DB::table('tbl_personnel_master')
         //    ->join('tbl_shift_settlement', 'tbl_personnel_master.id', '=', 'shift_settlement.trans_by')
         //    ->select('tbl_personnel_master.*', 'tbl_shift_settlement.*')
         //    ->get();
         //    dd($shiftsettelemt);
           $RoPersonalManagement = RoPersonalManagement::where('RO_Code',$rocode)->first();
           
        $ShiftSettelementModel = ShiftSettelementModel::where('trans_by',$RoPersonalManagement->id)->get();
         //$Personalname = RoPersonalManagement::where('id',$ShiftSettelementModel->trans_by)->get();
         // dd($Personalname);
       return view('backend.ShiftSettelment',compact('ShiftSettelementModel','RoPersonalManagement'));
    }
}
