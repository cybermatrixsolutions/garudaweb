<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoCustomertManagement;
use App\RoMaster;
use App\Credit_limit;
use App\IndustryDepartment;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use App\User;
use Gate;
use Auth;
use App\customerData;
use Log;
use  Session;

class CustomerProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeRo()
    {
        if(Auth::user()->user_type == 4)
          {
           $email =Auth::user()->email;
           $customer_ro = RoCustomertManagement::join('tbl_ro_master', 'tbl_customer_master.RO_code', '=', 'tbl_ro_master.RO_code')
                ->where('tbl_customer_master.Email',$email)
                ->select('tbl_customer_master.*','tbl_ro_master.pump_legal_name as pump_name')->get();
             
              return view('backend.customerChangeRo',compact('customer_ro')); 
          }

        return view('backend.dashboard'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editprofile(Request $request , $id)
    {
            $User = User::where('id',$id)->first();
           
            $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }

         $customer_mangement_data = DB::table('tbl_customer_master')->where('Email', '=', $User->email)->first();

         Log::info('CustomerProfileController@editprofile  customer_mangement_data - '.print_r($customer_mangement_data,true));
        
         $datas = RoMaster::where('is_active',1);

          if(Auth::user()->user_type!=1 && $rocode!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();

            $Industry=IndustryDepartment::where('type','Industry Vertical')->get();
            $Department=IndustryDepartment::where('type','Department')->get();
            $countries_list =DB::table('countries')->get();

         $state_list =DB::table('states')->where('country_id',$customer_mangement_data->country)->get();
         $city_list =DB::table('cities')->where('state_id',$customer_mangement_data->state)->get();
        return view('backend.customerprofile.customerprofile', compact(['customer_mangement_data', 'datas','countries_list','state_list','city_list','Industry','Department']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateprofile(Request $request, $id)
    {
           Log::info('CustomerProfileController@updateprofile - input '.print_r($request->all(),true));

            $customer_mangement = RoCustomertManagement::find($id);

            $loginUser=DB::table('users')->where('email','=',$customer_mangement->Email)->value('email');




              $loginUserUpdate=DB::table('users')->where('email','=',$loginUser)->update(['email'=>$request->input('email')]);
           // Log::info('loginUserUpdate     ==    '.print_r($loginUserUpdate,true));

            

             $oldEmail=$customer_mangement->Email;
            $customer_mangement->RO_code = $request->input('RO_code');
            //$customer_mangement->Customer_Code = 'CUS'.$result;
            $email=$customer_mangement->Email;
            $customer_mangement->Phone_Number = $request->input('Phone_Number');
            $customer_mangement->Mobile = $request->input('mobile_no');
            $customer_mangement->CustomerDeposit = $request->input('CustomerDeposit');
            $customer_mangement->Email= $request->input('email');
            $customer_mangement->Customer_Name = $request->input('coustomer_name');
            $customer_mangement->gender = $request->input('gender');
            $customer_mangement->company_name = $request->input('company_name');
            $customer_mangement->address_one = $request->input('address_one');
            $customer_mangement->vat = $request->input('vat');
            $customer_mangement->city = $request->input('city');
            $customer_mangement->state = $request->input('state');
            $customer_mangement->address_two = $request->input('address_two');
            $customer_mangement->address_three = $request->input('address_three');
            $customer_mangement->pin_no = $request->input('pin_no');
            $customer_mangement->gst_no = $request->input('gst_no');
            $customer_mangement->pan_no = $request->input('pan_no');
            $customer_mangement->industry_vertical = $request->input('industry_vertical');
            $customer_mangement->Department = $request->input('Department');
			
			if(Auth::user()->user_type != 3){
				$customer_mangement->confirm = 1;
			}
            $customer_mangement->save();


             Log::info('loginUser     ==    '.print_r($loginUser,true));
             Log::info('$request->input(email)   ==    '.print_r($request->input('email'),true));
           

            if($request->input('email')==$loginUser){

                 Log::info('data update Sucessgully not email');
                 $request->session()->flash('success','Profile  Updated  Successfully!!'); 
                    return back();
            }else{

            Log::info(' email and data update Sucessgully');
            Session::flush(); 
            return back('/');

            }
           

           
    }
    

    
}
