<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\RoOwnerManger;
use App\RoMaster;
use DB;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
class RoOwnerMangerController extends Controller
{   
    public function __contractor(){

        if (Gate::denies('custermer')) {
            
            return redirect('Dashboard');
        }

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $principle_company = DB::table('tbl_company_principal_master')->get();
      $stock_group = DB::table('tbl_stock_item_group')->get();
      $unit_measure = DB::table('tbl_unit_of_measure')->get();
      $stock_list = StockItemManagement::all();

  //return view('backend.stockItemManagement');
    return view('backend.StockItemManagement',compact(['principle_company','stock_group','unit_measure','stock_list']));
     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
             $validatedData = $request->validate([
                     
                    
                     'owner_Email' => 'required|unique:tbl_ro_owner_master',
                     // 'password' => 'required|string|min:6',
                     // 'owner_name' => 'required',
                     'owner_mobile' => 'required|regex:/[0-9]{10}/',

                ]);

             try {
                        $owner_mangement = new RoOwnerManger();
                        $owner_mangement->RO_code = $request->input('RO_code');
                        $owner_mangement->owner_name = $request->input('owner_name');
                        $owner_mangement->owner_phone = $request->input('owner_phone');
                        $owner_mangement->owner_mobile = $request->input('owner_mobile');
                        $owner_mangement->owner_Email = $request->input('owner_Email');
                       
                        $owner_mangement->save();
                        $neuser=User::where('email',$owner_mangement->owner_Email)->first();
            
                        if($neuser==null){

                            $user = new User();
                            $user->name=$owner_mangement->owner_name;
                            $user->email=$owner_mangement->owner_Email;
                            $user->username=$owner_mangement->owner_Email;
                            $user->user_type=3;
                            $user->password=bcrypt($request->input('password'));
                            $user->save();
                            //dd('hellodfsdfsd');
                        }
                        $request->session()->flash('success','Added Successfully!!');

                        $owner_list = RoOwnerManger::All();
                        $datas= RoMaster::All();
             } catch(\Illuminate\Database\QueryException $e){
                            
                            $request->session()->flash('success','Something wrong!!');  
                        }
                   
                    
            return redirect('owner');
            
       
    }

    public function ownerDeactive(Request $request,$id)
    {
        DB::table('tbl_ro_owner_master')
            ->where('id', $id)
            ->update(['is_active' => 0]);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('owner');
    }
     public function ownerActive(Request $request,$id)
    {
        DB::table('tbl_ro_owner_master')
            ->where('id', $id)
            ->update(['is_active' => 1]);
             $request->session()->flash('success','Activated Successfully !!');
                return redirect('owner');
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $owner_mangement_data = DB::table('tbl_ro_owner_master')->where('id', '=', $id)->first();
        $datas = RoMaster::All();

        return view('backend.owner_edit_page', compact(['owner_mangement_data', 'datas']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $validatedData = $request->validate([
         
         'owner_name' => 'required',
         'owner_mobile' => 'required|regex:/[0-9]{10}/',
        
    ]);
        try {
            $owner_mangement = RoOwnerManger::find($id);
           $owner_mangement->RO_code = $request->input('RO_code');
            $owner_mangement->owner_name = $request->input('owner_name');
            $owner_mangement->owner_phone = $request->input('owner_phone');
            $owner_mangement->owner_mobile = $request->input('owner_mobile');
            // $owner_mangement->owner_IMEI = $request->input('owner_IMEI');
            
           

        $owner_mangement->save();

        $owner_list = RoOwnerManger::All();
        $datas= RoMaster::All();
        $request->session()->flash('success','Data Updated Successfully!!'); 
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');  
            }
        return redirect('owner');
    }
//
//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param  int  $id
//     * @return \Illuminate\Http\Response
//     */
    public function destroy(Request $request,$id)
    {
       try {
         $owner_mangement = RoOwnerManger::find($id);

        $owner_mangement->delete();
            $request->session()->flash('success','Data has been deleted Successfully!!');
       } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }

        return back();
    }
}
