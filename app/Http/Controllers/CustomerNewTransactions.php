<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoPieceListManagement;
use App\RoPersonalManagement;
use App\TableStockItemMaster;
use App\RoMaster;
use App\RoDetails;
use App\RoLOBItemSelectionModel;
use App\RoLobSelectionModel;
use App\StockItemManagement;
use App\LOBMasterModel;
use App\AddPetrolDieselModel;
use App\TableItemPriceListModel;
use App\TransactionModel;
use App\TransactionInvoiceModel;
use App\invoiceCustomer;
use App\CustomerLubeRequest;
use App\RoCustomertManagement;
use App\Pitem_tax;
use App\VechilManage;
use App\TransTaxModel;
use App\tax_master;
use App\Shift;
use Auth;
use App\OtpModel;
use Gate;
use App\Credit_limit;
use DB;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\User;

class CustomerNewTransactions extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
        
        $datas= RoMaster::where('is_active','1');

        if(Auth::user()->user_type!=1 && $rocode!='')
            $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

        $datas=$datas->get();
        $customer_code=$request->input('customer_code');  
        $Vehicle_Reg_No=$request->input('Vehicle_Reg_No');  
         
         $CustomerLubeRequest = CustomerLubeRequest::where('Vehicle_Reg_No',$Vehicle_Reg_No)->where('Execution_date',null)->get();
        
          
          $AddPetrolDieselModel = AddPetrolDieselModel::where('Vehicle_Reg_No',$Vehicle_Reg_No)->where('Execution_date',null)->get();
          
          $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
          $getpric=  $getpric->pluck('item_id')->toArray();
          $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->whereIn('tbl_price_list_master.id',$getpric)->select('tbl_price_list_master.*');

        if(Auth::user()->user_type!=1 && $rocode!='')
                $fuel_type=$fuel_type->where('tbl_price_list_master.RO_Code',$rocode);

              $fuel_type=$fuel_type->get();
        
              return view('backend.customernewtrans.customer_newtrans',compact(['datas','fuel_type','CustomerLubeRequest','AddPetrolDieselModel','customer_code','Vehicle_Reg_No']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storedata(Request $request)
    {     
          try{
          if($request->input('customercode')==null){
              return redirect('customernewtransactions');
          }

          $customercode=$request->input('customercode'); 
          $petrolDiesel_Type=$request->input('petrolDiesel_Type');
          $Vehicleno=$request->input('Vehicleno');  
          $RoCustomertManagement =RoCustomertManagement::where('Customer_Code',$customercode)->first();
          $Vehiclenod =VechilManage::where('Registration_Number',$Vehicleno)->first();
          $Request_Type=$request->input('Request_Type');  
          $Request_Value=$request->input('Request_Value'); 
          $request_id=$request->input('request_id'); 
          $current_driver_mobile=$request->input('current_driver_mobile');

         if(Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
         }
         $rocode='';
         $id=0;

         if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
          }elseif(Auth::user()->getPersonel!=null) {
             $id=Auth::user()->getPersonel->id;
             $rocode=Auth::user()->getPersonel->RO_Code;
          }
         
          if( $id!=0)
             $allshifts=Shift::where('is_active',1)->where('shift_manager',$id)->get();
          else
            $allshifts=Shift::where('is_active',1)->get();

          $person=[];
          $Padestal=[];

          if($allshifts!=null){
              foreach ($allshifts as $allshift) {
                   if($allshift!=null && $allshift->getPersonnel!=null)
                  $person=array_merge($person,$allshift->getPersonnel->pluck('id')->toArray());  
              } 
          }
         
          

          $pedestals=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->whereIn('id',$person)->get();

          $datas= RoMaster::where('is_active','1');

          if(Auth::user()->user_type!=1 && $rocode!='')
              $datas=$datas->where('tbl_ro_master.RO_code',$rocode);
           
           $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
           $getpric=  $getpric->pluck('item_id')->toArray();

           $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->whereIn('tbl_price_list_master.id',$getpric)->where('tbl_price_list_master.Sub_Stock_Group',$Vehiclenod->getitem->Sub_Stock_Group)->select('tbl_price_list_master.*');
          if(Auth::user()->user_type!=1 && $rocode!='')
                $fuel_type=$fuel_type->where('tbl_price_list_master.RO_Code',$rocode);
             
              $fuel_type=$fuel_type->get();
      

          return view('backend.customernewtrans.customertransactionprocess',compact(['Request_Value','current_driver_mobile','Request_Type','fuel_type','Vehicleno','RoCustomertManagement','customercode','request_id','pedestals','Vehiclenod','petrolDiesel_Type']));
          } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');
                return back();
                
          } 

      }

      public function storetransactions(Request $request)
    {

        $fuel_type_id=$request->input('fuel_type');
       // dd($fuel_type_id);

         $Customer= DB::table('tbl_price_list_master')->where('id',$fuel_type_id)->first();
       
         $measureunit = DB::table('tbl_unit_of_measure')->where('id',$Customer->Unit_of_Measure)->first();
         $as=$measureunit->No_of_Decimals;
          // dd($measureunit->No_of_Decimals);
           $petroldiesel_qty = $request->input('petroldiesel_qty');
            
           $asbd=explode('.',$petroldiesel_qty);
           
           if(isset($asbd[1])){
              if($as<strlen($asbd[1])){
                 $request->session()->flash('success','Decimal Point Is Greater then your Dispense Value !!');
                return back();
              }
           }

        
            
       try{

       $otp=$request->input('otp'); 
       $mobile = $request->input('mobile');
       $request_id = $request->input('request_id');
       $OtpModel=DB::table('tbl_trans_otp')->where('OTP','=',$otp)->where('mobile',$mobile)->first();
       if($OtpModel==null || $otp!=$OtpModel->OTP){
         $request->session()->flash('success','Invailid OTP!!');
           return redirect('customernewtransactions');  
       }

      
       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
       $ccc = $request->input('customer_code');
       //$fuel_type_id=$request->input('fuel_type');
      
          $Credit_limit=Credit_limit::where('Customer_Code',$ccc)->first(); 
          $transactionModels = TransactionModel::where('RO_code',$rocode)->where('customer_code',$ccc)->where('status',0)->get();
          $uselimitvalue=0;
          foreach ($transactionModels as $transactionModel) {
           
           if($transactionModel->getItemName!=null && $transactionModel->getItemName->price!=null)
            $uselimitvalue=($transactionModel->petroldiesel_qty*$transactionModel->getItemName->price->price)+$uselimitvalue;
          }

        if($uselimitvalue>=$Credit_limit->Limit_Value){
           $request->session()->flash('success','Out of Customer Credit limit ');
           return redirect('customernewtransactions');  
        }
        
      // $petroldiesel_qty = $request->input('petroldiesel_qty');
       $shift_id =0;
        $RoPersonalManagement = RoPersonalManagement::find($request->input('salesman')); 
       if( $RoPersonalManagement!=null){
         $shifts = $RoPersonalManagement->getShift->where('is_active',1)->first();
          $shift_id=$shifts->id;
       }
        if (date('m') <= 3) {
          $year = date('y')-1;
        } else {
              $year = date('y');
        }

          $j=0;
          $Delivery_Slip_Prefix='dev';
          $Delivery_serial=0;
          $nom=2;
          $Delivery_serial=0;
          $Delivery_invoice='';
        if (Auth::user()->getRocode!=null) {

             $rocode=Auth::user()->getRocode->RO_code;
             $roid=Auth::user()->getRocode->id;
            
            if(Auth::user()->getRocode->getdetails!=null){
                 $Delivery_Slip_Prefix=Auth::user()->getRocode->getdetails->Delivery_Slip_Prefix;
                 $Delivery_serial=Auth::user()->getRocode->getdetails->Delivery_serial;
                 $nom=Auth::user()->getRocode->getdetails->D_serial_no_lenght;
            }

             $Delivery_serial= $Delivery_serial+1;
             $Delivery_invoice= $Delivery_serial;

         }
       
         $l=mb_strlen($Delivery_serial);
         $nom=$nom-$l;
         for ($i=1; $i <=$nom ; $i++) { 
              $Delivery_invoice='0'.$Delivery_invoice;
         }

        $Delivery_invoice=$Delivery_Slip_Prefix.'/'.$Delivery_invoice.'/'.$year;
        RoDetails::where('ro_id',$roid)->update(['Delivery_serial' => $Delivery_serial]);

        $fuel_peicesss = TableItemPriceListModel::where('item_id',$fuel_type_id)->where('is_active',1)->first();
        $RoPieceListManagement=RoPieceListManagement::where('id',$fuel_type_id)->first();
        
        $fuel_peice=$fuel_peicesss->price;
        if($request->input('petroldiesel_type') == 'Rs')
        {

            $petroldiesel_qty = $petroldiesel_qty / $fuel_peice;      
        }

        $length = 5;
        $randomBytes = openssl_random_pseudo_bytes($length);
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $result = '';
        for ($i = 0; $i < $length; $i++)
           $result .= $characters[ord($randomBytes[$i]) % $charactersLength];

        $length = 6;
        $randomBytes = openssl_random_pseudo_bytes($length);
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $trans_id_no = '';
        for ($i = 0; $i < $length; $i++)
           $trans_id_no .= $characters[ord($randomBytes[$i]) % $charactersLength];
        $trans_id="GARTRANS".$trans_id_no;
        

        $shiftCal=Shift::find($shift_id);
        
        $customer_requests=DB::table('tbl_customer_request_petroldiesel')->where('request_id',$request_id)->first();
      
        $transaction = new TransactionModel();
        $transaction->RO_code = $rocode;
        $transaction->item_price = $fuel_peice;
        $transaction->customer_code = $ccc;
        $transaction->petroldiesel_type = $request->input('petroldiesel_type');
        $transaction->petrol_or_lube = 1;
        $transaction->petroldiesel_qty = $petroldiesel_qty;            
        $transaction->Vehicle_Reg_No = $request->input('Vehicle_Reg_No');
        $transaction->item_code = $fuel_type_id;
        $transaction->Trans_By =$request->input('salesman');
        $transaction->shift_id=$shift_id;

        if($customer_requests!=null)
           $transaction->request_date=date('Y-m-d',strtotime($customer_requests->request_date));


        if($shiftCal!=null)
         $transaction->trans_date = $shiftCal->created_at;


        $transaction->shift_id=$shift_id;
        $transaction->Trans_Mobile = $mobile;
        $transaction->cust_name = 'credit';
        $transaction->Request_id = $request_id;
        $transaction->trans_id = $trans_id;
        $transaction->invoice_number = $Delivery_invoice;
        $transaction->save();
      
        if($RoPieceListManagement->gettex!=null)
       foreach ($RoPieceListManagement->gettex as $geds) {
         $TransTaxModel = new TransTaxModel();
         $TransTaxModel->Tax_Code=$geds->Tax_Code;
         $TransTaxModel->Tax_Type=$geds->Tax_Type;
         $TransTaxModel->Tax_percentage=$geds->Tax_percentage;
         $TransTaxModel->Description=$geds->Description;
         $TransTaxModel->GST_Type=$geds->GST_Type;
         $TransTaxModel->tax_name=$geds->tax_name;
         $TransTaxModel->row_columns=$geds->row_columns;
         $TransTaxModel->position=$geds->position;
         $TransTaxModel->strate_type=$geds->strate_type;
         $TransTaxModel->transaction_id=$transaction->id;
         $TransTaxModel->is_active=1;
         $TransTaxModel->save();
        }
        $length = 6;
        $randomBytes = openssl_random_pseudo_bytes($length);
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $trans_invoice = '';
        for ($i = 0; $i < $length; $i++)
           $trans_invoice .= $characters[ord($randomBytes[$i]) % $charactersLength];

          $invoice_number = "GARINV".$trans_invoice;
          
          if($request->input('petroldiesel_type') == 'Ltr'  || $request->input('petroldiesel_type') == 'Full Tank')
          {
             $trans_amount = $fuel_peice *   $request->input('petroldiesel_qty');
          }
          else
          {
            $trans_amount =  $request->input('petroldiesel_qty');
          }

     /*  $exutu = date("Y-m-d H:i:s");
       $date1 = str_replace('/', '-', $exutu);
       $exutuce = date('Y-m-d H:i:s', strtotime($date1));*/

       AddPetrolDieselModel::where('request_id',$request_id)->update(['Execution_date'=>date("Y-m-d H:i:s")]);
       

       $request->session()->flash('success','Transactions Successfully!!');
       return redirect('customernewtransactions');  
       } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');
                return back();
                
      } 

         
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function show(Request  $request)
    {
          $Vehicle_Reg_No=$request->input('Vehicle_Reg_No'); 
          $CustomerLubeRequest = CustomerLubeRequest::where('Vehicle_Reg_No',$Vehicle_Reg_No)->get();
          $AddPetrolDieselModel = AddPetrolDieselModel::where('Vehicle_Reg_No',$Vehicle_Reg_No)->get();
       

           


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function storeothertrans(Request $request)

    {

      try{


      if($request->input('customer_code')==null || $request->input('request_id')== null || count($request->input('item_code'))<=0){
         $request->session()->flash('success','Please Select Item!!');
           return redirect('customernewtransactions'); 
         }
              $item_code=$request->input('item_code');
            
          
              $rod= DB::table('tbl_price_list_master')->whereIn('id',$item_code)->where('is_active',1)->get();
             
             $itenmName =$rod->pluck('Item_Name','id')->toArray();

             //dd($itenmName);
              $Vehicle_Reg_No = $request->input('Vehicle_Reg_No');
              $customer_code = $request->input('customer_code');
              $request_id = $request->input('request_id');
              $driver_reuqest_mobile=$request->input('driver_reuqest_mobile');

              $petroldiesel_qty=[];
             
              foreach ($itenmName as $key=> $itenmN) {
               $petroldiesel_qty['name_'.$key]=$request->input('name_'.$key);

              }
                
             $rocode='';
             $id=0;
             if (Auth::user()->getRocode!=null) {
                $rocode=Auth::user()->getRocode->RO_code;
             }
             elseif(Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $rocode=Auth::user()->getPersonel->RO_Code;

        }

           if( $id!=0)
             $allshifts=Shift::where('is_active',1)->where('shift_manager',$id)->get();
            else
              $allshifts=Shift::where('is_active',1)->get();
               $person=[];
              $Padestal=[];
              if($allshifts!=null){
                    foreach ($allshifts as $allshift) {
                         if($allshift!=null && $allshift->getPersonnel!=null)
                        $person=array_merge($person,$allshift->getPersonnel->pluck('id')->toArray());  
                    } 
                }
               $pedestals=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->whereIn('id',$person)->get();
             
               $RoCustomertManagement =RoCustomertManagement::where('Customer_Code',$customer_code)->first();
              // dd($RoCustomertManagement);

               $item_id = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_price_list_master.RO_Code',$rocode)->where('tbl_stock_item_group.Group_Name','!=', 'FUEL')->select('tbl_price_list_master.*')->get();

             return view('backend.customernewtrans.othercustomerrequest',compact('item_code','driver_reuqest_mobile','Vehicle_Reg_No','RoCustomertManagement','item_id','customer_code','itenmName','request_id','pedestals','petroldiesel_qty'));

            
            
            
             $fuel_price = TableItemPriceListModel::whereIn('item_id',$item_code)->where('is_active',1)->get();
             
             $fuel_price =$fuel_price->pluck('price','item_id')->toArray();
          
          } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');

                return back();
                
          } 


    }
    
    public function createtransaction(Request $request)
    {
           $item_code=$request->input('petroldiesel_qty');
        
      try{
        $rocode='';
         if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
         }
       $otp=$request->input('otp'); 
      
      $mobile = $request->input('mobile');
      $request_id = $request->input('request_id');
      //dd($mobile);
      //$OtpModel= OtpModel::where('OTP',$otp)->get();
       $OtpModel=DB::table('tbl_trans_otp')->where('OTP','=',$otp)->where('mobile',$mobile)->where('request_id',$request_id)->first();
      //dd($OtpModel->OTP);
       if($OtpModel==null || $otp!=$OtpModel->OTP){

            $request->session()->flash('success','Invailid OTP!!');
         
           return redirect('customernewtransactions');  
       }
          $item_code=$request->input('item_code');
         
          $customer_code = $request->input('customer_code');

          $Credit_limit=Credit_limit::where('Customer_Code',$customer_code)->first(); 
          $transactionModels = TransactionModel::where('RO_code',$rocode)->where('customer_code',$customer_code)->where('status',0)->get();
          $uselimitvalue=0;
          foreach ($transactionModels as $transactionModel) {
           
           if($transactionModel->getItemName!=null && $transactionModel->getItemName->price!=null)
            $uselimitvalue=($transactionModel->petroldiesel_qty*$transactionModel->getItemName->price->price)+$uselimitvalue;
          }

        if($uselimitvalue>=$Credit_limit->Limit_Value){
           $request->session()->flash('success','Out of Customer Credit limit ');
           return redirect('customernewtransactions');  
        }

        $shift_id =0;
        $RoPersonalManagement = RoPersonalManagement::find($request->input('salesman')); 
       
       if( $RoPersonalManagement!=null){
         $shifts = $RoPersonalManagement->getShift->where('is_active',1)->first();
          $shift_id=$shifts->id;
       }
            
            if (date('m') <= 3) {
                $year = date('y')-1;
              } else {
                    $year = date('y');
              }

          $j=0;
          $Delivery_Slip_Prefix='dev';
          $Delivery_serial=0;
          $nom=2;
          $Delivery_serial=0;
          $Delivery_invoice='';
        if (Auth::user()->getRocode!=null) {

             $rocode=Auth::user()->getRocode->RO_code;
             $roid=Auth::user()->getRocode->id;
            
            if(Auth::user()->getRocode->getdetails!=null){
                 $Delivery_Slip_Prefix=Auth::user()->getRocode->getdetails->Delivery_Slip_Prefix;
                 $Delivery_serial=Auth::user()->getRocode->getdetails->Delivery_serial;
                 $nom=Auth::user()->getRocode->getdetails->D_serial_no_lenght;
            }

             $Delivery_serial= $Delivery_serial+1;
             $Delivery_invoice= $Delivery_serial;

         }
       
         $l=mb_strlen($Delivery_serial);
         $nom=$nom-$l;
         for ($i=1; $i <=$nom ; $i++) { 
              $Delivery_invoice='0'.$Delivery_invoice;
         }

          $Delivery_invoice=$Delivery_Slip_Prefix.'/'.$Delivery_invoice.'/'.$year;
          RoDetails::where('ro_id',$roid)->update(['Delivery_serial' => $Delivery_serial]); 
          $fuel_price = TableItemPriceListModel::whereIn('item_id',$item_code)->where('is_active',1)->get();
          $fuel_price =$fuel_price->pluck('price','item_id')->toArray();
          $RoPieceListManagement=RoPieceListManagement::where('id',$item_code)->first();
          
          $length = 5;
          $randomBytes = openssl_random_pseudo_bytes($length);
          $characters = '0123456789';
          $charactersLength = strlen($characters);
          $result = '';
          for ($i = 0; $i < $length; $i++)
             $result .= $characters[ord($randomBytes[$i]) % $charactersLength];
              $length = 6;
             $randomBytes = openssl_random_pseudo_bytes($length);
          $characters = '0123456789';
          $charactersLength = strlen($characters);
          $trans_id_no = '';
        for ($i = 0; $i < $length; $i++)
           $trans_id_no .= $characters[ord($randomBytes[$i]) % $charactersLength];
           $trans_id="GARTRANS".$trans_id_no;

       

        $shiftCal=Shift::find($shift_id);
        
        $customer_requests=DB::table('tbl_customer_request_lube')->where('request_id',$request_id)->first();
        
       

        foreach($fuel_price as $key=>$fuel_prices)
        {
          $transaction = new TransactionModel();
          $transaction->RO_code = $rocode;
          $transaction->item_price = $fuel_prices;
          $transaction->customer_code = $request->input('customer_code');
          $transaction->petroldiesel_type = 0;
          $transaction->petrol_or_lube = 2;
          $transaction->petroldiesel_qty =$request->input('name_'.$key);           
          $transaction->Vehicle_Reg_No = $request->input('Vehicle_Reg_No');
         // $transaction->slip_detail = $request->input('slip_detail');
          $transaction->item_code = $key;
          $transaction->Trans_By =$request->input('salesman');
          $transaction->shift_id=$shift_id;

           if($customer_requests!=null)
           $transaction->request_date=date('Y-m-d',strtotime($customer_requests->request_date));

          if($shiftCal!=null)
           $transaction->trans_date = $shiftCal->created_at;

          $transaction->Trans_Mobile =$mobile;
          $transaction->cust_name = 'credit';
         
          $transaction->Request_id =$request_id;
          $transaction->trans_id = $trans_id;
          $transaction->invoice_number = $Delivery_invoice;
          $transaction->save();
          
          if($RoPieceListManagement->gettex!=null)
            foreach ($RoPieceListManagement->gettex as $geds) {
               $TransTaxModel = new TransTaxModel();
               $TransTaxModel->Tax_Code=$geds->Tax_Code;
               $TransTaxModel->Tax_Type=$geds->Tax_Type;
               $TransTaxModel->Tax_percentage=$geds->Tax_percentage;
               $TransTaxModel->Description=$geds->Description;
               $TransTaxModel->GST_Type=$geds->GST_Type;
               $TransTaxModel->tax_name=$geds->tax_name;
               $TransTaxModel->row_columns=$geds->row_columns;
               $TransTaxModel->position=$geds->position;
               $TransTaxModel->strate_type=$geds->strate_type;
               $TransTaxModel->transaction_id=$transaction->id;
               $TransTaxModel->is_active=1;
               $TransTaxModel->save();
            }

        }

        /*$price =0;
       
       foreach($fuel_price as $key=>$fuel_prices)
       {
        $price += $fuel_prices;
       }
*/
        

       /* $length = 6;
         $randomBytes = openssl_random_pseudo_bytes($length);
      $characters = '0123456789';
      $charactersLength = strlen($characters);
      $trans_invoice = '';
      for ($i = 0; $i < $length; $i++)
         $trans_invoice .= $characters[ord($randomBytes[$i]) % $charactersLength];
*/
        //$invoice_number = "GARINV".$trans_invoice;

      
      /* $invoice_data = new TransactionInvoiceModel();
       $invoice_data->trans_id=$trans_id;
       $invoice_data->RO_code=$rocode;
       $invoice_data->customer_code=$request->input('customer_code');
       $invoice_data->trans_amount=$price;
       $invoice_data->invoice_number=$invoice_number;
       $invoice_data->Trans_By=$request->input('salesman');
       $invoice_data->shift_id=$shift_id;
       $invoice_data->cust_type='credit';
       $invoice_data->petrol_or_lube=2;
       $invoice_data->tax_amount=0;
       $invoice_data->save();*/
       /*$exutu = date("Y-m-d H:i:s");
       $date1 = str_replace('/', '-', $exutu);
       $exutuce = date('Y-m-d H:i:s', strtotime($date1));*/
      
       $CustomerLube=CustomerLubeRequest::where('request_id',$request_id)->update(['Execution_date' => date("Y-m-d H:i:s")]);
       
        $request->session()->flash('success','Transactions Successfully!!');
              
              return redirect('customernewtransactions');  

    } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');

                return back();
                
           } 
}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContributionModel $contributionModel)
    {
        //
    }

    public function getmobile(Request $request){
       $otppin = rand(1000, 9999);
      $request_id = $request->input('request_id');
      $OtpModel=OtpModel::where('request_id',$request_id)->first();

      if($OtpModel==null)
       $OtpModel = new OtpModel();

      $mobile=$request->input('mobile'); 
     
      $otp=$otppin; 
      $OtpModel->mobile=$mobile;
      $OtpModel->request_id=$request_id;
      $OtpModel->OTP=$otp; 
      $OtpModel->save();
     
}
public function sendotherotp(Request $request){
     
      
    
      $otppin = rand(1000, 9999);
     $request_id = $request->input('request_id');
     $OtpModel=OtpModel::where('request_id',$request_id)->first();

   if($OtpModel==null)
      $OtpModel = new OtpModel();

      $mobile=$request->input('mobile'); 
      $otp=$otppin; 
      $OtpModel->mobile=$mobile;
      $OtpModel->request_id=$request_id;
      $OtpModel->OTP=$otp; 

      $OtpModel->save();
     
}
public function verifity(Request $request){
    

      $otpsubmit=$request->input('otpsubmit'); 
      $OtpModel = OtpModel::where('OTP',$otpsubmit)->get();
     
}
}
