<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\RoPieceListManagement;
use App\TableStockItemMaster;
use App\RoMaster;
use App\RoLOBItemSelectionModel;
use App\RoLobSelectionModel;
use App\StockItemManagement;

use DB;
use Illuminate\Support\Facades\Redirect;

use App\Http\Controllers\Controller;
use Gate;
use Auth;

class RoLobItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
         $rocode='';
         $ro_code = $request->input('ro_code');
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
          
         if($ro_code == null)
         {
             $pump_legal_name_list= RoMaster::where('is_active','1');
             if(Auth::user()->user_type!=1 && $rocode!='')
             $pump_legal_name_list=$pump_legal_name_list->where('RO_code',$rocode);
             $get_lob= RoLobSelectionModel::where('RO_code',$rocode)->get();
            $pump_legal_name_list=$pump_legal_name_list->get();
             
            $lob_item_list= RoLOBItemSelectionModel::where('is_active','1')->get();
            return view('backend.lob_item_page',compact('pump_legal_name_list','lob_item_list','get_lob'));
         }
         else
         {

         $pump_legal_name_list= RoMaster::where('is_active','1');

          if(Auth::user()->user_type!=1 && $rocode!='')
            $pump_legal_name_list=$pump_legal_name_list->where('RO_code',$rocode);

            $pump_legal_name_list=$pump_legal_name_list->get();

            $lob_item_list= RoLOBItemSelectionModel::where('is_active','1')->get();
            $get_lob= RoLobSelectionModel::where('RO_code',$ro_code)->get();
            $request->session()->flash('success','Data Add Successfully!!');
         
        //return back()->with('ro_code',$ro_code);
        return view('backend.lob_item_page',compact('pump_legal_name_list','lob_item_list','get_lob','ro_code'));

         }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($ro_code)
    {

       
       $pump_legal_name_list= RoMaster::where('is_active','1')->get();
        $lob_item_list= RoLOBItemSelectionModel::where('is_active','1')->get();
        $get_lob= RoLobSelectionModel::where('RO_code',$ro_code)->get();

     return view('backend.lob_item_page',compact('pump_legal_name_list','lob_item_list','get_lob','ro_code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        try{
                $ro_code='';
                $roType='';

                if (Auth::user()->getRocode!=null) {
                  $ro_code=Auth::user()->getRocode->RO_code;
                  $company_code=Auth::user()->getRocode->company_code;
                  $roType=Auth::user()->getRocode->Ro_type;
                }

                $lob_name= $request->input('lob_name');
               
               // $ro_code = $request->input('RO_code');
        
                RoLobSelectionModel::where('RO_code',$ro_code)->delete();
               
                /**
                *  when ro type petrol pump
                */

                /*if($roType==1){

                    $lobs=['FUELS','LUBES'];

                    foreach ($lobs as $lob) {
                        $lob_data = new RoLobSelectionModel();
                        $lob_data->RO_code=$ro_code;  
                        $lob_data->lob_name=$lob;
                        $lob_data->save();
                    }

                    $price_list=  StockItemManagement::whereIn('LOB',$lobs)->get();
                    
                    $this->addItemByLob($price_list, $ro_code);
                }*/
                
                /**
                * GEN LOB
                */
                
                if($lob_name!=null){

                    foreach ($lob_name as $lob_nam) {
                        $lob_data = new RoLobSelectionModel();
                        $lob_data->RO_code=$ro_code;  
                        $lob_data->lob_name=$lob_nam;
                        $lob_data->save();
                    }
                
              
                $price_list_data=  StockItemManagement::whereIn('LOB',$lob_name)->get();
                $this->addItemByLob($price_list_data, $ro_code,$company_code);
               }
                $request->session()->flash('success','Data Updated Successfully!!'); 

        }catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');   
        }

        return back();

    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $price_list = DB::table('tbl_price_list_master')->where('id', '=', $id)->first();
         $stock_group = DB::table('tbl_stock_item_group')->where('is_active', '>=',1)->get();
         $datas= RoMaster::where('is_active','1')->get();
          $lob = DB::table('tbl_lob_master')->get();
         $unit_measure = DB::table('tbl_unit_of_measure')->where('is_active', '>=',1)->get();
         $item_master_list= TableStockItemMaster::All();
         $taxmaster = DB::table('tbl_tax_master')->get();
        return view('backend.priceEdit', compact('price_list', 'datas','item_master_list','stock_group','unit_measure','taxmaster','lob'));
    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $price_list = DB::table('tbl_price_list_master')->where('id', '=', $id)->first();
          $stock_group = DB::table('tbl_stock_item_group')->where('is_active', '>=',1)->get();
         $datas= RoMaster::where('is_active','1')->get();
         $unit_measure = DB::table('tbl_unit_of_measure')->where('is_active', '>=',1)->get();
         $item_master_list= TableStockItemMaster::All();
        return view('backend.price_view', compact(['price_list', 'datas','item_master_list','stock_group','unit_measure']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        
        $validatedData = $request->validate([
         
        
         'price_ro_code' => 'required',
         'price_item_list' => 'required',
         
         'volume_liter' => 'required',
         'item_name' => 'required',
    ]);


        try {
           
            $personal_mangement = RoPieceListManagement::find($id);
            $personal_mangement->RO_Code = $request->input('price_ro_code');
            $personal_mangement->Item_id = $request->input('price_item_list');
            // $personal_mangement->Price = $request->input('price');
            
            $personal_mangement->Volume_ltr = $request->input('volume_liter');            
            $personal_mangement->Item_Name = $request->input('item_name');
            $date =$request->input('Active_From_Date');
            $Active_From_Date = date("Y-m-d", strtotime($date));
            
            $personal_mangement->Stock_Group = $request->input('Stock_Group');
            $personal_mangement->Unit_of_Measure = $request->input('Unit_of_Measure');
            $personal_mangement->Active_From_Date= $Active_From_Date;
            $personal_mangement->Tail_Unit = $request->input('Tail_Unit');
            $personal_mangement->Conversion = $request->input('Conversion');
            $personal_mangement->Alternate_Unit = $request->input('Alternate_Unit');
            $personal_mangement->LOB = $request->input('LOB');
            $personal_mangement->brand = $request->input('brand'); 
            $personal_mangement->tax = $request->input('tax');
            $personal_mangement->hsncode = $request->input('hsncode');    
            $personal_mangement->save();


            $request->session()->flash('success','Data Updated Successfully!!'); 
       } catch(\Illuminate\Database\QueryException $e){
                
                 $request->session()->flash('success','Something wrong!!');
                
            }
       
       
            return redirect('price_list');
    }
    function active(Request $request,$id, $actt)
    {
        
     $ac = RoPieceListManagement::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $price_list = RoPieceListManagement::find($id);

        $price_list->delete();
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function roLobUpdate()
    {
        $rocode='';
        if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
        }

        $RoLobSelectionModel= RoLobSelectionModel::where('RO_code',$rocode)->pluck('lob_name')->toArray();

        $lob_item_list = RoLOBItemSelectionModel::where('is_active','1')->whereNotIn('lob_name',$RoLobSelectionModel)->get();
       

     return view('backend.lob.update',compact('rocode','lob_item_list','RoLobSelectionModel'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function roLobUpdateSave(Request $request)
    {
         $rocode='';
        $company_code='GEN';
        $roType='';
        if (Auth::user()->getRocode!=null) {
           $rocode=Auth::user()->getRocode->RO_code;
           $company_code=Auth::user()->getRocode->company_code;
           $roType=Auth::user()->getRocode->Ro_type;
        }
        
        try{
            $ro_code='';
            $roType='';
            $lob_name= $request->input('lob_name');
           
           if($lob_name!=null){

                foreach ($lob_name as $lob_nam) {
                    $lob_data = new RoLobSelectionModel();
                    $lob_data->RO_code=$rocode;  
                    $lob_data->lob_name=$lob_nam;
                    $lob_data->save();
                }
              
                $price_list_data=  StockItemManagement::whereIn('LOB',$lob_name)->get();
                $this->addItemByLob($price_list_data, $rocode,$company_code);

              $request->session()->flash('success','Data Updated Successfully!!');
           }else{
            $request->session()->flash('success','Please Select LOB !!'); 
           }

            

        }catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');   
        }

        return back();
    }

     public function addItemByLob($price_list_data, $ro_code,$company_code){

        foreach ($price_list_data as $value) {
             if($value->Company_Code=='GEN' || $value->Company_Code==$company_code){

                $RoPiecess=RoPieceListManagement::where('RO_Code',$ro_code)->where('Item_Code',$value->Stock_Item_Code)->first();
                
                if($RoPiecess==null){
                    $personal_mangement = new RoPieceListManagement();
                    $personal_mangement->RO_Code = $ro_code;
                    $personal_mangement->Item_Code = $value->Stock_Item_Code;
                    $personal_mangement->Volume_ltr = 1;            
                    $personal_mangement->Item_Name = $value->Stock_Item_Name;
                    $personal_mangement->Stock_Group = $value->Stock_Group;
                    $personal_mangement->Sub_Stock_Group = $value->Sub_Stock_Group;
                    $personal_mangement->Unit_of_Measure = $value->Unit_of_Measure;
                    $personal_mangement->Active_From_Date= $value->Active_From_Date;
                    $personal_mangement->Tail_Unit = $value->Tail_Unit;
                    $personal_mangement->Conversion = $value->Conversion;
                    $personal_mangement->Alternate_Unit = $value->Alternate_Unit;
                    $personal_mangement->LOB = $value->LOB;
                    $personal_mangement->brand = $value->brand;
                    $personal_mangement->tax = $value->tax;
                    $personal_mangement->hsncode = $value->hsncode;
                    $personal_mangement->gst_vat = $value->gst_vat;
                    $personal_mangement->save();
                    
                } 
             }
        }

    }
}
