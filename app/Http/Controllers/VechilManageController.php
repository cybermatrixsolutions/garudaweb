<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VechilManage;
use App\QR_Code;
use App\VehicleModel;
use App\VehicleMakeModel;
use App\IndustryDepartment;
use App\MastorRO;
use DB;
use Gate;
use Auth;
use Log;


class VechilManageController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
       Log::info('vehiclemanagement@index');
       
        $data = VechilManage::join('tbl_price_list_master', 'tbl_customer_vehicle_master.Fuel_Type', '=', 'tbl_price_list_master.id')
            ->join('tbl_ro_master', 'tbl_customer_vehicle_master.Ro_code', '=', 'tbl_ro_master.RO_code')
            ->join('tbl_customer_master', 'tbl_customer_vehicle_master.Customer_code', '=', 'tbl_customer_master.Customer_Code')
            ->select('tbl_customer_vehicle_master.*','tbl_price_list_master.Item_Name as type',
                'tbl_ro_master.pump_legal_name as pump_name','tbl_customer_master.company_name as company_name')->orderBy('id','desc');
         

          if(Auth::user()->user_type!=1 && $rocode!='')
                $data=$data->where('tbl_customer_vehicle_master.Ro_code',$rocode);

              $data=$data->get();
           
            
        $data1 = $MastorRO=MastorRO::where('is_active','1');

         if(Auth::user()->user_type!=1 && $rocode!='')
            $data1=$data1->where('tbl_ro_master.RO_code',$rocode);

            $data1=$data1->get();
            $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
            $getpric=  $getpric->pluck('item_id')->toArray();

        $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->whereIn('tbl_price_list_master.id',$getpric)->select('tbl_price_list_master.*');

        if(Auth::user()->user_type!=1 && $rocode!='')
                $fuel_type=$fuel_type->where('tbl_price_list_master.RO_Code',$rocode);

              $fuel_type=$fuel_type->get();

              //dd($fuel_type);
        // $getdesil = DB::table('tbl_ro_petroldiesel')->get();
        $vehicleModelList = VehicleModel::All();
        $vehicleMakeList = VehicleMakeModel::orderBy('name', 'asc')->get();
        $IndustryDepartment = IndustryDepartment::where('type','Insurance Company')->get();
        return view('backend.vehiclemanagement',compact('data','data1','fuel_type','vehicleModelList','vehicleMakeList','IndustryDepartment'));

    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getItemByModel(Request $request)
    {   
         $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
       else if (Auth::user()->customer!=null) {
          $rocode=Auth::user()->customer->RO_code;
       }
        $make = $request->input('model');
        $VehicleModel=VehicleModel::find($make);
        

        
        $items=$VehicleModel->getstockitemname->getItem()->where('RO_Code',$rocode)->where('is_active',1)->pluck('Item_Name','id')->toArray();
        $data=['capacity'=>$VehicleModel->capacity,'items'=>$items];
        Log::info('data ======'.print_r($data,true));

       return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function vechstore(Request $request)
    { 

         $make = $request->input('Make'); 
          $Model = $request->input('Model'); 

        
        //this is used for show default date null
        $date = $request->input('puc_date');
        if(!empty($date))
        {
           $date1 = str_replace('/', '-', $date);
           $puc_date = date('Y-m-d', strtotime($date1));
        }
         else
                    { 
                      $puc_date = null;
                    }
       

        $rc_date = $request->input('rc_valide');
            if(!empty($rc_date))
                {

                 $date2 = str_replace('/', '-', $rc_date);
                 $rc_valide = date('Y-m-d', strtotime($date2));
             }
            else
                { 
                  $rc_valide = null;
                }


            $date = $request->input('insurance_due_date');
            if(!empty($date))
            {

              $date1 = str_replace('/', '-', $date);
              $insurance_due_date = date('Y-m-d', strtotime($date1));
              }
            else
                { 
                  $insurance_due_date = null;
                }
           


            // $date =$request->input('puc_date');
            // $puc_date = date("Y-m-d", strtotime($date));

         /*$date = $request->input('insurance_due_date');
        $date1 = str_replace('/', '-', $date);
        $mysqltime = date('Y-m-d', strtotime($date1));
        $rc_date = $request->input('rc_valide');
        $date2 = str_replace('/', '-', $rc_date);
        $rc_valide = date('Y-m-d', strtotime($date2));*/

        $Registration_Number=$request->input('Registration_Number');
        Log::info('Registration_Number -  '.$Registration_Number);
       /// $Registration_Number="123456";
         //Log::info('Registration_Number -  '.$Registration_Number);

        $checkReg=DB::table('tbl_customer_vehicle_master')->where('Customer_code',$request->input('Customer_code'))->where('Registration_Number',$Registration_Number)->get();
         Log::info('checkReg     -  '.Print_r($checkReg,true));

        if(!$checkReg->isEmpty()){
             Log::info('Registration_Number - already exist '.$Registration_Number);

         $request->session()->flash('success','Registration Number Already Exist!!');

         return redirect('vehiclemanagement');
        }else{
            Log::info('Registration_Number - new registration continue - '.$Registration_Number);
        }
      
            $VechilManage = new VechilManage();
         // $date =$request->input('insurance_due_date');
         // $mysqltime = date("Y-m-d", strtotime($date));
       
        $VechilManage->Ro_code=$request->input('Ro_code'); 
        $VechilManage->Customer_code=$request->input('Customer_code'); 
        $VechilManage->Registration_Number=$Registration_Number;
        $VechilManage->pre_authori=$request->input('pre_authori');
        $VechilManage->Make=$make;
        $VechilManage->Model=$Model;
        $VechilManage->rc_valide=$rc_valide;
        $VechilManage->insurance_due_date=$insurance_due_date;
        $VechilManage->Fuel_Type=$request->input('Fuel_Type');
        $VechilManage->puc_date=$puc_date;
        $VechilManage->capacity=$request->input('capacity');
        $VechilManage->van_color=$request->input('van_color');
        $VechilManage->industry_company=$request->input('industry_company');
        $VechilManage->save();
         
        //  $num1='-'.$request->input('Ro_code').'-'.$request->input('Customer_code').'-'.$request->input('Registration_Number');
        //    $num2  = uniqid();
        //  //= (rand(100,1000));
        // $QR_Code = new QR_Code();
        // $QR_Code->Ro_code=$request->input('Ro_code'); 
        // $QR_Code->Customer_Code=$request->input('Customer_code');
        // $QR_Code->Vehicle_Reg_no=$request->input('Registration_Number');
        // $QR_Code->qrcode=$num2.$num1;
        // $QR_Code->save();
       // $request->session()->flash('success','QR Code Generate Successfully!!');
        $request->session()->flash('success','Added Successfully!!');
        // } catch(\Illuminate\Database\QueryException $e){
                
        //         $request->session()->flash('success','Something wrong!!');
                
        //     }
        
        
       return redirect('vehiclemanagement');
    }

   public function vechilDeactive(Request $request ,$id)
   {
   	    $data = $VechilManage=VechilManage::all(); 
       



   }
    /**
     * Display the specified resource.
     *
     * @param  \App\VechilManage  $VechilManage
     * @return \Illuminate\Http\Response
     */
    public function show(VechilManage $VechilManage,$id)
    {
        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
         $VechilManage= VechilManage::find($id);
          $VehicleMakeModel = VehicleMakeModel::where('id',$VechilManage->Make)->first();
           $VehicleModel = VehicleModel::where('id',$VechilManage->Model)->first();
         
         $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->select('tbl_price_list_master.*')->get();

          $data1 = $MastorRO=MastorRO::where('is_active','1');

          if(Auth::user()->user_type!=1 && $rocode!='')
                $data1=$data1->where('tbl_ro_master.RO_code',$rocode);

              $data1=$data1->get();
        // $getdesil = DB::table('tbl_ro_petroldiesel')->get();
 $IndustryDepartment = IndustryDepartment::where('type','Insurance Company')->get();
 Log::info('VehicleMangeController@show  VechilManage - '.print_r($VechilManage,true));

        return view('backend.vehicle_view_page',compact('VechilManage','fuel_type','data1','VehicleMakeModel','VehicleModel','IndustryDepartment')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VechilManage  $VechilManage
     * @return \Illuminate\Http\Response
     */
    public function edit(VechilManage $VechilManage , $id)
    {

        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $VechilManage=$VechilManage->find($id);
        $QR_Code =  QR_Code::where('Vehicle_Reg_no',$VechilManage->Registration_Number)->first();
          $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
         $getpric=  $getpric->pluck('item_id')->toArray();
         $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->whereIn('tbl_price_list_master.id',$getpric)->select('tbl_price_list_master.*');

         if(Auth::user()->user_type!=1 && $rocode!='')
                $fuel_type=$fuel_type->where('tbl_price_list_master.RO_Code',$rocode);

              $fuel_type=$fuel_type->get();

          $data1 = $MastorRO=MastorRO::where('is_active','1');

          if(Auth::user()->user_type!=1 && $rocode!='')
                $data1=$data1->where('tbl_ro_master.RO_code',$rocode);

              $data1=$data1->get();

        // $getdesil = DB::table('tbl_ro_petroldiesel')->get();
       $Model=VehicleModel::where('id',$VechilManage->Model)->first();
    
        $vehicleModelList = VehicleModel::orderBy('name', 'asc')->get();;
        $vehicleMakeList = VehicleMakeModel::orderBy('name', 'asc')->get();
         $IndustryDepartment = IndustryDepartment::where('type','Insurance Company')->get();
         Log::info('VehicleMangeController@edit VechilManage'.print_r($VechilManage,true));


        return view('backend.vehicle_update',compact('VechilManage','fuel_type','data1','vehicleModelList','vehicleMakeList','Model','QR_Code','IndustryDepartment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VechilManage  $VechilManage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VechilManage $VechilManage, $id)
    {
 
        $input=$request->all();

         Log::info('vechiclemanageController@update input '.print_r($input,true));
        $vechicle_no=$input['Registration_Number'];
        $customer_code=$input['Customer_code'];
        
        if($request->input('Make')==null || $request->input('Make')=='Others'){
              
              $make = $request->input('Makeother');

        }
        else
        {
           $make = $request->input('Make'); 

        }

         if($request->input('Model')==null || $request->input('Model')=='other'){
              
              $model = $request->input('modelother');

        }
        else
        {
           $model = $request->input('Model'); 

        }

   DB::beginTransaction();
       
      
        try {
                   //this is used for show default date null
                 $date = $request->input('puc_date');
                  
                if(!empty($date))
                {
                   $date1 = str_replace('/', '-', $date);
                   $puc_date = date('Y-m-d', strtotime($date1));
                    
                }
                 else
                    { 
                      $puc_date = null;
                    }

            $rc_date = $request->input('rc_valide');
            if(!empty($rc_date))
                {

                 $date2 = str_replace('/', '-', $rc_date);
                 $rc_valide = date('Y-m-d', strtotime($date2));
             }
            else
                { 
                  $rc_valide = null;
                }


            $date = $request->input('insurance_due_date');
            if(!empty($date))
            {

              $date1 = str_replace('/', '-', $date);
              $insurance_due_date = date('Y-m-d', strtotime($date1));
              }
            else
                { 
                  $insurance_due_date = null;
                }
           

            $VechilManage = VechilManage::find($id);
            // check vechicle no. and update
            Log::info('AdminController@tankEditData VechilManage '.print_r($VechilManage,true));
            Log::info('AdminController@tankEditData id '.print_r($id,true));
            



            $status=0;
            $rocode='';
             if (Auth::user()->getRocode!=null) {
                $rocode=Auth::user()->getRocode->RO_code;
             }
             // $vechicle_no=$input['Vehicle_Reg_No'];
              //$customer_code=$input['Customer_code'];
             Log::info('VechilManageController@update rocode   - '.$rocode);

             $vechicle_check= VechilManage::where('Registration_Number',$vechicle_no)
                             ->where('RO_code',$rocode)
                             ->where('Customer_code',$customer_code)->first();

             Log::info('AdminController@tankEditData vechicle_check '.print_r($vechicle_check,true));
            
             if($vechicle_check){

              if($vechicle_check->id!=$VechilManage->id){

                   Log::info('AdminController@tankEditData     VechicleAlready Exist!!!!!!!!!!!!!!!!!!!!');
                   $request->session()->flash('success','Vechicle Already Exist!!');
                return back();

              }else{

                 if($vechicle_check->Registration_Number!=$VechilManage->Registration_Number){

                   $status=1;   
                 }  

                Log::info('AdminController@tankEditData      Ready to update Vechicle!!!!!!!!!!!!!!!!!!!!');

        
              }


             }else{

             $status=1;               
             }

             //update dependant table

              if($VechilManage->Registration_Number&&$status=1){

                   Log::info('AdminController@tankEditData     need to update reg_no in all table!!!!!!!!!!!!!!!!!!!!');
                     Log::info('AdminController@tankEditData     customer_code - '.$customer_code.'  Ro_code'.$rocode.' $VechilManage->Registration_Number old  -'.$VechilManage->Registration_Number.' vechicle_no new - '.$vechicle_no);

                      DB::table('tbl_customer_vehicle_driver')
                            ->where('customer_code', $customer_code)
                            ->where('Ro_code', $rocode)
                            ->where('Registration_Number', $VechilManage->Registration_Number)
                            ->update(['Registration_Number' =>$vechicle_no]);


             DB::table('tbl_qrcode_manage')
                            ->where('Customer_Code', $customer_code)
                            ->where('Ro_code', $rocode)
                            ->where('Vehicle_Reg_no', $VechilManage->Registration_Number)
                            ->update(['Vehicle_Reg_no' =>$vechicle_no]);



            DB::table('tbl_customer_request_petroldiesel')
                            ->where('customer_code', $customer_code)
                            ->where('Ro_code', $rocode)
                            ->where('Vehicle_Reg_no', $VechilManage->Registration_Number)
                            ->update(['Vehicle_Reg_no' =>$vechicle_no]);




            DB::table('tbl_customer_transaction')
                            ->where('customer_code', $customer_code)
                            ->where('Ro_code', $rocode)
                            ->where('Vehicle_Reg_no', $VechilManage->Registration_Number)
                            ->update(['Vehicle_Reg_no' =>$vechicle_no]);


                DB::table('customer_slips')
                            ->where('customer_code', $customer_code)
                            ->where('rocode', $rocode)
                            ->where('Vehicle_Reg_no', $VechilManage->Registration_Number)
                            ->update(['Vehicle_Reg_no' =>$vechicle_no]);


             DB::table('tbl_customer_request_lube')
                            ->where('customer_code', $customer_code)
                            ->where('Ro_code', $rocode)
                            ->where('Vehicle_Reg_no', $VechilManage->Registration_Number)
                            ->update(['Vehicle_Reg_no' =>$vechicle_no]);

                DB::table('invoice_vehicle_summary')
                            //->where('customer_code', $customer_code)
                            ->where('Ro_code', $rocode)
                            ->where('vihcle_no', $VechilManage->Registration_Number)
                            ->update(['vihcle_no' =>$vechicle_no]);




                
              }








            // check vechicle no. and update


            $VechilManage->Ro_code=$request->input('Ro_code'); 
            $VechilManage->Customer_code=$request->input('Customer_code'); 
            $VechilManage->Registration_Number=$request->input('Registration_Number');
            $VechilManage->pre_authori=$request->input('pre_authori');
            $VechilManage->Make=$make;
            $VechilManage->Model=$model;
            $VechilManage->Fuel_Type=$request->input('Fuel_Type');
            $VechilManage->capacity=$request->input('capacity');
            $VechilManage->puc_date=$puc_date;
            $VechilManage->rc_valide=$rc_valide;
            $VechilManage->insurance_due_date=$insurance_due_date;
            $VechilManage->van_color=$request->input('van_color');
            $VechilManage->industry_company=$request->input('industry_company');
            $VechilManage->save();
            
        //$data = $VechilManage;
          DB::commit();
        $request->session()->flash('success','Data Updated Successfully!!'); 
        } catch(\Illuminate\Database\QueryException $e){
                DB::rollback();
                
                $request->session()->flash('success','Something wrong!!');
                
            }
          return redirect('vehiclemanagement');
    }

    function active(Request $request,$id, $actt)
    {
        
    	$ac = VechilManage::where('id',$id)->update(['is_active' => $actt]);
        if($actt == 0)
        {
            $reg_no = VechilManage::where('id',$id)->pluck('Registration_Number')->first();
            $this->allVechileRegistrationDeactivate($reg_no);
        }

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }

     public function allVechileRegistrationDeactivate($reg_no)
     {
        DB::table('tbl_customer_vehicle_driver')->where('Registration_Number',$reg_no)->update(['is_active' => 0]);
        DB::table('tbl_qrcode_manage')->where('Vehicle_Reg_no',$reg_no)->update(['is_active' => 0]);
     }
      public function delete(Request $request, $id)
    {
        try {
          $ac = VechilManage::where('id',$id)->delete();
        $request->session()->flash('success','Delete Successfully !!');  
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
       
        return back();


    }
     public function getmodelname(Request $request){

        $make = $request->input('make1');
        $VehicleMakeModel = VehicleModel::where('make_id',$make)->orderBy('name', 'asc')->get();
         $VehicleMakeModel =$VehicleMakeModel->pluck('name','id')->toArray();
         return response()->json($VehicleMakeModel);
     }
     public function getmodelnameupdate(Request $request){

        $make = $request->input('make1');
        $VehicleMakeModel = VehicleModel::where('make_id',$make)->orderBy('name', 'asc')->get();
         $VehicleMakeModel =$VehicleMakeModel->pluck('name','id')->toArray();
       // Log::info('VehicleMangeController  getmodelnameupdate'.print_r($getmodelnameupdate,true));
         return response()->json($VehicleMakeModel);
     }
}
    
	   
	
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VechilManage  $VechilManage
     * @return \Illuminate\Http\Response
     */
    

