<?php

namespace App\Http\Controllers;

//use App\CityController;
use Illuminate\Http\Request;
use DB;
use App\User;
use Carbon\Carbon;
use Auth;
use App\OutletConfigModel;
use App\TableItemPriceListModel;
use App\Stock_item_group;


class OutletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        if (Auth::user()->getRocode!=null) {

         $rocode=Auth::user()->getRocode->RO_code;
         
     }
        $AdminconfigtModels = OutletConfigModel::where('rocode',$rocode)->get();
        //dd($AdminconfigtModels);
        
        return view('backend.outletconfig',compact('AdminconfigtModels')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create(Request $request)
    {
        //dd($request->all());
         $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;//dd($rocode);
       }
        $price_specific = $request->input('price_specific');
        


        if (!empty($price_specific)) {
            $Stock_item_group=Stock_item_group::where('Group_Name','FUEL')->first();
            if($Stock_item_group!=null && $Stock_item_group->getAllitem!=null){
                $datepr=$Stock_item_group->getAllitem()->where('RO_Code',$rocode)->where('is_active',1)->pluck('id')->toArray();
                $TableItemPriceListModel=TableItemPriceListModel::whereIn('item_id',$datepr)->where('is_active',1)->get();
             
            
           //dd($TableItemPriceListModel);
            if (count($TableItemPriceListModel)>0) {//dd('hiii');
                $date1 = $TableItemPriceListModel[0]->effective_date;
                $date_last_price =date('Y-m-d H:i:s',strtotime($date1));
                $fdate = Carbon::now();
                $date2 = new Carbon(str_replace('/', '-',$fdate));
                $form_date =date('Y-m-d H:i:s',strtotime($date2));
                foreach ($TableItemPriceListModel as  $value) {
                    $item[]=$value->item_id;
                    $price[]=$value->price;
                }
               //dd($item,$price,$date_last_price,$form_date);
                if ($date_last_price <= $form_date ) {
                   
                    while ($date_last_price <= $form_date ) {
                        for($i=0;$i<count($item);$i++)
                        {
                            if(isset($price[$i]) && isset($item[$i])){
                                $is_active = TableItemPriceListModel::where('item_id',$item[$i])->where('is_active',1)->update(['is_active' =>0]);
                                $update_price = new TableItemPriceListModel();
                                $update_price->item_id = $item[$i];
                                $update_price->price = $price[$i];
                                $update_price->effective_date =  $date_last_price;
                                $update_price->save();
                            }
                        }    
                        $date_last_price = date('Y-m-d H:i:s',strtotime($date_last_price.'+ 1 Day'));
                    }      
                }
            }
        }
        }
       
         $fields = $request->input('field');
          foreach ($fields as $field) {
              $v=$request->input($field);
             $rocode = $request->input('RO_code');

             $AdminconfigtModel = OutletConfigModel::where('rocode',$rocode)->where('field_name',$field)->first();

               if($field=='FUEL_PRICE_Date_time'){
                   $date = new Carbon(str_replace('/', '-',$v));
                   $date =date('Y-m-d H:i:s',strtotime($date));
                  
                   $AdminconfigtModel->value =$date;
             }else{
                
                   $AdminconfigtModel->value =$v;
            }
            
             $AdminconfigtModel->save();
              
          }
          
          
          $request->session()->flash('success','Update Successfully!!'); 
        
         
          return back();
 
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function show(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function edit(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContributionModel $contributionModel)
    {
        //
    }
}
