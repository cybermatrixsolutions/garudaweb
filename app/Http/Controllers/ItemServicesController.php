<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ItemServicesModel;
use App\ItemServicesTaxModel;
use App\MastorRO;
use Auth;
use Gate;
use DB;
class ItemServicesController extends Controller
{    

   public function index()

    {
       
        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $services =ItemServicesModel::where('RO_code',$rocode)->orderby('id','desc')->get();

        $data1 = $MastorRO=MastorRO::where('is_active','1');

        if(Auth::user()->user_type!=1 && $rocode!='')
                $data1=$data1->where('tbl_ro_master.RO_code',$rocode);

              $data1=$data1->get();
        
        return view('backend.item_services',compact('data','data1','services'));

    }
    public function getrotaxservices(Request $request){
            $rcode=$request->input('rocode');
            $taxtype=$request->input('taxtype');
            
            $rodode = MastorRO::where('RO_code',$rcode)->first();
             //dd($rodode);
            $state = DB::table('states')->where('id',$rodode->state)->first();
           
            $tax=DB::table('tbl_tax_master')->where('Tax_Code','Like', $state->statecode.'%')->where('Tax_Type',$taxtype);
            // dd($tax);
            $rodode=$tax->pluck('Tax_Code','id')->toArray();
            // dd($rodode);
            return response()->json($rodode);

          }

    public function add_item(Request $request)
    { 
        try {
        $ItemServicesModel = new ItemServicesModel();
        $ItemServicesModel->RO_code=$request->input('Ro_code'); 
        $ItemServicesModel->name=$request->input('ServicesName');
        $ItemServicesModel->tax_type=$request->input('taxtype');
        $ItemServicesModel->hsn=$request->input('hsn_code');
        $ItemServicesModel->save();
        $daat=$request->input('tax');

        foreach ($daat as $key => $value) {
        $ItemServicesTaxModel = new ItemServicesTaxModel();
        $ItemServicesTaxModel->tax_id = $value;
        $ItemServicesTaxModel->services_id = $ItemServicesModel->id;
        $ItemServicesTaxModel->save();
        $request->session()->flash('success','Added Successfully!!'); 
        }
  }
         catch(\Illuminate\Database\QueryException $e){
                
        $request->session()->flash('success','Something wrong!!');
                
           }
       
        
       return back();
    }
    
}
