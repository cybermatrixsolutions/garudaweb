<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\RoPieceListManagement;
use App\TableStockItemMaster;
use App\RoMaster;
use App\RoLOBItemSelectionModel;
use App\RoLobSelectionModel;
use App\StockItemManagement;
use App\LOBMasterModel;

use DB;
use Illuminate\Support\Facades\Redirect;

use App\Http\Controllers\Controller;

class LOBMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          
         $lob_list_master = LOBMasterModel::orderby('id','desc')->get();
        //return back()->with('ro_code',$ro_code);
        return view('backend.lob_master_page',compact('lob_list_master'));

         
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($ro_code)
    {

       
       $pump_legal_name_list= RoMaster::where('is_active','1')->get();
        $lob_item_list= RoLOBItemSelectionModel::where('is_active','1')->get();
        $get_lob= RoLobSelectionModel::where('RO_code',$ro_code)->get();

     return view('backend.lob_item_page',compact('pump_legal_name_list','lob_item_list','get_lob','ro_code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $lob_data = new LOBMasterModel();
          $lob_data->lob_name = strtoupper($request->input('lob_name'));
          $lob_data->save();
          $request->session()->flash('success','Add Successfully!!'); 
         return Redirect('lob_master_page');

         
    }


//          $validatedData = $request->validate([
//          'price_ro_code' => 'required',
//          'price_item_list' => 'required',
         
//          'volume_liter' => 'required',
//          'item_name' => 'required',
//          ]);
       
//         try {

            
//             $personal_mangement = new RoPieceListManagement();
//             $personal_mangement->RO_Code = $request->input('price_ro_code');
//             $personal_mangement->Item_id = $request->input('price_item_list');
//             // $personal_mangement->Price = $request->input('price');
            
//             $personal_mangement->Volume_ltr = $request->input('volume_liter');            
//             $personal_mangement->Item_Name = $request->input('item_name');
//             $date =$request->input('Active_From_Date');

//             $Active_From_Date = date("Y-m-d", strtotime($date));

            
//             $personal_mangement->Stock_Group = $request->input('Stock_Group');
//             $personal_mangement->Unit_of_Measure = $request->input('Unit_of_Measure');
//             $personal_mangement->Active_From_Date= $Active_From_Date;
//             $personal_mangement->Tail_Unit = $request->input('Tail_Unit');
//             $personal_mangement->Conversion = $request->input('Conversion');
//             $personal_mangement->Alternate_Unit = $request->input('Alternate_Unit');
//             $personal_mangement->LOB = $request->input('LOB');
//             $personal_mangement->brand = $request->input('brand');
//             $personal_mangement->tax = $request->input('tax');
//             $personal_mangement->hsncode = $request->input('hsncode');
//             $personal_mangement->save();
//              $request->session()->flash('success','Add Successfully!!');
             
//         } catch(\Illuminate\Database\QueryException $e){
                
//                 $request->session()->flash('success','Something wrong!!');
                
// } 
        

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $price_list = DB::table('tbl_price_list_master')->where('id', '=', $id)->first();
         $stock_group = DB::table('tbl_stock_item_group')->where('is_active', '>=',1)->get();
         $datas= RoMaster::where('is_active','1')->get();
          $lob = DB::table('tbl_lob_master')->get();
         $unit_measure = DB::table('tbl_unit_of_measure')->where('is_active', '>=',1)->get();
         $item_master_list= TableStockItemMaster::All();
         $taxmaster = DB::table('tbl_tax_master')->get();
        return view('backend.priceEdit', compact('price_list', 'datas','item_master_list','stock_group','unit_measure','taxmaster','lob'));
    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $price_list = DB::table('tbl_price_list_master')->where('id', '=', $id)->first();
          $stock_group = DB::table('tbl_stock_item_group')->where('is_active', '>=',1)->get();
         $datas= RoMaster::where('is_active','1')->get();
         $unit_measure = DB::table('tbl_unit_of_measure')->where('is_active', '>=',1)->get();
         $item_master_list= TableStockItemMaster::All();
        return view('backend.price_view', compact(['price_list', 'datas','item_master_list','stock_group','unit_measure']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        
        $validatedData = $request->validate([
         
        
         'price_ro_code' => 'required',
         'price_item_list' => 'required',
         
         'volume_liter' => 'required',
         'item_name' => 'required',
    ]);


        try {
           
            $personal_mangement = RoPieceListManagement::find($id);
            $personal_mangement->RO_Code = $request->input('price_ro_code');
            $personal_mangement->Item_id = $request->input('price_item_list');
            // $personal_mangement->Price = $request->input('price');
            
            $personal_mangement->Volume_ltr = $request->input('volume_liter');            
            $personal_mangement->Item_Name = $request->input('item_name');
            $date =$request->input('Active_From_Date');
            $Active_From_Date = date("Y-m-d", strtotime($date));
            
            $personal_mangement->Stock_Group = $request->input('Stock_Group');
            $personal_mangement->Unit_of_Measure = $request->input('Unit_of_Measure');
            $personal_mangement->Active_From_Date= $Active_From_Date;
            $personal_mangement->Tail_Unit = $request->input('Tail_Unit');
            $personal_mangement->Conversion = $request->input('Conversion');
            $personal_mangement->Alternate_Unit = $request->input('Alternate_Unit');
            $personal_mangement->LOB = $request->input('LOB');
            $personal_mangement->brand = $request->input('brand'); 
            $personal_mangement->tax = $request->input('tax');
            $personal_mangement->hsncode = $request->input('hsncode');    
            $personal_mangement->save();


            $request->session()->flash('success','Data Updated Successfully!!'); 
       } catch(\Illuminate\Database\QueryException $e){
                
                 $request->session()->flash('success','Something wrong!!');
                
            }
       
       
            return redirect('price_list');
    }
    function active(Request $request,$id, $actt)
    {
        
     $ac = LOBMasterModel::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $price_list = RoPieceListManagement::find($id);

        $price_list->delete();
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
        return back();
    }
}
