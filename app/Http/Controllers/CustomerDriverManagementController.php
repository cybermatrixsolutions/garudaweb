<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Storage;
use Illuminate\Support\Facades\Input;
use File;
use Carbon\Carbon;
use App\RoMaster;
use DB;
use App\RoCustomertManagement;
use App\CustomerDriverManager;
use App\Http\Controllers\Controller;
use Gate;
use Auth;
use App\User;

class CustomerDriverManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $datas= RoMaster::where('is_active','1');

        if(Auth::user()->user_type!=1 && $rocode!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();

        $customer_driver_list = CustomerDriverManager::join('tbl_ro_master', 'tbl_customer_vehicle_driver.Ro_code', '=', 'tbl_ro_master.RO_code')
            ->join('tbl_customer_master', 'tbl_customer_vehicle_driver.customer_code', '=', 'tbl_customer_master.Customer_Code')
            ->select('tbl_customer_vehicle_driver.*','tbl_ro_master.pump_legal_name as pump_name','tbl_customer_master.company_name as company_name')->orderby('id','desc');

            if(Auth::user()->user_type!=1 && $rocode!='')
                $customer_driver_list=$customer_driver_list->where('tbl_customer_vehicle_driver.Ro_code',$rocode);

              $customer_driver_list=$customer_driver_list->get();

        $customer_code = RoCustomertManagement::where('is_active',1);

        if(Auth::user()->user_type!=1 && $rocode!='')
                $customer_code=$customer_code->where('tbl_customer_master.RO_code',$rocode);

              $customer_code=$customer_code->get();


        return view('backend.rodriver',compact(['datas','customer_driver_list','customer_code']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { //dd($request->all());
      $customercode= $request->input('customer_code');
      $Driver_Mobile =$request->input('Driver_Mobile');
      $Registration_Number=$request->input('Registration_Number');

      if($Registration_Number==null || $Registration_Number==''){
             $CustomerDriverManager = CustomerDriverManager::where('Driver_Mobile',$Driver_Mobile)->where('customer_code',$customercode)->get();

      }
      else{
            $CustomerDriverManager = CustomerDriverManager::where('Registration_Number',$Registration_Number)->where('customer_code',$customercode)->get();

      }
        if($CustomerDriverManager->count()>0)
        {
            $request->session()->flash('success','Already Exits  !!');  
             return back(); 
        }
       


        $neusers=User::where('user_type','<>',8)->where('mobile',$request->input('Driver_Mobile'))->get();

        if($neusers->count()>0){
           $request->session()->flash('success','Mobile No Already Exist !!');
           return back(); 
        }
      $date = $request->input('valid_upto');
         if(!empty($date))
                    {
                        $date1 = str_replace('/', '-', $date);
                        $value = date('Y-m-d', strtotime($date1));

                    } 
                    else
                    { 
                      $value = null;
                    }
                    
        try {
             if($request->hasFile('aadhaar_img'))
             {
                $file = Input::file('aadhaar_img');
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $filename = $file->getClientOriginalName();

                $name = $timestamp . '-' . $filename;
                $aadhaar_img = 'images/' . $name;

                $file->move('images/', $name);
             }//dd($aadhaar_img);
        $customer_driver_mangement = new CustomerDriverManager();
        $customer_driver_mangement->Ro_code = $request->input('customer_driver_ro_code');
        $customer_driver_mangement->customer_code = $customercode;
        $customer_driver_mangement->Driver_Name = $request->input('driver_name');
        $customer_driver_mangement->gender = $request->input('gender');
        $customer_driver_mangement->Driver_Mobile = $request->input('Driver_Mobile');
        $customer_driver_mangement->Registration_Number = $request->input('Registration_Number');            
        $customer_driver_mangement->email = $request->input('email');
        $customer_driver_mangement->Driver_Licence_No = $request->input('licence_no');
        $customer_driver_mangement->valid_upto = $value; 
         if (!empty($request->input('aadhaar_no'))) {
               $customer_driver_mangement->Aadhaar_no = $request->input('aadhaar_no');
            }
            if (!empty( $request->hasFile('aadhaar_img'))) {
                //dd('hii');
                 $customer_driver_mangement->Aadhaar_img = $aadhaar_img;
            }
        $customer_driver_mangement->save(); 

         if($customer_driver_mangement->email!=null){
                 $email=$customer_driver_mangement->email;
             }else{
                 $email=$request->input('Driver_Mobile').'@gmail.com';
             }


            $neuser=User::where('email',$email)->first();

            
            if($neuser==null){

                $user = new User();
                $user->name=$customer_driver_mangement->Driver_Name;
                $user->email=$email;
                $user->username=$email;
                $user->user_type=8;
                $user->password=bcrypt($request->input('Driver_Mobile'));
                $user->mobile=$request->input('Driver_Mobile');
                $user->save();
               // dd('hellodfsdfsd');
            }

        $request->session()->flash('success','Added Successfully!!');
        } catch(\Illuminate\Database\QueryException $e){
              
             
                $request->session()->flash('success','Something Wrong!!');  
            }
      
       
        
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $customber_driver_data = DB::table('tbl_customer_vehicle_driver')->where('id', '=', $id)->first();
         $datas = RoMaster::where('is_active',1);

          if(Auth::user()->user_type!=1 && $rocode!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();

         $customer_code = RoCustomertManagement::where('is_active',1);

         if(Auth::user()->user_type!=1 && $rocode!='')
                $customer_code=$customer_code->where('tbl_customer_master.RO_code',$rocode);

              $customer_code=$customer_code->get();
        
        return view('backend.customer_driver_view', compact(['customber_driver_data', 'datas','customer_code']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $customber_driver_data = DB::table('tbl_customer_vehicle_driver')->where('id', '=', $id)->first();
         $datas = RoMaster::where('is_active',1);

         if(Auth::user()->user_type!=1 && $rocode!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();

         $customer_code = RoCustomertManagement::where('is_active',1);

         if(Auth::user()->user_type!=1 && $rocode!='')
                $customer_code=$customer_code->where('tbl_customer_master.RO_code',$rocode);

              $customer_code=$customer_code->get();
        
        return view('backend.customer_driver_edit', compact(['customber_driver_data', 'datas','customer_code']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        // $url='images/driver.png';
        // if($request->hasFile('licencephoto'))
        //  {
        //     $file = Input::file('licencephoto');
        //     $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        //     $filename = $file->getClientOriginalName();

        //     $name = $timestamp . '-' . $filename;
        //     $url = 'images/' . $name;

        //     $file->move('images/', $name);
        //  }
        $customercode= $request->input('customer_code');
        $Driver_Mobile =$request->input('Driver_Mobile');
        $Registration_Number =$request->input('Registration_Number');

        if($Registration_Number==null || $Registration_Number==''){


                $CustomerDriverManager = CustomerDriverManager::where('id','!=',$id)->where('Driver_Mobile',$Driver_Mobile)->where('customer_code',$customercode)->get();
        }
        else{
            $CustomerDriverManager = CustomerDriverManager::where('id','!=',$id)->where('Registration_Number',$Registration_Number)->where('customer_code',$customercode)->get();

        }
              
        if($CustomerDriverManager->count()>0)
        {
            $request->session()->flash('success','Already Exits !!');  
             return back(); 
        }else{ 
     
       $neusers=User::where('user_type','<>',8)->where('mobile',$request->input('Driver_Mobile'))->get();

        if($neusers->count()>0){
           $request->session()->flash('success','Mobile No Already Exist !!');
           return back(); 
        }

        $date = $request->input('valid_upto');
       // $date1 = str_replace('/', '-', $date);
      // $valid_upto = date('Y-m-d', strtotime($date1));

        if(!empty($date))
                    {
                        $date1 = str_replace('/', '-', $date);
                        $value = date('Y-m-d', strtotime($date1));

                    } 
                    else
                    { 
                      $value = null;
                    }
        try {
            if($request->hasFile('aadhaar_img'))
             {
                $file = Input::file('aadhaar_img');
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $filename = $file->getClientOriginalName();

                $name = $timestamp . '-' . $filename;
                $aadhaar_img = 'images/' . $name;

                $file->move('images/', $name);
             }
            $customer_driver_mangement = CustomerDriverManager::find($id);
         $customer_driver_mangement->Ro_code = $request->input('customer_driver_ro_code');
        $customer_driver_mangement->customer_code = $request->input('customer_code');
        $customer_driver_mangement->Driver_Name = $request->input('driver_name');
         $customer_driver_mangement->gender = $request->input('gender');
        $customer_driver_mangement->Driver_Mobile = $request->input('Driver_Mobile');
        $customer_driver_mangement->valid_upto = $value; 
        $customer_driver_mangement->email = $request->input('email');
         $customer_driver_mangement->Registration_Number = $request->input('Registration_Number');
        $customer_driver_mangement->Driver_Licence_No = $request->input('licence_no');
         if (!empty($request->input('aadhaar_no'))) {
                       $customer_driver_mangement->Aadhaar_no = $request->input('aadhaar_no');
                    }
                    if (!empty( $request->hasFile('aadhaar_img'))) {
                        //dd('hii');
                         $customer_driver_mangement->Aadhaar_img = $aadhaar_img;
                    }
       // $customer_driver_mangement->photo = $url;
        // $date =$request->input('Operating_date');
        // $Operating_date=date('Y-m-d', strtotime(str_replace('-','/', $date)));
        // $customer_driver_mangement->Operating_date = $Operating_date;

            $customer_driver_mangement->save();
             $request->session()->flash('success','Data Updated Successfully!!');
            //return back();
            return redirect('driver');
           
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');  
            }
           }
            return redirect('rodriver');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
           $personal_mangement = CustomerDriverManager::find($id);

        $personal_mangement->delete(); 
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
        

        return back();
    }
    function active(Request $request,$id, $actt)
    {
        
        $ac = CustomerDriverManager::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }
}
