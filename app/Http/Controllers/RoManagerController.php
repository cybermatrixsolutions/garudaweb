<?php

namespace App\Http\Controllers;

use App\RoManager;
use Illuminate\Http\Request;
use App\MastorRO;
use Auth;
use Gate;

class RoManagerController extends Controller
{  

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
       if (Gate::allows('custermer',Auth::user())) {
                return redirect('Dashboard');
             }
       
        $RoManager = RoManager::all();
        $data1 =MastorRO::where('is_active','1')->get();
       return view('backend.manager',compact('RoManager','data1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    

        //dd($request->all());

          
 // $validatedData = $request->validate([
         
 //         'manager_EMAIL' => 'required|unique:tbl_ro_manager_master',
        
         
         
 //    ]);
 try {
        $RoManager = new RoManager();
         $RoManager->RO_code=$request->input('RO_code');
        $RoManager->manager_name=$request->input('manager_name');
        $RoManager->manager_phone=$request->input('manager_phone');
        $RoManager->manager_mobile=$request->input('manager_mobile');
        // $RoManager->manager_IMEI=$request->input('manager_IMEI');
        $RoManager->manager_EMAIL=$request->input('manager_EMAIL');
        
        $RoManager->save();
        $request->session()->flash('success','Added Successfully!!');
 } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');  
            }
        
        
       
        return redirect('manager');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RoManager  $roManager
     * @return \Illuminate\Http\Response
     */
    public function show(RoManager $roManager)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RoManager  $roManager
     * @return \Illuminate\Http\Response
     */
    public function edit(RoManager $roManager , $id)
    {  
        if (Gate::allows('custermer',Auth::user())) {
                return redirect('Dashboard');
        }
        $data1 =MastorRO::where('is_active','1')->get();

        $roManager=$roManager->find($id);

        return view('backend.managerEdit',compact('roManager','data1'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RoManager  $roManager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   

        try {
             $RoManager = RoManager::find($id);
              $RoManager->RO_code=$request->input('RO_code');
        $RoManager->manager_name=$request->input('manager_name');
        $RoManager->manager_phone=$request->input('manager_phone');
        $RoManager->manager_mobile=$request->input('manager_mobile');
        $RoManager->manager_EMAIL=$request->input('manager_EMAIL')
        // $RoManager->manager_IMEI=$request->input('manager_IMEI');
        
        $RoManager->save();
        $request->session()->flash('success','Data Updated Successfully!!');
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');  
            }
        
          
        return redirect('manager');
    }

  function active(Request $request,$id, $actt)
    {   
        
        
        $ac = RoManager::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactive Successfully !!');
            else
             $request->session()->flash('success','Activeted Successfully  !!');

         return back();
             
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RoManager  $roManager
     * @return \Illuminate\Http\Response
     */
    public function destroy(RoManager $roManager, $id)
    {
        try {
            $RoManager = $RoManager->find($id);
        $roManager->delete();
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
        

        return back();
     }   
    
}
