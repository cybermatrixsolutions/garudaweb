<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
// use Illuminate\Support\Facades\Auth;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/Dashboard';

    /**
     * Create a new controller instance. 
     *
     * @return void
     */
            protected function credentials(Request $request) {
              return array_merge($request->only($this->username(), 'password'),['is_active' => 1]);
        }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    // public function authenticate()
    //  {
    //     if (Auth::attempt(['email' => $email, 'password' => $password, 'is_active' => 1])) {
    //      return redirect('backend.dashboard');
    //    }else{
    //       return redirect('\login');
    //    }
    // }

     protected function authenticated($request, $user)
    {  
        if(!in_array($user->user_type, [1,2,3,4,5])){
         
         Auth::logout();

         return redirect('login')->withErrors(['username',  'You do not have access to this application.']);
        }
        //user is admin
        if($user->user_type == 3) {

  //dd('hiii');
            if ($user->getRocode!=null && $user->getRocode->is_active==1) {
                $rocode=$user->getRocode->RO_code;
                 $roconfigTime='06:00:00';

                $OutletConfigModel=\App\OutletConfigModel::where('rocode',$rocode)->where('field_name','FUEL_PRICE_Date_time')->first();//dd($OutletConfigModel); 
                $price_specific=\App\OutletConfigModel::where('rocode',$rocode)->where('field_name','price_specific')->first();//dd($price_specific); 
                 if ($price_specific->value == "on") {
        //dd($data_price_specific->value);
            $TableItemPriceListModel=\App\TableItemPriceListModel::where('is_active',1)->get();
            if (count($TableItemPriceListModel)>0) {//dd($TableItemPriceListModel);
                $date1 = $TableItemPriceListModel[0]->effective_date;
                $date_last_price =date('Y-m-d H:i:s',strtotime($date1));//dd($date_last_price);
                $fdate = Carbon::now();
                $date2 = new Carbon(str_replace('/', '-',$fdate));
                $form_date =date('Y-m-d H:i:s',strtotime($date2));//dd($date_last_price,$form_date);
                foreach ($TableItemPriceListModel as  $value) {
                    $item[]=$value->item_id;
                    $price[]=$value->price;
                }
               
                if ($date_last_price <= $form_date ) {
                   //dd('hii');
                    while ($date_last_price <= $form_date ) {
                        for($i=0;$i<count($item);$i++)
                        {
                            if(isset($price[$i]) && isset($item[$i])){
                                $is_active = \App\TableItemPriceListModel::where('item_id',$item[$i])->where('is_active',1)->update(['is_active' =>0]);
                                $update_price = new \App\TableItemPriceListModel();
                                $update_price->item_id = $item[$i];
                                $update_price->price = $price[$i];
                                $update_price->effective_date =  $date_last_price;
                                $update_price->save();
                            }
                        }    
                        $date_last_price = date('Y-m-d H:i:s',strtotime($date_last_price.'+ 1 Day'));
                    }      
                }
            
        }
      
      }else{
        
                if($OutletConfigModel!=null)
                 $roconfigTime=date('H:i:s',strtotime($OutletConfigModel->value));

                $rocode=$user->getRocode->RO_code;
                $Stock_item_group=\App\Stock_item_group::where('Group_Name','FUEL')->first();
                if($Stock_item_group->getAllitem()!=null){
                 
                
                    $datepr=$Stock_item_group->getAllitem()->where('RO_Code',$rocode)->where('is_active',1)->pluck('id')->toArray();
                    if($datepr!=null){
                        
                        $TableItemPriceListModels=$Stock_item_group->getAllitem()->where('RO_Code',$rocode)->where('is_active',1)->get();
                        $TableItemPriceListModel=\App\TableItemPriceListModel::whereIn('item_id',$datepr)->orderBy('effective_date', 'desc')->first();
                        
                        if($TableItemPriceListModel!=null){
                            
                            $enddate=$TableItemPriceListModel->effective_date;

                            if(strtotime($enddate)<strtotime(date('Y-m-d').' '.$roconfigTime)){
                               
                               $enddate=date('Y-m-d',strtotime($enddate));
                               $enddate=date('Y-m-d', strtotime('+1 day', strtotime($enddate)));
                              
                              return redirect('fuel_price_management?date='.$enddate);
                            }
                        }else{
                            $enddate=date('Y-m-d');
                            return redirect('fuel_price_management?date='.$enddate);
                         }
                    }

                }
                
            }
            }
        }

        
    }
    
}
