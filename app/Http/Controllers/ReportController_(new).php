<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InvoiceBilling;
use App\TransactionModel;
use App\tax_master;
use App\nozzelsReadingModel;
use App\RoCustomertManagement;
use App\AddPetrolDieselModel;
use App\CustomerLubeRequest;
use App\InvoiceVehicleSummary;
use App\Credit_limit;
use App\RoPersonalManagement;
use App\CustomerDriverManager;
use Carbon\Carbon;
use Auth;
use Log;
class ReportController extends Controller
{
 //
     /**
      * salesRegisterBilled
      * @return \Illuminate\Http\Response
      */
    public function salesRegisterBilled(Request $request){
         
         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }

      $fule_type=$request->input('fule_type');
      $from_date = $request->input('fromdate');
      $to_date = $request->input('to_date');

       $taxs=tax_master::get()->groupBy('GST_Type');
        if($fule_type==null){
           $fule_type=2;
         }
   
       
   

      if($from_date!=null && $to_date!=null){
        
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));

              
        $bills=InvoiceBilling::where('ro_code',$rocode)->where('type',$fule_type)->where('billing_from','>=',$from_dates)->where('billing_from','<=',$to_dates)->get();

      }


       return view('backend.reports.salesRegisterBilled',compact('taxs','fule_type','bills'));

    }

    /**
    * salesRegisterBilledDownload
    * @return \Illuminate\Http\Response
    */
    public function salesRegisterBilledDownload(Request $request,$fule_type){


         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }

         $taxs=tax_master::get()->groupBy('GST_Type');
       
        
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');

         $taxs=tax_master::get()->groupBy('GST_Type');
          if($fule_type==null){
             $fule_type=2;
           }

         $bills=InvoiceBilling::where('ro_code',$rocode)->where('type',$fule_type)->get();
     

        if($from_date!=null && $to_date!=null){

          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));

                
          $bills=$bills->where('billing_from','>=',$from_dates)->where('billing_from','<=',$to_dates);
        }



                
            $str='Inv.Type,Invoice No.,Invoice Date,Invoice Amount,Party Code,Party Name,Item Group,Item Sub-Group,Item Code,Item Name,HSN Code,Item Qty Sold,Item Rate';
             
              foreach($taxs as $key=>$tax){
                $str.=','.$key.'%,'.$key.' Amount';
              }
             
              $str.= ',Handling Charges,Discount Amount,Round Off';

              $CsvData=array($str);
                    
                    $i=1;
             foreach($bills as $bill){

                foreach($bill->trangection as $TransactionModel){
                            $str='';
  
                             if($bill->type==1){
                                $str.='VAT';
                             }else{
                              $str.='GST';
                             }
                            
                            $str.=','.$bill->invoice_no;
                            $str.=','.date('d/m/y',strtotime($bill->created_at));
                            $str.=','.bcadd($bill->grand_total_amount, 0, 2);
                            $str.=','.$bill->customer_code;
                            $str.=','.$bill->party->company_name;
                            $str.=','.$TransactionModel->getItemName->getgroup->Group_Name;
                            $str.=','.$TransactionModel->getItemName->getsubgroup->Group_Name;
                            $str.=','.$TransactionModel->getItemName->Item_Code;
                            $str.=','.$TransactionModel->getItemName->Item_Name;
                            $str.=','.$TransactionModel->getItemName->hsncode;
                            $str.=','.$TransactionModel->petroldiesel_qty;
                            $str.=','.bcadd($TransactionModel->item_price, 0, 2);

                            $tratex=$TransactionModel->gettex()->get()->groupby('GST_Type');
                            foreach($taxs as $key=>$tax){

                              if(isset($tratex[$key])){
                                  $getaxss=$tratex[$key]->first();

                                      $str.= ','.$getaxss->Tax_percentage.'%,'.bcadd($getaxss->Tax_Value, 0, 2);
                                 }else{

                                       $str.= ', , ';
                                   }
                              }
                           
                            $str.= ', , , ';
                        
                           

                           
                        
                         $CsvData[]=$str;
                         $i++;
                 }  

                      $str=',,,,,,,,,,,Totals,';

                      foreach($taxs as $key=>$tax){
                        $str.=',,';
                      }

                      $str.=','.bcadd($bill->handling_amount, 0, 2);
                      $str.=','.bcadd($bill->discount, 0, 2);
                      $str.=','.bcadd($bill->rounded_of, 0, 2);
                      $CsvData[]=$str;
              }
               
              $filename=date('Y-m-d')."-sales-register-billed-reports.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
    }
    /**
    * ItemwiseNozzleWiseReadingReport
    * @return \Illuminate\Http\Response
    */
    public function ItemwiseNozzleWiseReadingReport(Request $request){

        if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }
       
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');
          
        

        if($from_date!=null && $to_date!=null){

          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
          $readings=nozzelsReadingModel::where('RO_code',$rocode)->where('Nozzle_End','<>',null)->where('Reading_Date','>=',$from_dates." 00:00:00")->where('Reading_Date','<=',$to_dates." 23:59:59")->get();
           $readings=$readings->groupby('shift_id');
        }

       

        return view('backend.reports.ItemwiseNozzleWiseReadingReport',compact('readings'));
    }

     /**
      * downloadNozzleReadingReport
      * @return \Illuminate\Http\Response
      */
    public function downloadNozzleReadingReport(Request $request){

         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }
       
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');
          
        

        if($from_date!=null && $to_date!=null){

          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
          $readings=nozzelsReadingModel::where('RO_code',$rocode)->where('Nozzle_End','<>',null)->where('Reading_Date','>=',$from_dates." 00:00:00")->where('Reading_Date','<=',$to_dates." 23:59:59")->get();
           $readings=$readings->groupby('shift_id');
        }

                
              $str='S No.,Shift,Item,Tank No.,Totalizer Number,OMR,CMR,Totalizer Out Qty,Totalizer Test Qty,Nett Sales Qty,Credit Sales Qty,Other Sales Qty';
             
              $CsvData=array($str);
                    
                    $i=1;
                     $j=1;
             foreach($readings as $shifts){
                $creditsales=0; $i=0; $natesales=0;
                $GtalizerOutQty=0;
                $GnettSalesQty=0;
                $testsales=0;

                foreach($shifts as $reading){
                  $str=$j;

                  $totalizerOutQty=0;
                  $nettSalesQty=0;
                  $otherSalesQty=0;
                 

                  if($i==0){
                  
                    foreach ($reading->getCreditSales as $getCreditSale) {
                      $creditsales=$getCreditSale->petroldiesel_qty+$creditsales;
                    }
                  
                  }
                  $natesales=($reading->Nozzle_End-$reading->Nozzle_Start)+$natesales;
                  $totalizerOutQty=$reading->Nozzle_End-$reading->Nozzle_Start;
                  $nettSalesQty=$reading->Nozzle_End-$reading->Nozzle_Start-$reading->test;

                  $GtalizerOutQty=$totalizerOutQty+$GtalizerOutQty;
                  $GnettSalesQty=$nettSalesQty+$GnettSalesQty;
                  $testsales=$reading->test+$testsales;

                  if($reading->getShift!=null){

                    $str.=','.date('d/m/Y  h:i:s A',strtotime($reading->getShift->created_at)).'  To  '. date('d/m/Y  h:i:s A',strtotime($reading->getShift->closer_date));
                  }else{
                     $str.=',';
                  }
                 
                  $str.=','.$reading->getNozzle->getItem->Item_Name;
                  $str.=','.$reading->getNozzle->getTank->Tank_Number;
                  $str.=','.$reading->Nozzle_No;
                  $str.=','.$reading->Nozzle_Start;
                  $str.=','.$reading->Nozzle_End;
                  $str.=','.$totalizerOutQty;
                  $str.=','.$reading->test;
                  $str.=','.$nettSalesQty;
                  $str.=', ';
                  $str.=', ';
                  $CsvData[]=$str;
                  $j++;
                 }
                 $str='';
                  $str.=', ';
                  $otherSalesQty=$GnettSalesQty-$creditsales;
                 
                  $str.=', ';
                  $str.=', ';
                  $str.=', ';
                  $str.=', ';
                  $str.=', Totals';
                  $str.=','.$GtalizerOutQty;
                  $str.=','.$testsales;
                  $str.=','.$GnettSalesQty;
                  $str.=','.$creditsales;
                  $str.=','.$otherSalesQty;
                  $CsvData[]=$str;
              }
               
              $filename=date('Y-m-d')."-item-wise-nozzle-wise-reading-report.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
  }
  
  /**
  * partywiseItemwiseDatewiseDeliveriesPending
  * @return \Illuminate\Http\Response
  */
   public function partywiseItemwiseDatewiseDeliveriesPending(Request $request){

        if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }

        $request_type=1;
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');

        if($request->input('request_type')!=null)
           $request_type=$request->input('request_type');

        
        $parties=RoCustomertManagement::where('RO_code',$rocode)->get();
          
        if($from_date!=null && $to_date!=null){
          
          
          
        if($request_type==1)
          $requests=AddPetrolDieselModel::where('RO_code',$rocode)->where('Execution_date',null)->get();
        else
          $requests=CustomerLubeRequest::where('RO_code',$rocode)->where('Execution_date',null)->get();

        if($request->input('party')!=null && trim($request->input('party'))!='')
            $requests=$requests->where('customer_code',$request->input('party'));


          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
          $requests=$requests->where('request_date','>=',$from_dates." 00:00:00")->where('request_date','<=',$to_dates." 23:59:59");
        
        }
       
       
       

        return view('backend.reports.partywiseItemwiseDatewiseDeliveriesPending',compact('request_type','parties','requests'));
    }

   /**
    * downloadPartywiseItemwiseDatewiseDeliveriesPendingReport
    * @return \Illuminate\Http\Response
    */
    public function downloadPartywiseItemwiseDatewiseDeliveriesPendingReport(Request $request){

        if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }


        $request_type=1;
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');

        if($request->input('request_type')!=null)
           $request_type=$request->input('request_type');

        $parties=RoCustomertManagement::where('RO_code',$rocode)->get();
          
        if($request_type==1)
          $requests=AddPetrolDieselModel::where('RO_code',$rocode)->get();
        else
          $requests=CustomerLubeRequest::where('RO_code',$rocode)->get();

        if($request->input('party')!=null && trim($request->input('party'))!='')
            $requests=$requests->where('customer_code',$request->input('party'));
        
          
        if($from_date!=null && $to_date!=null){

          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
          $requests=$requests->where('request_date','>=',$from_dates." 00:00:00")->where('request_date','<=',$to_dates." 23:59:59");
        
        }

                
              $str='Slip Date,Requisition / Slip No.,Party Code,Party Name,Vehicle Number,Product Ordered';

              if($request_type==1)
                $str.=',Request Type';
             
                $str.=',Qty.Order';

              
              $CsvData=array($str);
                    
                    
             foreach($requests as $requ){
             
                  $str=date('d/m/y',strtotime($requ->request_date));
                  $str.=','.$requ->request_id;
                  $str.=','.$requ->customer_code;

                  if($requ->getCustomer!=null)
                    $str.=','.$requ->getCustomer->company_name;
                  else
                     $str.=', ';

                   $str.=','.$requ->Vehicle_Reg_No;

                   if($request_type==1){

                       if($requ->itemname!=null)
                          $str.=','.$requ->itemname->Item_Name;
                        else
                           $str.=', ';

                         $str.=','.$requ->Request_Type;
                         $str.=','.$requ->Request_Value;

                   }else{

                       if($requ->itemname!=null)
                          $str.=','.$requ->itemname->Item_Name;
                        else
                           $str.=', ';

                         $str.=','.$requ->quantity;
                   }
                     
                  $CsvData[]=$str;
              }
               
              $filename=date('Y-m-d')."-item-wise-nozzle-wise-reading-report.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
  }

  /**
  * Partywise - Credit Limits & Status Report
  * @return \Illuminate\Http\Response
  */
  public function partywiseCreditLimitsandStatusReport(){

        if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }
        
        $Credit_limits=Credit_limit::where('tbl_customer_limit.RO_code',$rocode)
        ->join('tbl_customer_master', 'tbl_customer_master.Customer_Code', '=', 'tbl_customer_limit.Customer_Code')
        ->select('tbl_customer_limit.*','tbl_customer_master.Customer_Name','tbl_customer_master.is_active')->get();

          Log::info('data   ======'.print_r($Credit_limits,true));
       
        return view('backend.reports.partywise-Credit-Limits-Report',compact('Credit_limits'));
 
  }

  /**
  * Partywise - Credit Limits & Status Report
  * @return \Illuminate\Http\Response
  */
  public function downloadPartywiseCreditLimitsandStatusReport(){

      if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
      }
      
      $Credit_limits=Credit_limit::where('RO_code',$rocode)->get();


              $str='S.no.,Party Code,Party Name,Status,Last Sale Date,Last Sale Value,Total Sale Value Yr. to Date,Current Unbilled Value,Current O/S Value,Last Bill O/s Date,Credit Limit Rs.,Credit Limit Days,Billing Cycle,Pymt Performance';

              $CsvData=array($str);
              
              $i=1;    
             foreach($Credit_limits as $CreditLimit){
             
                  $str=$i;
                  $str.=','.$CreditLimit->Customer_Code;
                  $str.=','.$CreditLimit->getcustomer->company_name;

                  if($CreditLimit->getcustomer->is_active==1)
                    $str.=',Active';
                  else
                     $str.=',Inactive';


                 if($CreditLimit->last_sale_date!=null){
                   $str.=','.date('d/m/Y',strtotime($CreditLimit->last_sale_date));
                 }else{
                   $str.=', ';
                 }

                 $str.=','.bcadd($CreditLimit->last_sale,0,2);
                 $str.=','.bcadd($CreditLimit->total_sale,0,2);
                 $str.=','.bcadd($CreditLimit->current_unbilled,0,2);
                 $str.=','.bcadd($CreditLimit->available_creadit_limit,0,2);
                
                if($CreditLimit->last_bill_Os_date!=null)
                    $str.=','.date('d/m/Y',strtotime($CreditLimit->last_bill_Os_date));
                else
                    $str.=', ';

                  $str.=','.bcadd($CreditLimit->Limit_Value,0,2);
                  $str.=','.$CreditLimit->no_of_days;
                  $str.=', ';
                  $str.=', ';
                     
                  $CsvData[]=$str;
                  $i++;
              }
               
              $filename=date('Y-m-d')."-Partywise-Credit-Limits-and-Status-Report.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);

  }

       /**
      * salesRegisterBilled
      * @return \Illuminate\Http\Response
      */
    public function partywiseVehiclewiseSalesReport(Request $request){
         
       if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }

      $fule_type=$request->input('fule_type');
      $from_date = $request->input('fromdate');
      $to_date = $request->input('to_date');
      $customer_code = $request->input('customer_code');

       $taxs=tax_master::get()->groupBy('GST_Type');
        if($fule_type==null){
           $fule_type=2;
         }
   
       
      $parties=RoCustomertManagement::where('RO_code',$rocode)->get();

      if($from_date!=null && $to_date!=null){
        
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));


             $transactionModels =TransactionModel::where('RO_code',$rocode)
                              ->where('customer_code',$customer_code)
                              ->whereBetween('trans_date', [date('Y-m-d',strtotime($from_dates))." 00:00:00", date('Y-m-d',strtotime($to_dates))." 23:59:59"])
                              ->orderBy('Vehicle_Reg_No', 'ASC')
                              ->get();
      }


       return view('backend.reports.Partywise-Vehiclewise-Sales-Report',compact('taxs','fule_type','transactionModels','parties'));

    }

    /**
    * salesRegisterBilledDownload
    * @return \Illuminate\Http\Response
    */
    public function partywiseVehiclewiseSalesReportDownload(Request $request){


        if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }

      $fule_type=$request->input('fule_type');
      $from_date = $request->input('fromdate');
      $to_date = $request->input('to_date');
      $customer_code = $request->input('customer_code');

       $taxs=tax_master::get()->groupBy('GST_Type');
        if($fule_type==null){
           $fule_type=2;
         }
   
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));


             $transactionModels =TransactionModel::where('RO_code',$rocode)
                              ->where('customer_code',$customer_code)
                              ->whereBetween('trans_date', [date('Y-m-d',strtotime($from_dates))." 00:00:00", date('Y-m-d',strtotime($to_dates))." 23:59:59"])
                              ->orderBy('Vehicle_Reg_No', 'ASC')
                              ->get();
      

                
              $str='S.No.,Invoice Date,Invoice No.,Party Code,Party Name,Vehicle Regn.No.,Item Sold Name,Item Sold Qty,Item Rate,Item Sold Value';
              $CsvData=array($str);
                    
                    $i=1;
           
                foreach($transactionModels as $TransactionModel){
                            $str=$i;
                            $str.=','.date('d/m/y',strtotime($TransactionModel->trans_date));
                            $str.=','.$TransactionModel->bill_no;
                            $str.=','.$TransactionModel->customer_code;
                            $str.=','.$TransactionModel->getCustomername->company_name;
                            $str.=','.$TransactionModel->Vehicle_Reg_No;
                            $str.=','.$TransactionModel->getItemName->Item_Name;
                            $str.=','.bcadd(round($TransactionModel->petroldiesel_qty,2),0,2);
                            $str.=','.bcadd(round($TransactionModel->item_price,2),0,2);
                            $str.=','.bcadd(round($TransactionModel->item_price*$TransactionModel->petroldiesel_qty,2),0,2);

                         $CsvData[]=$str;
                         $i++;
                

              }
               
              $filename=date('Y-m-d')."-partywise-Vehiclewise-Sales-Report.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");

              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
    }


       /**
      * salesRegisterBilled
      * @return \Illuminate\Http\Response
      */
    public function salesmanWiseSalesReport(Request $request){
         
         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }

      $fule_type=$request->input('fule_type');
      $from_date = $request->input('fromdate');
      $to_date = $request->input('to_date');


       $taxs=tax_master::get()->groupBy('GST_Type');
        if($fule_type==null){
           $fule_type=2;
         }
   
       
      

      if($from_date!=null && $to_date!=null){
        
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));

             $RoPersonalManagements=RoPersonalManagement::with(['getTrangections' => function ($query) use ($from_dates,$to_dates) {

                  $query=$query->whereBetween('trans_date', [date('Y-m-d',strtotime($from_dates))." 00:00:00", date('Y-m-d',strtotime($to_dates))." 23:59:59"]);
           
            }])->where('RO_Code',$rocode)->whereIn('Designation',[29,30])->get();
            
      }


       return view('backend.reports.SalesmanWise-Sales-Report',compact('RoPersonalManagements'));

    }

    /**
    * salesRegisterBilledDownload
    * @return \Illuminate\Http\Response
    */
    public function salesmanWiseSalesReportDownload(Request $request){


        if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }

      $fule_type=$request->input('fule_type');
      $from_date = $request->input('fromdate');
      $to_date = $request->input('to_date');
      $customer_code = $request->input('customer_code');

       $taxs=tax_master::get()->groupBy('GST_Type');
        if($fule_type==null){
           $fule_type=2;
         }
   
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));


            $RoPersonalManagements=RoPersonalManagement::with(['getTrangections' => function ($query) use ($from_dates,$to_dates) {

                  $query=$query->whereBetween('trans_date', [date('Y-m-d',strtotime($from_dates))." 00:00:00", date('Y-m-d',strtotime($to_dates))." 23:59:59"]);
           
            }])->where('RO_Code',$rocode)->whereIn('Designation',[29,30])->get();
      

                
              $str='S.No.,Invoice Date,Invoice No.,Salesman Name,Shift,Party Code,Party Name,Item Sold Name,Item Sold Qty,Item Rate,Item Sold Value';
              $CsvData=array($str);
                    
                    $i=1;
           foreach($RoPersonalManagements as $RoPersonalManagement){
                $amount=0;
                if($RoPersonalManagement->getTrangections->count()>0){
                  
                      foreach($RoPersonalManagement->getTrangections as $TransactionModel){

                              $amount=bcadd(round($TransactionModel->item_price*$TransactionModel->petroldiesel_qty,2),0,2)+$amount;
                                  $str=$i;
                                  $str.=','.date('d/m/y',strtotime($TransactionModel->trans_date));
                                  $str.=','.$TransactionModel->bill_no;
                                  $str.=','.$RoPersonalManagement->Personnel_Name;
                                  
                                  if($TransactionModel->getShift!=null)        
                                    $str.=','.date('d/m/Y  h:i:s A',strtotime($TransactionModel->getShift->created_at)).' To '.date('d/m/Y  h:i:s A',strtotime($TransactionModel->getShift->closer_date));
                                  else
                                    $str.=', ';

                                  $str.=','.$TransactionModel->customer_code;
                                  $str.=','.$TransactionModel->getCustomername->company_name;
                                  $str.=','.$TransactionModel->getItemName->Item_Name;
                                  $str.=','.bcadd(round($TransactionModel->petroldiesel_qty,2),0,2);
                                  $str.=','.bcadd(round($TransactionModel->item_price,2),0,2);
                                  $str.=','.bcadd(round($TransactionModel->item_price*$TransactionModel->petroldiesel_qty,2),0,2);

                               $CsvData[]=$str;
                               $i++;
                      

                    }
                    $str=', , , , , , , ,Total, ,'.$amount;
                    $CsvData[]=$str;
                }
           }
               
              $filename=date('Y-m-d')."-salesman-Wise-Sales-Report.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");

              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
    }

       /**
      * salesRegisterBilled
      * @return \Illuminate\Http\Response
      */
    public function partyDriverVehicleListings(Request $request){
         
        if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
            
        }

        $customer_code=$request->input('customer_code');

        $CustomerDrivers=CustomerDriverManager::where('Ro_code',$rocode);

       if($customer_code!=null && $customer_code!='all')
        $CustomerDrivers=$CustomerDrivers->where('customer_code',$customer_code);

       if(Auth::user()->user_type==4 && $request->session()->has('custm'))
         $CustomerDrivers=$CustomerDrivers->where('customer_code',$request->session()->get('custm'));

        $CustomerDrivers=$CustomerDrivers->get();

        $parties=RoCustomertManagement::where('RO_code',$rocode)->get();
       


       return view('backend.reports.Party-Driver-Vehicle-Listings',compact('CustomerDrivers','parties'));

    }

    /**
    * salesRegisterBilledDownload
    * @return \Illuminate\Http\Response
    */
    public function partyDriverVehicleListingsDownload(Request $request){


        if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }

        $customer_code=$request->input('customer_code');
        $CustomerDrivers=CustomerDriverManager::where('Ro_code',$rocode);

       if($customer_code!=null && $customer_code!='all')
        $CustomerDrivers=$CustomerDrivers->where('customer_code',$customer_code);

      if(Auth::user()->user_type==4 && $request->session()->has('custm'))
         $CustomerDrivers=$CustomerDrivers->where('customer_code',$request->session()->get('custm'));

        $CustomerDrivers=$CustomerDrivers->get();

        $parties=RoCustomertManagement::where('RO_code',$rocode)->get();
                    
            $i=1;

               
                
              $str='S.No.,Party Code,Party Name,Vehicle Regn.No.,Vehicle Make,Vehicle Model,Vehicle Color,Vehicle Fuel,Vehicle Insurance Co.,Vehicle Insurance Due On,Vehicle PUC Due On,Vehicle Regn.Valid upto,Driver Name,Driver Mobile,Driver License Validity';
              $CsvData=array($str);
                    
                    
          foreach($CustomerDrivers as $CustomerDriver){

              $str=$i;
              $str.=','.$CustomerDriver->customer_code;
               if($CustomerDriver->getCustomer!=null)
                  $str.=','.$CustomerDriver->getCustomer->company_name;
                else
                 $str.=',';
               
             
              $str.=','.$CustomerDriver->Registration_Number;

              if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->getmake!=null)
                $str.=','.$CustomerDriver->getVihicle->getmake->name;
              else
                $str.=',';
              
              if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->getmmodel!=null)
                  $str.=','.$CustomerDriver->getVihicle->getmmodel->name;
              else
                 $str.=',';

               if($CustomerDriver->getVihicle!=null)
                $str.=','.$CustomerDriver->getVihicle->van_color;
              else
                 $str.=',';


                if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->getitem!=null)
                 $str.=','.$CustomerDriver->getVihicle->getitem->Item_Name;
                else
                 $str.=',';


                if($CustomerDriver->getVihicle!=null)
                  $str.=','.$CustomerDriver->getVihicle->industry_company;
                else
                 $str.=',';

             

              if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->insurance_due_date!=null)
                $str.=','.date('d/m/Y',strtotime($CustomerDriver->getVihicle->insurance_due_date));
              else
                 $str.=',';

                if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->puc_date!=null)
                $str.=','.date('d/m/Y',strtotime($CustomerDriver->getVihicle->puc_date));
              else
                 $str.=',';

                if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->rc_valide!=null)
                $str.=','.date('d/m/Y',strtotime($CustomerDriver->getVihicle->rc_valide));
              else
                 $str.=',';


              $str.=','.$CustomerDriver->Driver_Name;
              $str.=','.$CustomerDriver->Driver_Mobile;
              if($CustomerDriver->valid_upto!=null)
                $str.=','.date('d/m/Y',strtotime($CustomerDriver->valid_upto));
              else
                 $str.=',';


             $CsvData[]=$str;
             $i++;
          

          }
            
               
           
               
              $filename=date('Y-m-d')."-Party-Driver-Vehicle-Listings.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");

              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
    }

  
  /**
  * PartywiseItemwiseDatewiseDeliveriesPendingforBilling
  * @return \Illuminate\Http\Response
  */
   public function PartywiseItemwiseDatewiseDeliveriesPendingforBilling(Request $request){

        if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }

        $request_type=1;
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');

        if($request->input('request_type')!=null)
           $request_type=$request->input('request_type');

        
        $parties=RoCustomertManagement::where('RO_code',$rocode)->get();
          
        if($from_date!=null && $to_date!=null){
          
       
          $Transactions=TransactionModel::where('RO_code',$rocode)->where('petrol_or_lube',$request_type)->where('status',0)->get();
       
        if($request->input('party')!=null && trim($request->input('party'))!='')
            $Transactions=$Transactions->where('customer_code',$request->input('party'));



          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
          $Transactions=$Transactions->where('trans_date','>=',$from_dates." 00:00:00")->where('trans_date','<=',$to_dates." 23:59:59");
        
        }
       
    
        return view('backend.reports.Partywise-Itemwise-Datewise-Deliveries-Pending-Billing',compact('request_type','parties','Transactions'));
    }

   /**
    * downloadPartywiseItemwiseDatewiseDeliveriesPendingforBillingReport
    * @return \Illuminate\Http\Response
    */
    public function partywiseItemwiseDatewiseDeliveriesPendingforBillingDownload(Request $request){

         if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
        }else if(Auth::user()->user_type==4){

            $rocode=$request->session()->get('ro');
        }

        $request_type=1;
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');

        if($request->input('request_type')!=null)
           $request_type=$request->input('request_type');

        
        $parties=RoCustomertManagement::where('RO_code',$rocode)->get();
          
        if($from_date!=null && $to_date!=null){
          
       
          $Transactions=TransactionModel::where('RO_code',$rocode)->where('petrol_or_lube',$request_type)->where('status',0)->get();
       
        if($request->input('party')!=null && trim($request->input('party'))!='')
            $Transactions=$Transactions->where('customer_code',$request->input('party'));



          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_dates = date('Y-m-d', strtotime($from_dates));
          $to_dates = date('Y-m-d', strtotime($to_dates));
          $Transactions=$Transactions->where('trans_date','>=',$from_dates." 00:00:00")->where('trans_date','<=',$to_dates." 23:59:59");
        
        }
              
              $str='S.No.,Slip Date,Requisition / Slip No,Transaction Type,Party Code,Party Name,Product Ordered,Product Delivered,Qty. Order,Qty Deld.,Rate,Value';

              

              
              $CsvData=array($str);
                    
                  $i=1;  
             foreach($Transactions as $Transaction){
                   $str=$i;
                    $str.=','.date('d/m/Y',strtotime($Transaction->trans_date));
                    $str.=','.$Transaction->Request_id;

                    if($Transaction->trans_mode=='SLIPS')
                      $str.=',SLIPS';
                    else
                      $str.=',GARRUDA';
                    

                     $str.=','.$Transaction->customer_code;
                     $str.=','.$Transaction->getCustomerMaster->company_name;
                    
                    if($Transaction->trans_mode=='SLIPS'){
                       $str.=','.$Transaction->getItemName->Item_Name;
                    }else{
                      
                      if($Transaction->petrol_or_lube==1){

                        if($Transaction->getfuelRequest!=null && $Transaction->getfuelRequest->itemname!=null){
                          $str.=','.$Transaction->getfuelRequest->itemname->Item_Name;
                        }

                      }else{
                        $str.=','.$Transaction->getItemName->Item_Name;
                      }

                    }
                  
                  $str.=','.$Transaction->getItemName->Item_Name;

                  if($Transaction->trans_mode=='SLIPS'){

                      $str.=','.$Transaction->petroldiesel_qty;

                  }else{
                       if($Transaction->petrol_or_lube==1){

                         if($Transaction->getfuelRequest!=null){

                              if($Transaction->getfuelRequest->Request_Type=='Full Tank'){

                                 $str.=','.$Transaction->getfuelRequest->Request_Type;

                              }else{

                                $str.=','.$Transaction->getfuelRequest->Request_Value;

                              }
                              
                         }

                       }else{

                        $str.=','.$Transaction->getlubeRequest()->where('item_id',$Transaction->item_code)->first()->quantity;
                       
                       }
                  }
               

                 $str.=','.$Transaction->petroldiesel_qty;
                 $str.=','.bcadd(round($Transaction->item_price,2),0,2);
                 $str.=','.bcadd(round($Transaction->item_price*$Transaction->petroldiesel_qty,2),0,2);
 
                     
                  $CsvData[]=$str;
                  $i++;
              }
               
              $filename=date('Y-m-d')."-item-wise-nozzle-wise-reading-report.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers )->deleteFileAfterSend(true);
  }

}
