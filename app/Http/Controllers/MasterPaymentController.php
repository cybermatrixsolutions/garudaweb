<?php

namespace App\Http\Controllers;

//use App\CityController;
use Illuminate\Http\Request;
use App\MasterPaymentmode;
use DB;
use App\User;
use Auth;
class MasterPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
        $PaymentModel = MasterPaymentmode::orderby('id','desc')->get();
         //  dd($StateManagementModel);
        return view('backend.masterpaymode',compact('PaymentModel')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create(Request $request)
    {
       try{
        
         
          $paymentmode=$request->input('name');
          $mode_type=$request->input('mode_type');
          $validatedData = $request->validate([
          'name' => 'required|unique:tpl_masterpayment_mode',

         ]);

          $PaymentModel = new MasterPaymentmode();
          $PaymentModel->name =$paymentmode;
          $PaymentModel->mode_type=$mode_type;
          $PaymentModel->save();
          $request->session()->flash('success','Added Successfully!!'); 
         }
         catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeupdate(Request $request , $id)
    {
       try{
          $paytypecreate=$request->input('name');  
          $mode_type=$request->input('mode_type');
          $validatedData = $request->validate([
          'name' => 'required|unique:tpl_masterpayment_mode,id,'.$id,
         ]);

          MasterPaymentmode::where('id', $id)->update(['name' =>$paytypecreate,'mode_type'=>$mode_type]);
          $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
      

    }
	 public function csvdownload()
    {

         $MasterPaymentmodes=MasterPaymentmode::where('IsActive',1)->orderby('id','DSC')->get();
          

           $CsvData=array('Payment Mode Name,Payment Type');  
              //$CsvData=array('Item Name,Price,Effective Date');         

              foreach($MasterPaymentmodes as $value){
                    
                  

                              $CsvData[]=$value->name.','.$value->mode_type;
                     
              }  
              $filename=date('Y-m-d')."-paymentmode.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function show(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function edit(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContributionModel $contributionModel)
    {
        //
    }
}
