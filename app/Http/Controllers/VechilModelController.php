<?php

namespace App\Http\Controllers;

//use App\CityController;
use Illuminate\Http\Request;
use App\VehicleModel;
use DB;
use App\Stock_item_group;
use App\VehicleMakeModel;
class VechilModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          $VehicleModel = VehicleModel::orderBy('id', 'desc')->get();
          $VehicleMakeModel = VehicleMakeModel::orderBy('name', 'asc')->get();
          $StockItemGroup = Stock_item_group::where('Group_Name','FUEL')->first();
        
        return view('backend.vechilemodel',compact('VehicleModel','VehicleMakeModel','StockItemGroup')); 
    }
    //  public function listing(Request $request)
    // {
    //       $VehicleModel = VehicleMakeModel::get();
    //       dd($VehicleModel->getstockitemname);
        
        
    //     return view('backend.VehicleModel',compact('VehicleModels')); 
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create(Request $request)
    {
         
          $vehicle=$request->input('name'); 
          $make_id=$request->input('make_id'); 
          $capacity=$request->input('capacity');
           $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";
          $validatedData = $request->validate([
          'name' => 'required|unique:tpl_vechile_model',
          'capacity' =>array('required','regex:'.$regex),

         ]);
          
          $VehicleModel = new VehicleModel();
          $VehicleModel->name =$vehicle;
          $VehicleModel->make_id =$make_id;
          $VehicleModel->capacity =  $capacity;
          $VehicleModel->fuel_type = $request->input('fuel_type');
          $VehicleModel->save();
          $request->session()->flash('success','Added Successfully!!'); 
          return back();
 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeupdate(Request $request , $id)
    {
        $vehicle=$request->input('name');  
        $make_id=$request->input('first_unit'); 
        $capacity=$request->input('capacity');
        $fuel_type=$request->input('fuel_type');
          $validatedData = $request->validate([
          'name' => 'required|unique:tpl_vechile_model,id,'.$id,
          'capacity' =>'required|between:0,99.99,id,'.$id,
         ]);
          VehicleModel::where('id', $id)->update(['name' =>$vehicle,'make_id' =>$make_id,'capacity'=>$capacity,'fuel_type'=> $fuel_type]);
          $request->session()->flash('success','Data Updated Successfully!!'); 
          return back();
  


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function show(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function edit(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContributionModel $contributionModel)
    {
        //
    }
	 public function csvvehiclemodel()
    {
       $vihiclemodel= VehicleModel::where('IsActive',1)->orderBy('id','DSC')->get();
          

           $CsvData=array('Company Name,Model Name,Fuel Type,Capacity');  
              //$CsvData=array('Item Name,Price,Effective Date');         
             
              foreach($vihiclemodel as $model){
                    
                  //$parentName = ($item->parentName!=null) ? $item->parentName->Group_Name : "";

               
                  $CsvData[]=$model->name.','.$model->getmake->name.','.$model->getstockitemname->Group_Name.','.$model->capacity;
                     
              }

             
               
              $filename=date('Y-m-d')."-Vehiclemodels.csv";
              $file_path=base_path().'/'.$filename;   
              $file = fopen($file_path,"w+");
              foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
              }

              fclose($file);          

              $headers = ['Content-Type' => 'application/csv'];
              return response()->download($file_path,$filename,$headers );
    } 
}
