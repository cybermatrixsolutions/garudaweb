<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use Illuminate\Support\Facades\Mail;
use App\TransactionInvoiceModel;
use Auth;
use App\RoCustomertManagement;
use App\RoPieceListManagement;
use App\TransactionModel;
use App\invoiceCustomer;
use Carbon\Carbon;
use App\MastorRO;
use App\Credit_limit;
use App\ItemServicesModel;
use App\TransTaxModel;
use App\OutletConfigModel;
use App\InvoiceBilling;
use App\RoMaster;
use App\AddPetrolDieselModel;
use App\CustomerLubeRequest;
use Session;
use Log;
use DateTime;

class PdfGenerateController extends Controller
{
/**
* Show the application dashboard.
*
* @return \Illuminate\Http\Response
*/



public function index(Request $request)
{

    $rocode='';
   
    $GSTlastBilledDate ='';
    $VATlastBilledDate ='';

    if (Auth::user()->getRocode!=null) {
        $rocode=Auth::user()->getRocode->RO_code;
        $GSTlastBilledDate=Auth::user()->getRocode->GST_bill_date;
        $VATlastBilledDate=Auth::user()->getRocode->VAT_bill_date;
    }
   Log::info('PdfGenerateController@index input - '.print_r($request->all(),true));

    $tbl_customer_transaction =  DB::table('tbl_customer_master')->where('is_active',1)->where('RO_code',$rocode)->orderBy('company_name', 'asc')->get();
    $Customerinvoices=invoiceCustomer::where('RO_code',$rocode)->get();
    Log::info('PdfGenerateController@index $tbl_customer_transaction count- '.print_r(count($tbl_customer_transaction),true));
    
   return view('backend.invoice.tran.create_invoice',compact('Customerinvoices','tbl_customer_transaction','GSTlastBilledDate','VATlastBilledDate'));
  
}
public  function getCustByDate(Request $request)
{
   Log::info('Ready to get customer input - '.print_r($request->all(),true));
   $input=$request->all();
   $date=$input['date'];
   $fule_type=$input['fule_type'];

        $date=date($date);
        $dateTime = $date . ' 23:59:59';
        Log::info('dateTime ==== '.$dateTime);
         
        $fromDate = DateTime::createFromFormat('d/m/Y  H:i:s', $dateTime)->format('Y-m-d H:i:s');
        Log::info('fromDate   ==== '.print_r($fromDate,true));
         $user=Auth::user();
         $rocode=$user->getRocode->RO_code;
         Log::info('rocode====='.$rocode);
        $tbl_customer_transaction =  DB::table('tbl_customer_master')
        ->join('tbl_customer_transaction','tbl_customer_transaction.customer_code','=','tbl_customer_master.Customer_Code')
       
        ->where('tbl_customer_master.is_active',1)
        ->where('tbl_customer_master.RO_code',$rocode)
         ->where('tbl_customer_transaction.trans_date','<=',$fromDate)
        
         ->where('tbl_customer_transaction.status',0)
         
        ->where('tbl_customer_transaction.petrol_or_lube','=',$fule_type)
        ->where('tbl_customer_transaction.petroldiesel_qty','!=',0)
        ->groupBy('tbl_customer_master.company_name')
        ->orderBy('tbl_customer_master.company_name', 'asc')

        ->get();
 Log::info('PdfGenerateController@getCustByDate $tbl_customer_transaction- '.print_r(count($tbl_customer_transaction),true));
 Log::info('PdfGenerateController@getCustByDate $tbl_customer_transaction- '.print_r($tbl_customer_transaction,true));
        return $tbl_customer_transaction;

}

public  function getVehicleByCustCode(Request $request)
{
   Log::info('Ready to getVehicleByCustCode input - '.print_r($request->all(),true));
   $input=$request->all();
   $to_date=$input['date'];
    $to_dates = new Carbon(str_replace('/', '-',$to_date));
$to_dates = date('Y-m-d', strtotime($to_dates));
 $Vehicle_Reg_No_data = DB::table('tbl_customer_transaction')
 ->where('customer_code',$input['cust_code_id'])
  ->where('status',0)
  ->where('petroldiesel_qty','!=',0)  
  ->where('petrol_or_lube',$input['fule_typ'])
  ->whereDate('trans_date','<=',$to_dates." 23:59:59")
  ->get(['Vehicle_Reg_No']);
return $Vehicle_Reg_No_data;
   



}



public function billsGenerated(Request $request)
{

    $rocode='';
    $customer='';
    Log::info('pdfgenrateControler input - '.print_r($request->all(),true));
    if (Auth::user()->user_type==3 && Auth::user()->getRocode!=null) {

      $rocode=Auth::user()->getRocode->RO_code;

    }else if(Auth::user()->user_type==4){

         $rocode = Session::get('ro');
         $customer=Session::get('custm');
    }

    $Customerinvoices=array();

      $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');
  

        if($from_date!=null && $to_date!=null){
          $from_dates = new Carbon(str_replace('/', '-',$from_date));
          $to_dates = new Carbon(str_replace('/', '-',$to_date));
          $from_date = date('Y-m-d', strtotime($from_dates));
          $to_date= date('Y-m-d', strtotime($to_dates));

           $Customerinvoices=invoiceCustomer::where('RO_code',$rocode)
          ->where('invoice_date','>=',$from_date)->where('invoice_date','<=',$to_date)
         ;

        }


     Log::info('pdfgenrateControler@billsGenerated from_dates '.$from_date. '  to_dates  '.$to_date );

    $tbl_customer_transaction =  DB::table('tbl_customer_master')->where('is_active',1)->where('RO_code',$rocode)->orderBy('company_name', 'asc')->get();

    //Log::info('PdfGenerateController   billgenrate -   customer  - '.print_r($customer,true));
   //Log::info('PdfGenerateController   billgenrate -   Customerinvoices  - '.print_r($Customerinvoices->get(),true));

    if(Auth::user()->user_type==4)
    {
      if(!empty($Customerinvoices)){
        
      $Customerinvoices=$Customerinvoices->where('Customer_name',$customer);
      }
    }

    if($Customerinvoices){

      $Customerinvoices=$Customerinvoices->get(); 
    }

   

    Log::info('pdfgenrateControler@billsGenerated Customerinvoices '.print_r(count($Customerinvoices),true));

   return view('backend.invoice.tran.billsGenerated',compact('Customerinvoices','tbl_customer_transaction'));
  
}


public function pdfview(Request $request)
{
    // $users = DB::table("users")->get();
    // view()->share('users',$users);

    // if($request->has('download')){
    //  // Set extra option
    //  PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
    //  // pass view file
    //     $customPaper = array(0,0,800.00,283.800);
    //     $pdf = PDF::loadView('backend.pdfview',['invoice_no'=>123,'tbl_invoice' =>1230,'transactionModel'=>990])->setPaper($customPaper, 'landscape');
    //   // download pdf
    //   return $pdf->download('pdfview.pdf');
    // }
  return view('backend.pdfview');
}
public function gettransectionasSelectList(Request $request)
{
  Log::info('PdfGenerateController@gettransectionasSelectList input - '.print_r($request->all(),true));


  $input=$request->all();
  $date=$input['to_dae'];
 
  if(($input['current_bill_date']) <$date)
  {
      Log::info('Last Bill Date Less Than Current Bill Date'.print_r($date,true));
      $request->session()->flash('success','Bill Upto Date is Not Greater Than Current Bill Date.');
      return back();
  }

  //$dbname=DB::connection()->getDatabaseName();
  //Log::info('$dbName   ======= '.print_r($dbname,true));
  else{
   Log::info('Current Bill Date Greater Than Last Bill Date='.print_r($date,true));
        $rocode='';
        

        if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;

        }

        $amount=0;

        $customer_code=$request->input('RO_code');
        //$tbl_customer_transaction =  DB::table('tbl_customer_master')->where('RO_code',$rocode)->get();
        $from_date = $request->input('fromdate'); 
        $current_bill_date=$request->input('current_bill_date');

        if($current_bill_date==null){

          $request->session()->flash('success','Current bill date can not be empty');
          return back();
        }

        $to_date = $request->input('to_dae'); 
        $fule_type=$request->input('fule_type');
        //$from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));

        
        $current_bill_date = new Carbon(str_replace('/', '-',$current_bill_date));
        
        //$from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));
        $current_bill_date = date('Y-m-d', strtotime($current_bill_date));
        
        $Credit_limit=Credit_limit::where('Customer_Code',$customer_code)->first();

        $transactionModels = TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
        ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')

        ->where('tbl_customer_transaction.customer_code',$customer_code)
        ->where('tbl_customer_transaction.RO_code',$rocode)
        ->where('tbl_customer_transaction.status',0)
        ->where('tbl_customer_transaction.petroldiesel_qty','!=',0)
        
        ->where('tbl_customer_transaction.petrol_or_lube',$fule_type)
        ->whereDate('tbl_customer_transaction.trans_date','<=',$to_dates." 23:59:59")
        ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode')
        ->get();
        if(!empty($request->input('vehicle_no'))){
          $transactionModels = TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
        ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')

        ->where('tbl_customer_transaction.customer_code',$customer_code)
        ->where('tbl_customer_transaction.RO_code',$rocode)
        ->where('tbl_customer_transaction.status',0)
        ->where('tbl_customer_transaction.petroldiesel_qty','!=',0)
        ->where('tbl_customer_transaction.petrol_or_lube',$fule_type)
        
        ->where('tbl_customer_transaction.Vehicle_Reg_No',$request->input('vehicle_no'))
        ->whereDate('tbl_customer_transaction.trans_date','<=',$to_dates." 23:59:59")
        ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode')
        ->get();
        }
      // dd($fule_type,$transactionModels);
        return View('backend.invoice.tran.transactionsList',compact('transactionModels','Credit_limit','from_date','to_date','fule_type','current_bill_date'));

      }
}
public function gettransection(Request $request)
{  
$input=$request->all();
$pdftype=$input['pdf'];

    if($pdftype=="Preview") {

     $pdfview= $this->gettransectionPreview($request);
      return $pdfview;
        
    }else{
   $pdfdata= $this->gettransectionGo($request);
 
   
     return $pdfdata;
    }
    
}

public function gettransectionPreview(Request $request)
{   
    
Log::info('PdfGenerateController@gettransectionPreview 1 ready to make pdf! input - '.print_r($request->all(),true));
 
$mainInput=$request->all();
    $amount = 100; // the amount of ids
    $previousValues = array();
    if (date('m') <= 3) {
          $year = date('y');
    } else {
          $year = date('y')+1;
    }
    DB::beginTransaction();
    
  try{  

        $invoice_no='';
        $fule_type=1;
         $cus_id=$request->input('cus_id');
        $fule_type=$request->input('fule_type');
        $from_dates=$request->input('from_dates');
        $to_dates=$request->input('to_dates');
        $current_bill_date=$request->input('current_bill_date');
        $order_by=1;
        $orderBy='invoice_number';
        $rocode='';
        $nom=5;
        $l=0;
        $invoice_prefix='';

        if (Auth::user()->getRocode!=null) {

           $rocode=Auth::user()->getRocode->RO_code;
           $GST_invoice_no=Auth::user()->getRocode->GST_serial;
           $VAT_invoice_no=Auth::user()->getRocode->VAT_serial;
            $GST_invoice_inc_no=$GST_invoice_no;
           $VAT_invoice_inc_no=$VAT_invoice_no;
           Log::info('GST_invoice_no = '.$GST_invoice_no);
           Log::info('VAT_invoice_no = '.$VAT_invoice_no);


          if($fule_type==2){

            $billDateUpdate=['GST_bill_date'=>$current_bill_date];

            $GST_invoice_inc_no=$GST_invoice_no+1;
            $invoice_no=$GST_invoice_no;
            $invoice_prefix=Auth::user()->getRocode->GST_prefix; 

             if(Auth::user()->getRocode->getdetails!=null)
                $nom=Auth::user()->getRocode->getdetails->GST_serial_lenght;

          }else{

            $billDateUpdate=['VAT_bill_date'=>$current_bill_date];

            $VAT_invoice_inc_no=$VAT_invoice_no+1;
            $invoice_no=$VAT_invoice_no;
            $invoice_prefix=Auth::user()->getRocode->VAT_prefix;

            if(Auth::user()->getRocode->getdetails!=null)
              $nom=Auth::user()->getRocode->getdetails->VAT_serial_lenght;
          }

        }

        //Log::info('$nom  getRocode->getdetails->GST_serial_lenght   from rodetails == '.$nom);

        if($fule_type==1 && count($cus_id)>0){
           
              $transactionModels = TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
                                    ->join('tbl_item_price_list', 'tbl_price_list_master.id', '=', 'tbl_item_price_list.item_id')
                                    ->where('tbl_customer_transaction.status',0)
                                      ->where('tbl_customer_transaction.RO_code',$rocode)
                                    ->where('tbl_customer_transaction.petrol_or_lube',$fule_type)
                                    ->whereIn('tbl_customer_transaction.id',$cus_id)
                                    ->where('tbl_item_price_list.effective_date','>=',date('Y-m-d H:m:s',strtotime($current_bill_date)))
                                    ->where('tbl_item_price_list.effective_date','<=',date('Y-m-d H:m:s',strtotime($current_bill_date)+86400))
                                    ->get();
// dd($transactionModels, $cus_id, $current_bill_date, $fule_type);
            if($transactionModels->count()<=0){
                $request->session()->flash('success','Please insert price on '.date('d/m/Y',strtotime($current_bill_date)));
               return redirect('invoiceindex');
            }
        }

     

        $RoCon=OutletConfigModel::where('rocode',$rocode)->where('field_name','with_commission')->first();

        $l=mb_strlen($invoice_no);

 
        $nom=$nom-$l;

        for ($i=1; $i <=$nom ; $i++) { 
            $invoice_no='0'.$invoice_no;
        }
         $now=date('m-d-');

        $invoice_no=$invoice_prefix.'-'.$invoice_no.'-'.$year;
      /* $invoice_no=$rocode.'-'.$invoice_prefix.'-'.$invoice_no.'-'.$now.$year;*/
        //Log::info('$invoice_no  after add prefix === = '.$invoice_no);
        $amount=0;
        $customer_code=$request->input('RO_code');
        $tbl_customer_transaction =  DB::table('tbl_customer_master')->where('RO_code',$rocode)->get();
       

        if(count($cus_id)<= null)
          return back();

        $updatediscount=$request->input('Discount');
        $handling_fee=0;
        $handling_amount=0;
        //$Credit_limit=Credit_limit::where('Customer_Code',$customer_code)-->first();
        $sevice=null;

        if($RoCon!=null && $RoCon->value=='on')
          $sevice=ItemServicesModel::where('RO_code',$rocode)->where('tax_type','GST')->first();
        
        if($updatediscount!=null){

        if($fule_type==1)
          Credit_limit::where('Customer_Code',$customer_code)->update(['Discount' => $updatediscount]);
        else
          Credit_limit::where('Customer_Code',$customer_code)->update(['Discountlube' => $updatediscount]);

        }else{

            if($fule_type==1)
              Credit_limit::where('Customer_Code',$customer_code)->update(['Discount' => 0]);
            else
              Credit_limit::where('Customer_Code',$customer_code)->update(['Discountlube' => 0]);


             $updatediscount=0;
        }

        $credit_limit=Credit_limit::where('Ro_code',$rocode)->where('Customer_Code',$customer_code)->latest()->first();
        
        if($credit_limit!=null){
          
           if($fule_type==1)
              $handling_fee=$credit_limit->handling_fee;
            else
              $handling_fee=$credit_limit->handlinglubefee;
            

              $order_by=$credit_limit->store_billing_mode;
        }
        //update handling fee for preview


       if(isset($mainInput['preview-billing-charges'])){

          $handling_fee=$mainInput['preview-billing-charges'];
          Log::info('$handling_fee  in preview-billing-charges = '.$handling_fee);
        }

       
        
        if($order_by==1)
          $orderBy='invoice_number';
        else
          $orderBy='Vehicle_Reg_No';


        Log::info('$orderBy ===== '.$orderBy);
        $transactionModels = TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
        ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
        ->where('tbl_customer_transaction.customer_code',$customer_code)
        ->where('tbl_customer_transaction.status',0)
        ->where('tbl_customer_transaction.petrol_or_lube',$fule_type)
        ->whereIn('tbl_customer_transaction.id',$cus_id)
       /* ->orderBy($orderBy, 'asc')*/
         /*->orderBy('tbl_customer_transaction.trans_date', 'asc')*/
         ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode');
        //->get();

         Log::info('PdfGenerateController@pdfview  OrderBy - '.$orderBy);
        if($orderBy=='invoice_number'){
          
          $transactionModels->orderBy('tbl_customer_transaction.trans_date', 'asc');
           $transactionModels->orderBy($orderBy, 'asc');

        }
         if($orderBy=='Vehicle_Reg_No'){
          
          $transactionModels->orderBy($orderBy, 'asc');
          $transactionModels->orderBy('tbl_customer_transaction.trans_date', 'asc');

        }

        $transactionModels=$transactionModels->get();
      
        $tragection=$transactionModels->pluck('id')->toArray();
        $taxmodels=TransTaxModel::whereIn('transaction_id',$tragection)->orderBy('transaction_id', 'asc')->get();
        
        $taxmodels=$taxmodels->groupBy('tax_name');

        if($transactionModels->count()<=0){

           $request->session()->flash('success','Something is wrong');
           return back();
        }

        $transactionss=$transactionModels;
        //$transactionss=$transactionss->groupBy($orderBy);

        //Log::info('PdfGenerateController@gettransection transactionss'.print_r($transactionss,true));
        $discountPer=0;
        $paybalAmount=0;
        $discount=0;
        $discountPer=$updatediscount;
        foreach ($transactionModels as $transactionModel) {
            //dd($transactionModel->item_price);
            if($fule_type==1){
              $amount=$amount+($transactionModel->item_price*$transactionModel->petroldiesel_qty);
            }else{
              $amount=$amount+$transactionModel->item_price*$transactionModel->petroldiesel_qty;
            }
        }
        
        if($discountPer!=0)
          $discount=($amount*$discountPer)/100;

        $paybalAmount=$amount-$discount;
        $transactionMs=$transactionModels->groupBy('invoice_number');
        $transactionvihicles= $transactionModels->groupBy('Vehicle_Reg_No');
        //Log::info('transactionvihicles =  '.print_r($transactionvihicles,true));  
        $transactionModels= $transactionModels->groupBy('Item_Name');
        $tbl_invoice  =  RoCustomertManagement::where('Customer_Code',$customer_code)->first();
        
        
        
             
        $from_date = new Carbon(str_replace('/', '-',$from_dates));
        $from_date =date('Y-m-d',strtotime($from_date));
        $to_date = new Carbon(str_replace('/', '-',$to_dates));
        $to_date =date('Y-m-d',strtotime($to_date));

      /* $InvoiceBilling= new InvoiceBilling();
        $InvoiceBilling->invoice_no=$invoice_no;
        $InvoiceBilling->customer_code=$customer_code;
        $InvoiceBilling->ro_code=$rocode;
        $InvoiceBilling->billing_from=$from_date;
        $InvoiceBilling->billing_to=$to_date;
        $InvoiceBilling->taxable_amt=round($amount,2);
        $InvoiceBilling->type=$fule_type;
        $InvoiceBilling->handling_fee=$discountPer;
        $InvoiceBilling->discount=$discount;
        $InvoiceBilling->save();
        $InvoiceBillingId=$InvoiceBilling->id;*/

        $InvoiceBillingId="";
       // TransactionModel::whereIn('id',$cus_id)->update(['status'=>1,'bill_no'=>$invoice_no]);

        //update bill date in ro 
        /*RoMaster::where('RO_code',$rocode)->update($billDateUpdate);
        
         
        $invoiceCustomer=new invoiceCustomer();
        $invoiceCustomer->Customer_name=$customer_code;
        $invoiceCustomer->invoice_path='invoice/'.$invoice_no;
        $invoiceCustomer->invoice_no=$invoice_no;
        // $invoiceCustomer->email_to_customer=$request->input('email_to_customer');
        $invoiceCustomer->Ro_code=$rocode;
        $invoiceCustomer->status=1;
        $invoiceCustomer->tax_type=$fule_type;
        $invoiceCustomer->invoice_date=$current_bill_date;
        $invoiceCustomer->amount=round($paybalAmount,2);
        $invoiceCustomer->total_amount=round($amount,2);
       
          
        $invoiceCustomer->save();*/
         // Log::info('$handling_amount  before == '.$handling_amount);
        if($RoCon!=null && $RoCon->value=='on' && $sevice!=null)
         $handling_amount=round($amount,2)*$handling_fee/100;
       //Log::info('$handling_amount  after == '.$handling_amount);
       Log::info('$handling_fee == '.$handling_fee);
       //Log::info('invoiceCustomer total_amount == '.$invoiceCustomer->total_amount);
        
        //update invoce no
       /* MastorRO::where('RO_code',$rocode)->update(['GST_invoice_no' => $GST_invoice_no,'VAT_invoice_no'=>$VAT_invoice_no]);*/
       /* MastorRO::where('RO_code',$rocode)->update(['GST_serial' => $GST_invoice_inc_no,'VAT_serial'=>$VAT_invoice_inc_no]);*/
        $customPaper = array(0,0,595,);

        //get ro image url
        $roImageUrl=DB::table('tbl_ro_master')->where('RO_code','=',$rocode)->value('image');
        Log::info('$roImageUrl == '.$roImageUrl);
        Log::info('$invoice_no  final invoice  === = '.$invoice_no);
       // Log::info('sevice ====== '.print_r($sevice,true));
        Log::info('$discount == '.$discount);
        //update discount for preview
        if(isset($mainInput['preview-discount'])){

          $discount=$mainInput['preview-discount'];
          Log::info('$discount  in preview-discount= '.$discount);
          $discountPer="";
          Log::info('$discountPer after give manual discount - '.$discountPer);
        }
         Log::info('mainInput ====== '.print_r($mainInput,true));
        
        if($fule_type==1){
          Log::info('ready to load   vat pdf view');
          $pdfview= View('backend.invoice.vatview',compact('current_bill_date','InvoiceBillingId','invoice_no','tbl_invoice','transactionss','transactionModels','transactionMs','transactionvihicles','discount','handling_amount','sevice','from_dates','to_dates','taxmodels','handling_fee','discountPer','roImageUrl','mainInput'));
        }
        else
        $pdfview=View('backend.invoice.gstview',compact('current_bill_date','InvoiceBillingId','invoice_no','tbl_invoice','transactionss','transactionModels','transactionMs','transactionvihicles','discount','handling_amount','sevice','from_dates','to_dates','taxmodels','handling_fee','discountPer','roImageUrl','mainInput'));
     
return $pdfview;
   //DB::commit();
   
      
        //return $pdf->download($invoice_no.'.pdf');
  }
   catch(\Illuminate\Database\QueryException $e){
            //DB::rollback();
            $request->session()->flash('success','Something is wrong');
            
    } 


    return back();

}
public function gettransectionGo(Request $request)
{   
    
Log::info('PdfGenerateController@gettransectionGo ready to make Generate Bill! input - '.print_r($request->all(),true));

$mainInput=$request->all();
    $amount = 100; // the amount of ids
    $previousValues = array();
    if (date('m') <= 3) {
          $year = date('y');
    } else {
          $year = date('y')+1;
    }
    DB::beginTransaction();
    
  try{  

        $invoice_no='';
        $fule_type=1;
         $cus_id=$request->input('cus_id');
        $fule_type=$request->input('fule_type');
        $from_dates=$request->input('from_dates');
        $to_dates=$request->input('to_dates');
        $current_bill_date=$request->input('current_bill_date');
        $order_by=1;
        $orderBy='invoice_number';
        $rocode='';
        $nom=5;
        $l=0;
        $invoice_prefix='';

        if (Auth::user()->getRocode!=null) {

           $rocode=Auth::user()->getRocode->RO_code;
           $GST_invoice_no=Auth::user()->getRocode->GST_serial;
           $VAT_invoice_no=Auth::user()->getRocode->VAT_serial;
            $GST_invoice_inc_no=$GST_invoice_no;
           $VAT_invoice_inc_no=$VAT_invoice_no;
           Log::info('GST_invoice_no = '.$GST_invoice_no);
           Log::info('VAT_invoice_no = '.$VAT_invoice_no);


          if($fule_type==2){

            $billDateUpdate=['GST_bill_date'=>$current_bill_date];

            $GST_invoice_inc_no=$GST_invoice_no+1;
            $invoice_no=$GST_invoice_no;
            $invoice_prefix=Auth::user()->getRocode->GST_prefix; 

             if(Auth::user()->getRocode->getdetails!=null)
                $nom=Auth::user()->getRocode->getdetails->GST_serial_lenght;

          }else{

            $billDateUpdate=['VAT_bill_date'=>$current_bill_date];

            $VAT_invoice_inc_no=$VAT_invoice_no+1;
            $invoice_no=$VAT_invoice_no;
            $invoice_prefix=Auth::user()->getRocode->VAT_prefix;

            if(Auth::user()->getRocode->getdetails!=null)
              $nom=Auth::user()->getRocode->getdetails->VAT_serial_lenght;
          }

        }

        //Log::info('$nom  getRocode->getdetails->GST_serial_lenght   from rodetails == '.$nom);

        if($fule_type==1 && count($cus_id)>0){
           
              $transactionModels = TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
                                    ->join('tbl_item_price_list', 'tbl_price_list_master.id', '=', 'tbl_item_price_list.item_id')
                                    ->where('tbl_customer_transaction.status',0)
                                    ->where('tbl_customer_transaction.petrol_or_lube',$fule_type)
                                    ->whereIn('tbl_customer_transaction.id',$cus_id)
                                    ->where('tbl_item_price_list.effective_date','>=',date('Y-m-d H:m:s',strtotime($current_bill_date)))
                                    ->where('tbl_item_price_list.effective_date','<=',date('Y-m-d H:m:s',strtotime($current_bill_date)+86400))
                                    ->get();
// dd($transactionModels, $cus_id, $current_bill_date, $fule_type);
            if($transactionModels->count()<=0){
                $request->session()->flash('success','Please insert price on '.date('d/m/Y',strtotime($current_bill_date)));
               return redirect('invoiceindex');
            }
        }

     

        $RoCon=OutletConfigModel::where('rocode',$rocode)->where('field_name','with_commission')->first();

        $l=mb_strlen($invoice_no);

 
        $nom=$nom-$l;

        for ($i=1; $i <=$nom ; $i++) { 
            $invoice_no='0'.$invoice_no;
        }
        $now=date('m-d-');
        $invoice_name=$rocode.'-'.$invoice_prefix.'-'.$invoice_no.'-'.$now.$year;
        $invoice_no=$invoice_prefix.'-'.$invoice_no.'-'.$year;

         
        //Log::info('$invoice_no  after add prefix === = '.$invoice_no);
        $amount=0;
        $customer_code=$request->input('RO_code');
        $tbl_customer_transaction =  DB::table('tbl_customer_master')->where('RO_code',$rocode)->get();
       

        if(count($cus_id)<= null)
          return back();

        $updatediscount=$request->input('Discount');
        $handling_fee=0;
        $handling_amount=0;
        //$Credit_limit=Credit_limit::where('Customer_Code',$customer_code)-->first();
        $sevice=null;

        if($RoCon!=null && $RoCon->value=='on')
          $sevice=ItemServicesModel::where('RO_code',$rocode)->where('tax_type','GST')->first();
        
        if($updatediscount!=null){

        if($fule_type==1)
          Credit_limit::where('Customer_Code',$customer_code)->update(['Discount' => $updatediscount]);
        else
          Credit_limit::where('Customer_Code',$customer_code)->update(['Discountlube' => $updatediscount]);

        }else{

            if($fule_type==1)
              Credit_limit::where('Customer_Code',$customer_code)->update(['Discount' => 0]);
            else
              Credit_limit::where('Customer_Code',$customer_code)->update(['Discountlube' => 0]);


             $updatediscount=0;
        }

        $credit_limit=Credit_limit::where('Ro_code',$rocode)->where('Customer_Code',$customer_code)->latest()->first();
        
        if($credit_limit!=null){
          
           if($fule_type==1)
              $handling_fee=$credit_limit->handling_fee;
            else
              $handling_fee=$credit_limit->handlinglubefee;
            

              $order_by=$credit_limit->store_billing_mode;
        }

        //update handling fee for preview


       if(isset($mainInput['preview-billing-charges'])){

          $handling_fee=$mainInput['preview-billing-charges'];
          Log::info('$handling_fee  in preview-billing-charges = '.$handling_fee);
        }
    
       
        
        if($order_by==1)
          $orderBy='invoice_number';
        else
          $orderBy='Vehicle_Reg_No';

        $transactionModels = TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
        ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
        ->where('tbl_customer_transaction.customer_code',$customer_code)
        ->where('tbl_customer_transaction.status',0)
        ->where('tbl_customer_transaction.petrol_or_lube',$fule_type)
        ->whereIn('tbl_customer_transaction.id',$cus_id)
      /*  ->orderBy($orderBy, 'asc')
         ->orderBy('tbl_customer_transaction.trans_date', 'asc')*/
         ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode');
      

          Log::info('PdfGenerateController@pdfGo OrderBy - '.$orderBy);
        if($orderBy=='invoice_number'){
          
          $transactionModels->orderBy('tbl_customer_transaction.trans_date', 'asc');
           $transactionModels->orderBy($orderBy, 'asc');

        }
         if($orderBy=='Vehicle_Reg_No'){
          
          $transactionModels->orderBy($orderBy, 'asc');
          $transactionModels->orderBy('tbl_customer_transaction.trans_date', 'asc');

        }

        $transactionModels=$transactionModels->get();
      
        $tragection=$transactionModels->pluck('id')->toArray();
        $taxmodels=TransTaxModel::whereIn('transaction_id',$tragection)->orderBy('transaction_id', 'asc')->get();
        
        $taxmodels=$taxmodels->groupBy('tax_name');

        if($transactionModels->count()<=0){

           $request->session()->flash('success','Something is wrong');
           return back();
        }

        $transactionss=$transactionModels;
        //$transactionss=$transactionss->groupBy($orderBy);

        //Log::info('PdfGenerateController@gettransection transactionss'.print_r($transactionss,true));
        $discountPer=0;
        $paybalAmount=0;
        $discount=0;
        $discountPer=$updatediscount;
        foreach ($transactionModels as $transactionModel) {
            //dd($transactionModel->item_price);
            if($fule_type==1){
              $amount=$amount+($transactionModel->item_price*$transactionModel->petroldiesel_qty);
            }else{
              $amount=$amount+$transactionModel->item_price*$transactionModel->petroldiesel_qty;
            }
        }
        
        if($discountPer!=0)
          $discount=($amount*$discountPer)/100;
$tranType="normal";
       Log::info('$discountPer  - '.$discountPer);
       Log::info('$discount == '.$discount);
        //update discount for preview
        if(isset($mainInput['preview-discount'])){

          $discount=$mainInput['preview-discount'];
          Log::info('$discount  in preview-discount= '.$discount);
          $discountPer="";
          Log::info('$discountPer after give manual discount - '.$discountPer);
          $tranType="preview";
        }

        $paybalAmount=$amount-$discount;
        $transactionMs=$transactionModels->groupBy('invoice_number');
        $transactionvihicles= $transactionModels->groupBy('Vehicle_Reg_No');
        //Log::info('transactionvihicles =  '.print_r($transactionvihicles,true));  
        $transactionModels= $transactionModels->groupBy('Item_Name');
        $tbl_invoice  =  RoCustomertManagement::where('Customer_Code',$customer_code)->first();
           
        $from_date = new Carbon(str_replace('/', '-',$from_dates));
        $from_date =date('Y-m-d',strtotime($from_date));
        $to_date = new Carbon(str_replace('/', '-',$to_dates));
        $to_date =date('Y-m-d',strtotime($to_date));

        $InvoiceBilling= new InvoiceBilling();
        $InvoiceBilling->invoice_no=$invoice_no;
        $InvoiceBilling->customer_code=$customer_code;
        $InvoiceBilling->ro_code=$rocode;
        $InvoiceBilling->billing_from=$from_date;
        $InvoiceBilling->billing_to=$to_date;
        $InvoiceBilling->taxable_amt=round($amount,2);
        $InvoiceBilling->type=$fule_type;
        $InvoiceBilling->handling_fee=$handling_fee;
        $InvoiceBilling->discount=$discount;
        $InvoiceBilling->save();
        $InvoiceBillingId=$InvoiceBilling->id;
        TransactionModel::whereIn('id',$cus_id)->update(['status'=>1,'bill_no'=>$invoice_no]);

        //update bill date in ro 
        RoMaster::where('RO_code',$rocode)->update($billDateUpdate);
        
         
        $invoiceCustomer=new invoiceCustomer();
        $invoiceCustomer->Customer_name=$customer_code;
        $invoiceCustomer->invoice_path='invoice/'.$invoice_name;
        $invoiceCustomer->invoice_no=$invoice_no;
        // $invoiceCustomer->email_to_customer=$request->input('email_to_customer');
        $invoiceCustomer->Ro_code=$rocode;
        $invoiceCustomer->status=1;
        $invoiceCustomer->tax_type=$fule_type;
        $invoiceCustomer->invoice_date=$current_bill_date;
        $invoiceCustomer->amount=round($paybalAmount,2);
        $invoiceCustomer->total_amount=round($amount,2);
       
          
        $invoiceCustomer->save();
         // Log::info('$handling_amount  before == '.$handling_amount);
        if($RoCon!=null && $RoCon->value=='on' && $sevice!=null)
         $handling_amount=$invoiceCustomer->total_amount*$handling_fee/100;
       //Log::info('$handling_amount  after == '.$handling_amount);
       Log::info('$handling_fee == '.$handling_fee);
       Log::info('invoiceCustomer total_amount == '.$invoiceCustomer->total_amount);
        Log::info('$handling_fee == '.$handling_fee);
       
        //update invoce no
       /* MastorRO::where('RO_code',$rocode)->update(['GST_invoice_no' => $GST_invoice_no,'VAT_invoice_no'=>$VAT_invoice_no]);*/
        MastorRO::where('RO_code',$rocode)->update(['GST_serial' => $GST_invoice_inc_no,'VAT_serial'=>$VAT_invoice_inc_no]);
        $customPaper = array(0,0,595,);

        //get ro image url
        $roImageUrl=DB::table('tbl_ro_master')->where('RO_code','=',$rocode)->value('image');
        Log::info('$roImageUrl == '.$roImageUrl.'     rocode  - '.$rocode);
      Log::info('$invoice_no  final invoice  === = '.$invoice_no);
       Log::info('$invoice_no  final discountPer  === = '.$discountPer);
        Log::info('$discountPer  - '.$discountPer);
       Log::info('$discount == '.$discount);
      //Log::info('sevice ====== '.print_r($sevice,true));
        if($fule_type==1)
          $pdf = PDF::loadView('backend.invoice.vat1',compact('current_bill_date','InvoiceBillingId','invoice_no','tbl_invoice','transactionss','transactionModels','transactionMs','transactionvihicles','discount','handling_amount','sevice','from_dates','to_dates','taxmodels','handling_fee','discountPer','roImageUrl','rocode'))->save(storage_path().'/invoice/'.$invoice_name.'.pdf');
        else
        $pdf = PDF::loadView('backend.invoice.gst',compact('current_bill_date','InvoiceBillingId','invoice_no','tbl_invoice','transactionss','transactionModels','transactionMs','transactionvihicles','discount','handling_amount','sevice','from_dates','to_dates','taxmodels','handling_fee','discountPer','roImageUrl','rocode'))->save(storage_path().'/invoice/'.$invoice_name.'.pdf');
     

   DB::commit();
        return response()->download(storage_path().'/invoice/'.$invoice_name.'.pdf');
        
        //return $pdf->download($invoice_no.'.pdf');
  }
   catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            $request->session()->flash('success','Something is wrong');
            
    } 


    return back();

}

public function downloadpdf(Request $request, $id)
{   //dd($request->all());
  
   return response()->download(storage_path().'/invoice/'.$id.'.pdf');
  }
 


public function mailToCustomer(Request $request, $id)
{
  //dd($id);

  Log::info('PdfGenerateController@mailToCustome  input - '.print_r($request->all(),true));
  $customer_invoice_mail=invoiceCustomer::findOrFail($id);
  Log::info('  customer_invoice_mail  ==='.$customer_invoice_mail->Customer_name.'   '.$customer_invoice_mail->invoice_no);

  $customer=RoCustomertManagement::join('tbl_ro_master','tbl_ro_master.RO_code','=','tbl_customer_master.RO_code')
  ->join('cities','cities.id','=','tbl_ro_master.city')
 
  ->where('tbl_customer_master.Customer_Code',$customer_invoice_mail->Customer_name)
  ->select('tbl_customer_master.Email','tbl_customer_master.company_name',
         'tbl_ro_master.pump_legal_name','cities.name as cityName')
  ->get();
  $customer=$customer->toArray();
  Log::info('PdfGenerateController@mailToCustome  customer - '.print_r($customer,true));
 


  Log::info('customer array'.print_r($customer,true));
  
  //Log::info('  $customer_invoice_mail->invoice_no'.$customer_invoice_mail->invoice_no);

  $billingDate=DB::table('invoice_billing')
  ->where('invoice_no',$customer_invoice_mail->invoice_no)
   ->where('customer_code',$customer_invoice_mail->Customer_name)
   ->value('billing_to');
    Log::info('  billingDate  ==='.$billingDate);

    $data=array();
 if($customer){
      $data = array(
        'company_name'=>$customer['0']['company_name'], 
        'pump_legal_name'=>$customer['0']['pump_legal_name'],
        'city_name'=>$customer['0']['cityName'],
        'date'=>$billingDate
      );
}else{
  $request->session()->flash('success','Customer detail not found !');
   return back();
}
 Log::info('customer data'.print_r($data,true));


  //dd($customer_invoice_mail);

  $file = storage_path().'/'.$customer_invoice_mail->invoice_path.'.pdf';

  $invoicename = str_replace('invoice/', '', $customer_invoice_mail->invoice_path) ;
  $invoicename=$invoicename.'.pdf';

 Log::info(' $file  =====   '.print_r($file,true));
 Log::info(' $invoicename  ====='.print_r($invoicename,true));


 Mail::send('mail.billing',$data, function ($message) use ($invoicename,$file,$billingDate,$customer) {

 $message->to($customer['0']['Email'])
        ->subject('Bill detail up to '.$billingDate)
       //->cc('princekumar14763@gmail.com')
       // ->replyTo($request->email)
        ->attach($file , [
            'as' => $invoicename, 
            'mime' => 'application/pdf'
        ]);
      });
 $request->session()->flash('success','Mail sent successfully');
   return back();
            
}


public function invoicePendingCollection()
{
  $rocode='';
  if (Auth::user()->getRocode!=null) {
    $rocode=Auth::user()->getRocode->RO_code;
  }
  $type=0;
  $customer='';
  $tbl_customer_transaction = RoCustomertManagement::where('is_active',1)->where('RO_code',$rocode)->get();
  if(count($tbl_customer_transaction)==null){
   return back();
  }

  $cus=$tbl_customer_transaction[0]->Customer_Code;
  $Customerinvoices=invoiceCustomer::where('RO_code',$rocode)
  ->where('Customer_name',$cus)
  ->where('settlement_status',0)->get();

  $customer=$cus;
  $amountTrajection=0;
  $Credit_limit=Credit_limit::where('Customer_Code',$cus)->where('RO_code',$rocode)->first();
  $transactionModels =TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
    ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
    ->where('tbl_customer_transaction.RO_code',$rocode)
    ->where('tbl_customer_transaction.customer_code',$cus)
    ->where('tbl_customer_transaction.status','<>',2)
     ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode')
    ->get();

   
    foreach ($transactionModels as $transactionModel) {
        //dd($transactionModel->item_price);
        if($transactionModel->petrol_or_lube==1){
          $amountTrajection=$amountTrajection+($transactionModel->item_price*$transactionModel->petroldiesel_qty);
        }else{
          $amountTrajection=$amountTrajection+$transactionModel->item_price;
        }
       
    }

    $pemdingFuelRes=AddPetrolDieselModel::where('customer_code',$cus)->where('RO_code',$rocode)->where('Execution_date',null)->get();
    $PendingFuelREAmount=0;
    $PendingLubeREAmount=0;

    foreach ($pemdingFuelRes as $pemdingFuelRe) {
        
        if($pemdingFuelRe->priceLists!=null){
          
            if($pemdingFuelRe->Request_Type!='Full Tank'){

                $PendingFuelREAmount=($pemdingFuelRe->priceLists->price*$pemdingFuelRe->quantity)+$PendingFuelREAmount;
            }else{
                
                $PendingFuelREAmount=($pemdingFuelRe->priceLists->price*$pemdingFuelRe->getVehicle->capacity)+$PendingFuelREAmount;
            }
        }
    }


    $CustomerLubeRequests=CustomerLubeRequest::where('customer_code',$cus)->where('Execution_date',null)->where('RO_code',$rocode)->get();

    foreach ($CustomerLubeRequests as $CustomerLubeRequest) {
        
        if($CustomerLubeRequest->priceLists!=null)
         $PendingLubeREAmount=($CustomerLubeRequest->priceLists->price*$CustomerLubeRequest->quantity)+$PendingLubeREAmount;
    }


    foreach ($Customerinvoices as $invoiceCustomer) {
        //$amountTrajection=$amountTrajection+$invoiceCustomer->amount;
    }
 
  return view('backend.creditLimit.settlement',compact('PendingFuelREAmount','PendingLubeREAmount','Customerinvoices','tbl_customer_transaction','amountTrajection','Credit_limit','type','customer'));

}

public function invoicePendingCollectionFillter(Request $request)
{
  $rocode='';
  if (Auth::user()->getRocode!=null) {
    $rocode=Auth::user()->getRocode->RO_code;
  }

  $customer_code=$request->input('RO_code');
  
  $from_date = $request->input('fromdate'); 
  $to_date = $request->input('to_dae'); 
  $Settlement=$request->input('Settlement');
  $from_dates = new Carbon(str_replace('/', '-',$from_date));
  $to_dates = new Carbon(str_replace('/', '-',$to_date));
  $from_dates = date('Y-m-d', strtotime($from_dates));
  $to_dates = date('Y-m-d', strtotime($to_dates));
  $customer=$customer_code;
  $type=$Settlement;
  $tbl_customer_transaction =  DB::table('tbl_customer_master')->where('is_active',1)->where('RO_code',$rocode)->get();
    if(count($tbl_customer_transaction)==null){

    return back();
  }

  $Customerinvoices=invoiceCustomer::where('RO_code',$rocode)
  ->where('settlement_status',$Settlement)
  ->where('Customer_name',$customer_code)
  ->whereBetween('created_at', [$from_dates." 00:00:00", $to_dates." 23:59:59"])
  ->get();

  $Credit_limit=Credit_limit::where('Customer_Code',$customer_code)->first();
  $transactionModels =   TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
    ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
    ->where('tbl_customer_transaction.customer_code',$customer_code)
    ->where('tbl_customer_transaction.status','<>',2)
     ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode')
    ->get();

  $amountTrajection=0;
 
  foreach ($transactionModels as $transactionModel) {
      //dd($transactionModel->item_price);
      if($transactionModel->petrol_or_lube==1){
        $amountTrajection=$amountTrajection+($transactionModel->item_price*$transactionModel->petroldiesel_qty);
      }else{
        $amountTrajection=$amountTrajection+$transactionModel->item_price;
      }
     
  }

  
    $pemdingFuelRes=AddPetrolDieselModel::where('customer_code',$customer_code)->where('Execution_date',null)->get();
    $PendingFuelREAmount=0;
    $PendingLubeREAmount=0;

    foreach ($pemdingFuelRes as $pemdingFuelRe) {
        
        if($pemdingFuelRe->priceLists!=null){
          
            if($pemdingFuelRe->Request_Type!='Full Tank'){

                $PendingFuelREAmount=($pemdingFuelRe->priceLists->price*$pemdingFuelRe->quantity)+$PendingFuelREAmount;
            }else{
                
                $PendingFuelREAmount=($pemdingFuelRe->priceLists->price*$pemdingFuelRe->getVehicle->capacity)+$PendingFuelREAmount;
            }
        }
    }


    $CustomerLubeRequests=CustomerLubeRequest::where('customer_code',$customer_code)->where('Execution_date',null)->get();

    foreach ($CustomerLubeRequests as $CustomerLubeRequest) {
        
        if($CustomerLubeRequest->priceLists!=null)
         $PendingLubeREAmount=($CustomerLubeRequest->priceLists->price*$CustomerLubeRequest->quantity)+$PendingLubeREAmount;
    }



  $Customerinvoi=invoiceCustomer::where('RO_code',$rocode)
  ->where('settlement_status',0)
  ->where('Customer_name',$customer_code)
  ->get();
  foreach ($Customerinvoi as $invoiceCustomer) {
      //$amountTrajection=$amountTrajection+$invoiceCustomer->amount;
  }

  //dd($tbl_customer_transaction);
 return view('backend.creditLimit.settlement',compact('PendingFuelREAmount','PendingLubeREAmount','Customerinvoices','tbl_customer_transaction','Credit_limit','amountTrajection','type','customer'));

}


public function settled($id)
{   
  $invoiceCustomer=invoiceCustomer::where('id',$id)->first();
  return view('backend.creditLimit.settled',compact('invoiceCustomer'));
}


public function settledUdate(Request $request, $id)
{  

   $invoiceCustomer=invoiceCustomer::where('id',$id)->first();
   $payment_mode=$request->input('payment_mode');
   $bank_name=$request->input('bank_name');
   $checque_no=$request->input('checque_no');
   $settlement_date=new Carbon(str_replace('/', '-',$request->input('payment_date')));
   $checque_date=new Carbon(str_replace('/', '-',$request->input('checque_date')));

   $invoiceCustomer->settlement_date=$settlement_date;
   $invoiceCustomer->payment_mode=$payment_mode;
   $invoiceCustomer->settlement_status=1;
   
   if($payment_mode='NEFT'){
     $invoiceCustomer->bank_name=$bank_name;
   }

   if($payment_mode='Cheque'){
     $invoiceCustomer->checque_no=$checque_no;
     $invoiceCustomer->checque_date=$checque_date;
   }

   $invoiceCustomer->save();
   TransactionModel::where('bill_no',$invoiceCustomer->invoice_no)->update(['status'=>2]);
   
   return redirect('invoicePendingCollection');

}

public function viewSettled($id)
{
   $invoiceCustomer=invoiceCustomer::where('id',$id)->first();
   return view('backend.creditLimit.viewSettled',compact('invoiceCustomer'));

}

public function slipPendingPrinting()
{

}

public function saveBillifo($data)
{

}


}
