<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VechilManage;
use App\MastorRO;
use App\VehicleModel;
use App\IndustryDepartment;
use App\VehicleMakeModel;
use Illuminate\Support\Facades\Input;
use File;
use Storage;
use Carbon\Carbon;
use Session;
use DB;
use Gate;
use Auth;


class CustomerVehicleDriverController  extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if (Gate::allows('ro-admin',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }


        $Customer_Code='';
       if (Auth::user()->customer!=null) {
          $Customer_Code=Session::get('custm');
       }
       $rocode = Session::get('ro');
        
        $data = VechilManage::join('tbl_price_list_master', 'tbl_customer_vehicle_master.Fuel_Type', '=', 'tbl_price_list_master.id')
            ->join('tbl_ro_master', 'tbl_customer_vehicle_master.Ro_code', '=', 'tbl_ro_master.RO_code')
            ->join('tbl_customer_master', 'tbl_customer_vehicle_master.Customer_code', '=', 'tbl_customer_master.Customer_Code')
            ->select('tbl_customer_vehicle_master.*','tbl_price_list_master.Item_Name as type',
                'tbl_ro_master.pump_legal_name as pump_name','tbl_customer_master.Customer_Name as Customer_Name');

          if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $data=$data->where('tbl_customer_vehicle_master.Ro_code',$rocode)->where('tbl_customer_vehicle_master.Customer_code',$Customer_Code);

              $data=$data->orderby('id','desc')->get();
           
        $data1 = $MastorRO=MastorRO::where('is_active','1');

         if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $data1=$data1->where('tbl_ro_master.RO_code',$rocode);

              $data1=$data1->get();
           $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
          $getpric=  $getpric->pluck('item_id')->toArray();
        $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->whereIn('tbl_price_list_master.id',$getpric)->select('tbl_price_list_master.*');
          
        if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $fuel_type=$fuel_type->where('tbl_price_list_master.RO_Code',$rocode);

              $fuel_type=$fuel_type->get();
        $vehicleModelList = VehicleModel::orderby('name','asc')->get();
        $vehicleMakeList = VehicleMakeModel::orderby('name','asc')->get();
        $IndustryDepartment = IndustryDepartment::where('type','Insurance Company')->get();
      
        // $getdesil = DB::table('tbl_ro_petroldiesel')->get();
       //dd($Customer_Code);
        return view('backend/customer.customer_vehiclemanagement',compact('data','data1','fuel_type','IndustryDepartment','vehicleModelList','vehicleMakeList','Customer_Code'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function vechstore(Request $request)
    {  
        if(Auth::User()->customer!=null){
               $customer=Session::get('custm');
            }else{
              $customer=$request->input('Customer_code');
            }
            $url="images/driver.png";
          if($request->hasFile('rcphoto'))
         {
            $file = Input::file('rcphoto');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $filename = $file->getClientOriginalName();
            $name = $timestamp . '-' . $filename;
            $url = 'images/' . $name;
            $file->move('images/', $name);
         }
         $urls="images/driver.png";
         if($request->hasFile('insurancephoto'))
         {
            $file = Input::file('insurancephoto');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $filename = $file->getClientOriginalName();

            $name = $timestamp . '-' . $filename;
            $urls = 'images/' . $name;

            $file->move('images/', $name);
         }
		try {
			
			$puc_date = $request->input('puc_date');
			if($puc_date && $puc_date!=""){
				$date1 = str_replace('/', '-', $puc_date);
				$puc_date = date('Y-m-d', strtotime($date1));
			}	

			$insurance_due_date = $request->input('insurance_due_date');
			if($insurance_due_date && $insurance_due_date!=""){
				$date1 = str_replace('/', '-', $insurance_due_date);
				$insurance_due_date = date('Y-m-d', strtotime($date1));
			}	
  
			$VechilManage = new VechilManage();
   
			$VechilManage->Ro_code=$request->input('Ro_code'); 
			$VechilManage->Customer_code=$customer;
			$VechilManage->Registration_Number=$request->input('Registration_Number');
			$VechilManage->Make=$request->input('Make');
			$VechilManage->Model=$request->input('Model');
			$VechilManage->capacity=$request->input('capacity');
			$VechilManage->insurance_due_date=$insurance_due_date;
			$VechilManage->Fuel_Type=$request->input('Fuel_Type');
			$VechilManage->puc_date=$puc_date;
			$VechilManage->van_color=$request->input('van_color'); 
			$VechilManage->rcphoto=$url;
			$VechilManage->insurancephoto=$urls;
			$VechilManage->industry_company=$request->input('industry_company');  
			$VechilManage->save();
			$request->session()->flash('success','Added Successfully!!');
		} catch(\Illuminate\Database\QueryException $e){
			
			$request->session()->flash('success','Something wrong!!');
			
		}
        
        
       return redirect('customer_vehiclemanagement');
    }

   public function vechilDeactive(Request $request ,$id)
   {
   	    $data = $VechilManage=VechilManage::all(); 
       



   }
    /**
     * Display the specified resource.
     *
     * @param  \App\VechilManage  $VechilManage
     * @return \Illuminate\Http\Response
     */
    public function show(VechilManage $VechilManage,$id)
    {
        if (Gate::allows('ro-admin',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }

        $Customer_Code='';
       if (Auth::user()->customer!=null) {
          $Customer_Code=Session::get('custm');
       }
         $VechilManage=$VechilManage->find($id);


         $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->select('tbl_price_list_master.*')->get();

          $data1 = $MastorRO=MastorRO::where('is_active','1');

          if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $data1=$data1->where('tbl_ro_master.RO_code',Auth::user()->customer->RO_code);

              $data1=$data1->get();
        // $getdesil = DB::table('tbl_ro_petroldiesel')->get();

   $Model=VehicleModel::where('id',$VechilManage->Model)->first();
    
        $vehicleModelList = VehicleModel::All();
        $vehicleMakeList = VehicleMakeModel::All();
        return view('backend/customer.customer_vehicle_view_page',compact('VechilManage','fuel_type','data1','vehicleModelList','vehicleMakeList','Model')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VechilManage  $VechilManage
     * @return \Illuminate\Http\Response
     */
    public function edit(VechilManage $VechilManage , $id)
    {

        if (Gate::allows('ro-admin',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }

        $Customer_Code='';
       if (Auth::user()->customer!=null) {
          $Customer_Code=Session::get('custm');
       }
       $rocode = Session::get('ro');

        $VechilManage=$VechilManage->find($id);
         $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
         $getpric=  $getpric->pluck('item_id')->toArray();
         $fuel_type = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')->where('tbl_stock_item_group.Group_Name','FUEL')->whereIn('tbl_price_list_master.id',$getpric)->select('tbl_price_list_master.*');

         if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $fuel_type=$fuel_type->where('tbl_price_list_master.RO_Code',$rocode);

              $fuel_type=$fuel_type->get();

          $data1 = $MastorRO=MastorRO::where('is_active','1');

          if(Auth::user()->user_type!=1 && $Customer_Code!='')
                $data1=$data1->where('tbl_ro_master.RO_code',$rocode);

              $data1=$data1->get();

        // $getdesil = DB::table('tbl_ro_petroldiesel')->get();

          $Model=VehicleModel::where('id',$VechilManage->Model)->first();
    
        $vehicleModelList = VehicleModel::orderby('name','asc')->get();
        $vehicleMakeList = VehicleMakeModel::orderby('name','asc')->get();
        $IndustryDepartment = IndustryDepartment::where('type','Insurance Company')->get();
        return view('backend/customer.customer_vehicle_update',compact('VechilManage','fuel_type','IndustryDepartment','data1','vehicleModelList','vehicleMakeList','Model','Customer_Code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VechilManage  $VechilManage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VechilManage $VechilManage, $id)
    {
          $url="images/driver.png";
            if($request->hasFile('rcphoto'))
         {
            $file = Input::file('rcphoto');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $filename = $file->getClientOriginalName();

            $name = $timestamp . '-' . $filename;
            $url = 'images/' . $name;

            $file->move('images/', $name);
         }
         else
         {
            $url= $request->input('rcphotoold');
         }
         $urls="images/driver.png";
            if($request->hasFile('insurancephoto'))
         {
            $file = Input::file('insurancephoto');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $filename = $file->getClientOriginalName();

            $name = $timestamp . '-' . $filename;
            $urls = 'images/' . $name;

            $file->move('images/', $name);
         }
         else
         {
            $urls= $request->input('insurancephotoold');
         }
       
        try {
			
			$puc_date = $request->input('puc_date');
			if($puc_date && $puc_date!=""){
				$date1 = str_replace('/', '-', $puc_date);
				$puc_date = date('Y-m-d', strtotime($date1));
			}	

			$insurance_due_date = $request->input('insurance_due_date');
			if($insurance_due_date && $insurance_due_date!=""){
				$date1 = str_replace('/', '-', $insurance_due_date);
				$insurance_due_date = date('Y-m-d', strtotime($date1));
			}
       
            $VechilManage = $VechilManage->find($id);
            $VechilManage->Ro_code=$request->input('Ro_code'); 
            $VechilManage->Customer_code=$request->input('customer_code');
            $VechilManage->Registration_Number=$request->input('Registration_Number');
            $VechilManage->Make=$request->input('Make');
            $VechilManage->Model=$request->input('Model');
            $VechilManage->capacity=$request->input('capacity');
            $VechilManage->Fuel_Type=$request->input('Fuel_Type');
            $VechilManage->puc_date=$puc_date;
            $VechilManage->insurance_due_date=$insurance_due_date;
            $VechilManage->van_color=$request->input('van_color');
            $VechilManage->rcphoto=$url;
            $VechilManage->insurancephoto=$urls; 
            $VechilManage->industry_company=$request->input('industry_company');
            
            $VechilManage->save();
            
			//$data = $VechilManage;
			$request->session()->flash('success','Data Updated Successfully!!'); 
        } 
		catch(\Illuminate\Database\QueryException $e){ 
            $request->session()->flash('success','Something wrong!!'); 
        }
        return redirect('customer_vehiclemanagement');
    }

    function active(Request $request,$id, $actt)
    {
        
    	$ac = VechilManage::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }
      public function delete(Request $request, $id)
    {
        try {
          $ac = VechilManage::where('id',$id)->delete();
        $request->session()->flash('success','Delete Successfully !!');  
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
       
        return back();


    }
}
    
	   
	
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VechilManage  $VechilManage
     * @return \Illuminate\Http\Response
     */
    

