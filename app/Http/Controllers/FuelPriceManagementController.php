<?php

namespace App\Http\Controllers;

use App\CustomerLubeRequest;
use Illuminate\Http\Request;
use App\RoMaster;
use App\TableItemPriceListModel;
use App\RoPieceListManagement;
use App\TransactionModel;
use App\TransactionInvoiceModel;
use DB;
use Carbon\Carbon;
use Gate;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use File;
use App\Stock_item_group;
use App\OutletConfigModel;
use Log;




class FuelPriceManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
Log::info('FuelPriceManagementController@index inpur = '.print_r($request->all(),true));

    /*$TableItemPriceListModels=RoPieceListManagement::with(['getgroup' => function ($query) {
                                   $query->where('Group_Name','FUEL');
                     }])->where('is_active',1)->where('RO_Code',$rocode)->get();*/
     if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }

        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $roconfigTime='06:00:00';
        $date =$request->input('date');

         if($date!=null){
              $date = new Carbon(str_replace('/', '-',$date));
              $date =date('Y-m-d',strtotime($date));
         }

        $OutletConfigModel=OutletConfigModel::where('rocode',$rocode)->where('field_name','FUEL_PRICE_Date_time')->first();
        
       
        if($OutletConfigModel!=null)
        $roconfigTime=$OutletConfigModel->value;

        $enddate=date('Y-m-d');
       $Stock_item_group=Stock_item_group::where('Group_Name','FUEL')->first();

       if($Stock_item_group!=null && $Stock_item_group->getAllitem!=null){

         $datepr=$Stock_item_group->getAllitem()->where('RO_Code',$rocode)->where('is_active',1)->pluck('id')->toArray();
         
         $TableItemPriceListModels=$Stock_item_group->getAllitem()->where('RO_Code',$rocode)->where('is_active',1)->get();
         $TableItemPriceListModel=TableItemPriceListModel::whereIn('item_id',$datepr)->orderBy('effective_date', 'desc')->first();
         
         if($TableItemPriceListModel!=null)
          $enddate=$TableItemPriceListModel->effective_date;
          //dd($enddate);
           if($date==null){
            
            return view('backend.price.fuelprice2',compact('TableItemPriceListModels','date','enddate','roconfigTime'));
           }else{
              
               $efective_date = new Carbon($date);
               $efective_date=$efective_date->format('Y-m-d');
               $fromdate=new Carbon($date);
               $fromdate=$fromdate->subDays(5)->format('Y-m-d');
               $dateitem=TableItemPriceListModel::whereIn('item_id',$datepr)->where('effective_date','>=',$efective_date.' 00:00:00')->where('effective_date','<=',$efective_date.' 23:59:00')->get();
                    
               $TableItemPriceListModel=TableItemPriceListModel::whereIn('item_id',$datepr)->where('effective_date','>=',$fromdate.' 00:00:00')->where('effective_date','<=',$efective_date.' 23:59:00')->orderBy('effective_date', 'asc')
                    ->get();

              $dateofprices=$TableItemPriceListModel->groupBy('effective_date');

            return view('backend.price.fuelprice2',compact('TableItemPriceListModels','dateofprices','date','enddate','dateitem','roconfigTime'));
           }
       }
       else{
        return back();
       }
      
      
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
  
        $datas= RoMaster::where('is_active',1)->get();
        $item_id= RoPieceListManagement::All();
        $lube_list= CustomerLubeRequest::join('tbl_stock_item_master', 'tbl_customer_request_lube.item_id', '=', 'tbl_stock_item_master.id') ->select('tbl_customer_request_lube.*', 'tbl_stock_item_master.Stock_Item_Name')->get();

        // $designation=DB::table('tbl_designation_master')->get();
         // return view('backend.lube_page',compact('designation'));
         return view('backend.lube_page',compact(['datas','item_id','lube_list']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

     
        
    }


    public function saveFuelPrice(Request $request)
    {

      Log::info('FuelPriceManagementController@saveFUelPrice input - '.print_r($request->all(),true));
          
           $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }
          $OutletConfigModel=OutletConfigModel::where('rocode',$rocode)->where('field_name','FUEL_PRICE_Date_time')->first();
        
          if($OutletConfigModel!=null)
            $time=date('h:i:s',strtotime($OutletConfigModel->value));

          $date =$request->input('date');
          
          //dd($date);
         
        $effective_date = $date.' '.$time;
        $item = $request->input('item');
        $price = $request->input('price');

        for($i=0;$i<count($item);$i++)
        {
            if(isset($price[$i]) && isset($item[$i])){
                  $is_active = TableItemPriceListModel::where('item_id',$item[$i])->where('is_active',1)->update(['is_active' =>0]);

                  $update_price = new TableItemPriceListModel();
                  $update_price->item_id = $item[$i];
                  $update_price->price = $price[$i];
                  $update_price->effective_date =  $effective_date;
                  $update_price->save();
            }
        }  

         $request->session()->flash('success','Price s Updated Successfully!!');
         return back();
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerLubeRequest $customerLubeRequest,$id)
    {
         $lube_data = DB::table('tbl_customer_request_lube')->where('id', '=', $id)->first();
        
        
        return view('backend.lube_view_page', compact(['lube_data']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerLubeRequest $customerLubeRequest)
    {
        
    }
    function active(Request $request,$id, $actt)
    {
        
        $ac = CustomerLubeRequest::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

       Log::info('FuelPriceManagementController@update input - '.print_r($request->all(),true));

        $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }

       
       try {

        $item = $request->input('itemPrId');
        $price = $request->input('price');
        $date = $request->input('date');
        $todate=date('Y-m-d',strtotime($date)+86400);
        $items = $request->input('item');
        $TransactionModel=TransactionModel::whereIn('item_code',$items)->where('status',1)->where('trans_date','>=',$date.' 06:00:00')->where('trans_date','<=',$todate.' 05:59:59')->get();
        
        if($TransactionModel->count()>0){
            $request->session()->flash('success','For this date ,billing already processed so you can not change the prices !!');
             return back();
        }

        for($i=0;$i<count($item);$i++)
        {
            if(isset($price[$i]) && isset($item[$i])){
              
                  $TableItemPriceListModel = TableItemPriceListModel::find($item[$i]);
                  $TableItemPriceListModel->price = $price[$i];
                  $TableItemPriceListModel->save();
                  $ndate=date('Y-m-d H:i:s',strtotime($TableItemPriceListModel->effective_date)+86400);
                  $alltranjections=TransactionModel::where('item_code',$TableItemPriceListModel->item_id)->where('status',0)->where('trans_date','>=',$TableItemPriceListModel->effective_date)->where('trans_date','<',$ndate)->get();
                  
                  $this->setAllTranjectionPrice($alltranjections,$TableItemPriceListModel->price);
                 


                 //TransactionModel::where('item_code',$TableItemPriceListModel->item_id)->where('status',0)->where('trans_date','>=',$TableItemPriceListModel->effective_date)->where('trans_date','<',$ndate)->update(['item_price' => $TableItemPriceListModel->price]);
           }
        }

          // create item record if not found
        $OutletConfigModel=OutletConfigModel::where('rocode',$rocode)->where('field_name','FUEL_PRICE_Date_time')->first();
        
          if($OutletConfigModel!=null)
            $time=date('h:i:s',strtotime($OutletConfigModel->value));


        $effective_date = $date.' '.$time;
       

       for($i=0;$i<count($items);$i++)
        {
            $checkPriceRecord=DB::table('tbl_item_price_list')->where('item_id',$items[$i])->whereDate('effective_date',$date)->get();

           
            if($checkPriceRecord->isEmpty()){
                   Log::info('checkPriceRecord   '.print_r($checkPriceRecord,true));
                  Log::info('checkPriceRecord not Found. need to create  $items'.$items[$i]);


                if(isset($price[$i]) && isset($items[$i])){


                 
                  $is_active = TableItemPriceListModel::where('item_id',$items[$i])->where('is_active',1)->update(['is_active' =>0]);

                  $update_price = new TableItemPriceListModel();
                  $update_price->item_id = $items[$i];
                  $update_price->price = $price[$i];
                  $update_price->effective_date =  $effective_date;
                  $update_price->save();
            }

            }else{

               Log::info('checkPriceRecord   found   $items-- '.$items[$i]);

            }


         /*   if(isset($price[$i]) && isset($items[$i])){


                 
                  $is_active = TableItemPriceListModel::where('item_id',$items[$i])->where('is_active',1)->update(['is_active' =>0]);

                  $update_price = new TableItemPriceListModel();
                  $update_price->item_id = $item[$i];
                  $update_price->price = $price[$i];
                  $update_price->effective_date =  $effective_date;
                  $update_price->save();
            }*/
        }   








        $request->session()->flash('success','Price s Updated Successfully!!');

      } catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something Wrong!!');
                
      }
      return back();
    }

     /**
     * update tranjection price and qty
     *
     * @param  $alltranjections object eluqent
     * @param  $price int
     * @return \Illuminate\Http\Response
     */
    public function setAllTranjectionPrice($alltranjections,$price){

        foreach ($alltranjections as $alltranjection) {

                   $petroldiesel_qty=$alltranjection->petroldiesel_qty;

                    if($alltranjection->getfuelRequest!=null){

                        if(trim($alltranjection->getfuelRequest->Request_Type)=='Rs'){

                           if($alltranjection->getfuelRequest->Request_Value!=null)
                            $petroldiesel_qty= $alltranjection->getfuelRequest->Request_Value / $price;
                        }
                    }

                   //TransactionModel::where('id',$alltranjection->id)->update();
                     DB::table('tbl_customer_transaction')->where('id',$alltranjection->id)->update(['petroldiesel_qty'=>$petroldiesel_qty,'item_price' => $price]);
//dd($alltranjection->gettex);
                     if($alltranjection->gettex!=null){
//dd($gettex);
                        foreach ($alltranjection->gettex as $geds) {
                            $Pval=0;//dd($geds->id);
                            $Pval=round($petroldiesel_qty*$price*$geds->Tax_percentage/100,2);
                            DB::table('item_transaction_tax')->where('id',$geds->id)->update(['Tax_Value' => $Pval]);
                        }
                     }
                 }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        try {
            $personal_mangement = CustomerLubeRequest::find($id);

        $personal_mangement->delete();
        $request->session()->flash('success','Data has been deleted Successfully!!');
        }  catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
         

        return back();
    }

    public function DownloadItemFUEL(Request $request)
	{
	


        try{  
	    	  $item='FUEL';

	    	   
	            $rocode='';
	            if(Auth::user()->getRocode!=null) {
	               $rocode=Auth::user()->getRocode->RO_code;
	            }
	         
	         	$getitem= RoPieceListManagement::join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
		            ->where('tbl_stock_item_group.Group_Name',$item)
		            ->where('tbl_price_list_master.is_active',1)
		            ->select('tbl_price_list_master.*','tbl_stock_item_group.Group_Name');
	       


	            if(Auth::user()->user_type!=1 && $rocode!='')
	                $getitem=$getitem->where('tbl_price_list_master.RO_Code',$rocode);

	            $getitem=$getitem->get();

	             $search_date=date('d/m/Y');
	           
	         
	            $tot_record_found=0;

			    if(count($getitem)>0){
			        $tot_record_found=1;
			         
			        $CsvData=array('Item Name,Price,Item Group,Effective Date');          
			        foreach($getitem as $value){
                         //dd($value->Effective_Date);
			            if(isset($value->getpricelist)){
                                   
			                        $CsvData[]=$value->Item_Name.','.$value->price.','.$item.','.date('d/m/Y').' 6:00:00 AM';
			               }else{
			                       if(isset($value->price)){
			                     	 $CsvData[]=$value->Item_Name.','.$value->price->price.','.$item.','.date('d/m/Y').' 6:00:00 AM';
			                       }else{
			                       	 $CsvData[]=$value->Item_Name.','." ".','.$item.','.date('d/m/Y').' 6:00:00 AM';
			                       }
			            }          
			        }
			         
			        $filename=date('Y-m-d').".csv";
			        $file_path=base_path().'/'.$filename;   
			        $file = fopen($file_path,"w+");
			        foreach ($CsvData as $exp_data){
			          fputcsv($file,explode(',',$exp_data));
			        }

			        fclose($file);          

			        $headers = ['Content-Type' => 'application/csv'];
			        return response()->download($file_path,$filename,$headers );
			    }

			}
		catch(\Illuminate\Database\QueryException $e)
			{
			                
			                $request->session()->flash('success','Please  Master For Add Tax !!');
			                
			}

        return back();
	}
	public function updateCsvDataFuel(Request $request)
	{
	 	$rocode='';
            if(Auth::user()->getRocode!=null) {
               $rocode=Auth::user()->getRocode->RO_code;
            }

       
   		 if (Input::hasFile('file'))
            {

                $file = Input::file('file');
                $name = time() . '-' . $file->getClientOriginalName();
                $path = storage_path('documents');
                $file->move($path, $name);
                $filename=base_path('/storage/documents/'.$name);
                //$file = fopen($filename, "r");
                 $file=fopen($filename, 'r');
                  while(! feof($file))
                  {
                    $row= fgetcsv($file);

                    if($row){
                        $datas[]=$row;
                    }

                }
                    

                    unset($datas[0]);
                               
                              foreach ($datas as $data)
                              {
                              	//dd($datas);
                                $itemname = DB::table('tbl_price_list_master')->where('Item_Name', '=', $data[0])->first();
                                  if($itemname == null || trim($data[0]) =='')
                                
                                    {
                                   $request->session()->flash('success','Could not found this iten name '.$data[0]); 
                                   return back();
                                    } 

                                    $date_now = date("d-m-Y");
                                     $efective_date = strtotime(new Carbon(str_replace('/', '-',$data[3])));

                                     $date_nows = strtotime(new Carbon($date_now));
                                     $date_nows = $date_nows+86400;

                                    if ($date_nows < $efective_date) {
                                  
                                       $request->session()->flash('success','Could not Add future date'); 
                                       return back();
                                    } 

                                     $date_nows = strtotime(new Carbon($date_now));
                                     $date_nows = $date_nows;

                                    if ($date_nows > $efective_date) {
                                  
                                       $request->session()->flash('success','Could  not Add Past date'); 
                                       return back();
                                    }

                                   if($data[1] == 0 || trim($data[1]) =='')
                                   {
                                    
                                      $request->session()->flash('success','Please  Check Price Value!!'); 
                                      return back();
                                     
                                  }
                                  
                              }
                          
                    
                            foreach ($datas as $data) {
                              
                                 
                                $tbl_price_list_master= RoPieceListManagement::join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
                                ->where('tbl_stock_item_group.Group_Name',trim($data[2]))
                                ->where('tbl_price_list_master.RO_Code',$rocode)
                                ->where('tbl_price_list_master.Item_Name',trim($data[0]))
                                 ->select('tbl_price_list_master.*')->first();
                               
              
                               
                                  if($tbl_price_list_master == null)
                                    {
                                   $request->session()->flash('success','Somthing Wrong'); 
                                   return back();
                                   }


                             $dat= TableItemPriceListModel::where('item_id',$tbl_price_list_master->id)->first();
                            
                          
                             if($dat!=null)


                              	TableItemPriceListModel::where('item_id',$tbl_price_list_master->id)->where('is_active',2)->update(['is_active' =>0]);
                                TableItemPriceListModel::where('item_id',$tbl_price_list_master->id)->where('is_active',1)->update(['is_active' =>2]);
                               
                              $Roitem= new TableItemPriceListModel();
                              $Roitem->item_id= $tbl_price_list_master->id;
                             
                               $Roitem->price= $data[1];

                                $efective_date = new Carbon(str_replace('/', '-',$data[3]));
         
                               //$effective_date = new Carbon(str_replace('/', '-',$data[3]));
                               $Roitem->effective_date=$efective_date;
                               //dd($Roitem->effective_date);
                               
                              
           
                               $Roitem->is_active =1;
                               $Roitem->save(); 

                                 
                           }
                      

                   
                $request->session()->flash('success','Data Import Csv  Successfully!!'); 
          
                  
            }
             return back();
    }
}
