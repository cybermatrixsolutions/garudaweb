<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Session;
use DB;
class RetailOutlet extends Controller
{
   
    public function login()
        {	
          return view('backend.login'); 
        }
	
    public function adminLogin(Request $request)
     {   
        {
        if($the_user=User::select()->where('user_type','=','admin')->where('email','=',$request->email)->where('password','=',($request->password))->first())
  
                    {
                     Auth::login($the_user);
                     return redirect('Dashboard');                       
                    }
            else
            { 
                 $request->session()->flash('success','Wrong Username or Password');
                 return redirect('masterAdmin');
            }
         }
    }
	
	public function Dashboard()
	   {
		 return view('backend.dashboard'); 
	   }


	
	
	public function retail()
	    {
		
		return view('backend.retail'); 
	
	    }
	
	/* Unit Of Measure Management Start*/  
	    
	public function unitMeasure()
	    {
		
		return view('backend.unit_of_measure'); 
	
	    }
	   
	public function add_unit_measure(Request $request){
		
		  $input = $request->all();

		  DB::table('tbl_unit_of_measure')->insert($input);
		  $request->session()->flash('success','Succesfully Added Unit Of Measure!!');
		  return redirect('unit_of_measure');
	    }   
	public function unit_measure_listing()
        { 		

		   $data['getUnitMeasure'] = DB::table('tbl_unit_of_measure')->orderBy('id', 'desc')->get();    
           return view('backend.unit_of_measure',$data);  
        }    
	public function unit_measure_update(Request $request ,$id)
	    {   
		   $data['unitMeasureData']=DB::table('tbl_unit_of_measure')->where('id','=',$id)->get();
		   
		   return view('backend.unit_of_measure_update', $data);
	    }

     public function unit_measure_edit(Request $request ,$id)
        {
           $input=$request->all(); 		   	  
           DB::table('tbl_unit_of_measure')->where('id',$id)->update($input); 
           $request->session()->flash('success','Data Updated Successfully!!');     
           return redirect('unit_of_measure');  
        }

    Public function unit_measure_delete(Request $request ,$id)
	   {  
		  DB::table('tbl_unit_of_measure')->where('id','=',$id)->delete();
		  $request->session()->flash('success','Data has been deleted Successfully!!');
		  return redirect('unit_of_measure');   
       } 
	function unitMeasureDeactive(Request $request,$id)
    { 
      
        DB::table('tbl_unit_of_measure')
            ->where('id', $id)
            ->update(['is_active' => 1]);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('unit_of_measure');
      
              }   
	   
	function unitMeasureActive(Request $request,$id)
    {
      
        DB::table('tbl_unit_of_measure')
            ->where('id', $id)
            ->update(['is_active' => 0]);
                $request->session()->flash('success','Activated Successfully !!');
                return redirect('unit_of_measure');
                
                   
    }   
	   
	   
	/* Unit Of Measure Management End*/
	
	
	
	/* Stock Item Group Management Start */
	
	public function stock_item_group()
	   {		
		 return view('backend.stock_item_group'); 	
	   }
	   
	public function add_stock_item_group(Request $request){
		
		  $input = $request->all();

		  DB::table('tbl_stock_item_group')->insert($input);
		  $request->session()->flash('success','Succesfully Added Stock Item Group!!');
		  return redirect('stock_item_group');
	    }   
	public function stock_item_group_listing()
        { 		

		   $data['getStockItem'] = DB::table('tbl_stock_item_group')->orderBy('id', 'desc')->get(); 
		   
           return view('backend.stock_item_group',$data);  
        }    
	public function stock_item_group_update(Request $request ,$id)
	    {   
		   $data['stockItemData']=DB::table('tbl_stock_item_group')->where('id','=',$id)->get();
		   
		   return view('backend.stock_item_group_update', $data);
	    }

     public function stock_item_group_edit(Request $request ,$id)
        {
           $input=$request->all(); 		   	  
           DB::table('tbl_stock_item_group')->where('id',$id)->update($input); 
           $request->session()->flash('success','Data Updated Successfully!!');     
           return redirect('stock_item_group');  
        }

    Public function stock_item_group_delete(Request $request ,$id)
	   {  
		  DB::table('tbl_stock_item_group')->where('id','=',$id)->delete();
		  $request->session()->flash('success','Data has been deleted Successfully!!');
		  return redirect('stock_item_group');   
       }
	   
	function stockItemGroupDeactive(Request $request,$id)
    { 
      
        DB::table('tbl_stock_item_group')
            ->where('id', $id)
            ->update(['is_active' => 1]);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('stock_item_group');
      
        }   
	   
	function stockItemGroupActive(Request $request,$id)
    {
      
        DB::table('tbl_stock_item_group')
            ->where('id', $id)
            ->update(['is_active' => 0]);
                $request->session()->flash('success','Activated Successfully !!');
                return redirect('stock_item_group');
                
                   
    }        
	
	/* Stock Item Group Management End */

	
	/* Designation Management Start */
	
	
	public function designation()
	   {		
		 return view('backend.designation'); 	
	   }
	   
	public function add_designation(Request $request){
		
		  $input = $request->all();
		  
		 // dd($input);die;

		  DB::table('tbl_designation_master')->insert($input);
		  $request->session()->flash('success','Succesfully Added Designation!!');
		  return redirect('designation');
	    }   
	public function designation_listing()
        { 		

		   $data['getDesignation'] = DB::table('tbl_designation_master')->orderBy('id', 'desc')->get(); 
		   
           return view('backend.designation',$data);  
        }    
	public function designation_update(Request $request ,$id)
	    {   
		   $data['designationData']=DB::table('tbl_designation_master')->where('id','=',$id)->get();
		   
		   return view('backend.designation_update', $data);
	    }

    public function designation_edit(Request $request ,$id)
        {
           $input=$request->all(); 		   	  
           DB::table('tbl_designation_master')->where('id',$id)->update($input); 
           $request->session()->flash('success','Data Updated Successfully!!');     
           return redirect('designation');  
        }

    Public function designation_delete(Request $request ,$id)
	   {  
		  DB::table('tbl_designation_master')->where('id','=',$id)->delete();
		  $request->session()->flash('success','Data has been deleted Successfully!!');
		  return redirect('designation');   
       }
	   
	function designationDeactive(Request $request,$id)
    { 
      
        DB::table('tbl_designation_master')
            ->where('id', $id)
            ->update(['is_active' => 1]);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('designation');
      
        }   
	   
	function designationActive(Request $request,$id)
    {
      
        DB::table('tbl_designation_master')
            ->where('id', $id)
            ->update(['is_active' => 0]);
                $request->session()->flash('success','Activated Successfully !!');
                return redirect('designation');
                
                   
    }      
	
	/* Designation Management End */
	
	
	
	
	/* Dip Stick Management Start */
	
	
	public function dip_stick()
	   {		
		 return view('backend.dip_stick'); 	
	   }
	   
	public function add_dip_stick(Request $request){
		
		  $input = $request->all();
          // dd($input);die;
		  DB::table('tbl_dip_stick_master')->insert($input);
		  $request->session()->flash('success','Succesfully Added Dip Stick!!');
		  return redirect('dip_stick');
	    }   
	public function dip_stick_listing()
        { 		

		   $data['getDipStick'] = DB::table('tbl_dip_stick_master')->orderBy('id', 'desc')->get(); 
		   
           return view('backend.dip_stick',$data);  
        }    
	public function dip_stick_update(Request $request ,$id)
	    {   
		   $data['dipSticData']=DB::table('tbl_dip_stick_master')->where('id','=',$id)->get();
		   
		   return view('backend.dip_stick_update', $data);
	    }

    public function dip_stick_edit(Request $request ,$id)
        {
           $input=$request->all(); 		   	  
           DB::table('tbl_dip_stick_master')->where('id',$id)->update($input); 
           $request->session()->flash('success','Data Updated Successfully!!');     
           return redirect('dip_stick');  
        }

    Public function dip_stick_delete(Request $request ,$id)
	   {  
		  DB::table('tbl_dip_stick_master')->where('id','=',$id)->delete();		  
		  $request->session()->flash('success','Data has been deleted Successfully!!');
		  return redirect('dip_stick');   
       }
	   
	function dipStickDeactive(Request $request,$id)
     { 
      
        DB::table('tbl_dip_stick_master')
            ->where('id', $id)
            ->update(['is_active' => 1]);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('dip_stick');
      
        }   
	   
	function dipStickActive(Request $request,$id)
    {
      
        DB::table('tbl_dip_stick_master')
            ->where('id', $id)
            ->update(['is_active' => 0]);
                $request->session()->flash('success','Activated Successfully !!');
                return redirect('dip_stick');
                
                   
    }      
	/* Dip Stick Management End */
	
	
	
	/* Vehicle Management Start */
	
/* 	
	public function vehicle()
	   {		
		 return view('backend.vehicle'); 	
	   }
	   
	public function add_vehicle(Request $request){
		
		  $input = $request->all();
          // dd($input);die;
		  DB::table('tbl_dip_stick_master')->insert($input);
		  $request->session()->flash('success','Succesfully Added Dip Stick!!');
		  return redirect('vehicle');
	    }   
	public function vehicle_listing()
        { 		

		   $data['getVehicle'] = DB::table('tbl_dip_stick_master')->orderBy('id', 'desc')->get(); 
		   
           return view('backend.vehicle',$data);  
        }    
	public function vehicle_update(Request $request ,$id)
	    {   
		   $data['vehicleData']=DB::table('tbl_dip_stick_master')->where('id','=',$id)->get();
		   
		   return view('backend.vehicle_update', $data);
	    }

    public function vehicle_edit(Request $request ,$id)
        {
           $input=$request->all(); 		   	  
           DB::table('tbl_dip_stick_master')->where('id',$id)->update($input); 
           $request->session()->flash('success','Data Updated Successfully!!');     
           return redirect('vehicle');  
        }

    Public function vehicle_delete(Request $request ,$id)
	   {  
		  DB::table('tbl_dip_stick_master')->where('id','=',$id)->delete();		  
		  $request->session()->flash('success','Data has been deleted Successfully!!');
		  return redirect('vehicle');   
       }
	 */
	/* Vehicle Management End */
	
	
	
	
	
	/* Tax Management Start */
	
	
	
	public function tax()
	   {		
		 return view('backend.tax'); 	
	   }
	   
	public function add_tax(Request $request){
		
		  $input = $request->all();
          // dd($input);die;
		  DB::table('tbl_tax_master')->insert($input);
		  $request->session()->flash('success','Succesfully Added Tax!!');
		  return redirect('tax');
	    }   
	public function tax_listing()
        { 		

		   $data['getTax'] = DB::table('tbl_tax_master')->orderBy('id', 'desc')->get(); 
		   
           return view('backend.tax',$data);  
        }    
	public function tax_update(Request $request ,$id)
	    {   
		   $data['taxData']=DB::table('tbl_tax_master')->where('id','=',$id)->get();
		   
		   return view('backend.tax_update', $data);
	    }

    public function tax_edit(Request $request ,$id)
        {
           $input=$request->all(); 		   	  
           DB::table('tbl_tax_master')->where('id',$id)->update($input); 
           $request->session()->flash('success','Data Updated Successfully!!');     
           return redirect('tax');  
        }

    Public function tax_delete(Request $request ,$id)
	   {  
		  DB::table('tbl_tax_master')->where('id','=',$id)->delete();		  
		  $request->session()->flash('success','Data has been deleted Successfully!!');
		  return redirect('tax');   
       }
	
	function taxDeactive(Request $request,$id)
     { 
      
        DB::table('tbl_tax_master')
            ->where('id', $id)
            ->update(['is_active' => 1]);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('tax');
      
        }   
	   
	function taxActive(Request $request,$id)
    {     
        DB::table('tbl_tax_master')
            ->where('id', $id)
            ->update(['is_active' => 0]);
                $request->session()->flash('success','Activated Successfully !!');
                return redirect('tax');
	}
	/* Tax Management Start */

	
	/* Accounting Management Start */
		
		
	public function accounting()
	   {		
		 return view('backend.accounting'); 	
	   }
	   
	public function add_accounting(Request $request){
		
		  $input = $request->all();
          // dd($input);die;
		  DB::table('tbl_accounting_ledger_master')->insert($input);
		  $request->session()->flash('success','Succesfully Added Accounting!!');
		  return redirect('accounting');
	    }   
	public function accounting_listing()
        { 		

		   $data['getAccounting'] = DB::table('tbl_accounting_ledger_master')->orderBy('id', 'desc')->get(); 
		   
           return view('backend.accounting',$data);  
        }    
	public function accounting_update(Request $request ,$id)
	    {   
		   $data['accountingData']=DB::table('tbl_accounting_ledger_master')->where('id','=',$id)->get();
		   
		   return view('backend.accounting_update', $data);
	    }

    public function accounting_edit(Request $request ,$id)
        {
           $input=$request->all(); 		   	  
           DB::table('tbl_accounting_ledger_master')->where('id',$id)->update($input); 
           $request->session()->flash('success','Data Updated Successfully!!');     
           return redirect('accounting');  
        }

    Public function accounting_delete(Request $request ,$id)
	   {  
		  DB::table('tbl_accounting_ledger_master')->where('id','=',$id)->delete();		  
		  $request->session()->flash('success','Data has been deleted Successfully!!');
		  return redirect('accounting');   
       }
	function accountingDeactive(Request $request,$id)
     { 
      
        DB::table('tbl_accounting_ledger_master')
            ->where('id', $id)
            ->update(['is_active' => 1]);
             $request->session()->flash('success','Deactivated Successfully !!');
                return redirect('accounting');
      
        }   
	   
	function accountingActive(Request $request,$id)
    {     
        DB::table('tbl_accounting_ledger_master')
            ->where('id', $id)
            ->update(['is_active' => 0]);
                $request->session()->flash('success','Activated Successfully !!');
                return redirect('accounting');
	}
	
		/* Accounting Management Start */
		
		
		
		
		
		
		
		
	
    public function stock_item_management()
	    {		
		  return view('backend.stock_item_management');
	    }
    
    public function addStockItem(){
		
	}



}
