<?php

namespace App\Http\Controllers;

use App\Dipchart;
use App\ChartVolume;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use File;

class DipchartController extends Controller
{
    /**
     * Display a listing of the resource. of Dipchart
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       $dipcharts=Dipchart::orderBy('id', 'desc')->get();

       return view('backend.dipchart.dipchart',compact('dipcharts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function discreaption(Request $request)
    {
            $code=$request->input('dip');
            $dipchart=Dipchart::where('volume',$code)->get();
            $dipchart=$dipchart->pluck('discretion','id')->toArray();

             return response()->json($dipchart);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewdipchart($id)
    {
            
            $ChartVolumes=ChartVolume::where('dipchart_id',$id)->get();

            $str='<table class="table table-striped">
                <thead>
                  <tr role="row">
                   <th >Dip</th>
                   <th >Vol</th>
                   <th >Diff/mm</th>
                 <tbody>';
            foreach ($ChartVolumes as $ChartVolume) {
                $str.='<tr role="row" class="odd">
                        <td>'.$ChartVolume->dip.'</td>
                        <td>'.$ChartVolume->vol.'</td>
                        <td>'.$ChartVolume->diff_mm.'</td>
                    </tr>';
            }
            $str.='</tbody>
              </table>';
            

             return $str;
    }
    
    
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dipChartsview()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$file = fopen("contacts.csv","r");
        print_r(fgetcsv($file));*/
        try {
            
            $dipchart= new Dipchart();
            $dipchart->volume=$request->input('volume');
            $dipchart->lenght_type=$request->input('lenght_type'); 
            $dipchart->discretion=$request->input('discretion'); 

            $dipchart->save();

             if (Input::hasFile('file'))
            {

                $file = Input::file('file');
                $name = time() . '-' . $file->getClientOriginalName();
                $path = storage_path('documents');
                $file->move($path, $name);
                $filename=base_path('/storage/documents/'.$name);
                //$file = fopen($filename, "r");
                 $file=fopen($filename, 'r');
                  while(! feof($file))
                  {
                    $row= fgetcsv($file);

                    if($row){
                        $datas[]=$row;
                    }

                }

                    unset($datas[0]);

                     
                    if($dipchart->lenght_type=='mm'){

                           foreach ($datas as $data) {
                              
                              if(isset($data[0]) && isset($data[1])){
                                  $ChartVolume= new ChartVolume();
                                  $ChartVolume->dip= trim(str_replace('\r','',str_replace('\n','',$data[0])));
                                  $ChartVolume->vol= trim(str_replace('\r','',str_replace('\n','',$data[1])));
                                  //$ChartVolume->diff_mm= $data[2];
                                  $ChartVolume->dipchart_id=$dipchart->id;
                                  $ChartVolume->save();
                              }
                           }

                    }else{
                        
                         foreach ($datas as $data) {
                             if(isset($data[0]) && isset($data[1]) && isset($data[2])){
                              $ChartVolume= new ChartVolume();
                              $ChartVolume->dip= trim(str_replace('\r','',str_replace('\n','',$data[0])));
                              $ChartVolume->vol= trim(str_replace('\r','',str_replace('\n','',$data[1])));
                              $ChartVolume->diff_mm= trim(str_replace('\r','',str_replace('\n','',$data[2])));
                              $ChartVolume->dipchart_id=$dipchart->id;
                              $ChartVolume->save();
                             }
                           }
                    }
                
                  
            }
            
            $request->session()->flash('success','Create Dipchart Successfully !!');
        } catch(\Illuminate\Database\QueryException $e){
              
             
                $request->session()->flash('success','Something wrong!!');  
            }
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dipchart  $dipchart
     * @return \Illuminate\Http\Response
     */
    public function show(Dipchart $dipchart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dipchart  $dipchart
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
       $dipcharts=ChartVolume::where('dipchart_id',$id)->get();
       return view('backend.dipchart.view',compact('dipcharts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dipchart  $dipchart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dipchart $dipchart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dipchart  $dipchart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dipchart $dipchart)
    {
        //
    }
    function active(Request $request,$id, $actt)
    {
        
        $ac = Dipchart::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }

    public function download(){
        $pathToFile=storage_path('documents/example/dipcharts.csv');
       
        return response()->download($pathToFile);
     }
}
