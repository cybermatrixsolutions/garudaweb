<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

use App\RoCustomertManagement;
use App\RoMaster;
use App\IndustryDepartment;
use App\Credit_limit;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use App\User;
use Gate;
use Auth;
use Session;
use App\customerData;

class RoCustomertManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         //dd($request->all());
 // $validatedData = $request->validate([
         
         
         
 //         'company_name' => 'required',
 //         'coustomer_name' => 'required',
 //         'password' => 'required|string|min:6',
 //         'mobile_no' => 'required|numeric|',
         
         
 //    ]);

            $length = 5;
            $randomBytes = openssl_random_pseudo_bytes($length);
            $characters = '0123456789';
            $charactersLength = strlen($characters);
            $result = '';
            for ($i = 0; $i < $length; $i++)
                $result .= $characters[ord($randomBytes[$i]) % $charactersLength];

 
         try {
             
            $neuser=User::where('user_type','<>',4)->where('mobile',$request->input('mobile_no'))->get();

            if($neuser->count()>0){
               $request->session()->flash('success','Mobile No. Already Exist !!');
               return back(); 
            }
             
            if($request->hasFile('aadhaar_img'))
             {
                $file = Input::file('aadhaar_img');
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $filename = $file->getClientOriginalName();

                $name = $timestamp . '-' . $filename;
                $aadhaar_img = 'images/' . $name;

                $file->move('images/', $name);
             }
            $customer_mangement = new RoCustomertManagement();
            $customer_mangement->RO_code = $request->input('customer_ro_code');
            $customer_mangement->Customer_Code = 'CUS'.$result;
            $customer_mangement->Phone_Number = $request->input('Phone_Number');
            $customer_mangement->Mobile = $request->input('mobile_no');
            $customer_mangement->CustomerDeposit = $request->input('CustomerDeposit');
            $customer_mangement->Email = $request->input('Email');
            $customer_mangement->company_name = $request->input('company_name');
            $customer_mangement->Customer_Name = $request->input('coustomer_name');
            $customer_mangement->gender = $request->input('gender');
            $customer_mangement->address_one = $request->input('address_one');
            $customer_mangement->vat = $request->input('vat');
            $customer_mangement->Security_Deposit = $request->input('Security_Deposit');
            $customer_mangement->industry_vertical = $request->input('industry_vertical');
            $customer_mangement->Department = $request->input('Department');
            $customer_mangement->country =101;
            $customer_mangement->city = $request->input('city');

            if (!empty($request->input('aadhaar_no'))) {
                $customer_mangement->Aadhaar_no = $request->input('aadhaar_no');
            
            }
            if (!empty( $request->hasFile('aadhaar_img'))) {
                //dd('hii');
              $customer_mangement->Aadhaar_img = $aadhaar_img;
            }
           
            $customer_mangement->state = $request->input('state');
            $customer_mangement->gst_no = $request->input('gst_no');
            $customer_mangement->address_two = $request->input('address_two');
            $customer_mangement->address_three = $request->input('address_three');
            $customer_mangement->pin_no = $request->input('pin_no');
            $customer_mangement->pan_no = $request->input('pan_no');
            $customer_mangement->gst_no = $request->input('gst_no');
            $customer_mangement->save();
           
            
            $neuser=User::where('email',$customer_mangement->Email)->first();

            
            if($neuser==null){

                $user = new User();
                $user->name=$customer_mangement->Customer_Name;
                $user->email=$customer_mangement->Email;
                $user->username=$customer_mangement->Email;
                $user->user_type=4;
                $user->password=bcrypt($request->input('password'));
                $user->mobile=$request->input('mobile_no');
                $user->save();
               // dd('hellodfsdfsd');
            }

            $Credit_limit = new Credit_limit();
            $Credit_limit->Ro_code=$customer_mangement->RO_code ; 
            $Credit_limit->Customer_Code=$customer_mangement->Customer_Code;
            $Credit_limit->Limit_Value=$request->input('Limit_Value');
            $Credit_limit->no_of_days=$request->input('no_of_days');

            if($request->input('handlingfee') != null)
                $Credit_limit->handling_fee=$request->input('handlingfee');

            if($request->input('Discount') != null)
                $Credit_limit->Discount=$request->input('Discount');

            if($request->input('handlinglubefee') != null)
                $Credit_limit->handlinglubefee=$request->input('handlinglubefee');

            if($request->input('Discountlube') != null)
                $Credit_limit->Discountlube=$request->input('Discountlube'); 

                 $Credit_limit->store_billing_mode=$request->input('billing_mode');  

            $Credit_limit->save();

             $request->session()->flash('success','Added Successfully!!');
            }  catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');
                
            }

            //dd('hello');
            
           


 

            

            return redirect('Customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
        $customer_mangement_data = DB::table('tbl_customer_master')->where('id', '=', $id)->first();
         $datas = RoMaster::where('is_active',1);
         if(Auth::user()->user_type!=1 && $rocode!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();

         $countries_list =DB::table('countries')->get();
         $state_list =DB::table('states')->where('country_id',$customer_mangement_data->country)->get();
         $city_list =DB::table('cities')->where('state_id',$customer_mangement_data->state)->get();
        return view('backend.customer_show_page', compact(['customer_mangement_data', 'datas','countries_list','state_list','city_list']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                    return redirect('Dashboard');
            }
            $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }

         $customer_mangement_data = DB::table('tbl_customer_master')->where('id', '=', $id)->first();

         $datas = RoMaster::where('is_active',1);

          if(Auth::user()->user_type!=1 && $rocode!='')
                $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

              $datas=$datas->get();

            $Industry=IndustryDepartment::where('type','Industry Vertical')->get();
            $Department=IndustryDepartment::where('type','Department')->get();
         $countries_list =DB::table('countries')->get();

         $state_list =DB::table('states')->where('country_id',$customer_mangement_data->country)->get();
         $city_list =DB::table('cities')->where('state_id',$customer_mangement_data->state)->get();
        return view('backend.customer_edit_page', compact(['customer_mangement_data', 'datas','countries_list','state_list','city_list','Industry','Department']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {//dd($request->all());
         try {


             $customer_mangement = RoCustomertManagement::find($id);

              $neuser=User::where('user_type','<>',4)->where('mobile',$request->input('mobile_no'))->get();

            if($neuser->count()>0){
               $request->session()->flash('success','Mobile No. Already Exist !!');
               return back(); 
            }
            if($request->hasFile('aadhaar_img'))
             {
                $file = Input::file('aadhaar_img');
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $filename = $file->getClientOriginalName();

                $name = $timestamp . '-' . $filename;
                $aadhaar_img = 'images/' . $name;

                $file->move('images/', $name);
             }

            $customer_mangement->RO_code = $request->input('customer_ro_code');
            //$customer_mangement->Customer_Code = 'CUS'.$result;
            $email=$customer_mangement->Email;
            $customer_mangement->Phone_Number = $request->input('Phone_Number');
            $customer_mangement->Mobile = $request->input('mobile_no');
            $customer_mangement->CustomerDeposit = $request->input('CustomerDeposit');
            $customer_mangement->Email= $request->input('email');
            $customer_mangement->Customer_Name = $request->input('coustomer_name');
            $customer_mangement->gender = $request->input('gender');
            $customer_mangement->company_name = $request->input('company_name');
            $customer_mangement->address_one = $request->input('address_one');
            $customer_mangement->vat = $request->input('vat');
            $customer_mangement->Security_Deposit = $request->input('Security_Deposit');
            $customer_mangement->industry_vertical = $request->input('industry_vertical');
            $customer_mangement->Department = $request->input('Department');
           if (!empty($request->input('aadhaar_no'))) {
                $customer_mangement->Aadhaar_no = $request->input('aadhaar_no');
            
            }
            if (!empty( $request->hasFile('aadhaar_img'))) {
                //dd('hii');
              $customer_mangement->Aadhaar_img = $aadhaar_img;
            }
            $customer_mangement->city = $request->input('city');
            $customer_mangement->state = $request->input('state');
            $customer_mangement->address_two = $request->input('address_two');
            $customer_mangement->address_three = $request->input('address_three');
            $customer_mangement->pin_no = $request->input('pin_no');
            $customer_mangement->gst_no = $request->input('gst_no');
           
           
            $customer_mangement->save();

             $neuser=User::where('email',$email)->first();
             
            if($neuser!=null){

                $user =  User::find($neuser->id);
                $user->name=$customer_mangement->Customer_Name;
                $user->email=$customer_mangement->Email;
                $user->mobile=$customer_mangement->Mobile;
                $user->username=$customer_mangement->Email;
                $user->save();
               // dd('hellodfsdfsd');
            }
          
            $request->session()->flash('success','Data Updated  Successfully!!'); 
          }  catch(\Illuminate\Database\QueryException $e){
                
              $request->session()->flash('success','Something wrong!!');
        
            }
            
            return redirect('Customer');
    }
    function active(Request $request,$id, $actt)
        {
            if($actt == 0)
            {
                $ac = RoCustomertManagement::where('id',$id)->first();
                
                $customerData=new customerData();
                $customerData->customer_id=$ac->id;
                $customerData->mobile=$ac->Mobile;
                $customerData->imi_no=$ac->IMEI_No;
                
                if($customerData->mobile!=null && $customerData->mobile!='')
                    $customerData->save();


                $ac = RoCustomertManagement::where('id',$id)->update(['is_active' => $actt,'IMEI_No' =>'']);
                $customer_code = RoCustomertManagement::where('id',$id)->pluck('Customer_Code')->first();
                $customer_email = RoCustomertManagement::where('id',$id)->pluck('Email')->first();
               
                //$this->allCustomerDataDeactive($customer_code,$customer_email);
                
            }

            else
            {
                 $ac = RoCustomertManagement::where('id',$id)->update(['is_active' => $actt]);
                 $customer_email = RoCustomertManagement::where('id',$id)->pluck('Email')->first();
                  //$this->customerUseraActive($customer_email);
     
            }
            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully Please Add Mobile No. !!');

         return back();
             
    }

    public function customerUseraActive($customer_email)
    {
       DB::table('users')->where('email', $customer_email)->update(['is_active' => 1]); 
    }

    public function allCustomerDataDeactive($customer_code,$customer_email)
    {
        DB::table('users')->where('email', $customer_email)->update(['is_active' => 0]);
        DB::table('tbl_customer_vehicle_driver')->where('customer_code', $customer_code)->update(['is_active' => 0]);
        DB::table('tbl_customer_vehicle_master')->where('Customer_code', $customer_code)->update(['is_active' => 0]);
        DB::table('tbl_qrcode_manage')->where('Customer_Code', $customer_code)->update(['is_active' => 0]);
        DB::table('tbl_customer_limit')->where('Customer_Code', $customer_code)->update(['is_active' => 0]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request ,$id)
    {
        try {
             $customer_mangement = RoCustomertManagement::find($id);

        $customer_mangement->delete();
            
        } catch(\Illuminate\Database\QueryException $e)
        {
            $request->session()->flash('success','Something is worong!!');
                 return back();
        }
            
        
       

        return back();
    }

    public function customerSession(Request $request)
    {
        if (Auth::user()->customer!=null) {
            $Customer_Code=Auth::user()->customer->Customer_Code;
            $email =Auth::user()->email;
        }
        $rocode = $request->input('RO_code');
        $customer_newcode = RoCustomertManagement::where('RO_Code',$rocode)->where('Email',$email)->first();
        
        $request->session()->flash('success','Outlet is change');
        session(['ro' => $rocode,'custm'=>$customer_newcode->Customer_Code]);
       
        return back();
        
    }
}
