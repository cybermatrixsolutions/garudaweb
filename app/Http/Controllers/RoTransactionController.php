<?php

namespace App\Http\Controllers;

use App\CustomerLubeRequest;
use Illuminate\Http\Request;
use App\RoMaster;
use App\RoPieceListManagement;
use App\TransactionModel;
use App\RoCustomertManagement;
use App\VechilManage;
use DB;
use Gate;
use Auth;
use App\CustomerTransactionInvoice;
use Carbon\Carbon;
use Log;
use DateTime;
class RoTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        Log::info('RoTransactionController@create  input - '.print_r($request->all(),true));
         if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
       $input=$request->all();
        $customerCode='0';
        $from_date_for_view="";
         $to_date_for_view="";
        if(isset($input['cust_code'])){

          $customerCode=$input['cust_code'];

        }
        if(isset($input['from_date'])){

          $from_date=$input['from_date'];
          $from_date_for_view=$input['from_date'];

        }else{
          $from_date=date('d/m/Y') ; 
        }
        if(isset($input['to_date'])){

          $to_date=$input['to_date'];
          $to_date_for_view=$input['to_date'];

        }else{
          $to_date=date('d/m/Y') ; 
        }

         $from_date = $from_date . '00:00:00';
         $to_date = $to_date . ' 23:59:59';
        
        $fromDate = DateTime::createFromFormat('d/m/Y  H:i:s', $from_date)->format('Y-m-d H:i:s');
        $toDate = DateTime::createFromFormat('d/m/Y  H:i:s', $to_date)->format('Y-m-d H:i:s');
Log::info('RoTransactionController@create fromDate - '.$fromDate.' toDate - ' .$toDate .' customerCode'.$customerCode);


        $RoCustomertManagement=RoCustomertManagement::where('RO_code',$rocode)->get();
         $transaction_data=TransactionModel::join('tbl_price_list_master','tbl_customer_transaction.item_code','=', 'tbl_price_list_master.id')
         ->join('shifts','shifts.id','=', 'tbl_customer_transaction.shift_id')
             ->select('tbl_customer_transaction.*','tbl_price_list_master.Item_Code as Item','shifts.confirm')
             ->where('tbl_customer_transaction.RO_code',$rocode)
             ->where('tbl_customer_transaction.status',0)
             ->where('tbl_customer_transaction.Vehicle_Reg_No','<>','Walkin')
             ->where('tbl_price_list_master.RO_Code',$rocode)
              ->whereBetween('tbl_customer_transaction.trans_date',[$fromDate,$toDate]);
            // ->get();
              if($customerCode!="All"){
                Log::info('RoTransactionController@create fromDate - '.$fromDate.' toDate - ' .$toDate .' customerCode'.$customerCode);
             $transaction_data=$transaction_data->where('tbl_customer_transaction.customer_code',$customerCode);

              }
              $transaction_data=$transaction_data->get();
        //Log::info('RoTransactionController@create  transaction_data'.print_r($transaction_data,true));
        // dd($transaction_data);
        $transaction_list= CustomerTransactionInvoice::join('tbl_ro_master','tbl_customer_transaction_invoice.RO_code','=','tbl_ro_master.RO_code')
        ->join('tbl_customer_master','tbl_customer_transaction_invoice.customer_code', '=' , 'tbl_customer_master.Customer_Code')
        ->where('tbl_customer_transaction_invoice.RO_code',$rocode)
        ->select('tbl_customer_transaction_invoice.*','tbl_ro_master.pump_legal_name','tbl_customer_master.Customer_Name')
        ->orderby('id','desc')
       ;

       //Log::info('RoTransactionController@create  transaction_list'.print_r($transaction_list,true));

        if(Auth::user()->user_type!=1 && $rocode!='')
                $transaction_list=$transaction_list->where('tbl_customer_transaction_invoice.RO_code',$rocode);

              $transaction_list=$transaction_list->get();

              $tbl_customer_data =  DB::table('tbl_customer_master')
              ->where('is_active',1)
              ->where('RO_code',$rocode)
              ->orderBy('company_name', 'asc')
              ->get();


        
         return view('backend.roall_transaction',compact(['tbl_customer_data','transaction_list','transaction_data','RoCustomertManagement','from_date_for_view','to_date_for_view','customerCode']));
    }
     
        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exportCSV(Request $request)
    {
    
        try{  

             $rocode='';
               if (Auth::user()->getRocode!=null) {
                  $rocode=Auth::user()->getRocode->RO_code;
               }

          
             $transaction_list= CustomerTransactionInvoice::join('tbl_ro_master','tbl_customer_transaction_invoice.RO_code','=','tbl_ro_master.RO_code')
             ->join('tbl_customer_master','tbl_customer_transaction_invoice.customer_code', '=' , 'tbl_customer_master.Customer_Code')
             ->select('tbl_customer_transaction_invoice.*','tbl_ro_master.pump_legal_name','tbl_customer_master.Customer_Name');

            if(Auth::user()->user_type!=1 && $rocode!='')
                $transaction_list=$transaction_list->where('tbl_customer_transaction_invoice.RO_code',$rocode);

            $transaction_list=$transaction_list->get();

            
        if(count($transaction_list)>0){
            
            $CsvData=array('Customer Name,Pump Legal Name,Item Code,Item Name,Item Price,Type,Petrol or Diesel Type,Petrol Or Diesel Qty,Transaction Id,Request ID,Transaction Date,Transaction Amount');          
            foreach($transaction_list as $list){
                 $traData='';
               //dd($list->getTrac->getItemName);

                if($list->getCustomerMaster->Customer_Name != 'Walkin'){
                    $traData=$list->getCustomerMaster->Customer_Name;
                 }else{
                    $traData='Walkin';
                 }

                  $traData.=','.$list->pump_legal_name;
                  $traData.=','.$list->getTrac->getItemName->Item_Code;
                  $traData.=','.$list->getTrac->getItemName->Item_Name;
                  $traData.=','.$list->getTrac->item_price;

                  if($list->petrol_or_lube==1){
                    $traData.=',Petrol';
                  }else{
                    $traData.=',Lube';
                  } 

                  $traData.=','.$list->getTrac->petroldiesel_type;
                  $traData.=','.$list->getTrac->petroldiesel_qty;
                  $traData.=','.$list->getTrac->trans_id;
                  $traData.=','.$list->getTrac->Request_id;
                  $trans_date=Carbon::parse($list->getTrac->trans_date)->format('d/m/Y');
                  $traData.=','.$trans_date;
                  $traData.=','.$list->trans_amount;
                  
                 
                $CsvData[]=$traData;
            }
             
            $filename=date('Y-m-d').".csv";
            $file_path=base_path().'/'.$filename;   
            $file = fopen($file_path,"w+");

            foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
            }

            fclose($file);          

            $headers = ['Content-Type' => 'application/csv'];
            return response()->download($file_path,$filename,$headers );

            }

        }
        catch(\Illuminate\Database\QueryException $e)
        {

        $request->session()->flash('success','No Transaction Available');

        }

        return redirect('transaction_list');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
           // $customer_code = TransactionModel::where('trans_id',$id)->pluck('customer_code')->first();

             $rocode='';
               if (Auth::user()->getRocode!=null) {
                  $rocode=Auth::user()->getRocode->RO_code;
               }


         $transaction_data=TransactionModel::join('tbl_price_list_master','tbl_customer_transaction.item_code','=', 'tbl_price_list_master.id')
          ->where('tbl_customer_transaction.RO_code',$rocode)
         ->where('tbl_customer_transaction.trans_id',$id)
         ->select('tbl_customer_transaction.*','tbl_price_list_master.Item_Code as Item')
         ->get();
          
             // $transaction_data=TransactionModel::where('trans_id',$id)->get();
           
  
      /*  $transaction_data=TransactionModel::join('tbl_customer_master', 'tbl_customer_transaction.customer_code', '=', 'tbl_customer_master.Customer_Code')
        ->join('tbl_ro_master', 'tbl_customer_transaction.RO_code', '=', 'tbl_ro_master.RO_code')
        ->join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.Item_Code')
        ->select('tbl_customer_transaction.*','tbl_customer_master.Customer_Name','tbl_ro_master.pump_legal_name','tbl_price_list_master.Item_Name as item_name')->where('tbl_customer_transaction.trans_id',$id)->get();*/
        
     
              
                   
        return view('backend.customer_transaction_view_page',compact('transaction_data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerLubeRequest $customerLubeRequest)
    {
        
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerLubeRequest $customerLubeRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        try {
            $personal_mangement = CustomerLubeRequest::find($id);

        $personal_mangement->delete();
        $request->session()->flash('success','Data has been deleted Successfully!!');
        }  catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
         

        return back();
    }
}
