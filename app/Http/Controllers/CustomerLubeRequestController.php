<?php

namespace App\Http\Controllers;

use App\CustomerLubeRequest;
use Illuminate\Http\Request;
use App\RoMaster;
use App\OtpModel;
use App\CustomerDriverManager;
use App\RoPieceListManagement;
use App\Credit_limit;
use App\TransactionModel;
use App\AddPetrolDieselModel;
use Session;
use DB;
use Gate;
use Auth;
use App\CityModel;
use Log;

class CustomerLubeRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           
      if (Gate::allows('ro-admin',Auth::user()) || Gate::allows('admin',Auth::user())) {
            return redirect('Dashboard');
        }
      
        $Customer_Code='';
        if (Auth::user()->customer!=null) {
            $Customer_Code=Auth::user()->customer->Customer_Code;
        }
        $Customer_Mobile='';
        if (Auth::user()->customer!=null) {
          $Customer_Mobile=Auth::user()->customer->Mobile;
        }
         $Customer_Name='';
        if (Auth::user()->customer!=null) {
          $Customer_Name=Auth::user()->customer->Customer_Name;
        }


           Log::info('CustomerLubeRequestController@cretae  gate Customer_Code - '.$Customer_Code);
           Log::info('CustomerLubeRequestController@cretae  gate Customer_Mobile - '.$Customer_Mobile);
           Log::info('CustomerLubeRequestController@cretae  gate Customer_Name - '.$Customer_Name);
        $rocode = Session::get('ro');
        $luberequest_Mobile_no = CustomerDriverManager::where('customer_code',$Customer_Code)->where('is_active',1)->get();
        $datas= RoMaster::where('is_active',1);

        if(Auth::user()->user_type!=1 && $Customer_Code!='')
            $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

          $datas=$datas->get();
          $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
          $getpric=  $getpric->pluck('item_id')->toArray();
          $item_id = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
           ->where('tbl_stock_item_group.Group_Name','!=', 'FUEL')->whereIn('tbl_price_list_master.id',$getpric)->select('tbl_price_list_master.*')->orderby('id','desc');

        if(Auth::user()->user_type!=1 && $Customer_Code!='')
            $item_id=$item_id->where('tbl_price_list_master.RO_Code',$rocode);

        $item_id=$item_id->get();
        return view('backend.lubRequest',compact('item_id','datas','luberequest_Mobile_no','Customer_Mobile','Customer_Name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('ro-admin',Auth::user()) || Gate::allows('admin',Auth::user())) {
            return redirect('Dashboard');
        }
      
        $Customer_Code='';
        if (Auth::user()->customer!=null) {
            $Customer_Code=Auth::user()->customer->Customer_Code;
        }
        $rocode = Session::get('ro');

        $datas= RoMaster::where('is_active',1);

        if(Auth::user()->user_type!=1 && $Customer_Code!='')
            $datas=$datas->where('tbl_ro_master.RO_code',$rocode);

          $datas=$datas->get();
          $getpric = DB::table('tbl_item_price_list')->select('item_id')->distinct()->get();
          $getpric=  $getpric->pluck('item_id')->toArray();
          $item_id = DB::table('tbl_price_list_master')->join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
           ->where('tbl_stock_item_group.Group_Name','!=', 'FUEL')->whereIn('tbl_price_list_master.id',$getpric)->select('tbl_price_list_master.*');

        if(Auth::user()->user_type!=1 && $Customer_Code!='')
            $item_id=$item_id->where('tbl_price_list_master.RO_Code',$rocode);

        $item_id=$item_id->get();
        // $item_id= RoPieceListManagement::All();
          $lube_list= CustomerLubeRequest::join('tbl_price_list_master', 'tbl_customer_request_lube.item_id', '=', 'tbl_price_list_master.id')
              ->join('tbl_ro_master', 'tbl_customer_request_lube.RO_code', '=', 'tbl_ro_master.RO_code')
              ->join('tbl_customer_master', 'tbl_customer_request_lube.customer_code', '=', 'tbl_customer_master.Customer_Code')
        ->select('tbl_customer_request_lube.*', 'tbl_price_list_master.Item_Name','tbl_ro_master.pump_legal_name','tbl_customer_master.Customer_Name');
        if(Auth::user()->user_type!=1 && $Customer_Code!='')
            $lube_list=$lube_list->where('tbl_customer_request_lube.RO_code',$rocode)->where('tbl_customer_request_lube.Customer_Code',$Customer_Code);

        $lube_list=$lube_list->get();
     

        // $designation=DB::table('tbl_designation_master')->get();
         // return view('backend.lube_page',compact('designation'));
         return view('backend.lube_page',compact(['datas','item_id','lube_list']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $vehicle_no= $request->input('Vehicle_Reg_No');
         if($vehicle_no!=null){
               $AddPetrolDieselModel = CustomerLubeRequest::where('Vehicle_Reg_No',$vehicle_no)->where('Execution_date',Null)->get();

             if($AddPetrolDieselModel!=null && $AddPetrolDieselModel->count()>0 ){
                
                 $request->session()->flash('success','Sorry, This vehicle request already exists');
                 return back();
             }
         }

         $rquestid="RQ".mt_rand();
         $item_id = $request->input('item_id');
         $rocode = $request->input('RO_code');
         $customer_code =  $request->input('customer_code');
         $vehiclenos = $request->input('Vehicle_Reg_No');
         $mobileno = $request->input('mobile');
         $petroldiesel_qty = $request->input('petroldiesel_qty');
         $amountTrajection=0;
         $Credit_limitAmount=0;
         
        $roInfo= RoMaster::where('RO_code',$rocode)->get()->first();
        $cityInfo = CityModel::where('id',$roInfo->city)->get()->first();

        //echo $roInfo->pump_legal_name.", ".$cityInfo->name;
        //dd($cityInfo);
        //dd($request->all());
            
           if($vehiclenos==null || $mobileno==null || $petroldiesel_qty==null || count($petroldiesel_qty)==0){
             $request->session()->flash('success','Something wrong ');
            return back();
           }


           //check creadit limit

            $Credit_limit=Credit_limit::where('Customer_Code',$customer_code)->first();

             if($Credit_limit!=null)
             $Credit_limitAmount=$Credit_limit->Limit_Value;

            


             $transactionModels =   TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
                ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
                ->where('tbl_customer_transaction.customer_code',$customer_code)
                ->where('tbl_customer_transaction.status','<>',2)
                 ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode')
                ->get();

                
                foreach ($transactionModels as $transactionModel) {
                  
                      $amountTrajection=$amountTrajection+($transactionModel->item_price*$transactionModel->petroldiesel_qty);
                   
                }

               $pemdingFuelRes=AddPetrolDieselModel::where('customer_code',$customer_code)->where('Execution_date',null)->get();
                
                foreach ($pemdingFuelRes as $pemdingFuelRe) {
                    
                     
                    if($pemdingFuelRe->priceLists!=null){
                      
                        if($pemdingFuelRe->Request_Type!='Full Tank'){

                            $amountTrajection=($pemdingFuelRe->priceLists->price*$pemdingFuelRe->quantity)+$amountTrajection;
                        }else{
                            
                            $amountTrajection=($pemdingFuelRe->priceLists->price*$pemdingFuelRe->getVehicle->capacity)+$amountTrajection;
                        }
                    }
                     
                   

                }
              

                $CustomerLubeRequests=CustomerLubeRequest::where('customer_code',$customer_code)->where('Execution_date',null)->get();

                foreach ($CustomerLubeRequests as $CustomerLubeRequest) {
                    
                    if($CustomerLubeRequest->priceLists!=null)
                     $amountTrajection=($CustomerLubeRequest->priceLists->price*$CustomerLubeRequest->quantity)+$amountTrajection;
                }

                 $z=0;
                 foreach ($item_id as $item_ids) {

                        $RoPieceListManagement=RoPieceListManagement::find($item_ids);
                        if($RoPieceListManagement!=null && $RoPieceListManagement->price!=null)
                            $amountTrajection=($RoPieceListManagement->price->price*$petroldiesel_qty[$z])+$amountTrajection;
                          
                       $z++;
                     }

                if($amountTrajection>$Credit_limitAmount){
                     $request->session()->flash('success','Credit Limit Exhausted!!');
                     return back();
                }
        
             //check creadit limit

          

            $length = 5;
             $randomBytes = openssl_random_pseudo_bytes($length);
             $characters = '0123456789';
             $charactersLength = strlen($characters);
             $result = '';
              $k=0;
             for ($i = 0; $i < $length; $i++)
             $result .= $characters[ord($randomBytes[$i]) % $charactersLength];
             foreach ($item_id as $item_ids) {

             $lube_request = new CustomerLubeRequest();
             $lube_request->Ro_code = $rocode;
             $lube_request->item_id = $item_ids;
             $lube_request->customer_code =  $customer_code;           
             $lube_request->Vehicle_Reg_No = $vehiclenos;
             $lube_request->current_driver_mobile =  $mobileno;
             $lube_request->quantity =  $petroldiesel_qty[$k]; 
             $lube_request->request_id =$rquestid;
             $lube_request->save(); 

              $k++;

    }
            $otppin = rand(1000, 9999);
             $OtpModel = new OtpModel();
             $otp=$otppin; 
             $OtpModel->mobile= $mobileno; 
             $OtpModel->OTP=$otp; 
             $OtpModel->request_id=$rquestid;
             $OtpModel->save();

            //send sms in mobile
            //$message="Your One Time Password is: ".$OtpModel->OTP." for request #".$OtpModel->request_id.".Please do not share OTP to anyone.";
            $message ="Your OTP is ".$OtpModel->OTP." for #".$OtpModel->request_id." dtd. ".date('d/m/Y')." - Others - ".$vehicle_no." at ".$roInfo->pump_legal_name.", ".$cityInfo->name;
            $this->sendsms($message, $lube_request->current_driver_mobile);

             $request->session()->flash('success','Create Request  Successfully and Send OTP !!');
       return redirect('lube_request');
}

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerLubeRequest $customerLubeRequest,$id)
    {
        if (Gate::allows('ro-admin',Auth::user()) || Gate::allows('admin',Auth::user())) {
            return redirect('Dashboard');
        }
        $Customer_Code='';
        if (Auth::user()->customer!=null) {
            $Customer_Code=Auth::user()->customer->Customer_Code;
        }
        $datas= RoMaster::where('is_active',1);

        if(Auth::user()->user_type!=1 && $Customer_Code!='')
            $datas=$datas->where('tbl_ro_master.RO_code',Auth::user()->customer->RO_code);

        $datas=$datas->get();

        $item_id = RoPieceListManagement::join('tbl_stock_item_group','tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
            ->where('tbl_stock_item_group.Group_Name','!=', 'FUEL')->select('tbl_price_list_master.*');

        if(Auth::user()->user_type!=1 && $Customer_Code!='')
            $item_id=$item_id->where('tbl_price_list_master.RO_Code',Auth::user()->customer->RO_code);

        $item_id=$item_id->get();
         $lube_data = CustomerLubeRequest::where('id', '=', $id)->first();
        
        
        return view('backend.lube_view_page', compact(['lube_data','datas','item_id']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerLubeRequest $customerLubeRequest)
    {
        
    }
    function active(Request $request,$id, $actt)
    {
        
        $ac = CustomerLubeRequest::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerLubeRequest $customerLubeRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerLubeRequest  $customerLubeRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        try {
            $personal_mangement = CustomerLubeRequest::find($id);
          
            $OtpModel = OtpModel::where('request_id',$personal_mangement->request_id);
            $OtpModel->delete();
            $personal_mangement->delete();

        $request->session()->flash('success','Data has been deleted Successfully!!');
        }  catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
         

        return back();
    }

    function sendsms($message,$mobile) { 
        $username="GARRUDA";
        $password ="9811078782";
        $number=$mobile;
        $sender="GARRUD";
        $message=$message;
        $url="login.bulksmsgateway.in/sendmessage.php?user=".urlencode($username)."&password=".urlencode($password)."&mobile=".urlencode($number)."&sender=".urlencode($sender)."&message=".urlencode($message)."&type=".urlencode('3');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch); 
        return $curl_scraped_page;

        }
}
