<?php

namespace App\Http\Controllers;

use App\PedestalMaster;
use Illuminate\Http\Request;

class PedestalMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PedestalMaster  $pedestalMaster
     * @return \Illuminate\Http\Response
     */
    public function show(PedestalMaster $pedestalMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PedestalMaster  $pedestalMaster
     * @return \Illuminate\Http\Response
     */
    public function edit(PedestalMaster $pedestalMaster)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PedestalMaster  $pedestalMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PedestalMaster $pedestalMaster)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PedestalMaster  $pedestalMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(PedestalMaster $pedestalMaster)
    {
        //
    }
}
