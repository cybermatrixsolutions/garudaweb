<?php

namespace App\Http\Controllers;

//use App\CityController;
use Illuminate\Http\Request;
use App\PaymentModel;
use App\MasterPaymentmode;
use DB;
use App\User;
use Auth;
class PaymentModeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
          $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
        $PaymentModel = PaymentModel::where('ro_code',$rocode)->orderby('order_number','ASC')->orderby('name','ASC')->get();
       // $checkpayment = PaymentModel::where('ro_code',$rocode)->first();

        if($PaymentModel->count()>0)
          $MasterPayment = MasterPaymentmode::whereNotIn('name',$PaymentModel->pluck('name')->toArray())->orderby('id','desc')->get();
        else
          $MasterPayment = MasterPaymentmode::orderby('id','desc')->get();



        $checkmaster = MasterPaymentmode::first();
         //  dd($StateManagementModel);
        return view('backend.paymentmood',compact('PaymentModel','MasterPayment','checkmaster','checkpayment')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create(Request $request)
    {
       try{
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
         
          $paymentmode=$request->input('name');  
          // $tally_config_name=$request->input('tally_config_name');  
          $validatedData = $request->validate([
          'name' => 'required|unique:tpl_payment_mode',

         ]);

          $PaymentModel = new PaymentModel();
          $PaymentModel->name =$paymentmode;
          // $PaymentModel->tally_config_name =$tally_config_name;
          $PaymentModel->ro_code=$rocode;
          $PaymentModel->mode_type=$request->mode_type;
          $PaymentModel->save();
          $request->session()->flash('success','Added Successfully!!'); 
         }
         catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
 
    }
    public function masterpaymentsave(Request $request){

      try{
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
         
          $paymentmode=$request->input('names');  
           
           
          if(!isset($paymentmode) || $paymentmode==null || count($paymentmode)==0){
      			$request->session()->flash('success','Please select payment mode');
      			return back();
      		  }

		  $paymentmodes=MasterPaymentmode::whereIn('name',$paymentmode)->get();
    
		  foreach ($paymentmodes as $key => $value) {
			
			  $PaymentModel = new PaymentModel();
			  $PaymentModel->name =$value->name;
			  $PaymentModel->ro_code=$rocode;
        $PaymentModel->mode_type=$value->mode_type;
			  $PaymentModel->save();
			  $request->session()->flash('success','Added Successfully!!'); 
			}
		 }
         catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
    }

   public function activePaymentMode(Request $request, $id, $act){
     $paymentMode=PaymentModel::find($id);

     $paymentMode->IsActive=$act;
     $paymentMode->save();

     if($act==1)
      $request->session()->flash('success','Activated Successfully');
    else
      $request->session()->flash('success','DeActivated Successfully');

    return back();
   }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeupdate(Request $request , $id)
    {
       try{
          $paytypecreate=$request->input('name');  
          $validatedData = $request->validate([
          'name' => 'required|unique:tpl_payment_mode,id,'.$id,
         ]);
          PaymentModel::where('id', $id)->update(['name' =>$paytypecreate]);
          $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
      

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function show(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function editmethod(Request $request)
    {
         $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
        $PaymentModel = PaymentModel::where('ro_code',$rocode)->orderby('order_number','ASC')->orderby('name','ASC')->get();
       // $checkpayment = PaymentModel::where('ro_code',$rocode)->first();

        if($PaymentModel->count()>0)
          $MasterPayment = MasterPaymentmode::whereNotIn('name',$PaymentModel->pluck('name')->toArray())->orderby('id','desc')->get();
        else
          $MasterPayment = MasterPaymentmode::orderby('id','desc')->get();



        $checkmaster = MasterPaymentmode::first();
         //  dd($StateManagementModel);
        return view('backend.paymentmoods',compact('PaymentModel','MasterPayment','checkmaster','checkpayment')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function updatemethod(Request $request)
    {
          try{
         $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }
          $paytypecreate=$request->input('name');  
          $tally_config_name=$request->input('tally_config_name');  
          $id=$request->input('id');  

           foreach ($tally_config_name as $key => $tally_config_names) {
          
           
         $paymentss = PaymentModel::where('ro_code', $rocode)->where('id',$id[$key])->update(['tally_config_name' =>$tally_config_names]);

          }


           
          $request->session()->flash('success','Data Updated  Successfully!!'); 
      }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
          return back();
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContributionModel $contributionModel)
    {
        //
    }
	
	/**
     * Update payment mode order
     *
     * 
     */
	public function updatePaymentModeOrder(Request $request){
		
		$postData = $request->all();
		
		$rocode='';
		if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
		}
		
		//dd($postData);
		
		if($rocode!="" && isset($postData['item']) && count($postData['item']) > 0){
			$i = 0;
			foreach($postData['item'] as $id) {
				//$sql ="UPDATE tpl_payment_mode SET order_number=".$i." WHERE ro_code='".$rocode."' AND id=".$data;
				DB::table('tpl_payment_mode')->where('ro_code',$rocode)->where('id',$id)->update(['order_number'=>$i]);
				
				$i++;
			}
			
		}
		return response()->json(array("success"=>true,"item"=>$postData));
	}
}
