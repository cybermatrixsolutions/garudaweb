<?php

namespace App\Http\Controllers;

use Auth;
use App\Shift;
use App\PedestalMaster;
use App\RoPersonalManagement;
use App\nozzelsReadingModel;
use App\TransactionModel;
use App\ShiftPedestal;
use App\PaymentModel;
use App\ShiftSettelementModel;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\TableTankMasterModel;
use App\TankReading;
use App\ShiftType;
use App\PaymentmodesattlementModel;
use Illuminate\Support\Facades\Validator;
use Log;

class ShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
     
        //
        $rocode='';
        $id=0;
        $person=[];
        $Padestal=[];
		$tank = [];
        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $rocode=Auth::user()->getPersonel->RO_Code;

        }else{
            
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }
        }
       
        $allshifts=Shift::where('ro_code',$rocode)->where('is_active',1)->get();
        $openshift=Shift::where('ro_code',$rocode)->where('closer_date',null)->get();
       
        foreach ($openshift as $value) {

          
          $shift = ShiftPedestal::where('shift_id',$value->id)->first();
          if (!empty($shift)) {

            $list[] = ShiftPedestal::where('shift_id',$value->id)->first();
           
          }

        }
          

        $Shift=Shift::where('ro_code',$rocode)->where('shift_manager',$id)->where('is_active',1)->first();

        $salesmanger=[];

        if($allshifts!=null){
            foreach ($allshifts as $allshift) {
                array_push($salesmanger,$allshift->shift_manager);
                
                if($allshift!=null && $allshift->getPersonnel!=null)
                $person=array_merge($person,$allshift->getPersonnel->pluck('id')->toArray());

                if($allshift!=null && $allshift->getPadestal!=null)
                $Padestal=array_merge($Padestal,$allshift->getPadestal->pluck('id')->toArray());
			
				if($allshift!=null && $allshift->getTanks!=null)
                $tank=array_merge($tank,$allshift->getTanks->pluck('tank_id')->toArray());
            }
            
        }

        $ShiftType_old = array('1'=>'Fuel','2'=>'Other','3'=>'Fuel & Other(Both)');

      $Shifttype=ShiftType::where('RO_Code',$rocode)->get();
		
		
        $personals=PedestalMaster::where('RO_code',$rocode)->where('is_Active',1)->whereNotIn('id',$Padestal)->get();
        $per=array_merge($person,$salesmanger);
        $pedestals=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->whereIn('Designation',[30,29])->whereNotIn('id',$per)->get();

        $shiftmanager=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->where('Designation',29)->whereNotIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();
		
		$tanks=TableTankMasterModel::where('RO_code',$rocode)->where('is_active',1)->whereNotIn('id',$tank)->get();
		
        
       return view('backend.shiftmanagement.create',compact('pedestals','openshift','personals','Shift','shiftmanager','shifts','tanks','Shifttype','ShiftType_old'));
    }
        
    //     $Shift=Shift::where('shift_manager',$id)->where('ro_code',$RO_Code)->where('is_active',1)->first();
        

    //     $personals=PedestalMaster::where('RO_code',$RO_Code)->get();
    //     $pedestals=RoPersonalManagement::where('RO_Code',$RO_Code)->where('Designation',34)->get();

    //    return view('backend.shiftmanagement.create',compact('pedestals','personals','Shift'));
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function shiftCloder(Request $request)
    {
          //
        $id=0;
        $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
           if (Auth::user()->getRocode!=null) {
              $RO_Code=Auth::user()->getRocode->RO_code;
           }
        } 

        $shift_manager=$request->input('shift_manager');

        if(isset($shift_manager) && $shift_manager!='')
            $id=$shift_manager;

        $allshifts=Shift::where('ro_code',$RO_Code)->where('is_active',1)->get();

        $salesmanger=[];

        if($allshifts!=null)
          foreach ($allshifts as $allshift) {
               array_push($salesmanger,$allshift->shift_manager);
           }
                    
              

        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('is_Active',1)->where('Designation',29)->whereIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();
         
        $Shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',1)->first();
    
       return view('backend.shiftmanagement.closer1',compact('Shift','shiftmanager','id'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function shiftCloderStore(Request $request)
    {   
       
       // try{
        $id=0;

        if (Auth::user()->getPersonel!=null) {

          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }
        }  
        
        $shift_manager=$request->input('shift_manager');

        if(isset($shift_manager) && $shift_manager!='')


            $id=$shift_manager;

          $cshift=$request->input('Shift');
        $padestal=$request->input('padestal');
        if (!empty($padestal)) 
        {
         
        
          $Reading_by=$request->input('Reading_by');
          
          $padestals= explode(',',$padestal);
          $ro_code=$request->input('ro_code');

          // Getting tanks reading info
          
          $tankArray = $request->input('tank');
          $capacityArray = $request->input('capacity');
          $unitMeasureArray = $request->input('unit_measure');
          $fuelTypeArray = $request->input('fuel_type');
          $readingCMArray = $request->input('reading_cm');
          $readingArray = $request->input('reading');
          $sumvalueArray = $request->input('sumvalue');
              
  		
          foreach ($padestals as $padesno) {
            if(trim($padesno)!=''){
                $Nozzle_Nos=$request->input($padesno.'_Nozzle_No');
                // dd($padesno);
                foreach ($Nozzle_Nos as $Nozzle_No) {

                  $price=$request->input($padesno.'_price_'.$Nozzle_No);
                  $nozzle_start=$request->input($padesno.'_Nozzle_End_'.$Nozzle_No);
                  $test=$request->input($padesno.'_test_'.$Nozzle_No);
                  $reading=$request->input($padesno.'_reading_'.$Nozzle_No);

              
                  
                  nozzelsReadingModel::where('RO_code',$ro_code)->where('Nozzle_No',$Nozzle_No)->where('Nozzle_End',null)->update(['Nozzle_End' =>$nozzle_start,'shift_id'=>$cshift,'test'=>$test,'reading'=>$reading,'price'=>$price]);
                  
                  // dd($price);
                  $nozzelsReading = new nozzelsReadingModel();
                  $nozzelsReading->RO_code=$ro_code;
                  /*$nozzelsReading->test=$test;
                  $nozzelsReading->reading=$reading;*/
                  $nozzelsReading->Nozzle_No=trim($Nozzle_No);
                  $nozzelsReading->Nozzle_Start=$nozzle_start;
                  $nozzelsReading->Reading_by=$Reading_by;
                  $nozzelsReading->save();

                }

            }
              
          }
  		
      		// Inserting Tank reading values
      		if(isset($tankArray) && is_array($tankArray) && count($tankArray)>0){
          
            for($i=0;$i<count($tankArray);$i++){

               $totalesInwards=DB::table('tank_tankinwart')->where('tank_id',$tankArray[$i])->where('status',1)->get();
               $TankReading=TankReading::where('Tank_code',$tankArray[$i])->latest()->first();
               
               $tank_stack=0;
               $salesvalue=0;
               if($TankReading!=null){
                  $tank_stack=$TankReading->value;
               }

               foreach ($totalesInwards as $totalesInward) {
                 $tank_stack=$totalesInward->value+$tank_stack;
               }
              // dd($totalesInwards);
              
               if($totalesInwards->count()>0)
               {
               DB::table('tank_tankinwart')->where('tank_id',$tankArray[$i])->update(['status'=>0]);

               $salesvalue=$tank_stack-$sumvalueArray[$i];

              $modelTankReading = new TankReading();
              $modelTankReading->shift_id=$cshift;
              $modelTankReading->Ro_code=$ro_code;
              $modelTankReading->Tank_code=$tankArray[$i];
              $modelTankReading->fuel_type=$fuelTypeArray[$i];
              $modelTankReading->capacity=$capacityArray[$i];
              $modelTankReading->unit_measure=$unitMeasureArray[$i];
              $modelTankReading->Reading=$readingCMArray[$i];
              $modelTankReading->dip_mm=$readingArray[$i];
              $modelTankReading->tank_stack=$tank_stack;
              $modelTankReading->sale_value=$salesvalue;
              $modelTankReading->is_active=1;
              $modelTankReading->value=$sumvalueArray[$i];
              $modelTankReading->reading_date  = date("Y-m-d");
              $modelTankReading->save();
            } 

            }

          }
        }  
        

        $ldate = $request->input('closer_date');

        $data_created = str_replace('/','-',str_replace('AM','',str_replace('PM','', $ldate)));

                $closer_date = date('Y-m-d ', strtotime(trim($data_created)));
				
			$allshifts=Shift::where('id',$cshift)->first();

 $Shifttype=ShiftType::where('id',$allshifts->shift_type)->first();

$closer_date = date('Y-m-d H:i:s', strtotime("$closer_date $Shifttype->end_time"));


// dd( $closer_date);



        Shift::where('id',$cshift)->update(['closer_date'=>$closer_date,'is_active' =>0]);

        $request->session()->flash('success','Shift Successfully Close ');
        return redirect()->route('shiftAllocation');

      //   }catch(\Illuminate\Database\QueryException $e){
                
      //     $request->session()->flash('success','Something wrong!!');
      // }
        return back();
       
    }



    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getShiftOfManager(Request $request,$id)
    {
        $id=0;
        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }
        }

       $Shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('status',0)->get();
       $data=[];
       $Shift->pluck('closer_date','id')->toArray();
       foreach ($Shift as $value) {
           $data[$value->id]=date('d/m/Y',strtotime($value->closer_date));
       }

       return $data;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  

		// dd($request->all());
        try{
         $id=0;
         $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;
        }elseif (Auth::user()->getRocode!=null) {

              $RO_Code=Auth::user()->getRocode->RO_code;
          }
         
        $pes=$request->input('Padastal');
        $type_shift=$request->input('type');
        $data_created = $request->input('created_at');
        $shift_type=$request->input('shift_type');
        $type = explode(',', $shift_type);
                //$date2 = new Carbon(str_replace('/', '-',$data_created));

                //$created_at =date('Y-m-d H:i:s',strtotime($date2));
        
        $data_created = str_replace('/','-',str_replace('AM','',str_replace('PM','', $data_created)));

                $created_ats = date('Y-m-d', strtotime(trim($data_created)));
        $type2 = $type[0];
        $created_at = date('Y-m-d H:i:s', strtotime("$created_ats $type2"));

        
             
        $shift_type = $type[1];
        

         if (!empty($Shifttype)) {
         
          $shift_type = $request->validate([
          'shift_type' => 'unique:shifts',
         ]);
        }

        $salesman=$request->input('salesman');
        if (!empty($request->input('tank'))&&($type_shift)!=2) {
                $tank=$request->input('tank');
        }
        
// dd($pes, $type_shift, $shift_type);

        $shift_manager=$request->input('shift_manager');
        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;

      $Shift_search=Shift::where('shift_manager',$id)->where('created_at',$created_at)->where('shift_type',$shift_type)->first();

     if(!empty($Shift_search)){

        $request->session()->flash('success','Shift Already Exist!!');
        return back();
      }
// dd($id, $created_at, $shift_type, $Shift_search);
        
        $shift=new Shift();
        $shift->shift_manager=$id;
        $shift->shift_type=$shift_type;
        $shift->type=$type_shift;
        $shift->created_at=$created_at;
        $shift->ro_code=$RO_Code;
        $shift->save();

        $psdata=[];
        $saldata=[];

        $tankdata=[];

        if (!empty($pes) && $type_shift!=2){
            $i=0;
            foreach ($pes as $pe){
               $psdata[$i]=array('shift_id'=>$shift->id,'pedestal_id'=>$pe);
               $i++;
            }
        }

        if (!empty($salesman)) {
            $i=0;
            foreach ($salesman as $salesm){
               $saldata[$i]=array('shift_id'=>$shift->id,'personnel_id'=>$salesm);
               $i++;
            }
        }
            
    		if (!empty($tank) && $type_shift!=2) {
          
    		    $i=0;
            foreach ($tank as $t){
               $tankdata[$i]=array('shift_id'=>$shift->id,'tank_id'=>$t);
               $i++;
            }
        }

        DB::table('shift_pedestal')->insert($psdata);
        DB::table('shift_personnel')->insert($saldata);
        DB::table('shift_tank')->insert($tankdata);
       }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function show(Shift $shift)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request , $ids)
    {
       $rocode='';
        $id=0;
        $person=[];
        $Padestal=[];
        $tank=[];
        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $rocode=Auth::user()->getPersonel->RO_Code;

        }else{
            
               if (Auth::user()->getRocode!=null) {
                  $rocode=Auth::user()->getRocode->RO_code;
               }
        }
         $allshifts=Shift::where('is_active',1)->get();

         $salesmanger=[];
        if($allshifts!=null){
          foreach ($allshifts as $allshift) {
              array_push($salesmanger,$allshift->shift_manager);
              
              if($allshift!=null && $allshift->getPersonnel!=null)
              $person=array_merge($person,$allshift->getPersonnel->pluck('id')->toArray());

              if($allshift!=null && $allshift->getPadestal!=null)
              $Padestal=array_merge($Padestal,$allshift->getPadestal->pluck('id')->toArray());
		  
			if($allshift!=null && $allshift->getTanks!=null)
			$tank=array_merge($tank,$allshift->getTanks->pluck('id')->toArray());
          }
            
        }

        $Shifttype=ShiftType::where('RO_Code',$rocode)->get();
         $ShiftType_old = array('1'=>'Fuel','2'=>'Other','3'=>'Fuel & Other(Both)');

        $shiftmanager=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->where('Designation',29)->whereIn('id',$salesmanger)->get();

        $pedestals=PedestalMaster::where('RO_code',$rocode)->where('is_Active',1)->whereNotIn('id',$Padestal)->get();
        $per=array_merge($person,$salesmanger);

        $personals =RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->whereIn('Designation',[30,29])->whereNotIn('id',$per)->get();
       
        $shiftmanager=RoPersonalManagement::where('RO_Code',$rocode)->where('is_Active',1)->where('Designation',29)->whereNotIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();
		
		$tanks=TableTankMasterModel::where('RO_code',$rocode)->where('is_active',1)->whereNotIn('id',$tank)->get();
         
        $shift = shift::find($ids);
     
      return view('backend.shiftmanagement.editshift',compact('shift','shiftmanager',
        'personals','pedestals','tanks','Shifttype','ShiftType_old'));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

   try{
         // dd($request->all());
         
         $salemans=$request->input('salesman');
         $shift_manager=$request->input('shift_manager');
         $type_shift=$request->input('type');
         $shift_type=$request->input('shift_type');
         $data_created = $request->input('created_at');

         $type = explode(',', $shift_type);
                //$date2 = new Carbon(str_replace('/', '-',$data_created));

                //$created_at =date('Y-m-d H:i:s',strtotime($date2));
        
        $data_created = str_replace('/','-',str_replace('AM','',str_replace('PM','', $data_created)));

                $created_ats = date('Y-m-d', strtotime(trim($data_created)));
        $type2 = $type[0];
        $created_at = date('Y-m-d H:i:s', strtotime("$created_ats $type2"));

        
              
        $shift_type = $type[1];

//         $shift = shift::find($id);
//          if ($shift->shift_type != $shift_type2) {
         
//           $shift_type = Validator::make($shift->shift_type, [
//           'shift_type' => 'unique:shifts',
//          ]);
//         }
// dd($shift_type);

        $pedestals=$request->input('Padastal');

         if (!empty($request->input('tank')) && $type_shift!=2) {
                $tank=$request->input('tank');
        }

        if(count($salemans)>0 || count($pedestals)>0 || count($tanks)>0)
        {

            $shift=Shift::find($id);
            $shift->shift_manager=$shift_manager;
            $shift->created_at=$created_at;
            $shift->type=$type_shift;
            $shift->shift_type=$shift_type;
            $shift->save();

            $shift->getPadestal()->detach();
            $shift->getPersonnel()->detach();
            $shift->getTanks()->detach();

      if (!empty($request->input('Padastal')) && $type_shift!=2) {
            $shift->getPadestal()->attach($pedestals);
      }
      $shift->getPersonnel()->attach($salemans);

			if (!empty($request->input('tank')) && $type_shift!=2) {
            $shift->getTanks()->attach($tank);
			}
            $request->session()->flash('success','Update Successfully !!');

        }else{
          
            $request->session()->flash('success','Something Wrong !!');

        }

       }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return back();
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shift $shift)
    {
        //
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function settlement(Request $request)
    {   


         $id=0;
        $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }

        }

        $Shiftid=0;

        if($request->input('siftname')!='')
            $Shiftid=$request->input('siftname');
        
        $shift_manager=$request->input('shift_manager');

        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;


        $ather=null;
        $fuels=null;
        $pendin=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('fuel',0)->orderBy('created_at', 'desc')->get();
        
        if($Shiftid!=0)
            $Shift=Shift::where('ro_code',$RO_Code)->where('id',$Shiftid)->where('fuel',0)->first();
        else
            $Shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('fuel',0)->first();
          
       

        if($Shift!=null){

            $ather=TransactionModel::where('shift_id',$Shift->id)->where('petrol_or_lube',2)->get();
            $fuels=TransactionModel::where('shift_id',$Shift->id)->where('petrol_or_lube',1)->get();
        }

          $allshifts=Shift::where('ro_code',$RO_Code)->where('is_active',0)->where('fuel',0)->get();
        $salesmanger=[];
// dd($ather);
        if($allshifts!=null)
          foreach ($allshifts as $allshift) {
               array_push($salesmanger,$allshift->shift_manager);
           }
        
        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('Designation',29)->whereIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();

        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->where('IsActive','1')->orderby('order_number','ASC')->orderby('name','ASC')->get();
       
       return view('backend.shiftmanagement.settlement1',compact('Shift','pendin','ather','fuels','PaymentModel','Shiftid','shiftmanager','id'));
    }
    
        /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */


    public function settlementconfirm(Request $request)
    {   

         $id=0;
        $RO_Code='';

        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }

        }





        $Shiftid=0;

        if($request->input('siftname')!='')
                    $Shiftid=$request->input('siftname');
                
        $shift_manager=$request->input('shift_manager');

        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;


        $ather=null;
        $fuels=null;
        $pendin=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('fuel',1)->where('confirm',0)->orderBy('created_at', 'desc')->get();
        
        if($Shiftid!=0)
            $shift=Shift::where('ro_code',$RO_Code)->where('id',$Shiftid)->where('confirm',0)->where('fuel',1)->first();
        else
            $shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('confirm',0)->where('fuel',1)->first();
          
       

        if($shift!=null){

            $ather=TransactionModel::where('shift_id',$shift->id)->where('petrol_or_lube',2)->get();
            $fuels=TransactionModel::where('shift_id',$shift->id)->where('petrol_or_lube',1)->get();
        }



// dd($Shiftid, $shift_manager, $shift);

// Log::info('shift  id---- '.)

         





        $allshifts=Shift::where('ro_code',$RO_Code)->where('fuel',1)->whereNotNull('closer_date')->where('confirm',0)->get();

        $salesmanger=[];
        if($allshifts!=null)
          foreach ($allshifts as $allshift) {
               array_push($salesmanger,$allshift->shift_manager);
           }
        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('Designation',29)->whereIn('id',$salesmanger)->orderBy('Personnel_Name', 'asc')->get();

// dd($Shift);
        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->orderby('order_number','ASC')->orderby('name','ASC')->get();

        

          // Log::info('paymentmode_sattlement_new   ====='.print_r($paymentmode_sattlement_new,true));

          // $Shift=Shift::where('ro_code',$RO_Code)->where('fuel',1)->where('confirm',0)->get();
          // $shift=Shift::find($id);


        
       
       return view('backend.shiftmanagement.settlement_confirm',compact('pendin','Shiftid','shift','PaymentModel','shiftmanager','id'));
    }
    
        /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */    


    public function settlementconfirmstore(Request $request)
    {    

      Log::info('ShiftController@settlementconfirmstore input '.print_r($request->all(),true));
      try{

         $id=0;
         $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
            
               if (Auth::user()->getRocode!=null) {
                  $RO_Code=Auth::user()->getRocode->RO_code;
                 
               }
        }

        $shift_manager=$request->input('shift_manager');
        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;

        $garruda_receipt=$request->input('garruda_receipt');
        $manual_receipt=$request->input('manual_receipt');
        $type=$request->input('type');
        
        $pes=explode(',',$request->input('paymentmode_new'));

        $Diff=$request->input('Diff');
        $shift_id=$request->input('shift_id');
        
        $shift = ShiftSettelementModel::where('shift_id',$shift_id)->first();
       
        $shift->manual_receipt=$manual_receipt;
        // dd($manual_receipt);
        $shift->Diff=$Diff;
   
        $shift->save();


    $delete_payment = DB::table('paymentmode_sattlement')->where('sattlement_id', $shift->id)->delete();

        $psdata=[];

        $i=0;
        foreach ($pes as $pe){
            if(trim($pe)!=''){
                $paymentmode_id=$request->input('paymentmode_'.$pe);
                
                $ref_no=$request->input('ref_nom_'.$pe);

                // if($request->input('payment_'.$pe)!=null)
                    $paymen=$request->input('payment_'.$pe);
                    $garruda_receipt1_=$request->input('garruda_receipt1_'.$pe);
                $manual_receipt1_=$request->input('manual_receipt1_'.$pe);
                // else
                //      $paymen=0;
               
               // if($paymen!=0){
 // dd( $manual_receipt1_);
                $psdata[$i]=array('amount'=>$paymen,'type'=>$shift->type,'paymentmode_id'=>$pe,'sattlement_id'=>$shift->id,'ref_no'=>$ref_no,'garruda_receipt'=>$garruda_receipt1_,'manual_receipt'=>$manual_receipt1_);
                $i++;
                
               // }
            }
            
        }

        DB::table('paymentmode_sattlement')->insert($psdata);

        // if($type==1){
           Shift::where('id',$shift_id)->update(['confirm' =>1]);
           
        // }else{
        //    Shift::where('id',$shift_id)->update(['confirm' =>1]);
        // }

         //return redirect()->route('settlement');
       
    }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return back();

    }


    public function settlementLueb(Request $request)
    {   


        $id=0;
        $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
           $id=Auth::user()->getPersonel->id;
           $RO_Code=Auth::user()->getPersonel->RO_Code;

        }else{
            
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }
        }

        $Shiftid=0;
        if($request->input('siftname')!='')
            $Shiftid=$request->input('siftname');
        
        $shift_manager=$request->input('shift_manager');
        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;

        $creditsale=null;
        $ather=null;
        $fuels=null;
        $pendin=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('lueb',0)->get();
        
        if($Shiftid!=0)
            $Shift=Shift::where('ro_code',$RO_Code)->where('id',$Shiftid)->first();
        else
            $Shift=Shift::where('ro_code',$RO_Code)->where('shift_manager',$id)->where('is_active',0)->where('lueb',0)->first();
          


        if($Shift!=null){
           
            $ather=TransactionModel::where('shift_id',$Shift->id)->where('cust_name','!=','credit')->where('petrol_or_lube',2)->get();

            $creditsale=TransactionModel::where('shift_id',$Shift->id)->where('petrol_or_lube',2)->get();
           
        }

        $shiftmanager=RoPersonalManagement::where('RO_Code',$RO_Code)->where('Designation',29)->orderBy('Personnel_Name', 'asc')->get();
      
        $PaymentModel=PaymentModel::where('ro_code',$RO_Code)->where('IsActive','1')->orderby('order_number','ASC')->orderby('name','ASC')->get();

       
       return view('backend.shiftmanagement.settlementLube',compact('Shift','pendin','ather','PaymentModel','Shiftid','shiftmanager','id','creditsale'));
    }
  

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    
    public function settlementstore(Request $request)
    {    
      try{
        // dd($request->all());

         $id=0;
         $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
          $id=Auth::user()->getPersonel->id;
          $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
            
               if (Auth::user()->getRocode!=null) {
                  $RO_Code=Auth::user()->getRocode->RO_code;
                 
               }
        }

        $shift_manager=$request->input('shift_manager');
        if(isset($shift_manager) && $shift_manager!='')
        $id=$shift_manager;
        $Nozzle_Amount=0;
        $Nozzle_grass=0;
        $Nozzle_test=0;
        $Credit_lube=0;
        
        if(null !==($request->input('Nozzle_Amount')) && $request->input('Nozzle_Amount')!=''){
                $Nozzle_Amount=$request->input('Nozzle_Amount');
                $Nozzle_grass=$request->input('Nozzle_grass');
                $Nozzle_test=$request->input('Nozzle_test');
              }
       
              
        if(null !==($request->input('Credit_lube')) && $request->input('Credit_lube')!=''){      
        $Credit_lube=$request->input('Credit_lube');
              }
        $garruda_receipt=$request->input('garruda_receipt');
        $manual_receipt=$request->input('manual_receipt');
        $type=$request->input('type');
        
        $pes=explode(',',$request->input('paymentmode'));
        $Credit_fuel='';
        
        $trans_date=date('Y-m-d H:i:s');
        $trans_by=$id;
        $Diff=$request->input('Diff');
        $shift_id=$request->input('shift_id');
        
        $shift=new ShiftSettelementModel();
        
          
        $shift->Nozzle_Amount=$Nozzle_Amount;
        $shift->Nozzle_test=$Nozzle_test;
        $shift->Nozzle_grass=$Nozzle_grass;
        

        if($type==1 || $type==3){
           $shift->Credit_fuel=$Credit_fuel;
         }else{
            $shift->Credit_lube=$Credit_lube;
         }
        $shift->trans_date=$trans_date;
        $shift->trans_by=$trans_by;
        $shift->Credit_receipt=$garruda_receipt;
        $shift->manual_receipt=$manual_receipt;
        $shift->Diff=$Diff;
        $shift->type=$type;
        $shift->shift_id=$shift_id;
       
        $shift->save();

        $psdata=[];

        $i=0;
        foreach ($pes as $pe){
            if(trim($pe)!=''){
                $paymentmode_id=$request->input('paymentmode_'.$pe);
                
                $ref_no=$request->input('ref_nom_'.$pe);
                $garruda_receipt1_=$request->input('garruda_receipt1_'.$pe);
                $manual_receipt1_=$request->input('manual_receipt1_'.$pe);

                    $paymen=$request->input('payment_'.$pe);
              
// dd($manual_receipt1_, $ref_no, $garruda_receipt1_, $paymentmode_id , $paymen);
                $psdata[$i]=array('amount'=>$paymen,'type'=>$shift->type,'paymentmode_id'=>$pe,'sattlement_id'=>$shift->id,'ref_no'=>$ref_no,'garruda_receipt'=>$garruda_receipt1_,'manual_receipt'=>$manual_receipt1_);
                $i++;
                
               // }
            }
            
        }

        DB::table('paymentmode_sattlement')->insert($psdata);

        // if($type==1){
           Shift::where('id',$shift_id)->update(['status' =>1,'fuel'=>1]);
           
        // }else{
           // Shift::where('id',$shift_id)->update(['status' =>1,'lueb'=>1]);
        // }

         //return redirect()->route('settlement');
       
    }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
      }
        return back();

    }


    public function getShift(Request $request){

       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $from_date=$request->input('request_date');

        $from_dates = new Carbon(str_replace('/', '-',$from_date));
       
        $from_dates = date('Y-m-d', strtotime($from_dates));
         
        // $Shifts=Shift::where('ro_code',$rocode)->whereDate('created_at','>=',$from_dates.' 00:00:00')->whereDate('created_at','<=',$from_dates.' 23:59:59')->get();

   $Shifts = DB::table('shifts')
            ->join('tbl_personnel_master', 'tbl_personnel_master.id', 'shifts.shift_manager')
            ->select('tbl_personnel_master.Personnel_Name as name','shifts.created_at','shifts.id')
            ->where('shifts.ro_code',$rocode)
            ->where('shifts.confirm',0)
            ->whereDate('shifts.created_at','>=',$from_dates.' 00:00:00')
            ->whereDate('shifts.created_at','<=',$from_dates.' 23:59:59')
            ->get();   

       $data=[];
       foreach ($Shifts as $Shift) {

        $data[$Shift->id]=($Shift->name).'-'.(date('d/m/Y h:m:s a',strtotime($Shift->created_at)));
       }
       
     return response()->json($data);
           
        
    }

    public function getPreShift($shiftManager){
       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }
        
       $Shift=Shift::where('ro_code',$rocode)->where('shift_manager',$shiftManager)->latest()->first();
       
       $data=['pedestals'=>[],'salesman'=>[]];

       if($Shift!=null){

           foreach ($Shift->getPadestal as $getPadestal) {

            $data['pedestals'][]= $getPadestal->id;

           }

           foreach ($Shift->getPersonnel as $getPersonnel) {

            $data['salesman'][]= $getPersonnel->id;
            
           }
       }
       
      return response()->json($data);
           
        
    }

    public function shifttype(Request $request)
    {  
       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

       $Shifttype=ShiftType::where('RO_Code',$rocode)->get();

      return view('backend.shifttypes',compact('Shifttype')); 
    } 

    public function shifttypestore(Request $request)
    {  
       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }


        $RO_Code=$request->input('RO_Code');
        $type=$request->input('type');
        $start_time=$request->input('start_time');
        $end_time=$request->input('end_time');
        // dd($type);
        $ShiftType=new ShiftType();
        $ShiftType->RO_Code=$RO_Code;
        $ShiftType->type=$type;
        $ShiftType->start_time=$start_time;
        $ShiftType->end_time=$end_time;
       
        $ShiftType->save();

      return Back(); 
    } 


    public function shifttypeedit(Request $request, $id)
    {  
       $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

       $Shifttype=ShiftType::where('RO_Code',$rocode)->where('id',$id)->first();


      return view('backend.shifttypeedit',compact('Shifttype')); 
    } 

    public function shifttypeupdate(Request $request, $id)
    {

    try{
           $rocode='';
         if (Auth::user()->getRocode!=null) {
            $rocode=Auth::user()->getRocode->RO_code;
         }
         $type=$request->input('type');
         $start_time=$request->input('start_time');
         $end_time=$request->input('end_time');

         $Shifttype=ShiftType::where('RO_Code',$rocode)->where('id',$id)->first();
         $Shifttype1 = $Shifttype->type;

        if ($type != $Shifttype1) {
         
          $type = $request->validate([
          'type' => 'unique:ro_shift_config',
         ]);
        }

        if(!empty($type) && !empty($start_time) || !empty($end_time)){

            $Shifttypess=ShiftType::find($id);
            $Shifttypess->type=$type;
            $Shifttypess->start_time=$start_time;
            $Shifttypess->end_time=$end_time;
            $Shifttypess->save();

      


            $request->session()->flash('success','Update Successfully !!');
        }
            $Shifttype=ShiftType::where('RO_Code',$rocode)->get();

       }catch(\Illuminate\Database\QueryException $e){
                
          $request->session()->flash('success','Something wrong!!');
   
      }
        
      return redirect('/shifttype')->with('Shifttype');
    }


    function getshiftbyshiftmanager(Request $request){
       $id=$request->id;
       $data=[];
       $shifts=Shift::where('shift_manager',$id)->where('status',1)->get();

       foreach ($shifts as $shift) {
        $data[$shift->id]=date('d/m/Y  h:i:s A',strtotime($shift->created_at))." To ".date('d/m/Y  h:i:s A',strtotime($shift->closer_date));
       }
       
      return response()->json($data);
    }
    
}
