<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoPersonalManagement;
use App\PaymentModel;
use App\Shift;
use Auth;
use DB;
use Carbon\Carbon;
use App\QR_Code;
use App\MastorRO;
use App\RoMaster;
use App\CityModel;
use App\StateManagementModel;
use App\Principal;
use App\TransactionModel;
use App\ItemServicesModel;
use Log;

class GstReprintController extends Controller
{
    
      function gstSlipFuel(Request $request){
    Log::info('GstReprintController@gstSlipFuel -  input -  '.print_r($request->all(),true));
        $from_date = $request->input('fromdate');
        $to_date = $request->input('to_date');
   
        $from_dates = new Carbon(str_replace('/', '-',$from_date));
        $to_dates = new Carbon(str_replace('/', '-',$to_date));
        $from_dates = date('Y-m-d', strtotime($from_dates));
        $to_dates = date('Y-m-d', strtotime($to_dates));

         $from_dates = $from_dates." 00:00:00";
         $to_dates = $to_dates." 11:59:59";

      Log::info('GstReprintController@gstSlipFuel -  from_dates -  '.$from_dates  .'   to_dates  - '.$to_dates);
       

        $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
           $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }
        }
    $roInfo = RoMaster::where('RO_code',$RO_Code)->get()->first();
    $transactionList = null;

     
      $transactionList = TransactionModel::where('RO_code',$RO_Code)->where('petrol_or_lube',2)
      ->where(DB::raw("(DATE_FORMAT(trans_date,'%Y-%m-%d'))"),'>=',$from_dates)
      ->where(DB::raw("(DATE_FORMAT(trans_date,'%Y-%m-%d'))"),'<=',$to_dates)
      ->groupBy('invoice_number')->get();

       Log::info('GstReprintController@gstSlipFuel -  transactionList count  -  '.print_r(count($transactionList),true));
      // dd($selectedDate, $RO_Code, $transactionList);

    
    return view('backend.reprint.gst-slip-fuel.index',compact('urlVars','roInfo','transactionList'));
  }
  function gstSlipFuelPrint(Request $request){

    Log::info('GstReprintController@gstSlipFuelPrint -  input -  '.print_r($request->all(),true));
    
    $invoice_no = $request->input('invoice_number');
        $RO_Code='';
        if (Auth::user()->getPersonel!=null) {
           $RO_Code=Auth::user()->getPersonel->RO_Code;
        }else{
          if (Auth::user()->getRocode!=null) {
            $RO_Code=Auth::user()->getRocode->RO_code;
          }
        }
   $roInfo = RoMaster::where('RO_code',$RO_Code)->get()->first();
    Log::info('GstReprintController@gstSlipFuelPrint -  roInfo-  '.print_r($roInfo,true));

       $Principal=Principal::where('company_code',$roInfo->company_code)->first();
       $transactionInfo = TransactionModel::leftJoin('tpl_payment_mode','tpl_payment_mode.id','=','tbl_customer_transaction.payment_mode')
       ->where('tbl_customer_transaction.RO_code',$RO_Code)->where('petrol_or_lube',2)->where('invoice_number',$invoice_no)
       ->selectRaw('IFNUll(tpl_payment_mode.name,"Credit") as payment_mode_name,
                tbl_customer_transaction.*')
       ->distinct()
        ->first();

      $transactionDetailsTaxwise = TransactionModel::join('tbl_price_list_master','tbl_price_list_master.id','=','tbl_customer_transaction.item_code')
             ->join('item_transaction_tax','item_transaction_tax.transaction_id','=','tbl_customer_transaction.id')
              ->where('tbl_customer_transaction.RO_code',$RO_Code)
              //->where('tbl_customer_transaction.cust_name','credit')
              ->where('petrol_or_lube',2)
              ->where('tbl_customer_transaction.invoice_number',$invoice_no)
              ->select('item_transaction_tax.*','tbl_price_list_master.hsncode','tbl_customer_transaction.*')
             ->get();
     $transactionDetails=collect($transactionDetailsTaxwise) ->reverse()->unique('item_code');
     $totalCgst=0;
     $totalSgst=0;
     $totalIgst=0;
     $perCgst=0;
     $perSgst=0;
     $perIgst=0;
      foreach($transactionDetailsTaxwise as $transaction){

                          if($transaction->GST_Type=="UT/SGST") {
                            $totalSgst=$totalSgst+$transaction->Tax_Value;
                            $perSgst=$transaction->Tax_percentage;
                          } 

                          if($transaction->GST_Type=="CGST") {
                           $totalCgst=$totalCgst+$transaction->Tax_Value;
                            $perCgst=$transaction->Tax_percentage;
                          } 
                          if($transaction->GST_Type=="IGST") {
                            $totalSgst=$totalIgst+$transaction->Tax_Value;
                            $perIgst=$transaction->Tax_percentage;
                          } 

                  // Log::info('GstReprintController@gstSlipFuelPrint -  transaction - hson -  '.print_r($transaction->hsncode,true));
                              //log::info("apply tax   -   ".$tratex);
           }

       
       // Log::info('GstReprintController@gstSlipFuelPrint -  transactionDetails -  '.print_r($transactionDetailsTaxwise,true));

        /* Log::info('  GstReprintController  $totalCgst ====== '.$totalCgst);  
         Log::info('  GstReprintController  $totalIgst ====== '.$totalIgst);  
         Log::info('  GstReprintController  $totalSgst ====== '.$totalSgst);  
         Log::info('  GstReprintController  $perCgst ====== '.$perCgst);  

         Log::info('  GstReprintController  $perSgst ====== '.$perSgst);  
         Log::info('  GstReprintController  $perIgst ====== '.$perIgst);*/
     // Log::info('GstReprintController@gstSlipFuelPrint -  Principal -  '.print_r($Principal,true));


          $transactionHsnWise=collect($transactionDetailsTaxwise) ->reverse()->groupBy('hsncode');

       

    return view('backend.reprint.gst-slip-fuel.print',compact('roInfo','transactionDetails','transactionInfo','Principal','totalCgst','totalIgst','totalSgst','transactionHsnWise'));
   }
 }