<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Credit_limit;
use App\MastorRO;
use Auth;
use Gate;

class Credit_limitController extends Controller
{    

  public function index()
  {
    if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
            return redirect('Dashboard');
    }
    $rocode='';
    if (Auth::user()->getRocode!=null) {
      $rocode=Auth::user()->getRocode->RO_code;
    }



    $data = Credit_limit::join('tbl_ro_master', 'tbl_customer_limit.Ro_code', '=', 'tbl_ro_master.RO_code')
    ->join('tbl_customer_master', 'tbl_customer_limit.Customer_Code', '=', 'tbl_customer_master.Customer_Code')

    ->select('tbl_customer_limit.*','tbl_ro_master.pump_legal_name as pump_name','tbl_customer_master.Customer_Name as Customer_Name')->orderby('id','desc');

    if(Auth::user()->user_type!=1 && $rocode!='')
        $data=$data->where('tbl_customer_limit.Ro_code',$rocode);

    $data=$data->get();

    $data1 = $MastorRO=MastorRO::where('is_active','1');

    if(Auth::user()->user_type!=1 && $rocode!='')
        $data1=$data1->where('tbl_ro_master.RO_code',$rocode);

        $data1=$data1->get();

    return view('backend.credit_limit',compact('data','data1'));

  }

  public function add_credit_limit(Request $request)
  { 
    $validatedData = $request->validate([
       'Ro_code' => 'required',
       'Customer_Code' => 'required',
       'Limit_Value' => 'required',
    ]);

    try {
        $Credit_limit = new Credit_limit();

        $Credit_limit->Ro_code=$request->input('Ro_code'); 
        $Credit_limit->Customer_Code=$request->input('Customer_Code');
       
        $Credit_limit->Limit_Value=$request->input('Limit_Value');
        $Credit_limit->no_of_days=$request->input('no_of_days');
        $Credit_limit->handling_fee=$request->input('handlingfee');
        $Credit_limit->Discount=$request->input('Discount');       
        $Credit_limit->save();
        $request->session()->flash('success','Data Insert  Successfully!!'); 
        }
        catch(\Illuminate\Database\QueryException $e){
                
        $request->session()->flash('success','Something wrong!!');
            
        }

    return redirect('credit_limit');
  }
  public function edit(Credit_limit $Credit_limit , $id)
  {
    if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
            return redirect('Dashboard');
    }
    $rocode='';
    if (Auth::user()->getRocode!=null) {
      $rocode=Auth::user()->getRocode->RO_code;
    }

    $Credit_limit=$Credit_limit->find($id);

    $data1 = $MastorRO=MastorRO::where('is_active','1');

    if(Auth::user()->user_type!=1 && $rocode!='')
            $data1=$data1->where('tbl_ro_master.RO_code',$rocode);

          $data1=$data1->get();

    return view('backend.credit_limit_update',compact('Credit_limit','data1'));
  }

  public function delete(Request $request, $id)
  {
    try {
         $ac = Credit_limit::where('id',$id)->delete();
    $request->session()->flash('success','Delete Successfully !!');
    } catch(\Illuminate\Database\QueryException $e){
            
            $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
            
        }

    return back();

  }
  function active(Request $request,$id, $actt)
  {

    $ac = Credit_limit::where('id',$id)->update(['is_active' => $actt]);

        if($actt==0)

         $request->session()->flash('success','Deactivated Successfully !!');
        else
         $request->session()->flash('success','Active Successfully !!');

     return back();
       
  }
  public function update(Request $request, Credit_limit $Credit_limit, $id)
  {
     $validatedData = $request->validate([
      'Limit_Value' => 'required', 
      ]);

    try {

        $Credit_limit = $Credit_limit->find($id);
        $Credit_limit->Ro_code=$request->input('Ro_code'); 
        $Credit_limit->Customer_Code=$request->input('Customer_Code');
        $Credit_limit->Limit_Value=$request->input('Limit_Value');
        $Credit_limit->no_of_days=$request->input('no_of_days');

        if($request->input('handlingfee') == null)
          $Credit_limit->handling_fee=0;
        else
          $Credit_limit->handling_fee=$request->input('handlingfee');

        if($request->input('Discount') == null)
          $Credit_limit->Discount=0;
        else
          $Credit_limit->Discount=$request->input('Discount');

        if($request->input('handlinglubefee') == null)
          $Credit_limit->handlinglubefee=0;
        else
          $Credit_limit->handlinglubefee=$request->input('handlinglubefee');

        if($request->input('Discountlube') == null)
          $Credit_limit->Discountlube=0;
        else
          $Credit_limit->Discountlube=$request->input('Discountlube'); 

         $Credit_limit->store_billing_mode=$request->input('billing_mode'); 

        $Credit_limit->save();

        $request->session()->flash('success','Data Updated Successfully!!');

      } catch(\Illuminate\Database\QueryException $e){
          
          $request->session()->flash('success','Something wrong!!');
          
      }

    return redirect('credit_limit');
  }
}
