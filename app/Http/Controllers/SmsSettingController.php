<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\SmsSetting;


class SmsSettingController extends Controller
{

  public function index()
  {
  	$dataInfo = SmsSetting::where('id',1)->get()->first();
  	//dd($dataInfo);
  	return view('backend.smssetting',compact('dataInfo'));
         
  }
  public  function update(Request $request)
  {
  	$dataInfo = SmsSetting::where('id',1)->get()->first();

  	$dataInfo->Username=$request->input('Username'); 
  	$dataInfo->Password=$request->input('Password');
  	$dataInfo->URL=$request->input('URL');
  	$dataInfo->save();
  
  	$request->session()->flash('success','Setting updated Successfully!!');
  	return redirect()->route('smssetting');
  }
}
