<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\SlipBook;
use Auth;
use Validator;

class SlipBookController extends Controller
{
	public $jsonResponse = ['success' => false, 'message' => '' , 'data' => null, 'errors' => null];
	
	public function index()
	{
		$dataInfo = SlipBook::where('id',1)->get()->first();
		return view('backend.slipbook',compact('dataInfo'));
	}
 

    public function store(Request $request)
    {
		
		//echo "<pre>";
		$postData = $request->all();
    	//print_r($postData);
		//$this->jsonResponse['data'] = $postData;
		
		$roCode = (isset($postData['RO_code']) && trim($postData['RO_code'])!="") ? trim($postData['RO_code']) : "";
		$customerArray = (isset($postData['customer_code']) && is_array($postData['customer_code']) && count($postData['customer_code'])>0) ? $postData['customer_code'] :array();
		$issueDateArray = (isset($postData['Slip_issue_date']) && is_array($postData['Slip_issue_date']) && count($postData['Slip_issue_date'])>0) ? $postData['Slip_issue_date'] :array();
		$numberOfBookArray = (isset($postData['Slip_no_of_book']) && is_array($postData['Slip_no_of_book']) && count($postData['Slip_no_of_book'])>0) ? $postData['Slip_no_of_book'] :array();
		
		$slipStartArray = (isset($postData['Slip_start']) && is_array($postData['Slip_start']) && count($postData['Slip_start'])>0) ? $postData['Slip_start'] :array();
		$slipEndArray = (isset($postData['Slip_end']) && is_array($postData['Slip_end']) && count($postData['Slip_end'])>0) ? $postData['Slip_end'] :array();
		$numberOfSlipArray = (isset($postData['no_of_slip']) && is_array($postData['no_of_slip']) && count($postData['no_of_slip'])>0) ? $postData['no_of_slip'] :array();
		
		$customerLength = count($customerArray);
		$slipStartLength = count($slipStartArray);
		$slipEndLength = count($slipEndArray);
		$numberOfSlipLength = count($slipEndArray);

		if($roCode!="" && $customerLength > 0 && $customerLength == $slipStartLength &&  $customerLength == $slipEndLength &&  $customerLength == $numberOfSlipLength){
			
			$formErrors = array();
			
			for($i=0;$i<count($customerArray);$i++){
				
				$validNoOfSlip = (abs($slipEndArray[$i] - $slipStartArray[$i]) + 1);
				//echo $validNoOfSlip;exit;
				if($slipStartArray[$i] >= $slipEndArray[$i]){
					$formErrors[] = "Slip start must be less than slip end";
					break;
				}
				if($numberOfSlipArray[$i] != $validNoOfSlip){
					$formErrors[] = "Invalid no of slip";
					break;
				}
				//SELECT * FROM tbl_slip_book WHERE RO_code ='RO86557' AND ((Slip_start BETWEEN 1 AND 10) OR (slip_end BETWEEN 1 AND 10))
				//$where = 'RO_code ="'.$roCode.'" AND ((Slip_start BETWEEN '.$slipStartArray[$i].' AND '.$slipEndArray[$i].') OR (slip_end BETWEEN '.$slipStartArray[$i].' AND '.$slipEndArray[$i].'))';
				//$where = 'RO_code ="'.$roCode.'" AND ((Slip_start >='.$slipStartArray[$i].' AND Slip_end >='.$slipStartArray[$i].') OR (Slip_start <='.$slipEndArray[$i].' AND Slip_end >='.$slipEndArray[$i].'))';
				$where = 'RO_code ="'.$roCode.'" AND ((Slip_start >='.$slipStartArray[$i].' OR Slip_end >='.$slipStartArray[$i].'))';
				$existingCustomerSlips =SlipBook::whereRaw($where)->get();
				//$formErrors[] = $where;
				if($existingCustomerSlips){
					$totalExist = count($existingCustomerSlips->toArray());
					if($totalExist>0){
						$formErrors[] = "Slip number range already exists";
					}	
				}
			}
			if(count($formErrors) == 0){
				
				for($i=0;$i<count($customerArray);$i++){
					if($roCode!="" && $customerArray[$i]!="" && $issueDateArray[$i]!="" && $numberOfBookArray[$i]!="" && $slipStartArray[$i]!="" && $slipEndArray[$i]!="" && $numberOfSlipArray[$i]!=""){
						
						$dateArray = explode("/",$issueDateArray[$i]);
						if(count($dateArray)==3){
							$tempDate = $dateArray[2].'-'.$dateArray[1].'-'.$dateArray[0];
							$issueDateArray[$i] = date("Y-m-d",strtotime($tempDate));
							
							SlipBook::create([
								'RO_code'=>$roCode,
								'Customer_Code'=>$customerArray[$i],
								'Slip_issue_date'=>$issueDateArray[$i],
								'Slip_no_of_book'=>$numberOfBookArray[$i],
								'Slip_start'=>$slipStartArray[$i],
								'Slip_end'=>$slipEndArray[$i],
								'no_of_slip'=>$numberOfSlipArray[$i],
							]);
						}	
					}
				}
				$request->session()->flash('success','Slip number saved Successfully!!');
				$this->jsonResponse['success'] = true;
				$this->jsonResponse['message'] = 'Slip number saved Successfully!!';
				$this->jsonResponse['redirectUrl'] = route('slipbook');
			}
			else{
				$this->jsonResponse['errors'] = $formErrors;
			}
		}
		else{
			$this->jsonResponse['message'] = 'Sorry!, unable to save slip number';
			$this->jsonResponse['errors'] = $formErrors;
		}
		return response()->json($this->jsonResponse);
    }
	
	public function slipList(){
		$roCode = Auth::user()->getRocode->RO_code;
		$slipBookList = SlipBook::where('RO_code',$roCode)->get();
		return view('backend.slipbook_list',compact('slipBookList'));
	}
	
	public function destroy(Request $request,$id)
	{
		$roCode = Auth::user()->getRocode->RO_code;
		$dateInfo = SlipBook::where('RO_code',$roCode)->where('id',$id)->first();
		if($dateInfo && $dateInfo->delete()){
			$request->session()->flash('success','Slip book deleted successfully');
		}
		return redirect('slipbooklist');
	}
	
	public function edit($id)
	{
		$dataInfo = SlipBook::where('id',$id)->get()->first();
		if(!$dataInfo){
			return redirect('slipbooklist');
		}
		return view('backend.slipbook_edit',compact('dataInfo'));
	}
	
	public function update(Request $request,$id)
	{
		$postData = $request->all();
		$roCode = Auth::user()->getRocode->RO_code;
		$dateInfo = SlipBook::where('RO_code',$roCode)->where('id',$id)->first();
		if(!$dateInfo){
			return redirect('slipbooklist');
		}
		
		$validator= Validator::make($postData, [
			'Slip_issue_date' => 'required',
			'Slip_no_of_book' => 'required',
			'Slip_start' => 'required',
			'Slip_end' => 'required',
			'no_of_slip' => 'required',
		]);
			
		if ($validator->fails()){
			return redirect()->back()->withInput()->withErrors($validator);
		}
		else{
			
			$formErrors = array();
			
			$validNoOfSlip = (abs($postData['Slip_end'] - $postData['Slip_start']) + 1);
			if($postData['Slip_start'] >= $postData['Slip_end']){
				$formErrors[] = "Slip start must be less than slip end";
			}
			if($postData['no_of_slip'] != $validNoOfSlip){
				$formErrors[] = "Invalid no of slip";
			}
			$where = 'RO_code ="'.$roCode.'" AND id!='.$id.' AND ((Slip_start >='.$postData['Slip_start'].' OR Slip_end >='.$postData['Slip_start'].'))';
			$existingCustomerSlips =SlipBook::whereRaw($where)->get();
			if($existingCustomerSlips){
				$totalExist = count($existingCustomerSlips->toArray());
				if($totalExist>0){
					$formErrors[] = "Slip number range already exists";
				}	
			}
			if(count($formErrors) == 0){
				
				$dateArray = explode("/",$postData['Slip_issue_date']);
				if(count($dateArray)==3){
					$tempDate = $dateArray[2].'-'.$dateArray[1].'-'.$dateArray[0];
					$postData['Slip_issue_date'] = date("Y-m-d",strtotime($tempDate));

					$dateInfo->Slip_issue_date = $postData['Slip_issue_date'];
					$dateInfo->Slip_no_of_book = $postData['Slip_no_of_book'];
					$dateInfo->Slip_start = $postData['Slip_start'];
					$dateInfo->Slip_end = $postData['Slip_end'];
					$dateInfo->no_of_slip = $postData['no_of_slip'];
					if($dateInfo->save()){
						$request->session()->flash('success','Slip book updated successfully');
						return redirect('slipbooklist');  
					}
				}
			}
			//print_r($formErrors);exit;
			$request->session()->flash('success',$formErrors[0]);
			return redirect()->back()->withInput();
		}	
	}
}