<?php

namespace App\Http\Controllers;

//use App\CityController;
use Illuminate\Http\Request;
use App\CityModel;
use App\StateManagementModel;
use DB;
class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , $id)
    {
         $CityModel = CityModel::where('state_id',$id)->get();
        $StateManagementModel = StateManagementModel::where('id',$id)->first();
         
         //  dd($StateManagementModel);
        return view('backend.state.city.city',compact('StateManagementModel','CityModel')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create(Request $request)
    {
         $id=$request->input('statecode');
         $cityname=strtolower(trim($request->input('name'))); 
          $allc=CityModel::where('state_id',$id)->where('name',$cityname)->get();
           
           /*$validatedData = $request->validate([
            'name' => 'required|unique:cities,state_id,'.$id,
           ]);*/
          if($allc->count()>0){
               $request->session()->flash('success','City Already Exists!!'); 
               return back();
          }

          $CityModelAdd = new CityModel();
          $CityModelAdd->state_id =$request->input('statecode');
          $CityModelAdd->name =$cityname;
          $CityModelAdd->save();
          $request->session()->flash('success','Add Successfully!!'); 
          return back();
 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeupdate(Request $request , $id)
    {
         $cityname=$request->input('name');  
        $validatedData = $request->validate([
          'name' => 'required|unique:cities',

         ]);
          CityModel::where('id', $id)->update(['name' =>$cityname]);
          $request->session()->flash('success','Data Updated Successfully!!'); 
          return back();
  


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function show(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function edit(ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContributionModel $contributionModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContributionModel  $contributionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContributionModel $contributionModel)
    {
        //
    }
}
