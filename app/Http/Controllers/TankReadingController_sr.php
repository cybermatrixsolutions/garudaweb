<?php

namespace App\Http\Controllers;

use App\TankReading;
use App\RoMaster;
use App\ChartVolume;
use App\Nozzle;
use App\TableTankMasterModel;
use Illuminate\Http\Request;
use DB;
use Gate;
use Auth;

class TankReadingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;

       }else if (Auth::user()->getPersonel!=null) {
          $rocode=Auth::user()->getPersonel->RO_Code;
       }

        $RoMasters= RoMaster::where('is_active','1');

        if(Auth::user()->user_type!=1 && $rocode!='')
                $RoMasters=$RoMasters->where('tbl_ro_master.RO_code',$rocode);

              $RoMasters=$RoMasters->get();

        $TankMasterModels=TableTankMasterModel::where('is_active','1');

        if(Auth::user()->user_type!=1 && $rocode!='')
                $TankMasterModels=$TankMasterModels->where('tbl_tank_master.RO_code',$rocode);

              $TankMasterModels=$TankMasterModels->get();
                        
        $TankReadings =TankReading::All();
        if(Auth::user()->user_type!=1 && $rocode!='')
                $TankReadings=TankReading::where('tbl_tank_reading.Ro_code',$rocode)->get();
        else
               $TankReadings =TankReading::get();
          // dd($TankReadings);
        return view('backend.dip_reading',compact('TankReadings','RoMasters','TankMasterModels','rocode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gettanknozzel($id)
    {   
        
        $nozzel=Nozzle::where('Tank_id',$id)->get();


        //dd($VehicleModel->getstockitemname->getItem);
       $str='<table class="table table-striped">
                <thead>
                  <tr role="row">
                   <th>Nozzle Number</th>
                   <th>Fuel Type</th>
                   <th>Status</th>
                 <tbody>';
            foreach ($nozzel as $nozzels) {
               $str.='<tr role="row" class="odd">
    <td>'.$nozzels->Nozzle_Number.'</td>
    <td>'.$nozzels->getItem->Item_Name.'</td>
    <td>';

if($nozzels->is_active==1){
    $str.='<img src="'.asset("images").'/test-pass-icon.png"" >';
  }else{ 
     $str.='<img src="'.asset("images").'/test-fail-icon.png" style="">';
 }

$str.='</td> </tr>';
            }
            $str.='</tbody>
              </table>';
            

             return $str;
    }

    public function gettankdipchartcalculation(Request $request){
        $cm=$request->input('cm');
        $mm=$request->input('mm');
        $diptype=$request->input('diptype');
        $dipid=$request->input('dipid');
        $sum=0;

        $dipchartval=ChartVolume::where('dipchart_id',$dipid);
        if($diptype=='cm'){

             $dipchartval=$dipchartval->where('dip',$cm)->first();

            if($dipchartval!=null){
            
             if($mm!=0)
             $sum=$mm*$dipchartval->diff_mm;
         
             $sum=$sum+$dipchartval->vol;

            }else{

                 $sum=0;
            }

        }else{

            $dipchartval=$dipchartval->where('dip',$mm)->first();

            if($dipchartval!=null)
                $sum=$dipchartval->vol;
            else
                $sum=0;

        }
    
      return $sum;
 
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
       try{
             $tankId=$request->input('Tank_code');
             $sumvalue=$request->input('sumvalue');
             $totalesInwards=DB::table('tank_tankinwart')->where('tank_id',$tankId)->where('status',1)->get();
             $TankReading=TankReading::where('Tank_code',$tankId)->latest()->first();
             
             $tank_stack=0;
             $salesvalue=0;

             if($TankReading!=null){
                $tank_stack=$TankReading->value;
             }

             foreach ($totalesInwards as $totalesInward) {
               $tank_stack=$totalesInward->value+$tank_stack;
             }
            
             if($totalesInwards->count()>0)
             DB::table('tank_tankinwart')->where('tank_id',$tankId)->update(['status'=>0]);

             $salesvalue=$tank_stack-$sumvalue;

            $TankReadings = new TankReading();
            $TankReadings->Ro_code=$request->input('Ro_code');
            $TankReadings->Tank_code=$request->input('Tank_code');
            $TankReadings->tank_stack=$tank_stack;
            $TankReadings->sale_value=$salesvalue;
            //$TankReadings->Reading=$request->input('reading');

            if($request->input('ditype')=='cm'){

                $mmval=$request->input('reading_cm');
                $val=$request->input('reading');
                $TankReadings->Reading= $val;
                $TankReadings->dip_mm= $mmval;

            }else{

               $TankReadings->Reading=$request->input('reading');
            }
            
            $TankReadings->fuel_type=$request->input('fuel_type');
            
            $date = $request->input('reading_date');
            $date1 = str_replace('/', '-', $date);
            $reading_date = date('Y-m-d', strtotime($date1));
            $TankReadings->reading_date=$reading_date;

            $TankReadings->capacity=$request->input('capacity');
            $TankReadings->unit_measure=$request->input('unit_measure');

            $TankReadings->value=$request->input('sumvalue');

            $TankReadings->save();
            $request->session()->flash('success','Added Successfully!!');  

        } catch(\Illuminate\Database\QueryException $e){
              
             
                $request->session()->flash('success','Something wrong!!');  
            }

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TankReading  $tankReading
     * @return \Illuminate\Http\Response
     */
    public function show(TankReading $tankReading)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TankReading  $tankReading
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request , $id)
    {    
           if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
        $rocode='';
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
       }

        $RoMasters= RoMaster::where('is_active','1');

        if(Auth::user()->user_type!=1 && $rocode!='')
                $RoMasters=$RoMasters->where('tbl_ro_master.RO_code',$rocode);

              $RoMasters=$RoMasters->get();

        $TankMasterModels=TableTankMasterModel::where('is_active','1');

        if(Auth::user()->user_type!=1 && $rocode!='')
                $TankMasterModels=$TankMasterModels->where('tbl_tank_master.RO_code',$rocode);

              $TankMasterModels=$TankMasterModels->get();
                        
        $TankReadings =TankReading::All();
        if(Auth::user()->user_type!=1 && $rocode!='')
                $TankReadings=TankReading::where('tbl_tank_reading.Ro_code',$rocode)->get();
        else
               $TankReadings =TankReading::get();
          $Dipreadingedit=TankReading::find($id);
          
         return view('backend.dipreadingedit',compact('Dipreadingedit','TankReadings','RoMasters','TankMasterModels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TankReading  $tankReading
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $TankReadings = TankReading::find($id);
        $TankReadings->Ro_code=$request->input('Ro_code');
        $TankReadings->Tank_code=$request->input('Tank_code');
        //$TankReadings->Reading=$request->input('reading');

        if($request->input('ditype')=='cm'){

            $mmval=$request->input('reading_cm');
            $val=$request->input('reading');
            $TankReadings->Reading= $val;
            $TankReadings->dip_mm= $mmval;

        }else{

           $TankReadings->Reading=$request->input('reading');
        }
        
        $TankReadings->fuel_type=$request->input('fuel_type');
        $TankReadings->capacity=$request->input('capacity');
        $TankReadings->unit_measure=$request->input('unit_measure');
        if($request->input('sumvalue')!=null){
         $TankReadings->value=$request->input('sumvalue');
        }
        else{
           $TankReadings->value=$request->input('oldsumvalue');  
        }
       

        $TankReadings->save();
         $request->session()->flash('success','Added Successfully!!'); 
         return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TankReading  $tankReading
     * @return \Illuminate\Http\Response
     */
    public function destroy(TankReading $tankReading)
    {
        //
    }

      /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TankReading  $tankReading
     * @return \Illuminate\Http\Response
     */
       public function gettankcapacity(Request $request)
    {   
        $val=explode('.', $request->input('reading'));
        $dipchartval=ChartVolume::where('dip',$val[0])->first();
        $sum=0;
        

        if($dipchartval!=null)
        if($dipchartval->Dipchart->lenght_type=='cm'){

             if(!isset($val[1]))
              $sum=$dipchartval->diff_mm+$val[0];
             else
             $sum=$dipchartval->diff_mm+($val[1]*$val[0]);

        }else{

                 if(!isset($val[1]))
                  $sum=$dipchartval->diff_mm+$val[0];
                 else
                 $sum=$dipchartval->diff_mm+$val[1];
        }

        $Tank_code = $request->input('Tank_code');
        $TableTankMasterModel=TableTankMasterModel::where('id',$Tank_code)->first();
        $Item_Name='';
        $Unit_Name='';
         if($TableTankMasterModel->getfueltypes!=null)
            $Item_Name=$TableTankMasterModel->getfueltypes->Item_Name;

        if($TableTankMasterModel->getunitmesure!=null)
            $Unit_Name=$TableTankMasterModel->getunitmesure->Unit_Name;

        //dd($VehicleModel->getstockitemname->getItem);
        //$items=$TableTankMasterModel->capacity->pluck('Item_Name','id')->toArray();
        $data=['capacity'=>$TableTankMasterModel->Capacity,'fueltype'=>$TableTankMasterModel->fuel_type,'unit'=>$TableTankMasterModel->Unit_of_Measure,'Item_Name'=>$Item_Name,'Unit_Name'=>$Unit_Name,'sum'=>$sum];

       return response()->json($data);
    }
    public function getTank($code)
    {
        $TankMasterModels=TableTankMasterModel::where('is_active','1')->where('RO_code',$code)->get();
        $TankMasterModels=$TankMasterModels->pluck('Tank_Number','id')->toArray();

        return response()->json($TankMasterModels);
    }
    
    function active(Request $request,$id, $actt)
    {
        
        $ac = TankReading::where('id',$id)->update(['is_active' => $actt]);

            if($actt==0)

             $request->session()->flash('success','Deactivated Successfully !!');
            else
             $request->session()->flash('success','Active Successfully !!');

         return back();
             
    }


    public function getTankStack($id){

             $data=['stack'=>0];
             $totalesInwards=DB::table('tank_tankinwart')->where('tank_id',$id)->where('status',1)->get();
             $TankReading=TankReading::where('Tank_code',$id)->latest()->first();
             
             $tank_stack=0;
             
             if($TankReading!=null){
                $tank_stack=$TankReading->value;
             }

             foreach ($totalesInwards as $totalesInward) {
               $tank_stack=$totalesInward->value+$tank_stack;
             }

         $data['stack']=$tank_stack;

       return response()->json($data);
    }
}
