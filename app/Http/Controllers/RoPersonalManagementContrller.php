<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

use App\Http\Requests;
use App\RoMaster;
use App\User;
use App\Http\Controllers\Controller;
use App\TableDesignationMaster;
use App\RoPersonalManagement;
use Auth;
use Gate;
use DB;

class RoPersonalManagementContrller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
         
        
         'email' => 'required|unique:users',
        
         'personal_ro_code' => 'required',
         'mobile' => 'required',
        
    ]);

          $length = 5;
          $randomBytes = openssl_random_pseudo_bytes($length);
          $characters = '0123456789';
          $charactersLength = strlen($characters);
          $result = '';
          for ($i = 0; $i < $length; $i++)
            $result .= $characters[ord($randomBytes[$i]) % $charactersLength];


  
        try {
             $user_type=5;
             if($request->input('designation')==29)
                   $user_type=5;
                else if($request->input('designation')==30)
                   $$user_type=6;
                else
                    $user_type=7;

            $neuser=User::where('mobile',$request->input('mobile'))->get();

            if($neuser->count()>0){
               $request->session()->flash('success','Mobile No Already Exist !!');
               return back(); 
            }

            $date = $request->input('d_o_b');
            $date1 = str_replace('/', '-', $date);
            $d_o_b = date('Y-m-d', strtotime($date1));
            $date = $request->input('date_of_appointment');
            $date1 = str_replace('/', '-', $date);
            $date_of_appointment = date('Y-m-d', strtotime($date1));
            
            if($request->hasFile('aadhaar_img'))
             {
                $file = Input::file('aadhaar_img');
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $filename = $file->getClientOriginalName();

                $name = $timestamp . '-' . $filename;
                $aadhaar_img = 'images/' . $name;

                $file->move('images/', $name);
             }//dd($aadhaar_img);
            $personal_mangement = new RoPersonalManagement();
            $personal_mangement->RO_code = $request->input('personal_ro_code');
            $personal_mangement->Personnel_Name = $request->input('personl_name');
            $personal_mangement->gender = $request->input('gender');
            $personal_mangement->Designation = $request->input('designation');
            $personal_mangement->Reporting_To = $request->input('reporting_to');
            $personal_mangement->Date_of_Birth = $d_o_b;            
            $personal_mangement->Date_of_Appointment = $date_of_appointment;
          
            $personal_mangement->Mobile = $request->input('mobile');
            if (!empty($request->input('aadhaar'))) {
               $personal_mangement->Aadhaar_no = $request->input('aadhaar_no');
            }
            if (!empty( $request->hasFile('aadhaar_img'))) {
                //dd('hii');
                $personal_mangement->Aadhaar_img = $aadhaar_img;
            }
          
            $personal_mangement->Email = $request->input('email');
          
            $personal_mangement->password = $request->input('password');       
            $personal_mangement->Personnel_Code = 'PER'.$result;
            $personal_mangement->save();
            //dd($personal_mangement);

            $neuser=User::where('email',$personal_mangement->Email)->first();
            
            if($neuser==null){

                $user = new User();
                $user->name=$personal_mangement->Personnel_Name;
                $user->email=$personal_mangement->Email;
               

                if($personal_mangement->Designation==29)
                   $user->user_type=5;
                else if($personal_mangement->Designation==30)
                   $user->user_type=6;
                else
                    $user->user_type=7;

                $user->password=bcrypt($request->input('password'));
                $user->mobile=$request->input('mobile');
                $user->save();
                //dd('hellodfsdfsd');
            }

            $request->session()->flash('success','Added Successfully!!'); 

        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');  
            }
           
            return redirect('persoonel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }
            $rocode='';
           if (Auth::user()->getRocode!=null) {
              $rocode=Auth::user()->getRocode->RO_code;
           }
         $personal_mangement_data = DB::table('tbl_personnel_master')->where('id', '=', $id)->first();
         $datas = RoMaster::where('is_active',1)->get();
         $designation= TableDesignationMaster::orderBy('Designation_Name', 'asc')->get();
            
            $personal_list = RoPersonalManagement::where('is_active',1)->where('RO_Code',$rocode)->orderBy('Personnel_Name', 'asc')->get();
        
        return view('backend.personaledits', compact(['personal_mangement_data','designation', 'datas','personal_list']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

     

         try {

            $personal_mangement = RoPersonalManagement::find($id);
            
             $neusers=User::where('email','<>',$personal_mangement->Email)->where('mobile',$request->input('mobile'))->get();

                 if($neusers->count()>0){
                     $request->session()->flash('success','Mobile No Already Exist !!');
                     return back(); 
                 }

            $date = $request->input('d_o_b');
            $date1 = str_replace('/', '-', $date);
            $d_o_b = date('Y-m-d', strtotime($date1));
            // $date =$request->input('d_o_b');
            // $d_o_b = date("Y-m-d", strtotime($date));

            $date = $request->input('date_of_appointment');
            $date1 = str_replace('/', '-', $date);
            $date_of_appointment = date('Y-m-d', strtotime($date1));
            if($request->hasFile('aadhaar_img'))
             {
                $file = Input::file('aadhaar_img');
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $filename = $file->getClientOriginalName();

                $name = $timestamp . '-' . $filename;
                $aadhaar_img = 'images/' . $name;

                $file->move('images/', $name);
             }//dd($aadhaar_img);
           
            // $date =$request->input('date_of_appointment');
            // $date_of_appointment = date("Y-m-d", strtotime($date));
       
           
             // $date =$request->input('date_of_termination');
            // $date_of_termination = date("Y-m-d", strtotime($date));
            
           
            // $personal_mangement->RO_code = $request->input('personal_ro_code');
            $personal_mangement->Personnel_Name = $request->input('personl_name');
            $personal_mangement->gender = $request->input('gender');
            $personal_mangement->Designation = $request->input('designation');
            $personal_mangement->Reporting_To = $request->input('reporting_to');
            $personal_mangement->Date_of_Birth = $d_o_b;            
            $personal_mangement->Date_of_Appointment = $date_of_appointment;
            
            $personal_mangement->Mobile = $request->input('mobile');
            if (!empty($request->input('aadhaar'))) {
               $personal_mangement->Aadhaar_no = $request->input('aadhaar_no');
            }
            if (!empty( $request->hasFile('aadhaar_img'))) {
                //dd('hii');
                $personal_mangement->Aadhaar_img = $aadhaar_img;
            }
         
            //$personal_mangement->password = $request->input('password');
            //$personal_mangement->Email = $request->input('Email');
           
            $personal_mangement->save();

             $userArr=['mobile'=>$personal_mangement->Mobile];
           
            if($request->input('password')!=null && trim($request->input('password'))!='')
              $userArr['password']= bcrypt($request->input('password'));

            User::where('email',$personal_mangement->Email)->update($userArr);


            $request->session()->flash('success','Data Updated Successfully!!'); 
             
         } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','Something wrong!!');  
            }
        
        return redirect('persoonel'); 
    }

    function active(Request $request,$id, $actt)
    {
         if($actt == 0)
            {   
                 $Personal=RoPersonalManagement::find($id);
                 
                 if($Personal->Mobile!=null && $Personal->Mobile!='')
                  DB::table('personnel_data')->insert(['personnel_id' => $id, 'mobile' => $Personal->Mobile,'IMEI_no' => $Personal->IMEI_No]);
                
                $ac = RoPersonalManagement::where('id',$id)->update(['is_active' => $actt,'IMEI_No' =>null]);
     
            }
            else
            {
                 $ac = RoPersonalManagement::where('id',$id)->update(['is_active' => $actt]);
     
            }
        
     

            if($actt==1)

             $request->session()->flash('success','Active Successfully !!');
            else
             $request->session()->flash('success','Deactivated Successfully !!');

         return back();
             
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
           $personal_mangement = RoPersonalManagement::find($id);

        $personal_mangement->delete(); 
        } catch(\Illuminate\Database\QueryException $e){
                
                $request->session()->flash('success','This record have some reference in other table so please remove these tables data first!!');
                
            }
        

        return back();
    }

    

    public function view($id)
    {
         if (Gate::allows('custermer',Auth::user()) || Gate::allows('admin',Auth::user())) {
                return redirect('Dashboard');
        }


         $personal_mangement_data = DB::table('tbl_personnel_master')->where('id', '=', $id)->first();
         $datas = RoMaster::All();
         $designation= TableDesignationMaster::All();
            
            $personal_list = RoPersonalManagement::All();
        
        return view('backend.personalView', compact(['personal_mangement_data','designation', 'datas','personal_list']));
    }
}
