<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerDriverManager extends Model
{
    protected $table="tbl_customer_vehicle_driver";
    public   $timestamps = false;

    public function getCustomer(){
      	return $this->hasOne(RoCustomertManagement::class,'Customer_Code','customer_code');
    }

    public function getVihicle(){
    	 
    	return $this->hasOne(VechilManage::class,'Registration_Number','Registration_Number');
    }
}
