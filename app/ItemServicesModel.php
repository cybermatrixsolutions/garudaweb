<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemServicesModel extends Model
{
    protected $table = "item_services";
   
    public function gettax(){
    	
    	return $this->belongsToMany(tax_master::class,'item_services_tax','services_id','tax_id');
    }
}