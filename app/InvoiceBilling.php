<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceBilling extends Model
{
    protected $table = "invoice_billing";

    public function party(){

        return $this->belongsTo(RoCustomertManagement::class,'customer_code','Customer_Code');

    }

     public function trangection(){

        return $this->hasMany(TransactionModel::class,'bill_no','invoice_no')->where('status','1');

    }
}
