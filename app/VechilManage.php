<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VechilManage extends Model
{
    protected $table ='tbl_customer_vehicle_master';

    public function getmake(){

    	return $this->belongsTo(VehicleMakeModel::class,'Make','id');
    }
    public function getmmodel(){

    	return $this->belongsTo(VehicleModel::class,'Model','id');
    }

    public function getitem(){

    	return $this->belongsTo(RoPieceListManagement::class,'Fuel_Type','id');
    }

    public function getQr(){

        return $this->hasOne(QR_Code::class,'Vehicle_Reg_no','Registration_Number');
    }

     public function getCustomer(){
        return $this->hasOne(RoCustomertManagement::class,'Customer_code','Customer_code');
    }


}
