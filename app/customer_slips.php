<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer_slips extends Model
{
   public function getcompanyname()
    {
        return $this->belongsTo(RoCustomertManagement::class,'customer_code','Customer_code');
    }
    public function getitemname()
    {
        return $this->belongsTo(RoPieceListManagement::class,'Fuel_Type','id');
    }
}
