<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    //

   public function getPadestal(){
   	return $this->belongsToMany(PedestalMaster::class,'shift_pedestal','shift_id','pedestal_id');
   }

   public function getPersonnel(){
   	return $this->belongsToMany(RoPersonalManagement::class,'shift_personnel','shift_id','personnel_id');
   }
    public function getPersonnelname(){
   	return $this->belongsTo(RoPersonalManagement::class,'shift_manager','id');
   }
   public function getPersonnelsalemane(){
   	return $this->belongsTo(RoPersonalManagement::class,'personnel_id','id');
   }
   
   public function getTanks(){
   	
		return $this->belongsToMany(TableTankMasterModel::class,'shift_tank','shift_id','tank_id');
   }
    
    public function getTanksReading(){
    
    return $this->hasMany(TankReading::class,'shift_id','id');
   }

   public function settelment(){
    
    return $this->hasOne(ShiftSettelementModel::class,'shift_id','id');
   }
}
