<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftSettelementModel extends Model
{
 
    protected $table ='tbl_shift_settlement';
    public $timestamps = false;

    public function getPayments(){
    	return $this->belongsToMany(PaymentModel::class,'paymentmode_sattlement','sattlement_id','paymentmode_id')->withPivot('amount', 'ref_no');
    }

}

