<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableTankMasterModel extends Model
{
    protected $table = "tbl_tank_master";
    public $timestamps = false;

    function getunitmesure(){
    	return $this->belongsTo(UnitModel::class,'Unit_of_Measure','id');
    }
    function getfueltypes(){
    	return $this->belongsTo(RoPieceListManagement::class,'fuel_type','id');
    }

    function getDipchart(){
    	return $this->belongsTo(Dipchart::class,'dipchart_id','id');
    }

    public function tankInward(){

        return $this->belongsToMany(TankInward::class,'tank_tankinwart','tank_id','tankinwart_id')->withPivot('id','value','statas');
    }

}
