<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommissionInvoice extends Model
{
    protected $table = "customer_commi_invoice";
    
      public function getcustomer(){

        return $this->belongsTo(RoCustomertManagement::class,'customer_code','Customer_Code');

    }
   
}
