<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlipBook extends Model
{
	 protected $table="tbl_slip_book";
   // public $timestamps = false;	
	
	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'RO_code', 'Customer_Code', 'Slip_issue_date', 'Slip_no_of_book', 'Slip_start','Slip_end','no_of_slip',
    ];
	
	public function getCustomer(){
		return $this->belongsTo('App\RoCustomertManagement','Customer_Code','Customer_Code');
	}
 }