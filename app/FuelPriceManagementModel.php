<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FuelPriceManagementModel extends Model
{
    protected $table = 'tbl_item_price_list';
}