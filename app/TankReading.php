<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TankReading extends Model
{
    protected $table ='tbl_tank_reading';

    function getRo(){
    	return $this->belongsTo(RoMaster::class,'Ro_code','RO_code');
    }

    function getTank(){
    	return $this->belongsTo(TableTankMasterModel::class,'Tank_code','id');
    }
    function getcapacity(){
    	return $this->belongsTo(TableTankMasterModel::class,'Tank_code','id');
    }
   
    function getunitmesure(){
    	return $this->belongsTo(UnitModel::class,'unit_measure','id');
    }
    function getfueltypes(){
    	return $this->belongsTo(RoPieceListManagement::class,'fuel_type','id');
    }
}
