<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LOBMasterModel extends Model
{
    protected $table = "tbl_lob_master";
}
