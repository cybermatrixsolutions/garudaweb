<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedestalMaster extends Model
{
    //
    protected $table = "tbl_pedestal_master";
    public $timestamps = false;

    public function getNozzle(){
    	return $this->hasMany(Nozzle::class,'Pedestal_id','id');
    }
}
