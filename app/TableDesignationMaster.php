<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableDesignationMaster extends Model
{
    protected $table="tbl_designation_master";

      public function reporting(){
        return $this->belongsTo(self::class,'Reporting_To_Designation');
    	
    }
}
