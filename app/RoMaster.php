<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoMaster extends Model
{
    protected $table = "tbl_ro_master";


    public function getdetails(){

        return $this->hasOne(RoDetails::class,'ro_id','id');

    }
    public function getstate(){

        return $this->belongsTo(StateManagementModel::class,'state','id');

    }
    public function getcity(){

        return $this->belongsTo(CityModel::class,'city','id');

    }

     public function user(){

        return $this->hasOne(User::class,'email','Email');

    }

    public function principal(){

        return $this->belongsTo(Principal::class,'company_code','company_code');

    }
}
