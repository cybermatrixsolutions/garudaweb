<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QR_Code extends Model
{
    protected $table ='tbl_qrcode_manage';

    public function getcustomer(){

        return $this->belongsTo(RoCustomertManagement::class,'Customer_Code','Customer_Code');

    }
    public function getcity(){

        return $this->hasOne(CityModel::class,'id','city');

    }

    public function getVihicle(){

        return $this->hasOne(VechilManage::class,'Registration_Number','Vehicle_Reg_no');

    }
}
