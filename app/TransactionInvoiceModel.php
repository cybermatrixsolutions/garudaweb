<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionInvoiceModel extends Model
{
    protected $table = "tbl_customer_transaction_invoice";
    // public $timestamps = false;
}
