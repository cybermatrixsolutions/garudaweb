<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceProductSummary extends Model
{
    protected $table = "invoice_product_summary";
}
