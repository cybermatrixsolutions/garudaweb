<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit_limit extends Model
{
    protected $table ='tbl_customer_limit';

     public function getcustomer(){

        return $this->belongsTo(RoCustomertManagement::class,'Customer_Code','Customer_Code');

    }
}

