<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Nozzle extends Model
{
    protected $table="tbl_pedestal_nozzle_master";
    public $timestamps = false;

    public function getReading(){
    	return $this->hasMany(nozzelsReadingModel::class,'Nozzle_No','Nozzle_Number')->where('RO_code',Auth::user()->getRocode->RO_code);
    }

    public function getItem(){
    	return $this->hasOne(RoPieceListManagement::class,'id','fuel_type')->where('RO_code',Auth::user()->getRocode->RO_code);
    }
   public function getItemname(){
    	return $this->belongsTo(RoPieceListManagement::class,'id','fuel_type');
    }

     public function getTank(){
        return $this->hasOne(TableTankMasterModel::class,'id','Tank_id');
    }
    public function getPadestals(){
        return $this->hasOne(PedestalMaster::class,'id','Pedestal_id')->where('RO_code',Auth::user()->getRocode->RO_code);
    }

}
