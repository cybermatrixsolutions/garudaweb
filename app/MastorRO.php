<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MastorRO extends Model
{
    protected $table = 'tbl_ro_master';

    public function services(){
    	return $this->hasMany(ItemServicesModel::class,'RO_code','RO_code');
    }
    
}