<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Log;

class nozzelsReadingModel extends Model
{
    protected $table = 'tbl_ro_nozzle_reading';
    
      public function getNozzle(){
    	return $this->belongsTo(Nozzle::class,'Nozzle_No','Nozzle_Number')->where('RO_code',Auth::user()->getRocode->RO_code);
    }

    public function getCreditSales(){
    	return $this->hasMany(TransactionModel::class,'shift_id','shift_id')->where('petrol_or_lube',1);
    }

     public function getShift(){
    	return $this->belongsTo(Shift::class,'shift_id','id');
    }
}