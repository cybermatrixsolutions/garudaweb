<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tax_master extends Model
{
   
    protected $table="tbl_tax_master";
    public   $timestamps = false;
}
