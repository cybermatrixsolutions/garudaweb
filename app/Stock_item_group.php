<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock_item_group extends Model
{
	
    protected $table="tbl_stock_item_group";
    public $timestamps = false;

    public function parentName() {
     return $this->belongsTo(self::class, 'parent'); 
    }

    public function children()
	{
    	return $this->hasMany(self::class, 'parent');
	}


    public function getItem()
    {
        return $this->hasMany(RoPieceListManagement::class,'Sub_Stock_Group','id');
    }

     public function getAllitem()
    {
        return $this->hasMany(RoPieceListManagement::class,'Stock_Group','id');
    }


}
