<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoDetails extends Model
{
    protected $table = 'ro_details';
    public $timestamps = false;
}
