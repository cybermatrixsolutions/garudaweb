<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleModel extends Model
{
    protected $table = "tpl_vechile_model";

    public function getmake(){

    	return $this->belongsTo(VehicleMakeModel::class,'make_id','id');
    }
    public function getstockitemname(){

    	return $this->belongsTo(Stock_item_group::class,'fuel_type','id');
    }
   

   
}
