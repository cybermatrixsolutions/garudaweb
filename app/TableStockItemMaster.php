<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableStockItemMaster extends Model
{
    protected $table = "tbl_stock_item_master";
}
