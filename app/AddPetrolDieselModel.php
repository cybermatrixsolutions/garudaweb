<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddPetrolDieselModel extends Model
{
    protected $table="tbl_customer_request_petroldiesel";
    public $timestamps = false;

     public function itemname()
    {
     return $this->belongsTo(RoPieceListManagement::class,'Item_id','id');
    }
    public function priceLists()
   {
     return $this->belongsTo(FuelPriceManagementModel::class,'PetrolDiesel_Type','item_id')->where('is_active',1);
   }

   public function getCustomer()
    {
     return $this->belongsTo(RoCustomertManagement::class,'customer_code','Customer_Code');
    }

    public function getTransaction()
    {
     return $this->hasOne(TransactionModel::class,'Request_id','request_id');
    }

     public function getVehicle()
    {
     return $this->belongsTo(VechilManage::class,'Vehicle_Reg_No','Registration_Number');
    }
    
}
