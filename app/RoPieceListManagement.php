<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoPieceListManagement extends Model
{
    protected $table="tbl_price_list_master";
    public $timestamps = false;

    public function price(){
      return $this->hasOne(TableItemPriceListModel::class,'item_id','id')->where('is_active',1);
    }

    public function itemPrices(){
      return $this->hasMany(TableItemPriceListModel::class,'item_id','id');
    }

    public function gettex(){
      return $this->belongsToMany(tax_master::class,'tbl_itemprice_tax','item_id','tax_id')->withPivot('effective_date');
    }
    public function getgroup(){
      return $this->belongsTo(Stock_item_group::class,'Stock_Group','id');
    }

    public function getsubgroup(){
      return $this->belongsTo(Stock_item_group::class,'Sub_Stock_Group','id');
    }

    public function UnitOfMeasure(){
      return $this->belongsTo(UnitModel::class,'Unit_of_Measure','id');
    }
}
