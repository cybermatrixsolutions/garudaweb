<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxAllocation extends Model
{
    protected $table="tbl_itemprice_tax";
    public   $timestamps = false;
}
