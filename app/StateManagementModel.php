<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StateManagementModel extends Model
{
    protected $table="states";
}