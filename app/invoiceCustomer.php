<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoiceCustomer extends Model
{
   protected $table ='invoice';

    public function getcustomer(){

        return $this->belongsTo(RoCustomertManagement::class,'Customer_name','Customer_Code');

    }

    public function getro(){

        return $this->belongsTo(MastorRO::class,'Ro_code','RO_code');

    }
}
