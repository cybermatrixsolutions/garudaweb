<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentmodesattlementModel extends Model
{
 
    protected $table ='paymentmode_sattlement';
    public $timestamps = false;

     public function getPaymentsName(){
    	return $this->hasMany(PaymentModel::class,'sattlement_id','shift_id');
    }

}

