<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftPedestal extends Model
{
    protected $table="shift_pedestal";
    public $timestamps = false;

   public function getPadestalno(){
    return $this->belongsTo(PedestalMaster::class,'id','shift_id','pedestal_id','id');
   }
   
}