<?php

namespace App\Listeners;

use App\Events\CreaditLimit;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Credit_limit;
use App\TransactionModel;
use App\CustomerLubeRequest;
use App\AddPetrolDieselModel;


class CreaditLimit
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct($cu)
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreaditLimit  $event
     * @return void
     */
    public function handle(CreaditLimit $event)
    {
         $CreditLimits=Credit_limit::get();

         foreach ($CreditLimits as $CreditLimit) {

            $this->updateCreaditLimit($CreditLimit->id,$CreditLimit->Ro_code,$CreditLimit->Customer_Code,$CreditLimit->Limit_Value);
         }

    }

    /**
    * update Creadit Limit
    * @param char RoCode
    * @param char customerCode
    * @return int
    */
    public function updateCreaditLimit($id,$RoCode,$CustomerCode,$creaditLimit){
        
          $UnSettledTrangectionAmount      =  $this->getTrangectionUnSettlement($RoCode,$CustomerCode);
          $getPendingLubRequestAmount      =  $this->getPendingLubRequest($CustomerCode);
          $getPendingFuelRequestAmount     =  $this->getPendingFuelRequest($CustomerCode);

          $available_creadit_limit         =  $creaditLimit-($UnSettledTrangectionAmount+$getPendingLubRequestAmount+$getPendingFuelRequestAmount);
          
          Credit_limit::where('id',$id)->update(['unsettled_bill'=>$UnSettledTrangectionAmount,'pending_fuel_request'=>$getPendingFuelRequestAmount,'pending_other_request'=>$getPendingLubRequestAmount,'available_creadit_limit'=>$available_creadit_limit]);

      return true;
    }

    /**
    * get get Trangection UnSettlement
    * @param char RoCode
    * @param char customerCode
    * @return int
    */
    public function getTrangectionUnSettlement($RoCode,$CustomerCode){

                $transactionModels =TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
                                    ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
                                    ->where('tbl_customer_transaction.RO_code',$RoCode)
                                    ->where('tbl_customer_transaction.customer_code',$CustomerCode)
                                    ->where('tbl_customer_transaction.status','<>',2)
                                    ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode')
                                    ->get();

                    $amountTrajection=0;
                    foreach ($transactionModels as $transactionModel) {

                        if($transactionModel->petrol_or_lube==1){
                          $amountTrajection=$amountTrajection+($transactionModel->item_price*$transactionModel->petroldiesel_qty);
                        }else{
                          $amountTrajection=$amountTrajection+$transactionModel->item_price;
                        }
                    }


        return $amountTrajection;
    }

    /**
    * get pending Lub Request
    * @param char customerCode
    * @return int
    */
    public function getPendingLubRequest($customerCode){

               $CustomerLubeRequests=CustomerLubeRequest::where('customer_code',$customerCode)->where('Execution_date',null)->get();
                
                $PendingLubeREAmount=0;
                foreach ($CustomerLubeRequests as $CustomerLubeRequest) {
                    
                    if($CustomerLubeRequest->priceLists!=null)
                     $PendingLubeREAmount=($CustomerLubeRequest->priceLists->price*$CustomerLubeRequest->quantity)+$PendingLubeREAmount;
                }

        return $PendingLubeREAmount;
    }

    /**
    * get pending fuel request
    * @param char customerCode
    * @return int
    */
    public function getPendingFuelRequest($customerCode){

               $pemdingFuelRes=AddPetrolDieselModel::where('customer_code',$cus)->where('Execution_date',null)->get();
                $PendingFuelREAmount=0;
                
                foreach ($pemdingFuelRes as $pemdingFuelRe) {
                    
                    if($pemdingFuelRe->priceLists!=null){
                      
                        if($pemdingFuelRe->Request_Type!='Full Tank'){

                            $PendingFuelREAmount=($pemdingFuelRe->priceLists->price*$pemdingFuelRe->quantity)+$PendingFuelREAmount;
                        }else{
                            
                            $PendingFuelREAmount=($pemdingFuelRe->priceLists->price*$pemdingFuelRe->getVehicle->capacity)+$PendingFuelREAmount;
                        }
                    }
                }

        return $PendingFuelREAmount;
    }
}
