<?php

namespace App\Listeners;

use App\Events\CreaditLimit;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Credit_limit;
use App\TransactionModel;
use App\CustomerLubeRequest;
use App\AddPetrolDieselModel;
use App\invoiceCustomer;
use Log;
class CreaditLimitListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreaditLimit  $event
     * @return void
     */
    public function handle(CreaditLimit $event)
    {
         $CreditLimits=Credit_limit::get();

         foreach ($CreditLimits as $CreditLimit) {

           $this->updateCreaditLimit($CreditLimit->id,$CreditLimit->Ro_code,$CreditLimit->Customer_Code,$CreditLimit->Limit_Value);
         }
    }


    /**
    * update Creadit Limit
    * @param char RoCode
    * @param char customerCode
    * @return int
    */
    public function updateCreaditLimit($id,$RoCode,$CustomerCode,$creaditLimit){
        
        $UnSettledTrangectionAmount      =  $this->getTrangectionUnSettlement($RoCode,$CustomerCode);
        $getPendingLubRequestAmount      =  $this->getPendingLubRequest($CustomerCode);
        $getPendingFuelRequestAmount     =  $this->getPendingFuelRequest($CustomerCode);
        $getLastSaleDate                 =  $this->getLastSaleDate($RoCode,$CustomerCode);
        $getTotalSale                    =  $this->getTotalSale($RoCode,$CustomerCode);
        $getCurrentUnbilled              =  $this->getCurrentUnbilled($RoCode,$CustomerCode);
        $getLastBillDate                 =  $this->getLastBillDate($RoCode,$CustomerCode);
        
        
        $available_creadit_limit         =  $creaditLimit-($UnSettledTrangectionAmount+$getPendingLubRequestAmount+$getPendingFuelRequestAmount);
        
        Credit_limit::where('id',$id)->update([
                                                'unsettled_bill'          =>$UnSettledTrangectionAmount,
                                                'pending_fuel_request'    =>$getPendingFuelRequestAmount,
                                                'pending_other_request'   =>$getPendingLubRequestAmount,
                                                'available_creadit_limit' =>$available_creadit_limit,
                                                'last_sale_date'          =>$getLastSaleDate['date'],
                                                'last_sale'               =>$getLastSaleDate['sale'],
                                                'total_sale'              =>$getTotalSale,
                                                'current_unbilled'        =>$getCurrentUnbilled,
                                                'last_bill_Os_date'     =>$getLastBillDate,

                                                ]);

      return true;
    }

    /**
    * get getLastSaleDate
    * @param char RoCode
    * @param char customerCode
    * @return array
    */
    
    public function getLastSaleDate($RoCode,$CustomerCode){

        $transactionModel =TransactionModel::where('RO_code',$RoCode)->where('customer_code',$CustomerCode)->orderBy('trans_date', 'desc')->first();
        
       
        $data=['date'=>null,'sale'=>0];
      
         if($transactionModel!=null){

            $data['date']=$transactionModel->trans_date;

            $data['sale'] =$this->getLastSaleValue($RoCode,$CustomerCode,$data['date']);
         }

      return $data;
    }

    /**
    * get last_sale_date
    * @param char RoCode
    * @param char customerCode
    * @return int
    */
    
    public function getLastSaleValue($RoCode,$CustomerCode,$date){

             $transactionModels =TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
                              ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
                              ->where('tbl_customer_transaction.RO_code',$RoCode)
                              ->where('tbl_customer_transaction.customer_code',$CustomerCode)
                              ->whereBetween('tbl_customer_transaction.trans_date', [date('Y-m-d',strtotime($date))." 00:00:00", date('Y-m-d',strtotime($date))." 23:59:59"])
      
                              ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode')
                              ->get();

              $amountTrajection=0;
              foreach ($transactionModels as $transactionModel) {

                  if($transactionModel->petrol_or_lube==1){
                    $amountTrajection=$amountTrajection+($transactionModel->item_price*$transactionModel->petroldiesel_qty);
                  }else{
                    $amountTrajection=$amountTrajection+$transactionModel->item_price;
                  }
              }


        return $amountTrajection;
    }

    /**
    * get last_sale_date
    * @param char RoCode
    * @param char customerCode
    * @return int
    */
    
    public function getLastBillDate($RoCode,$CustomerCode){

        $transactionModel =invoiceCustomer::where('Ro_code',$RoCode)->where('Customer_name',$CustomerCode)->where('settlement_status',1)->orderBy('invoice_date', 'desc')->first();
          
          $date=null;
        
           if($transactionModel!=null)
            $date=$transactionModel->invoice_date;


        return $date;
    }
    /**
    * get total_sale
    * @param char RoCode
    * @param char customerCode
    * @return int
    */
    
    public function getTotalSale($RoCode,$CustomerCode){

        $transactionModels =TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
                            ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
                            ->where('tbl_customer_transaction.RO_code',$RoCode)
                            ->where('tbl_customer_transaction.customer_code',$CustomerCode)
                            ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode')
                            ->get();

            $amountTrajection=0;
            foreach ($transactionModels as $transactionModel) {

                if($transactionModel->petrol_or_lube==1){
                  $amountTrajection=$amountTrajection+($transactionModel->item_price*$transactionModel->petroldiesel_qty);
                }else{
                  $amountTrajection=$amountTrajection+$transactionModel->item_price;
                }
            }


        return $amountTrajection;
    }

     /**
    * get total_sale
    * @param char RoCode
    * @param char customerCode
    * @return int
    */
    
    public function getCurrentUnbilled($RoCode,$CustomerCode){

          $transactionModels =TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
                              ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
                              ->where('tbl_customer_transaction.RO_code',$RoCode)
                              ->where('tbl_customer_transaction.customer_code',$CustomerCode)
                              ->where('tbl_customer_transaction.status',0)
                              ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode')
                              ->get();

              $amountTrajection=0;
              foreach ($transactionModels as $transactionModel) {

                  if($transactionModel->petrol_or_lube==1){
                    $amountTrajection=$amountTrajection+($transactionModel->item_price*$transactionModel->petroldiesel_qty);
                  }else{
                    $amountTrajection=$amountTrajection+$transactionModel->item_price;
                  }
              }


        return $amountTrajection;
    }

    /**
    * get get Trangection UnSettlement
    * @param char RoCode
    * @param char customerCode
    * @return int
    */

    public function getTrangectionUnSettlement($RoCode,$CustomerCode){

          $transactionModels =TransactionModel::join('tbl_price_list_master', 'tbl_customer_transaction.item_code', '=', 'tbl_price_list_master.id')
                              ->join('tbl_stock_item_group', 'tbl_price_list_master.Stock_Group', '=', 'tbl_stock_item_group.id')
                              ->where('tbl_customer_transaction.RO_code',$RoCode)
                              ->where('tbl_customer_transaction.customer_code',$CustomerCode)
                              ->where('tbl_customer_transaction.status','<>',2)
                              ->select('tbl_customer_transaction.*', 'tbl_stock_item_group.Group_Name', 'tbl_price_list_master.Item_Name','tbl_price_list_master.hsncode')
                              ->get();

              $amountTrajection=0;
              foreach ($transactionModels as $transactionModel) {

                  if($transactionModel->petrol_or_lube==1){
                    $amountTrajection=$amountTrajection+($transactionModel->item_price*$transactionModel->petroldiesel_qty);
                  }else{
                    $amountTrajection=$amountTrajection+$transactionModel->item_price;
                  }
              }


        return $amountTrajection;
    }

    /**
    * get pending Lub Request
    * @param char customerCode
    * @return int
    */
    public function getPendingLubRequest($customerCode){

         $CustomerLubeRequests=CustomerLubeRequest::where('customer_code',$customerCode)->where('Execution_date',null)->get();
          
          $PendingLubeREAmount=0;
          foreach ($CustomerLubeRequests as $CustomerLubeRequest) {
              
              if($CustomerLubeRequest->priceLists!=null)
               $PendingLubeREAmount=($CustomerLubeRequest->priceLists->price*$CustomerLubeRequest->quantity)+$PendingLubeREAmount;
          }

        return $PendingLubeREAmount;
    }

    /**
    * get pending fuel request
    * @param char customerCode
    * @return int
    */
    public function getPendingFuelRequest($customerCode){
         Log::info('CreditLimit@getPendingFuelRequest customerCode - '.$customerCode);
         $pemdingFuelRes=AddPetrolDieselModel::where('customer_code',$customerCode)->where('Execution_date',null)->get();
         
          $PendingFuelREAmount=0;
          
          foreach ($pemdingFuelRes as $pemdingFuelRe) {
              
              if($pemdingFuelRe->priceLists!=null){
                
                          $capacity = 0;
                  if($pemdingFuelRe->Request_Type!='Full Tank'){


                 //Log::info('$pemdingFuelRe  in letter ----'.print_r($pemdingFuelRe,true));

                      /*$PendingFuelREAmount=($pemdingFuelRe->priceLists->price*$pemdingFuelRe->quantity)+$PendingFuelREAmount;
*/
                      Log::info('$pemdingFuelRe  in ltr AddPetrolDieselModel record id----'.print_r($pemdingFuelRe->id,true));
                      $price=$pemdingFuelRe->priceLists->price;
                      Log::info('$price ======'.$price);
                      $quantity=$pemdingFuelRe->Request_Value;
                      Log::info('$pemdingFuelRe->quantity $quantity ======'.$quantity);
                      $PendingFuelREAmount=($price*$quantity)+$PendingFuelREAmount;
                      Log::info('$PendingFuelREAmount ======'.$PendingFuelREAmount);
                  }else{

                    Log::info('$pemdingFuelRe  full tank AddPetrolDieselModel record id----'.print_r($pemdingFuelRe->id,true));
                      $price=$pemdingFuelRe->priceLists->price;
                      Log::info('$price ======'.$price);

                      if(empty($pemdingFuelRe->getVehicle->capacity))
                       {
                        $capacity = 0;
                       }else{
                          $capacity=$pemdingFuelRe->getVehicle->capacity;
                       }
                      Log::info('$capacity ======'.$capacity);
                      $PendingFuelREAmount=($price*$capacity)+$PendingFuelREAmount;
                      Log::info('$PendingFuelREAmount ======'.$PendingFuelREAmount);
                  }
              }
          }

        return $PendingFuelREAmount;
    }
}
