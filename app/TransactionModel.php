<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionModel extends Model
{
    protected $table="tbl_customer_transaction";
    public   $timestamps = false;

    function getro(){
    	return $this->belongsTo(RoMaster::class,'RO_code','RO_code');
    }

     function getCustomerMaster(){
    	return $this->belongsTo(RoCustomertManagement::class,'customer_code','Customer_Code');
    }
     function getCustomername(){
        return $this->belongsTo(RoCustomertManagement::class,'customer_code','Customer_Code');
    }

    function getItemName(){
    	return $this->belongsTo(RoPieceListManagement::class,'item_code','id');
    }
     function getname(){
        return $this->belongsTo(RoPieceListManagement::class,'Fuel_Types','id');
    }
   
    public function gettex(){
        return $this->hasMany(TransTaxModel::class,'transaction_id','id');
    }

    public function getShift(){
        return $this->belongsTo(Shift::class,'shift_id','id');
    }


    public function getlubeRequest(){
        return $this->hasMany(CustomerLubeRequest::class,'request_id','Request_id');
    }

    public function getfuelRequest(){
        return $this->belongsTo(AddPetrolDieselModel::class,'Request_id','request_id');
    }

    public function getTrangections(){
        return $this->hasOne(RoPersonalManagement::class,'id','Trans_By');
   }

    public function getdrivercustomer(){
        return $this->hasOne(CustomerDriverManager::class,'Driver_Mobile','Driver_Mobile');
   }

   
}
