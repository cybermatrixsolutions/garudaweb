<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerLubeRequest extends Model
{
   protected $table ="tbl_customer_request_lube";

   public function priceList()
   {
     return $this->belongsTo(RoPieceListManagement::class,'Item_id','item_id');
   }

    public function priceLists()
   {
     return $this->belongsTo(FuelPriceManagementModel::class,'item_id','Item_id')->where('is_active',1);
   }

    public function itemname()
   {
     return $this->belongsTo(RoPieceListManagement::class,'item_id','id');
   }

    public function getCustomer()
   {
     return $this->belongsTo(RoCustomertManagement::class,'customer_code','Customer_Code');
   }

   public function getTransaction()
    {
     return $this->hasMany(TransactionModel::class,'Request_id','request_id');
    }
}
