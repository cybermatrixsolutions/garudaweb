<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TankInward extends Model
{
    protected $table ='tbl_tank_inward';

    public function tanks(){

    	return $this->belongsToMany(TableTankMasterModel::class,'tank_tankinwart','tankinwart_id','tank_id')->withPivot('id','value');
    }

    public function fuel(){
    	return $this->belongsTo(RoPieceListManagement::class,'fuel_type','id');
    }

    public function shift(){
    	return $this->belongsTo(Shift::class,'shift_id','id');
    }
}
