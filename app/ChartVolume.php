<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChartVolume extends Model
{
    
    protected $table ='dipchartvolume';

    public function Dipchart()
    {
    	return $this->belongsTo(Dipchart::class,'dipchart_id','id');
    }
}
