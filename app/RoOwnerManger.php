<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoOwnerManger extends Model
{
    protected $table = "tbl_ro_owner_master";
    public $timestamps = false;
}
