<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftTank extends Model
{
    protected $table="shift_tank";
    public $timestamps = false;
}
