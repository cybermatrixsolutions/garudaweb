<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftType extends Model
{
     protected $table ='ro_shift_config';

     public $timestamps = false;
}
