<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoLOBItemSelectionModel extends Model
{
    protected $table = "tbl_lob_master";
    // public $timestamps = false;
}
