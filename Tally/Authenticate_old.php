<?php
include("connection.php");

header('Content-type: text/xml');
$postData = file_get_contents('php://input');



$xml = simplexml_load_string($postData);





$doc = new DOMDocument();
$doc->loadXML($postData);

function truncate_number( $number, $precision = 2) {
    // Zero causes issues, and no need to truncate
    if ( 0 == (int)$number ) {
        return $number;
    }
    // Are we negative?
    $negative = $number / abs($number);
    // Cast the number to a positive to solve rounding
    $number = abs($number);
    // Calculate precision number for dividing / multiplying
    $precision = pow(10, $precision);
    // Run the math, re-applying the negative value to ensure returns correctly negative / positive
    return floor( $number * $precision ) / $precision * $negative;
}





$books = $doc->getElementsByTagName("HEADER");
foreach ($books as $book) 
{
    
    $TALLYREQUEST   = $book->getElementsByTagName("TALLYREQUEST");
    $TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;
    
    
    
    
    $DISTID   = $book->getElementsByTagName("DISTID");
    $DISTID_S = $DISTID->item(0)->nodeValue;
    
    $DISTPASS   = $book->getElementsByTagName("DISTPASS");
    $DISTPASS_S = $DISTPASS->item(0)->nodeValue;
    
    $rocode = $DISTID_S;
    $secret = $DISTPASS_S;
    
    
}

if ($TALLYREQUEST_S == 'DISTRIBUTOR REGISTRATION REQUEST') 
{
    
    
    $sql = "select t1.RO_code,pump_legal_name,pump_address,address_2,address_3,t4.name as state,t5.name as city,
		Contact_Person_Name,Contact_Person_Email,Contact_Person_Mobile,website,GST_TIN,VAT_TIN,PAN_No
		from tbl_ro_master t1  inner join states t4 on 
		 t4.id=t1.state inner join cities t5 on t5.id=t1.city 
		 
		 inner join ro_details t6 on t6.ro_id=t1.id
		 where RO_code='$rocode' and secret_key='$secret' and t1.tally_update_status=0";
    
    
    
    
    $res1 = mysqli_query($con, $sql);
    
    $count = mysqli_num_rows($res1);
    
    
    while ($rows = mysqli_fetch_assoc($res1)) 
	{
        
        $RO_code         = $rows['RO_code'];
        $pump_legal_name = $rows['pump_legal_name'];
        $pump_address    = $rows['pump_address'];
        $address_2       = $rows['address_2'];
        $address_3       = $rows['address_3'];
        $state           = $rows['state'];
        $city            = $rows['city'];
        
        $Contact_Person_Name   = $rows['Contact_Person_Name'];
        $Contact_Person_Email  = $rows['Contact_Person_Email'];
        $Contact_Person_Mobile = $rows['Contact_Person_Mobile'];
        $website               = $rows['website'];
        $GST_TIN               = $rows['GST_TIN'];
        $VAT_TIN               = $rows['VAT_TIN'];
        $PAN_No                = $rows['PAN_No'];
        
    }
    
    
    
    $simplexml = "<ENVELOPE>
 <HEADER>
  <TALLYREQUEST>$TALLYREQUEST_S</TALLYREQUEST>
  <DISTID>$RO_code</DISTID>
  <DISTPASS>$DISTPASS_S</DISTPASS>
 </HEADER>
 <DATA>
   <DISTRIBUTORDATA>
    <DISTID>$RO_code</DISTID>
    <DISTNAME>$pump_legal_name</DISTNAME>
    <DISTADDRESS1>$pump_address</DISTADDRESS1>
    <DISTADDRESS2>$address_2</DISTADDRESS2>
    <DISTADDRESS3>$address_3</DISTADDRESS3>
    <DISTSTATE>$state</DISTSTATE>
    <DISTOCOUNTRY>INDIA</DISTOCOUNTRY>
	
	<DISTCONTACT>$Contact_Person_Name</DISTCONTACT>
    <DISTPHONE></DISTPHONE>
    <DITMOBILE>$Contact_Person_Mobile</DITMOBILE>
    <DISTEMAIL>$Contact_Person_Email</DISTEMAIL>
    <DISTWEBSITE>$website</DISTWEBSITE>
    <DISTGSTIN>$GST_TIN</DISTGSTIN>
	<DISTVATTIN>$VAT_TIN</DISTVATTIN>
	<DISTPANNO>$PAN_No</DISTPANNO>
	<DISTCINNO></DISTCINNO>
	
	
   </DISTRIBUTORDATA>
  </DATA>
</ENVELOPE>";
    
    
    
    header('Content-type: text/xml');
    print_r($simplexml);
    
    
} 


else if ($TALLYREQUEST_S == 'CUSTOMER DOWNLOAD REQUEST') 

{
    

    $sql = "select t1.RO_code,Customer_Code,company_name,address_one,address_two,address_three,t4.name as state,t5.name as city,t1.pin_no,t1.pan_no,t1.gst_no,
	t1.Email,t1.Mobile,t1.Phone_Number,t1.vat,Customer_Name,company_code,t1.id
	from tbl_customer_master t1  inner join states t4 on 
		 t4.id=t1.state inner join cities t5 on t5.id=t1.city 
		 
		 inner join tbl_ro_master t6 on t6.RO_code=t1.RO_code
		 
		 where t1.RO_code='$DISTID_S' and t1.tally_update_status=0 and t1.is_active=1";
    
    
 //print_r($sql);
    
    
    $res1 = mysqli_query($con, $sql);
    
    $count = mysqli_num_rows($res1);
    
    
    $simplexml = "<ENVELOPE>
 <HEADER>
  <TALLYREQUEST>CUSTOMER DOWNLOAD</TALLYREQUEST>
  <DISTID>$DISTID_S</DISTID>
  <DISTPASS>$DISTPASS_S</DISTPASS>
 </HEADER>
 <DATA>";
    
    
    
    
    
    while ($rows = mysqli_fetch_assoc($res1)) 
	{
		 $id            = $rows['id'];
		 
        $ro            = $rows['RO_code'];
        $company_name  = $rows['company_name'];
        $Customer_Code = $rows['Customer_Code'];
        $company_code  = $rows['company_code'];
        $address_one   = $rows['address_one'];
        $address_two   = $rows['address_two'];
        $address_three = $rows['address_three'];
        $state         = $rows['state'];
        $pin_no        = $rows['pin_no'];
        $Phone_Number  = $rows['Phone_Number'];
        $Mobile        = $rows['Mobile'];
        $Email         = $rows['Email'];
        $ro            = $rows['RO_code'];
        $gst_no        = $rows['gst_no'];
        $vat           = $rows['vat'];
        $pan_no        = $rows['pan_no'];
        $Customer_Name = $rows['Customer_Name'];
		
		if ($gst_no!='' || $gst_no!=null)
		{
			$gsttype='REGULAR';
		}
		else
		{
			$gsttype='UNREGISTERED';
		}
		
		if ($vat!='' || $vat!=null)
		{
			$vattype='REGULAR';
		}
		else
		{
			$vattype='UNREGISTERED';
		}
        
        
        $simplexml = $simplexml . "
   <CUSTOMERDATA>
    <DISTID>$ro</DISTID>
    <CUSTOMERNAME>$company_name</CUSTOMERNAME>
    <CUSTOMERCODE>$Customer_Code</CUSTOMERCODE>
    <CUSTOMERPARENT>$company_code</CUSTOMERPARENT>
    <CUSTADDRESS1>$address_one</CUSTADDRESS1>
    <CUSTADDRESS2>$address_two</CUSTADDRESS2>
    <CUSTADDRESS3>$address_three</CUSTADDRESS3>
    <CUSTADDRESS4></CUSTADDRESS4>
    <CUSTSTATE>$state</CUSTSTATE>
    <CUSTCOUNTRY>INDIA</CUSTCOUNTRY>
	<CUSTPINCODE>$pin_no</CUSTPINCODE>
	<CUSTCONTACT>$Customer_Name</CUSTCONTACT>
    <CUSTPHONE>$Phone_Number</CUSTPHONE>
    <CUSTMOBILE>$Mobile</CUSTMOBILE>
    <CUSTEMAIL>$Email</CUSTEMAIL>
    <CUSTWEBSITE></CUSTWEBSITE>
	<GSTREGTYPE>$gsttype</GSTREGTYPE>
	<GSTPARTYTYPE></GSTPARTYTYPE>
	<CUSTGSTIN>$gst_no</CUSTGSTIN>
	<VATDEALERTYPE>$vattype</VATDEALERTYPE>
    <CUSTVATTIN>$vat</CUSTVATTIN>
	<CUSTPANNO>$pan_no</CUSTPANNO>
	<ISBILLWISEON>YES</ISBILLWISEON>
   </CUSTOMERDATA>";
   
   
  
        
        
        
        
    }
    
    $simplexml = $simplexml . "</DATA>
</ENVELOPE>";
    
     header('Content-type: text/xml');
    print_r($simplexml);
    
}

else if ($TALLYREQUEST_S == 'PRODUCT DOWNLOAD REQUEST') 
{
    
    
    $sql = "select t1.id ,t1.RO_code,Item_Code,Item_Name,Unit_Symbol as Unit_of_Measure,Group_Name as Stock_Group,
	
	DATE_FORMAT(t1.Active_From_Date, '%d-%M-%Y') as Active_From_Date,price,hsncode,t1.Volume_ltr from tbl_price_list_master t1  
	
	inner join tbl_stock_item_group t2 on t2.id=t1.Stock_Group

inner join tbl_ro_master t3 on t3.RO_code=t1.RO_Code

 inner join tbl_item_price_list t4 on t4.item_id=t1.id and t4.is_active=1
 
  inner join tbl_unit_of_measure t5 on t5.id=t1.Unit_of_Measure 
 
		where t1.RO_code='$rocode' and t1.tally_update_status=0 and t1.is_active=1 ";
		
		//print_r($sql);
		//exit();
    
    
  
    
    $res1 = mysqli_query($con, $sql);
    
    $count = mysqli_num_rows($res1);
    
    
    $simplexml = "<ENVELOPE>
 <HEADER>
  <TALLYREQUEST>PRODUCT DOWNLOAD</TALLYREQUEST>
  <DISTID>$DISTID_S</DISTID>
  <DISTPASS>$DISTPASS_S</DISTPASS>
 </HEADER>
 <DATA>";
    
    
    
    
    
    while ($rows = mysqli_fetch_assoc($res1)) 
	{
        
        $id               = $rows['id'];
        $ro               = $rows['RO_code'];
        $Item_Code        = $rows['Item_Code'];
        $Item_Name        = $rows['Item_Name'];
        $Unit_of_Measure  = $rows['Unit_of_Measure'];
        $Stock_Group      = $rows['Stock_Group'];
        $Active_From_Date = $rows['Active_From_Date'];
        $price            = $rows['price'];
        $hsncode          = $rows['hsncode'];
		$stock            = $rows['Volume_ltr'];
        
        
    $simplexml = $simplexml . "
	<PRODUCTDATA>
    <DISTID>$ro</DISTID>
    <PRODUCTCODE>$Item_Code</PRODUCTCODE>
	<PRODUCTNAME>$Item_Name</PRODUCTNAME>
    <PRODUCTPARENT>$Stock_Group</PRODUCTPARENT>
    <PRODUCTUOM>$Unit_of_Measure</PRODUCTUOM>
	<PRODOPQTY>$stock</PRODOPQTY>
    <PRODOPRATE></PRODOPRATE>
    <PRODOPAMOUNT></PRODOPAMOUNT>
	<EFFECTIVEDATE>$Active_From_Date</EFFECTIVEDATE>
	<MRPRATE>$price</MRPRATE>
	<VATTYPEOFGOODS></VATTYPEOFGOODS>
	<VATTAXABILITY></VATTAXABILITY>";
        
        $sql123 = "select * from tbl_itemprice_tax t1 inner join tbl_tax_master t2 on t1.tax_id=t2.id where item_id='$id'";
        
        $res123 = mysqli_query($con, $sql123);
		
		  //print_r($sql);
	//exit();
        
        while ($rows123 = mysqli_fetch_assoc($res123)) 
		{
            $Type = $rows123['GST_Type'];
            
            
            if ($Type == 'CGST') {
                $CGST_RATE = $rows123['Tax_percentage'];
                
            } else if ($Type == 'UT/SGST') {
                $SGST_RATE = $rows123['Tax_percentage'];
                
            } else if ($Type == 'IGST') {
                $IGST_RATE = $rows123['Tax_percentage'];
                
            } else if ($Type == 'VAT') {
                $VATPERCENT = $rows123['Tax_percentage'];
                
            } else if ($Type == 'ADVAT') {
                $ADDLVATRATE = $rows123['Tax_percentage'];
                
            } else if ($Type == 'CSVAT') {
                $CESSONVATRATE = $rows123['Tax_percentage'];
                
            } else if ($Type == 'SCCESS') {
                $CESS_RATE = $rows123['Tax_percentage'];
                
            }
            
        }
        
        if ($CGST_RATE != '' || $SGST_RATE != '' || $IGST_RATE != '') {
            $GST = 'YES';
        } else {
            $GST = 'NO';
        }
        
        $simplexml = $simplexml ."
		<VATRATE>$VATPERCENT</VATRATE>
		<VATADDLRATE>$ADDLVATRATE</VATADDLRATE>
		<VATCESSRATE>$CESSONVATRATE</VATCESSRATE>
		<ISGSTAPPLICABLE>$GST</ISGSTAPPLICABLE>
		<TYPEOFSUPPLY>GOODS</TYPEOFSUPPLY>
		<HSNSACCODE>$hsncode</HSNSACCODE>
		<GSTCALCTYPE>ON VALUE</GSTCALCTYPE>
	   <GSTTAXABILITY>TAXABLE</GSTTAXABILITY>
	  <IGSTRATE>$IGST_RATE</IGSTRATE>
	<CGSTRATE>$CGST_RATE</CGSTRATE>
	<SGSTRATE>$SGST_RATE</SGSTRATE>
	<GSTCESSRATE>$CESS_RATE</GSTCESSRATE>";
	
	 $sql1234 = "select * from tbl_tank_master  where fuel_type='$id'";
	 //print_r($sql1234);
        
     $res1234 = mysqli_query($con, $sql1234);
	 
	 while ($rows1234 = mysqli_fetch_assoc($res1234)) 
	 {
	
	 $simplexml = $simplexml . "<BATCHALLOCATIONS>
		<PRODBATCH>Primary Batch</PRODBATCH>
       		<PRODGODOWN>".$rows1234['Tank_Number']."</PRODGODOWN>
       		<BATCHOPQTY>".$rows1234['Opening_Reading']."</BATCHOPQTY>
       		<BATCHOPRATE></BATCHOPRATE>
    		<BATCHOPAMOUNT></BATCHOPAMOUNT>	
	</BATCHALLOCATIONS>";
	 }
	
	
	 $simplexml = $simplexml ."</PRODUCTDATA>";
	 
	
        
    }
    
    $simplexml = $simplexml . "</DATA>
</ENVELOPE>";


		//$sql = "update tbl_price_list_master set tally_update_status=1 where RO_code='$rocode'";
	
	    //$res1 = mysqli_query($con, $sql);


 header('Content-type: text/xml');
    print_r($simplexml);
    
    
}

else if ($TALLYREQUEST_S == 'SALES DOWNLOAD REQUEST') 
{
	$rtamt=0;
	
      $sql = "select t1.id,t1.RO_code,DATE_FORMAT(t1.trans_date, '%d-%M-%Y') as trans_date,invoice_number,t1.customer_code,company_name,t3.address_one, t3.address_two,t3.address_three, t7.name as state,t8.name as city,t3.gst_no,t3.vat,sum(item_price* petroldiesel_qty) as amount from tbl_customer_transaction t1 inner join tbl_ro_master t2 on t1.RO_code=t2.RO_code inner join tbl_customer_master t3 on t3.Customer_Code=t1.customer_code inner join states t4 on t4.id=t2.state inner join cities t5 on t5.id=t2.city inner join tbl_price_list_master t6 on t6.id=t1.item_code inner join states t7 on t7.id=t3.state inner join cities t8 on t8.id=t3.city where t1.RO_code='$DISTID_S' and t1.tally_update_status=0  group by invoice_number";
    
    $res1 = mysqli_query($con, $sql);
    
    $count = mysqli_num_rows($res1);
    
    $simplexml = "<ENVELOPE>
 <HEADER>
  <TALLYREQUEST>SALES DOWNLOAD</TALLYREQUEST>
  <DISTID>$DISTID_S</DISTID>
  <DISTPASS>$DISTPASS_S</DISTPASS>
 </HEADER>
 <DATA>";
    
    
    while ($rows = mysqli_fetch_assoc($res1)) 
	{
       //$Totalvch=$Totalvch+ $rows['amount'];
	   
	   $tid=$rows['id'];
        
        $simplexml = $simplexml . "
   <SALESDATA>
    <DISTID>".$rows['RO_code']."</DISTID>
    <TRANSID>".$rows['invoice_number']."</TRANSID>
	<TRANSTYPE>SALES</TRANSTYPE>
	<TRANSDATE>".$rows['trans_date']."</TRANSDATE>
	<PARTYCODE>".$rows['customer_code']."</PARTYCODE>
    <PARTYNAME>".$rows['company_name']."</PARTYNAME>
    <BILLTONAME>".$rows['company_name']."</BILLTONAME>
    <BILLTOADDRESS1>".$rows['address_one']."</BILLTOADDRESS1>
    <BILLTOADDRESS2>".$rows['address_two']."</BILLTOADDRESS2>
    <BILLTOADDRESS3>".$rows['address_three']."</BILLTOADDRESS3>
	<BILLTOADDRESS4></BILLTOADDRESS4>
	<BILLTOSTATE>".$rows['state']."</BILLTOSTATE>
    <BILLTOCOUNTRY>INDIA</BILLTOCOUNTRY>
	<BILLTOGSTIN>".$rows['gst_no']."</BILLTOGSTIN>
	<BILLTOVATTIN>".$rows['vat']."</BILLTOVATTIN>
	<PLACEOFSUPPLY>".$rows['state']."</PLACEOFSUPPLY>
	<SHIPTONAME>".$rows['company_name']."</SHIPTONAME>
	<SHIPTOADDRESS1>".$rows['address_one']."</SHIPTOADDRESS1>
	<SHIPTOADDRESS2>".$rows['address_two']."</SHIPTOADDRESS2>
	<SHIPTOADDRESS3>".$rows['address_three']."</SHIPTOADDRESS3>
	<SHIPTOADDRESS4></SHIPTOADDRESS4>
	<SHIPTOSTATE>".$rows['state']."</SHIPTOSTATE>
	<SHIPTOCOUNTRY>INDIA</SHIPTOCOUNTRY>
	<SHIPTOGSTIN>".$rows['gst_no']."</SHIPTOGSTIN>
	<SHIPTOVATTIN>".$rows['vat']."</SHIPTOVATTIN>
	<VCHTOTALAMT>".truncate_number(($rows['amount']),2) ."</VCHTOTALAMT>
	<NARRATION></NARRATION>
	<BILLALLOCATIONS>
		<BILLTYPE>NEW REF</BILLTYPE>
		<BILLNAME>".$rows['invoice_number']."</BILLNAME>
		<BILLDUEDATE></BILLDUEDATE>
		<BILLAMOUNT>".truncate_number(($rows['amount']),2) ."</BILLAMOUNT>
	</BILLALLOCATIONS>";
	
	$sqlitem = "select t1.id,t1.RO_code,DATE_FORMAT(t1.trans_date, '%d-%M-%Y') as trans_date,invoice_number,t1.customer_code,company_name,t3.address_one,
	  t3.address_two,t3.address_three,

t7.name as state,t8.name as city,t3.gst_no,t3.vat,(item_price*petroldiesel_qty) as amount,t6.Item_Code,t6.Item_Name,item_price as item_price_act,
item_price,petroldiesel_qty,petrol_or_lube
	from tbl_customer_transaction t1 inner join tbl_ro_master t2 on t1.RO_code=t2.RO_code
    inner join tbl_customer_master t3 on t3.Customer_Code=t1.customer_code
	inner join states t4 on 
		 t4.id=t2.state inner join cities t5 on t5.id=t2.city 
		 inner join tbl_price_list_master t6 on t6.id=t1.item_code
		 
		 inner join states t7 on 
		 t7.id=t3.state inner join cities t8 on t8.id=t3.city 
	
	where t1.RO_code='$DISTID_S' and t1.tally_update_status=0 and invoice_number='".$rows['invoice_number']."'";
		
		
        
    $resitem = mysqli_query($con, $sqlitem);
        
    while ($rowsitem = mysqli_fetch_assoc($resitem)) 
    {
		
	 $tid=$rowsitem['id'];
	
	 $simplexml = $simplexml . "
	<INVENTORYENTRIES>
	<ITEMCODE>".$rowsitem['Item_Code']."</ITEMCODE>
	<ITEMNAME>".$rowsitem['Item_Name']."</ITEMNAME>
    <ITEMQTY>".$rowsitem['petroldiesel_qty']."</ITEMQTY>";
	
	$sqlpricebtax = "select * from item_transaction_tax where transaction_id='".$rowsitem['id']."'";
	
        
    $resbtax = mysqli_query($con, $sqlpricebtax);
	
	 $itembtax=0;
	 
	 $taxper=0;
	 $itembtaxamount=0;
        
    while ($rowsbtax = mysqli_fetch_assoc($resbtax)) 
	{
		$itembtax=$itembtax+$rowsbtax['Tax_Value'];
		$taxper=$taxper+$rowsbtax['Tax_percentage'];
	}
     
	 //$itembtaxamount=$rowsitem['item_price_act']-$itembtax;
	 
	 $itembtaxamount=$rowsitem['item_price_act']/(1+$taxper/100);
	 
	 $itembtaxamount=truncate_number($itembtaxamount,2);
	 
	 $ITEMAMOUNT=$itembtaxamount*$rowsitem['petroldiesel_qty'];
	 
	 
	
     $simplexml = $simplexml."<ITEMMRP>".$itembtaxamount."</ITEMMRP>
    <ITEMRATE>".$itembtaxamount."</ITEMRATE>
    <ITEMDISCOUNT></ITEMDISCOUNT>
    <ITEMAMOUNT>".$ITEMAMOUNT."</ITEMAMOUNT>
    <HSNSAC>".$rowsitem['hsncode']."</HSNSAC>";
        
        $sql123 = "select * from item_transaction_tax where transaction_id='".$rowsitem['id']."'";
		
		//echo $sql123;
		//exit();
        
        $res123 = mysqli_query($con, $sql123);
        
        while ($rows123 = mysqli_fetch_assoc($res123)) 
		{
            
            if ($rows123['GST_Type'] == 'VAT') 
			{
                  $vatrate=$rows123['Tax_percentage'];
				  //$vatamount=$rowsitem['petroldiesel_qty']*$rows123['Tax_Value'];
				  
				  $vatamount=truncate_number($ITEMAMOUNT*$rows123['Tax_percentage']/100,2);
            }
			            
            if ($rows123['GST_Type'] == 'ADVAT') 
			{
                 $advatrate=$rows123['Tax_percentage'];
				  //$advatamount=$rowsitem['petroldiesel_qty']*$rows123['Tax_Value'];
				  
				  $advatamount=truncate_number($ITEMAMOUNT*$rows123['Tax_percentage']/100,2);
                
            }
			
			
            
            if ($rows123['GST_Type'] == 'SCVAT') 
			{
                  $scvatrate=$rows123['Tax_percentage'];
				  $scvatamount=truncate_number($ITEMAMOUNT*$rows123['Tax_percentage']/100,2);
            }
			
			
			
			if ($rows123['GST_Type'] == 'IGST') 
			{
                $igstrate=$rows123['Tax_percentage'];
				  $igstamt=truncate_number($ITEMAMOUNT*$rows123['Tax_percentage']/100,2);
            }
			
			
			
			if ($rows123['GST_Type'] == 'CGST') 
			{
                $cgstrate=$rows123['Tax_percentage'];
				  $cgstamt=truncate_number($ITEMAMOUNT*$rows123['Tax_percentage']/100,2);
            }
			
			
			
			if ($rows123['GST_Type'] == 'UT/SGST') 
			{
                $sgstrate=$rows123['Tax_percentage'];
				  $sgstamt=truncate_number($ITEMAMOUNT*$rows123['Tax_percentage']/100,2);
            }
			
		
			if ($rows123['GST_Type'] == 'SPCESS') 
			{
                $gstcessrate=$rows123['Tax_percentage'];
				  $gstcessamt=truncate_number($sgstamt=$ITEMAMOUNT*$rows123['Tax_percentage']/100,2);
            }
			
			
			
            
            
        }
		
		
    $simplexml = $simplexml."<VATRATE>$vatrate</VATRATE>
	<VATAMT>$vatamount</VATAMT>";
     $simplexml = $simplexml."<VATADDLRATE>$advatrate</VATADDLRATE>
	<VATADDLAMT>$advatamount</VATADDLAMT>";
    $simplexml = $simplexml."<VATCESSRATE>$scvatrate</VATCESSRATE>
	<VATCESSAMT>$scvatamount</VATCESSAMT>";
    $simplexml = $simplexml."<IGSTRATE>$igstrate</IGSTRATE>
    <IGSTAMT>$igstamt</IGSTAMT>";
    $simplexml = $simplexml."<CGSTRATE>$cgstrate</CGSTRATE>
    <CGSTAMT>$cgstamt</CGSTAMT>";
    $simplexml = $simplexml."<SGSTRATE>$sgstrate</SGSTRATE>
	<SGSTAMT>$sgstamt</SGSTAMT>";
    $simplexml = $simplexml."<GSTCESSRATE>$gstcessrate</GSTCESSRATE>
	<GSTCESSAMT>$gstcessamt </GSTCESSAMT>";
       
       
$itemtotal=$vatamount+$advatamount+$scvatamount+$igstamt+$cgstamt+$sgstamt+$gstcessamt;
		
		$itemtotal=$itemtotal+($itembtaxamount*$rowsitem['petroldiesel_qty']);
        $roundamt=$rowsitem['amount']-$itemtotal;
		
		
		$rtamt=$rtamt+ $roundamt;

	   $simplexml = $simplexml . "<ROUNDOFFAMT>".round($roundamt,2)."</ROUNDOFFAMT>";
		
		
		
		
		
	$simplexml = $simplexml ."<ITEMTOTALAMT>$itemtotal</ITEMTOTALAMT>
	<ITEMACCLEDGER>LOCAL SALES</ITEMACCLEDGER>";
	
	if ($rowsitem['petrol_or_lube']==1)
	{
	
		$sqltank = "Select * from tbl_tank_master where 
		RO_code='$DISTID_S' and fuel_type='".$rowsitem['Item_Code']."'";
			        
		$restank = mysqli_query($con, $sqltank);
		
		while ($rowstank = mysqli_fetch_assoc($restank)) 
		{
	
	
			$simplexml = $simplexml . "<BATCHALLOCATIONS>
		<PRODORDERNO></PRODORDERNO>
		<PRODTRACKINGNO></PRODTRACKINGNO>
		<PRODBATCH>Primary Batch</PRODBATCH>
		<PRODGODOWN>".$rowstank['Tank_Number']."</PRODGODOWN>
		<BATCHQTY>".$rowsitem['petroldiesel_qty']."</BATCHQTY>
		<BATCHRATE>".$itembtaxamount."</BATCHRATE>
		<BATCHDISCOUNT></BATCHDISCOUNT>
		<BATCHAMOUNT>".$ITEMAMOUNT."</BATCHAMOUNT>
			</BATCHALLOCATIONS>";
			}
		
		
	}
	else
	{
		$simplexml = $simplexml . "<BATCHALLOCATIONS>
		<PRODORDERNO></PRODORDERNO>
		<PRODTRACKINGNO></PRODTRACKINGNO>
		<PRODBATCH>Primary Batch</PRODBATCH>
		<PRODGODOWN>GODOWN1</PRODGODOWN>
		<BATCHQTY>".$rowsitem['petroldiesel_qty']."</BATCHQTY>
		<BATCHRATE>".$itembtaxamount."</BATCHRATE>
		<BATCHDISCOUNT></BATCHDISCOUNT>
		<BATCHAMOUNT>".$ITEMAMOUNT."</BATCHAMOUNT>
		</BATCHALLOCATIONS>";
	}
        
    
	
	
	
	
   $simplexml = $simplexml . "</INVENTORYENTRIES>";
   
   
   
   
   
   
	}
        
        
		
	
		
		
        
        $sql1234 = "select GST_Type,sum(Tax_Value) as Tax_Value,petroldiesel_qty  from item_transaction_tax t1 inner join tbl_customer_transaction t2 on t1.transaction_id=t2.id where invoice_number='".$rows['invoice_number']."' group by GST_Type";
		
		//echo $sql1234;
        
        $res1234 = mysqli_query($con, $sql1234);
        
        while ($rows1234 = mysqli_fetch_assoc($res1234)) 
		{
            
            $simplexml = $simplexml ."<LEDGERENTRIES>
					<LEDGERNAME>".$rows1234['GST_Type']."</LEDGERNAME>
					<AMOUNT>".round($rows1234['Tax_Value'],2)."</AMOUNT>
				</LEDGERENTRIES>";
        }
		
		  $simplexml = $simplexml . "<LEDGERENTRIES>
                <LEDGERNAME>Rounded off </LEDGERNAME>
                <AMOUNT>".round($rtamt,2)."</AMOUNT>
            </LEDGERENTRIES>";
        
        
    }
    
    $simplexml = $simplexml . "</SALESDATA></DATA>
</ENVELOPE>";

 header('Content-type: text/xml');
    print_r($simplexml);
    
   
}


else if ($TALLYREQUEST_S == 'RECEIPT DOWNLOAD REQUEST') 
{
    
    
    $sql = "select DATE_FORMAT(t1.created_at, '%d-%M-%Y') as created_at,t1.id,t2.Personnel_Name as shift_manager,Nozzle_Amount  from shifts t1 inner join tbl_personnel_master t2 on t1.shift_manager=t2.id 
	inner join tbl_shift_settlement t3 on t3.shift_id=t1.id where fuel=1 and t1.tally_update_status=0 and t1.ro_code='$DISTID_S'";
    
    
   
    
    $res1 = mysqli_query($con, $sql);
    
    $count = mysqli_num_rows($res1);
    
    
    $simplexml = "<ENVELOPE>
 <HEADER>
  <TALLYREQUEST>RECEIPT DOWNLOAD</TALLYREQUEST>
  <DISTID>$DISTID_S</DISTID>
  <DISTPASS>$DISTPASS_S</DISTPASS>
 </HEADER>
 <DATA>";
    
    
    
    
    
    while ($rows = mysqli_fetch_assoc($res1)) 
	{
        
        $shift_id               = $rows['id'];
		
		
		
        $created_at       = $rows['created_at'];
        $shift_manager    = $rows['shift_manager'];
		$Nozzle_Amount    = $rows['Nozzle_Amount'];
      
        $Nozzle_Amount=truncate_number($Nozzle_Amount,2);
        
    $simplexml = $simplexml ."
	<RECEIPTDATA>
<DISTID>$DISTID_S</DISTID>
<TRANSID>$shift_id</TRANSID>
<TRANSTYPE>Receipt</TRANSTYPE>
<TRANSDATE>$created_at</TRANSDATE>
<PARTYCODE></PARTYCODE>
<PARTYNAME>Daily Sales Control A/c</PARTYNAME>
<NARRATION>settlement dt  $created_at shift by $shift_manager</NARRATION>
<LEDGERENTRIES>
<LEDGERNAME>Daily Sales Control A/c</LEDGERNAME>
<AMOUNT>$Nozzle_Amount</AMOUNT>
<BILLALLOCATIONS>
<BILLTYPE></BILLTYPE>
<BILLNAME></BILLNAME>
<BILLAMOUNT></BILLAMOUNT>
</BILLALLOCATIONS>
</LEDGERENTRIES>";


	
   $sql123 = "select * from shifts t1 inner join tbl_personnel_master t2 on t1.shift_manager=t2.id 
	inner join tbl_shift_settlement t3 on t3.shift_id=t1.id
	inner join paymentmode_sattlement t4 on t4.sattlement_id=t3.id
	inner join tpl_payment_mode t5 on t5.id=t4.paymentmode_id
	where fuel=1 and t1.ro_code='$DISTID_S' and t1.id='$shift_id' ";
	
	
	//print_r($sql );
	//exit();
    
    $res123 = mysqli_query($con, $sql123);
    
    $amt=0;
	
    while ($rows123 = mysqli_fetch_assoc($res123)) 
	{

			$amt=$amt+$rows123['amount'];
			
			$simplexml = $simplexml ."<LEDGERENTRIES><LEDGERNAME>".$rows123['name']."</LEDGERNAME>
			<AMOUNT>-".$rows123['amount']."</AMOUNT>
			<BANKALLOCATIONS>
			<DATE>$created_at</DATE>
			<INSTRUMENTNO>".$rows123['ref_no']."</INSTRUMENTNO>
			<INSTRUMENTDATE>$created_at</INSTRUMENTDATE>
			<TRANSACTIONTYPE>Cheque/DD</TRANSACTIONTYPE>
			<PAYMENTFAVOURING>Daily Sales Control A/c</PAYMENTFAVOURING>
			<BANKPARTYNAME>Daily Sales Control A/c</BANKPARTYNAME>
			<AMOUNT>-".$rows123['amount']."</AMOUNT>
			</BANKALLOCATIONS>
			</LEDGERENTRIES>";
			
			
			
			
	}
	
	$diff=$Nozzle_Amount-$amt;
	
	
	$simplexml = $simplexml ."<LEDGERENTRIES><LEDGERNAME>$shift_manager</LEDGERNAME><AMOUNT>$diff</AMOUNT></LEDGERENTRIES>";
	
	 $simplexml = $simplexml . "</RECEIPTDATA>";
	
	
	
	}
    
    
   
	
	 $simplexml = $simplexml . "</DATA></ENVELOPE>";
	 
	 
	 
    header('Content-type: text/xml');
    print_r($simplexml);
    
    
}

else if ($TALLYREQUEST_S == 'SALES ACKNOWLEDGEMENT') 
{
	
	$books = $doc->getElementsByTagName("DATA");
	foreach ($books as $book) 
	{
    
		$TALLYREQUEST   = $book->getElementsByTagName("SALESDATA");
		$TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;
    
    
    
    
		$DISTID   = $book->getElementsByTagName("DISTID");
		$DISTID_S = $DISTID->item(0)->nodeValue;
    
		$TRANSID   = $book->getElementsByTagName("TRANSID");
		$TRANSID_S = $TRANSID->item(0)->nodeValue;
    
		$SUCCESS   = $book->getElementsByTagName("SUCCESS");
		$SUCCESS_S = $SUCCESS->item(0)->nodeValue;
		
		if ($SUCCESS_S=="YES")
		{
		
			$sql1234 = "update tbl_customer_transaction set tally_update_status=1 
			where invoice_number='$TRANSID_S'";
		
			//echo $sql1234;
        
			$res1234 = mysqli_query($con, $sql1234);
		}
    
    
	}
	
}

else if ($TALLYREQUEST_S == 'RECEIPT ACKNOWLEDGEMENT') 
{
	
	$books = $doc->getElementsByTagName("DATA");
	foreach ($books as $book) 
	{
    
		$TALLYREQUEST   = $book->getElementsByTagName("SALESDATA");
		$TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;
    
    
    
    
		$DISTID   = $book->getElementsByTagName("DISTID");
		$DISTID_S = $DISTID->item(0)->nodeValue;
    
		$TRANSID   = $book->getElementsByTagName("TRANSID");
		$TRANSID_S = $TRANSID->item(0)->nodeValue;
    
		$SUCCESS   = $book->getElementsByTagName("SUCCESS");
		$SUCCESS_S = $SUCCESS->item(0)->nodeValue;
		
		if ($SUCCESS_S=="YES")
		{
		
			$sql1234 = "update shifts set tally_update_status=1 
			where id='$TRANSID_S'";
		
			//echo $sql1234;
        
			$res1234 = mysqli_query($con, $sql1234);
		}
    
    
	}
	
}

else if ($TALLYREQUEST_S == 'PRODUCT ACKNOWLEDGEMENT') 
{
	
	$books = $doc->getElementsByTagName("DATA");
	foreach ($books as $book) 
	{
    
		$TALLYREQUEST   = $book->getElementsByTagName("SALESDATA");
		$TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;
    
    
    
    
		$DISTID   = $book->getElementsByTagName("DISTID");
		$DISTID_S = $DISTID->item(0)->nodeValue;
    
		$TRANSID   = $book->getElementsByTagName("PRODUCTCODE");
		$TRANSID_S = $TRANSID->item(0)->nodeValue;
    
		$SUCCESS   = $book->getElementsByTagName("SUCCESS");
		$SUCCESS_S = $SUCCESS->item(0)->nodeValue;
		
		if ($SUCCESS_S=="YES")
		{
		
			//$sql1234 = "update shifts set tally_update_status=1 
			//where id='$TRANSID_S'";
			
			
			$sql1234 = "update tbl_price_list_master set tally_update_status=1 where 
			Item_Code='$TRANSID_S' and RO_code='$DISTID_S'";
	
	  
		
			//echo $sql1234;
        
			$res1234 = mysqli_query($con, $sql1234);
		}
    
    
	}
	
}

else if ($TALLYREQUEST_S == 'CUSTOMER ACKNOWLEDGEMENT') 
{
	
	$books = $doc->getElementsByTagName("DATA");
	foreach ($books as $book) 
	{
    
		$TALLYREQUEST   = $book->getElementsByTagName("SALESDATA");
		$TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;
    
    
    
    
		$DISTID   = $book->getElementsByTagName("DISTID");
		$DISTID_S = $DISTID->item(0)->nodeValue;
    
		$TRANSID   = $book->getElementsByTagName("CUSTOMERCODE");
		$TRANSID_S = $TRANSID->item(0)->nodeValue;
    
		$SUCCESS   = $book->getElementsByTagName("SUCCESS");
		$SUCCESS_S = $SUCCESS->item(0)->nodeValue;
		
		if ($SUCCESS_S=="YES")
		{
		
			
			//$sql1234 = "update tbl_price_list_master set tally_update_status=1 where id='$TRANSID_S'";
	
	        $sql1234 = "update tbl_customer_master set tally_update_status=1 where Customer_Code='$TRANSID_S'";
	
			//$res1 = mysqli_query($con, $sql);
		
			//echo $sql1234;
        
			$res1234 = mysqli_query($con, $sql1234);
		}
    
    
	}
	
}

else if ($TALLYREQUEST_S == 'DISTRIBUTOR REGISTRATION ACKNOWLEDGEMENT') 
{
	
	$books = $doc->getElementsByTagName("DATA");
	foreach ($books as $book) 
	{
    
		$TALLYREQUEST   = $book->getElementsByTagName("SALESDATA");
		$TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;
    
    
    
    
		$DISTID   = $book->getElementsByTagName("DISTID");
		$DISTID_S = $DISTID->item(0)->nodeValue;
    
		//$TRANSID   = $book->getElementsByTagName("TRANSID");
		//$TRANSID_S = $TRANSID->item(0)->nodeValue;
    
		$SUCCESS   = $book->getElementsByTagName("SUCCESS");
		$SUCCESS_S = $SUCCESS->item(0)->nodeValue;
		
		if ($SUCCESS_S=="YES")
		{
		
			
			//$sql1234 = "update tbl_price_list_master set tally_update_status=1 where id='$TRANSID_S'";
	
	        $sql1234 = "update tbl_ro_master set tally_update_status=1 where RO_code='$DISTID_S'";
	
			//$res1 = mysqli_query($con, $sql);
		
			//echo $sql1234;
        
			$res1234 = mysqli_query($con, $sql1234);
		}
    
    
	}
	
}











?>