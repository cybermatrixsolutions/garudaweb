<?php

ini_set('max_execution_time', 0);
ini_set('memory_limit', -1);
set_time_limit(0);

include("connection.php");



header('Content-type: text/xml');

$postData = file_get_contents('php://input');



$xml = simplexml_load_string($postData);



$doc = new DOMDocument();

$doc->loadXML($postData);



function truncate_number( $number, $precision = 2) {

    // Zero causes issues, and no need to truncate

    if ( 0 == (int)$number ) {

        return $number;

    }

    // Are we negative?

    $negative = $number / abs($number);

    // Cast the number to a positive to solve rounding

    $number = abs($number);

    // Calculate precision number for dividing / multiplying

    $precision = pow(10, $precision);

    // Run the math, re-applying the negative value to ensure returns correctly negative / positive

    return floor( $number * $precision ) / $precision * $negative;

}











$books = $doc->getElementsByTagName("HEADER");

foreach ($books as $book) 

{

    

    $TALLYREQUEST   = $book->getElementsByTagName("TALLYREQUEST");

    $TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;

    

    

    

    

    $DISTID   = $book->getElementsByTagName("DISTID");

    $DISTID_S = $DISTID->item(0)->nodeValue;

    

    $DISTPASS   = $book->getElementsByTagName("DISTPASS");

    $DISTPASS_S = $DISTPASS->item(0)->nodeValue;

    

    $rocode = $DISTID_S;

    $secret = $DISTPASS_S;

    

    

}



if ($TALLYREQUEST_S == 'DISTRIBUTOR REGISTRATION REQUEST') 

{

    

    

    $sql = "select t1.RO_code,pump_legal_name,pump_address,address_2,address_3,t4.name as state,t5.name as city,

		Contact_Person_Name,Contact_Person_Email,Contact_Person_Mobile,website,GST_TIN,VAT_TIN,PAN_No

		from tbl_ro_master t1  inner join states t4 on 

		 t4.id=t1.state inner join cities t5 on t5.id=t1.city 

		 

		 inner join ro_details t6 on t6.ro_id=t1.id

		 where RO_code='$rocode' and secret_key='$secret' and t1.tally_update_status=0";

    

    

    

    

    $res1 = mysqli_query($con, $sql);

    

    $count = mysqli_num_rows($res1);

    

    

    while ($rows = mysqli_fetch_assoc($res1)) 

	{

        

        $RO_code         = str_replace("&","&amp;",$rows['RO_code']);

        $pump_legal_name = str_replace("&","&amp;",$rows['pump_legal_name']);

        $pump_address    = str_replace("&","&amp;",$rows['pump_address']); 

        $address_2       =  str_replace("&","&amp;",$rows['address_2']);

        $address_3       =  str_replace("&","&amp;",$rows['address_3']);

        $state           =  str_replace("&","&amp;",$rows['state']);

        $city            =  str_replace("&","&amp;",$rows['city']); 

        
        $Contact_Person_Name   =  str_replace("&","&amp;",$rows['Contact_Person_Name']); 

        $Contact_Person_Email  =  str_replace("&","&amp;",$rows['Contact_Person_Email']); 

        $Contact_Person_Mobile =  str_replace("&","&amp;",$rows['Contact_Person_Mobile']);

        $website               = str_replace("&","&amp;",$rows['website']);

        $GST_TIN               = str_replace("&","&amp;",$rows['GST_TIN']); 

        $VAT_TIN               = str_replace("&","&amp;",$rows['VAT_TIN']); 

        $PAN_No                = str_replace("&","&amp;",$rows['PAN_No']); 

        

    }

    

    $simplexml = "<ENVELOPE>

 <HEADER>

  <TALLYREQUEST>$TALLYREQUEST_S</TALLYREQUEST>

  <DISTID>$RO_code</DISTID>

  <DISTPASS>$DISTPASS_S</DISTPASS>

 </HEADER>

 <DATA>

   <DISTRIBUTORDATA>

    <DISTID>$RO_code</DISTID>

    <DISTNAME>$pump_legal_name</DISTNAME>

    <DISTADDRESS1>$pump_address</DISTADDRESS1>

    <DISTADDRESS2>$address_2</DISTADDRESS2>

    <DISTADDRESS3>$address_3</DISTADDRESS3>

    <DISTSTATE>$state</DISTSTATE>

    <DISTOCOUNTRY>INDIA</DISTOCOUNTRY>

	

	<DISTCONTACT>$Contact_Person_Name</DISTCONTACT>

    <DISTPHONE></DISTPHONE>

    <DITMOBILE>$Contact_Person_Mobile</DITMOBILE>

    <DISTEMAIL>$Contact_Person_Email</DISTEMAIL>

    <DISTWEBSITE>$website</DISTWEBSITE>

    <DISTGSTIN>$GST_TIN</DISTGSTIN>

	<DISTVATTIN>$VAT_TIN</DISTVATTIN>

	<DISTPANNO>$PAN_No</DISTPANNO>

	<DISTCINNO></DISTCINNO>

	

	

   </DISTRIBUTORDATA>

  </DATA>

</ENVELOPE>";

    

    

    

    header('Content-type: text/xml');

    print_r($simplexml);

    

    

} 





else if ($TALLYREQUEST_S == 'CUSTOMER DOWNLOAD REQUEST') 

{
 

    $sql = "select t1.RO_code,Customer_Code,company_name,address_one,address_two,address_three,t4.name as state,t5.name as city,t1.pin_no,t1.pan_no,t1.gst_no,

	t1.Email,t1.Mobile,t1.Phone_Number,t1.vat,Customer_Name,company_code,t1.id

	from tbl_customer_master t1  inner join states t4 on 

		 t4.id=t1.state inner join cities t5 on t5.id=t1.city 

		 

		 inner join tbl_ro_master t6 on t6.RO_code=t1.RO_code

		 

		 where t1.RO_code='$DISTID_S' and t1.tally_update_status=0 and t1.is_active=1";

    

    

 //print_r($sql);

    

    

    $res1 = mysqli_query($con, $sql);

    

    $count = mysqli_num_rows($res1);

    

    

    $simplexml = "<ENVELOPE>

 <HEADER>

  <TALLYREQUEST>CUSTOMER DOWNLOAD</TALLYREQUEST>

  <DISTID>$DISTID_S</DISTID>

  <DISTPASS>$DISTPASS_S</DISTPASS>

 </HEADER>

 <DATA>";

    

    

    

    

    

    while ($rows = mysqli_fetch_assoc($res1)) 

	{

		 $id            = $rows['id'];

		 

        $ro            = $rows['RO_code'];

        $company_name  = str_replace("&","&amp;",$rows['company_name']); 

        $Customer_Code = str_replace("&","&amp;",$rows['Customer_Code']);

        $company_code  = str_replace("&","&amp;",$rows['company_code']);

        $address_one   = str_replace("&","&amp;",$rows['address_one']); 

        $address_two   = str_replace("&","&amp;",$rows['address_two']); 

        $address_three = str_replace("&","&amp;",$rows['address_three']); 

        $state         = str_replace("&","&amp;",$rows['state']); 

        $pin_no        = str_replace("&","&amp;",$rows['pin_no']);  

        $Phone_Number  = str_replace("&","&amp;",$rows['Phone_Number']);   

        $Mobile        = str_replace("&","&amp;",$rows['Mobile']);

        $Email         = str_replace("&","&amp;",$rows['Email']);

        $ro            =  str_replace("&","&amp;",$rows['RO_code']); 

        $gst_no        = str_replace("&","&amp;",$rows['gst_no']); 

        $vat           =  str_replace("&","&amp;",$rows['vat']);  

        $pan_no        = str_replace("&","&amp;",$rows['pan_no']);  

        $Customer_Name = str_replace("&","&amp;",$rows['Customer_Name']);  

		

		if ($gst_no!='' || $gst_no!=null)

		{

			$gsttype='REGULAR';

		}

		else

		{

			$gsttype='UNREGISTERED';

		}

		

		if ($vat!='' || $vat!=null)

		{

			$vattype='REGULAR';

		}

		else

		{

			$vattype='UNREGISTERED';

		}

        

        

        $simplexml = $simplexml . "

   <CUSTOMERDATA>

    <DISTID>$ro</DISTID>

    <CUSTOMERNAME>$company_name</CUSTOMERNAME>

    <CUSTOMERCODE>$Customer_Code</CUSTOMERCODE>

    <CUSTOMERPARENT>$company_code</CUSTOMERPARENT>

    <CUSTADDRESS1>$address_one</CUSTADDRESS1>

    <CUSTADDRESS2>$address_two</CUSTADDRESS2>

    <CUSTADDRESS3>$address_three</CUSTADDRESS3>

    <CUSTADDRESS4></CUSTADDRESS4>

    <CUSTSTATE>$state</CUSTSTATE>

    <CUSTCOUNTRY>INDIA</CUSTCOUNTRY>

	<CUSTPINCODE>$pin_no</CUSTPINCODE>

	<CUSTCONTACT>$Customer_Name</CUSTCONTACT>

    <CUSTPHONE>$Phone_Number</CUSTPHONE>

    <CUSTMOBILE>$Mobile</CUSTMOBILE>

    <CUSTEMAIL>$Email</CUSTEMAIL>

    <CUSTWEBSITE></CUSTWEBSITE>

	<GSTREGTYPE>$gsttype</GSTREGTYPE>

	<GSTPARTYTYPE></GSTPARTYTYPE>

	<CUSTGSTIN>$gst_no</CUSTGSTIN>

	<VATDEALERTYPE>$vattype</VATDEALERTYPE>

    <CUSTVATTIN>$vat</CUSTVATTIN>

	<CUSTPANNO>$pan_no</CUSTPANNO>

	<ISBILLWISEON>YES</ISBILLWISEON>

   </CUSTOMERDATA>";

   

   

  

        

        

        

        

    }

    

    $simplexml = $simplexml . "</DATA>

</ENVELOPE>";

    

     header('Content-type: text/xml');

    print_r($simplexml);

    

}



else if ($TALLYREQUEST_S == 'PRODUCT DOWNLOAD REQUEST') 

{

    

    

    $sql = "select t1.id ,t1.RO_code,Item_Code,Item_Name,Unit_Symbol as Unit_of_Measure,Group_Name as Stock_Group,

	

	DATE_FORMAT(t1.Active_From_Date, '%d-%M-%Y') as Active_From_Date,price,hsncode,t1.Volume_ltr from tbl_price_list_master t1  

	

	inner join tbl_stock_item_group t2 on t2.id=t1.Stock_Group



inner join tbl_ro_master t3 on t3.RO_code=t1.RO_Code



 inner join tbl_item_price_list t4 on t4.item_id=t1.id and t4.is_active=1

 

  inner join tbl_unit_of_measure t5 on t5.id=t1.Unit_of_Measure 

 

		where t1.RO_code='$rocode' and t1.tally_update_status=0 and t1.is_active=1 ";

		

		//print_r($sql);

		//exit();

    

    

  

    

    $res1 = mysqli_query($con, $sql);

    

    $count = mysqli_num_rows($res1);

    

    

    $simplexml = "<ENVELOPE>

 <HEADER>

  <TALLYREQUEST>PRODUCT DOWNLOAD</TALLYREQUEST>

  <DISTID>$DISTID_S</DISTID>

  <DISTPASS>$DISTPASS_S</DISTPASS>

 </HEADER>

 <DATA>";

    

    

    

    

    

    while ($rows = mysqli_fetch_assoc($res1)) 

	{

        

        $id               = $rows['id'];

        $ro               = $rows['RO_code'];

        $Item_Code        = str_replace("&","&amp;",$rows['Item_Code']); 

        $Item_Name        = str_replace("&","&amp;",$rows['Item_Name']); 

        $Unit_of_Measure  = str_replace("&","&amp;",$rows['Unit_of_Measure']); 

        $Stock_Group      = str_replace("&","&amp;",$rows['Stock_Group']); 

        $Active_From_Date = $rows['Active_From_Date'];

        $price            = $rows['price'];

        $hsncode          = str_replace("&","&amp;",$rows['hsncode']); 

		$stock            =str_replace("&","&amp;",$rows['Volume_ltr']);  

        

        

    $simplexml = $simplexml . "

	<PRODUCTDATA>

    <DISTID>$ro</DISTID>

    <PRODUCTCODE>$Item_Code</PRODUCTCODE>

	<PRODUCTNAME>$Item_Name</PRODUCTNAME>

    <PRODUCTPARENT>$Stock_Group</PRODUCTPARENT>

    <PRODUCTUOM>$Unit_of_Measure</PRODUCTUOM>

	<PRODOPQTY>$stock</PRODOPQTY>

    <PRODOPRATE></PRODOPRATE>

    <PRODOPAMOUNT></PRODOPAMOUNT>

	<EFFECTIVEDATE>$Active_From_Date</EFFECTIVEDATE>

	<MRPRATE>$price</MRPRATE>

	<VATTYPEOFGOODS></VATTYPEOFGOODS>

	<VATTAXABILITY></VATTAXABILITY>";

        

        $sql123 = "select * from tbl_itemprice_tax t1 inner join tbl_tax_master t2 on t1.tax_id=t2.id where item_id='$id'";

        

        $res123 = mysqli_query($con, $sql123);

		

		  //print_r($sql);

	//exit();

        

        while ($rows123 = mysqli_fetch_assoc($res123)) 

		{

            $Type = $rows123['GST_Type'];

            

            

            if ($Type == 'CGST') {

                $CGST_RATE = $rows123['Tax_percentage'];

                

            } else if ($Type == 'UT/SGST') {

                $SGST_RATE = $rows123['Tax_percentage'];

                

            } else if ($Type == 'IGST') {

                $IGST_RATE = $rows123['Tax_percentage'];

                

            } else if ($Type == 'VAT') {

                $VATPERCENT = $rows123['Tax_percentage'];

                

            } else if ($Type == 'ADVAT') {

                $ADDLVATRATE = $rows123['Tax_percentage'];

                

            } else if ($Type == 'CSVAT') {

                $CESSONVATRATE = $rows123['Tax_percentage'];

                

            } else if ($Type == 'SCCESS') {

                $CESS_RATE = $rows123['Tax_percentage'];

                

            }

            

        }

        

        if ($CGST_RATE != '' || $SGST_RATE != '' || $IGST_RATE != '') {

            $GST = 'YES';

        } else {

            $GST = 'NO';

        }

        

        $simplexml = $simplexml ."

		<VATRATE>$VATPERCENT</VATRATE>

		<VATADDLRATE>$ADDLVATRATE</VATADDLRATE>

		<VATCESSRATE>$CESSONVATRATE</VATCESSRATE>

		<ISGSTAPPLICABLE>$GST</ISGSTAPPLICABLE>

		<TYPEOFSUPPLY>GOODS</TYPEOFSUPPLY>

		<HSNSACCODE>$hsncode</HSNSACCODE>

		<GSTCALCTYPE>ON VALUE</GSTCALCTYPE>

	   <GSTTAXABILITY>TAXABLE</GSTTAXABILITY>

	  <IGSTRATE>$IGST_RATE</IGSTRATE>

	<CGSTRATE>$CGST_RATE</CGSTRATE>

	<SGSTRATE>$SGST_RATE</SGSTRATE>

	<GSTCESSRATE>$CESS_RATE</GSTCESSRATE>";

	

	 $sql1234 = "select * from tbl_tank_master  where fuel_type='$id'";

	 //print_r($sql1234);

        

     $res1234 = mysqli_query($con, $sql1234);

	 

	 $countbatch = mysqli_num_rows($res1234);

	 

	 if ($countbatch==0)

	 {

		 

		  $simplexml = $simplexml . "<BATCHALLOCATIONS>

		<PRODBATCH>Primary Batch</PRODBATCH>

       		<PRODGODOWN></PRODGODOWN>

       		<BATCHOPQTY></BATCHOPQTY>

       		<BATCHOPRATE></BATCHOPRATE>

    		<BATCHOPAMOUNT></BATCHOPAMOUNT>	

	</BATCHALLOCATIONS>";

	

	 }

	 

	 while ($rows1234 = mysqli_fetch_assoc($res1234)) 

	 {

	

	 $simplexml = $simplexml . "<BATCHALLOCATIONS>

		<PRODBATCH>Primary Batch</PRODBATCH>

       		<PRODGODOWN>".str_replace("&","&amp;",$rows1234['Tank_Number'])."</PRODGODOWN>

       		<BATCHOPQTY>".$rows1234['Opening_Reading']."</BATCHOPQTY>

       		<BATCHOPRATE></BATCHOPRATE>

    		<BATCHOPAMOUNT></BATCHOPAMOUNT>	

	</BATCHALLOCATIONS>";

	 }

	

	

	 $simplexml = $simplexml ."</PRODUCTDATA>";

	 

	

        

    }

    

    $simplexml = $simplexml . "</DATA>

</ENVELOPE>";





		//$sql = "update tbl_price_list_master set tally_update_status=1 where RO_code='$rocode'";

	

	    //$res1 = mysqli_query($con, $sql);





 header('Content-type: text/xml');

    print_r($simplexml);

    

    

}


else if ($TALLYREQUEST_S == 'GSTBILL DOWNLOAD REQUEST') 

{
	
	$rtamt=0;
	$taxtotal=0;
	
 $sql = "select t1. *,t2. *,t3. *,t4. *,t5.name as state, t6.name as city,t7.name as rostate from invoice t1 inner join invoice_billing t2 on t1.invoice_no=t2.invoice_no inner join tbl_ro_master t3 on t1.Ro_code=t3.RO_code left join tbl_customer_master t4 on t4. Customer_Code=t1.Customer_name left join states t5 on t5.id=t4.state left join cities t6 on t6.id=t4.city 
 left join states t7 on t7.id=t3.state
 
 where t1.Ro_code='$DISTID_S' and t1.tally_update_status=0 and bill_amount>0 and tax_type=2";
$res2 = mysqli_query($con, $sql);

$simplexml = "<ENVELOPE>

 <HEADER>

  <TALLYREQUEST>GSTBILL DOWNLOAD</TALLYREQUEST>

  <DISTID>$DISTID_S</DISTID>

  <DISTPASS>$DISTPASS_S</DISTPASS>

 </HEADER>

 <DATA>";


    while ($rows = mysqli_fetch_assoc($res2)) 

	{
		
		
	   $inv=$rows['invoice_no'];
	   
	     $created_at= $rows['invoice_date'];
		
		 $newdate = date("d-M-Y", strtotime($created_at));
		
		//$created_at =date_format($created_at,"d-M-Y");

        //$shift_manager    = str_replace("&","&amp;",$rows['shift_manager']) ;

        

        $simplexml = $simplexml . "

   <SALESDATA>

    <DISTID>".$DISTID_S."</DISTID>

    <TRANSID>".$inv."</TRANSID>

	<TRANSTYPE>GST Bill</TRANSTYPE>

	<TRANSDATE>".$newdate."</TRANSDATE>
	
	
	<PARTYCODE>".str_replace("&","&amp;",$rows['customer_code'])."</PARTYCODE>

    <PARTYNAME>".str_replace("&","&amp;",$rows['company_name'])."</PARTYNAME>

    <BILLTONAME>".str_replace("&","&amp;",$rows['company_name'])."</BILLTONAME>

    <BILLTOADDRESS1>".str_replace("&","&amp;",$rows['address_one'])."</BILLTOADDRESS1>

    <BILLTOADDRESS2>".str_replace("&","&amp;",$rows['address_two'])."</BILLTOADDRESS2>

    <BILLTOADDRESS3>".str_replace("&","&amp;",$rows['address_three'])."</BILLTOADDRESS3>

	<BILLTOADDRESS4></BILLTOADDRESS4>

	<BILLTOSTATE>".str_replace("&","&amp;",$rows['state'])."</BILLTOSTATE>

    <BILLTOCOUNTRY>INDIA</BILLTOCOUNTRY>

	<BILLTOGSTIN>".str_replace("&","&amp;",$rows['gst_no'])."</BILLTOGSTIN>

	<BILLTOVATTIN>".str_replace("&","&amp;",$rows['vat'])."</BILLTOVATTIN>

	<PLACEOFSUPPLY>".str_replace("&","&amp;",$rows['state'])."</PLACEOFSUPPLY>

	<SHIPTONAME>".str_replace("&","&amp;",$rows['company_name'])."</SHIPTONAME>

	<SHIPTOADDRESS1>".str_replace("&","&amp;",$rows['address_one'])."</SHIPTOADDRESS1>

	<SHIPTOADDRESS2>".str_replace("&","&amp;",$rows['address_two'])."</SHIPTOADDRESS2>

	<SHIPTOADDRESS3>".str_replace("&","&amp;",$rows['address_three'])."</SHIPTOADDRESS3>

	<SHIPTOADDRESS4></SHIPTOADDRESS4>

	<SHIPTOSTATE>".str_replace("&","&amp;",$rows['state'])."</SHIPTOSTATE>

	<SHIPTOCOUNTRY>INDIA</SHIPTOCOUNTRY>

	<SHIPTOGSTIN>".str_replace("&","&amp;",$rows['gst_no'])."</SHIPTOGSTIN>

	<SHIPTOVATTIN>".str_replace("&","&amp;",$rows['vat'])."</SHIPTOVATTIN>

	<VCHTOTALAMT>".$rows['bill_amount']."</VCHTOTALAMT>

	<NARRATION>Bill upto</NARRATION>

	<BILLALLOCATIONS>

		<BILLTYPE>NEW REF</BILLTYPE>

		<BILLNAME>".$inv."</BILLNAME>

		<BILLDUEDATE></BILLDUEDATE>

		<BILLAMOUNT>".$rows['bill_amount']."</BILLAMOUNT>

	</BILLALLOCATIONS>";

    $simplexml = $simplexml . "

	<INVENTORYENTRIES>

	<ITEMCODE></ITEMCODE>

	<ITEMNAME></ITEMNAME>

    <ITEMQTY></ITEMQTY>";
    $simplexml = $simplexml."<ITEMMRP></ITEMMRP>";
	

    $simplexml = $simplexml."<ITEMRATE></ITEMRATE>

    <ITEMDISCOUNT></ITEMDISCOUNT>

    <ITEMAMOUNT></ITEMAMOUNT>

    <HSNSAC></HSNSAC>";

   
    $simplexml = $simplexml."<VATRATE></VATRATE>

	<VATAMT></VATAMT>";

     $simplexml = $simplexml."<VATADDLRATE></VATADDLRATE>

	<VATADDLAMT></VATADDLAMT>";

    $simplexml = $simplexml."<VATCESSRATE></VATCESSRATE>

	<VATCESSAMT></VATCESSAMT>";

    $simplexml = $simplexml."<IGSTRATE></IGSTRATE>

    <IGSTAMT></IGSTAMT>";

    $simplexml = $simplexml."<CGSTRATE></CGSTRATE>

    <CGSTAMT></CGSTAMT>";

    $simplexml = $simplexml."<SGSTRATE></SGSTRATE>

	<SGSTAMT></SGSTAMT>";

    $simplexml = $simplexml."<GSTCESSRATE></GSTCESSRATE>

	<GSTCESSAMT></GSTCESSAMT>";
       
	
	$simplexml = $simplexml . "<ROUNDOFFAMT></ROUNDOFFAMT>";

	
	$simplexml = $simplexml ."<ITEMTOTALAMT></ITEMTOTALAMT>

	<ITEMACCLEDGER>LOCAL SALES</ITEMACCLEDGER>";

	
	$simplexml = $simplexml . "<BATCHALLOCATIONS>

		<PRODORDERNO></PRODORDERNO>

		<PRODTRACKINGNO></PRODTRACKINGNO>

		<PRODBATCH>Primary Batch</PRODBATCH>

		<PRODGODOWN></PRODGODOWN>

		<BATCHQTY></BATCHQTY>

		<BATCHRATE></BATCHRATE>

		<BATCHDISCOUNT></BATCHDISCOUNT>

		<BATCHAMOUNT></BATCHAMOUNT>

		</BATCHALLOCATIONS>";

   $simplexml = $simplexml . "</INVENTORYENTRIES>";
   
	

          
 $simplexml = $simplexml ."<LEDGERENTRIES>

					<LEDGERNAME>Billing Charges</LEDGERNAME>
					

					<AMOUNT>".round(($rows['handling_amount']),2) ."</AMOUNT>

				</LEDGERENTRIES>";
				
				if ($rows['state']==$rows['rostate'])
				{
					
				$cgstamt=round($rows['handling_amount']*9/100,2);
				$sgstamt=round($rows['handling_amount']*9/100,2);
				
				
	$simplexml = $simplexml . "<LEDGERENTRIES>

	<LEDGERNAME>CGST</LEDGERNAME>
	<AMOUNT>$cgstamt</AMOUNT>


</LEDGERENTRIES>
<LEDGERENTRIES>

	<LEDGERNAME>UT/SGST</LEDGERNAME>
	<AMOUNT>$sgstamt</AMOUNT>


</LEDGERENTRIES>";
				}
				else
				
				{
				
				$igstamt=round($rows['handling_amount']*18/100,2);
				
				$simplexml = $simplexml . "<LEDGERENTRIES>

	<LEDGERNAME>IGST</LEDGERNAME>
	<AMOUNT>$igstamt</AMOUNT>


</LEDGERENTRIES>";
				
				
				
				}


		  $simplexml = $simplexml . "<LEDGERENTRIES>

                <LEDGERNAME>Rounded off </LEDGERNAME>

                <AMOUNT>".round(($rows['rounded_of']),2) ."</AMOUNT>

            </LEDGERENTRIES>";
           

        
	$simplexml = $simplexml . "</SALESDATA>";
        
}
    

    

    $simplexml = $simplexml . "</DATA>

</ENVELOPE>";



 header('Content-type: text/xml');

    print_r($simplexml);

    

   

}


else if ($TALLYREQUEST_S == 'SALESJRNL DOWNLOAD REQUEST') 

{
	
	$rtamt=0;
	$taxtotal=0;
	
 $sql = "select t1. *,t2. *,t3. *,t4. *,t5.name as state, t6.name as city,t7.name as rostate from invoice t1 inner join invoice_billing t2 on t1.invoice_no=t2.invoice_no inner join tbl_ro_master t3 on t1.Ro_code=t3.RO_code left join tbl_customer_master t4 on t4. Customer_Code=t1.Customer_name left join states t5 on t5.id=t4.state left join cities t6 on t6.id=t4.city 
 left join states t7 on t7.id=t3.state
 where t1.Ro_code='$DISTID_S' and t1.tally_update_status_vat=0  and bill_amount>0 and tax_type=1
 order by invoice_date";
 $res2 = mysqli_query($con, $sql);

 $simplexml = "<ENVELOPE>

 <HEADER>

  <TALLYREQUEST>SALESJRNL DOWNLOAD</TALLYREQUEST>

  <DISTID>$DISTID_S</DISTID>

  <DISTPASS>$DISTPASS_S</DISTPASS>

 </HEADER>

 <DATA>";


    while ($rows = mysqli_fetch_assoc($res2)) 

	{
		
		
	     $inv=$rows['invoice_no'];
	   
	     $created_at= $rows['invoice_date'];
		
		 $newdate = date("d-M-Y", strtotime($created_at));
		
		//$created_at =date_format($created_at,"d-M-Y");

        //$shift_manager    = str_replace("&","&amp;",$rows['shift_manager']) ;

        

        $simplexml = $simplexml . "

   <SALESJRNLDATA>

    <DISTID>".$DISTID_S."</DISTID>

    <TRANSID>".$inv."</TRANSID>

	<TRANSTYPE>Sales BC Jrnl</TRANSTYPE>

	<TRANSDATE>".$newdate."</TRANSDATE>
	
    <PARTYNAME>".str_replace("&","&amp;",$rows['company_name'])."</PARTYNAME>

	<NARRATION>Bill upto ".$newdate."</NARRATION>";

          
	$simplexml = $simplexml ."<LEDGERENTRIES>

					<LEDGERNAME>".str_replace("&","&amp;",$rows['company_name'])."</LEDGERNAME>
					

					<AMOUNT>-".round(($rows['grand_total_amount']),2) ."</AMOUNT>
					 <BILLALLOCATIONS>
					<BILLTYPE>NEW REF</BILLTYPE>
					<BILLNAME>".$rows['invoice_no']."</BILLNAME>
					<BILLDUEDATE>".$newdate."</BILLDUEDATE>
					<BILLAMOUNT>-".round(($rows['grand_total_amount']),2) ."</BILLAMOUNT>
					</BILLALLOCATIONS>

				</LEDGERENTRIES>";
				
	$simplexml = $simplexml ."<LEDGERENTRIES>

					<LEDGERNAME>Credit Customer Control A/c</LEDGERNAME>
					

					<AMOUNT>".round(($rows['amount']),2) ."</AMOUNT>
					 <BILLALLOCATIONS>
					<BILLTYPE>NEW REF</BILLTYPE>
					<BILLNAME>".$rows['invoice_no']."</BILLNAME>
					<BILLDUEDATE>".$newdate."</BILLDUEDATE>
					<BILLAMOUNT>".round(($rows['amount']),2) ."</BILLAMOUNT>
					</BILLALLOCATIONS>

				</LEDGERENTRIES>";
				
				
		$simplexml = $simplexml ."<LEDGERENTRIES>
       <LEDGERNAME>Billing &amp; Handling Charges</LEDGERNAME>
       <AMOUNT>".$rows['handling_amount']."</AMOUNT>
     </LEDGERENTRIES>";
	 
	 $hand=0;
	 $cgst=0;
	 $sgst=0;
	 
	 if ($rows['state']==$rows['rostate'])
	 {
	   $hand= $rows['handling_amount'];
	   
	   $cgst=round($rows['handling_amount']*9/100,2);
	   
	   $sgst=round($rows['handling_amount']*9/100,2);
		 
	   $simplexml = $simplexml ."<LEDGERENTRIES>
       <LEDGERNAME>CGST</LEDGERNAME>
       <AMOUNT>".$cgst."</AMOUNT>
     </LEDGERENTRIES>";
	 
	  $simplexml = $simplexml ."<LEDGERENTRIES>
       <LEDGERNAME>SGST</LEDGERNAME>
       <AMOUNT>".$sgst."</AMOUNT>
     </LEDGERENTRIES>";
	 }
	 else
	 {
	 
	  $igst=round($rows['handling_amount']*18/100,2);
	 
	  $simplexml = $simplexml ."<LEDGERENTRIES>
       <LEDGERNAME>IGST</LEDGERNAME>
       <AMOUNT>".$igst."</AMOUNT>
     </LEDGERENTRIES>";
	 }
				
				

		  $simplexml = $simplexml . "<LEDGERENTRIES>

                <LEDGERNAME>Rounded off </LEDGERNAME>

                <AMOUNT>".$rows['rounded_of']."</AMOUNT>

            </LEDGERENTRIES>";
           

        
	$simplexml = $simplexml . "</SALESJRNLDATA>";
        
}
    

    

    $simplexml = $simplexml . "</DATA>

</ENVELOPE>";



 header('Content-type: text/xml');

    print_r($simplexml);

    

   

}
	


else if ($TALLYREQUEST_S == 'FUELSALES DOWNLOAD REQUEST') 

{

	$rtamt=0;

	
 $sql = "select DATE_FORMAT(t1.created_at, '%d-%M-%Y') as created_at,t1.id,t2.Personnel_Name as shift_manager,Nozzle_Amount,t1.id  from shifts t1 inner join tbl_personnel_master t2 on t1.shift_manager=t2.id 

	inner join tbl_shift_settlement t3 on t3.shift_id=t1.id where fuel=1 and t1.ro_code='$DISTID_S' and confirm=1 and tally_update_status_fuel=0
 order by t1.created_at limit 0,500";


    

    $res2 = mysqli_query($con, $sql);

   

    //$count = mysqli_num_rows($res1);

    

    $simplexml = "<ENVELOPE>

 <HEADER>

  <TALLYREQUEST>SALES DOWNLOAD</TALLYREQUEST>

  <DISTID>$DISTID_S</DISTID>

  <DISTPASS>$DISTPASS_S</DISTPASS>

 </HEADER>

 <DATA>";


    while ($rows = mysqli_fetch_assoc($res2)) 

	{
		
		
	   $sid=$rows['id'];
	   
	    $created_at       = $rows['created_at'];

        $shift_manager    = str_replace("&","&amp;",$rows['shift_manager']) ;

        

        $simplexml = $simplexml . "

   <SALESDATA>

    <DISTID>".$DISTID_S."</DISTID>

    <TRANSID>".$sid."</TRANSID>

	<TRANSTYPE>SALES</TRANSTYPE>

	<TRANSDATE>".$rows['created_at']."</TRANSDATE>

	<PARTYCODE></PARTYCODE>

    <PARTYNAME>Daily Sales Control A/c</PARTYNAME>

    <BILLTONAME>Daily Sales Control A/c</BILLTONAME>

    <BILLTOADDRESS1></BILLTOADDRESS1>

    <BILLTOADDRESS2></BILLTOADDRESS2>

    <BILLTOADDRESS3></BILLTOADDRESS3>

	<BILLTOADDRESS4></BILLTOADDRESS4>

	<BILLTOSTATE>Delhi</BILLTOSTATE>

    <BILLTOCOUNTRY>INDIA</BILLTOCOUNTRY>

	<BILLTOGSTIN></BILLTOGSTIN>

	<BILLTOVATTIN></BILLTOVATTIN>

	<PLACEOFSUPPLY>Delhi</PLACEOFSUPPLY>

	<SHIPTONAME>Daily Sales Control A/c</SHIPTONAME>

	<SHIPTOADDRESS1></SHIPTOADDRESS1>

	<SHIPTOADDRESS2></SHIPTOADDRESS2>

	<SHIPTOADDRESS3></SHIPTOADDRESS3>

	<SHIPTOADDRESS4></SHIPTOADDRESS4>

	<SHIPTOSTATE>Delhi</SHIPTOSTATE>

	<SHIPTOCOUNTRY>INDIA</SHIPTOCOUNTRY>

	<SHIPTOGSTIN></SHIPTOGSTIN>

	<SHIPTOVATTIN></SHIPTOVATTIN>";
	
	$sql="Select round(sum(reading * price),2) as total from tbl_ro_nozzle_reading where shift_id='$sid'";
	 
	$resitem1 = mysqli_query($con, $sql);
	
	$gtotal=0;

	while ($rowsitem1 = mysqli_fetch_assoc($resitem1)) 

	{


		$gtotal=$rowsitem1['total'];



	}


	 $simplexml = $simplexml . "<VCHTOTALAMT>".$gtotal."</VCHTOTALAMT>

	<NARRATION>Fuel Sales dt $created_at by $shift_manager</NARRATION>

	<BILLALLOCATIONS>

		<BILLTYPE>NEW REF</BILLTYPE>

		<BILLNAME>$created_at $shift_manager</BILLNAME>

		<BILLDUEDATE></BILLDUEDATE>

		<BILLAMOUNT>".$gtotal."</BILLAMOUNT>

	</BILLALLOCATIONS>";

	

	$sql="Select t3.Item_Name,t3.item_code,round(sum(Nozzle_End-Nozzle_Start-test),2) as qty,price,t3.id,hsncode from tbl_ro_nozzle_reading t1 inner join tbl_pedestal_nozzle_master t2 on t1.Nozzle_No=t2.Nozzle_Number inner join tbl_price_list_master t3 on t3.id=t2.fuel_type where shift_id='$sid' group by Item_Name";
	 
	$resitem1 = mysqli_query($con, $sql);
	
	$totalexp=0;
	$taxtotal=0;
	$tamount=0;
	$totalexp=0;
	$tot=0;
	$tamount1=0;
	$itembtaxamount=0;
	$ITEMAMOUNT=0;
	
    while ($rowsitem = mysqli_fetch_assoc($resitem1)) 

    {

	 //$tid=$rowsitem['id'];
	 
	 //$invoice_roundoff=$rowsitem['invoice_roundoff'];

	 $simplexml = $simplexml . "

	<INVENTORYENTRIES>

	<ITEMCODE>".str_replace("&","&amp;",$rowsitem['item_code']) ."</ITEMCODE>

	<ITEMNAME>".str_replace("&","&amp;",$rowsitem['Item_Name']) ."</ITEMNAME>

    <ITEMQTY>".$rowsitem['qty']."</ITEMQTY>";
    $simplexml = $simplexml."<ITEMMRP>".$rowsitem['price']."</ITEMMRP>";
	
	$sql123 = "select sum(Tax_percentage) as Tax_percentage from tbl_itemprice_tax t1 inner join tbl_tax_master t2 on t1.tax_id=t2.id where t1.item_id='".$rowsitem['id']."'";
	$res123 = mysqli_query($con, $sql123);
	
	while ($rows123 = mysqli_fetch_assoc($res123)) 
	{
		$vatrate=$rows123['Tax_percentage'];
	}
	
	//echo $vatrate;
	//exit();
	
	$tot=$rowsitem['price']*$rowsitem['qty'];
	
	$itembtaxamount=round($rowsitem['price']/(1+$vatrate/100),2);
	$tamount=$rowsitem['price']-$itembtaxamount;
	
	$tamount1=round($tamount * $rowsitem['qty'],2);
	
	//echo $btavvalue=$tot-$tamount1;
	//exit();
	
	$ITEMAMOUNT=round($itembtaxamount * $rowsitem['qty'],2);
	
	$totalexp=$totalexp+$ITEMAMOUNT;

    $simplexml = $simplexml."<ITEMRATE>".$itembtaxamount."</ITEMRATE>

    <ITEMDISCOUNT></ITEMDISCOUNT>

    <ITEMAMOUNT>".$ITEMAMOUNT."</ITEMAMOUNT>

    <HSNSAC>".str_replace("&","&amp;",$rowsitem['hsncode']) ."</HSNSAC>";

    $sql123 = "select GST_Type,Tax_percentage from tbl_itemprice_tax t1 inner join tbl_tax_master t2 on t1.tax_id=t2.id where t1.item_id='".$rowsitem['id']."'";
	
	$res123 = mysqli_query($con, $sql123);
	
		 $vatamount=0;
		 $advatamount=0;
         $scvatamount =0;
		 $igstamt=0;
		 $cgstamt=0;
		 $sgstamt=0;
		 $gstcessamt=0;
		 

        while ($rows123 = mysqli_fetch_assoc($res123)) 

		{

            

            if ($rows123['GST_Type'] == 'VAT') 

			{

                 $vatrate=$rows123['Tax_percentage'];
				 
				 $vatrate1=100;

				 $vatamount=round($tamount1* $vatrate1/100,2);
				 
				 //$totalexp=$totalexp+$vatamount;
				 
				 //$vatamount=round($ITEMAMOUNT*$rows123['Tax_percentage']/100,2);

            }

			            

            if ($rows123['GST_Type'] == 'ADVAT') 

			{

                 $advatrate=$rows123['Tax_percentage'];
				 
				 $vatrate1=0;

				 $advatamount=round($tamount1* $vatrate1/100,2);

				 //$totalexp=$totalexp+$advatamount;
				
				  //$advatamount=round($ITEMAMOUNT*$rows123['Tax_percentage']/100,2);

                

            }

            if ($rows123['GST_Type'] == 'SCVAT') 

			{

                  $scvatrate=$rows123['Tax_percentage'];
				  
				   $vatrate1=0;

				 $scvatamount=round($tamount1* $vatrate1/100,2);
				 
				 //$totalexp=$totalexp+$scvatamount;

				  //$scvatamount=round($ITEMAMOUNT*$rows123['Tax_percentage']/100,2);

            }


			if ($rows123['GST_Type'] == 'IGST') 

			{

                $igstrate=$rows123['Tax_percentage'];

				  $igstamt=round($ITEMAMOUNT*$rows123['Tax_percentage']/100,2);

            }


			if ($rows123['GST_Type'] == 'CGST') 
			{

                $cgstrate=$rows123['Tax_percentage'];

				  $cgstamt=round($ITEMAMOUNT*$rows123['Tax_percentage']/100,2);

            }


			if ($rows123['GST_Type'] == 'UT/SGST') 

			{

                $sgstrate=$rows123['Tax_percentage'];

				  $sgstamt=round($ITEMAMOUNT*$rows123['Tax_percentage']/100,2);

            }

			if ($rows123['GST_Type'] == 'SPCESS') 

			{

                $gstcessrate=$rows123['Tax_percentage'];

				  $gstcessamt=round($ITEMAMOUNT*$rows123['Tax_percentage']/100,2);

            }

        }

		

    $simplexml = $simplexml."<VATRATE>$vatrate</VATRATE>

	<VATAMT>$vatamount</VATAMT>";

     $simplexml = $simplexml."<VATADDLRATE>$advatrate</VATADDLRATE>

	<VATADDLAMT>$advatamount</VATADDLAMT>";

    $simplexml = $simplexml."<VATCESSRATE>$scvatrate</VATCESSRATE>

	<VATCESSAMT>$scvatamount</VATCESSAMT>";

    $simplexml = $simplexml."<IGSTRATE>$igstrate</IGSTRATE>

    <IGSTAMT>$igstamt</IGSTAMT>";

    $simplexml = $simplexml."<CGSTRATE>$cgstrate</CGSTRATE>

    <CGSTAMT>$cgstamt</CGSTAMT>";

    $simplexml = $simplexml."<SGSTRATE>$sgstrate</SGSTRATE>

	<SGSTAMT>$sgstamt</SGSTAMT>";

    $simplexml = $simplexml."<GSTCESSRATE>$gstcessrate</GSTCESSRATE>

	<GSTCESSAMT>$gstcessamt</GSTCESSAMT>";
       
	$itemtotal=0;
	
	
	$taxtotal=$taxtotal+$vatamount+$advatamount+$scvatamount+$igstamt+$cgstamt+$sgstamt+$gstcessamt;
	//$taxtotal=$taxtotal+$taxtotal;

	$itemtotal=$vatamount+$advatamount+$scvatamount+$igstamt+$cgstamt+$sgstamt+$gstcessamt+$ITEMAMOUNT;


	$simplexml = $simplexml . "<ROUNDOFFAMT></ROUNDOFFAMT>";

	
	$simplexml = $simplexml ."<ITEMTOTALAMT>$itemtotal</ITEMTOTALAMT>

	<ITEMACCLEDGER>LOCAL SALES</ITEMACCLEDGER>";

	//round(sum(t5.Nozzle_End-t5.Nozzle_Start-t5.test),2) as qty

	    $sqltank ="select t5 .*,t4.Tank_Number from shifts t1 inner join shift_pedestal t2 on t1.id=t2.shift_id inner join tbl_pedestal_nozzle_master t3 on t3.Pedestal_id=t2.pedestal_id 
		inner join tbl_tank_master t4 on t4.fuel_type= t3.fuel_type 
		
		inner join tbl_ro_nozzle_reading t5 on t5.shift_id=t1.id and t5.nozzle_no=t3.nozzle_number
		where t1.id=".$sid." and (t5.Nozzle_End-t5.Nozzle_Start-t5.test)>0 
		and t4.fuel_type=".$rowsitem['id']." group by nozzle_no ";
		
		$restank = mysqli_query($con, $sqltank);
		
		$qty=0;
		
		while ($rowstank = mysqli_fetch_assoc($restank)) 
		{
			$qty=$qty+$rowstank['Nozzle_End']-$rowstank['Nozzle_Start']-$rowstank['test'];
			$tankname=$rowstank['Tank_Number'];
		}
		
		//print_r($sqltank);
		//exit();

		$qty=round($qty,2);
		
		

		//while ($rowstank = mysqli_fetch_assoc($restank)) 
		//{
          
		$total=$itembtaxamount * $qty;

		$simplexml = $simplexml . "<BATCHALLOCATIONS>

		<PRODORDERNO></PRODORDERNO>

		<PRODTRACKINGNO></PRODTRACKINGNO>

		<PRODBATCH>Primary Batch</PRODBATCH>

		<PRODGODOWN>".str_replace("&","&amp;",$tankname)."</PRODGODOWN>

		<BATCHQTY>".$qty."</BATCHQTY>

		<BATCHRATE>".round($itembtaxamount,2)."</BATCHRATE>

		<BATCHDISCOUNT></BATCHDISCOUNT>

		<BATCHAMOUNT>".round($total,2)."</BATCHAMOUNT>

		</BATCHALLOCATIONS>";

			//}

		

   $simplexml = $simplexml . "</INVENTORYENTRIES>";

   

	}

          
 $simplexml = $simplexml ."<LEDGERENTRIES>

					<LEDGERNAME>VAT</LEDGERNAME>
					

					<AMOUNT>".$taxtotal."</AMOUNT>

				</LEDGERENTRIES>";
$rnd=0;
//$gtotal;
//exit();
$rnd=$gtotal-($totalexp+$taxtotal);

		  $simplexml = $simplexml . "<LEDGERENTRIES>

                <LEDGERNAME>Rounded off </LEDGERNAME>

                <AMOUNT>".round($rnd,2)."</AMOUNT>

            </LEDGERENTRIES>";
           

        
	$simplexml = $simplexml . "</SALESDATA>";
        

    }

    

    $simplexml = $simplexml . "</DATA>

</ENVELOPE>";



 header('Content-type: text/xml');

    print_r($simplexml);

    

   

}


else if ($TALLYREQUEST_S == 'SALES DOWNLOAD REQUEST') 

{

	$rtamt=0;

	
        $sql = "select t1.id,t1.RO_code,DATE_FORMAT(t1.trans_date, '%d-%M-%Y') as trans_date,invoice_number,t1.customer_code,company_name,t3.address_one,
	t3.address_two,t3.address_three, t7.name as state,t8.name as city,t3.gst_no,t3.vat,sum(item_price* petroldiesel_qty) as amount,invoice_amount,invoice_roundoff
	from tbl_customer_transaction t1 inner join tbl_ro_master t2 on t1.RO_code=t2.RO_code left join tbl_customer_master t3 on t3.
	Customer_Code=t1.customer_code left join states t4 on t4.id=t2.state left join cities t5 on t5.id=t2.city inner join tbl_price_list_master 
	t6 on t6.id=t1.item_code left join states t7 on t7.id=t3.state left join cities t8 on t8.id=t3.city where t1.RO_code='$DISTID_S' and 
	t1.tally_update_status=0 and petrol_or_lube=2  group by invoice_number 
	";

    

    $res2 = mysqli_query($con, $sql);

   

    //$count = mysqli_num_rows($res1);

    

    $simplexml = "<ENVELOPE>

 <HEADER>

  <TALLYREQUEST>SALES DOWNLOAD</TALLYREQUEST>

  <DISTID>$DISTID_S</DISTID>

  <DISTPASS>$DISTPASS_S</DISTPASS>

 </HEADER>

 <DATA>";


    while ($rows = mysqli_fetch_assoc($res2)) 

	{
		
		
	   $tid=$rows['id'];

$invno=$rows['invoice_number'];

        

        $simplexml = $simplexml . "

   <SALESDATA>

    <DISTID>".$rows['RO_code']."</DISTID>

    <TRANSID>".str_replace("&","&amp;",$rows['invoice_number'])."</TRANSID>

	<TRANSTYPE>SALES</TRANSTYPE>

	<TRANSDATE>".$rows['trans_date']."</TRANSDATE>

	<PARTYCODE>".str_replace("&","&amp;",$rows['customer_code'])."</PARTYCODE>

    <PARTYNAME>".str_replace("&","&amp;",$rows['company_name'])."</PARTYNAME>

    <BILLTONAME>".str_replace("&","&amp;",$rows['company_name'])."</BILLTONAME>

    <BILLTOADDRESS1>".str_replace("&","&amp;",$rows['address_one'])."</BILLTOADDRESS1>

    <BILLTOADDRESS2>".str_replace("&","&amp;",$rows['address_two'])."</BILLTOADDRESS2>

    <BILLTOADDRESS3>".str_replace("&","&amp;",$rows['address_three'])."</BILLTOADDRESS3>

	<BILLTOADDRESS4></BILLTOADDRESS4>

	<BILLTOSTATE>".str_replace("&","&amp;",$rows['state'])."</BILLTOSTATE>

    <BILLTOCOUNTRY>INDIA</BILLTOCOUNTRY>

	<BILLTOGSTIN>".str_replace("&","&amp;",$rows['gst_no'])."</BILLTOGSTIN>

	<BILLTOVATTIN>".str_replace("&","&amp;",$rows['vat'])."</BILLTOVATTIN>

	<PLACEOFSUPPLY>".str_replace("&","&amp;",$rows['state'])."</PLACEOFSUPPLY>

	<SHIPTONAME>".str_replace("&","&amp;",$rows['company_name'])."</SHIPTONAME>

	<SHIPTOADDRESS1>".str_replace("&","&amp;",$rows['address_one'])."</SHIPTOADDRESS1>

	<SHIPTOADDRESS2>".str_replace("&","&amp;",$rows['address_two'])."</SHIPTOADDRESS2>

	<SHIPTOADDRESS3>".str_replace("&","&amp;",$rows['address_three'])."</SHIPTOADDRESS3>

	<SHIPTOADDRESS4></SHIPTOADDRESS4>

	<SHIPTOSTATE>".str_replace("&","&amp;",$rows['state'])."</SHIPTOSTATE>

	<SHIPTOCOUNTRY>INDIA</SHIPTOCOUNTRY>

	<SHIPTOGSTIN>".str_replace("&","&amp;",$rows['gst_no'])."</SHIPTOGSTIN>

	<SHIPTOVATTIN>".str_replace("&","&amp;",$rows['vat'])."</SHIPTOVATTIN>

	<VCHTOTALAMT>".round(($rows['invoice_amount']),2) ."</VCHTOTALAMT>

	<NARRATION></NARRATION>

	<BILLALLOCATIONS>

		<BILLTYPE>NEW REF</BILLTYPE>

		<BILLNAME>".str_replace("&","&amp;",$rows['invoice_number'])."</BILLNAME>

		<BILLDUEDATE></BILLDUEDATE>

		<BILLAMOUNT>".round(($rows['invoice_amount']),2) ."</BILLAMOUNT>

	</BILLALLOCATIONS>";

	

	$sqlitem = "select t1.id,t1.RO_code,DATE_FORMAT(t1.trans_date, '%d-%M-%Y') as trans_date,invoice_number,t1.customer_code,company_name,t3.address_one,

	  t3.address_two,t3.address_three,



t7.name as state,t8.name as city,t3.gst_no,t3.vat,(item_price*petroldiesel_qty) as amount,t6.Item_Code,t6.Item_Name,item_price as item_price_act,

item_price,petroldiesel_qty,petrol_or_lube,t6.id as pid,t1.shift_id,invoice_amount,invoice_roundoff,
taxbefore_price,taxable_value

	from tbl_customer_transaction t1 inner join tbl_ro_master t2 on t1.RO_code=t2.RO_code

    left join tbl_customer_master t3 on t3.Customer_Code=t1.customer_code

	left join states t4 on 

    t4.id=t2.state left join cities t5 on t5.id=t2.city 

    inner join tbl_price_list_master t6 on t6.id=t1.item_code

	left join states t7 on 

	t7.id=t3.state left join cities t8 on t8.id=t3.city 

	where t1.RO_code='$DISTID_S' and t1.tally_update_status=0 and invoice_number='".$rows['invoice_number']."'";

	  

    $resitem = mysqli_query($con, $sqlitem);

      $invoice_roundoff=0;  

    while ($rowsitem = mysqli_fetch_assoc($resitem)) 

    {

	 $tid=$rowsitem['id'];
	 
	 $invoice_roundoff=$rowsitem['invoice_roundoff'];

	 $simplexml = $simplexml . "

	<INVENTORYENTRIES>

	<ITEMCODE>".str_replace("&","&amp;",$rowsitem['Item_Code']) ."</ITEMCODE>

	<ITEMNAME>".str_replace("&","&amp;",$rowsitem['Item_Name']) ."</ITEMNAME>

    <ITEMQTY>".$rowsitem['petroldiesel_qty']."</ITEMQTY>";

	
     $simplexml = $simplexml."<ITEMMRP>".$rowsitem['item_price_act']."</ITEMMRP>

    <ITEMRATE>".$rowsitem['taxbefore_price']."</ITEMRATE>

    <ITEMDISCOUNT></ITEMDISCOUNT>

    <ITEMAMOUNT>".$rowsitem['taxable_value']."</ITEMAMOUNT>

    <HSNSAC>".str_replace("&","&amp;",$rowsitem['hsncode']) ."</HSNSAC>";

    $sql123 = "select * from item_transaction_tax where transaction_id='".$rowsitem['id']."'";
	$res123 = mysqli_query($con, $sql123);
	
		 $vatamount=0;
		 $advatamount=0;
         $scvatamount =0;
		 $igstamt=0;
		 $cgstamt=0;
		 $sgstamt=0;
		 $gstcessamt=0;
		 

        while ($rows123 = mysqli_fetch_assoc($res123)) 

		{

            

            if ($rows123['GST_Type'] == 'VAT') 

			{

                  $vatrate=$rows123['Tax_percentage'];

				
				  $vatamount=$rows123['Tax_Value'];

            }

			            

            if ($rows123['GST_Type'] == 'ADVAT') 

			{

                 $advatrate=$rows123['Tax_percentage'];

				
				  $advatamount=$rows123['Tax_Value'];

                

            }

            if ($rows123['GST_Type'] == 'SCVAT') 

			{

                  $scvatrate=$rows123['Tax_percentage'];

				  $scvatamount=$rows123['Tax_Value'];

            }


			if ($rows123['GST_Type'] == 'IGST') 

			{

                $igstrate=$rows123['Tax_percentage'];

				  $igstamt=$rows123['Tax_Value'];

            }


			if ($rows123['GST_Type'] == 'CGST') 
			{

                $cgstrate=$rows123['Tax_percentage'];

				  $cgstamt=$rows123['Tax_Value'];

            }


			if ($rows123['GST_Type'] == 'UT/SGST') 

			{

                $sgstrate=$rows123['Tax_percentage'];

				  $sgstamt=$rows123['Tax_Value'];

            }

			if ($rows123['GST_Type'] == 'SPCESS') 

			{

                $gstcessrate=$rows123['Tax_percentage'];

				  $gstcessamt=$rows123['Tax_Value'];

            }

        }

		

    $simplexml = $simplexml."<VATRATE>$vatrate</VATRATE>

	<VATAMT>$vatamount</VATAMT>";

     $simplexml = $simplexml."<VATADDLRATE>$advatrate</VATADDLRATE>

	<VATADDLAMT>$advatamount</VATADDLAMT>";

    $simplexml = $simplexml."<VATCESSRATE>$scvatrate</VATCESSRATE>

	<VATCESSAMT>$scvatamount</VATCESSAMT>";

    $simplexml = $simplexml."<IGSTRATE>$igstrate</IGSTRATE>

    <IGSTAMT>$igstamt</IGSTAMT>";

    $simplexml = $simplexml."<CGSTRATE>$cgstrate</CGSTRATE>

    <CGSTAMT>$cgstamt</CGSTAMT>";

    $simplexml = $simplexml."<SGSTRATE>$sgstrate</SGSTRATE>

	<SGSTAMT>$sgstamt</SGSTAMT>";

    $simplexml = $simplexml."<GSTCESSRATE>$gstcessrate</GSTCESSRATE>

	<GSTCESSAMT>$gstcessamt</GSTCESSAMT>";
       
	$itemtotal=0;
	

	$itemtotal=$vatamount+$advatamount+$scvatamount+$igstamt+$cgstamt+$sgstamt+$gstcessamt+$rowsitem['taxable_value'];


	$simplexml = $simplexml . "<ROUNDOFFAMT></ROUNDOFFAMT>";

	
	$simplexml = $simplexml ."<ITEMTOTALAMT>$itemtotal</ITEMTOTALAMT>

	<ITEMACCLEDGER>LOCAL SALES</ITEMACCLEDGER>";

	

	if ($rowsitem['petrol_or_lube']==1)

	{

	    $sqltank ="select distinct t4.Tank_Number from shifts t1 inner join shift_pedestal t2 on t1.id=t2.shift_id inner join tbl_pedestal_nozzle_master t3 on t3.Pedestal_id=t2.pedestal_id inner join tbl_tank_master t4 on t4.fuel_type= t3.fuel_type where shift_id=".$rowsitem['shift_id']." and t4.fuel_type=".$rowsitem['pid']."";

		//$sqltank = "Select * from tbl_tank_master where 

		//RO_code='$DISTID_S' and fuel_type='".$rowsitem['pid']."'";

			        

		$restank = mysqli_query($con, $sqltank);
		
		$counttank = mysqli_num_rows($restank);
		
		if ($counttank==0)
		{
			$simplexml = $simplexml . "<BATCHALLOCATIONS>

			<PRODORDERNO></PRODORDERNO>

			<PRODTRACKINGNO></PRODTRACKINGNO>

			<PRODBATCH>Primary Batch</PRODBATCH>

			<PRODGODOWN>TANK</PRODGODOWN>

			<BATCHQTY></BATCHQTY>

			<BATCHRATE></BATCHRATE>

			<BATCHDISCOUNT></BATCHDISCOUNT>

			<BATCHAMOUNT></BATCHAMOUNT>

			</BATCHALLOCATIONS>";
		}
        
		

		while ($rowstank = mysqli_fetch_assoc($restank)) 
		{


		$simplexml = $simplexml . "<BATCHALLOCATIONS>

		<PRODORDERNO></PRODORDERNO>

		<PRODTRACKINGNO></PRODTRACKINGNO>

		<PRODBATCH>Primary Batch</PRODBATCH>

		<PRODGODOWN>".str_replace("&","&amp;",$rowstank['Tank_Number'])."</PRODGODOWN>

		<BATCHQTY>".$rowsitem['petroldiesel_qty']."</BATCHQTY>

		<BATCHRATE>".$itembtaxamount."</BATCHRATE>

		<BATCHDISCOUNT></BATCHDISCOUNT>

		<BATCHAMOUNT>".$ITEMAMOUNT."</BATCHAMOUNT>

		</BATCHALLOCATIONS>";

			}

		

		

	}

	else

	{

		$simplexml = $simplexml . "<BATCHALLOCATIONS>

		<PRODORDERNO></PRODORDERNO>

		<PRODTRACKINGNO></PRODTRACKINGNO>

		<PRODBATCH>Primary Batch</PRODBATCH>

		<PRODGODOWN>GODOWN1</PRODGODOWN>
		
		<BATCHQTY>".$rowsitem['petroldiesel_qty']."</BATCHQTY>

		<BATCHRATE>".$rowsitem['taxbefore_price']."</BATCHRATE>

		<BATCHDISCOUNT></BATCHDISCOUNT>

		<BATCHAMOUNT>".$rowsitem['taxable_value']."</BATCHAMOUNT>

		</BATCHALLOCATIONS>";

	}

    

   $simplexml = $simplexml . "</INVENTORYENTRIES>";

   

	}

        

             

        $sql1234 = "select GST_Type,sum(Tax_Value) as Tax_Value,petroldiesel_qty  from item_transaction_tax t1 inner join tbl_customer_transaction t2 on t1.transaction_id=t2.id where invoice_number='".$rows['invoice_number']."' group by GST_Type";

		

		//echo $sql1234;

        

        $res1234 = mysqli_query($con, $sql1234);

        

        while ($rows1234 = mysqli_fetch_assoc($res1234)) 

		{

            

            $simplexml = $simplexml ."<LEDGERENTRIES>

					<LEDGERNAME>".str_replace("&","&amp;",$rows1234['GST_Type'])."</LEDGERNAME>
					
					

					<AMOUNT>".round($rows1234['Tax_Value'],2)."</AMOUNT>

				</LEDGERENTRIES>";

        }

		

		  $simplexml = $simplexml . "<LEDGERENTRIES>

                <LEDGERNAME>Rounded off </LEDGERNAME>

                <AMOUNT>".$invoice_roundoff."</AMOUNT>

            </LEDGERENTRIES>";

        
	$simplexml = $simplexml . "</SALESDATA>";




	
        

    }

    

    $simplexml = $simplexml . "</DATA>

</ENVELOPE>";



 header('Content-type: text/xml');

    print_r($simplexml);

    

   

}





else if ($TALLYREQUEST_S == 'RECEIPT DOWNLOAD REQUEST') 

{

    

    

    $sql = "select DATE_FORMAT(t1.created_at, '%d-%M-%Y') as created_at,t1.id,t2.Personnel_Name as shift_manager,Nozzle_Amount  from shifts t1 inner join tbl_personnel_master t2 on t1.shift_manager=t2.id 

	inner join tbl_shift_settlement t3 on t3.shift_id=t1.id where fuel=1 and t1.tally_update_status=0 and t1.ro_code='$DISTID_S' and t1.confirm=1   order by t1.created_at";

    

    //print_r( $sql);

	//exit();

   

    

    $res1 = mysqli_query($con, $sql);

    

    $count = mysqli_num_rows($res1);

    

    

    $simplexml = "<ENVELOPE>

 <HEADER>

  <TALLYREQUEST>RECEIPT DOWNLOAD</TALLYREQUEST>

  <DISTID>$DISTID_S</DISTID>

  <DISTPASS>$DISTPASS_S</DISTPASS>

 </HEADER>

 <DATA>";

    

    

    

    

    

    while ($rows = mysqli_fetch_assoc($res1)) 

	{

        

        $shift_id               = $rows['id'];

		

        $created_at       = $rows['created_at'];

        $shift_manager    = str_replace("&","&amp;",$rows['shift_manager']) ;

		$Nozzle_Amount    = $rows['Nozzle_Amount'];

      

        $Nozzle_Amount=truncate_number($Nozzle_Amount,2);

        

    $simplexml = $simplexml ."

	<RECEIPTDATA>

<DISTID>$DISTID_S</DISTID>

<TRANSID>$shift_id</TRANSID>

<TRANSTYPE>Receipt</TRANSTYPE>

<TRANSDATE>$created_at</TRANSDATE>

<PARTYCODE></PARTYCODE>

<PARTYNAME>Daily Sales Control A/c</PARTYNAME>

<NARRATION>settlement dt  $created_at shift by $shift_manager</NARRATION>

<LEDGERENTRIES>

<LEDGERNAME>Daily Sales Control A/c</LEDGERNAME>

<AMOUNT>$Nozzle_Amount</AMOUNT>

<BILLALLOCATIONS>

<BILLTYPE></BILLTYPE>

<BILLNAME></BILLNAME>

<BILLAMOUNT></BILLAMOUNT>

</BILLALLOCATIONS>

</LEDGERENTRIES>";





	

   $sql123 = "select * from shifts t1 inner join tbl_personnel_master t2 on t1.shift_manager=t2.id 

	inner join tbl_shift_settlement t3 on t3.shift_id=t1.id

	inner join paymentmode_sattlement t4 on t4.sattlement_id=t3.id

	inner join tpl_payment_mode t5 on t5.id=t4.paymentmode_id

	where fuel=1 and t1.ro_code='$DISTID_S' and t1.id='$shift_id' ";

	$res123 = mysqli_query($con, $sql123);
	$amt=0;
	
	
	//$sqlcredit="SELECT round(sum(item_price*petroldiesel_qty),2) as trans_amount_p from tbl_customer_transaction 
	//t1 inner join tbl_price_list_master t2 on t1.item_code=t2.id 
	//where  shift_id='$shiftid' and cust_name='credit'";
	
	
	$sqlcredit="SELECT round(sum(item_price*petroldiesel_qty),2) as trans_amount_p from tbl_customer_transaction where  shift_id='$shift_id' and cust_name='credit'";
	

    $rescredit = mysqli_query($con, $sqlcredit);
	
	
    while ($rowscredit = mysqli_fetch_assoc($rescredit)) 
	{
				
		    $amt=$amt+$rowscredit['trans_amount_p'];
			
			$simplexml = $simplexml ."<LEDGERENTRIES><LEDGERNAME>credit</LEDGERNAME>

			<AMOUNT>-".$rowscredit['trans_amount_p']."</AMOUNT>

			<BANKALLOCATIONS>

			<DATE>$created_at</DATE>

			<INSTRUMENTNO></INSTRUMENTNO>

			<INSTRUMENTDATE>$created_at</INSTRUMENTDATE>

			<TRANSACTIONTYPE>Cheque/DD</TRANSACTIONTYPE>

			<PAYMENTFAVOURING>Daily Sales Control A/c</PAYMENTFAVOURING>

			<BANKPARTYNAME>Daily Sales Control A/c</BANKPARTYNAME>

			<AMOUNT>-".$rowscredit['trans_amount_p']."</AMOUNT>

			</BANKALLOCATIONS>

			</LEDGERENTRIES>";
		
		
		
	}
	


	

    while ($rows123 = mysqli_fetch_assoc($res123)) 

	{

			$amt=$amt+$rows123['amount'];
			
			$simplexml = $simplexml ."<LEDGERENTRIES><LEDGERNAME>".str_replace("&","&amp;",$rows123['name']) ."</LEDGERNAME>

			<AMOUNT>-".$rows123['amount']."</AMOUNT>

			<BANKALLOCATIONS>

			<DATE>$created_at</DATE>

			<INSTRUMENTNO>".str_replace("&","&amp;",$rows123['ref_no']) ."</INSTRUMENTNO>

			<INSTRUMENTDATE>$created_at</INSTRUMENTDATE>

			<TRANSACTIONTYPE>Cheque/DD</TRANSACTIONTYPE>

			<PAYMENTFAVOURING>Daily Sales Control A/c</PAYMENTFAVOURING>

			<BANKPARTYNAME>Daily Sales Control A/c</BANKPARTYNAME>

			<AMOUNT>-".$rows123['amount']."</AMOUNT>

			</BANKALLOCATIONS>

			</LEDGERENTRIES>";

			

			

			

			

	}

	$diff=0;

	//$diff=truncate_number($Nozzle_Amount,2)-truncate_number($amt,2);

	$diff=round($amt,2)-round($Nozzle_Amount,2);


	$diff=round($diff,2);

	

	

	

	$simplexml = $simplexml ."<LEDGERENTRIES><LEDGERNAME>$shift_manager</LEDGERNAME><AMOUNT>$diff</AMOUNT></LEDGERENTRIES>";

	

	 $simplexml = $simplexml . "</RECEIPTDATA>";

	

	

	

	}

    

    

   

	

	 $simplexml = $simplexml . "</DATA></ENVELOPE>";

	 

	 

	 

    header('Content-type: text/xml');

    print_r($simplexml);

    

    

}



else if ($TALLYREQUEST_S == 'SALES ACKNOWLEDGEMENT') 

{

	

	$books = $doc->getElementsByTagName("SALESDATA");

	foreach ($books as $book) 

	{

    

		//$TALLYREQUEST   = $book->getElementsByTagName("SALESDATA");

		//$TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;

    

    

    

    

		$DISTID   = $book->getElementsByTagName("DISTID");

		$DISTID_S = $DISTID->item(0)->nodeValue;

    

		$TRANSID   = $book->getElementsByTagName("TRANSID");

		$TRANSID_S = $TRANSID->item(0)->nodeValue;

    

		$SUCCESS   = $book->getElementsByTagName("SUCCESS");

		$SUCCESS_S = $SUCCESS->item(0)->nodeValue;

		

		if ($SUCCESS_S=="YES")

		{

		

			$sql1234 = "update tbl_customer_transaction set tally_update_status=1 

			where invoice_number='$TRANSID_S'";

		

			//echo $sql1234;

        

			$res1234 = mysqli_query($con, $sql1234);

		}

    

    

	}

	

}
else if ($TALLYREQUEST_S == 'GSTBILL ACKNOWLEDGEMENT') 

{

	

	$books = $doc->getElementsByTagName("SALESDATA");

	foreach ($books as $book) 

	{

    

		//$TALLYREQUEST   = $book->getElementsByTagName("SALESDATA");

		//$TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;

    

    

    

    

		$DISTID   = $book->getElementsByTagName("DISTID");

		$DISTID_S = $DISTID->item(0)->nodeValue;

    

		$TRANSID   = $book->getElementsByTagName("TRANSID");

		$TRANSID_S = $TRANSID->item(0)->nodeValue;

    

		$SUCCESS   = $book->getElementsByTagName("SUCCESS");

		$SUCCESS_S = $SUCCESS->item(0)->nodeValue;

		

		if ($SUCCESS_S=="YES")

		{

		

			$sql1234 = "update invoice set tally_update_status=1 

			where invoice_no='$TRANSID_S'";

		

			//echo $sql1234;

        

			$res1234 = mysqli_query($con, $sql1234);

		}

    

    

	}

	

}


else if ($TALLYREQUEST_S == 'FUELSALES ACKNOWLEDGEMENT') 

{

	

	$books = $doc->getElementsByTagName("SALESDATA");

	foreach ($books as $book) 

	{

    

		//$TALLYREQUEST   = $book->getElementsByTagName("SALESDATA");

		//$TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;

    

    

    

    

		$DISTID   = $book->getElementsByTagName("DISTID");

		$DISTID_S = $DISTID->item(0)->nodeValue;

    

		$TRANSID   = $book->getElementsByTagName("TRANSID");

		$TRANSID_S = $TRANSID->item(0)->nodeValue;

    

		$SUCCESS   = $book->getElementsByTagName("SUCCESS");

		$SUCCESS_S = $SUCCESS->item(0)->nodeValue;

		

		if ($SUCCESS_S=="YES")

		{

		

			$sql1234 = "update shifts set tally_update_status_fuel=1 

			where id='$TRANSID_S'";

		

			//echo $sql1234;

        

			$res1234 = mysqli_query($con, $sql1234);

		}

    

    

	}

	

}


else if ($TALLYREQUEST_S == 'SALESJRNL ACKNOWLEDGEMENT') 

{

	

	$books = $doc->getElementsByTagName("SALESJRNLDATA");

	foreach ($books as $book) 

	{

    

		//$TALLYREQUEST   = $book->getElementsByTagName("SALESDATA");

		//$TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;

    

    

    

    

		$DISTID   = $book->getElementsByTagName("DISTID");

		$DISTID_S = $DISTID->item(0)->nodeValue;

    

		$TRANSID   = $book->getElementsByTagName("TRANSID");

		$TRANSID_S = $TRANSID->item(0)->nodeValue;

    

		$SUCCESS   = $book->getElementsByTagName("SUCCESS");

		$SUCCESS_S = $SUCCESS->item(0)->nodeValue;

		

		if ($SUCCESS_S=="YES")

		{

		

			$sql1234 = "update invoice set tally_update_status_vat=1 

			where id='$TRANSID_S'";

		

			//echo $sql1234;

        

			$res1234 = mysqli_query($con, $sql1234);

		}

    

    

	}

	

}


else if ($TALLYREQUEST_S == 'RECEIPT ACKNOWLEDGEMENT') 

{

	//echo "ssss";
	//exit();

	$books = $doc->getElementsByTagName("RECEIPTDATA");

	foreach ($books as $book) 

	{

    

		//$TALLYREQUEST   = $book->getElementsByTagName("RECEIPTDATA");

		//$TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;

    

		$DISTID   = $book->getElementsByTagName("DISTID");

		$DISTID_S = $DISTID->item(0)->nodeValue;

    

		$TRANSID   = $book->getElementsByTagName("TRANSID");

		$TRANSID_S = $TRANSID->item(0)->nodeValue;

    

		$SUCCESS   = $book->getElementsByTagName("SUCCESS");

		$SUCCESS_S = $SUCCESS->item(0)->nodeValue;

		

		if ($SUCCESS_S=="YES")

		{

		

			$sql1234 = "update shifts set tally_update_status=1 

			where id='$TRANSID_S'";

		

			//echo $sql1234;

        

			$res1234 = mysqli_query($con, $sql1234);

		}

    

    

	}

	

}



else if ($TALLYREQUEST_S == 'PRODUCT ACKNOWLEDGEMENT') 

{

	

	$books = $doc->getElementsByTagName("PRODUCTDATA");

	foreach ($books as $book) 

	{

    

		//$TALLYREQUEST   = $book->getElementsByTagName("SALESDATA");

		//$TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;

    

    

    

    

		$DISTID   = $book->getElementsByTagName("DISTID");

		$DISTID_S = $DISTID->item(0)->nodeValue;

    

		$TRANSID   = $book->getElementsByTagName("PRODUCTCODE");

		$TRANSID_S = $TRANSID->item(0)->nodeValue;

    

		$SUCCESS   = $book->getElementsByTagName("SUCCESS");

		$SUCCESS_S = $SUCCESS->item(0)->nodeValue;

		

		if ($SUCCESS_S=="YES")

		{

		

			//$sql1234 = "update shifts set tally_update_status=1 

			//where id='$TRANSID_S'";

			

			

			$sql1234 = "update tbl_price_list_master set tally_update_status=1 where 

			Item_Code='$TRANSID_S' and RO_code='$DISTID_S'";

	

	  

		

			//echo $sql1234;

        

			$res1234 = mysqli_query($con, $sql1234);

		}

    

    

	}

	

}



else if ($TALLYREQUEST_S == 'CUSTOMER ACKNOWLEDGEMENT') 

{

	

	$books = $doc->getElementsByTagName("CUSTOMERDATA");

	foreach ($books as $book) 

	{

    

		//$TALLYREQUEST   = $book->getElementsByTagName("SALESDATA");

		//$TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;

    

    

    

    

		$DISTID   = $book->getElementsByTagName("DISTID");

		$DISTID_S = $DISTID->item(0)->nodeValue;

    

		$TRANSID   = $book->getElementsByTagName("CUSTOMERCODE");

		$TRANSID_S = $TRANSID->item(0)->nodeValue;

    

		$SUCCESS   = $book->getElementsByTagName("SUCCESS");

		$SUCCESS_S = $SUCCESS->item(0)->nodeValue;

		

		if ($SUCCESS_S=="YES")

		{

		

			

			//$sql1234 = "update tbl_price_list_master set tally_update_status=1 where id='$TRANSID_S'";

	

	        $sql1234 = "update tbl_customer_master set tally_update_status=1 where Customer_Code='$TRANSID_S'";

	

			//$res1 = mysqli_query($con, $sql);

		

			//echo $sql1234;

        

			$res1234 = mysqli_query($con, $sql1234);

		}

    

    

	}

	

}



else if ($TALLYREQUEST_S == 'DISTRIBUTOR REGISTRATION ACKNOWLEDGEMENT') 

{

	

	$books = $doc->getElementsByTagName("DISTRIBUTORDATA");

	foreach ($books as $book) 

	{

    

		//$TALLYREQUEST   = $book->getElementsByTagName("SALESDATA");

		//$TALLYREQUEST_S = $TALLYREQUEST->item(0)->nodeValue;

    

    

    

    

		$DISTID   = $book->getElementsByTagName("DISTID");

		$DISTID_S = $DISTID->item(0)->nodeValue;

    

		//$TRANSID   = $book->getElementsByTagName("TRANSID");

		//$TRANSID_S = $TRANSID->item(0)->nodeValue;

    

		$SUCCESS   = $book->getElementsByTagName("SUCCESS");

		$SUCCESS_S = $SUCCESS->item(0)->nodeValue;

		

		if ($SUCCESS_S=="YES")

		{

		

			

			//$sql1234 = "update tbl_price_list_master set tally_update_status=1 where id='$TRANSID_S'";

	

	        $sql1234 = "update tbl_ro_master set tally_update_status=1 where RO_code='$DISTID_S'";

	

			//$res1 = mysqli_query($con, $sql);

		

			//echo $sql1234;

        

			$res1234 = mysqli_query($con, $sql1234);

		}

    

    

	}

	

}























?>