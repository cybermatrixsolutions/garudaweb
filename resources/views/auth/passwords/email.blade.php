<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body>
<!--
    you can substitue the span of reauth email for a input with the email and
    include the remember me checkbox
    -->
	
	<div class="admin-header-wrap">
    <div class="container">
    <div id="header">
        <a href="#" id="logo"><img src="../images/logo/garrudanew.jpg"></a>		
	</div> 
	</div>
    </div>
<div class="container">
	<div class="row">
	<div class="col-md-6">
		  <div class="card card-container"> 
				<div class="circle"></div>
				<center> 
					@if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
				</center> 
			<form class="form-signin" action="{{ route('password.email') }}" method="post">
				 {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail Address</label>

                            <div>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
			
			  
			  <button type="submit" style="cursor: pointer;" class="btn btn-lg btn-success btn-block btn-signin"> Send Password Reset Link</button>
 <a class="btn btn-link" style="cursor: pointer;" href="{{ route('login') }}">
											Login here?
										</a>
			</form>
		</div>
    <div class="diright">
			<img src="../images/digital_lion.jpg" class="pull-right"/>
		</div>
	</div>
	<div class="col-md-6">
		<div class="login_right">
			<img src="../images/sales.png">
		</div>
		
	</div>
	</div>
	</div>
<footer>
	<div class="footer-sec">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
				</div>
				<div class="col-md-6">
					<img src="../images/ft-logo2.png" class="pull-right">
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- /container --> 
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script>
</body>
</html>