<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
   @include('backend-includes.header')

        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Add Slip Book</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="{{route('slipbooklist')}}"> Slip Book List</a></li>
        </ul>
      </div>
    </div>

    
      <br/>
      <br/>
      <br/>
       <div class="">
         @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
       @endif
          
           <center>
            @if(Session::has('success'))
               <font style="color:red">{!!session('success')!!}</font>
            @endif
          </center>
        </div>
        <div class="right vat">
		<a href="{{route('slipbooklist')}}" data-toggle="modal" >Slip Book List</a>
		</div>
      <form class="form-horizontal" id="form_slipbook" action="" method="post"  onsubmit="return confirm('Do you want to Continue?');">
          {{ csrf_field() }}
          <input type="hidden" name="RO_code" id="RO_code" value="{{Auth::user()->getRocode->RO_code}}">
         
        
        <table class="table table-striped no-footer">
          <thead>
            <tr>
				<th>Customer Name</th>
				<th>Date of issue</th>
				<th>No of books</th>
				<th>Slip Start</th>
				<th>Slip End</th>
				<th>No Of Slip <a style="float:right" id="addNew" class="btn btn-success" href="#">+</a></th>
            </tr>
          </thead>
          <tbody id="parant-app-row">
           <tr class='parent-tr' id="parant-row">
				<td>  
					<select class="form-control customer_code" required="required"  name="customer_code[]" ></select>
				</td>
				<td>
					<input class="form-control datepicker1 input-slip-issuedate" required="required" name="Slip_issue_date[]">
				</td>
				<td>
					<input class="form-control input-slip-noofbooks" required="required" name="Slip_no_of_book[]">
				</td>
				<td>
					<input class="form-control input-slip-start" required="required" name="Slip_start[]">
				</td>
				<td>
					<input class="form-control input-slip-end" required="required" name="Slip_end[]">
				</td>
				<td>
				   <a href='#'class='deleteNew' style="display:none;">X</a>
				  <input style="text-align: right;" type="text" class="form-control input-slip-number" required="required" name="no_of_slip[]" placeholder="" readonly>
				</td>
           </tr>
          </tbody>
        </table>
      
        
      
      
        <div class="form-group">
          <div>
            <button type="submit" class="btn btn-default pull-right" id="btnSubmit">Submit</button>
          </div>
        </div>
      </form>
   

   
 </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body">
        <div id="myModal_content"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div id="modalSlipList" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Slip List</h4>
      </div>
      <div class="modal-body">
        <div id="modalSlipList_content"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script  src="{{URL::asset('js/index.js')}}"></script> 
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

 jQuery("#numeriDesimal").keyup(function()
  {
    var $this = jQuery(this);
    $this.val($this.val().replace(/[^\d.]/g, ''));
    });
</script>
<script>
jQuery('.table-responsive').on('show.bs.dropdown', function () {
     jQuery('.table-responsive').css( "overflow", "inherit" );
});

jQuery('.table-responsive').on('hide.bs.dropdown', function () {
     jQuery('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">
jQuery(function(){
      var currentP=jQuery('#parant-row');
      var SlipBookHelpers={
		  
           init:function(){
			   
			  
				SlipBookHelpers.getcom();
				jQuery('#addNew').on('click',function(e){
					e.preventDefault();
					/*
					var cnt=jQuery('#parant-row').html();
					jQuery('#parant-app-row').append("<tr class='parent-tr'>"+cnt+"</tr>");
					currentP=jQuery('#parant-app-row').last();
					*/
					var cnt=jQuery('#parant-row').clone();
					  cnt.removeAttr('id').appendTo('#parant-app-row').find('input,select').each(function(){
						  jQuery(this).prev().show();
					  });
					  
					 jQuery(".datepicker1").datepicker({ 
						  autoclose: true, 
						  todayHighlight: true,
						  format: 'dd/mm/yyyy',
						  endDate : 'now',
					}).on('changeDate', function(){
					   currentP=jQuery(this).parents('.parent-tr');
		
					});
				});
				jQuery(document).on('click','.deleteNew',function(e){
				  e.preventDefault();
				  jQuery(this).parents('.parent-tr').remove();
					
				});
				jQuery("#form_slipbook").submit(function(event){
					event.preventDefault();
					SlipBookHelpers.addSlipNumbers();
				});
				jQuery(document).on('click','#btnViewSlipBookList',function(e){
					e.preventDefault();
					SlipBookHelpers.displaySlipBookList();
				});
				
				jQuery(document).on('click','#parant-app-row input',function(event){
				  currentP = jQuery(this).parents('.parent-tr');
				  
				});
				
	
				jQuery(document).on('keyup','.input-slip-start,.input-slip-end',function(event){
					var currentSlipStart=0,currentSlipEnd=0,currentSlipNumber=0;
					currentSlipStart = parseInt(currentP.find('.input-slip-start').val());
					currentSlipEnd = parseInt(currentP.find('.input-slip-end').val());
					if(currentSlipEnd > currentSlipStart){
						currentSlipNumber = parseInt(((currentSlipEnd - currentSlipStart)+1));
					}
					currentP.find('.input-slip-number').val(currentSlipNumber);
					console.log("currentSlipStart:"+currentSlipStart+"===currentSlipEnd:"+currentSlipEnd+"===currentSlipNumber:"+currentSlipNumber);
				});
				
				 jQuery(".datepicker1").datepicker({ 
							autoclose: true, 
							todayHighlight: true,
							format: 'dd/mm/yyyy',
							endDate : 'now',
					}).on('changeDate', function(){
                       currentP=jQuery(this).parents('.parent-tr');
                    });
			},
			getcom:function(){

				jQuery.get('getrocodes',{
					rocode:jQuery('#RO_code').val(),
					'_token': jQuery('meta[name="csrf-token"]').attr('content'),
				},function(data){
					var opt='';
					jQuery.each(data, function(index,value){
					opt+='<option value="'+index+'">'+value+'</option>';
					});
					currentP.find('.customer_code').html(opt);
				});
			}
			,
			addSlipNumbers : function(){
				
				jQuery("#myModal_content").html("");
					jQuery.ajax({
						url :"{{route('saveSlipBook')}}",
						type : "POST",
						data : jQuery("#form_slipbook").serialize(),
						headers : {
							_token: jQuery('meta[name="csrf-token"]').attr('content'),
						},
						dataType : "json",
						success : function(response){
							console.log("success:"+JSON.stringify(response));
							jQuery("#myModal").modal("show");
							if(response.success){
								if(response.message){
									jQuery("#myModal_content").append('<p class="text-success">'+response.message+'</p>');
								}
								if(response.redirectUrl){
									setTimeout(function(){
										location.href = response.redirectUrl;
									},1000);
								}	
							}
							else{
								if(response.message){
									jQuery("#myModal_content").append('<p class="text-danger">'+response.message+'</p>');
								}
								if(response.errors){
									for(var i=0;i<response.errors.length;i++){
										jQuery("#myModal_content").append('<p class="text-danger">'+response.errors[i]+'</p>');
									}
								}
							}
						}
						,
						error : function(err){
							console.log("err:"+JSON.stringify(err));
						}
					});
					return false;
			}
			,
			displaySlipBookList : function(){
				jQuery("#modalSlipList_content").html("");
				jQuery.ajax({
						url :"{{route('slipbooklist')}}",
						type : "GET",
						data : {},
						headers : {
							_token: jQuery('meta[name="csrf-token"]').attr('content'),
						},
						dataType : "html",
						success : function(response){
							jQuery("#modalSlipList").modal("show");
							jQuery("#modalSlipList_content").html(response);
						}
						,
						error : function(err){
							
						}
				});
				return false;
			}
     }
     SlipBookHelpers.init();
});
</script>
</body>
</html>