<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Driver  View</h1>
      </div>
      <div>
        <ul class="breadcrumb">
            <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('driver')}}">Driver  </a></li>
        </ul>
      </div>
    </div>
	
	
	                         <div class="">  <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                             </div>
	
	
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row" style="margin-top:-20px !important;">
          
         
          
                      <form class="form-horizontal" action="{{url('customer_driver_update')}}/{{$customber_driver_data->id}}" enctype="multipart/form-data" method="post">
            
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="customer_driver_ro_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
                    <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Pump Legal Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" disabled="disabled" required="required" name="customer_driver_ro_code" id="sel1">
                             
                                                    @foreach($datas as $ro_master)
                                                        <option name="company_id" value="{{$ro_master->RO_code}}"
                                                                @if($customber_driver_data->Ro_code == $ro_master->RO_code) selected @endif >{{$ro_master->pump_legal_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div> -->
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Customer Name</label>
                          <div class="col-sm-9">
                             <select class="form-control" disabled="disabled" required="required" name="customer_code" id="customer_code" data-customer="{{$customber_driver_data->customer_code}}">
                              
                                                   
                            </select>
                          </div>
                          </div>

                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Registration Number </label>
                          <div class="col-sm-9">
                            <select class="form-control "  disabled="disabled" required="required" name="Registration_Number" id="registration_number" data-registration="{{$customber_driver_data->Registration_Number}}">
                              
                                                   
                            </select>

                             </div>
                        </div>
                        
                            <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Driver Name</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" disabled="disabled" required="required" name="driver_name" id="" value="{{$customber_driver_data->Driver_Name}}" placeholder="Driver Name">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Gender</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" disabled="disabled" required="required" name="driver_name" id="" value="{{$customber_driver_data->gender}}" placeholder="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Aadhaar No</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" disabled="disabled" required="required" name="aadhaar_no" id="" value="{{$customber_driver_data->Aadhaar_no}}" placeholder="">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Aadhaar Photo </label>
                          <div class="col-sm-9">
                            
                            <br><img src="{{URL::asset('')}}/{{$customber_driver_data->Aadhaar_img}}" style="width: 250px; height: 150px;">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Licence No.</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" disabled="disabled" required="required" name="licence_no" id="" value="{{$customber_driver_data->Driver_Licence_No}}" placeholder="Phone Number">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Valid Upto</label>
                          <div class="col-sm-9">
                             <input type="text"  disabled="disabled" class="form-control" value="{{date('d/m/Y',strtotime($customber_driver_data->valid_upto))}}" required="required" name="valid_upto" id="" placeholder="Valid Upto">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Mobile(+91)</label>
                          <div class="col-sm-9">
                             <input type="text" pattern="[789][0-9]{9}" disabled="disabled" class="form-control" required="required" name="Driver_Mobile" id="" value="{{$customber_driver_data->Driver_Mobile}}" placeholder="Mobile ">
                          </div>
                        </div>
                         
                           <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Opening Date</label>
                          <div class="col-sm-9">
                            <input class="form-control datepicker" required="required" value="{{$customber_driver_data->Operating_date}}" name="Operating_date"  type="text" />
                          </div>
                        </div> -->

                            <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Email</label>
                          <div class="col-sm-9">
                             <input type="email" class="form-control" disabled="disabled" required="required" name="email" id=""  value="{{$customber_driver_data->email}}" placeholder="Email">
                          </div>
                        </div>

                            <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Photo of Licence</label>
                          <div class="col-sm-9">
                            <img @if($customber_driver_data->photo!=null) src="{{URL::asset('')}}{{$customber_driver_data->photo}}" @endif>
                          </div>
                        </div>
            
                       
                        
           
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>
	
	
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{url::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{url::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{url::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{url::asset('js/main.js')}}"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="{{url::asset('js/index.js')}}"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script type="text/javascript">
  jQuery(function(){
        var url1="{{url('getrocode')}}";
        var url2="{{url('getregierids')}}";
     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
               jQuery.get(url1,{
                rocode:jQuery('#sel1').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){
                   var customer=jQuery('#customer_code').data('customer');
                     if(customer==index)
                        opt+='<option selected value="'+index+'">'+value+'</option>';
                      else
                      opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);
                   vil.getcrg();
               });
           },
           getcrg:function(){

             jQuery.get(url2,{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var registration=jQuery('#registration_number').data('registration');
                 var opt='';
                  jQuery.each(data, function(index,value){

                     if(registration==value)
                        opt+='<option selected value="'+value+'">'+value+'</option>';
                      else
                      opt+='<option value="'+value+'">'+value+'</option>';

                  });
                  
                   
                jQuery('#registration_number').html(opt);
               });
           },

     }
     vil.init();
  });
</script>

<!-- <script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#sel1').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
               });
           },

     }
     vil.init();
  });
</script>

<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   //id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique',{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script> -->
</body>
</html>