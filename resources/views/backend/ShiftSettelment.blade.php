@extends('backend-includes.app')

@section('content')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Shift Settlement </h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Shift Settlement </a></li>
        </ul>
      </div>

    </div>
    <div class="">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            
            
          </div>
        </div>
      </div>
    </div>
    <!--end add box-->
    <!--list contant-->
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped myTable" id="myTable">
            <thead>
              <tr>
                <th>S.No.</th>
                 <th>Nozzles Sale</th>
                 <th>Credit Fuel Sales</th>
                 <th>Credit Lude & Other Sales</th>
                 <th>Cash Collections</th>
                 <th>Petro Card</th>
                  <th>Debit Card</th>
                 <th>Wallet</th>
                 <th>Difference</th>
                 <th>Employe Name</th>
                 <th>Shift Date</th>
                
            </thead>
            <tbody>

              <?php $i=0; ?>
               @foreach($ShiftSettelementModel as $ShiftSettelementModels)
              <?php $i++ ;?>
               <tr>
                <td>{{$i}}</td>
                <td>{{$ShiftSettelementModels->Nozzle_Amount}}</td>
                <td>{{$ShiftSettelementModels->Credit_fuel}}</td>
                <td>{{$ShiftSettelementModels->Credit_lube}}</td>
                <td>{{$ShiftSettelementModels->Cash}}</td>
                <td>{{$ShiftSettelementModels->Petrocard}}</td>
                <td>{{$ShiftSettelementModels->Debit_credit_card}}</td>
                <td>{{$ShiftSettelementModels->Wallet}}</td>Diff
                  <td>{{$ShiftSettelementModels->Diff}}</td>
                <td>{{$RoPersonalManagement->Personnel_Name}}</td>  
                <td>{{$ShiftSettelementModels->trans_date}}</td>
             </tr>
             @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>


@endsection