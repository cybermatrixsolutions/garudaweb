<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Slip Book List </h1>
       
      </div>
      <div>
        <ul class="breadcrumb">
            <li><a href="{{route('slipbooklist')}}"><i class="fa fa-home fa-lg"></i></a></li>
			<li><a href="{{route('slipbook')}}">Add Slip Book</a></li>
        </ul>
      </div>

    </div>
     <div class="">
      
      <ul>
        @foreach($errors->all() as $error)
          <li style="color: red">{{ $error}}</li>
        @endforeach
      </ul>
		
    </div>

	<div class="row">
		<div class="col-md-12 col-sm-12">

			<div class="row">
			   <center style="text-align: center;">@if(Session::has('success'))
			  <font style="color:red;">{!!session('success')!!}</font>
				@endif</center>
			  <div class="col-md-6 col-sm-7 col-xs-12 vat">
			  </div>
			</div>
		</div>
	</div>	
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
				<thead>
					<tr>
						<th>Customer Name</th>
						<th>Date of Issue</th>
						<th>No of Books</th>
						<th>Slip Start</th>
						<th>Slip End</th>
						<th>No Of Slip</th>
						<th>Actions</th>
					</tr>
				</thead>
                @if(isset($slipBookList) && $slipBookList!=null && count($slipBookList)>0)
				<tbody>
				  <?php $i=0;?>
					@foreach($slipBookList as $slip)
				  <?php $i++;?>
				  <tr>
					<tr>
						<td>{{$slip->getCustomer->company_name}}</td>
						<td>{{date("d/m/Y",strtotime($slip->Slip_issue_date))}}</td>
						<td>{{$slip->Slip_no_of_book}}</td>
						<td>{{$slip->Slip_start}}</td>
						<td>{{$slip->Slip_end}}</td>
						<td>{{$slip->no_of_slip}}</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-danger">Action</button>
								<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
								<ul class="dropdown-menu" role="menu">
								  <li><a href="slipbook/edit/{{$slip->id}}">Edit</a></li>
								  <li><a href="slipbook/delete/{{$slip->id}}" onclick="return confirm('are you sure to delete');">Delete</a></li>
								</ul>
							  </div>
						</td>
				   </tr>
					@endforeach
				 @endif	
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<!-- Javascripts--> 

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script>
<script>

  function validateFloatKeyPress() {
    var v = document.getElementById("creidt").value;
    if(v.length>7){
      alert("Please under 0-9999999 value");
     document.getElementById("buttontn").disabled = true;
    }
    else{
       document.getElementById("buttontn").disabled = false;
    }

 
}
function numberofdecimal() {
    var v = document.getElementById("decimal").value;
    if(v.length>2){
      alert("Please under 0-99 value");
     document.getElementById("buttontn").disabled = true;
    }
    else{
       document.getElementById("buttontn").disabled = false;
    }

 
}
  function myFunction() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
  $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
 jQuery(".numeriDesimal").keyup(function() {
	var $this = jQuery(this);
   

	$this.val($this.val().replace(/[^\d.]/g, ''));
});

</script>


<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
</body>
</html>