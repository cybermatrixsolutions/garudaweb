
@extends('backend-includes.app')

@section('content')
<div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Sales Register-Summary (Billed)
</h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          
         
          <li><a href="#">Sales Register-Summary (Billed)
</a></li>
        </ul>
      </div>
    </div>
    <?php

    $fromdate=date('d/m/Y');
    $to_date=date('d/m/Y');
    if(isset($_GET['to_date'])){
      $to_date=$_GET['to_date'];
    }

    if(isset($_GET['fromdate'])){
      $fromdate=$_GET['fromdate'];
    }

    ?>
     <div class="row">
        <div class="col-md-12" style="margin-top: 40px;;">
            <form class="form-horizontal form-inline" name="myForm" action="{{'salesRegisterBilledSummary'}}" enctype="multipart/form-data" method="get">
               
                    
                     {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label" for="pwd">Select Type <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                             <select class="form-control" name="fule_type" >
                                      <option @if($fule_type==2) selected @endif value="2">GST Bill</option>
                                         <option  value="GST Invoice">GST Invoice</option>
                                      <option @if($fule_type==1) selected @endif value="1">VAT Bill</option>
                              </select>
                        </div>
                        <div class="form-group">
                          <label class="control-label"  for="from">From <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                           <input type="text"  @if(isset($_GET['fromdate'])) value="{{$_GET['fromdate']}}" @endif class="form-control" id="fromdate" name="fromdate" placeholder="from date" required/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                          <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                            <input  type="text" @if(isset($_GET['to_date'])) value="{{$_GET['to_date']}}" @endif class="form-control" id="to_date" name="to_date" placeholder="To Date" required/>&nbsp;&nbsp;
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default" id="checkdate" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                        </div>

                    </div>
                </div>
            </form> 
        </div>

    </div>

    <div class="row" >
      @if(isset($bills))
       @if($bills->count()>0)
        <div class="col-md-9">
            </div>
         <div class="col-md-3" style="margin-top: 40px;">
            <a href="{{route('salesRegisterBilledDownloadSummary',['fule_type'=>$fule_type])}}?fromdate={{$fromdate}}&to_date={{$to_date}}" class="btn btn-default" style="float:right;width: auto;">Export in csv</a>
        </div>
      <div class="col-md-12">
        <div class="table-responsive" style="min-height: 400px;">
          <table class="table table-striped" id="myTable">
            <thead>
              
              <tr>
                
                <th>Inv.Type</th>
                <th>Invoice No.</th>
                <th>Invoice Date</th>
                
                <th>Party Code</th>
                <th>Party Name</th>
                 <th>GSTIN No.</th>
                
                <th> Place of Supply</th>
                <th>Invoice Amount</th>
                <th>VAT Taxable Value</th>
                 <th>GST Taxable Value</th>
                
                @foreach($taxs as $key=>$tax)
                <th>{{$key}} %</th>
                <th>{{$key}} Amount</th>
                @endforeach
                
                <th>Handling Charges</th>
                <th>Discount Amount</th>
                <th>Round Off </th>
              </tr>          
            </thead>
            <tbody>
              <?php $i=1; 
                $totalFinalAmount=0;

                $totalHandlingCharges=0;
                $totalDiscount=0;
                $totalRoundOff=0;
                 $totalTaxableValue=0;


              ?>
             @foreach($bills as $bill)

                 <?php   

             $totalFinalAmount=$totalFinalAmount+$bill->grand_total_amount;
             
              $totalHandlingCharges=$totalHandlingCharges+ $bill->handling_amount;
              $totalDiscount=$totalDiscount+$bill->discount;
              $totalRoundOff= $totalRoundOff+$bill->rounded_of;
              $totalTaxableValue=$totalTaxableValue+$bill->taxable_amt;
                 $tval=[];
                  ?>
                  @foreach($bill->trangection as $TransactionModel)

                  
                  <tr> 
                        
                        <td>@if($bill->type==1) VAT @else GST @endif</td>
                        <td>{{$bill->invoice_no}}</td>
                        <td>{{date('d/m/y',strtotime($TransactionModel->bill_date))}}</td>
                        
                        <td>{{$bill->customer_code}}</td>
                        <td>{{$bill->party->company_name}}</td>
                        <td>{{$bill->party->gst_no}}</td>
                         <td>{{$bill->party->getstate->name}}</td>
                         <td style="text-align: right;">{{bcadd($bill->grand_total_amount, 0, 2)}} </td>
                        <td style="text-align: right;">{{bcadd($bill->taxable_amt, 0, 2)}} </td>
                        <td style="text-align: right;">0.00</td>
                         
                      
                        <?php
                              $tratex=$TransactionModel->gettex()->get()->groupby('GST_Type');

                             // dd($TransactionModel,$tratex);
                         
                         ?>
                         @foreach($taxs as $key=>$tax)
                           <?php 
                               if(isset($tratex[$key])){
                                $getaxss=$tratex[$key]->first();
                                  
                                  if(isset($tval[$key]))
                                    $tval[$key]=$tval[$key]+$getaxss->Tax_Value;
                                  else
                                   $tval[$key]=$getaxss->Tax_Value;

                                   echo '<td style="text-align: right;"> '.$getaxss->Tax_percentage.'%</td><td style="text-align: right;">'.bcadd($getaxss->Tax_Value, 0, 2).' </td>';
                               }else{

                                    echo '<td style="text-align: right;"> 0.00</td><td style="text-align: right;">0.00 </td>';
                               }
                           ?>
                            
                         @endforeach
                       
                        <td style="text-align: right;">0.00</td>
                        <td style="text-align: right;">0.00</td>
                        <td style="text-align: right;">0.00</td>
                         
                  </tr>
                 <?php  $i++;?>
                  @endforeach
                 
                  <tr> 
                        
                        <td>@if($bill->type==1) VAT @else GST @endif</td>
                        <td>{{$bill->invoice_no}}</td>
                        <td>{{date('d/m/y',strtotime($TransactionModel->bill_date))}}</td>
                        
                        <td>{{$bill->customer_code}}</td>
                        <td>{{$bill->party->company_name}}</td>
                      
                        <td>{{$bill->party->gst_no}}</td>
                       
                        <td>{{$bill->party->getstate->name}}</td>
                        <td style="text-align: right;">{{bcadd($bill->grand_total_amount, 0, 2)}} </td>
                        <td style="text-align: right;">{{bcadd($bill->taxable_amt, 0, 2)}} </td>
                          <td style="text-align: right;">0.00</td>
                        
                    

                         @foreach($taxs as $key=>$tax)


                            @if(isset($commitionTexs[$key]))
                              <td style="text-align: right;">{{$commitionTexs[$key]['tax_percentage']}}%</td>
                              <td style="text-align: right;" class="{{$key}}">{{bcadd($commitionTexs[$key]['amount'], 0, 2)}}</td>
                            @else
                               <td style="text-align: right;"> 0.00</td>
                              <td style="text-align: right;"> 0.00</td>
                            @endif

                         @endforeach
                       
                        <td class="ClassHandlingAmount" style="text-align: right;">{{bcadd($bill->handling_amount, 0, 2)}}</td>
                        <td class="ClassDiscount" style="text-align: right;">{{bcadd($bill->discount, 0, 2)}}</td>
                        <td Class="ClassRoundof" style="text-align: right;">{{bcadd($bill->rounded_of, 0, 2)}} </td>
                         
                  </tr>
                <?php  $i++;?>
              @endforeach
            </tbody>
            

                     <tfoot border:solid 1px>
                      <tr>
                      </tr>
                      <tr>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp; </td>
                         <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>Grand Total :  </td>
                         <td style="text-align: right;">{{$totalFinalAmount}}</td>
                         <td style="text-align: right;">{{$totalTaxableValue}}</td>
                         <td style="text-align: right;">0.00</td>
                        <td colspan="2">&nbsp;&nbsp;</td>
                        <td colspan="2">&nbsp;&nbsp; </td>
                         <td colspan="2">&nbsp;&nbsp;</td>
                        <td colspan="2">&nbsp;&nbsp;</td>
                        <td colspan="2">&nbsp;&nbsp; </td>
                         <td style="text-align: right;">{{$totalHandlingCharges}}</td>
                        <td style="text-align: right;">{{$totalDiscount}}</td>
                        <td style="text-align: right;">{{$totalRoundOff}}</td>
                      </tr>
  

              </tfoot>

          </table>
         
        </div>
      </div>
       @else
          <p  class="h3" style="text-align: center;padding: 25px;background: #fff;">No record found </p>
        @endif
    </div>
     @endif
    </div>
@endsection
@section('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
 


jQuery(function(){
    var enddated=new Date();
    var todate=new Date();
      var app={
               
              init:function(){
               
                
                app.fromDate();
                app.to_date();
             
              },
              fromDate:function(){
                
                jQuery("#fromdate").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         endDate: new Date(),
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     jQuery("#to_date").val('');
                     enddated=jQuery(this).val();
                     todate=new Date();
                     app.to_date();
                    });
              },
              to_date:function(){
                  
                  jQuery("#to_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         startDate:enddated,
                        
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).datepicker('setStartDate',enddated).datepicker('setEndDate',todate);

              },

      };

      app.init();
});

</script>

@endsection