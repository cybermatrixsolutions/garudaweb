@extends('backend-includes.app')

@section('content')
<div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Itemwise Nozzle Wise Reading Report </h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          
         
          <li><a href="#">Itemwise Nozzle Wise Reading Report </a></li>
        </ul>
      </div>
    </div>
    <?php

    $fromdate=date('d/m/Y');
    $to_date=date('d/m/Y');
    if(isset($_GET['to_date'])){
      $to_date=$_GET['to_date'];
    }

    if(isset($_GET['fromdate'])){
      $fromdate=$_GET['fromdate'];
    }

    ?>
     <div class="row">
        <div class="col-md-12" style="margin-top: 40px;;">
            <form class="form-horizontal form-inline" name="myForm" action="{{route('reading-report')}}" enctype="multipart/form-data" method="get">
               
                    
                     {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="form-group">
                          <label class="control-label"  for="from">From <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                           <input type="text"  @if(isset($_GET['fromdate'])) value="{{$_GET['fromdate']}}" @endif class="form-control" id="fromdate" name="fromdate" placeholder="from date" required/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                          <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                            <input  type="text" @if(isset($_GET['to_date'])) value="{{$_GET['to_date']}}" @endif class="form-control" id="to_date" name="to_date" placeholder="To Date" required/>&nbsp;&nbsp;
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default" id="checkdate" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                        </div>

                    </div>
                </div>
            </form> 
        </div>

    </div>

    <div class="row" >
      @if(isset($readings))
       @if($readings->count()>0)
        <div class="col-md-9">
            </div>
         <div class="col-md-3" >
            <a href="{{route('reading-report')}}/download?fromdate={{$fromdate}}&to_date={{$to_date}}" class="btn btn-default" style="float:right;width: auto;">Export in csv</a>
        </div>
      <div class="col-md-12">
        <div class="table-responsive" style="min-height: 400px;">
         
          <table class="table table-striped" id="myTable">
            <thead>
              
              <tr>
                <th>S.No.</th>
                <th>Shift</th>
                <th>Item</th>
                <th>Tank No.</th>
                
                <th>Totalizer Number</th>
                <th>OMR</th>
                <th>CMR</th>
                <th>Totalizer Out Qty</th>
                <th>Totalizer Test Qty</th>
                <th>Nett Sales Qty</th>
                <th>Credit Sales Qty</th>
                <th>Other Sales Qty</th>
                
              </tr>          
            </thead>
            <tbody>
              <?php $j=1;?>
              @foreach($readings as $shifts)
                 <?php $creditsales=0; $i=0; $natesales=0;
                        $GgotalizerOutQty=0;
                        $GnettSalesQty=0;
                        $testsales=0;
                        
                 ?>
                 @foreach($shifts as $reading)
                 <?php
                      $gotalizerOutQty=0;
                      $nettSalesQty=0;
                     if($i==0){
                      
                      foreach ($reading->getCreditSales as $getCreditSale) {
                       $creditsales=$getCreditSale->petroldiesel_qty+$creditsales;
                      }
                      
                     }
                      
                    $natesales=($reading->Nozzle_End-$reading->Nozzle_Start)+$natesales;
                    $gotalizerOutQty=$reading->Nozzle_End-$reading->Nozzle_Start;
                    $nettSalesQty=$reading->Nozzle_End-$reading->Nozzle_Start-$reading->test;
                    $GgotalizerOutQty=$gotalizerOutQty+$GgotalizerOutQty;
                    $GnettSalesQty=$nettSalesQty+$GnettSalesQty;
                    $testsales=$reading->test+$testsales;

                 ?>
                 
                  <tr>
                    <td>{{$j}}</td>
                     <td>@if($reading->getShift!=null){{date('d/m/Y  h:i:s A',strtotime($reading->getShift->created_at))}}  To {{date('d/m/Y  h:i:s A',strtotime($reading->getShift->closer_date))}}@endif
                                   
                    
                    <td>{{$reading->getNozzle->getItem->Item_Name}}</td>
                    <td>{{$reading->getNozzle->getTank->Tank_Number}}</td>
                    <td>{{$reading->Nozzle_No}}</td>
                    <td  style="text-align: right;">{{bcadd($reading->Nozzle_Start,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($reading->Nozzle_End,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($gotalizerOutQty,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($reading->test,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($nettSalesQty,0,3)}}</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                  </tr>
                  <?php $j++; ?>
                 @endforeach
                    <tr>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>Totals</td>
                    <td  style="text-align: right;">{{bcadd($GgotalizerOutQty,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($testsales,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($GnettSalesQty,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($creditsales,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($GnettSalesQty-$creditsales,0,3)}}</td>
                  </tr>

              @endforeach
              
            </tbody>
          </table>
         
        </div>
      </div>
       @else
          <p  class="h3" style="text-align: center;padding: 25px;background: #fff;">No record found </p>
        @endif
    </div>
    @endif
    </div>
@endsection
@section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
 


jQuery(function(){
    var enddated=new Date();
    var todate=new Date();
      var app={
               
              init:function(){
               
                
                app.fromDate();
                app.to_date();
             
              },
              fromDate:function(){
                
                jQuery("#fromdate").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         endDate: new Date(),
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     jQuery("#to_date").val('');
                     enddated=jQuery(this).val();
                     todate=new Date();
                     app.to_date();
                    });
              },
              to_date:function(){
                  
                  jQuery("#to_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         startDate:enddated,
                        
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).datepicker('setStartDate',enddated).datepicker('setEndDate',todate);

              },

      };

      app.init();
});

</script>
@endsection