@extends('backend-includes.app')

@section('content')
<div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Sales Register (Billed) </h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          
         
          <li><a href="#">Sales Register (Billed) </a></li>
        </ul>
      </div>
    </div>
    <?php

    $fromdate=date('d/m/Y');
    $to_date=date('d/m/Y');
    if(isset($_GET['to_date'])){
      $to_date=$_GET['to_date'];
    }

    if(isset($_GET['fromdate'])){
      $fromdate=$_GET['fromdate'];
    }

    ?>
     <div class="row">
        <div class="col-md-12" style="margin-top: 40px;;">
            <form class="form-horizontal form-inline" name="myForm" action="{{'salesRegisterBilled'}}" enctype="multipart/form-data" method="get">
               
                    
                     {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label" for="pwd">Select Type <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                             <select class="form-control" name="fule_type" >
                                      <option @if($fule_type==2) selected @endif value="2">GST</option>
                                      <option @if($fule_type==1) selected @endif value="1">VAT</option>
                              </select>
                        </div>
                        <div class="form-group">
                          <label class="control-label"  for="from">From <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                           <input type="text"  @if(isset($_GET['fromdate'])) value="{{$_GET['fromdate']}}" @endif class="form-control" id="fromdate" name="fromdate" placeholder="from date" required/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                          <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                            <input  type="text" @if(isset($_GET['to_date'])) value="{{$_GET['to_date']}}" @endif class="form-control" id="to_date" name="to_date" placeholder="To Date" required/>&nbsp;&nbsp;
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default" id="checkdate" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                        </div>

                    </div>
                </div>
            </form> 
        </div>

    </div>

    <div class="row" >
      @if(isset($bills))
       @if($bills->count()>0)
        <div class="col-md-9">
            </div>
         <div class="col-md-3" style="margin-top: 40px;;">
            <a href="{{route('salesRegisterBilledDownload',['fule_type'=>$fule_type])}}?fromdate={{$fromdate}}&to_date={{$to_date}}" class="btn btn-default" style="float:right;width: auto;">Export in csv</a>
        </div>
      <div class="col-md-12">
        <div class="table-responsive" style="min-height: 400px;">
         
          <table class="table table-striped" id="myTable">
            <thead>
              
              <tr>
                
                <th>Inv.Type</th>
                <th>Invoice No.</th>
                <th>Invoice Date</th>
                <th>Invoice Amount</th>
                <th>Party Code</th>
                <th>Party Name</th>
                <th>Item Group</th>
                <th>Item Sub-Group</th>
                <th>Item Code</th>
                <th>Item Name</th>
                <th>HSN Code</th>
                <th>Item Qty Sold</th>
                <th>Item Rate</th>
                @foreach($taxs as $key=>$tax)
                <th>{{$key}} %</th>
                <th>{{$key}} Amount</th>
                @endforeach
                
                <th>Handling Charges</th>
                <th>Discount Amount</th>
                <th>Round Off </th>
              </tr>          
            </thead>
            <tbody>
              <?php $i=1; ?>
             @foreach($bills as $bill)
                 <?php $tval=[]; ?>
                  @foreach($bill->trangection as $TransactionModel)
              
                  
                  <tr> 
                        
                        <td>@if($bill->type==1) VAT @else GST @endif</td>
                        <td>{{$bill->invoice_no}}</td>
                        <td>{{date('d/m/y',strtotime($bill->created_at))}}</td>
                        <td style="text-align: right;">{{bcadd($bill->grand_total_amount, 0, 2)}} </td>
                        <td>{{$bill->customer_code}}</td>
                        <td>{{$bill->party->company_name}}</td>
                        <td>{{$TransactionModel->getItemName->getgroup->Group_Name}}</td>
                        <td>{{$TransactionModel->getItemName->getsubgroup->Group_Name}}</td>
                        <td>{{$TransactionModel->getItemName->Item_Code}}</td>
                        <td>{{$TransactionModel->getItemName->Item_Name}}</td>
                        <td>{{$TransactionModel->getItemName->hsncode}}</td>
                        <td style="text-align: right;">{{$TransactionModel->petroldiesel_qty}}</td>
                        <td style="text-align: right;">{{bcadd($TransactionModel->item_price, 0, 2)}}</td>
                        <?php
                              $tratex=$TransactionModel->gettex()->get()->groupby('GST_Type');
                         
                         ?>
                         @foreach($taxs as $key=>$tax)
                           <?php 
                               if(isset($tratex[$key])){
                                $getaxss=$tratex[$key]->first();
                                  
                                  if(isset($tval[$key]))
                                    $tval[$key]=$tval[$key]+$getaxss->Tax_Value;
                                  else
                                   $tval[$key]=$getaxss->Tax_Value;

                                   echo '<td style="text-align: right;"> '.$getaxss->Tax_percentage.'%</td><td style="text-align: right;">'.bcadd($getaxss->Tax_Value, 0, 2).' </td>';
                               }else{

                                    echo '<td> </td><td> </td>';
                               }
                           ?>
                            
                         @endforeach
                       
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp; </td>
                         
                  </tr>
                 <?php  $i++;?>
                  @endforeach
                 
                  <tr> 
                        
                        <td>@if($bill->type==1) VAT @else GST @endif</td>
                        <td>{{$bill->invoice_no}}</td>
                        <td>{{date('d/m/y',strtotime($bill->created_at))}}</td>
                        <td style="text-align: right;">{{bcadd($bill->grand_total_amount, 0, 2)}}</td>
                        <td>{{$bill->customer_code}}</td>
                        <td>{{$bill->party->company_name}}</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>Totals</td>
                        <td>&nbsp;&nbsp;</td>
                        <td style="text-align: right;">{{bcadd($bill->qtl, 0, 2)}}</td>
                        <td>&nbsp;&nbsp;</td>
                        <?php $commitionTexs=@unserialize($bill->service_taxes_by_type); ?>

                         @foreach($taxs as $key=>$tax)

                         

                            @if(isset($commitionTexs[$key]))
                              <td style="text-align: right;">{{$commitionTexs[$key]['tax_percentage']}}%</td>
                              <td style="text-align: right;">{{bcadd($commitionTexs[$key]['amount'], 0, 2)}}</td>
                            @else
                               <td> &nbsp;&nbsp;</td>
                              <td> &nbsp;&nbsp;</td>
                            @endif

                         @endforeach
                       
                        <td style="text-align: right;">{{bcadd($bill->handling_amount, 0, 2)}}</td>
                        <td style="text-align: right;">{{bcadd($bill->discount, 0, 2)}}</td>
                        <td style="text-align: right;">{{bcadd($bill->rounded_of, 0, 2)}} </td>
                         
                  </tr>
                <?php  $i++;?>
              @endforeach
            </tbody>
          </table>
         
        </div>
      </div>
       @else
          <p  class="h3" style="text-align: center;padding: 25px;background: #fff;">No record found </p>
        @endif
    </div>
     @endif
    </div>
@endsection
@section('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
 


jQuery(function(){
    var enddated=new Date();
    var todate=new Date();
      var app={
               
              init:function(){
               
                
                app.fromDate();
                app.to_date();
             
              },
              fromDate:function(){
                
                jQuery("#fromdate").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         endDate: new Date(),
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     jQuery("#to_date").val('');
                     enddated=jQuery(this).val();
                     todate=new Date();
                     app.to_date();
                    });
              },
              to_date:function(){
                  
                  jQuery("#to_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         startDate:enddated,
                        
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).datepicker('setStartDate',enddated).datepicker('setEndDate',todate);

              },

      };

      app.init();
});

</script>

@endsection