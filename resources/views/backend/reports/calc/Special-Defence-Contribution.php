
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>calculator</title>
<link href="css/style.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker.min.css" rel="stylesheet">
<link href="css/angular-ui.min.css" rel="stylesheet">
<!--<link href="css/angular.css" rel="stylesheet">-->
<style>
.space > tr
{

padding:5px;

}
.sp
{

padding-right:50px;
padding-left:50px;


}

</style>
</head>

<body>

<header class="headerbar">

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-7 hidden-lg hidden-md hidden-sm">
<div class="toggle" id="menu-toggle">
<span class="line line1"></span>
<span class="line line2"></span>
<span class="line line3"></span>
</div>
</div>

<div class="col-lg-3 col-sm-6 col-md-3 col-xs-5">
<div class="logo"><a href="#"><img src="img/logo.png" alt=""></a></div>
</div>

</header>

<div class="container-fluid">
<div class="row">

<div class="main">
<!--<div class="col-md-2 col-xs-12">
<div class="blur">

<aside id="sidebar">
<div id="sidebar-wrapper" class="field input">
 <input type="text" value="" name="" placeholder="company name"  autofocus>
 <input type="text" value="" name="" placeholder="address">
 <input type="text" value="" name="" placeholder="tic">
 <input type="text" value="" name="" placeholder="telephone">
 <select>
 <option>option</option>
 <option>option</option>
 </select>
 <div class="date input-group" data-provide="datepicker">
  <input type="text" class="input-group-addon" id="paymentdate" placeholder="Mandatory">
  </div>
  
 <select>
 <option selected>option</option>
 <option selected>option</option>
 </select>
 <input type="text" value="" name="" placeholder="cheque No">
 <select>
 <option selected>select option</option>
 <option selected>option</option>
 <option selected>option</option>
 <option selected>option</option>
 </select>
 
 <input type="text" value="" name="" placeholder="name">
 
 <div class="btn-area">
 <button type="button" value="">clear</button>
 <button type="button" value="">submit</button>
 </div>
 
</div>

</aside>
</div>
</div> --><link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
	<div class="rightbar-area">
		<div ng-app="productsTableApp">
			<div ng-controller="ProductsController"> 
				<div class="btn-section">
					<table>
						<tr><center><p style="font-size:18px ">Special Defence Contribution</p></center></tr>
						<tr>
							<center>
								<td> <div class="date input-group">
									<input type="text" class="input-group-addon" id="paymentdate" placeholder="select Date" onkeypress="return false;">
								</div></td>
								<td></td>

								<td class="sp">
									<select onchange="location = this.options[this.selectedIndex].value;">
										<option value="#">Please select</option>
										<option value="corporation.php">Corporation</option>
										<option value="paye.php"> P.A.Y.E /Special Contribution </option>
										<option value="with-holding-tax.php">Withholding</option>
										<option value="Special-Defence-Contribution.php" selected> Special Defence Contribution </option>
									</select>
								</td>
								<td> <button onclick="calculate()" style="float:right">Calculate</button> </td>
								<td> <button onclick="add()" style="float:right" id="add_id">+</button> </td>
								<td> <button onclick="subtract()" style="float:right" id="minus_id">-</button> </td>
								<td style="padding:0px 30px;"> <button id="collapse-init" class="">Expend</button></td>

							</center>
						</tr>
					</table>
				</div>
				<div class="tables-group">
					<table class="table table-condensed table-responsive table-striped">
						<tbody>
							<tr >
								<td>
									<table class="table-responsive">
										<tr >
											<td style=" font-weight: bold; color: Red;">
												INTEREST PAID for the month ending(code 0602): 
												<span style="text-align:center;float:right;cursor: pointer;padding: 5px;" class="pull-right">
													<i id="i_showhide1" class="fa  sdcshowhide1 fa-minus" data-toggle="collapse" data-target="#demo1" class="accordion-toggle">
													</i>
												</span>
											</td>
											<td colspan="1" style="width:150px;font-weight:bold;">
												<input type="text" id="txt_sdcIpintpaidenddate1" class="form-control clssdc cls1" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
											</td>
											<td class="hideSec" colspan="1" style="width:150px;font-weight:bold;">
												<input type="text" id="txt_sdcIpintpaidenddate1_2nd" class="form-control clssdc cls1" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
											</td>
											<td class="hideSec_next" colspan="1" style="width:150px;font-weight:bold;">
												<input type="text" id="txt_sdcIpintpaidenddate1_3rd" class="form-control clssdc cls1" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
											</td>
											<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="txt_sdcIpintpaidenddate1_total">Total</output>
											</td>
										</tr>
									</table>
								</td>
							</tr>

							<tr class="accordian-body collapse panel-collapse" id="demo1">  
								<td>
									<table class="table secondLevel">
										<tr style="">
											<td >
												Payment by Selfassessment?(Y/N)
											</td>
											<td colspan="1" style="width: 150px;">
												<select id="sel_sdcIppayselfass1" dir="rtl" class="form-control corpselfass">
													<option selected="selected" value="0">-Select-</option>
													<option value="1">Yes</option>
													<option value="2">No</option>
												</select>
											</td>
											<td class="hideSec" colspan="1" style="width: 150px;">
												<select id="sel_sdcIppayselfass1_2nd" dir="rtl" class="form-control corpselfass">
													<option selected="selected" value="0">-Select-</option>
													<option value="1">Yes</option>
													<option value="2">No</option>
												</select>
											</td>
											<td class="hideSec_next" colspan="1" style="width: 150px;">
												<select id="sel_sdcIppayselfass1_3rd" dir="rtl" class="form-control corpselfass">
													<option selected="selected" value="0">-Select-</option>
													<option value="1">Yes</option>
													<option value="2">No</option>
												</select>
											</td>
											<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sel_sdcIppayselfass1_total"></output>
											</td>
										</tr>
										<!-- <tr style="display: none;">
								<td >
									End of month:
								</td>
								<td colspan="1" style="width: 150px;">
									<output id="sp_sdcIpendofmonth1" class="pull-right">-</output>
								</td>
							</tr>
							<tr style="display: none;">
								<td >
									End of following month:
								</td>
								<td colspan="1" style="width: 150px;">
									<output id="sp_sdcIpendoffollmonth1" class="pull-right">-</output>
								</td>
							</tr> -->

							<tr style="">
								<td >
									Reduced SDC rate?(pmt to provided funds?):
								</td>
								<td colspan="1" style="width: 150px;">
									<select id="sel_sdcIpredsdcratetype1" dir="rtl" style="width:50%;float:left;height:auto;padding:6px 12px;" class="sdcIpselfass">
										<option value="1">Yes</option>
										<option selected="selected" value="2">No</option>
									</select>
									<output id="sp_sdIpcredsdcrate1" style="width:45%;float:right;padding:8px;text-align:right;color:Red;">0,00%</output>
								</td>
								<td class="hideSec" colspan="1" style="width: 150px;">
									<select id="sel_sdcIpredsdcratetype1_2nd" dir="rtl" style="width:50%;float:left;height:auto;padding:6px 12px;" class="sdcIpselfass">
										<option value="1">Yes</option>
										<option selected="selected" value="2">No</option>
									</select>
									<output id="sp_sdIpcredsdcrate1_2nd" style="width:45%;float:right;padding:8px;text-align:right;color:Red;">0,00%</output>
								</td>
								<td class="hideSec_next" colspan="1" style="width: 150px;">
									<select id="sel_sdcIpredsdcratetype1_3rd" dir="rtl" style="width:50%;float:left;height:auto;padding:6px 12px;" class="sdcIpselfass">
										<option value="1">Yes</option>
										<option selected="selected" value="2">No</option>
									</select>
									<output id="sp_sdIpcredsdcrate1_3rd" style="width:45%;float:right;padding:8px;text-align:right;color:Red;">0,00%</output>
								</td>
								<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
									<output id="sp_sdIpcredsdcrate1_total"></output>
								</td>
							</tr>
							<tr style="">
								<td >
									Interest Amount:
								</td>
								<td colspan="1" style="width: 150px;">
									<input name="txt_sdcIpintamt1" type="text" id="txt_sdcIpintamt1" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
								</td>
								<td class="hideSec" colspan="1" style="width: 150px;">
									<input name="txt_sdcIpintamt1" type="text" id="txt_sdcIpintamt1_2nd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
								</td>
								<td class="hideSec_next" colspan="1" style="width: 150px;">
									<input name="txt_sdcIpintamt1" type="text" id="txt_sdcIpintamt1_3rd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
								</td>
								<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
									<output id="txt_sdcIpintamt1_total"></output>
								</td>
							</tr>
							<tr style="">
								<td style=" font-weight: bold;">
									SDC:
								</td>
								<td colspan="1" style="width: 150px; font-weight: bold;">
									<output id="sp_sdcIprate1" class="pull-right">0,00</output>
								</td>
								<td class="hideSec" colspan="1" style="width: 150px; font-weight: bold;">
									<output id="sp_sdcIprate1_2nd" class="pull-right">0,00</output>
								</td>
								<td class="hideSec_next" colspan="1" style="width: 150px; font-weight: bold;">
									<output id="sp_sdcIprate1_3rd" class="pull-right">0,00</output>
								</td>
								<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcIprate1_total"></output>
											</td>
							</tr>
							<tr style="">
								<td >
									Monetary Charge 5%
								</td>
								<td colspan="1" style="width: 150px;">
									<output id="sp_sdcIpmoncharge1" class="pull-right">0,00</output>
								</td>
								<td class="hideSec" colspan="1" style="width: 150px;">
									<output id="sp_sdcIpmoncharge1_2nd" class="pull-right">0,00</output>
								</td>
								<td class="hideSec_next" colspan="1" style="width: 150px;">
									<output id="sp_sdcIpmoncharge1_3rd" class="pull-right">0,00</output>
								</td>
								<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcIpmoncharge1_total"></output>
											</td>
							</tr>
							<tr style="">
								<td >
									Interest and penalties:
								</td>
								<td colspan="1" style="width: 150px;">
									<output id="sp_sdcIpintpen1" class="pull-right">0,00</output>
								</td>
								<td class="hideSec" colspan="1" style="width: 150px;">
									<output id="sp_sdcIpintpen1_2nd" class="pull-right">0,00</output>
								</td>
								<td class="hideSec_next" colspan="1" style="width: 150px;">
									<output id="sp_sdcIpintpen1_3rd" class="pull-right">0,00</output>
								</td>
								<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcIpintpen1_total"></output>
											</td>

							</tr>
							<tr style="">
								<td style=" font-weight: bold;">
									Total SDC + interest and penalties:
								</td>
								<td colspan="1" style="width: 150px; font-weight: bold;">
									<output id="sp_scdIptotsdcintpen1" class="pull-right">0,00</output>
								</td>
								<td class="hideSec" colspan="1" style="width: 150px; font-weight: bold;">
									<output id="sp_scdIptotsdcintpen1_2nd" class="pull-right">0,00</output>
								</td>
								<td class="hideSec_next" colspan="1" style="width: 150px; font-weight: bold;">
									<output id="sp_scdIptotsdcintpen1_3rd" class="pull-right">0,00</output>
								</td>
								<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_scdIptotsdcintpen1_total"></output>
											</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table class="table-responsive">                                                  
							<tr>

								<td style=" font-weight: bold; color: Red;">
									DIVIDENTS PAID for the month ending(code 0603): 
									<span data-toggle="collapse" data-target="#demo2" class="accordion-toggle" style="text-align:center;float:right;cursor:pointer;padding:5px;" class="pull-right">
										<i id="i_sdcshowhide2" class="fa  sdcshowhide2 fa-minus">
										</i>
									</span>
								</td>
								<td colspan="1" style="width: 150px; font-weight: bold;">
									<input type="text" id="txt_sdcDppaidenddate1" class="form-control clssdc cls2" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
								</td>
								<td class="hideSec" colspan="1" style="width: 150px; font-weight: bold;">
									<input type="text" id="txt_sdcDppaidenddate1_2nd" class="form-control clssdc cls2" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
								</td>
								<td class="hideSec_next" colspan="1" style="width: 150px; font-weight: bold;">
									<input type="text" id="txt_sdcDppaidenddate1_3rd" class="form-control clssdc cls2" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
								</td>
								<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="txt_sdcDppaidenddate1_total"></output>
											</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="accordian-body collapse panel-collapse" id="demo2">  
					<td>
						<table class="table secondLevel">
							<tr style="">
								<td >
									Payment by Selfassessment?(Y/N):
								</td>
								<td colspan="1" style="width: 150px;">
									<select id="sel_sdcDppayselfass1" dir="rtl" class="form-control corpselfass">
										<option selected="selected" value="0">-Select-</option>
										<option value="1">Yes</option>
										<option value="2">No</option>
									</select>
								</td>
								<td class="hideSec" colspan="1" style="width: 150px;">
									<select id="sel_sdcDppayselfass1_2nd" dir="rtl" class="form-control corpselfass">
										<option selected="selected" value="0">-Select-</option>
										<option value="1">Yes</option>
										<option value="2">No</option>
									</select>
								</td>
								<td class="hideSec_next" colspan="1" style="width: 150px;">
									<select id="sel_sdcDppayselfass1_3rd" dir="rtl" class="form-control corpselfass">
										<option selected="selected" value="0">-Select-</option>
										<option value="1">Yes</option>
										<option value="2">No</option>
									</select>
								</td>
								<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sel_sdcDppayselfass1_total"></output>
											</td>
							</tr>
					<!-- <tr style="display: none;">
						<td >
							End of month:
						</td>
						<td colspan="1" style="width: 150px;">
							<output id="sp_sdcDpendofmonth1" class="pull-right">-</output>
						</td>
					</tr>
					<tr style="display: none;">
						<td >
							End of following month:
						</td>
						<td colspan="1" style="width: 150px;">
							<output id="sp_sdcDpendoffollmonth1" class="pull-right">-</output>
						</td>
					</tr> -->
					<tr style="">
						<td >
							SDC rate:
						</td>
						<td colspan="1" style="width: 150px; color: Red;">
							<output id="sp_sdcDpscrate1" class="pull-right">0,00%</output>
						</td>
						<td class="hideSec" colspan="1" style="width: 150px; color: Red;">
							<output id="sp_sdcDpscrate1_2nd" class="pull-right">0,00%</output>
						</td>
						<td class="hideSec_next" colspan="1" style="width: 150px; color: Red;">
							<output id="sp_sdcDpscrate1_3rd" class="pull-right">0,00%</output>
						</td>
						<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcDpscrate1_total"></output>
											</td>
					</tr>
					<tr style="">
						<td >
							Dividend Amount:
						</td>
						<td colspan="1" style="width: 150px;">
							<input name="txt_sdcDpdivamt1" type="text" id="txt_sdcDpdivamt1" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
						</td>
						<td class="hideSec" colspan="1" style="width: 150px;">
							<input name="txt_sdcDpdivamt1" type="text" id="txt_sdcDpdivamt1_2nd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
						</td>
						<td class="hideSec_next" colspan="1" style="width: 150px;">
							<input name="txt_sdcDpdivamt1" type="text" id="txt_sdcDpdivamt1_3rd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
						</td>
						<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="txt_sdcDpdivamt1_total"></output>
											</td>
					</tr>
					<tr style="">
						<td style=" font-weight: bold;">
							SDC:
						</td>
						<td colspan="1" style="width: 150px; font-weight: bold;">
							<output id="sp_sdcDprate1" class="pull-right">0,00</output>
						</td>
						<td  class="hideSec" colspan="1" style="width: 150px; font-weight: bold;">
							<output id="sp_sdcDprate1_2nd" class="pull-right">0,00</output>
						</td>
						<td class="hideSec_next" colspan="1" style="width: 150px; font-weight: bold;">
							<output id="sp_sdcDprate1_3rd" class="pull-right">0,00</output>
						</td>
						<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcDprate1_total"></output>
											</td>
					</tr>
					<tr style="">
						<td >
							Monetary Charge 5%:
						</td>
						<td colspan="1" style="width: 150px;">
							<output id="sp_sdcDpmoncharge1" class="pull-right">0,00</output>
						</td>
						<td class="hideSec" colspan="1" style="width: 150px;">
							<output id="sp_sdcDpmoncharge1_2nd" class="pull-right">0,00</output>
						</td>
						<td class="hideSec_next" colspan="1" style="width: 150px;">
							<output id="sp_sdcDpmoncharge1_3rd" class="pull-right">0,00</output>
						</td>
						<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcDpmoncharge1_total"></output>
											</td>
					</tr>
					<tr style="">
						<td >
							Interest and penalties:
						</td>
						<td colspan="1" style="width: 150px;">
							<output id="sp_sdcDpintpen1" class="pull-right">0,00</output>
						</td>
						<td class="hideSec" colspan="1" style="width: 150px;">
							<output id="sp_sdcDpintpen1_2nd" class="pull-right">0,00</output>
						</td>
						<td class="hideSec_next" colspan="1" style="width: 150px;">
							<output id="sp_sdcDpintpen1_3rd" class="pull-right">0,00</output>
						</td>
						<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcDpintpen1_total"></output>
											</td>
					</tr>
					<tr style="">
						<td style=" font-weight: bold;">
							Total SDC + interest and penalties:
						</td>
						<td colspan="1" style="width: 70px; font-weight: bold;">
							<output id="sp_scdDptotsdcintpen1" class="pull-right">0,00</output>
						</td>
						<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
							<output id="sp_scdDptotsdcintpen1_2nd" class="pull-right">0,00</output>
						</td>
						<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
							<output id="sp_scdDptotsdcintpen1_3rd" class="pull-right">0,00</output>
						</td>
						<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_scdDptotsdcintpen1_total"></output>
											</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table class="table-responsive">


					<tr>
						<td style=" font-weight: bold; color: Red;">
							RENTS PAID for the month ending(after 30/06/2011)(code 0614):                                                    
							<span data-toggle="collapse" data-target="#demo3" class="accordion-toggle" style="text-align:center;float:right;cursor: pointer; padding: 5px;" class="pull-right">
							<i id="i_sdcshowhide3" class="fa  sdcshowhide3 fa-minus">
							</i>
						</span>
					</td>
					<td colspan="1" style="width: 70px; font-weight: bold;">
						<input type="text" id="txt_sdcRppaidenddate1" class="form-control clssdc cls3" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
					</td>
					<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
						<input type="text" id="txt_sdcRppaidenddate1_2nd" class="form-control clssdc cls3" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
					</td>
					<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
						<input type="text" id="txt_sdcRppaidenddate1_3rd" class="form-control clssdc cls3" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
					</td>
					<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="txt_sdcRppaidenddate1_total"></output>
											</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="accordian-body collapse panel-collapse" id="demo3">  
		<td>
			<table class="table secondLevel">
				<tr style="">
					<td >
						Payment by Selfassessment?(Y/N):
					</td>
					<td colspan="1" style="width: 70px;">
						<select id="sel_sdcRppayselfass1" dir="rtl" class="form-control corpselfass">
							<option selected="selected" value="0">-Select-</option>
							<option value="1">Yes</option>
							<option value="2">No</option>
						</select>
					</td>
					<td class="hideSec" colspan="1" style="width: 70px;">
						<select id="sel_sdcRppayselfass1_2nd" dir="rtl" class="form-control corpselfass">
							<option selected="selected" value="0">-Select-</option>
							<option value="1">Yes</option>
							<option value="2">No</option>
						</select>
					</td>
					<td class="hideSec_next" colspan="1" style="width: 70px;">
						<select id="sel_sdcRppayselfass1_3rd" dir="rtl" class="form-control corpselfass">
							<option selected="selected" value="0">-Select-</option>
							<option value="1">Yes</option>
							<option value="2">No</option>
						</select>
					</td>
					<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sel_sdcRppayselfass1_total"></output>
											</td>
				</tr>
				<tr style="">
					<td >
						Have you submitIR614(Analysis)?(if recipient is a tax resident)(Y/N):
					</td>
					<td colspan="1" style="width: 70px;">
						<select id="sel_sdcRpsubmit1" dir="rtl" class="form-control corpselfass">
							<option selected="selected" value="0">-Select-</option>
							<option value="1">Yes</option>
							<option value="2">No</option>
						</select>
					</td>
					<td class="hideSec" colspan="1" style="width: 70px;">
						<select id="sel_sdcRpsubmit1_2nd" dir="rtl" class="form-control corpselfass">
							<option selected="selected" value="0">-Select-</option>
							<option value="1">Yes</option>
							<option value="2">No</option>
						</select>
					</td>
					<td class="hideSec_next" colspan="1" style="width: 70px;">
						<select id="sel_sdcRpsubmit1_3rd" dir="rtl" class="form-control corpselfass">
							<option selected="selected" value="0">-Select-</option>
							<option value="1">Yes</option>
							<option value="2">No</option>
						</select>
					</td>
					<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sel_sdcRpsubmit1_total"></output>
											</td>
				</tr>
				<!-- <tr style="display: none;">
					<td >
						End of month:
					</td>
					<td colspan="1" style="width: 70px;">
						<output id="sp_sdcRpendofmonth1" class="pull-right">-</output>
					</td>
				</tr> -->
				<tr style="display:none;">
					<td >
						SDC rate:
					</td>
					<td colspan="1" style="width: 70px; color: Red;">
						<output id="sp_sdcRpscrate1" class="pull-right">0,00%</output>
					</td>
					<td class="hideSec" colspan="1" style="width: 70px; color: Red;">
						<output id="sp_sdcRpscrate1_2nd" class="pull-right">0,00%</output>
					</td>
					<td class="hideSec_next" colspan="1" style="width: 70px; color: Red;">
						<output id="sp_sdcRpscrate1_3rd" class="pull-right">0,00%</output>
					</td>
					<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcRpscrate1_total"></output>
											</td>
				</tr>
				<tr style="">
					<td >
						Rent Amount (gross amount minus 25%):
					</td>
					<td colspan="1" style="width: 70px;">
						<input name="txt_sdcRprentamt1" type="text" id="txt_sdcRprentamt1" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
					</td>
					<td class="hideSec" colspan="1" style="width: 70px;">
						<input name="txt_sdcRprentamt1" type="text" id="txt_sdcRprentamt1_2nd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
					</td>
					<td class="hideSec_next" colspan="1" style="width: 70px;">
						<input name="txt_sdcRprentamt1" type="text" id="txt_sdcRprentamt1_3rd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
					</td>
					<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="txt_sdcRprentamt1_total"></output>
											</td>
				</tr>
				<tr style="">
					<td style=" font-weight: bold;">
						SDC:
					</td>
					<td colspan="1" style="width: 70px; font-weight: bold;">
						<output id="sp_sdcRprate1" class="pull-right">0,00</output>
					</td>
					<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
						<output id="sp_sdcRprate1_2nd" class="pull-right">0,00</output>
					</td>
					<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
						<output id="sp_sdcRprate1_3rd" class="pull-right">0,00</output>
					</td>
					<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcRprate1_total"></output>
											</td>
				</tr>
				<tr style="">
					<td >
						Monetary Charge 5%:
					</td>
					<td colspan="1" style="width: 70px;">
						<output id="sp_sdcRpmoncharge1" class="pull-right">0,00</output>
					</td>
					<td class="hideSec" colspan="1" style="width: 70px;">
						<output id="sp_sdcRpmoncharge1_2nd" class="pull-right">0,00</output>
					</td>
					<td class="hideSec_next" colspan="1" style="width: 70px;">
						<output id="sp_sdcRpmoncharge1_3rd" class="pull-right">0,00</output>
					</td>
					<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcRpmoncharge1_total"></output>
											</td>
				</tr>
				<tr style="">
					<td >
						Fixed penalty for not submitting IR614
					</td>
					<td colspan="1" style="width: 70px;">
						<output id="sp_sdcRpfixpennotsubmit1" class="pull-right">0,00</output>
					</td>
					<td class="hideSec" colspan="1" style="width: 70px;">
						<output id="sp_sdcRpfixpennotsubmit1_2nd" class="pull-right">0,00</output>
					</td>
					<td class="hideSec_next" colspan="1" style="width: 70px;">
						<output id="sp_sdcRpfixpennotsubmit1_3rd" class="pull-right">0,00</output>
					</td>
					<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcRpfixpennotsubmit1_total"></output>
											</td>
				</tr>
				<tr style="">
					<td >
						Fixed penalty for not deducting/paying SDC
					</td>
					<td colspan="1" style="width: 70px;">
						<output id="sp_sdcRpfixpennotdeduct1" class="pull-right">0,00</output>
					</td>
					<td class="hideSec" colspan="1" style="width: 70px;">
						<output id="sp_sdcRpfixpennotdeduct1_2nd" class="pull-right">0,00</output>
					</td>
					<td class="hideSec_next" colspan="1" style="width: 70px;">
						<output id="sp_sdcRpfixpennotdeduct1_3rd" class="pull-right">0,00</output>
					</td>
					<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcRpfixpennotdeduct1_total"></output>
											</td>
				</tr>
				<tr style="">
					<td >
						Interest and penalties:
					</td>
					<td colspan="1" style="width: 70px;">
						<output id="sp_sdcRpintpen1" class="pull-right">0,00</output>
					</td>
					<td class="hideSec" colspan="1" style="width: 70px;">
						<output id="sp_sdcRpintpen1_2nd" class="pull-right">0,00</output>
					</td>
					<td class="hideSec_next" colspan="1" style="width: 70px;">
						<output id="sp_sdcRpintpen1_3rd" class="pull-right">0,00</output>
					</td>
					<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcRpintpen1_total"></output>
											</td>
				</tr>
				<tr style="">
					<td style=" font-weight: bold;">
						Total SDC + interest and penalties:
					</td>
					<td colspan="1" style="width: 70px; font-weight: bold;">
						<output id="sp_scdRptotsdcintpen1" class="pull-right">0,00</output>
					</td>
					<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
						<output id="sp_scdRptotsdcintpen1_2nd" class="pull-right">0,00</output>
					</td>
					<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
						<output id="sp_scdRptotsdcintpen1_3rd" class="pull-right">0,00</output>
					</td>
					<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_scdRptotsdcintpen1_total"></output>
											</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table class="table-responsive">                                                    
				<tr>
					<td style=" font-weight: bold; color: Red;">
						INTEREST RECEIVED for the month ending(code 0612):                                                    
						<span data-toggle="collapse" data-target="#demo4" class="accordion-toggle" style="text-align: center;
						cursor:pointer;float:right;padding: 5px;" class="pull-right">
						<i id="i_sdcshowhide4" class="fa  sdcshowhide4 fa-minus">
						</i>
					</span>
				</td>
				<td colspan="1" style="width: 70px; font-weight: bold;">
					<input type="text" id="txt_sdcIRintRECenddate1" class="form-control clssdc cls4" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
				</td>
				<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
					<input type="text" id="txt_sdcIRintRECenddate1_2nd" class="form-control clssdc cls4" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
				</td>
				<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
					<input type="text" id="txt_sdcIRintRECenddate1_3rd" class="form-control clssdc cls4" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
				</td>
				<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="txt_sdcIRintRECenddate1_total"></output>
											</td>
			</tr>
		</table>
	</td>
</tr>
<tr class="accordian-body collapse panel-collapse" id="demo4">  
	<td>
		<table class="table secondLevel">
			<tr style="">
				<td >
					Payment by Selfassessment?(Y/N):
				</td>
				<td colspan="1" style="width: 70px;">
					<select id="sel_sdcIrpayselfass1" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="hideSec" colspan="1" style="width: 70px;">
					<select id="sel_sdcIrpayselfass1_2nd" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="hideSec_next" colspan="1" style="width: 70px;">
					<select id="sel_sdcIrpayselfass1_3rd" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sel_sdcIrpayselfass1_total"></output>
											</td>
			</tr>
			<!-- <tr style="display: none;">
				<td >
					End of month:
				</td>
				<td colspan="1" style="width: 70px;">
					<span id="sp_sdcIrendofmonth1" class="pull-right">-</span>
				</td>
			</tr>
			<tr style="display: none;">
				<td >
					End of following month:
				</td>
				<td colspan="1" style="width: 70px;">
					<span id="sp_sdcIrendoffollmonth1" class="pull-right">-</span>
				</td>
			</tr> -->
			<tr style="">
				<td >
					Reduced SDC rate?(pmt to provided funds?):
				</td>
				<td colspan="1" style="width: 70px;">
					<select id="sel_sdcIrredsdcratetype1" dir="rtl" style="width: 50%;float: left; height: auto;
					padding: 6px 12px;" class="sdcIpselfass">
					<option value="1">Yes</option>
					<option selected="selected" value="2">No</option>
				</select>
				<output id="sp_sdcIrredsdcrate1" style="width: 45%; text-align: right; float: right;color: Red;
				">0,00%</output>
			</td>
			      <td class="hideSec" colspan="1" style="width: 70px;">
					<select id="sel_sdcIrredsdcratetype1_2nd" dir="rtl" style="width: 50%;float: left; height: auto;
					padding: 6px 12px;" class="sdcIpselfass">
					<option value="1">Yes</option>
					<option selected="selected" value="2">No</option>
				</select>
				<output id="sp_sdcIrredsdcrate1_2nd" style="width: 45%; text-align: right;float: right; color: Red;">0,00%</output>
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px;">
					<select id="sel_sdcIrredsdcratetype1_3rd" dir="rtl" style="width: 50%;float: left; height: auto;
					padding: 6px 12px;" class="sdcIpselfass">
					<option value="1">Yes</option>
					<option selected="selected" value="2">No</option>
				</select>
				<output id="sp_sdcIrredsdcrate1_3rd" style="width: 45%; text-align: right;float: right; color: Red;">0,00%</output>
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcIrredsdcrate1_total"></output>
											</td>
		</tr>
		<tr style="">
			<td >
				Interest Amount:
			</td>
			<td colspan="1" style="width: 70px;">
				<input name="txt_sdcIrintamt1" type="text" id="txt_sdcIrintamt1" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
			</td>
			<td class="hideSec" colspan="1" style="width: 70px;">
				<input name="txt_sdcIrintamt1" type="text" id="txt_sdcIrintamt1_2nd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px;">
				<input name="txt_sdcIrintamt1" type="text" id="txt_sdcIrintamt1_3rd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="txt_sdcIrintamt1_total"></output>
											</td>
		</tr>
		<tr style="">
			<td style=" font-weight: bold;">
				SDC:
			</td>
			<td colspan="1" style="width: 70px; font-weight: bold;">
				<output id="sp_sdcIrrate1" class="pull-right">0,00</output>
			</td>
			<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
				<output id="sp_sdcIrrate1_2nd" class="pull-right">0,00</output>
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
				<output id="sp_sdcIrrate1_3rd" class="pull-right">0,00</output>
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcIrrate1_total"></output>
											</td>
		</tr>
		<tr style="">
			<td >
				Monetary Charge 5%:
			</td>
			<td colspan="1" style="width: 70px;">
				<output id="sp_sdcIrmoncharge1" class="pull-right">0,00</output>
			</td>
			<td class="hideSec" colspan="1" style="width: 70px;">
				<output id="sp_sdcIrmoncharge1_2nd" class="pull-right">0,00</output>
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px;">
				<output id="sp_sdcIrmoncharge1_3rd" class="pull-right">0,00</output>
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcIrmoncharge1_total"></output>
											</td>
		</tr>
		<tr style="">
			<td >
				Interest and penalties:
			</td>
			<td colspan="1" style="width: 70px;">
				<output id="sp_sdcIrintpen1" class="pull-right">0,00</output>
			</td>
			<td class="hideSec" colspan="1" style="width: 70px;">
				<output id="sp_sdcIrintpen1_2nd" class="pull-right">0,00</output>
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px;">
				<output id="sp_sdcIrintpen1_3rd" class="pull-right">0,00</output>
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcIrintpen1_total"></output>
											</td>
		</tr>
		<tr style="">
			<td style=" font-weight: bold;">
				Total SDC + interest and penalties:
			</td>
			<td colspan="1" style="width: 70px; font-weight: bold;">
				<output id="sp_scdIrtotsdcintpen1" class="pull-right">0,00</output>
			</td>
			<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
				<output id="sp_scdIrtotsdcintpen1_2nd" class="pull-right">0,00</output>
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
				<output id="sp_scdIrtotsdcintpen1_3rd" class="pull-right">0,00</output>
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_scdIrtotsdcintpen1_total"></output>
											</td>
		</tr>
	</table>
</td>
</tr>
<tr>
	<td>
		<table class="table-responsive">                                                  
			<tr>
				<td style=" font-weight: bold; color: Red;">
					<div style="display: table; width: 100%;">
						<div style="display: table-row; width: 100%;">
							<div style="display: table-cell; vertical-align: middle; width: 92%;">DIVIDENTS RECEIVED/SH. CAPITAL REDUCTION/<br> COMPANY DISSOLUTION for the month ending(code 0613):
							</div>
							<div style="display: table-cell; vertical-align: middle; width: 20%;">
								<span data-toggle="collapse" data-target="#demo5" class="accordion-toggle" style="text-align: center; cursor:pointer;float:right; padding: 5px;" class="pull-right">
									<i id="i_sdcshowhide5" class="fa  sdcshowhide5 fa-minus">
									</i>
								</span>
							</div>
						</div>
					</div>
				</td>
				<td colspan="1" style="width: 70px; font-weight: bold;">
					<input type="text" id="txt_sdcDrpaidenddate1" class="form-control clssdc cls5" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
				</td>
				<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
					<input type="text" id="txt_sdcDrpaidenddate1_2nd" class="form-control clssdc cls5" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
				</td>
				<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
					<input type="text" id="txt_sdcDrpaidenddate1_3rd" class="form-control clssdc cls5" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
				</td>
				<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="txt_sdcDrpaidenddate1_total"></output>
											</td>
			</tr>
		</table>
	</td>
</tr>
<tr class="accordian-body collapse panel-collapse" id="demo5">  
	<td>
		<table class="table secondLevel">
			<tr style="">
				<td >
					Payment by Selfassessment?(Y/N):
				</td>
				<td colspan="1" style="width: 70px;">
					<select id="sel_sdcDrpayselfass1" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="hideSec" colspan="1" style="width: 70px;">
					<select id="sel_sdcDrpayselfass1_2nd" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="hideSec_next" colspan="1" style="width: 70px;">
					<select id="sel_sdcDrpayselfass1_3rd" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sel_sdcDrpayselfass1_total"></output>
											</td>
			</tr>
		<!-- <tr style="display: none;">
			<td >
				End of month:
			</td>
			<td colspan="1" style="width: 70px;">
				<output id="sp_sdcDrendofmonth1" class="pull-right">-</output>
			</td>
		</tr>
		<tr style="display: none;">
			<td >
				End of following month:
			</td>
			<td colspan="1" style="width: 70px;">
				<output id="sp_sdcDrendoffollmonth1" class="pull-right">-</output>
			</td>
		</tr> -->
		<tr style="">
			<td >
				SDC rate:
			</td>
			<td colspan="1" style="width: 70px; color: Red;">
				<output id="sp_sdcDrscrate1" class="pull-right">0,00%</output>
			</td>
			<td class="hideSec" colspan="1" style="width: 70px; color: Red;">
				<output id="sp_sdcDrscrate1_2nd" class="pull-right">0,00%</output>
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px; color: Red;">
				<output id="sp_sdcDrscrate1_3rd" class="pull-right">0,00%</output>
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcDrscrate1_total"></output>
											</td>
		</tr>
		<tr style="">
			<td >
				Divident/deemed dividend Amount:
			</td>
			<td colspan="1" style="width: 70px;">
				<input name="txt_sdcDrdivamt1" type="text" id="txt_sdcDrdivamt1" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
			</td>
			<td class="hideSec" colspan="1" style="width: 70px;">
				<input name="txt_sdcDrdivamt1" type="text" id="txt_sdcDrdivamt1_2nd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px;">
				<input name="txt_sdcDrdivamt1" type="text" id="txt_sdcDrdivamt1_3rd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="txt_sdcDrdivamt1_total"></output>
											</td>
		</tr>
		<tr style="">
			<td style=" font-weight: bold;">
				SDC:
			</td>
			<td colspan="1" style="width: 70px; font-weight: bold;">
				<output id="sp_sdcDrrate1" class="pull-right">0,00</output>
			</td>
			<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
				<output id="sp_sdcDrrate1_2nd" class="pull-right">0,00</output>
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
				<output id="sp_sdcDrrate1_3rd" class="pull-right">0,00</output>
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcDrrate1_total"></output>
											</td>
		</tr>
		<tr style="">
			<td >
				Monetary Charge 5%:
			</td>
			<td colspan="1" style="width: 70px;">
				<output id="sp_sdcDrmoncharge1" class="pull-right">0,00</output>
			</td>
			<td class="hideSec" colspan="1" style="width: 70px;">
				<output id="sp_sdcDrmoncharge1_2nd" class="pull-right">0,00</output>
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px;">
				<output id="sp_sdcDrmoncharge1_3rd" class="pull-right">0,00</output>
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcDrmoncharge1_total"></output>
											</td>
		</tr>
		<tr style="">
			<td >
				Interest and penalties:
			</td>
			<td colspan="1" style="width: 70px;">
				<output id="sp_sdcDreintpen1" class="pull-right">0,00</output>
			</td>
			<td class="hideSec" colspan="1" style="width: 70px;">
				<output id="sp_sdcDreintpen1_2nd" class="pull-right">0,00</output>
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px;">
				<output id="sp_sdcDreintpen1_3rd" class="pull-right">0,00</output>
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_sdcDreintpen1_total"></output>
											</td>
		</tr>
		<tr style="">
			<td style=" font-weight: bold;">
				Total SDC + interest and penalties:
			</td>
			<td colspan="1" style="width: 70px; font-weight: bold;">
				<output id="sp_scdDrtotsdcintpen1" class="pull-right">0,00</output>
			</td>
			<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
				<output id="sp_scdDrtotsdcintpen1_2nd" class="pull-right">0,00</output>
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
				<output id="sp_scdDrtotsdcintpen1_3rd" class="pull-right">0,00</output>
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sp_scdDrtotsdcintpen1_total"></output>
											</td>
		</tr>
	</table>
</td>
</tr>
<tr>
	<td>
		<table class="table-responsive">                                                 
			<tr>
				<td style=" font-weight: bold; color: Red;">
					INTEREST RECEIVED(from abroad) for the month ending(code 0612): 
					<span data-toggle="collapse" data-target="#demo6" class="accordion-toggle" style="text-align: center;
					cursor: pointer;float:right; padding: 5px;" class="pull-right">
					<i id="i_sdcshowhide6" class="fa  sdcshowhide6 fa-minus">
					</i>
				</span>
			</td>
			<td colspan="1" style="width: 70px; font-weight: bold;">
				<input type="text" id="txt_sdcIrpaidenddate1" class="form-control clssdc cls6" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
			</td>
			<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
				<input type="text" id="txt_sdcIrpaidenddate1_2nd" class="form-control clssdc cls6" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
				<input type="text" id="txt_sdcIrpaidenddate1_3rd" class="form-control clssdc cls6" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="txt_sdcIrpaidenddate1_total"></output>
											</td>
		</tr>
	</table>
</td>
</tr>
<tr class="accordian-body collapse panel-collapse" id="demo6">  
	<td>
		<table class="table secondLevel">
			<tr style="">
				<td >
					Payment by Selfassessment?(Y/N):
				</td>
				<td colspan="1" style="width: 70px;">
					<select id="sel_sdcIrepayselfass1" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="hideSec" colspan="1" style="width: 70px;">
					<select id="sel_sdcIrepayselfass1_2nd" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="hideSec_next" colspan="1" style="width: 70px;">
					<select id="sel_sdcIrepayselfass1_3rd" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
												<output id="sel_sdcIrepayselfass1_total"></output>
											</td>
			</tr>
	<!-- <tr style="display: none;">
		<td >
			End of month:
		</td>
		<td colspan="1" style="width: 70px;">
			<output id="sp_sdcIreendofmonth1" class="pull-right">-</output>
		</td>
	</tr>
	<tr style="display: none;">
		<td >
			End of following month:
		</td>
		<td colspan="1" style="width: 70px;">
			<output id="sp_sdcIreendoffollmonth1" class="pull-right">-</output>
		</td>
	</tr> -->
	<tr style="">
		<td >
			Reduced SDC rate(Are you a providend fund?):
		</td>
		<td colspan="1" style="width: 70px;">
			<select id="sel_sdcIreredsdctypre1" dir="rtl" style="width: 50%; float: left;height: auto; padding: 6px 12px;" class="sdcIpselfass">
				<option value="1">Yes</option>
				<option selected="selected" value="2">No</option>
			</select>
			<output id="sp_sdcIreredsdcrate1" style="width: 45%; text-align: right;float: right; color: Red;">0,00%</output>
		</td>
		<td class="hideSec" colspan="1" style="width: 70px;">
			<select id="sel_sdcIreredsdctypre1_2nd" dir="rtl" style="width: 50%; float: left;height: auto; padding: 6px 12px;" class="sdcIpselfass">
				<option value="1">Yes</option>
				<option selected="selected" value="2">No</option>
			</select>
			<output id="sp_sdcIreredsdcrate1_2nd" style="width: 45%; text-align: right;float: right; color: Red;
			">0,00%</output>
		</td>
		<td class="hideSec_next" colspan="1" style="width: 70px;">
			<select id="sel_sdcIreredsdctypre1_3rd" dir="rtl" style="width: 50%; height: auto;float: left; padding: 6px 12px;" class="sdcIpselfass">
				<option value="1">Yes</option>
				<option selected="selected" value="2">No</option>
			</select>
			<output id="sp_sdcIreredsdcrate1_3rd" style="width: 45%; text-align: right;float: right; color: Red;
			">0,00%</output>
		</td>
		<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcIreredsdcrate1_total"></output>
		</td>
	</tr>
	<tr style="">
		<td >
			Interest Amount:
		</td>
		<td colspan="1" style="width: 70px;">
			<input name="txt_sdcIreIntrate1" type="text" id="txt_sdcIreIntrate1" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
		</td>
		<td class="hideSec" colspan="1" style="width: 70px;">
			<input name="txt_sdcIreIntrate1" type="text" id="txt_sdcIreIntrate1_2nd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
		</td>
		<td class="hideSec_next" colspan="1" style="width: 70px;">
			<input name="txt_sdcIreIntrate1" type="text" id="txt_sdcIreIntrate1_3rd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
		</td>
		<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
		    <output id="txt_sdcIreIntrate1_total"></output>
		</td>
	</tr>
	<tr style="">
		<td style=" font-weight: bold;">
			SDC:
		</td>
		<td colspan="1" style="width: 70px; font-weight: bold;">
			<output id="sp_sdcIrerate1" class="pull-right">0,00</output>
		</td>
		<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
			<output id="sp_sdcIrerate1_2nd" class="pull-right">0,00</output>
		</td>
		<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
			<output id="sp_sdcIrerate1_3rd" class="pull-right">0,00</output>
		</td>
		<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcIrerate1_total"></output>
		</td>
	</tr>
	<tr style="">
		<td >
			Monetary Charge 5%:
		</td>
		<td colspan="1" style="width: 70px;">
			<output id="sp_sdcIremoncharge1" class="pull-right">0,00</output>
		</td>
		<td class="hideSec" colspan="1" style="width: 70px;">
			<output id="sp_sdcIremoncharge1_2nd" class="pull-right">0,00</output>
		</td>
		<td class="hideSec_next" colspan="1" style="width: 70px;">
			<output id="sp_sdcIremoncharge1_3rd" class="pull-right">0,00</output>
		</td>
		<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcIremoncharge1_total"></output>
		</td>
	</tr>
	<tr style="">
		<td >
			Interest and penalties:
		</td>
		<td colspan="1" style="width: 105px;">
			<output id="sp_sdcIreintpen1" class="pull-right">0,00</output>
		</td>
		<td class="hideSec" colspan="1" style="width: 105px;">
			<output id="sp_sdcIreintpen1_2nd" class="pull-right">0,00</output>
		</td>
		<td class="hideSec_next" colspan="1" style="width: 105px;">
			<output id="sp_sdcIreintpen1_3rd" class="pull-right">0,00</output>
		</td>
		<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcIreintpen1_total"></output>
		</td>
	</tr>
	<tr style="">
		<td style=" font-weight: bold;">
			Total SDC + interest and penalties:
		</td>
		<td colspan="1" style="width: 70px; font-weight: bold;">
			<output id="sp_scdIretotsdcintpen1" class="pull-right">0,00</output>
		</td>
		<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
			<output id="sp_scdIretotsdcintpen1_2nd" class="pull-right">0,00</output>
		</td>
		<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
			<output id="sp_scdIretotsdcintpen1_3rd" class="pull-right">0,00</output>
		</td>
		<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_scdIretotsdcintpen1_total"></output>
		</td>
	</tr>
</table>
</td>
</tr>
<tr>
	<td>
		<table class="table-responsive">                                                  
			<tr>
				<td style=" font-weight: bold; color: Red;">
					DIVIDEND RECEIVED(from abroad) for the semester ending(code 0613):
					<span data-toggle="collapse" data-target="#demo7" class="accordion-toggle" style="text-align: center;
					cursor: pointer;float:right; padding: 5px;" class="pull-right">
					<i id="i_sdcshowhide7" class="fa  sdcshowhide7 fa-minus">
					</i>
				</span>
			</td>
			<td colspan="1" style="width: 70px; font-weight: bold;">
				<input type="text" id="txt_sdcDrsemend1" class="form-control clssdc cls7" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
			</td>
			<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
				<input type="text" id="txt_sdcDrsemend1_2nd" class="form-control clssdc cls7" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
				<input type="text" id="txt_sdcDrsemend1_3rd" class="form-control clssdc cls7" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
				<output id="txt_sdcDrsemend1_total"></output>
			</td>
		</tr>
	</table>
</td>
</tr>
<tr class="accordian-body collapse panel-collapse" id="demo7">  
	<td>
		<table class="table secondLevel">
			<tr style="">
				<td >
					Payment by Selfassessment?(Y/N):
				</td>
				<td colspan="1" style="width: 70px;">
					<select id="txt_sdcDraelfass1" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="hideSec" colspan="1" style="width: 70px;">
					<select id="txt_sdcDraelfass1_2nd" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="hideSec_next" colspan="1" style="width: 70px;">
					<select id="txt_sdcDraelfass1_3rd" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			      <output id="txt_sdcDraelfass1_total"></output>
		         </td>
			</tr>
<!-- <tr style="display: none;">
	<td >
		End of month:
	</td>
	<td colspan="1" style="width: 70px;">
		<output id="sp_sdcDreendofmonth1" class="pull-right">-</output>
	</td>
</tr>
<tr style="display: none;">
	<td >
		End of following month:
	</td>
	<td colspan="1" style="width: 70px;">
		<output id="sp_sdcDreendoffollmonth1" class="pull-right">-</output>
	</td>
</tr> -->
<tr style="">
	<td >
		SDC rate:
	</td>
	<td colspan="1" style="width: 70px; color: Red;">
		<output id="sp_sdcDrsdcrate1" class="pull-right">0,00%</output>
	</td>
	<td class="hideSec" colspan="1" style="width: 70px; color: Red;">
		<output id="sp_sdcDrsdcrate1_2nd" class="pull-right">0,00%</output>
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px; color: Red;">
		<output id="sp_sdcDrsdcrate1_3rd" class="pull-right">0,00%</output>
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcDrsdcrate1_total"></output>
		</td>
</tr>
<tr style="">
	<td >
		Dividend Amount:
	</td>
	<td colspan="1" style="width: 70px;">
		<input name="txt_sdcDredivamt1" type="text" id="txt_sdcDredivamt1" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
	</td>
	<td class="hideSec" colspan="1" style="width: 70px;">
		<input name="txt_sdcDredivamt1_2nd" type="text" id="txt_sdcDredivamt1_2nd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px;">
		<input name="txt_sdcDredivamt1_3rd" type="text" id="txt_sdcDredivamt1_3rd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="txt_sdcDredivamt1_total"></output>
		</td>
</tr>
<tr style="">
	<td style=" font-weight: bold;">
		SDC:
	</td>
	<td colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdcDrsdc1" class="pull-right">0,00</output>
	</td>
	<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdcDrsdc1_2nd" class="pull-right">0,00</output>
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdcDrsdc1_3rd" class="pull-right">0,00</output>
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcDrsdc1_total"></output>
		</td>
</tr>
<tr style="">
	<td >
		Monetary Charge 5%:
	</td>
	<td colspan="1" style="width: 70px;">
		<output id="sp_sdcDrMonchrge1" class="pull-right">0,00</output>
	</td>
	<td class="hideSec" colspan="1" style="width: 70px;">
		<output id="sp_sdcDrMonchrge1_2nd" class="pull-right">0,00</output>
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px;">
		<output id="sp_sdcDrMonchrge1_3rd" class="pull-right">0,00</output>
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcDrMonchrge1_total"></output>
		</td>
</tr>
<tr style="">
	<td >
		Interest and penalties:
	</td>
	<td colspan="1" style="width: 70px;">
		<output id="sp_sdcDrintpen1" class="pull-right">0,00</output>
	</td>
	<td class="hideSec" colspan="1" style="width: 70px;">
		<output id="sp_sdcDrintpen1_2nd" class="pull-right">0,00</output>
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px;">
		<output id="sp_sdcDrintpen1_3rd" class="pull-right">0,00</output>
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcDrintpen1_total"></output>
		</td>
</tr>
<tr style="">
	<td style=" font-weight: bold;">
		Total SDC + interest and penalties:
	</td>
	<td colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdcDrsdcintpen1" class="pull-right">0,00</output>
	</td>
	<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdcDrsdcintpen1_2nd" class="pull-right">0,00</output>
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdcDrsdcintpen1_3rd" class="pull-right">0,00</output>
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcDrsdcintpen1_total"></output>
		</td>
</tr>
</table>
</td>
</tr>
<tr>
	<td>
		<table class="table-responsive">                                                  
			<tr>
				<td style=" font-weight: bold; color: Red;">
					RENTS RECEIVED for the semester ending(code 0604): 
					<span data-toggle="collapse" data-target="#demo8" class="accordion-toggle" style="text-align: center;
					cursor: pointer;float:right; padding: 5px;" class="pull-right">
					<i id="i_sdcshowhide8" class="fa  sdcshowhide8 fa-minus">
					</i>
				</span>
			</td>
			<td colspan="1" style="width: 70px; font-weight: bold;">
				<input type="text" id="txt_sdcRrsemend1" class="form-control clssdc cls8" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
			</td>
			<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
				<input type="text" id="txt_sdcRrsemend1_2nd" class="form-control clssdc cls8" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
			</td>
			<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
				<input type="text" id="txt_sdcRrsemend1_3rd" class="form-control clssdc cls8" onkeypress="return false;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Choose a date from 2008 onwards">
			</td>
			<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="txt_sdcRrsemend1_total"></output>
		</td>
		</tr>
	</table>
</td>
</tr>
<tr class="accordian-body collapse panel-collapse" id="demo8">  
	<td>
		<table class="table secondLevel">
			<tr style="">
				<td >
					Payment by Selfassessment?(Y/N):
				</td>
				<td colspan="1" style="width: 70px;">
					<select id="sel_sdcRrselfass1" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="hideSec" colspan="1" style="width: 70px;">
					<select id="sel_sdcRrselfass1_2nd" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="hideSec_next" colspan="1" style="width: 70px;">
					<select id="sel_sdcRrselfass1_3rd" dir="rtl" class="form-control corpselfass">
						<option selected="selected" value="0">-Select-</option>
						<option value="1">Yes</option>
						<option value="2">No</option>
					</select>
				</td>
				<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sel_sdcRrselfass1_total"></output>
		</td>
			</tr>
<!-- <tr style="display: none;">
<td >
	End of month:
</td>
<td colspan="1" style="width: 70px;">
	<output id="sp_sdcRrendmonth1" class="pull-right">-</output>
</td>
</tr> -->
<tr style="">
	<td >
		SDC rate:
	</td>
	<td colspan="1" style="width: 70px; color: Red;">
		<output id="sp_sdcRrsdcrate1" class="pull-right">0,00%</output>
	</td>
	<td class="hideSec" colspan="1" style="width: 70px; color: Red;">
		<output id="sp_sdcRrsdcrate1_2nd" class="pull-right">0,00%</output>
	</td>
	<td class="hideSec_next" class="hideSec" colspan="1" style="width: 70px; color: Red;">
		<output id="sp_sdcRrsdcrate1_3rd" class="pull-right">0,00%</output>
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcRrsdcrate1_total"></output>
		</td>
</tr>
<tr style="">
	<td >
		Rent Amount(gross amount minus 25%):
	</td>
	<td colspan="1" style="width: 70px;">
		<input name="txt_sdcRrrentamt1" type="text" id="txt_sdcRrrentamt1" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
	</td>
	<td class="hideSec" colspan="1" style="width: 70px;">
		<input name="txt_sdcRrrentamt1" type="text" id="txt_sdcRrrentamt1_2nd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px;">
		<input name="txt_sdcRrrentamt1" type="text" id="txt_sdcRrrentamt1_3rd" class="form-control  Amt" autocomplete="off" data-a-sep="." data-a-dec="," style="text-align: right;">
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="txt_sdcRrrentamt1_total"></output>
		</td>
</tr>
<tr style="">
	<td style=" font-weight: bold;">
		SDC:
	</td>
	<td colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdcRrsdc1" class="pull-right">0,00</output>
	</td>
	<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdcRrsdc1_2nd" class="pull-right">0,00</output>
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdcRrsdc1_3rd" class="pull-right">0,00</output>
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcRrsdc1_total"></output>
		</td>
</tr>
<tr style="">
	<td >
		Monetary Charge 5%:
	</td>
	<td colspan="1" style="width: 70px;">
		<output id="sp_sdcRrmonchrge1" class="pull-right">0,00</output>
	</td>
	<td class="hideSec" colspan="1" style="width: 70px;">
		<output id="sp_sdcRrmonchrge1_2nd" class="pull-right">0,00</output>
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px;">
		<output id="sp_sdcRrmonchrge1_3rd" class="pull-right">0,00</output>
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcRrmonchrge1_total"></output>
		</td>
</tr>
<tr style="">
	<td >
		Interest and penalties:
	</td>
	<td colspan="1" style="width: 70px;">
		<output id="sp_sdcRrintpen1" class="pull-right">0,00</output>
	</td>
	<td class="hideSec" colspan="1" style="width: 70px;">
		<output id="sp_sdcRrintpen1_2nd" class="pull-right">0,00</output>
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px;">
		<output id="sp_sdcRrintpen1_3rd" class="pull-right">0,00</output>
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcRrintpen1_total"></output>
		</td>
</tr>
<tr style="">
	<td style=" font-weight: bold;">
		Total SDC + interest and penalties:
	</td>
	<td colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdcRrsdcintpen1" class="pull-right">0,00</output>
	</td>
	<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdcRrsdcintpen1_2nd" class="pull-right">0,00</output>
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdcRrsdcintpen1_3rd" class="pull-right">0,00</output>
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcRrsdcintpen1_total"></output>
		</td>
</tr>
<tr>
	<td>
		&nbsp;
	</td>
	<td colspan="1">&nbsp;</td>
	<td class="hideSec" colspan="1"><output>&nbsp;</output></td>
	<td class="hideSec_next" colspan="1"><output>&nbsp;</output></td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output>&nbsp;</output>
	</td>
</tr>
</table>
</td>
</tr>
<tr>
   <td>
		<table class="table secondLevel">
			<tr style="">
	<td style=" font-weight: bold;">
		Total SDC :
	</td>
	<td colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdctotsdc1" class="pull-right">0,00</output>
	</td>
	<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdctotsdc1_2nd" class="pull-right">0,00</output>
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdctotsdc1_3rd" class="pull-right">0,00</output>
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdctotsdc1_total"></output>
		</td>
	</tr>
	</table>
	</td>
</tr>
<tr>
<td>
		<table class="table secondLevel">
			<tr style="">
	<td style=" font-weight: bold;">
		TOTAL SDC + INTEREST AND PENALTIES PAYABLE:
	</td>
	<td coloutput="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdctotsdcintpen1" class="pull-right">0,00</output>
	</td>
	<td class="hideSec" colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdctotsdcintpen1_2nd" class="pull-right">0,00</output>
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px; font-weight: bold;">
		<output id="sp_sdctotsdcintpen1_3rd" class="pull-right">0,00</output>
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdctotsdcintpen1_total"></output>
		</td>
	</tr>
	</table>
	</td>
</tr>
<tr>
<td>
		<table class="table secondLevel">
			<tr style="">
	<td >
		There might be a penalty for not submitting IR614(Analysis):
	</td>
	<td colspan="1" style="width: 70px;">
		<output id="sp_sdcpen1" class="pull-right">0,00</output>
	</td>
	<td class="hideSec" colspan="1" style="width: 70px;">
		<output id="sp_sdcpen1_2nd" class="pull-right">0,00</output>
	</td>
	<td class="hideSec_next" colspan="1" style="width: 70px;">
		<output id="sp_sdcpen1_3rd" class="pull-right">0,00</output>
	</td>
	<td class="sdcTotal" colspan="1" style="width:150px;font-weight:bold;">
			<output id="sp_sdcpen1_total"></output>
		</td>
	</tr>
	</table>
	</td>
</tr>
</tbody>
</table>
</div>

</div>
</div>
</div>
</div>

</div>
</div>
</div>
<script src="js/jquery.min-3.1.1.js" type="text/javascript"></script>
<script src="js/angular.min.js" type="text/javascript"></script>
<script src="js/angular-ui.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="js/main.js" type="text/javascript"></script>

<script>
$scope.dateOptions = { // pass jQueryUI DatePicker options through
    changeYear: true,
    changeMonth: true,
    dateFormat: 'yy-mm-dd',
}
</script>

<!--<script>
//accordian show hide all
$('.closeall').click(function(){
  $('.panel-collapse.in')
    .collapse('hide');
});

$('.openall').click(function(){
  $('.panel-collapse:not(".in")')
    .collapse('show');
});
</script>-->



<script>
$(function () {

    var active = true;

    $('#collapse-init').click(function () {
	        if (active) {
            active = false;
            $('.panel-collapse').collapse('show');
            $('.panel-title').attr('data-toggle', '');
            $(this).text('Collapse');
        } 
		
		else {
            active = true;
            $('.panel-collapse').collapse('hide');
            $('.panel-title').attr('data-toggle', 'collapse');
            $(this).text('Expend');
        }
    });
    
    $('#accordion').on('show.bs.collapse', function () {
        if (active) $('#accordion .in').collapse('hide');
    });

});
</script>




<script type="text/javascript">
var collectionMonths = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", 
               "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

//var selectedMonthName = collectionMonths[$("#datepicker").datepicker('getDate').getMonth()];

$(function () {
/*$('#datetimepicker').datetimepicker();*/

    /*$('.autoDateChange').datepicker({
     format: 'M/yyyy',
    });
    $('.autoDateChange').datepicker().on(picker_event, function(e) {
        console.log("hi there");
    });
    function picker_event(){
      alert("hooo");
    }*/
    $("#paymentdate").datepicker({
      format: 'dd/M/yyyy',
      autoclose: true
    });
    $(".autoDateChange").datepicker({
      format: 'M/yyyy'
    });
    function getMonth(monthStr){
      return new Date(monthStr+'-1-01').getMonth()+1
    }
    function getDaysInMonth(m, y) { //TrueBlueAussie
        return m===2?y&3||!(y%25)&&y&15?28:29:30+(m+(m>>3)&1);
    }

    $(".autoDateChange").on("change",function (){
      var date12 = $(this).val();
      var temp = date12.split("/");
      var monthTi = getMonth(temp[0]);
      var chanYear = parseInt(temp[1]);
      var SecondMonth = monthTi+2;
      
      if(SecondMonth==13){
      	SecondMonth=1;
      	chanYear = chanYear+1;
      }else if(SecondMonth==14){
      	SecondMonth=2;
      	chanYear = chanYear+1;
      }
      var lastDate = getDaysInMonth(SecondMonth, chanYear);
      if(chanYear%4==0 && SecondMonth==2){
      	lastDate=29;
      }
      
      var showViewDate = lastDate+"/"+collectionMonths[SecondMonth-1]+"/"+chanYear;

      var kid = $(this).parent().parent().next().find(".lastDateChange").text(showViewDate);
      $(this).datepicker('hide');
       
    });
    $(".taxYear").datepicker({
    	format: 'yyyy',
      viewMode: "years",
      minViewMode: "years",
      autoclose: true
    });
    $(".taxYear").on("change",function (){
      var date12 = $(this).val();
      var da1 = new Date(date12);
      var chYears = da1.getFullYear()+1;
      var chMonths = da1.getMonth()+8;
      var lastDa = getDaysInMonth(chMonths, chYears);
      if(chYears%4==0 && chMonths==2){
        lastDa=29;
      }
      var compPaidDate = lastDa+'/'+collectionMonths[chMonths-1]+'/'+chYears;

      var kid1 = $(this).parent().parent().parent().find(".comPaidDate").val(compPaidDate);
    });
    $(".taxReturnSubmit").datepicker({
      format: 'dd/M/yyyy',
      autoclose: true
    });
    $(".dateAssessment").datepicker({
      format: 'dd/M/yyyy',
      autoclose: true
    });
    
    


});
</script>

<script>

// Products Table Angular App
angular.module('productsTableApp', [])

// create a controller for the table
.controller('ProductsController', function($scope){
  // initialize the array
  $scope.data=[[{"en":""}]];
             
	// add a column
  $scope.addColumn = function(){
    $scope.data.forEach(function($){
      $.push({"en":""})
    });
  };

  // remove the selected column
  $scope.removeColumn = function (index) {
    // remove the column specified in index
    // you must cycle all the rows and remove the item
    // row by row
    $scope.data.forEach(function (element) {
      element.splice(index, 1);
	});
  };
 
});

</script>

<script>
 $("#menu-toggle").click(function(e) {
	e.preventDefault();
	$("#sidebar").toggle();
	$("#sidebar").toggleClass("is-active");
	$("#menu-toggle").toggleClass("active");
  });


</script>

<script>
$scope.dateOptions = { // pass jQueryUI DatePicker options through
    changeYear: true,
    changeMonth: true,
    dateFormat: 'yy-mm-dd',
}
</script>

<script>
var app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "home.php"
    })
    .when("/red", {
        templateUrl : "index.htm"
    })
    .when("/green", {
        templateUrl : "green.htm"
    })
    .when("/blue", {
        templateUrl : "blue.htm"
    });
});
</script>

</body>
</html>
<script>
	function add() {
		if ($('.hideSec').css('display') == 'none')
		{
			$('.hideSec').show()
			$('#minus_id').show()
		} else if ($('.hideSec_next').css('display') == 'none')
		{
			$('.hideSec_next').show()
			$('#add_id').hide()
		}
	}
	function subtract(){
		if ($('.hideSec_next').css('display') != 'none')
		{
			/*document.getElementById('taxrate_3rd').value = '0.00%';
			document.getElementById('afterpercent_3rd').value= 0.00;
			document.getElementById('total_3rd').value = 0,0;
			document.getElementById('latepayment_3rd').value = 0,00;
			document.getElementById('delaymonths_3rd').value = 0;
			document.getElementById('grossincome_3rd').value = '';
			document.getElementById('edatepicker_3rd').value = '';
			document.getElementById('name_2nd').value = '';*/
			$('.hideSec_next').hide()
			$('#add_id').show()
		} else if ($('.hideSec').css('display') != 'none')
		{
			/*document.getElementById('taxrate_2nd').value = '0.00%';
			document.getElementById('afterpercent_2nd').value= 0.00;
			document.getElementById('total_2nd').value = 0,0;
			document.getElementById('latepayment_2nd').value = 0,00;
			document.getElementById('delaymonths_2nd').value = 0;
			document.getElementById('grossincome_2nd').value = '';
			document.getElementById('edatepicker_2nd').value = '';
			document.getElementById('name').value = '';*/

			$('.hideSec').hide()
			$('#minus_id').hide()
		}
	}
	$(".clssdc").datepicker({
      format: 'dd/M/yyyy',
      autoclose: true
    });

    /*Calculate*/

    function calculate(){

    	var sp_sdcIprate1_total = 0;
    	var sp_sdcIpmoncharge1_total = 0;
    	var sp_sdcIpintpen1_total = 0;
    	var sp_scdIptotsdcintpen1_total = 0;

    	var sp_sdcDprate1_total = 0;
    	var sp_sdcDpmoncharge1_total  = 0;
    	var sp_sdcDpintpen1_total  = 0;
    	var sp_scdDptotsdcintpen1_total  = 0;

    	var sp_sdcRprate1_total = 0;
    	var sp_sdcRpmoncharge1_total  = 0;
    	var sp_sdcRpfixpennotsubmit1_total = 0.00;
    	var sp_sdcRpfixpennotdeduct1_total = 0.00;
    	var sp_sdcRpintpen1_total  = 0;
    	var sp_scdRptotsdcintpen1_total  = 0;

    	var sp_sdcIrrate1_total  = 0;
    	var sp_sdcIrmoncharge1_total  = 0;
    	var sp_sdcIrintpen1_total  = 0;
    	var sp_scdIrtotsdcintpen1_total  = 0;

    	var sp_sdcDrrate1_total  = 0;
    	var sp_sdcDrmoncharge1_total  = 0;
    	var sp_sdcDreintpen1_total  = 0;
    	var sp_scdDrtotsdcintpen1_total  = 0;

    	/*6th*/

    	var sp_sdcIrerate1_total  = 0;
    	var sp_sdcIremoncharge1_total  = 0;
    	var sp_sdcIreintpen1_total  = 0;
    	var sp_scdIretotsdcintpen1_total  = 0;

    	var sp_sdcDrsdc1_total  = 0;
    	var sp_sdcDrMonchrge1_total  = 0;
    	var sp_sdcDrintpen1_total  = 0;
    	var sp_sdcDrsdcintpen1_total  = 0;

    	var sp_sdcRrsdc1_total  = 0;
    	var sp_sdcRrmonchrge1_total  = 0;
    	var sp_sdcRrintpen1_total  = 0;
    	var sp_sdcRrsdcintpen1_total  = 0;

    	var completeTotalSdc = 0;
    	var completeTotalSdcAndInterest = 0;
    	var completeTotal_sp_sdcpen1 = 0;

    	
    	var completeTotalSdc_total = 0;
    	var completeTotalSdcAndInterest_total = 0;
    	var completeTotal_sp_sdcpen1_total = 0;


    	var txt_sdcIpintpaidenddate1 = document.getElementById('txt_sdcIpintpaidenddate1').value;
    	var edate = document.getElementById('paymentdate').value;
    	var sel_sdcIppayselfass1 = document.getElementById('sel_sdcIppayselfass1').value;
    	var sel_sdcIpredsdcratetype1 = document.getElementById('sel_sdcIpredsdcratetype1').value;
    	var sp_sdIpcredsdcrate1 = 0;
    	var txt_sdcIpintamt1 = document.getElementById('txt_sdcIpintamt1').value;
    	var sp_sdcIprate1;
    	var sp_sdcIpmoncharge1 = 0.00;
    	var sp_sdcIpintpen1 = 0.00;
    	var sp_scdIptotsdcintpen1 = 0.00;
        
        /*2nd*/
    	var txt_sdcDppaidenddate1 = document.getElementById('txt_sdcDppaidenddate1').value;
    	var sel_sdcDppayselfass1;
    	var sp_sdcDpscrate1;
    	var txt_sdcDpdivamt1 = document.getElementById('txt_sdcDpdivamt1').value;
    	var sp_sdcDprate1;
    	var sp_sdcDpmoncharge1 = 0.00;
    	var sp_sdcDpintpen1 = 0.00;
    	var sp_scdDptotsdcintpen1 = 0.00;

    	/*3rd*/
    	var txt_sdcRppaidenddate1 = document.getElementById('txt_sdcRppaidenddate1').value;
    	var sel_sdcRppayselfass1;
    	var sel_sdcRpsubmit1;
    	var sp_sdcRpscrate1;
    	var txt_sdcRprentamt1 = document.getElementById('txt_sdcRprentamt1').value;
    	var sp_sdcRprate1;
    	var sp_sdcRpmoncharge1 = 0.00;
    	var sp_sdcRpfixpennotsubmit1;
    	var sp_sdcRpfixpennotdeduct1 = 0.00;
    	var sp_sdcRpintpen1 = 0.00;
    	var sp_scdRptotsdcintpen1 = 0.00;

    	/*4th*/
    	var txt_sdcIRintRECenddate1 = document.getElementById('txt_sdcIRintRECenddate1').value;
    	var sel_sdcIrpayselfass1;
    	var sel_sdcIrredsdcratetype1;
    	var sp_sdcIrredsdcrate1;
    	var txt_sdcIrintamt1 = document.getElementById('txt_sdcIrintamt1').value;
    	var sp_sdcIrrate1;
    	var sp_sdcIrmoncharge1 = 0.00;
    	var sp_sdcIrintpen1 = 0.00;
    	var sp_scdIrtotsdcintpen1 = 0.00;

    	/*5th*/
    	var txt_sdcDrpaidenddate1 = document.getElementById('txt_sdcDrpaidenddate1').value;
    	var sel_sdcDrpayselfass1;
    	var sp_sdcDrscrate1;
    	var txt_sdcDrdivamt1 = document.getElementById('txt_sdcDrdivamt1').value;
    	var sp_sdcDrrate1;
    	var sp_sdcDrmoncharge1 = 0.00;
    	var sp_sdcDreintpen1 = 0.00;
    	var sp_scdDrtotsdcintpen1 = 0.00;

    	/*6th*/
    	var txt_sdcIrpaidenddate1 = document.getElementById('txt_sdcIrpaidenddate1').value;
    	var sel_sdcIrepayselfass1;
    	var sel_sdcIreredsdctypre1;
    	var sp_sdcIreredsdcrate1;
    	var txt_sdcIreIntrate1 = document.getElementById('txt_sdcIreIntrate1').value;
    	var sp_sdcIrerate1;
    	var sp_sdcIremoncharge1 = 0.00;
    	var sp_sdcIreintpen1 = 0.00;
    	var sp_scdIretotsdcintpen1 = 0.00;

    	/*7th*/
    	var txt_sdcDrsemend1 = document.getElementById('txt_sdcDrsemend1').value;
    	var sp_sdcDrsdcrate1;
    	var txt_sdcDredivamt1 = document.getElementById('txt_sdcDredivamt1').value;
    	var sp_sdcDrsdc1;
    	var sp_sdcDrMonchrge1 = 0.00;
    	var sp_sdcDrintpen1 = 0.00;
    	var sp_sdcDrsdcintpen1 = 0.00;

    	/*8th*/
    	var txt_sdcRrsemend1 = document.getElementById('txt_sdcRrsemend1').value;
    	var sel_sdcRrselfass1;
    	var sp_sdcRrsdcrate1;
    	var txt_sdcRrrentamt1 = document.getElementById('txt_sdcRrrentamt1').value;
    	var sp_sdcRrsdc1;
    	var sp_sdcRrmonchrge1 = 0.00;
    	var sp_sdcRrintpen1 = 0.00;
    	var sp_sdcRrsdcintpen1 = 0.00;


   /*var select = document.getElementById('selects').value;
   var GrossIncome = document.getElementById('grossincome').value;*/
   /*level 1*/
   var months1;
   var total_firlst1;
   var total1 = 0;
   var digit1;
   var mon1;
   var mul1=1;

   var date1 = new Date(txt_sdcIpintpaidenddate1);
   var sbmisDate = new Date(edate);
   var diffYears1 = sbmisDate.getFullYear()-date1.getFullYear();
   var diffMonths1 = sbmisDate.getMonth()-date1.getMonth();
   var diffDays1 = sbmisDate.getDate()-date1.getDate();
   months1 = (diffYears1*12 + diffMonths1);

    if(diffDays1>0) {
   	   months1 += '.'+diffDays1;
   	}
   	else if(diffDays1<0){
   		months1--;
   		months1 += '.'+ (new Date(sbmisDate.getFullYear(),sbmisDate.getMonth(),0).getDate()+diffDays1);
   	}
   	mon1 =parseInt(months1)-2;

   	if(mon1>0){
   		digit1 = mon1;
   		mul1 = mon1;
   	}else{
   		digit1=0;
   	}

   	//document.getElementById('delaymonths').value = digit;
   	if(sel_sdcIpredsdcratetype1=='1')
   	{
   		
   		total1 = txt_sdcIpintamt1 * 0.03;
   		sp_sdcIpintpen1  = txt_sdcIpintamt1*0.00029*mul1;
   		sp_sdcIpmoncharge1  = total1*0.05;

   		//console.log(total1+" : "+sp_sdcIpintpen1+" : "+sp_sdcIpmoncharge1);
   		if(digit1==0)
   		{
   			sp_sdcIpintpen1=0;
   			sp_sdcIpmoncharge1=0;
   		}

   		total_firlst1 = total1 +sp_sdcIpmoncharge1+ sp_sdcIpintpen1;
   		document.getElementById('sp_sdIpcredsdcrate1').value = '3.00%';
   		document.getElementById('sp_sdcIprate1').value= total1.toFixed(2);
   		document.getElementById('sp_sdcIpmoncharge1').value = sp_sdcIpmoncharge1.toFixed(2);
   		document.getElementById('sp_sdcIpintpen1').value = sp_sdcIpintpen1.toFixed(2);
   		document.getElementById('sp_scdIptotsdcintpen1').value = total_firlst1.toFixed(2);
   	}
   	else
   	{
   		total1 = txt_sdcIpintamt1 * 0.1;
   		sp_sdcIpintpen1  = txt_sdcIpintamt1*0.00029*mul1;
   		sp_sdcIpmoncharge1  = total1*0.05;
   	    if(digit1==0){
   	    	sp_sdcIpintpen1=0;
   	    	sp_sdcIpmoncharge1=0;
   	    }
   	    total_firlst1 = total1 +sp_sdcIpmoncharge1+ sp_sdcIpintpen1;
   	    document.getElementById('sp_sdIpcredsdcrate1').value = '3.00%';
   	    document.getElementById('sp_sdcIprate1').value= total1.toFixed(2);
   	    document.getElementById('sp_sdcIpmoncharge1').value = sp_sdcIpmoncharge1.toFixed(2);
   	    document.getElementById('sp_sdcIpintpen1').value = sp_sdcIpintpen1.toFixed(2);
   	    document.getElementById('sp_scdIptotsdcintpen1').value = total_firlst1.toFixed(2);

   	    // var latepayment  = GrossIncome*0.0003*mul;
   
   	}

   	/*level 2*/   
   var months2;
   var total_firlst2;
   var total2 = 0;
   var digit2;
   var mon2;
   var mul2=1;

   var date2 = new Date(txt_sdcDppaidenddate1);
   var diffYears2 = sbmisDate.getFullYear()-date2.getFullYear();
   var diffMonths2 = sbmisDate.getMonth()-date2.getMonth();
   var diffDays2 = sbmisDate.getDate()-date2.getDate();
   months2 = (diffYears2*12 + diffMonths2);

   if(diffDays2>0) {
   	   months2 += '.'+diffDays2;
   	}
   	else if(diffDays2<0){
   		months2--;
   		months2 += '.'+ (new Date(sbmisDate.getFullYear(),sbmisDate.getMonth(),0).getDate()+diffDays2);
   	}
   	mon2 =parseInt(months2)-2;

   	if(mon2>0){
   		digit2 = mon2;
   		mul2 = mon2;
   	}else{
   		digit2=0;
   	}

   	if(txt_sdcDpdivamt1 != '')
   	{
   		
   		total2 = txt_sdcDpdivamt1 * 0.17;
   		sp_sdcDpintpen1  = txt_sdcDpdivamt1*0.00029*mul2;
   		sp_sdcDpmoncharge1  = total2*0.05;

   		//console.log(total2+" : "+sp_sdcDpintpen1+" : "+sp_sdcDpmoncharge1);
   		if(digit2==0)
   		{
   			sp_sdcDpintpen1=0;
   			sp_sdcDpmoncharge1=0;
   		}

   		total_firlst2 = total2 +sp_sdcDpmoncharge1+ sp_sdcDpintpen1;
   		document.getElementById('sp_sdcDpscrate1').value = '17.00%';
   		document.getElementById('sp_sdcDprate1').value= total2.toFixed(2);
   		document.getElementById('sp_sdcDpmoncharge1').value = sp_sdcDpmoncharge1.toFixed(2);
   		document.getElementById('sp_sdcDpintpen1').value = sp_sdcDpintpen1.toFixed(2);
   		document.getElementById('sp_scdDptotsdcintpen1').value = total_firlst2.toFixed(2);
   	}

   	/*level 3*/   
   var months3;
   var total_firlst3;
   var total3 = 0;
   var digit3;
   var mon3;
   var mul3=1;
   var sp_sdcRpfixpennotsubmit1 =0.00;
   var sp_sdcRpfixpennotdeduct1 =0.00;

   var date3 = new Date(txt_sdcRppaidenddate1);
   var diffYears3 = sbmisDate.getFullYear()-date3.getFullYear();
   var diffMonths3 = sbmisDate.getMonth()-date3.getMonth();
   var diffDays3 = sbmisDate.getDate()-date3.getDate();
   months3 = (diffYears3*12 + diffMonths3);

   if(diffDays3>0) {
   	   months3 += '.'+diffDays3;
   	}
   	else if(diffDays3<0){
   		months3--;
   		months3 += '.'+ (new Date(sbmisDate.getFullYear(),sbmisDate.getMonth(),0).getDate()+diffDays3);
   	}
   	mon3 =parseInt(months3)-2;

   	if(mon3>0){
   		digit3 = mon3;
   		mul3 = mon3;
   	}else{
   		digit3=0;
   	}

   	if(txt_sdcRprentamt1 != '')
   	{
   		
   		total3 = txt_sdcRprentamt1*0.0;
   		sp_sdcRpintpen1  = txt_sdcRprentamt1*0.00029*mul3;
   		sp_sdcRpmoncharge1  = total3*0.05;

   		//console.log(total3+" : "+sp_sdcRpintpen1+" : "+sp_sdcRpmoncharge1);
   		if(digit3==0)
   		{
   			sp_sdcRpintpen1=0;
   			sp_sdcRpmoncharge1=0;
   		}

   		total_firlst3 = total3 +sp_sdcRpmoncharge1+ sp_sdcRpintpen1;
   		/*document.getElementById('sp_sdcRpscrate1').value = '3.00%';*/
   		document.getElementById('sp_sdcRprate1').value= total3.toFixed(2);
   		document.getElementById('sp_sdcRpmoncharge1').value = sp_sdcRpmoncharge1.toFixed(2);
   		document.getElementById('sp_sdcRpfixpennotsubmit1').value = sp_sdcRpfixpennotsubmit1.toFixed(2);
   		document.getElementById('sp_sdcRpfixpennotdeduct1').value = sp_sdcRpfixpennotdeduct1.toFixed(2);
   		document.getElementById('sp_sdcRpintpen1').value = sp_sdcRpintpen1.toFixed(2);
   		document.getElementById('sp_scdRptotsdcintpen1').value = total_firlst3.toFixed(2);
   	}

   	/*level 4*/ 

   	var months4;
   var total_firlst4;
   var total4 = 0;
   var digit4;
   var mon4;
   var mul4=1;

   var date4 = new Date(txt_sdcIRintRECenddate1);
   var diffYears4 = sbmisDate.getFullYear()-date4.getFullYear();
   var diffMonths4 = sbmisDate.getMonth()-date4.getMonth();
   var diffDays4 = sbmisDate.getDate()-date4.getDate();
   months4 = (diffYears4*12 + diffMonths4);

   if(diffDays4>0) {
   	   months4 += '.'+diffDays4;
   	}
   	else if(diffDays4<0){
   		months4--;
   		months4 += '.'+ (new Date(sbmisDate.getFullYear(),sbmisDate.getMonth(),0).getDate()+diffDays4);
   	}
   	mon4 =parseInt(months4)-2;

   	if(mon4>0){
   		digit4 = mon4;
   		mul4 = mon4;
   	}else{
   		digit4=0;
   	}

   	if(txt_sdcIrintamt1 != '')
   	{
   		
   		total4 = txt_sdcIrintamt1*0.03;
   		sp_sdcIrintpen1  = txt_sdcIrintamt1*0.00029*mul4;
   		sp_sdcIrmoncharge1  = total4*0.05;

   		//console.log(total4+" : "+sp_sdcIrintpen1+" : "+sp_sdcIrmoncharge1);
   		if(digit4==0)
   		{
   			sp_sdcIrintpen1=0;
   			sp_sdcIrmoncharge1=0;
   		}

   		total_firlst4 = total4 +sp_sdcRpmoncharge1+ sp_sdcIrintpen1;
   		document.getElementById('sp_sdcIrredsdcrate1').value = '3.00%';
   		document.getElementById('sp_sdcIrrate1').value= total4.toFixed(2);
   		document.getElementById('sp_sdcIrmoncharge1').value = sp_sdcIrmoncharge1.toFixed(2);
   		document.getElementById('sp_sdcIrintpen1').value = sp_sdcIrintpen1.toFixed(2);
   		document.getElementById('sp_scdIrtotsdcintpen1').value = total_firlst4.toFixed(2);
   	}

   		/*level 5*/ 

   	var months5;
   var total_firlst5;
   var total5 = 0;
   var digit5;
   var mon5;
   var mul5=1;

   var date5 = new Date(txt_sdcDrpaidenddate1);
   var diffYears5 = sbmisDate.getFullYear()-date5.getFullYear();
   var diffMonths5 = sbmisDate.getMonth()-date5.getMonth();
   var diffDays5 = sbmisDate.getDate()-date5.getDate();
   months5 = (diffYears5*12 + diffMonths5);

   if(diffDays5>0) {
   	   months5 += '.'+diffDays5;
   	}
   	else if(diffDays5<0){
   		months5--;
   		months5 += '.'+ (new Date(sbmisDate.getFullYear(),sbmisDate.getMonth(),0).getDate()+diffDays5);
   	}
   	mon5 =parseInt(months5)-2;

   	if(mon5>0){
   		digit5 = mon5;
   		mul5 = mon5;
   	}else{
   		digit5=0;
   	}

   	if(txt_sdcDrdivamt1 != '')
   	{
   		
   		total5 = txt_sdcDrdivamt1*0.17;
   		sp_sdcDreintpen1  = txt_sdcDrdivamt1*0.00029*mul5;
   		sp_sdcDrmoncharge1  = total5*0.05;

   		//console.log(total5+" : "+sp_sdcDreintpen1+" : "+sp_sdcDrmoncharge1);
   		if(digit5==0)
   		{
   			sp_sdcDreintpen1=0;
   			sp_sdcDrmoncharge1=0;
   		}

   		total_firlst5 = total5 +sp_sdcDrmoncharge1+ sp_sdcDreintpen1;
   		document.getElementById('sp_sdcDrscrate1').value = '17.00%';
   		document.getElementById('sp_sdcDrrate1').value= total5.toFixed(2);
   		document.getElementById('sp_sdcDrmoncharge1').value = sp_sdcDrmoncharge1.toFixed(2);
   		document.getElementById('sp_sdcDreintpen1').value = sp_sdcDreintpen1.toFixed(2);
   		document.getElementById('sp_scdDrtotsdcintpen1').value = total_firlst5.toFixed(2);
   	}

   			/*level 6*/ 

   	var months6;
   var total_firlst6;
   var total6 = 0;
   var digit6;
   var mon6;
   var mul6=1;

   var date6 = new Date(txt_sdcIrpaidenddate1);
   var diffYears6 = sbmisDate.getFullYear()-date6.getFullYear();
   var diffMonths6 = sbmisDate.getMonth()-date6.getMonth();
   var diffDays6 = sbmisDate.getDate()-date6.getDate();
   months6 = (diffYears6*12 + diffMonths6);
   console.log('Anish@'+months6);
   console.log('nam@'+diffDays6);

   if(diffDays6>0) {
   	   months6 += '.'+diffDays6;
   	}
   	else if(diffDays6<0){
   		months6--;
   		months6 += '.'+ (new Date(sbmisDate.getFullYear(),sbmisDate.getMonth(),0).getDate()+diffDays6);
   	}
   	mon6 =parseInt(months6)-2;
   	console.log('Anish@'+mon6);

   	if(mon6>0){
   		digit6 = mon6;
   		mul6 = mon6;
   	}else{
   		digit6=0;
   	}

   	if(txt_sdcIreIntrate1 != '')
   	{
   		
   		total6 = txt_sdcIreIntrate1*0.03;
   		sp_sdcIreintpen1  = txt_sdcIreIntrate1*0.00029*mul6;
   		sp_sdcIremoncharge1  = total6*0.05;

   		//console.log(total6+" : "+sp_sdcIreintpen1+" : "+sp_sdcIremoncharge1);
   		if(digit6==0)
   		{
   			sp_sdcIreintpen1=0;
   			sp_sdcIremoncharge1=0;
   		}

   		total_firlst6 = total6 +sp_sdcIremoncharge1+ sp_sdcIreintpen1;
   		document.getElementById('sp_sdcIreredsdcrate1').value = '3.00%';
   		document.getElementById('sp_sdcIrerate1').value= total6.toFixed(2);
   		document.getElementById('sp_sdcIremoncharge1').value = sp_sdcIremoncharge1.toFixed(2);
   		document.getElementById('sp_sdcIreintpen1').value = sp_sdcIreintpen1.toFixed(2);
   		document.getElementById('sp_scdIretotsdcintpen1').value = total_firlst6.toFixed(2);
   	}

   			/*level 7*/ 

   	var months7;
   var total_firlst7;
   var total7 = 0;
   var digit7;
   var mon7;
   var mul7=1;

   var date7 = new Date(txt_sdcDrsemend1);
   var diffYears7 = sbmisDate.getFullYear()-date7.getFullYear();
   var diffMonths7 = sbmisDate.getMonth()-date7.getMonth();
   var diffDays7 = sbmisDate.getDate()-date7.getDate();
   months7 = (diffYears7*12 + diffMonths7);
   console.log(date7);
   console.log('Anish-'+months7);
   console.log('nam-'+diffDays7);

   if(diffDays7>0) {
   	   months7 += '.'+diffDays7;
   	}
   	else if(diffDays7<0){
   		months7--;
   		months7 += '.'+ (new Date(sbmisDate.getFullYear(),sbmisDate.getMonth(),0).getDate()+diffDays7);
   	}
   	mon7 =parseInt(months7)-2;
   	console.log('Anish-'+months7);
   	

   	if(mon7>0){
   		digit7 = mon7;
   		mul7 = mon7;
   	}else{
   		digit7=0;
   	}

   	if(txt_sdcDredivamt1 != '')
   	{
   		
   		
   		total7 = txt_sdcDredivamt1*0.17;
   		sp_sdcDrintpen1  = txt_sdcDredivamt1*0.00029*mul7;
   		sp_sdcDrMonchrge1  = total7*0.05;

   		//console.log(total7+" : "+sp_sdcDrintpen1+" : "+sp_sdcDrMonchrge1);
   		if(digit7==0)
   		{
   			sp_sdcIreintpen1=0;
   			sp_sdcDrMonchrge1=0;
   		}

   		total_firlst7 = total7 +sp_sdcDrMonchrge1+ sp_sdcDrintpen1;
   		document.getElementById('sp_sdcDrsdcrate1').value = '17.00%';
   		document.getElementById('sp_sdcDrsdc1').value= total7.toFixed(2);
   		document.getElementById('sp_sdcDrMonchrge1').value = sp_sdcDrMonchrge1.toFixed(2);
   		document.getElementById('sp_sdcDrintpen1').value = sp_sdcDrintpen1.toFixed(2);
   		document.getElementById('sp_sdcDrsdcintpen1').value = total_firlst7.toFixed(2);
   	}

   	/*level 8*/ 

   	var months8;
   var total_firlst8;
   var total8 = 0;
   var digit8;
   var mon8;
   var mul8=1;

   var date8 = new Date(txt_sdcRrsemend1);
   var diffYears8 = sbmisDate.getFullYear()-date8.getFullYear();
   var diffMonths8 = sbmisDate.getMonth()-date8.getMonth();
   var diffDays8 = sbmisDate.getDate()-date8.getDate();
   months8 = (diffYears8*12 + diffMonths8);

   if(diffDays8>0) {
   	   months8 += '.'+diffDays8;
   	}
   	else if(diffDays8<0){
   		months8--;
   		months8 += '.'+ (new Date(sbmisDate.getFullYear(),sbmisDate.getMonth(),0).getDate()+diffDays8);
   	}
   	mon8 =parseInt(months8)-2;

   	if(mon8>0){
   		digit8 = mon8;
   		mul8 = mon8;
   	}else{
   		digit8=0;
   	}

   	if(txt_sdcRrrentamt1 != '')
   	{
   		
   		total8 = txt_sdcRrrentamt1*0.0;
   		sp_sdcRrintpen1  = txt_sdcRrrentamt1*0.00029*mul8;
   		sp_sdcRrmonchrge1  = total8*0.05;

   		//console.log(total8+" : "+sp_sdcRrintpen1+" : "+sp_sdcRrmonchrge1);
   		if(digit8==0)
   		{
   			sp_sdcRrintpen1=0;
   			sp_sdcRrmonchrge1=0;
   		}

   		total_firlst8 = total8 +sp_sdcDrMonchrge1+ sp_sdcRrintpen1;
   		document.getElementById('sp_sdcRrsdcrate1').value = '0.00%';
   		document.getElementById('sp_sdcRrsdc1').value= total8.toFixed(2);
   		document.getElementById('sp_sdcRrmonchrge1').value = sp_sdcRrmonchrge1.toFixed(2);
   		document.getElementById('sp_sdcRrintpen1').value = sp_sdcRrintpen1.toFixed(2);
   		document.getElementById('sp_sdcRrsdcintpen1').value = total_firlst8.toFixed(2);
   	}


   	

   	if(edate =='')
   	{
   		alert("Please select submission date.");
   		document.getElementById('paymentdate').focus();		
    }else if(txt_sdcIpintpaidenddate1 =='')
   	{
   		alert("Please select month ending submission date.");
   		document.getElementById('txt_sdcIpintpaidenddate1').focus();   			
    }else if(txt_sdcIpintamt1 =='')
   	{
   		alert("Please insert gross amount of income paid.");
   		document.getElementById('txt_sdcIpintamt1').focus();   			
    }else{
    	if($(".hideSec").is(":visible") && !$(".hideSec_next").is(":visible")){
    		if(txt_sdcIpintpaidenddate1_2nd ==''){
    			alert("Please select 2nd month ending submission date.");
    			document.getElementById('edatepicker_2nd').focus();
    		}else if(GrossIncome_2nd ==''){
    			alert("Please insert 2nd month gross amount of income paid.");
    			document.getElementById('grossincome_2nd').focus();
    		}else{
    			submitTotal();
    		}
    	}else if($(".hideSec").is(":visible") && $(".hideSec_next").is(":visible") ){
    		if(sdate_2nd ==''){
    			alert("Please select 2nd month ending submission date.");
    			document.getElementById('edatepicker_2nd').focus();
    		}else if(GrossIncome_2nd ==''){
    			alert("Please insert 2nd month gross amount of income paid.");
    			document.getElementById('grossincome_2nd').focus();
    		}else if(sdate_3rd ==''){
    			alert("Please select 3rd month ending submission date.");
    			document.getElementById('edatepicker_3rd').focus();
    		}else if(GrossIncome_3rd ==''){
    			alert("Please insert 3rd month gross amount of income paid.");
    			document.getElementById('grossincome_3rd').focus();
    		}else{
    			submitTotal();
    		}
    	}else if(!$(".hideSec").is(":visible") && !$(".hideSec_next").is(":visible")){
    		submitTotal();
    	}
    }
    function submitTotal(){
    	$('.sdcTotal').show();

    	sp_sdcIprate1_total += total1;
    	sp_sdcIpmoncharge1_total += sp_sdcIpmoncharge1;
    	sp_sdcIpintpen1_total += sp_sdcIpintpen1;
    	sp_scdIptotsdcintpen1_total += total_firlst1;

    	sp_sdcDprate1_total += total2;
    	sp_sdcDpmoncharge1_total += sp_sdcDpmoncharge1;
    	sp_sdcDpintpen1_total += sp_sdcDpintpen1;
    	sp_scdDptotsdcintpen1_total += total_firlst2;

    	sp_sdcRprate1_total += total3;
    	sp_sdcRpmoncharge1_total += sp_sdcRpmoncharge1;
    	sp_sdcRpfixpennotsubmit1_total +=sp_sdcRpfixpennotsubmit1;
    	sp_sdcRpfixpennotdeduct1_total +=sp_sdcRpfixpennotdeduct1;
    	sp_sdcRpintpen1_total += sp_sdcRpintpen1;
    	sp_scdRptotsdcintpen1_total += total_firlst3;

    	/*4th*/

    	sp_sdcIrrate1_total += total4;
    	sp_sdcIrmoncharge1_total += sp_sdcIrmoncharge1;
    	sp_sdcIrintpen1_total += sp_sdcIrintpen1;
    	sp_scdIrtotsdcintpen1_total += total_firlst4;

    	/*5th*/

    	sp_sdcDrrate1_total += total5;
    	sp_sdcDrmoncharge1_total += sp_sdcDrmoncharge1;
    	sp_sdcDreintpen1_total += sp_sdcDreintpen1;
    	sp_scdDrtotsdcintpen1_total += total_firlst5;

    	/*6th*/

    	sp_sdcIrerate1_total += total6;
    	sp_sdcIremoncharge1_total += sp_sdcIremoncharge1;
    	sp_sdcIreintpen1_total += sp_sdcIreintpen1;
    	sp_scdIretotsdcintpen1_total += total_firlst6;

    	sp_sdcDrsdc1_total += total7;
    	sp_sdcDrMonchrge1_total += sp_sdcDrMonchrge1;
    	sp_sdcDrintpen1_total += sp_sdcDrintpen1;
    	sp_sdcDrsdcintpen1_total += total_firlst7;

    	sp_sdcRrsdc1_total += total8;
    	sp_sdcRrmonchrge1_total += sp_sdcRrmonchrge1;
    	sp_sdcRrintpen1_total += sp_sdcRrintpen1;
    	sp_sdcRrsdcintpen1_total += total_firlst8;


    	completeTotalSdc = total1+total2+total3+total4+total5+total6+total7+total8;
    	completeTotalSdcAndInterest = total_firlst1+total_firlst2+total_firlst3+total_firlst4+total_firlst5+total_firlst6+total_firlst7+total_firlst8;

    	completeTotalSdc_total += completeTotalSdc;
    	completeTotalSdcAndInterest_total += completeTotalSdcAndInterest;



    	document.getElementById('sp_sdcIprate1_total').value= sp_sdcIprate1_total.toFixed(2);
   		document.getElementById('sp_sdcIpmoncharge1_total').value= sp_sdcIpmoncharge1_total.toFixed(2);
   		document.getElementById('sp_sdcIpintpen1_total').value= sp_sdcIpintpen1_total.toFixed(2);
   		document.getElementById('sp_scdIptotsdcintpen1_total').value= sp_scdIptotsdcintpen1_total.toFixed(2);

   		document.getElementById('sp_sdcDprate1_total').value= sp_sdcDprate1_total.toFixed(2);
   		document.getElementById('sp_sdcDpmoncharge1_total').value= sp_sdcDpmoncharge1_total.toFixed(2);
   		document.getElementById('sp_sdcDpintpen1_total').value= sp_sdcDpintpen1_total.toFixed(2);
   		document.getElementById('sp_scdDptotsdcintpen1_total').value= sp_scdDptotsdcintpen1_total.toFixed(2);

   		document.getElementById('sp_sdcRprate1_total').value= sp_sdcRprate1_total.toFixed(2);
   		document.getElementById('sp_sdcRpmoncharge1_total').value= sp_sdcRpmoncharge1_total.toFixed(2);
   		document.getElementById('sp_sdcRpfixpennotsubmit1_total').value= sp_sdcRpfixpennotsubmit1_total.toFixed(2);
   		document.getElementById('sp_sdcRpfixpennotdeduct1_total').value= sp_sdcRpfixpennotdeduct1_total.toFixed(2);
   		document.getElementById('sp_sdcRpintpen1_total').value= sp_sdcRpintpen1_total.toFixed(2);
   		document.getElementById('sp_scdRptotsdcintpen1_total').value= sp_scdRptotsdcintpen1_total.toFixed(2);


   		document.getElementById('sp_sdcIrrate1_total').value= sp_sdcIrrate1_total.toFixed(2);
   		document.getElementById('sp_sdcIrmoncharge1_total').value= sp_sdcIrmoncharge1_total.toFixed(2);
   		document.getElementById('sp_sdcIrintpen1_total').value= sp_sdcIrintpen1_total.toFixed(2);
   		document.getElementById('sp_scdIrtotsdcintpen1_total').value= sp_scdIrtotsdcintpen1_total.toFixed(2);

   		document.getElementById('sp_sdcDrrate1_total').value= sp_sdcDrrate1_total.toFixed(2);
   		document.getElementById('sp_sdcDrmoncharge1_total').value= sp_sdcDrmoncharge1_total.toFixed(2);
   		document.getElementById('sp_sdcDreintpen1_total').value= sp_sdcDreintpen1_total.toFixed(2);
   		document.getElementById('sp_scdDrtotsdcintpen1_total').value= sp_scdDrtotsdcintpen1_total.toFixed(2);

   		document.getElementById('sp_sdcIrerate1_total').value= sp_sdcIrerate1_total.toFixed(2);
   		document.getElementById('sp_sdcIremoncharge1_total').value= sp_sdcIremoncharge1_total.toFixed(2);
   		document.getElementById('sp_sdcIreintpen1_total').value= sp_sdcIreintpen1_total.toFixed(2);
   		document.getElementById('sp_scdIretotsdcintpen1_total').value= sp_scdIretotsdcintpen1_total.toFixed(2);

   		document.getElementById('sp_sdcDrsdc1_total').value= sp_sdcDrsdc1_total.toFixed(2);
   		document.getElementById('sp_sdcDrMonchrge1_total').value= sp_sdcDrMonchrge1_total.toFixed(2);
   		document.getElementById('sp_sdcDrintpen1_total').value= sp_sdcDrintpen1_total.toFixed(2);
   		document.getElementById('sp_sdcDrsdcintpen1_total').value= sp_sdcDrsdcintpen1_total.toFixed(2);

   		document.getElementById('sp_sdcRrsdc1_total').value= sp_sdcRrsdc1_total.toFixed(2);
   		document.getElementById('sp_sdcRrmonchrge1_total').value= sp_sdcRrmonchrge1_total.toFixed(2);
   		document.getElementById('sp_sdcRrintpen1_total').value= sp_sdcRrintpen1_total.toFixed(2);
   		document.getElementById('sp_sdcRrsdcintpen1_total').value= sp_sdcRrsdcintpen1_total.toFixed(2);

   		document.getElementById('sp_sdctotsdc1').value= completeTotalSdc.toFixed(2);
   		document.getElementById('sp_sdctotsdcintpen1').value= completeTotalSdcAndInterest.toFixed(2);
   		document.getElementById('sp_sdcpen1').value= completeTotal_sp_sdcpen1.toFixed(2);

   		document.getElementById('sp_sdctotsdc1_total').value= completeTotalSdc_total.toFixed(2);
   		document.getElementById('sp_sdctotsdcintpen1_total').value= completeTotalSdcAndInterest_total.toFixed(2);
   		document.getElementById('sp_sdcpen1_total').value= completeTotal_sp_sdcpen1_total.toFixed(2);

   		

   		/*total_all += total_firlst;
   		total_taxwithheld_all += total;
   		total_monetry_charage_all += monetCharge;
   		total_interestLate_all += latepayment;
   		document.getElementById('total_Calculate_afterpercent').value= total_taxwithheld_all;
   		document.getElementById('total_Calculate_latepayment').value= total_interestLate_all.toFixed(2);
   		document.getElementById('total_monetaryCharge').value= total_monetry_charage_all;
   		document.getElementById('total_Calculate_total').value= total_all.toFixed(2);

   		document.getElementById('sp_sdIpcredsdcrate1').value = '3.00%';
   		document.getElementById('sp_sdcIprate1').value= total;
   		document.getElementById('sp_sdcIpmoncharge1').value = monetCharge;
   		document.getElementById('sp_sdcIpintpen1').value = latepayment.toFixed(2);
   		document.getElementById('sp_scdIptotsdcintpen1').value = total + monetCharge+latepayment;*/

    }
}
</script>
<style type="text/css">
output{
	width: 198px;
	text-align: right;
}
input{
	text-align: right;
}
.hideSec,.hideSec_next,.sdcTotal{
	display: none;
}
	.hiddingDivShow td{
		max-width: 220px;
	}
	.totalClass{
		padding-bottom: 6px;
		padding-top: 14px;
	}
	.autoDateChange, .grossCss{
		text-align: right;
	}
	.outputLeft{
		padding-left: 10px;
	}
	.outputRight,.lastDateChange{
		padding-right: 7px;
	}
	.tables-group>table>tbody>tr>td>table>tbody>tr>td:nth-child(1) {
		width: 375px;
	}
	table>tbody>tr>td>table>tbody>tr>td>table>tbody>tr>td:nth-child(1){
		width: 100%;
	}
</style>
