
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>calculator</title>
<link href="css/style.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker.min.css" rel="stylesheet">
<link href="css/angular-ui.min.css" rel="stylesheet">
<!--<link href="css/angular.css" rel="stylesheet">-->
<style>
.space > tr
{

padding:5px;

}
.sp
{

padding-right:50px;
padding-left:50px;


}

</style>
</head>

<body>

<header class="headerbar">

<div class="col-lg-2 col-md-2 col-sm-6 col-xs-7 hidden-lg hidden-md hidden-sm">
<div class="toggle" id="menu-toggle">
<span class="line line1"></span>
<span class="line line2"></span>
<span class="line line3"></span>
</div>
</div>

<div class="col-lg-3 col-sm-6 col-md-3 col-xs-5">
<div class="logo"><a href="#"><img src="img/logo.png" alt=""></a></div>
</div>

</header>

<div class="container-fluid">
<div class="row">

<div class="main">
<!--<div class="col-md-2 col-xs-12">
<div class="blur">

<aside id="sidebar">
<div id="sidebar-wrapper" class="field input">
 <input type="text" value="" name="" placeholder="company name"  autofocus>
 <input type="text" value="" name="" placeholder="address">
 <input type="text" value="" name="" placeholder="tic">
 <input type="text" value="" name="" placeholder="telephone">
 <select>
 <option>option</option>
 <option>option</option>
 </select>
 <div class="date input-group" data-provide="datepicker">
  <input type="text" class="input-group-addon" id="paymentdate" placeholder="Mandatory">
  </div>
  
 <select>
 <option selected>option</option>
 <option selected>option</option>
 </select>
 <input type="text" value="" name="" placeholder="cheque No">
 <select>
 <option selected>select option</option>
 <option selected>option</option>
 <option selected>option</option>
 <option selected>option</option>
 </select>
 
 <input type="text" value="" name="" placeholder="name">
 
 <div class="btn-area">
 <button type="button" value="">clear</button>
 <button type="button" value="">submit</button>
 </div>
 
</div>

</aside>
</div>
</div> -->
<script>
    function add_paye() {
        if ($('#hide_paye').css('display') == 'none')
        {
            $('#hide_paye').show()
            $('#minus').show()
        } else if ($('#hide_paye_next').css('display') == 'none')
        {
            $('#hide_paye_next').show()
            $('#plus').hide()
        }
    }
    function subtract_paye()
    {
        if ($('#hide_paye_next').css('display') != 'none')
        {
            $('#hide_paye_next').hide();
            $('#plus').show();
            document.getElementById('taxform_3rd').value = '';
            document.getElementById('ty_3rd').value = 0;
            document.getElementById('referencemonths_3rd').value = 0;
            document.getElementById('pdbm_3rd').value = 0;
            document.getElementById('pay_3rd').value = '';
            document.getElementById('p1_3rd').value = 0;
            document.getElementById('ip_3rd').value = 0;
            document.getElementById('ppa1_3rd').value = 0;
            document.getElementById('TaxShdPaid_3rd').value = '';
            document.getElementById('tp1_3rd').value = 0;
            document.getElementById('pay1_3rd').value = '';
            document.getElementById('ppa_3rd').value = 0;
            document.getElementById('tp_3rd').value = 0;
            document.getElementById('p2_3rd').value = 0;
            document.getElementById('ip1_3rd').value = 0;
            document.getElementById('totals_3rd').value = 0;
        } else if ($('#hide_paye').css('display') != 'none')
        {
            document.getElementById('taxform_2nd').value = '';
            document.getElementById('ty_2nd').value = 0;
            document.getElementById('referencemonths_2nd').value = 0;
            document.getElementById('pdbm_2nd').value = 0;
            document.getElementById('pay_2nd').value = '';
            document.getElementById('p1_2nd').value = 0;
            document.getElementById('ip_2nd').value = 0;
            document.getElementById('TaxShdPaid_2nd').value = '';
            document.getElementById('ppa1_2nd').value = 0;
            document.getElementById('tp1_2nd').value = 0;
            document.getElementById('pay1_2nd').value = '';
            document.getElementById('ppa_2nd').value = 0;
            document.getElementById('tp_2nd').value = 0;
            document.getElementById('p2_2nd').value = 0;
            document.getElementById('ip1_2nd').value = 0;
            document.getElementById('totals_2nd').value = 0;
            $('#hide_paye').hide()
            $('#minus').hide()
        }

    }

    function getMonth12(monthStr){
      return new Date(monthStr+'-1-01').getMonth()+1
    }
    function getDaysInMonth12(m, y) { //TrueBlueAussie
        return m===2?y&3||!(y%25)&&y&15?28:29:30+(m+(m>>3)&1);
    }
function calculate()
{
    var total_all_paye = 0;
    var total_all_1penalty = 0;
    var total_all_Interest_penalty = 0;
    var total_all_total_penalties_late_pmt = 0;
    var total_all_paye_subtotal = 0;
    var total_all_special_contribution = 0;
    var total_all_1penaltyForSp_contribution = 0;
    var total_all_IntPenaltyForSp_contribution = 0;
    var total_all_TotalPenaltyLateForSp_contribution = 0;
    var total_all_sp_contribution_subtotal = 0;
    var total_all_paye_subtotal_with_special_contribution = 0;

    var mon_3rd = 0;
    var pay_3rd = document.getElementById('pay_3rd').value;
    var pay1_3rd = document.getElementById('pay1_3rd').value;
    var sdate_3rd = document.getElementById('taxform_3rd').value;
    var edate_3rd = document.getElementById('paymentdate').value;
    var date1_3rd = new Date(sdate_3rd);
    var intrest_3rd = 0;
    var intrest1_3rd = 0;
    var interestp1_3rd = 0;
    var interestp_3rd =0;
    var paye_3rd = 0;
    var paye1_3rd = 0;
    var t1_3rd=0;
    var t2_3rd=0;
    var totals_3rd =0;
    if(sdate_3rd!='' && pay_3rd!=''){
    document.getElementById('ty_3rd').value = date1_3rd.getFullYear();
    document.getElementById('referencemonths_3rd').value = date1_3rd.getMonth() + 1;
  }
    var ani155_3rd = date1_3rd.getMonth()+3;
    var ani133_3rd = date1_3rd.getFullYear();
    if(ani155_3rd==13){
        ani155_3rd=1;
        ani133_3rd = ani133_3rd+1;
      }else if(ani155_3rd==14){
        ani155_3rd=2;
        ani133_3rd = ani133_3rd+1;
      }
    var lastDate12_3rd = getDaysInMonth12(ani155_3rd, ani133_3rd);
      if(ani133_3rd%4==0 && ani155_3rd==2){
        lastDate12_3rd=29;
      }

    var showViewDate12_3rd = lastDate12_3rd+"/"+collectionMonths[ani155_3rd-1]+"/"+ani133_3rd;
    

    var date2_3rd = new Date(edate_3rd);
    var diffYears_3rd = date2_3rd.getFullYear() - date1_3rd.getFullYear();
    var diffMonths_3rd = date2_3rd.getMonth() - date1_3rd.getMonth();
    var diffDays_3rd = date2_3rd.getDate() - date1_3rd.getDate();
    var months_3rd = (diffYears_3rd * 12 + diffMonths_3rd);
    if (diffDays_3rd > 0) {
        months_3rd += '.' + diffDays_3rd;
    }
    else if (diffDays_3rd < 0) {
        months_3rd--;
        months_3rd += '.' + (new Date(date2_3rd.getFullYear(), date2_3rd.getMonth(), 0).getDate() + diffDays_3rd);

    }

    mon_3rd = parseInt(months_3rd) - 2;

    if (mon_3rd > 0) {

        intrest_3rd = pay_3rd * 0.01 * mon_3rd;
        
        if(mon_3rd>8){
           interestp_3rd = pay_3rd * 0.0029167 * parseFloat(mon_3rd)+(parseFloat(mon_3rd)-8)*0.4161;
        }else{
          interestp_3rd = pay_3rd * 0.0029167 * parseFloat(mon_3rd);
        }
        paye_3rd = parseFloat(intrest_3rd) + parseFloat(interestp_3rd);
        t1_3rd = parseFloat(pay_3rd) + parseFloat(paye_3rd);
        

        if (pay1_3rd != "" && pay1_3rd != 0) {

            intrest1_3rd = pay1_3rd * 0.01 * mon_3rd;
            
            if(mon_3rd>8){
                  interestp1_3rd = pay1_3rd * 0.0029167 * parseFloat(mon_3rd)+(parseFloat(mon_3rd)-8)*0.4161;
             }else{
                interestp1_3rd = pay1_3rd * 0.0029167 * parseFloat(mon_3rd);
             }
            paye1_3rd = parseFloat(intrest1_3rd) + parseFloat(interestp1_3rd);
            t2_3rd = parseFloat(pay1_3rd) + parseFloat(paye1_3rd);
            totals_3rd = parseFloat(t1_3rd) + parseFloat(t2_3rd);

            
            /*document.getElementById('total1_3rd').value = parseFloat() + parseFloat(t2_3rd);
            document.getElementById('total2_3rd').value = parseFloat() + parseFloat(t3_3rd);*/

        }else{
          pay1_3rd=0;
          intrest1_3rd = 0;
          interestp1_3rd = 0;
          paye1_3rd = 0;
          t2_3rd = 0;
          totals_3rd = parseFloat(t1_3rd);
        }

        if(sdate_3rd!='' && pay_3rd!=''){
        document.getElementById('TaxShdPaid_3rd').value = showViewDate12_3rd;
        document.getElementById('pdbm_3rd').value = mon_3rd;
        document.getElementById('p1_3rd').value = intrest_3rd.toFixed(2);        
        document.getElementById('ip_3rd').value = interestp_3rd.toFixed(2);
        document.getElementById('ppa_3rd').value = paye_3rd.toFixed(2);
        document.getElementById('tp_3rd').value = parseFloat(t1_3rd).toFixed(2);
        document.getElementById('p2_3rd').value = intrest1_3rd.toFixed(2);
        document.getElementById('ip1_3rd').value = interestp1_3rd.toFixed(2);
        document.getElementById('ppa1_3rd').value = paye1_3rd.toFixed(2);
        document.getElementById('tp1_3rd').value = parseFloat(t2_3rd).toFixed(2);
        document.getElementById('totals_3rd').value = totals_3rd.toFixed(2);
      }

    }

    else {

        t1_3rd = pay_3rd;
        document.getElementById('tp_3rd').value = t1_3rd;
        document.getElementById('tp1_3rd').value = t2_3rd;

        if (pay1_3rd != "")
        {
            t2_3rd =  pay1_3rd;
            totals_3rd = parseFloat(t1_3rd) + parseFloat(t2_3rd);
        }else{
          pay1_3rd =0;
          t2_3rd = pay1_3rd;
          totals_3rd = parseFloat(t1_3rd)+parseFloat(t2_3rd);
        }

        if(sdate_3rd!='' && pay_3rd!=''){
        document.getElementById('TaxShdPaid_3rd').value = showViewDate12_3rd;
        document.getElementById('pdbm_3rd').value = 0;
        document.getElementById('p1_3rd').value = intrest_3rd.toFixed(2);        
        document.getElementById('ip_3rd').value = interestp_3rd.toFixed(2);
        document.getElementById('ppa_3rd').value = paye_3rd.toFixed(2);
        document.getElementById('tp_3rd').value = parseFloat(t1_3rd).toFixed(2);
        document.getElementById('p2_3rd').value = intrest1_3rd.toFixed(2);
        document.getElementById('ip1_3rd').value = interestp1_3rd.toFixed(2);
        document.getElementById('ppa1_3rd').value = paye1_3rd.toFixed(2);
        document.getElementById('tp1_3rd').value = parseFloat(t2_3rd).toFixed(2);
        document.getElementById('totals_3rd').value = totals_3rd.toFixed(2);
      }


    }
    if(t1_3rd!='' && totals_3rd!=''){
      total_all_paye += parseFloat(pay_3rd);
      total_all_1penalty += intrest_3rd;
      total_all_Interest_penalty += parseFloat(interestp_3rd);
      total_all_total_penalties_late_pmt += paye_3rd;
      total_all_paye_subtotal += parseFloat(t1_3rd);
      total_all_special_contribution += parseFloat(pay1_3rd);
      total_all_sp_contribution_subtotal += t2_3rd;
      total_all_1penaltyForSp_contribution +=intrest1_3rd;
      total_all_IntPenaltyForSp_contribution +=interestp1_3rd;
      total_all_TotalPenaltyLateForSp_contribution +=paye1_3rd;
      total_all_paye_subtotal_with_special_contribution +=totals_3rd;
    }


    var mon_2nd = 0;
    var pay_2nd = document.getElementById('pay_2nd').value;
    var pay1_2nd = document.getElementById('pay1_2nd').value;
    var sdate_2nd = document.getElementById('taxform_2nd').value;
    var edate_2nd = document.getElementById('paymentdate').value;
    var intrest_2nd = 0;
    var intrest1_2nd = 0;
    var interestp1_2nd = 0;
    var interestp_2nd =0;
    var paye_2nd = 0;
    var t1_2nd=0;
    var t2_2nd=0;
    var totals_2nd =0;

    var date1_2nd = new Date(sdate_2nd);
    if(sdate_2nd!='' && pay_2nd!=''){
    document.getElementById('ty_2nd').value = date1_2nd.getFullYear();
    document.getElementById('referencemonths_2nd').value = date1_2nd.getMonth() + 1;
    }
    var ani155_2nd = date1_2nd.getMonth()+3;
    var ani133_2nd = date1_2nd.getFullYear();
    if(ani155_2nd==13){
        ani155_2nd=1;
        ani133_2nd = ani133_2nd+1;
      }else if(ani155_2nd==14){
        ani155_2nd=2;
        ani133_2nd = ani133_2nd+1;
      }
    var lastDate12_2nd = getDaysInMonth12(ani155_2nd, ani133_2nd);
      if(ani133_2nd%4==0 && ani155_2nd==2){
        lastDate12_2nd=29;
      }

    var showViewDate12_2nd = lastDate12_2nd+"/"+collectionMonths[ani155_2nd-1]+"/"+ani133_2nd;
    

    var date2_2nd = new Date(edate_2nd);
    var diffYears_2nd = date2_2nd.getFullYear() - date1_2nd.getFullYear();
    var diffMonths_2nd = date2_2nd.getMonth() - date1_2nd.getMonth();
    var diffDays_2nd = date2_2nd.getDate() - date1_2nd.getDate();
    var months_2nd = (diffYears_2nd * 12 + diffMonths_2nd);
    if (diffDays_2nd > 0) {
        months_2nd += '.' + diffDays_2nd;
    }
    else if (diffDays_2nd < 0) {
        months_2nd--;
        months_2nd += '.' + (new Date(date2_2nd.getFullYear(), date2_2nd.getMonth(), 0).getDate() + diffDays_2nd);

    }

    mon_2nd = parseInt(months_2nd) - 2;

    if (mon_2nd > 0) {
        intrest_2nd = pay_2nd * 0.01 * mon_2nd;

         if(mon_2nd>8){
        interestp_2nd = pay_2nd*0.0029167*parseFloat(mon_2nd)+(parseFloat(mon_2nd)-8)*0.4161;
      }else{
        interestp_2nd = pay_2nd*0.0029167*parseFloat(mon_2nd);

      }

        paye_2nd = parseFloat(intrest_2nd) + parseFloat(interestp_2nd);
        t1_2nd = parseFloat(pay_2nd) + parseFloat(paye_2nd);

        if (pay1_2nd != "" && pay1_2nd != 0) {
            intrest1_2nd = pay1_2nd * 0.01 * mon_2nd;

             if(mon_2nd>8){
        interestp1_2nd = pay1_2nd*0.0029167*parseFloat(mon_2nd)+(parseFloat(mon_2nd)-8)*0.4161;
      }else{
        interestp1_2nd = pay1_2nd*0.0029167*parseFloat(mon_2nd);

      }
            paye1_2nd = parseFloat(intrest1_2nd) + parseFloat(interestp1_2nd);
            t2_2nd = parseFloat(pay1_2nd) + parseFloat(paye1_2nd);
            totals_2nd = parseFloat(t1_2nd) + parseFloat(t2_2nd);

            /*document.getElementById('total1_2nd').value = parseFloat() + parseFloat(t2_2nd);
            document.getElementById('total2_2nd').value = parseFloat() + parseFloat(t3_2nd);*/

        }else{
          paye1_2nd=0;
          t2_2nd = 0;
          pay1_2nd = 0;
          intrest1_2nd = 0;
          interestp1_2nd = 0;
          paye1_2nd = 0;
          t2_2nd = 0;
          totals_2nd = parseFloat(t1_2nd);
        }

        if(sdate_2nd!='' && pay_2nd!=''){
        document.getElementById('TaxShdPaid_2nd').value = showViewDate12_2nd;
        document.getElementById('pdbm_2nd').value = mon_2nd;
        document.getElementById('p1_2nd').value = intrest_2nd;
        document.getElementById('ip_2nd').value = interestp_2nd.toFixed(2);
        document.getElementById('ppa_2nd').value = paye_2nd.toFixed(2);
        document.getElementById('tp_2nd').value = t1_2nd.toFixed(2);
        document.getElementById('p2_2nd').value = intrest1_2nd;
        document.getElementById('ip1_2nd').value = interestp1_2nd.toFixed(2);
        document.getElementById('ppa1_2nd').value = paye1_2nd.toFixed(2);
        document.getElementById('tp1_2nd').value = parseFloat(t2_2nd).toFixed(2);
        document.getElementById('totals_2nd').value = totals_2nd.toFixed(2);
       }
    }else {
      t1_2nd = pay_2nd;
      if (pay1_2nd != ""){
        t2_2nd = pay1_2nd;
        totals_2nd = parseFloat(t1_2nd) + parseFloat(t2_2nd);
      }else{
        pay1_2nd =0;
        t2_2nd = pay1_2nd;
        totals_2nd = parseFloat(t1_2nd) + parseFloat(t2_2nd);
      }

      intrest_2nd=0;
      interestp_2nd=0;
      intrest1_2nd = 0;
      interestp1_2nd = 0;
      paye1_2nd = 0;

      if(sdate_2nd!='' && pay_2nd!=''){
      document.getElementById('TaxShdPaid_2nd').value = showViewDate12_2nd;
      document.getElementById('pdbm_2nd').value = 0;
      document.getElementById('p1_2nd').value = intrest_2nd;
      document.getElementById('ip_2nd').value = interestp_2nd.toFixed(2);
      document.getElementById('ppa_2nd').value = paye_2nd.toFixed(2);
      document.getElementById('tp_2nd').value = t1_2nd;
      document.getElementById('p2_2nd').value = intrest1_2nd;
      document.getElementById('ip1_2nd').value = interestp1_2nd.toFixed(2);
      document.getElementById('ppa1_2nd').value = paye1_2nd.toFixed(2);
      document.getElementById('tp1_2nd').value = parseFloat(t2_2nd).toFixed(2);
      document.getElementById('totals_2nd').value = totals_2nd.toFixed(2);
     }

    }

    if(t1_2nd!='' && totals_2nd!=''){
      total_all_paye += parseFloat(pay_2nd);
      total_all_1penalty += intrest_2nd;
      total_all_Interest_penalty += parseFloat(interestp_2nd);
      total_all_total_penalties_late_pmt += paye_2nd;
      total_all_paye_subtotal += parseFloat(t1_2nd);
      total_all_special_contribution += parseFloat(pay1_2nd);
      total_all_sp_contribution_subtotal += t2_2nd;
      total_all_1penaltyForSp_contribution +=intrest1_2nd;
      total_all_IntPenaltyForSp_contribution +=interestp1_2nd;
      total_all_TotalPenaltyLateForSp_contribution +=paye1_2nd;
      total_all_paye_subtotal_with_special_contribution +=totals_2nd;
    }




    var pay = 0;
    var mon =0;
    var intrest = 0;
    var interestp = 0;
    var intrest1 = 0;
    var interestp1 = 0;
    var sdate;
    var pay1 = 0;
    var edate;
    var date1;
    var date2;
    var diffYears;
    var diffMonths;
    var diffDays;
    var months;
    var paye = 0;
    var t1 = 0;
    var paye1 = 0;
    var t2 = 0;
    var total_pay_firlst;
    var totals = 0;

    pay  = document.getElementById('pay').value;
    pay1  = document.getElementById('pay1').value;
    sdate = document.getElementById('taxform').value;
    edate = document.getElementById('paymentdate').value;

    date1 = new Date(sdate);
    if(sdate!='' && pay!=''){
    document.getElementById('ty').value  = date1.getFullYear();
    document.getElementById('referencemonths').value  = date1.getMonth()+1;
    }
    var ani155 = date1.getMonth()+3;
    var ani133 = date1.getFullYear();
    if(ani155==13){
        ani155=1;
        ani133 = ani133+1;
      }else if(ani155==14){
        ani155=2;
        ani133 = ani133+1;
      }
    var lastDate12 = getDaysInMonth12(ani155, ani133);
      if(ani133%4==0 && ani155==2){
        lastDate12=29;
      }

    var showViewDate12 = lastDate12+"/"+collectionMonths[ani155-1]+"/"+ani133;
    

    date2 = new Date(edate);
    diffYears = date2.getFullYear()-date1.getFullYear();
    diffMonths = date2.getMonth()-date1.getMonth();
    diffDays = date2.getDate()-date1.getDate();
    months = (diffYears*12 + diffMonths);
    if(diffDays>0) {
      months += '.'+diffDays;
    }else if(diffDays<0){
      months--;
      months+= '.'+ (new Date(date2.getFullYear(),date2.getMonth(),0).getDate()+diffDays);
    }

    mon =parseInt(months)-2;
    if(mon>0){
      intrest = pay *0.01* mon; 
      /*interestp = pay*0.0029167*parseFloat(mon);*/
      if(mon>8){
        interestp = pay*0.0029167*parseFloat(mon)+(parseFloat(mon)-8)*0.4161;
      }else{
        interestp = pay*0.0029167*parseFloat(mon);

      }
      paye = parseFloat(intrest) +parseFloat(interestp);
      t1 = parseFloat(pay) +parseFloat(paye);
      

      if(pay1 != "" && pay1 != 0){
        intrest1 = pay1*0.01*mon;
        
        if(mon>8){
        interestp1 = pay1*0.0029167*parseFloat(mon)+(parseFloat(mon)-8)*0.4161;
        }else{
          interestp1 = pay1*0.0029167*parseFloat(mon);

        }
        paye1 = parseFloat(intrest1) + parseFloat(interestp1);
        t2 = parseFloat(pay1) + parseFloat(paye1);
        totals = parseFloat(t1)+parseFloat(t2);

        /*document.getElementById('total1').value =  parseFloat()+parseFloat(t2);
        document.getElementById('total2').value  = parseFloat()+parseFloat(t3);*/
      }else{
          pay1 = 0;
          intrest1 = 0;
          interestp1 = 0;
          paye1 = 0;
          t2 = 0;
          totals = parseFloat(t1);
        }

      if(sdate!='' && pay!=''){
        document.getElementById('TaxShdPaid').value = showViewDate12;
        document.getElementById('pdbm').value = mon;
        document.getElementById('p1').value = intrest;
        document.getElementById('ip').value = interestp.toFixed(2);
        document.getElementById('ppa').value = paye.toFixed(2);
        document.getElementById('tp').value = t1.toFixed(2);
        document.getElementById('p2').value = intrest1;
        document.getElementById('ip1').value = interestp1.toFixed(2);
        document.getElementById('ppa1').value = paye1.toFixed(2);
        document.getElementById('tp1').value = t2.toFixed(2);
        document.getElementById('totals').value = totals.toFixed(2);
      }
    }else{
      t1 = pay;

      if(pay1 !=""){
        t2 = pay1;
        totals = parseFloat(t1)+parseFloat(t2);  
      }else{
        pay1 =0;
        t2 = pay1;
        totals = parseFloat(t1)+parseFloat(t2);
      }
      intrest=0;
      interestp=0;
      intrest1 = 0;
      interestp1 = 0;
      paye1 = 0;

      if(sdate!='' && pay!=''){
      document.getElementById('TaxShdPaid').value = showViewDate12;
      document.getElementById('pdbm').value = 0;
      document.getElementById('p1').value = intrest;
      document.getElementById('ip').value = interestp;
      document.getElementById('ppa').value = paye;
      document.getElementById('tp').value = t1;

      document.getElementById('tp1').value = t2;
      document.getElementById('p2').value = intrest1;
      document.getElementById('ip1').value = interestp1.toFixed(2);
      document.getElementById('ppa1').value = paye1.toFixed(2);
      document.getElementById('tp1').value = t2.toFixed(2);
      document.getElementById('totals').value = totals.toFixed(2);
    }
    }

    if(t1!='' && totals!=''){
      total_all_paye += parseFloat(pay);
      total_all_1penalty += intrest;
      total_all_Interest_penalty += parseFloat(interestp);
      total_all_total_penalties_late_pmt += paye;
      total_all_paye_subtotal += parseFloat(t1);
      total_all_special_contribution += parseFloat(pay1);
      total_all_sp_contribution_subtotal += t2;
      total_all_1penaltyForSp_contribution += intrest1;
      total_all_IntPenaltyForSp_contribution += interestp1;
      total_all_TotalPenaltyLateForSp_contribution += paye1;
      total_all_paye_subtotal_with_special_contribution +=totals;
    }
    if(edate =='')
    {
      alert("Please select submission date.");
      document.getElementById('paymentdate').focus();   
    }else if(sdate==''){
      alert("Please select tax month ending.");
      document.getElementById('taxform').focus();

    }else if(pay==''){
      alert("Please insert paye amount.");
      document.getElementById('pay').focus();

    }else{
       if($("#hide_paye").is(":visible") && !$("#hide_paye_next").is(":visible")){
          if(sdate_2nd ==''){
            alert("Please select 2nd tax month ending.");
            document.getElementById('taxform_2nd').focus();
          }else if(pay_2nd ==''){
            alert("Please insert 2nd paye amount.");
            document.getElementById('pay_2nd').focus();
          }else{
            submitTotal();
          }

       }else if($("#hide_paye").is(":visible") && $("#hide_paye_next").is(":visible") ){
          if(sdate_2nd ==''){
            alert("Please select 2nd tax month ending.");
            document.getElementById('taxform_2nd').focus();
          }else if(pay_2nd ==''){
            alert("Please insert 2nd paye amount.");
            document.getElementById('pay_2nd').focus();
          }else if(sdate_3rd ==''){
            alert("Please select 3rd tax month ending.");
            document.getElementById('taxform_3rd').focus();
          }else if(pay_3rd ==''){
            alert("Please insert 3rd paye amount.");
            document.getElementById('pay_3rd').focus();
          }else{
            submitTotal();
          }

       }else if(!$("#hide_paye").is(":visible") && !$("#hide_paye_next").is(":visible")){
        submitTotal();
       }

    }



    function submitTotal(){
      $('#total_paye').show();
      document.getElementById('total_all_pay').value= total_all_paye.toFixed(2);
      document.getElementById('total_all_p1').value= total_all_1penalty.toFixed(2);
      document.getElementById('total_all_ip').value= total_all_Interest_penalty.toFixed(2);
      document.getElementById('total_all_ppa').value= total_all_total_penalties_late_pmt.toFixed(2);
      document.getElementById('total_all_tp').value= total_all_paye_subtotal.toFixed(2);
      document.getElementById('total_all_pay1').value= total_all_special_contribution;
      document.getElementById('total_all_tp1').value= parseFloat(total_all_sp_contribution_subtotal).toFixed(2);
      document.getElementById('total_all_p2').value= total_all_1penaltyForSp_contribution.toFixed(2);
      document.getElementById('total_all_ip1').value= total_all_IntPenaltyForSp_contribution.toFixed(2);
      document.getElementById('total_all_ppa1').value= total_all_TotalPenaltyLateForSp_contribution.toFixed(2);
      document.getElementById('total_all_totals').value= total_all_paye_subtotal_with_special_contribution.toFixed(2);
    }
}

</script>
<style type="text/css">
  .hiddingDivShow td{
          max-width: 220px;
        }
  #total_paye{
    display: none;
  }
  input{
    text-align: right;
  }
  .input-disabled{
          background-color:#e6e6e4!important;
          cursor:not-allowed;
        }
</style>
      <div class="col-md-12 col-xs-12">
        <div class="rightbar-area">
          <div ng-app="productsTableApp">
            <div ng-controller="ProductsController">
              <div class="btn-section"> 
                
                <!--<button data-ng-click="removeRow($index)" class="btn-blue">- Row</button>
                <input class="button btn-blue" type="button" value="+Row" ng-click="addRow()"/>-->
             <!--   <input class="button btn-blue" type="button" value="+" ng-click="addColumn()"/>
                <button ng-click="removeColumn($index)" class="btn-blue">-</button> -->

                <table class="space">
<tr><center><p style="font-size:18px ">P.A.Y.E/Special Contribution</p></center></tr>
<tr>
<center>
<td> <div class="date input-group">
  <input type="text" class="input-group-addon" id="paymentdate" onkeypress="return false;" placeholder="select Date">
  </div></td>


 <td class="sp">
 <select onchange="location = this.options[this.selectedIndex].value;">
    <option>Please select</option>
    <option value="corporation.php" >Corporation</option>
  
    <option value="paye.php" selected> P.A.Y.E /Special Contribution </option>
      <option value="with-holding-tax.php" >Withholding</option>
      <option value="Special-Defence-Contribution.php"> Special Defence Contribution </option>

   
</select>
 </td>
<td>    <button onclick="calculate()">Calculate</button></td>
<td>    <button onclick="add_paye()" id="plus">+</button></td>
<td>    <button onclick="subtract_paye()" id="minus">-</button></td>
</center>
 </tr>
 </table>
              </div>
              <div class="tables-group">
                <table class="table table-condensed table-responsive table-striped">
                  <tbody>
                    <tr>
                        <div class="col-md-6">
                      <td><table class="table-responsive">
                          <tr ng-repeat="datum in data">
                            <td><strong>Tax for the Month ending on:</strong></td>
                            <td ng-repeat="field in datum" class="date">
                            <input  value="" class="input-group-addon paySelectDate" id="taxform" onkeypress="return false;"></td>
                          </tr>
                          
                          <tr ng-repeat="datum in data">
                            <td>Tax Year:</td>
                            <td ng-repeat="field in datum"><p><output id="ty" value="0">0</output></p></td>
                          </tr>
                          <tr ng-repeat="datum in data">
                            <td>Month of Reference:</td>
                            <td ng-repeat="field in datum">
                            <p><output id="referencemonths" value="0">0</output></p>
                            </td>
                          </tr>
                          
                          <tr ng-repeat="datum in data">
                            <td>Tax should have been Paid by:</td>
                            <td ng-repeat="field in datum"><p><output id="TaxShdPaid" value="0" style="">0</output></p></td>
                          </tr>
                          
                          <tr ng-repeat="datum in data">
                            <td>Payment Delayed by (months):</td>
                            <td ng-repeat="field in datum">
                           <p ><output id="pdbm" value="0">0</output></p>
                              </td>
                          </tr>
                          <tr ng-repeat="datum in data">
                            <td><strong>PAYE:</strong></td>
                            <td ng-repeat="field in datum">
                            <input type="text" placeholder="" onBlur="this.placeholder=''" onFocus="this.placeholder=''" id="pay"/>
                            </td>
                          </tr>
                          <tr ng-repeat="datum in data">
                            <td>1% penalty(per complete delayed month):</td>
                            <td ng-repeat="field in datum"><p><output id="p1" value="0">0</output></p></td>
                          </tr>
                          <tr ng-repeat="datum in data">
                            <td>Interest penalty:</td>
                            <td ng-repeat="field in datum"><p><output id="ip" value="0">0</output></p></td>
                          </tr>
                          <tr ng-repeat="datum in data">
                            <td>Total Penalties for late pmt of PAYE:</td>
                            <td ng-repeat="field in datum"><p><output id="ppa" value="0">0</output></p></td>
                          </tr>
                          
                          <tr ng-repeat="datum in data">
                            <td><strong>PAYE Subtotal <span>(Tax + Penalties)</span>:</strong></td>
                            <td ng-repeat="field in datum"><b><output id="tp" value="0" style="min-height:27px;">0</output></b></td>
                          </tr>
                          
                          <tr ng-repeat="datum in data">
                            <td><strong>SPECIAL CONTRIBUTION:</strong></td>
                            <td ng-repeat="field in datum"> 
                            <input class="statusSpCont" type="text" placeholder="" onBlur="this.placeholder=''" onFocus="this.placeholder=''" id="pay1"/>
                            </td>
                          </tr>
                          
                          <tr ng-repeat="datum in data">
                            <td>1% penalty(per complete delayed month):</td>
                            <td ng-repeat="field in datum"><p><output id="p2" value="0">0</output></p></td>
                          </tr>
                          
                          <tr ng-repeat="datum in data">
                            <td>Interest penalty:</td>
                            <td ng-repeat="field in datum"><p><output id="ip1" value="0">0</output></p></td>
                          </tr>
                          
                          <tr ng-repeat="datum in data">
                            <td>Total Penalties for late pmt of Sp.Contribution:</td>
                            <td ng-repeat="field in datum"><p><output id="ppa1" value="0">0</output></p></td>
                          </tr>
                                                
                          <tr ng-repeat="datum in data">
                           <td><strong>SP.CONTRIBUTION Subtotal <span>(Tax + Penalties)</span>:</strong></td>
                            <td ng-repeat="field in datum"><b><output id="tp1" value="0" style="min-height:27px;">0</output></b></td>
                          </tr>
                          
                          <tr ng-repeat="datum in data">
                           <td><strong>TOTAL AMOUNT PAYABLE <span> (incl.penalties)</span>:</strong></td>
                            <td ng-repeat="field in datum"><b><output id="totals" value="0">0</output></b></td>
                          </tr>
                          
                        </table>
                        </td>
                    </div>
                        <div class="col-md-3">
                            <td id="hide_paye"><table class="table-responsive hiddingDivShow">
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum" class="date">
                                            <input  value="" class="input-group-addon paySelectDate" id="taxform_2nd"  onkeypress="return false;"></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="ty_2nd" value="0">0</output></p></td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum">
                                            <p><output id="referencemonths_2nd" value="0">0</output></p>
                                        </td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="TaxShdPaid_2nd" value="0" style="min-height:27px;">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum">
                                            <p><output id="pdbm_2nd" value="0">0</output></p>
                                        </td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum">
                                            <input type="text" placeholder="" onBlur="this.placeholder=''" onFocus="this.placeholder=''" id="pay_2nd"/>
                                        </td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="p1_2nd" value="0">0</output></p></td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="ip_2nd" value="0">0</output></p></td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="ppa_2nd" value="0">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><b><output id="tp_2nd" value="0" style="min-height: 27px;">0</output></b></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum">
                                            <input class="statusSpCont" type="text" placeholder="" onBlur="this.placeholder=''" onFocus="this.placeholder=''" id="pay1_2nd"/>
                                        </td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="p2_2nd" value="0">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="ip1_2nd" value="0">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="ppa1_2nd" value="0">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><b><output id="tp1_2nd" style="min-height:27px;" value="0">0</output></b></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><b><output id="totals_2nd" value="0">0</output></b></td>
                                    </tr>

                                </table>
                            </td>
                        </div>
                        <div class="col-md-3">
                            <td id="hide_paye_next"><table class="table-responsive hiddingDivShow">
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum" class="date">
                                            <input  value="" class="input-group-addon paySelectDate" id="taxform_3rd"  onkeypress="return false;"></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="ty_3rd" value="0">0</output></p></td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum">
                                            <p><output id="referencemonths_3rd" value="0">0</output></p>
                                        </td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="TaxShdPaid_3rd" value="0" style="min-height:27px;">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum">
                                            <p ><output id="pdbm_3rd" value="0">0</output></p>
                                        </td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum">
                                            <input type="text" placeholder="" onBlur="this.placeholder=''" onFocus="this.placeholder=''" id="pay_3rd"/>
                                        </td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="p1_3rd" value="0">0</output></p></td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="ip_3rd" value="0">0</output></p></td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="ppa_3rd" value="0">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><b><output id="tp_3rd" value="0" style="min-height:27px;">0</output></b></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum">
                                            <input class="statusSpCont" type="text" placeholder="" onBlur="this.placeholder=''" onFocus="this.placeholder=''" id="pay1_3rd"/>
                                        </td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="p2_3rd" value="0">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="ip1_3rd" value="0">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="ppa1_3rd" value="0">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><b><output id="tp1_3rd" style="min-height:27px;" value="0">0</output></b></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><b><output id="totals_3rd" value="0">0</output></b></td>
                                    </tr>

                                </table>
                            </td>
                        </div>
                        <div class="col-md-3">
                            <td id="total_paye"><table class="table-responsive hiddingDivShow">
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum" class="date" data-provide="datepicker" >
                                           <b><p style="margin:10px;">Total</p></b>
                                            </td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p>
                                        <!-- <output id="total_all_ty">0</output> -->
                                        <output>&nbsp;</output>
                                        </p></td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum">
                                            <p>
                                            <!-- <output id="total_all_referencemonths">0</output> -->
                                            <output>&nbsp;</output>
                                            </p>
                                        </td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p style="min-height:27px;">&nbsp;</p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum">
                                            <p ><!-- <output id="total_all_pdbm">0</output> -->
                                              <output>&nbsp;</output>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum">
                                            <!-- <input type="text" placeholder="" onBlur="this.placeholder=''" onFocus="this.placeholder=''" id="total_all_pay"/> -->
                                            <p style="margin-bottom: 13px;"><output id="total_all_pay" value="0">0</output></p>
                                        </td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="total_all_p1" value="0">0</output></p></td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="total_all_ip" value="0">0</output></p></td>
                                    </tr>
                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="total_all_ppa" value="0">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><b><p style="margin:auto;"><output id="total_all_tp" value="0">0</output></p></b></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum">
                                            <!-- <input type="text" placeholder="" onBlur="this.placeholder=''" onFocus="this.placeholder=''" id="total_all_pay1"/> -->
                                            <p style="margin-bottom: 13px;"><output id="total_all_pay1">0</output>
                                            </p>
                                        </td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="total_all_p2" value="0">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="total_all_ip1" value="0">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><p><output id="total_all_ppa1" value="0">0</output></p></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><b><p style="margin: auto;"><output id="total_all_tp1" value="0">0</output></p></b></td>
                                    </tr>

                                    <tr ng-repeat="datum in data">
                                        <td ng-repeat="field in datum"><b><p style="margin: auto;"><output id="total_all_totals" value="0">0</output></p></b></td>
                                    </tr>

                                </table>
                            </td>
                        </div>
                    </tr>
                  </tbody>
                </table>
               
              </div>
            </div>
          </div>
        </div>
      </div>
      
</div>
</div>
</div>
<script src="js/jquery.min-3.1.1.js" type="text/javascript"></script>
<script src="js/angular.min.js" type="text/javascript"></script>
<script src="js/angular-ui.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="js/main.js" type="text/javascript"></script>

<script>
$scope.dateOptions = { // pass jQueryUI DatePicker options through
    changeYear: true,
    changeMonth: true,
    dateFormat: 'yy-mm-dd',
}
</script>

<!--<script>
//accordian show hide all
$('.closeall').click(function(){
  $('.panel-collapse.in')
    .collapse('hide');
});

$('.openall').click(function(){
  $('.panel-collapse:not(".in")')
    .collapse('show');
});
</script>-->



<script>
$(function () {

    var active = true;

    $('#collapse-init').click(function () {
	        if (active) {
            active = false;
            $('.panel-collapse').collapse('show');
            $('.panel-title').attr('data-toggle', '');
            $(this).text('Collapse');
        } 
		
		else {
            active = true;
            $('.panel-collapse').collapse('hide');
            $('.panel-title').attr('data-toggle', 'collapse');
            $(this).text('Expend');
        }
    });
    
    $('#accordion').on('show.bs.collapse', function () {
        if (active) $('#accordion .in').collapse('hide');
    });

});
</script>




<script type="text/javascript">
var collectionMonths = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", 
               "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

//var selectedMonthName = collectionMonths[$("#datepicker").datepicker('getDate').getMonth()];

$(function () {
/*$('#datetimepicker').datetimepicker();*/

    /*$('.autoDateChange').datepicker({
     format: 'M/yyyy',
    });
    $('.autoDateChange').datepicker().on(picker_event, function(e) {
        console.log("hi there");
    });
    function picker_event(){
      alert("hooo");
    }*/
    $("#paymentdate").datepicker({
      format: 'dd/M/yyyy',
      autoclose: true
    });
    $(".autoDateChange").datepicker({
      format: 'M/yyyy'
    });
    function getMonth(monthStr){
      return new Date(monthStr+'-1-01').getMonth()+1
    }
    function getDaysInMonth(m, y) { //TrueBlueAussie
        return m===2?y&3||!(y%25)&&y&15?28:29:30+(m+(m>>3)&1);
    }

    $(".autoDateChange").on("change",function (){
      var date12 = $(this).val();
      var temp = date12.split("/");
      var monthTi = getMonth(temp[0]);
      var chanYear = parseInt(temp[1]);
      var SecondMonth = monthTi+2;
      
      if(SecondMonth==13){
      	SecondMonth=1;
      	chanYear = chanYear+1;
      }else if(SecondMonth==14){
      	SecondMonth=2;
      	chanYear = chanYear+1;
      }
      var lastDate = getDaysInMonth(SecondMonth, chanYear);
      if(chanYear%4==0 && SecondMonth==2){
      	lastDate=29;
      }
      
      var showViewDate = lastDate+"/"+collectionMonths[SecondMonth-1]+"/"+chanYear;

      var kid = $(this).parent().parent().next().find(".lastDateChange").text(showViewDate);
      $(this).datepicker('hide');
       
    });
    $(".taxYear").datepicker({
    	format: 'yyyy',
      viewMode: "years",
      minViewMode: "years",
      autoclose: true
    });
    $(".taxYear").on("change",function (){
      var date12 = $(this).val();
      var da1 = new Date(date12);
      var chYears = da1.getFullYear()+1;
      var chMonths = da1.getMonth()+8;
      var lastDa = getDaysInMonth(chMonths, chYears);
      if(chYears%4==0 && chMonths==2){
        lastDa=29;
      }
      var compPaidDate = lastDa+'/'+collectionMonths[chMonths-1]+'/'+chYears;

      var kid1 = $(this).parent().parent().parent().find(".comPaidDate").val(compPaidDate);
    });
    $(".taxReturnSubmit").datepicker({
      format: 'dd/M/yyyy',
      autoclose: true
    });
    $(".dateAssessment").datepicker({
      format: 'dd/M/yyyy',
      autoclose: true
    });
    
    


});
</script>

<script>

// Products Table Angular App
angular.module('productsTableApp', [])

// create a controller for the table
.controller('ProductsController', function($scope){
  // initialize the array
  $scope.data=[[{"en":""}]];
             
	// add a column
  $scope.addColumn = function(){
    $scope.data.forEach(function($){
      $.push({"en":""})
    });
  };

  // remove the selected column
  $scope.removeColumn = function (index) {
    // remove the column specified in index
    // you must cycle all the rows and remove the item
    // row by row
    $scope.data.forEach(function (element) {
      element.splice(index, 1);
	});
  };
 
});

</script>

<script>
 $("#menu-toggle").click(function(e) {
	e.preventDefault();
	$("#sidebar").toggle();
	$("#sidebar").toggleClass("is-active");
	$("#menu-toggle").toggleClass("active");
  });


</script>

<script>
$scope.dateOptions = { // pass jQueryUI DatePicker options through
    changeYear: true,
    changeMonth: true,
    dateFormat: 'yy-mm-dd',
}
</script>

<script>
var app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "home.php"
    })
    .when("/red", {
        templateUrl : "index.htm"
    })
    .when("/green", {
        templateUrl : "green.htm"
    })
    .when("/blue", {
        templateUrl : "blue.htm"
    });
});
</script>

</body>
</html>
      <script type="text/javascript">
      $(function () {
        $(".paySelectDate").datepicker({
      format: 'M/yyyy',
      viewMode: "months",
      minViewMode: "months",
      autoclose: true
    });
        $(".paySelectDate").on("change",function (){
          var startDate66 = document.getElementById('paymentdate').value;
          var paydate66 = new Date($(this).val());
          
          var temp66 = startDate66.split("/");
          var startYear66 = parseInt(temp66[2]);

          var payYear66 = parseInt(paydate66.getFullYear());
          var gap66 = startYear66-payYear66;

          if(gap66<=0){
            $(this).parent().parent().parent().find('.statusSpCont').attr('readonly', true);
            $(this).parent().parent().parent().find('.statusSpCont').val('');
            $(this).parent().parent().parent().find('.statusSpCont').addClass('input-disabled');

          }else{
            $(this).parent().parent().parent().find('.statusSpCont').attr('readonly', false);
            $(this).parent().parent().parent().find('.statusSpCont').removeClass('input-disabled')
          }
          
        });
        });
      </script>