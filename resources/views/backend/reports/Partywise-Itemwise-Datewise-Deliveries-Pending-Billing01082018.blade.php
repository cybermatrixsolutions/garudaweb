@extends('backend-includes.app')

@section('content')
<div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Partywise-Itemwise-Datewise Deliveries Pending for Billing </h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          
         
          <li><a href="#">Partywise-Itemwise-Datewise Deliveries Pending for Billing </a></li>
        </ul>
      </div>
    </div>
    <?php

    $fromdate=date('d/m/Y');
    $to_date=date('d/m/Y');
    if(isset($_GET['to_date'])){
      $to_date=$_GET['to_date'];
    }

    if(isset($_GET['fromdate'])){
      $fromdate=$_GET['fromdate'];
    }

    ?>
     <div class="row">
         <div class="col-md-12" style="margin-top: 40px;;">
            <form class="form-horizontal form-inline" name="myForm" action="{{route('PartyDeliveriesPendingforBilling')}}" enctype="multipart/form-data" method="get">

                     {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                       <div class="form-group">
                            <label class="control-label" for="pwd">Select Request <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                             <select class="form-control" name="request_type" >
                                      
                                      <option @if($request_type==1) selected @endif value="1">Fuel</option>
                                      <option @if($request_type==2) selected @endif value="2">Others</option>

                              </select>
                        </div>
                        @if(Auth::user()->user_type==3)
                        <div class="form-group">
                            <label class="control-label" for="pwd">Select Party <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                             <select class="form-control" name="party" >
                                      <option value="">All</option>
                                      @foreach($parties as $party)
                                        <option @if(isset($_GET['party']) && $_GET['party']==$party->Customer_Code) selected @endif value="{{$party->Customer_Code}}">{{$party->company_name}}</option>
                                      @endforeach
                              </select>
                        </div>
                        @else
                        <input type="hidden" name="party" value="{{session()->get('custm')}}">
                        @endif

                        <div class="form-group">
                          <label class="control-label"  for="from">From <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                           <input type="text"  @if(isset($_GET['fromdate'])) value="{{$_GET['fromdate']}}" @endif class="form-control" id="fromdate" name="fromdate" placeholder="from date" required/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                          <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                            <input  type="text" @if(isset($_GET['to_date'])) value="{{$_GET['to_date']}}" @endif class="form-control" id="to_date" name="to_date" placeholder="To Date" required/>&nbsp;&nbsp;
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default" id="checkdate" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                        </div>

                    </div>
                </div>
            </form> 
        </div>

    </div>

    <div class="row" >

     @if(isset($Transactions))

       @if($Transactions->count()>0)
        <div class="col-md-9">
            </div>
         <div class="col-md-3" >
            <a href="{{route('PartyDeliveriesPendingforBilling')}}/download?request_type={{$request_type}}&fromdate={{$fromdate}}&to_date={{$to_date}}" class="btn btn-default" style="float:right;width: auto;">Export in csv</a>
        </div>
      <div class="col-md-12">
        <div class="table-responsive" style="min-height: 400px;">
         
          <table class="table table-striped" id="myTable">
            <thead>
              
              <tr>

                <th>S.No.</th>
                <th>Slip Date</th>
                <th>Requisition / Slip No</th>
                <th>Transaction Type</th>
                <th>Party Code</th>
                <th>Party Name</th>
                <th>Product Ordered</th>
                 <th>Product Delivered</th>
                <th>Qty. Order</th>
                <th>Qty Deld.</th>
                <th>Rate </th>
                <th>Value</th>
               
              </tr>          
            </thead>
            <tbody>
              @foreach($Transactions as $Transaction)
              <tr>

                <td>{{$loop->iteration}}</td>
                <td>{{date('d/m/Y',strtotime($Transaction->trans_date))}}</td>
                <td>{{$Transaction->Request_id}}</td>
                <td>@if($Transaction->trans_mode=='SLIPS') SLIPS @else GARRUDA @endif</td>
                <td>{{$Transaction->customer_code}}</td>
                <td>{{$Transaction->getCustomerMaster->company_name}}</td>
               <td >
                  @if($Transaction->trans_mode=='SLIPS') 
                    {{$Transaction->getItemName->Item_Name}} 
                  @else
                     @if($Transaction->petrol_or_lube==1)
                        @if($Transaction->getfuelRequest!=null && $Transaction->getfuelRequest->itemname!=null){{$Transaction->getfuelRequest->itemname->Item_Name}}@endif
                     @else

                         {{$Transaction->getItemName->Item_Name}}

                     @endif
                  @endif
                </td>
                <td>{{$Transaction->getItemName->Item_Name}}</td>
                <td style="text-align: right;">
                  @if($Transaction->trans_mode=='SLIPS') 
                    {{$Transaction->petroldiesel_qty}}
                  @else
                     @if($Transaction->petrol_or_lube==1)
                        @if($Transaction->getfuelRequest!=null)

                           @if($Transaction->getfuelRequest->Request_Type=='Full Tank')
                            {{$Transaction->getfuelRequest->Request_Type}}
                            @else
                               {{$Transaction->getfuelRequest->Request_Value}}
                            @endif

                        @endif
                     @else
                         {{$Transaction->getlubeRequest()->where('item_id',$Transaction->item_code)->first()->quantity}}
                     @endif
                  @endif

                  
                </td>
                <td style="text-align: right;">{{$Transaction->petroldiesel_qty}}</td>
                <td style="text-align: right;">{{bcadd(round($Transaction->item_price,2),0,2)}}</td>
                
                 <td style="text-align: right;">{{bcadd(round($Transaction->item_price*$Transaction->petroldiesel_qty,2),0,2)}}</td>
               
              </tr>
              @endforeach

            </tbody>
          </table>
         
        </div>
      </div>
       @else
          <p  class="h3" style="text-align: center;padding: 25px;background: #fff;">No record found </p>
        @endif
    </div>
    @endif
    </div>
@endsection
@section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
 


jQuery(function(){
    var enddated=new Date();
    var todate=new Date();
      var app={
               
              init:function(){
               
                
                app.fromDate();
                app.to_date();
             
              },
              fromDate:function(){
                
                jQuery("#fromdate").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         endDate: new Date(),
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     jQuery("#to_date").val('');
                     enddated=jQuery(this).val();
                     todate=new Date();
                     app.to_date();
                    });
              },
              to_date:function(){
                  
                  jQuery("#to_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         startDate:enddated,
                        
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).datepicker('setStartDate',enddated).datepicker('setEndDate',todate);

              },

      };

      app.init();
});

</script>

@endsection