@extends('backend-includes.app')

@section('content')
<div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Salesmanwise Sales Report </h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          
         
          <li><a href="#">Salesmanwise Sales Report </a></li>
        </ul>
      </div>
    </div>
    <?php

    $fromdate=date('d/m/Y');
    $to_date=date('d/m/Y');
    if(isset($_GET['to_date'])){
      $to_date=$_GET['to_date'];
    }

    if(isset($_GET['fromdate'])){
      $fromdate=$_GET['fromdate'];
    }

   
    ?>
     <div class="row">
        <div class="col-md-12" style="margin-top: 40px;;">
            <form class="form-horizontal form-inline" name="myForm" action="{{'SalesmanWise-Sales-Report'}}" enctype="multipart/form-data" method="get">
               
                    
                     {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        

                        <div class="form-group">
                          <label class="control-label"  for="from">From <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                           <input type="text"  @if(isset($_GET['fromdate'])) value="{{$_GET['fromdate']}}" @endif class="form-control" id="from_date" name="fromdate" placeholder="from date" required/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                          <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                            <input  type="text" @if(isset($_GET['to_date'])) value="{{$_GET['to_date']}}" @endif class="form-control" id="to_date" name="to_date" placeholder="To Date" required/>&nbsp;&nbsp;
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default" id="checkdate" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                        </div>

                    </div>
                </div>
            </form> 
        </div>

    </div>

    <div class="row" >
      @if(isset($RoPersonalManagements))
       @if($RoPersonalManagements->count()>0)
        <div class="col-md-9">
            </div>
         <div class="col-md-3" style="margin-top: 40px;;">
            <a href="{{route('SalesmanWise-Sales-Report')}}/download?fromdate={{$fromdate}}&to_date={{$to_date}}" class="btn btn-default" style="float:right;width: auto;">Export in csv</a>
        </div>
      <div class="col-md-12">
        <div class="table-responsive" style="min-height: 400px;">
         
          <table class="table table-striped" id="myTable">
            <thead>
              
              <tr>
                
                <th>S.No.</th>
                <th>Invoice Date</th>
                <th>Invoice No.</th>
                <th>Salesman Name</th>
                <th>Shift</th>
                <th>Party code</th>
                <th>Party Name</th>
                <th>Item Sold Name</th>
                <th>Item Sold Qty </th>
                <th>Item Rate</th>
                <th>Item Sold Value</th>
                
              </tr>          
            </thead>
            <tbody>
             
                  @foreach($RoPersonalManagements as $RoPersonalManagement)
                     <?php $amount=0;?>

                     @if($RoPersonalManagement->getTrangections->count()>0)

                            @foreach($RoPersonalManagement->getTrangections as $TransactionModel)
                                 <?php 
                                 $amount=bcadd(round($TransactionModel->item_price*$TransactionModel->petroldiesel_qty,2),0,2)+$amount;
                                 $customerName = $TransactionModel->getCustomername;
                                 ?>
                                  <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{date('d/m/y',strtotime($TransactionModel->trans_date))}}</td>
                                        <td>{{$TransactionModel->invoice_number}}</td>
                                        <td>{{$RoPersonalManagement->Personnel_Name}}</td>
                                         <td>@if($TransactionModel->getShift!=null){{date('d/m/Y  h:i:s A',strtotime($TransactionModel->getShift->created_at))}}  To {{date('d/m/Y  h:i:s A',strtotime($TransactionModel->getShift->closer_date))}}@endif
                                       </td>
                                        <td>{{$TransactionModel->customer_code}}</td>
                                        <td>{{($customerName)? $customerName->company_name : $TransactionModel->customer_code}}</td>
                                       
                                        <td>{{$TransactionModel->getItemName->Item_Name}}</td>
                                        <td style="text-align: right;">{{bcadd(round($TransactionModel->petroldiesel_qty,2),0,2)}}</td>
                                        <td style="text-align: right;">{{bcadd(round($TransactionModel->item_price,2),0,2)}}</td>
                                        <td style="text-align: right;">{{bcadd(round($TransactionModel->item_price*$TransactionModel->petroldiesel_qty,2),0,2)}}</td>
                                  </tr>
                          
                                 @endforeach
                            <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td>Total</td>
                                  <td style="text-align: right;"></td>
                                  <td style="text-align: right;"></td>
                                  <td style="text-align: right;">{{bcadd(round($amount,2),0,2)}}</td>
                            </tr>
                      @endif
              @endforeach
            </tbody>
          </table>
         
        </div>
      </div>
       @else
          <p  class="h3" style="text-align: center;padding: 25px;background: #fff;">No record found </p>
        @endif
    </div>
     @endif
    </div>
@endsection
@section('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>


<script type="text/javascript">
 


jQuery(function(){
    var enddated=new Date();
    var todate=new Date();
      var app={
               
              init:function(){
               
                
                app.fromDate();
                app.to_date();
             
              },
              fromDate:function(){
                
                jQuery("#from_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         endDate: new Date(),
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     jQuery("#to_date").val('');
                     enddated=jQuery(this).val();
                     todate=new Date();
                     app.to_date();
                    });
              },
              to_date:function(){
                  
                  jQuery("#to_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         startDate:enddated,
                        
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).datepicker('setStartDate',enddated).datepicker('setEndDate',todate);

              },

      };

      app.init();
});

</script>

@endsection