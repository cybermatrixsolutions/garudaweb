@extends('backend-includes.app')

@section('content')
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link href="css/style.css" type="text/css" rel="stylesheet">

<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<!-- <script src="js/main.js"></script>  -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<style>
button.dt-button, div.dt-button, a.dt-button {
    
    background-image: linear-gradient(to bottom, #00786a 0%, #00786a 100%)!important;
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,StartColorStr='white', EndColorStr='#00786a');
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    text-decoration: none;
    outline: none;
    color:white!important;
    }
    .dt-buttons {
    float: right!important;
}

.dataTables_wrapper .dataTables_filter {
float: left;
text-align: left;
}
</style>


<div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Shift Fuels Sales </h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          
         
          <li><a href="#">Shift Fuels Sales </a></li>
        </ul>
      </div>
    </div>
    <?php

    $fromdate=date('d/m/Y');
    $to_date=date('d/m/Y');
    if(isset($_GET['to_date'])){
      $to_date=$_GET['to_date'];
    }

    if(isset($_GET['fromdate'])){
      $fromdate=$_GET['fromdate'];
    }

    ?>
     <div class="row">
        <div class="col-md-12" style="margin-top: 40px;;">
            <form class="form-horizontal form-inline" name="myForm" action="{{route('reading-report')}}" enctype="multipart/form-data" method="get">
               
                    
                     {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="form-group">
                          <label class="control-label"  for="from">From <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                           <input type="text"  @if(isset($_GET['fromdate'])) value="{{$_GET['fromdate']}}" @endif class="form-control" id="fromdate" name="fromdate" placeholder="from date" required/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                          <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                            <input  type="text" @if(isset($_GET['to_date'])) value="{{$_GET['to_date']}}" @endif class="form-control" id="to_date" name="to_date" placeholder="To Date" required/>&nbsp;&nbsp;
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default" id="checkdate" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                        </div>

                    </div>
                </div>
            </form> 
        </div>

    </div>

    <div class="row" >
      @if(isset($readings))
       @if($readings->count()>0)
        <div class="col-md-9">
            </div>
        <!--  <div class="col-md-3" >
            <a href="{{route('reading-report')}}/download?fromdate={{$fromdate}}&to_date={{$to_date}}" class="btn btn-default" style="float:right;width: auto;">Export in csv</a>
        </div> -->
      <div class="col-md-12">
        <div class="table-responsive" style="min-height: 400px;">
         
          <table class="table table-striped" id="myTable">
            <thead>
              
              <tr>
             <!--    <th>S.No.</th> -->
        
                <th>Shift Manager</th>
                <th>Item Name</th>
                <th>Pedestal </th>
                <th>Tank </th>
               <th>Nozzle No.</th>
               
                <th>Nozzle Start</th>
                <th>Nozzle End</th>
                <th>Gross</th>
                <th>Test</th>
                <th>Qty</th>

                <th>Created Date</th>
                <th>Created Time</th>                   
                <th>Price</th>
                <th>Before VAT</th>
                <th>VAT</th>
                <th>Val w/o VAT</th>
                <th>VAT Val</th>
                <th>Tot Val</th>
               
                
              </tr>          
            </thead>
            <tbody>
              <?php $j=1;?>
              @foreach($readings as $shifts)


                 <?php



                  $creditsales=0; $i=0; $natesales=0;
                        $GgotalizerOutQty=0;
                        $GnettSalesQty=0;
                        $testsales=0;
                        
                 ?>
                 @foreach($shifts as $reading)
                 <?php

                 //dd($reading);
                      $gotalizerOutQty=0;
                      $nettSalesQty=0;
                     if($i==0){
                      
                      foreach ($reading->getCreditSales as $getCreditSale) {
                       $creditsales=$getCreditSale->petroldiesel_qty+$creditsales;
                      }
                      
                     }
                      
                    $natesales=($reading->Nozzle_End-$reading->Nozzle_Start)+$natesales;
                    $gotalizerOutQty=$reading->Nozzle_End-$reading->Nozzle_Start;
                    $nettSalesQty=$reading->Nozzle_End-$reading->Nozzle_Start-$reading->test;
                    $GgotalizerOutQty=$gotalizerOutQty+$GgotalizerOutQty;
                    $GnettSalesQty=$nettSalesQty+$GnettSalesQty;
                    $testsales=$reading->test+$testsales;

                    $finalQty=$reading->reading-$reading->test

                 ?>
                 
                  <tr>

                  <!--   <td>{{$j}}</td> -->
                   
                    <td> <?php if(empty($reading->getShift->getPersonnelname->Personnel_Name)){
                       $Personal_Name="";
                      
                    }else{

                      $Personal_Name=$reading->getShift->getPersonnelname->Personnel_Name;
                    }

                    ?>
                    {{$Personal_Name}}

 </td>
                     <!-- <td>@if($reading->getShift!=null){{date('d/m/Y  h:i:s A',strtotime($reading->getShift->created_at))}}  To {{date('d/m/Y  h:i:s A',strtotime($reading->getShift->closer_date))}}@endif</td> -->
                                   
                    <?php   
                   
                    if(empty($reading->getNozzle->getItem->Item_Name)){
                       $Item_Name="";

                      
                    }else{

                      $Item_Name=$reading->getNozzle->getItem->Item_Name;
                    }
                    if(empty($reading->getNozzle->getTank->Tank_Number)){
                       $Tank_Name="";
                      
                    }else{

                      $Tank_Name=$reading->getNozzle->getTank->Tank_Number;
                    }
                    

                    $totalTaxPer=0;
                    $beforeVat=0;
                     $onlyVat=0;
                    $oVAT=0;
                    $vatValue=0;
                    $totalVal= 0;
                     if(empty($reading->getNozzle->getItem->gettex)){
                       $itemPercentages=0;
                      
                    }else{

                       $itemPercentages= $reading->getNozzle->getItem->gettex;

                       foreach ($itemPercentages as  $value) {

                         $totalTaxPer=$totalTaxPer+$value->Tax_percentage;
                         # code...
                          // dd($value,$value->Tax_percentage);
                       }
                    }

                    $beforeVat=round(($reading->price)/(1+($totalTaxPer/100)),2);
                   // dd($beforeVat);
                    $onlyVat=$reading->price-$beforeVat;
                    $oVAT=$reading->reading*$beforeVat;
                    $vatValue=$reading->reading*$onlyVat;
                    $totalVal=$oVAT+$vatValue;
                      ?>

                    <td>{{$Item_Name}}</td>
                    <td><?php if(empty($reading->getNozzle->getPadestals->Pedestal_Number)){
                       $Pedestal_Name="";
                    }else{
                      $Pedestal_Name=$reading->getNozzle->getPadestals->Pedestal_Number;
                    } ?>{{$Pedestal_Name}}</td> 

                    <td>{{$Tank_Name}}</td> 

                    <td>{{$reading->Nozzle_No}}</td>
                    <td  style="text-align: right;">{{bcadd($reading->Nozzle_Start,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($reading->Nozzle_End,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($reading->reading,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($reading->test,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($finalQty,0,3)}}</td>

                   
                    <td >{{$reading->created_date,""}}</td>
                    <td>{{$reading->created_time,""}}</td>
                   
                     <td style="text-align: right;">{{$reading->price,""}}</td>
                    <td style="text-align: right;">{{bcadd($beforeVat,0,2)}}</td>
                     <td style="text-align: right;">{{bcadd($onlyVat,0,2)}}</td>
                    <td style="text-align: right;">{{bcadd($oVAT,0,2)}}</td>
                    <td style="text-align: right;">{{bcadd($vatValue,0,2)}}</td>
                    <td style="text-align: right;">{{bcadd(round($totalVal,2),0,2)}}</td>
                  </tr>
                  <?php $j++; ?>
                 @endforeach
                  

              @endforeach

            <!--   <tr>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>Totals</td>




                    <td  style="text-align: right;">{{bcadd($testsales,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($testsales,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($GnettSalesQty,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($creditsales,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($GnettSalesQty-$creditsales,0,3)}}</td>
                  </tr>  -->
              
            </tbody>
            <tfoot align="right">
                  <!--   <td>&nbsp;&nbsp;</td> -->
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                      <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
              </tfoot>
          </table>
         
        </div>
      </div>
       @else
          <p  class="h3" style="text-align: center;padding: 25px;background: #fff;">No record found </p>
        @endif
    </div>
    @endif
    </div>
@endsection
@section('script')

<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>


















<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">
  
  $(document).ready(function() {
       $('#myTable').DataTable( {
         "dom": 'lBfr',
          "processing": true,
         buttons: [
           
            { extend: 'excelHtml5',text: 'Export in CSV', footer: true , filename: ''+Date('Y-m-d')+'-item-wise-nozzle-wise-reading-report'},
            
        ],  paging: false,



        "footerCallback": function ( row, data, start, end, display ) {


            var api = this.api(), data;
 
            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // computing column Total of the complete result 
            var grossTotal = api
                .column( 7 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
        
      var testTotal = api
                .column( 8 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
        
      var qtyTotal = api
                .column( 9 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
        
      

      var withVatTotal = api
                .column( 15 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );         

          var vatValTotal = api
                .column( 16 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                

                
              var totalValTotal = api
                .column( 17, { page: 'current'}  )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );                 
      
        
            // Update footer by showing the total with the reference of the column index 
      $( api.column( 4).footer() ).html('Total : ');
            $( api.column( 7 , { page: 'current'} ).footer() ).html(grossTotal.toFixed(3) );
            $( api.column( 8, { page: 'current'} ).footer() ).html(testTotal.toFixed(3));
            $( api.column( 9 , { page: 'current'} ).footer() ).html(qtyTotal.toFixed(3));
            
            $( api.column( 15, { page: 'current'}  ).footer() ).html(withVatTotal.toFixed(2));
            $( api.column( 16 , { page: 'current'} ).footer() ).html(vatValTotal.toFixed(2));
            $( api.column( 17 , { page: 'current'} ).footer() ).html(totalValTotal.toFixed(2));
        },
          
       
            
       
    } );
} );


jQuery(function(){
    var enddated=new Date();
    var todate=new Date();
    var enddatedAfterDay=new Date();
      var app={
               
              init:function(){
               
                
                app.fromDate();
                app.to_date();
             
              },
              fromDate:function(){
                
                jQuery("#fromdate").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         endDate: new Date(),
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     jQuery("#to_date").val('');
                      todate=new Date();
                     enddated=jQuery(this).val();
                    /* enddatedAfterDay = jQuery(this).val();
                     enddatedAfterDay.setDate(enddated.getDate() + 30);
                      alert(enddatedAfterDay);
                  var date2 = $('#fromdate').datepicker('getDate', '+30d'); 


*/
                    var enddatedAfterDay = $('#fromdate').datepicker('getDate');
                    enddatedAfterDay.setDate(enddatedAfterDay.getDate()+15)

                     app.to_date();
                    });
              },
              to_date:function(){

                 var enddatedAfterDay = $('#fromdate').datepicker('getDate');
                    enddatedAfterDay.setDate(enddatedAfterDay.getDate()+15);
              
                  
                  jQuery("#to_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         startDate:enddated,
                         
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).datepicker('setStartDate',enddated).datepicker('setEndDate',enddatedAfterDay);

                    /* alert(enddatedAfterDay);
                     alert(enddated);*/
              },

      };

      app.init();
});

</script>
@endsection