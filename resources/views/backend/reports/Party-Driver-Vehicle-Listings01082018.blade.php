@extends('backend-includes.app')

@section('content')
<div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Party, Driver, Vehicle Listings </h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          
         
          <li><a href="#">Party, Driver, Vehicle Listings </a></li>
        </ul>
      </div>
    </div>
    <?php

  
   $customer_code='all';
   
    if(isset($_GET['customer_code'])){
      $customer_code=$_GET['customer_code'];
    }

    ?>
     <div class="row">
        <div class="col-md-12" style="margin-top: 40px;;">
          @if(Auth::user()->user_type==3)
            <form class="form-horizontal form-inline" name="myForm" action="{{'Party-Driver-Vehicle-Listings'}}" enctype="multipart/form-data" method="get">
               
                    
                     {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label" for="pwd">Select Party <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                             <select class="form-control" name="customer_code" >
                                     <option value="all"> All </option>
                                      @foreach($parties as $party)
                                        <option @if(isset($_GET['customer_code']) && $_GET['customer_code']==$party->Customer_Code) selected @endif value="{{$party->Customer_Code}}">{{$party->company_name}}</option>
                                      @endforeach
                              </select>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default" id="checkdate" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                        </div>

                    </div>
                </div>
            </form> 
             @endif
        </div>

    </div>

    <div class="row" >
      @if(isset($CustomerDrivers))
       @if($CustomerDrivers->count()>0)
        <div class="col-md-9">
            </div>
         <div class="col-md-3" style="margin-top: 40px;;">
            <a href="{{route('Party-Driver-Vehicle-Listings')}}/download?customer_code={{$customer_code}}" class="btn btn-default" style="float:right;width: auto;">Export in csv</a>
        </div>
      <div class="col-md-12">
        <div class="table-responsive" style="min-height: 400px;">
         
          <table class="table table-striped" id="myTable">
            <thead>
              
              <tr>
                
                <th>S.No.</th>
                <th>Party Code</th>
                <th>Party Name</th>
                <th>Vehicle Regn.No.</th>
                <th>Vehicle Make</th>
                <th>Vehicle Model </th>
                <th>Vehicle Color</th>
                <th>Vehicle Fuel</th>
                <th>Vehicle Insurance Co.</th>
                <th>Vehicle Insurance Due On</th>
                <th>Vehicle PUC Due On</th>
                <th>Vehicle Regn.Valid upto</th>
                <th>Driver Name</th>
                <th>Driver Mobile</th>
                <th>Driver License Validity</th>
                
              </tr>          
            </thead>
            <tbody>
              @foreach($CustomerDrivers as $CustomerDriver)
              
              
                   <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$CustomerDriver->customer_code}}</td>
                    <td>@if($CustomerDriver->getCustomer!=null){{$CustomerDriver->getCustomer->company_name}}@endif</td>
                    <td>{{$CustomerDriver->Registration_Number}}</td>
                    <td>@if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->getmake!=null){{$CustomerDriver->getVihicle->getmake->name}}@endif</td>
                    <td>@if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->getmmodel!=null){{$CustomerDriver->getVihicle->getmmodel->name}}@endif</td>
                
                    <td>@if($CustomerDriver->getVihicle!=null){{$CustomerDriver->getVihicle->van_color}}@endif</td>
                    <td>@if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->getitem!=null){{$CustomerDriver->getVihicle->getitem->Item_Name}}@endif</td>
                    <td>@if($CustomerDriver->getVihicle!=null){{$CustomerDriver->getVihicle->industry_company}}@endif</td>
                    <td>@if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->insurance_due_date!=null){{date('d/m/Y',strtotime($CustomerDriver->getVihicle->insurance_due_date))}}@endif</td>
                    <td>@if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->puc_date!=null){{date('d/m/Y',strtotime($CustomerDriver->getVihicle->puc_date))}}@endif</td>
                    <td>@if($CustomerDriver->getVihicle!=null && $CustomerDriver->getVihicle->rc_valide!=null){{date('d/m/Y',strtotime($CustomerDriver->getVihicle->rc_valide))}}@endif</td>
                    <td>{{$CustomerDriver->Driver_Name}}</td>
                    <td>{{$CustomerDriver->Driver_Mobile}}</td>
                     <td>@if($CustomerDriver->valid_upto!=null){{date('d/m/Y',strtotime($CustomerDriver->valid_upto))}}@endif</td>
                  </tr>    
              @endforeach              
            </tbody>
          </table>
         
        </div>
      </div>
       @else
          <p  class="h3" style="text-align: center;padding: 25px;background: #fff;">No record found </p>
        @endif
    </div>
     @endif
    </div>
@endsection
