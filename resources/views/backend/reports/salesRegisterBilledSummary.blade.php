  @extends('backend-includes.app')

  @section('content')
  <div class="content-wrapper">
      <div class="page-title">
        <div>
          <h1><i class="fa fa-dashboard"></i>&nbsp;Sales Register-Summary (VAT Bill)
  </h1>
        </div>
        <div>
          <ul class="breadcrumb">
            <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

            
           
            <li><a href="#">Sales Register-Summary (VAT Bill)
  </a></li>
          </ul>
        </div>
      </div>
      <?php

      $fromdate=date('d/m/Y');
      $to_date=date('d/m/Y');
      if(isset($_GET['to_date'])){
        $to_date=$_GET['to_date'];
      }

      if(isset($_GET['fromdate'])){
        $fromdate=$_GET['fromdate'];
      }

      ?>
       <div class="row">
          <div class="col-md-12" style="margin-top: 40px;;">
              <form class="form-horizontal form-inline" name="myForm" action="{{'salesRegisterBilledSummary'}}" enctype="multipart/form-data" method="get">
                 
                      
                       {{ csrf_field() }}
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label class="control-label" for="pwd">Select Type <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                               <select class="form-control" name="fule_type" >
                                        <option @if($fule_type==2) selected @endif value="2">GST Bill</option>
                                           <option @if($fule_type=="GST Invoice") selected @endif value="GST Invoice">GST Invoice</option>
                                        <option @if($fule_type==1) selected @endif value="1">VAT Bill</option>
                                </select>
                          </div>
                          <div class="form-group">
                            <label class="control-label"  for="from">From <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                             <input type="text"  @if(isset($_GET['fromdate'])) value="{{$_GET['fromdate']}}" @endif class="form-control" id="fromdate" name="fromdate" placeholder="from date" required/>&nbsp;&nbsp;
                          </div>
                          <div class="form-group">
                            <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                              <input  type="text" @if(isset($_GET['to_date'])) value="{{$_GET['to_date']}}" @endif class="form-control" id="to_date" name="to_date" placeholder="To Date" required/>&nbsp;&nbsp;
                          </div>

                          <div class="form-group">
                              <input type="submit" class="btn btn-default" id="checkdate" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                          </div>

                      </div>
                  </div>
              </form> 
          </div>

      </div>

      <div class="row" >
  
        @if(isset($bills))
         @if($bills->count()>0)
          <div class="col-md-9">

          </div>
           <div class="col-md-3" style="margin-top: 40px;;">

           
              <a href="{{route('salesRegisterBilledDownloadSummary',['fule_type'=>$fule_type])}}?fromdate={{$fromdate}}&to_date={{$to_date}}" class="btn btn-default" style="float:right;width: auto;">Export in csv</a>
          </div>
        <div class="col-md-12">
          <div class="table-responsive" style="min-height: 400px;">
           
            <table class="table table-striped" id="myTable">
              <thead>
                
                <tr>
                  
                  <th>Inv.Type</th>
                  <th>Invoice No.</th>
                  <th>Invoice Date</th>
                  
                  <th>Party Code</th>
                  <th>Party Name</th>
                  <th>GSTIN No.</th>
                  <th> Place of Supply</th>
                  <th>Invoice Amount</th>
                  <th>VAT Taxable Value</th>
                  <th>GST Taxable Value</th>
                  
                 <th>CGST %</th>
                <th>CGST Amount</th>
                 <th>UT/SGST  %</th>
                <th>UT/SGST  Amount</th>
                 <th>IGST %</th>
                <th>IGST Amount</th>
                 <th>SPCESS %</th>
                <th>SPCESS Amount</th>
                 <th>VAT  %</th>
                <th>VAT Amount</th>
                
                  
                  <th>Handling Charges</th>
                  <th>Discount Amount</th>
                  <th>Round Off </th>
                </tr>          
              </thead>
              <tbody>
                <?php $i=1; 
                  $totalFinalAmount=0;

                  $totalHandlingCharges=0;
                  $totalDiscount=0;
                  $totalRoundOff=0;
                  $totalTaxableValue=0;

                  $totalCgstAmount=0;
                  $totalSgstAmt=0;  
                  $totalIgstAmt=0; 
                  $totalSpcesstAmt=0;
                  $totalVatAmt=0;  

                   $cgstPer="0.00";
                   $cgstAmount1=0;
                   $sgstPer="0.00";
                   $sgstAmt="0.00";
                   $igstPer="0.00";
                   $igstAmt="0.00";
                   $spcessPer="0.00";
                   $spcessAmt="0.00";
                   $vatPer="0.00";
                   $vatAmt="0.00";  


                ?>
         @foreach($bills as $bill)


               <?php 
            

               $getallTransactionid=App\TransactionModel::where('bill_no',$bill['invoice_no'])->pluck('id');
                // dd($bill['invoice_no'],$getallTransactionid) ; 
                 $getApplyTax=App\TransTaxModel::whereIn('transaction_id',$getallTransactionid)
                 ->groupby('Tax_percentage')
                  ->pluck('Tax_percentage');
               
               $getApplyTaxBoth=App\TransTaxModel::whereIn('transaction_id',$getallTransactionid)
                 ->groupby('GST_Type')
                  ->groupby('Tax_percentage')
                  ->selectRaw('GST_Type,Tax_percentage,SUM(Tax_Value_W) as taxAmount')->get();
                  
                //dd($getApplyTaxBoth);
                $totalFinalAmount=$totalFinalAmount+$bill->grand_total_amount;
                $totalHandlingCharges=$totalHandlingCharges+ $bill->handling_amount;
                $totalDiscount=$totalDiscount+$bill->discount;
                $totalRoundOff= $totalRoundOff+$bill->rounded_of;
              /*  $totalTaxableValue=$totalTaxableValue+$bill->taxable_amt;*/
               $tval=[];
              
             
             ?>    
            
           <?php

           $tratex=$getApplyTaxBoth->groupBy('taxAmount'); 

           //dd( $tratex);
                //dd($bill['invoice_no'],$getallTransactionid) ; 

              /*  if($tratex->isEmpty()){

                  dd('tratex is empty');
                }else{
                    dd('tratex is not empty');
                }*/
     if(!$tratex->isEmpty()){

           ?>


              @foreach($tratex as $key =>$tax)

             <?php 
            // dd($tax);
               $cgstPer="0.00";
               $cgstAmount1=0;
               $sgstPer="0.00";
               $sgstAmt="0.00";
               $igstPer="0.00";
               $igstAmt="0.00";
               $spcessPer="0.00";
               $spcessAmt="0.00";
               $vatPer="0.00";
               $vatAmt="0.00";
               ?>

                         <tr> 
                          
                           <td>@if($bill->type==1) VAT @else GST @endif</td>
                            <td>{{$bill->invoice_no}}</td>
                            <td>{{date('d/m/y',strtotime($bill->invoiceDate))}}</td>
                            <td>{{$bill->customer_code}}</td>
                          <td>{{$bill->party->company_name}}</td>
                      
                        <td>{{$bill->party->gst_no}}</td>
                       
                        <td>{{$bill->party->getstate->name}}</td>
                        <td style="text-align: right;">{{bcadd($bill->grand_total_amount, 0, 2)}} </td>
                          @foreach($tax as $value)

                                          
                                   <?php  

                                             if($value['GST_Type']=="VAT"){
                                                 $vatPer=$value['Tax_percentage'];
                                                 $vatAmt=$value['taxAmount'];
                                               }


                                $gettransTaxableValue=App\TransTaxModel::join('tbl_customer_transaction','tbl_customer_transaction.id','=','item_transaction_tax.transaction_id')
                                 ->whereIn('transaction_id',$getallTransactionid)
                                 ->groupby('item_transaction_tax.Tax_percentage')
                  
                              ->selectRaw('item_transaction_tax.Tax_percentage,SUM(tbl_customer_transaction.taxable_value_W) as taxableValue')
                              ->get();
                              $finalTaxable="0.00";
                             // dd($gettransTaxableValue);
                              foreach($gettransTaxableValue as $taxvisevalue){
                                if($taxvisevalue->Tax_percentage==$vatPer)
                                {


                                  $finalTaxable=$taxvisevalue->taxableValue;
                                }

                              }

                              $totalTaxableValue=$totalTaxableValue+$finalTaxable;
                               $totalVatAmt=$totalVatAmt+$vatAmt; 
                              //dd($taxvisevalue->Tax_percentage,$cgstPer,$finalTaxable);

                                         
                           ?>

                      
                              @endforeach
                              

                              <td style="text-align: right;">{{bcadd($finalTaxable ,0, 2)}} </td>
                        <td style="text-align: right;">0.00</td> 

                          <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                          <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">{{bcadd($vatPer,0,2)}}</td>
                         <td style="text-align: right;">{{bcadd($vatAmt ,0, 2)}}</td>
                      


                                         
                        <td style="text-align: right;">0.00</td>
                        <td style="text-align: right;">0.00</td>
                        <td style="text-align: right;">0.00</td>
                         
                  </tr>

                @endforeach  


             <?php  }else{ ?>

                 <tr> 
                          
                        <td>@if($bill->type==1) VAT @else GST @endif</td>
                        <td>{{$bill->invoice_no}}</td>
                        <td>{{date('d/m/y',strtotime($bill->invoiceDate))}}</td>
                        <td>{{$bill->customer_code}}</td>
                        <td>{{$bill->party->company_name}}</td>
                      
                        <td>{{$bill->party->gst_no}}</td>
                       
                        <td>{{$bill->party->getstate->name}}</td>
                        <td style="text-align: right;">{{bcadd($bill->grand_total_amount, 0, 2)}} </td>


                         <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td> 

                          <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                          <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                      


                                         
                        <td style="text-align: right;">0.00</td>
                        <td style="text-align: right;">0.00</td>
                        <td style="text-align: right;">0.00</td>



                      </tr>
               




             <?php  }  ?>





                        <?php 

                      
                  //dd($gettransTaxableValue);

                 $per=0;
                   foreach($allTexByRo as $allTax)
                   {

                        if($rostate==$bill->party->getstate->name){

                          $per=$allTax['Tax_percentage'];
//calculate cgst and sgst

                          if($allTax['GST_Type']=="CGST"){
                            $per=$allTax['Tax_percentage'];
                             $cgstPer=$allTax['Tax_percentage'];
                             $cgstAmount1=($bill->handling_amount* $cgstPer)/100;

                          } 
                           if($allTax['GST_Type']=="UT/SGST"){
                             $sgstPer=$allTax['Tax_percentage'];
                             $sgstAmt=($bill->handling_amount* $sgstPer)/100;


                          } 



                        }else{

                         
//calculate igst

                          if($allTax['GST_Type']=="IGST"){

                            $per=$allTax['Tax_percentage'];
                             $igstPer=$allTax['Tax_percentage'];
                             $igstAmt=($bill->handling_amount* $igstPer)/100;

                          } 


                        }


                   }



                    $totalCgstAmount=$totalCgstAmount+$cgstAmount1;
                    $totalSgstAmt=$totalSgstAmt+$sgstAmt;  
                    $totalIgstAmt=$totalIgstAmt+$igstAmt; 
                    $totalSpcesstAmt=$totalSpcesstAmt+$spcessAmt;
                    /* $totalVatAmt=$totalVatAmt+$vatAmt; */



                        ?>
                  @if($bill->handling_amount!="0.00"||$bill->handling_amount!="0")

                    <tr> 
                          
                           <td>@if($bill->type==1) VAT @else GST @endif</td>
                            <td>{{$bill->invoice_no}}</td>
                            <td>{{date('d/m/y',strtotime($bill->invoiceDate))}}</td>
                            <td>{{$bill->customer_code}}</td>
                          <td>{{$bill->party->company_name}}</td>
                      
                        <td>{{$bill->party->gst_no}}</td>
                       
                        <td>{{$bill->party->getstate->name}}</td>
                        <td style="text-align: right;">{{bcadd($bill->grand_total_amount, 0, 2)}} </td>
                          <td  style="text-align: right;">0.00</td>
                          <td style="text-align: right;">0.00</td>

                           <td style="text-align: right;">{{bcadd($cgstPer,0,2)}}</td>
                          <td style="text-align: right;">{{bcadd($cgstAmount1,0,2)}}</td>
                         <td style="text-align: right;" >{{bcadd($sgstPer,0,2)}}</td>
                         <td style="text-align: right;">{{bcadd($sgstAmt ,0, 2)}}</td>
                         <td style="text-align: right;">{{bcadd($igstPer,0,2)}}</td>
                         <td style="text-align: right;">{{bcadd($igstAmt, 0, 2)}}</td>
                          <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                          
                          <td style="text-align: right;">0.00</td>
                         <td style="text-align: right;">0.00</td>
                         
                          <td class="ClassHandlingAmount" style="text-align: right;">{{bcadd($bill->handling_amount, 0, 2)}}</td>
                          <td class="ClassDiscount" style="text-align: right;">{{bcadd($bill->discount, 0, 2)}}</td>
                          <td Class="ClassRoundof" style="text-align: right;">{{bcadd($bill->rounded_of, 0, 2)}} </td>
                           
                    </tr>
                    @endif
                
          @endforeach
              </tbody>
              

                       <tfoot border:solid 1px>
                        <tr>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;</td>
                          <td>&nbsp;&nbsp;</td>
                          <td>&nbsp;&nbsp; </td>
                           <td>&nbsp;&nbsp;</td>
                            <td>&nbsp;&nbsp;</td>
                          <td>&nbsp;&nbsp;</td>
                         
                          <td>Grand Total : </td>
                           <td  style="text-align: right;">{{bcadd($totalFinalAmount, 0, 2)}}</td>
                          
                           <td style="text-align: right;">{{bcadd($totalTaxableValue, 0, 2)}}</td>
                             <td  style="text-align: right;">0.00</td>

                           <td  style="text-align: right;"></td>
                           <td style="text-align: right;">{{bcadd($totalCgstAmount, 0, 2)}}</td>
                            <td  style="text-align: right;"></td>
                           <td style="text-align: right;"> {{bcadd($totalSgstAmt, 0, 2)}}</td>
                            <td  style="text-align: right;"></td>
                           <td style="text-align: right;">{{bcadd($totalIgstAmt, 0, 2)}}</td>
                           
                            <td  style="text-align: right;"></td>
                           <td style="text-align: right;"> {{bcadd($totalSpcesstAmt, 0, 2)}}</td>
                            <td  style="text-align: right;"></td>
                           <td style="text-align: right;"> {{bcadd($totalVatAmt, 0, 2)}}</td>
                          
                           <td  style="text-align: right;">{{bcadd($totalHandlingCharges, 0, 2)}}</td>
                          <td style="text-align: right;">{{bcadd($totalDiscount, 0, 2)}}</td>
                          <td  style="text-align: right;">{{bcadd($totalRoundOff, 0, 2)}}</td>
                        </tr>
    

  </tfoot>

            </table>

             <p style="float:left;width: auto;">Record Count : {{count($bills)}}</p>
           
          </div>
        </div>
         @else
            <p  class="h3" style="text-align: center;padding: 25px;background: #fff;">No record found </p>
          @endif
      </div>
       @endif
      </div>
  @endsection
  @section('script')

      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

  <script type="text/javascript">
   


  jQuery(function(){
      var enddated=new Date();
      var todate=new Date();
        var app={
                 
                init:function(){
                 
                  
                  app.fromDate();
                  app.to_date();
               
                },
                fromDate:function(){
                  
                  jQuery("#fromdate").datepicker({ 
                          autoclose: true, 
                          todayHighlight: true,
                           endDate: new Date(),
                          defaultDate:'dd/mm/yyy',
                          format: 'dd/mm/yyyy',
                    }).on('changeDate', function(e) {
                       jQuery("#to_date").val('');
                       enddated=jQuery(this).val();
                       todate=new Date();
                       app.to_date();
                      });
                },
                to_date:function(){
                    
                    jQuery("#to_date").datepicker({ 
                          autoclose: true, 
                          todayHighlight: true,
                           startDate:enddated,
                          
                          defaultDate:'dd/mm/yyy',
                          format: 'dd/mm/yyyy',
                    }).datepicker('setStartDate',enddated).datepicker('setEndDate',todate);

                },

        };

        app.init();
  });

  </script>

  @endsection