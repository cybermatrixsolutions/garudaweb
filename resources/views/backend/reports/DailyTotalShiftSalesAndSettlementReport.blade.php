@extends('backend-includes.app')

@section('content')
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link href="css/style.css" type="text/css" rel="stylesheet">

<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<!-- <script src="js/main.js"></script>  -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 

<style>
button.dt-button, div.dt-button, a.dt-button {
    
    background-image: linear-gradient(to bottom, #00786a 0%, #00786a 100%)!important;
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,StartColorStr='white', EndColorStr='#00786a');
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    text-decoration: none;
    outline: none;
    color:white!important;
    }
    .dt-buttons {
    float: right!important;
}

.dataTables_wrapper .dataTables_filter {
float: left;
text-align: left;
}
</style>



<div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Daily Total Shift Sales & Settlement Report
 </h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          
         
          <li><a href="#">Daily Total Shift Sales & Settlement Report
 </a></li>
        </ul>
      </div>
    </div>
    <?php

    $fromdate=date('d/m/Y');
    $to_date=date('d/m/Y');
    if(isset($_GET['to_date'])){
      $to_date=$_GET['to_date'];
    }

    if(isset($_GET['fromdate'])){
      $fromdate=$_GET['fromdate'];
    }

    ?>
     <div class="row">
        <div class="col-md-12" style="margin-top: 40px;;">
            <form class="form-horizontal form-inline" name="myForm" action="{{URL('DailyTotalShiftSalesAndSettlementReport')}}" enctype="multipart/form-data" method="get">
  
                     {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="form-group">
                          <label class="control-label"  for="from">From <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                           <input type="text"  @if(isset($_GET['fromdate'])) value="{{$_GET['fromdate']}}" @endif class="form-control" id="fromdate" name="fromdate" placeholder="from date" required/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                          <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                            <input  type="text" @if(isset($_GET['to_date'])) value="{{$_GET['to_date']}}" @endif class="form-control" id="to_date" name="to_date" placeholder="To Date" required/>&nbsp;&nbsp;
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default" id="checkdate" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                        </div>

                    </div>
                </div>
            </form> 
        </div>

    </div>

    <div class="row" >

     @if(isset($readings))
  
       @if($readings->count()>0)
        <div class="col-md-9">
            </div>
        <!--  <div class="col-md-3" >
            <a href="{{route('reading-report')}}/download?fromdate={{$fromdate}}&to_date={{$to_date}}" class="btn btn-default" style="float:right;width: auto;">Export in csv</a>
        </div> -->
      <div class="col-md-12">
        <div class="table-responsive" style="min-height: 400px;">
       
          <table class="table table-striped" id="myTable">
            <thead>
              
              <tr>
                <th>Shift Date</th>
                <th>Item</th>
                <th>Gross Qty</th>
                <th>Test Qty</th>
                <th>Nett Sale</th>
                <th>Rate</th>
                <th>Nett Sale Val</th>
                <th>Tot Val</th>

                @foreach($paymentDataFinal as $data)

                    <th>{{$data->name}}</th>


                @endforeach
                  <th>Total Settlement</th>
                <th>Diff</th>

                <!-- <th>Total Settlement</th>
                <th>Diff</th> -->
            </tr>          
            </thead>
            <tbody>
              <?php $j=1;

              
 $gross=0;

              ?>
 <?php $j=1; ?>

              @foreach($readings as $reading1)



               @foreach($reading1 as $reading)
                 

              

             
              
                  <?php 

                  $gross=$reading->Nozzle_End-$reading->Nozzle_Start;?>
                   <tr>
                    <td >{{$reading->created_date,""}}</td>
                    <td>{{$reading->itemName}} </td>
                    <td  style="text-align: right;">{{bcadd($gross,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($reading->test,0,3)}}</td>
                    <td  style="text-align: right;">{{bcadd($reading->reading,0,3)}}</td>
                    <td style="text-align: right;">{{$reading->price,""}}</td>
                    <td style="text-align: right;">{{bcadd($reading->totalSales,0,2)}}</td>
                    <td style="text-align: right;">{{bcadd($reading->totalSales,0,2)}}</td>

                @foreach($reading->payment_sales as $data)

                    <td style="text-align: right;">{{bcadd($data->total_sales,0,2)}}</td>

                    
                @endforeach

                <td style="text-align: right;">{{bcadd($reading->totalAmount,0,2)}}</td>
                    <td style="text-align: right;">{{bcadd($reading->totalDiff,0,2)}}</td>


                  </tr>
                

                     @endforeach
               
              @endforeach
  
            </tbody>
            <tfoot align="right">
                  <!--   <td>&nbsp;&nbsp;</td> -->
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                     @foreach($paymentDataFinal as $data)

                        <td>&nbsp;&nbsp;</td>


                      @endforeach
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <!-- <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                      <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td> -->
              </tfoot>
          </table>
         
        </div>
      </div>
       @else
          <p  class="h3" style="text-align: center;padding: 25px;background: #fff;">No record found </p>
        @endif
    </div>
    @endif
    </div>
@endsection
@section('script')

<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>




<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">
  
  $(document).ready(function() {
       $('#myTable').DataTable( {
         "dom": 'lBfr',
          "processing": true,
         buttons: [
           
            { extend: 'excelHtml5',text: 'Export in CSV', footer: true , filename: ''+Date('Y-m-d')+'-DailyTotalShiftSales&SettlementReport'},
            
        ],  paging: false,



      "footerCallback": function ( row, data, start, end, display ) {


          var api = this.api(), data;
 
            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // computing column Total of the complete result 
             /*   var grossTotal = api
                .column( 5 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );*/
        
      var testTotal = api
                .column( 6 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
        
      var qtyTotal = api
                .column( 7 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
        
      
/*
      var withVatTotal = api
                .column( 13 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );         

          var vatValTotal = api
                .column( 14 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                

                
              var totalValTotal = api
                .column( 15, { page: 'current'}  )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );                 
      
        
            // Update footer by showing the total with the reference of the column index 
      $( api.column( 4).footer() ).html('Total : ');
            $( api.column( 5 , { page: 'current'} ).footer() ).html(grossTotal.toFixed(3) );*/
          /*  $( api.column( 6, { page: 'current'} ).footer() ).html(testTotal.toFixed(3));
            $( api.column( 7 , { page: 'current'} ).footer() ).html(qtyTotal.toFixed(3));*/
            
           /* $( api.column( 13, { page: 'current'}  ).footer() ).html(withVatTotal.toFixed(2));
            $( api.column( 14 , { page: 'current'} ).footer() ).html(vatValTotal.toFixed(2));
            $( api.column( 15 , { page: 'current'} ).footer() ).html(totalValTotal.toFixed(2));
       
          */
           },
       
            
       
    });
});


jQuery(function(){
    var enddated=new Date();
    var todate=new Date();
    var enddatedAfterDay=new Date();
      var app={
               
              init:function(){
               
                
                app.fromDate();
                app.to_date();
             
              },
              fromDate:function(){
                
                jQuery("#fromdate").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         endDate: new Date(),
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     jQuery("#to_date").val('');
                      todate=new Date();
                     enddated=jQuery(this).val();
                    /* enddatedAfterDay = jQuery(this).val();
                     enddatedAfterDay.setDate(enddated.getDate() + 30);
                      alert(enddatedAfterDay);
                  var date2 = $('#fromdate').datepicker('getDate', '+30d'); 


*/
                    var enddatedAfterDay = $('#fromdate').datepicker('getDate');
                    enddatedAfterDay.setDate(enddatedAfterDay.getDate()+15)

                     app.to_date();
                    });
              },
              to_date:function(){

                 var enddatedAfterDay = $('#fromdate').datepicker('getDate');
                    enddatedAfterDay.setDate(enddatedAfterDay.getDate()+15);
              
                  
                  jQuery("#to_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         startDate:enddated,
                         
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).datepicker('setStartDate',enddated).datepicker('setEndDate',enddatedAfterDay);

                    /* alert(enddatedAfterDay);
                     alert(enddated);*/
              },

      };

      app.init();
});

</script>
@endsection