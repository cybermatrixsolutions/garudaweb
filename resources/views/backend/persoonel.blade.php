@extends('backend-includes.app')

@section('content')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Personnel & Staff</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#"> Personnel & Staff</a></li>
        </ul>
      </div>
    </div>
     <div class="">
      
      <ul>
        @foreach($errors->all() as $error)
          <li style="color: red">{{ $error}}</li>
        @endforeach
      </ul>
     
      
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
           <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
               <div class="">
      
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      
    </div>

            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document" style=" width: 1100px;">
            
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Personnel & Staff</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal" action="{{url('save_personal_management')}}" method="post" onsubmit="dobcheck()" enctype="multipart/form-data">
                         {{ csrf_field() }}
                         <div class="col-md-6">
                          <input type="hidden" name="personal_ro_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">

                        <!-- <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Pump Legal Name</label>
                          <div class="col-sm-8">
                             <select class="form-control" required="required" name="personal_ro_code" id="sel1">
                             
                                                    @foreach($datas as $ro_master)
                                                        <option name="company_id" value="{{$ro_master->RO_code}}"
                                                                >{{$ro_master->pump_legal_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Personal Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="text" required="required" class="form-control" name="personl_name" id="" placeholder="Personal Name">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Gender <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                           <select class="form-control" required="required" name="gender">
                             <option value="Male">Male</option>
                             <option value="Female">Female</option>
                           </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Role <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <select class="form-control" required="required" name="designation" id="designation">
                             
                                                    @foreach($designation as $desig)
                                                        <option data-pre="{{$desig->Reporting_To_Designation}}" value="{{$desig->id}}"
                                                                >{{$desig->Designation_Name}}</option>
                                                    @endforeach
                            </select>
                        </div>
                      </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Reporting To <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-4">
                            
                             <select class="form-control" required="required" name="reporting_to" id="reporting_to">
                                                   <option data-pre="0" value="0"
                                                                >No One</option>
                                                    @foreach($personal_list as $personnel)
                                                        <option data-pre="{{$personnel->Designation}}" value="{{$personnel->id}}"
                                                                >{{$personnel->Personnel_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Date of Birth </label>
                          <div class="col-sm-8"><!--datepickerabh-->
                              <input type="text" id="datepickerabh" class="form-control "  name="d_o_b" id="" placeholder="Date of Birth">

                          </div>
                        </div>
                       
                      </div>
                       <div class="col-md-6 col-sm-6 col-xs-12 vat">
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Date of Appointment <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8"><!--datepickerapp-->
                            <input type="text" id="datepickers"  class="form-control " value="01/04/2017" onkeypress="return false;" required="required" name="date_of_appointment" id="" placeholder="Date of Appointment">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Mobile(+91) <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="text"  data-table="tbl_personnel_master"  data-colum="Mobile" data-mass="Mobile No. Already Exist" class="form-control checkunique number" required="required" name="mobile" id="mobile" maxlength="10" placeholder="Mobile">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" >Aadhaar No </label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" name="aadhaar" id="aadhaar_no" placeholder="Aadhaar No">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Aadhaar Photo </label>
                          <div class="col-sm-8">
                            <input type="file"  class="form-control" name="aadhaar_img" id="aadhaar_img" placeholder="Aadhaar No">
                          </div>
                        </div>


                        
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Email/Login ID <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="email" data-table="tbl_personnel_master"  data-colum="Email" data-mass="Email Id Already Exist" required="required" class="form-control checkunique" name="email" placeholder="Email">
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Password <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="Password" required="required" id="myInput" class="form-control" name="password" placeholder="Password"><input type="checkbox" onclick="myFunction()">Show Password
                          </div>
                        </div>
                        </div><hr>
                        <!--  <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Personal Code</label>
                          <div class="col-sm-8">
                            <input type="text" required="required" class="form-control" name="Personnel_Code" placeholder="Personnel Code">
                          </div>
                        </div> -->
                        <div class="form-group">
                          <div class="col-sm-offset-9 col-sm-3">
                            <button type="submit" class="btn btn-default submit-button" id="id-submit-button" onclick="return gstAndPan()"  disabled="disabled">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">

      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
             @if(isset($personal_list) && count($personal_list)  != 0)
            <thead>
              <tr>
                <th>S.No</th>
               <!--  <th>Outlet Name</th> -->
                <th>Personal Name</th>
                <th>Role</th>
                <th>Mobile No.</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=0;?>
               @foreach($personal_list as $list)
               <?php $i++;?>
              <tr>
                <td>{{$i}}</td> 
               <!--  <td scope="row">{{$list->pump_name}}</td> -->
                <td scope="row">{{$list->Personnel_Name}}</td>
                <td scope="row">{{$list->Designation_Name}}</td>
                <td scope="row">{{$list->Mobile}}</td>
                <td> <?php if($list->is_active==1){?>

                   <a  title="Active" href="personalmanagement/active/{{$list->id}}/0" onclick="return confirm('Do you want to deactivate?');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="personalmanagement/active/{{$list->id}}/1" onclick="return confirm('Do you want to activate?');">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
          </td>
               
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      @if($list->is_active==1)
                      <li><a href="persoonel/edit/{{$list->id}}">Edit</a></li>

                      @endif
                      <li><a href="persoonel/view/{{$list->id}}">View</a></li>
                     
                     <!-- <li><a href="personal_delete_page/{{$list->id}}" onclick="return confirm('are you sure to delete?'); ">Delete</a></li>-->
                    </ul>
                  </div></td>
              </tr>
             @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
  function  gstAndPan() {

        var mobile = document.getElementById("mobile").value;
        var pattern = /^[0]?[789]\d{9}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);

    
        return confirm("Do you want to Continue? ");
    

            return true;
        }
        alert("It is not valid mobile number.input 10 digits number!");

        return false;
    }
  function myFunction() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
  $(function () {
  $("#datepickerabh").datepicker({ 
        
       
        format: 'dd/mm/yyyy',
         endDate: '+2d',
        autoclose: true

  })
  $("#datepickers").datepicker({ 
        
       
        format: 'dd/mm/yyyy',
       

  })
  $("#datepickerapp").datepicker({ 
        
       
        format: 'dd/mm/yyyy',
        startDate: '-0m',

        autoclose: true

  })
  $("#datepickerinact").datepicker({ 
        
       
        format: 'dd/mm/yyyy',
        startDate: '-0m',

        autoclose: true

  })
});
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
             vil.checkdesignation();

             jQuery('#ToDate').on('blur',function(){
                 vil.CalculateDiff();
             });
               vil.checkdesignation();
              jQuery('#designation').on('change',function(){
                  vil.checkdesignation();
              });
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   //id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique',{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
           CalculateDiff:function() {
 
                    if(jQuery("#FromDate").val()!="" && jQuery("#ToDate").val()!=""){
                       
                      /*var From_date = new Date(jQuery("#FromDate").val());
                      var To_date = new Date(jQuery("#ToDate").val());
                      var diff_date =  To_date - From_date;
                       
                      var years = Math.floor(diff_date/31536000000);
                      var months = Math.floor((diff_date % 31536000000)/2628000000);
                      var days = Math.floor(((diff_date % 31536000000) % 2628000000)/86400000);
                      
                        if(months<18){
                           alert('Age is not Less than 18 Years');
                        }*/

                      }
                      else{
                          alert("Please select dates");
                          return false;
                      }
          },
          checkdesignation:function(){
           
              var pre=jQuery('#designation  option:selected').data('pre');
              console.log(pre);
              var d=0;
              jQuery('#reporting_to').children('option').each(function(i,v){
                      console.log(jQuery(this).data('pre'));
                      if(jQuery(this).data('pre')!=pre){
                          jQuery(this).hide();
                      }else{
                         if(d==0){
                            d=jQuery(this).val();
                         }
                         jQuery(this).show();
                      }
              });
               
              //jQuery('#reporting_to option[value="0"').show();
              jQuery('#reporting_to option[value="'+d+'"').prop('selected', 'selected').change();
           },
          
     }
     vil.init();
  });


</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();

             $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
    });

 
</script>
@endsection


