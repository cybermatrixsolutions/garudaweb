<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Driver  Update</h1>
      </div>
      <div>
        <ul class="breadcrumb">
           <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('driver')}}">Driver</a></li>
        </ul>
      </div>
    </div>
	
	
	                         <div class="">  
                             </div>
	
	
	
	
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           
            <div class="row" style="margin-top: -15px !important;">
             @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                      <form class="form-horizontal" action="{{url('customer_driver_update')}}/{{$customber_driver_data->id}}" enctype="multipart/form-data" method="post"  onsubmit="return confirm('Do you want to Continue?');">
            
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="customer_driver_ro_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
                    <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Pudmp Legal Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="customer_driver_ro_code" id="sel1">
                             
                                                    @foreach($datas as $ro_master)
                                                        <option name="company_id" value="{{$ro_master->RO_code}}"
                                                                @if($customber_driver_data->Ro_code == $ro_master->RO_code) selected @endif >{{$ro_master->pump_legal_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div> -->
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Customer Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control" required="required" name="customer_code" id="customer_code" data-customer="{{$customber_driver_data->customer_code}}" value="{{$customber_driver_data->customer_code}}">
                              
                                                   
                            </select>
                          </div>
                          </div>
                       
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Registration Number  </label>
                          <div class="col-sm-9">
                            <select class="form-control"  name="Registration_Number" id="registration_number" data-registration="{{$customber_driver_data->Registration_Number}}">
                              
                                                   
                            </select>

                             </div>
                        </div>
                        
                            <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Driver Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" required="required" name="driver_name" id="" value="{{$customber_driver_data->Driver_Name}}" placeholder="Driver Name">
                          </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-3" for="pwd">Gender <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control" required="required" name="gender">
                             <option value="Male"  @if($customber_driver_data->gender == "Male") selected @endif>Male</option>
                             <option value="Female" @if($customber_driver_data->gender == "Female") selected @endif>Female</option>
                           </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Email </label>
                          <div class="col-sm-9">
                             <input type="email" class="form-control"  name="email"   value="{{$customber_driver_data->email}}" placeholder="Email">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Licence No.</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control"  name="licence_no" value="{{$customber_driver_data->Driver_Licence_No}}" placeholder="Phone Number">
                          </div>
                        </div>
                         
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Valid Upto</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control"  @if($customber_driver_data->valid_upto!=null) value="{{date('d/m/Y',strtotime($customber_driver_data->valid_upto))}}" @endif name="valid_upto" id="datepicker" placeholder="Valid Upto">

                          </div>
                        </div>
                       
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Mobile(+91) <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" maxlength="10" class="form-control number" required="required" name="Driver_Mobile" id="mobile" value="{{$customber_driver_data->Driver_Mobile}}" placeholder="Mobile ">
                          </div> 
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Aadhaar No </label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="aadhaar_no" value="{{$customber_driver_data->Aadhaar_no}}"" id="aadhaar_no" placeholder="Aadhaar No">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Aadhaar Photo </label>
                          <div class="col-sm-9">
                            <input type="file"  class="form-control" name="aadhaar_img" id="aadhaar_img" placeholder="Aadhaar photo">
                            <br><img src="{{URL::asset('')}}/{{$customber_driver_data->Aadhaar_img}}" style="width: 250px; height: 150px;">
                          </div>
                        </div>
                        
                        <div class="form-group">
            
                          <div class="col-sm-offset-3 col-sm-9">
                           <!-- <button type="submit" class="btn btn-default">Submit</button>-->
              <center><input type="submit" id="success-btn" class="btn btn-primary site-btn" value="Submit" onclick="return getmobilevalid()"></center>
                          </div>
                        </div>
           
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>
	
	
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script type="text/javascript">
  $(function () {
  $("#datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
       
         format: 'dd/mm/yyyy'
  });
});
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
function  getmobilevalid() {
        var mobile = document.getElementById("mobile").value;
        var pattern = /^[0]?[789]\d{9}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
            return true;
        }
        alert("It is not valid mobile number.input 10 digits number!");
        return false;
    }
    $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
</script>



<script type="text/javascript">
  jQuery(function(){
        var url1="{{url('getrocode')}}";
        var url2="{{url('getregierids')}}";
     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
               jQuery.get(url1,{
                rocode:jQuery('#sel1').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){
                   var customer=jQuery('#customer_code').data('customer');
                     if(customer==index)
                        opt+='<option selected value="'+index+'">'+value+'</option>';
                      else
                      opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);
                   vil.getcrg();
               });
           },
           getcrg:function(){

             jQuery.get(url2,{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var registration=jQuery('#registration_number').data('registration');
                 var opt='';
                  opt+='<option  selected>--Select Registration Number--</option>';
                  jQuery.each(data, function(index,value){

                     if(registration==value)
                      
                        opt+='<option selected value="'+value+'">'+value+'</option>';
                      else
                        
                      opt+='<option value="'+value+'">'+value+'</option>';

                  });
                  
                   console.log(registration);
                jQuery('#registration_number').html(opt);
               });
           },

     }
     vil.init();
  });
</script>

<!-- <script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#sel1').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
               });
           },

     }
     vil.init();
  });
</script>

<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   //id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique',{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script> -->
</body>
</html>