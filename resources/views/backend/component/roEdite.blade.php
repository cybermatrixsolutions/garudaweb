 <form class="form-horizontal" action="{{url('retail_edit')}}/{{$row->id}}" enctype="multipart/form-data" method="post">
                         <input type="hidden" name="_token" value="{{ csrf_token()}}"/> 

                        <div id="tabs-n">
                            <div class="row">
                                <div class="col-md-12">
                                  <ul>
                                    <li><a style="pointer-events: none;" href="#tabs-1">Info</a></li>
                                    <li><a style="pointer-events: none;" href="#tabs-2">Configuration</a></li>
                                     <li><a style="pointer-events: none;" href="#tabs-3">Bank Details and T&C </a></li>
                                     &nbsp;&nbsp;&nbsp;
                                     <li style="background-color: #403333;margin-left: 91px;border-radius: 8px 8px 8px;padding: 2px;"><input type="text" readonly name="" id="copyprincple"></li>
                                     &nbsp;&nbsp;
                                     <li style="background-color: #403333;border-radius: 8px 8px 8px;padding: 2px;"><input type="text" readonly name="" style="width: 276px;" id="copycompany"></li>
                                  </ul>
                                </div>
                           <div id="tabs-1">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Principal Company </label>
                                    <div class="col-sm-8">
                                      
                                       <select class="form-control" id="printpp" name="company_code" required/>
                                          @foreach($getPrincipal as $row1) 
                                        <option value="{{$row1->company_code}}" @if($row->company_code == $row1->company_code) selected @endif>{{$row1->company_name}}</option>
                                        
                                         @endforeach
                                       </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Outlet Name <span style="color:#f70b0b;">*</span></label>
                                    <div class="col-sm-8">
                                      <input type="text" class="form-control" id="Outlet" name="pump_legal_name" value="{{$row->pump_legal_name}}" placeholder="Pump Legal Name" required/>
                                    </div>
                                  </div>
                                    <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Select Outlet Type <span style="color:#f70b0b;">*</span></label>
                                    <div class="col-sm-8">
                                      <select class="form-control" name="Ro_type">
                                        <option  @if($row->Ro_type == 1) selected @endif value="1">Petrol Pump</option>
                                      <option  @if($row->Ro_type == 2) selected @endif value="2">Other Retail</option>
                                      
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Outlet Address 1 <span style="color:#f70b0b;">*</span></label>
                                    <div class="col-sm-8">
                                      <textarea class="form-control" rows="1"  id="comment" name="pump_address"  required/>{{$row->pump_address}}</textarea>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Outlet Address 2</label>
                                    <div class="col-sm-8">
                                      <textarea class="form-control" rows="1"  id="comment" name="address_2"  />{{$row->address_2}}</textarea>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Outlet Address 3</label>
                                    <div class="col-sm-8">
                                      <textarea class="form-control" rows="1"  id="comment" name="address_3"  />{{$row->address_3}}</textarea>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Country</label>
                                    <div class="col-sm-8">
                                       <select class="form-control" disabled="disabled" id="country" name="country"  value="{{$row->country}}">
                                         @foreach($countries_list as $country)
                                            <option @if($country->id==101) selected @endif  value="{{$country->id}}"
                                                    >{{$country->name}}</option>
                                        @endforeach
                                       </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">State <span style="color:#f70b0b;">*</span></label>
                                    <div class="col-sm-8">
                                       <select class="form-control" id="state" name="state"  data-value="{{$row->state}}" >
                                         
                                       </select>
                                    </div>
                                  </div>
                                   <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">City</label>
                                    <div class="col-sm-8">
                                       <select class="form-control" id="city" name="city"  data-value="{{$row->city}}" >
                                         
                                       </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">PIN Code <span style="color:#f70b0b;">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control number" id="" name="pin_code"  title="Five digit zip code" placeholder="pin code" value="{{$row->pin_code}}" required/>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Phone Number</label>
                                    <div class="col-sm-8">
                                      <input type="text" class="form-control" id="" name="phone_no" value="{{$row->phone_no}}" placeholder="Phone Number" />
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label class="control-label col-sm-4 " for="pwd">Mobile(+91) <span style="color:#f70b0b;">*</span></label>
                                    <div class="col-sm-8">
                                      <input type="text" data-table="tbl_ro_master" id="mobile" data-colum="mobile" data-mass="Mobile No. Already Exit" data-id="{{$row->id}}" class="form-control checkunique number" id="mobile" maxlength="10" name="mobile" value="{{$row->mobile}}" placeholder="Mobile" required/>
                                    </div>
                                  </div>
                                    <div class="form-group">
                                      <label class="control-label col-sm-4" for="pwd">Email as login ID <span style="color:#f70b0b;">*</span></label>
                                      <div class="col-sm-8">
                                        <input type="email" readonly data-table="tbl_ro_master" data-colum="Email" data-mass="Email Id Already Exit" data-id="{{$row->id}}" class="form-control checkunique" id="email" name="Email" value="{{$row->Email}}" placeholder="Email" required/>
                                      </div>
                                    </div>
                                    <h4> RO Principal (Owner) Contact Person</h4>
                                         <hr/>
                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd"> Name <span style="color:#f70b0b;">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control " name="Contact_Person_Name" placeholder="Contact Person Name" @if($row->getdetails!=null) value="{{$row->getdetails->Contact_Person_Name}}" @endif required/>
                                            </div>
                                         </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd"> Email <span style="color:#f70b0b;">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="email" class="form-control" id="emailcody" name="Contact_Person_Email" placeholder="Email" @if($row->getdetails!=null) value="{{$row->getdetails->Contact_Person_Email}}" @endif required/>
                                            </div>
                                         </div>

                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd">Password <span style="color:#f70b0b;">*</span> </label>
                                            <div class="col-sm-7">
                                                <input type="password" class="form-control" id="myInput" name="password" placeholder="password" ><input type="checkbox" onclick="myFunction('myInput')">Show Password
                                            </div>
                                          </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd"> Mobile <span style="color:#f70b0b;">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control number" id="copymobile" maxlength="10" name="Contact_Person_Mobile"  @if($row->getdetails!=null) value="{{$row->getdetails->Contact_Person_Mobile}}" @endif placeholder="Contact Person Mobile" required/>
                                            </div>
                                         </div>

                                         <h4>RO Secondary Contact Person</h4>
                                         <hr/>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd"> Name <span style="color:#f70b0b;">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control " name="Secondary_Person_Name" @if($row->getdetails!=null) value="{{$row->getdetails->Secondary_Person_Name}}" @endif placeholder="Contact Person Name" required/>
                                            </div>
                                         </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd"> Email <span style="color:#f70b0b;">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="email" class="form-control " name="Secondary_Person_Email" @if($row->getdetails!=null) value="{{$row->getdetails->Secondary_Person_Email}}" @endif placeholder="Email" required/>
                                            </div>
                                         </div>
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd">Password <span style="color:#f70b0b;">*</span> </label>
                                            <div class="col-sm-7">
                                                <input type="password" class="form-control" id="myInputSecondary" name="passwordSecondary" placeholder="password" ><input type="checkbox" onclick="myFunction('myInputSecondary')">Show Password
                                            </div>
                                          </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd"> Mobile <span style="color:#f70b0b;">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control number" maxlength="10" name="Secondary_Person_Mobile" @if($row->getdetails!=null) value="{{$row->getdetails->Secondary_Person_Mobile}}" @endif placeholder="Contact Person Mobile" required/>
                                            </div>
                                         </div>

                                   <!--end col 6-->
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <!--<button type="submit" class="btn btn-default">Submit</button>-->
                                        <center><input type="button" class="btn btn-primary" value="NEXT" id="next_1" data-id="1"></center>
                                    </div>

                                </div>
                            <!--end tab 1-->
                           </div>
                           <div id="tabs-2">
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-sm-4" for="website">Customer Care No.</label>
                                  <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{$row->customer_care}}" id="website" name="customer_care" placeholder="Customer Care No." />
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="control-label col-sm-4" for="website">Website</label>
                                  <div class="col-sm-8">
                                    <input type="text"  class="form-control" value="{{$row->website}}" id="website" name="website" placeholder="Website" />
                                  </div>
                                </div>
                                
                              
                                <div class="form-group">
                                  <label class="control-label col-sm-4" for="pwd">Lube License </label>
                                  <div class="col-sm-8">
                                    <input type="text" class="form-control" id=""  name="Lube_License" value="{{$row->Lube_License}}" placeholder="Lube License" />
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-4" for="pwd">Outlet License </label>
                                  <div class="col-sm-8">
                                    <input type="text" class="form-control" id="" name="Pump_License" value="{{$row->Pump_License}}" placeholder="Pump License" />
                                  </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5"></div>
                                    <div class="col-md-3">Start from</div>
                                    <div class="col-md-2">Serial no. Width</div>
                                    <div class="col-md-2">Reset FY in</div>
                                </div>
                                 <div class="row leftln-rigstln">
                                  <div class="col-md-5">
                                       <div class="form-group">
                                    
                                        <label class="control-label col-sm-7" for="GST Invoice Prefix" >GST Invoice Prefix <span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                          <input type="text" @if($row->confirm==1) readonly @endif required minlength="1" maxlength="4" class="form-control" id="invoice_prefix" value="{{$row->GST_prefix}}"  name="GST_prefix"  placeholder="GST Invoice Prefix" required/>
                                       
                                      </div>
                                       </div>
                                  </div>
                                    <div class="col-md-3">
                                          <div class="form-group">
                                       <input type="text" required maxlength="8"   class="form-control quantity leftln number" @if($row->getdetails!=null) value="{{$row->GST_serial}}"  @endif  name="GST_serial"  placeholder="Starting No." required/>
                                  </div>
                                   </div>
                                    <div class="col-md-2">
                                          <div class="form-group">
                                     
                                        <select class="form-control rigstln" name="GST_serial_lenght" @if($row->confirm==1) readonly @endif >
                                          <option value="3"  @if($row->getdetails!=null && $row->getdetails->GST_serial_lenght == "3") selected @endif>3</option>
                                          <option value="4"  @if($row->getdetails!=null && $row->getdetails->GST_serial_lenght == "4") selected @endif >4</option>
                                          <option value="5"  @if($row->getdetails!=null && $row->getdetails->GST_serial_lenght == "5") selected @endif >5</option>
                                          <option value="6"  @if($row->getdetails!=null && $row->getdetails->GST_serial_lenght == "6") selected @endif >6</option>
                                          <option value="7"  @if($row->getdetails!=null && $row->getdetails->GST_serial_lenght == "7") selected @endif >7</option>
                                           <option value="8"  @if($row->getdetails!=null && $row->getdetails->GST_serial_lenght == "8") selected @endif >8</option>
                                          </select>
                                   
                                    </div>
                                  </div>
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        
                                        <input @if($row->confirm==1) readonly @endif type="checkbox"   @if($row->getdetails!=null && $row->getdetails->GST_serial_fy ==1) checked @endif name="GST_serial_fy" value="1">
                                   </div>
                                  </div>
                             </div>
                              <div class="row leftln-rigstln">
                                   <div class="col-md-5">
                                        <div class="form-group">
                                     
                                        <label class="control-label col-sm-7" for="VAT Invoice Prefix">VAT Invoice Prefix <span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                          <input type="text" @if($row->confirm==1) readonly @endif  minlength="1" maxlength="4" class="form-control" id="VAT_prefix" value="{{$row->VAT_prefix}}"  name="VAT_prefix"  placeholder="VAT Invoice Prefix" required/>
                                         
                                       
                                      </div>
                                       </div>
                                   </div>
                                    <div class="col-md-3">
                                          <div class="form-group">
                                       <input type="text" required @if($row->confirm==1) readonly @endif  maxlength="8"   class="form-control quantity leftln number" name="VAT_serial" value="{{$row->VAT_serial}}"  placeholder="" required/>
                                  </div>
                                   </div>
                                    <div class="col-md-2">
                                       
                                       <div class="form-group">
                                        <select class="form-control rigstln" name="VAT_serial_lenght" @if($row->confirm==1) readonly @endif>
                                           <option value="3"  @if($row->getdetails!=null && $row->getdetails->VAT_serial_lenght == "3") selected @endif>3</option>
                                          <option value="4"  @if($row->getdetails!=null && $row->getdetails->VAT_serial_lenght == "4") selected @endif >4</option>
                                          <option value="5"  @if($row->getdetails!=null && $row->getdetails->VAT_serial_lenght == "5") selected @endif >5</option>
                                          <option value="6"  @if($row->getdetails!=null && $row->getdetails->VAT_serial_lenght == "6") selected @endif >6</option>
                                          <option value="7"  @if($row->getdetails!=null && $row->getdetails->VAT_serial_lenght == "7") selected @endif >7</option>
                                           <option value="8"  @if($row->getdetails!=null && $row->getdetails->VAT_serial_lenght == "8") selected @endif >8</option>
                           
                                    </select>
                                   
                                    </div>
                                  </div>
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        <input type="checkbox" @if($row->confirm==1) readonly @endif name="VAT_serial_fy" @if($row->getdetails!=null && $row->getdetails->VAT_serial_fy==1) checked @endif  value="1">
                                   </div>
                                  </div>
                              </div>

                               
                                       
                                       <div class="col-sm-6">
                                 <div class="form-group">
                                    <div class=" col-md-offset-4 col-md-4">
                                  </div>
                                    </div>
                                    </div>
                                 <!-- <div class="form-group">
                                    <div class="col-sm-7">
                                        <label class="control-label col-sm-7" for="GST Invoice Prefix" >Delivery Slip Prefix <span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="text" required minlength="1" maxlength="4" class="form-control" id="Delivery_Slip_Prefix"  name="Delivery_Slip_Prefix"  @if($row->getdetails!=null) value="{{$row->getdetails->Delivery_Slip_Prefix}}" @endif placeholder="Delivery Slip Prefix" required/>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <label class="control-label col-sm-7" for="Serial No.">Start from.<span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="number" readonly required  class="form-control" value="1" name="Delivery_serial"  @if($row->getdetails!=null) value="{{$row->getdetails->Delivery_serial}}" @endif placeholder="Starting No." required/>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                 </div>
                                <div class="form-group">
                                    <div class="col-sm-7">
                                        <label class="control-label col-sm-7" for="VAT Invoice Prefix">GST Invoice Slip Prefix<span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="text" required minlength="1" maxlength="4" class="form-control" id="GST_Slip_Prefix" @if($row->getdetails!=null) value="{{$row->getdetails->GST_Slip_Prefix}}" @endif  name="GST_Slip_Prefix" placeholder="GST Invoice Slip Prefix" required/>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <label class="control-label col-sm-7" for="Serial No.">Start from<span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="number" readonly required value="1" class="form-control"  name="GST_Slip_serial" @if($row->getdetails!=null) value="{{$row->getdetails->GST_Slip_serial}}" @endif placeholder="Serial No." required/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                      <div class="col-sm-7">
                                        <label class="control-label col-sm-7" for="GST Invoice Prefix" >GST Invoice Prefix <span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                          <input type="text" required minlength="4" maxlength="4" class="form-control" id="invoice_prefix"  name="GST_prefix" value="{{$row->GST_prefix}}" placeholder="GST Invoice Prefix" required/>
                                        </div>
                                      </div>
                                      <div class="col-sm-5">
                                        <label class="control-label col-sm-7" for="Serial No.">Serial No.</label>
                                        <div class="col-sm-5">
                                          <input type="number" required readonly  class="form-control" value="{{$row->GST_serial}}" name="GST_serial" placeholder="Serial No." required/>
                                        </div>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <div class="col-sm-7">
                                        <label class="control-label col-sm-7" for="VAT Invoice Prefix">VAT Invoice Prefix <span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                          <input type="text" required minlength="4" value="{{$row->VAT_prefix}}" maxlength="4" class="form-control" id="VAT_prefix"  name="VAT_prefix" placeholder="VAT Invoice Prefix" required/>
                                        </div>
                                      </div>
                                      <div class="col-sm-5">
                                        <label class="control-label col-sm-7" for="Serial No.">Serial No. <span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                          <input type="number" readonly required value="{{$row->VAT_serial}}" class="form-control"  name="VAT_serial" placeholder="Serial No." required/>
                                        </div>
                                      </div>
                                  </div>
                               </div> -->

                            <!--end col 6-->
                            </div>

                            <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="SAP Code">Principle  ERP code</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control"  name="SAP_Code" @if($row->getdetails!=null) value="{{$row->getdetails->SAP_Code}}" @endif placeholder="SAP Code" />
                                  </div>
                                  </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-4" for="pwd">VAT No.</label>
                                  <div class="col-sm-8">
                                    <input type="text" class="form-control" maxlength="15"  id="" name="VAT_TIN" value="{{$row->VAT_TIN}}" placeholder="VAT TIN" />
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label class="control-label col-sm-4" for="pwd">PAN </label>
                                  <div class="col-sm-8">
                                    <input type="text" style="text-transform: uppercase" class="form-control"  id="pan" name="PAN_No" value="{{$row->PAN_No}}" maxlength="10" placeholder="PAN No"/>
                                  </div>
                                  <input type="hidden" name="" value="" id="getstatecode" ng-bind="name">
                                </div>
                                
                                <!-- <div class="form-group">
                                  <label class="control-label col-sm-4" for="pwd">GSTIN</label>

                                  <div class="col-sm-8">
                                    <input type="text" style="text-transform: uppercase" class="form-control"  id="gst" name="GST_TIN" value="{{$row->GST_TIN}}" maxlength="15" placeholder="GST TIN"  style/>
                                  <span><input type="text" style="text-transform: uppercase" class="form-control"  id="gst" name="GST_TIN" value="{{$row->GST_TIN}}" maxlength="15" placeholder="GST TIN"/></span>
                                  </div>

                                </div> -->
                                  <div class="form-group">
                                      <label class="control-label col-sm-4" for="pwd">GSTIN </label>
                                      <div class="col-sm-8">
                 
                                           <input type="hidden"  value="{{$row->GST_TIN}}" class="form-control" id="getsta"  name="GST_TIN"  />
                                           
                                             <div class="col-sm-5" style="padding-right:0px;    width:49.666667%;text-transform: uppercase;">
                                               <input type="text"  readonly value="{{substr($row->GST_TIN,0,12)}}" class="form-control" id="views"   />
                                            </div>

                                           <div class="col-sm-4" style="padding-left:0px">
                                              <input type="text" value="{{substr($row->GST_TIN,12,15)}}" class="form-control GST_TIN" id="gst" name="GST_ddTIN" placeholder="GST No." maxlength="3" /><br>
                                          </div>

                                      </div>
                                    
                                  </div>
                                
                            <!--end col 6-->
                             <div class="row ">
                                    <div class="col-md-5"></div>
                                    <div class="col-md-3">Start from</div>
                                    <div class="col-md-2">Serial no. Width</div>
                                    <div class="col-md-2">Reset FY in</div>
                                </div>
                                 <div class="row leftln-rigstln">
                                    <div class="col-md-5">
                                  <div class="form-group">
                                        <label class="control-label col-sm-7" for="GST Invoice Prefix" >Delivery Slip Prefix <span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                            <input @if($row->confirm==1) readonly @endif type="text" required minlength="1" maxlength="4" class="form-control" id="Delivery_Slip_Prefix"  name="Delivery_Slip_Prefix"   placeholder="Delivery Slip Prefix" @if($row->getdetails!=null) value="{{$row->getdetails->Delivery_Slip_Prefix}}" @endif  required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                     <div class="form-group">
                                    <input type="text" @if($row->confirm==1) readonly @endif required maxlength="8"   class="form-control quantity leftln number"  name="Delivery_serial"  @if($row->getdetails!=null) value="{{$row->getdetails->Delivery_serial}}" @endif" required/>
                                            
                                           
                                </div>
                                </div>
                                <div class="col-md-2">
                                <div class="form-group">
                                    <select class="form-control rigstln" name="D_serial_no_lenght" @if($row->confirm==1) readonly @endif>
                                          <option value="3"  @if($row->getdetails!=null && $row->getdetails->D_serial_no_lenght == "3") selected @endif>3</option>
                                          <option value="4"  @if($row->getdetails!=null && $row->getdetails->D_serial_no_lenght == "4") selected @endif >4</option>
                                          <option value="5"  @if($row->getdetails!=null && $row->getdetails->D_serial_no_lenght == "5") selected @endif >5</option>
                                          <option value="6"  @if($row->getdetails!=null && $row->getdetails->D_serial_no_lenght == "6") selected @endif >6</option>
                                          <option value="7"  @if($row->getdetails!=null && $row->getdetails->D_serial_no_lenght == "7") selected @endif >7</option>
                                          <option value="8" @if($row->getdetails!=null && $row->getdetails->D_serial_no_lenght == "8") selected @endif >8</option>
                           
                                    </select><br>
                                         
                                </div>
                                 </div>
                                <div class="col-md-2">
                                     <div class="form-group">
                                          <input @if($row->confirm==1) readonly @endif type="checkbox" name="D_no_fy" @if($row->getdetails!=null && $row->getdetails->D_no_fy ==1) checked @endif value="1">
                    
                                </div>
                                     </div>
                            </div>
                              <div class="row">
                                  <div class="col-md-5">
                                      <div class="form-group">
                                 
                                        <label class="control-label col-sm-7" for="VAT Invoice Prefix">GST Invoice Slip Prefix<span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                            <input @if($row->confirm==1) readonly @endif type="text" required minlength="1" maxlength="4" class="form-control" id="GST_Slip_Prefix"   name="GST_Slip_Prefix" placeholder="GST Invoice Slip Prefix" @if($row->getdetails!=null) value="{{$row->getdetails->GST_Slip_Prefix}}" @endif  required/>
                                        </div>
                                  
                                    </div>
                                  </div>
                                   <div class="col-md-3">
                                     <div class="form-group">
                                       <input type="text" @if($row->confirm==1) readonly @endif required maxlength="8"   class="form-control quantity leftln number"  name="GST_Slip_serial" @if($row->getdetails!=null) value="{{$row->getdetails->GST_Slip_serial}}" @endif placeholder="Starting No." required/>
                                   </div>
                                     </div>
                                   <div class="col-md-2">
                                     <div class="form-group">
                                        <select class="form-control rigstln" name="GST_Slip_lenght" @if($row->confirm==1) readonly @endif>
                                              <option value="3"  @if($row->getdetails!=null && $row->getdetails->GST_Slip_lenght == "3") selected @endif>3</option>
                                              <option value="4"  @if($row->getdetails!=null && $row->getdetails->GST_Slip_lenght == "4") selected @endif >4</option>
                                              <option value="5"  @if($row->getdetails!=null && $row->getdetails->GST_Slip_lenght == "5") selected @endif >5</option>
                                              <option value="6"  @if($row->getdetails!=null && $row->getdetails->GST_Slip_lenght == "6") selected @endif >6</option>
                                              <option value="7"  @if($row->getdetails!=null && $row->getdetails->GST_Slip_lenght == "7") selected @endif >7</option>
                                              <option value="8" @if($row->getdetails!=null && $row->getdetails->GST_Slip_lenght == "8") selected @endif >8</option>
                           
                                          </select>
                                   </div>
                                     </div>
                                   <div class="col-md-2">
                                     <div class="form-group">
                                        <input @if($row->confirm==1) readonly @endif type="checkbox" name="GST_Slip_fy"  @if($row->getdetails!=null && $row->getdetails->GST_Slip_fy ==1) checked @endif value="1">
                                   </div>
                                     </div>
                              </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <!--<button type="submit" class="btn btn-default">Submit</button>-->
                                    <center><input type="button" class="btn btn-primary nextbutton" value="Previous" data-id="0"> <input type="button" class="btn btn-primary" id="next-2" value="NEXT" data-id="2"></center>
                                </div>

                            </div>
                            <!--end tab 2-->
                           </div>

                           <div id="tabs-3">
                                
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="Bank Name">Bank Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="Bank_Name" name="Bank_Name" @if($row->getdetails!=null) value="{{$row->getdetails->Bank_Name}}" @endif placeholder="Bank Name"  />
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="Bank Branch">Bank Branch</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="Bank_Branch" name="Bank_Branch" @if($row->getdetails!=null) value="{{$row->getdetails->Bank_Branch}}" @endif placeholder="Bank Branch" />
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="Account Name">Account Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="Account_Name" name="Account_Name" @if($row->getdetails!=null) value="{{$row->getdetails->Account_Name}}" @endif placeholder="Account Name" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="Account Number">Account Number</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" maxlenght="10" maxlenght="16" id="Account_Number" name="Account_Number" @if($row->getdetails!=null) value="{{$row->getdetails->Account_Number}}" @endif placeholder="Account Number" />
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="IFSC Code">IFSC Code</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" maxlength="11" id="IFSC_Code" name="IFSC_Code"  @if($row->getdetails!=null) value="{{$row->getdetails->IFSC_Code}}" @endif placeholder="IFSC Code" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for Delivery Slip">T&C for Delivery Slip 1</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control" rows="1" id="TC_Delivery_Slip" name="TC_Delivery_Slip"  maxlength="40" >@if($row->getdetails!=null) {{$row->getdetails->TC_Delivery_Slip}} @endif</textarea>
                                               
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for Delivery Slip">T&C for Delivery Slip 2</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  rows="1" id="TC_Delivery_Slip2" name="TC_Delivery_Slip2"  maxlength="40">@if($row->getdetails!=null) {{$row->getdetails->TC_Delivery_Slip2}} @endif</textarea>
                                               
                                            </div>
                                        </div>

                                        
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for GST Invoice Slip">T&C for GST Invoice Slip 1</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control" rows="1"  maxlength="40" id="TC_for_GST_Invoice_Slip" name="TC_for_GST_Invoice_Slip"  >@if($row->getdetails!=null) {{$row->getdetails->TC_for_GST_Invoice_Slip}} @endif</textarea>
                                                
                                            </div>
                                        </div>
                                             <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for GST Invoice Slip">T&C for GST Invoice Slip 2</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  rows="1"  maxlength="40" id="TC_for_GST_Invoice_Slip2" name="TC_for_GST_Invoice_Slip2">@if($row->getdetails!=null) {{$row->getdetails->TC_for_GST_Invoice_Slip2}} @endif</textarea>
                                                
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for VAT Invoice">T&C for VAT Invoice 1</label>
                                            <div class="col-sm-8">
                                                 <textarea class="form-control" rows="1"  maxlength="250" id="TC_for_VAT_Invoice" name="TC_for_VAT_Invoice"  >@if($row->getdetails!=null) {{$row->getdetails->TC_for_VAT_Invoice}} @endif</textarea>
                                               
                                            </div>
                                        </div>

                        

                                    </div>

                                    <div class="col-md-6">
                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for VAT Invoice">T&C for VAT Invoice 2</label>
                                            <div class="col-sm-7"> 
                                                 <textarea class="form-control"  rows="1"  maxlength="250" id="TC_for_VAT_Invoice2" name="TC_for_VAT_Invoice2">@if($row->getdetails!=null) {{$row->getdetails->TC_for_VAT_Invoice2}} @endif</textarea>
                                               
                                            </div>
                                        </div>
                                        
                                       
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for GST Invoice">T&C for GST Invoice 1</label>
                                            <div class="col-sm-7">
                                                 <textarea class="form-control"  rows="1" maxlength="250" id="TC_for_GST_Invoice" name="TC_for_GST_Invoice" rows="1"  >@if($row->getdetails!=null) {{$row->getdetails->TC_for_GST_Invoice}} @endif</textarea>
                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for GST Invoice">T&C for GST Invoice 2</label>
                                            <div class="col-sm-7">
                                                 <textarea class="form-control"  rows="1" maxlength="250" id="TC_for_GST_Invoice2" name="TC_for_GST_Invoice2">@if($row->getdetails!=null) {{$row->getdetails->TC_for_GST_Invoice2}} @endif</textarea>
                                                
                                            </div>
                                        </div>
                                         <div class="form-group">
                                    <label class="control-label col-sm-4" for="Image of RO VAT Registration">Image of RO VAT Registration </label>
                                    <div class="col-sm-4">
                                          
                                        <input type="file"  class="form-control" id="" name="VAT_image" placeholder="Image of RO VAT Registration "/>
                                    </div>
                                    <div class="col-sm-4">
                                      <img @if($row->getdetails!=null) src="{{URL::asset('')}}{{$row->getdetails->VAT_image}}" @endif width="100px" hieght="100px">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-sm-4" for="Image of RO VAT Registration">Image of RO PAN Card </label>
                                    <div class="col-sm-4">
                                        <input type="file"  class="form-control" id="" name="PAN_image" placeholder="Image of RO PAN Card"/>
                                    </div>
                                     <div class="col-sm-4">
                                      <img @if($row->getdetails!=null) src="{{URL::asset('')}}{{$row->getdetails->PAN_image}}" @endif width="100px" hieght="100px">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-sm-4" for="Image of RO GST Registration">Image of RO GST Registration </label>
                                    <div class="col-sm-4">
                                        <input type="file"  class="form-control" id="" name="GST_image" placeholder="Image of RO Pan Card"/>
                                    </div>
                                    <div class="col-sm-4">
                                      <img @if($row->getdetails!=null) src="{{URL::asset('')}}{{$row->getdetails->GST_image}}" @endif width="100px" hieght="100px">
                                    </div>
                                </div>
                                   <div class="form-group">
                                    <label class="control-label col-sm-4" for="Image of RO GST Registration">RO Logo </label>
                                    <div class="col-sm-4">
                                        <input type="file"  class="form-control" id="" name="image" placeholder="Ro logo"/>
                                    </div>
                                    <div class="col-sm-4">
                                      <img @if($row->image!=null) src="{{URL::asset('')}}{{$row->image}}" @endif width="100px" hieght="100px">
                                    </div>
                                </div>
                                    </div>
                                   

                                    <div class="form-group">
                                        <div class="col-sm-offset-4 col-sm-8">
                                            <!--<button type="submit" class="btn btn-default">Submit</button>-->
                                            <center><input type="button" class="btn btn-primary nextbutton" value="Previous" data-id="1"> <input type="submit" id="success-btn"  class="btn btn-primary site-btn submit-button" value="Submit" ></center>
                                        </div>

                                    </div>

                                </div>
                       </div>
                       </div>
                      </form>