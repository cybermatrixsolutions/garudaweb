

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Invoice</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
 <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Owners</h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Owner Lists</a></li>
        </ul>
      </div>
    </div>
    <div class="">
      
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
      
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Owner Name</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form name="myForm" class="form-horizontal" action="{{url('save_owner_management')}}" method="post" onsubmit="return validateForm()">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      	<div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">RO Code</label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="RO_code" id="sel1">
                             
                                                    @foreach($datas as $ro_master)
                                                        <option name="company_id" value="{{$ro_master->RO_code}}"
                                                                >{{$ro_master->RO_code}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Owner Name</label>
                          <div class="col-sm-9">
                            <input type="text" required="required" class="form-control" id="" name="owner_name" placeholder="Owner Name ">
                          </div>
                        </div>
                         <div class="form-group">


                          <label class="control-label col-sm-3" for="pwd">Email(Login Id)   </label>
                          <div class="col-sm-9">
                            <input type="email" data-mass="Email Id Already Exist" required="required" class="form-control checkunique" name="owner_Email" data-table="tbl_ro_owner_master" data-colum="owner_Email"  id="owner_Email" placeholder="Email">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Password</label>
                          <div class="col-sm-9">
                             <input type="password" required="required"  class="form-control" id="" name="password" placeholder="password">
                          </div>
                        </div>
                         
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Phone Number</label>
                          <div class="col-sm-9">
                            <input type="Number" required="required" data-table="tbl_ro_owner_master" data-colum="owner_phone"  class="form-control checkunique" id="" name="owner_phone" placeholder="Phone Number ">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Mobile</label>
                          <div class="col-sm-9">
                            <input ype="text" required="required"  pattern="[789][0-9]{9}" class="form-control checkunique" name="owner_mobile" data-mass="Mobile No. Already Exist" data-table="tbl_ro_owner_master"  data-colum="owner_mobile" id="" placeholder="Mobile" max="10">
                          </div>
                        </div>
                      <!--   <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">IMEI No</label>
                          <div class="col-sm-9">
                            <input type="text"  class="form-control" name="owner_IMEI" id="" placeholder="IMEI No">
                          </div>
                        </div> -->
                       
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default submit-button" disabled="disabled">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped">
             @if(isset($owner_list) && count($owner_list)  != 0)
            <thead>
              <tr>
                <th>S.No.</th>
                <th>RO Code</th>
                <th>Owner Name</th>
                <th>Email</th>
                <th>Phone No.</th>
                <th>Mobile No</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=0;?>
              @foreach($owner_list as $list)
              <?php $i++; ?>
              <tr>
                <td>{{$i}}</td>
                <td scope="row">{{$list->RO_code}}</td>
                <td>{{$list->owner_name}}</td>

                <td>{{$list->owner_Email}}</td>
                <td>{{$list->owner_phone}}</td>
                <td>{{$list->owner_mobile}}</td>

                <td> <?php if($list->is_active==1){?>

                   <a  title="Active" href="{{URL('owner_deactive')}}/<?php echo $list->id;?>"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="{{URL('owner_active')}}/<?php echo $list->id;?>">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
          </td> 
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      @if($list->is_active==1)
 
                      <li><a href="owner_edit_page/{{$list->id}}">Edit</a></li>
                       @endif
                   <!--   <li><a href="owner_delete/{{$list->id}}" onclick="return confirm('are u sure to delete?')">Delete</a></li>-->
                    </ul>
                  </div></td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Javascripts--> 
<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script>
function validateForm() {
    var x = document.forms["myForm"]["Pedestal_Number"].value;
    var y = document.forms["myForm"]["No_of_Nozzles"].value;
    if (x == "") {
        alert("Please Enter Pedestal Number !!");
       
        return false;
    }
    if (y == "") {
        alert("Please Enter Nozzles No. !!");
         
        return false;
    }
}
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   //id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique',{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>
</body>
</html>