<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<script>

function validateForm() {
    var x = document.forms["myForm"]["company_code"].value;
    var y = document.forms["myForm"]["company_name"].value;
    if (x == "") {

        alert("Please Enter your Country Code Only Capital Letter !!");
        return false;
    }
    if (y == "") {
        alert("Please Enter your Country Name !!");
        return false;
    }
}
</script>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Principal Company  Update</h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/principal_listing')}}">Principal  </a></li>
        </ul>
      </div>
    </div>
	
	
	                         <div class="">  <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                             </div>
	
	
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
         
          
          @foreach($principalData as $row)
          
                      <form class="form-horizontal" name="myForm" action="{{url('principalEdit')}}/{{$row->id}}" enctype="multipart/form-data"  onsubmit="return validateForm()" method="post">
            
                <input type="hidden" name="_token" value="{{ csrf_token()}}"/>
            
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Principal Company Code</label>
                          <div class="col-sm-9">
                            <input type="text" required="required" class="form-control checkunique" id="" name="company_code" value="{{$row-> company_code}}" placeholder="Principal Code" data-table="tbl_company_principal_master"  data-colum="company_code" data-mass="Principal Code Already Exist"  data-id="{{$row->id}}" readonly>
                          </div>
                        </div>
            
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Principal Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="" name="company_name" value="{{$row->company_name}}" placeholder="Company Name" required>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Principal Company Logo</label>
                          <div class="col-sm-9">
                            <input type="file" accept="image/*" id="file"  class="form-control jq_fileUploader"   name="image" onchange="return fileValidation()" accept="image/*">
                            <input type="hidden" value="{{url($row->image)}}"  id="imagadd" name="imagenew">

                          </div>
                        </div>
                        
                            <img id="blah"  src="{{url($row->image)}}" class="img-circle" style="width: 69px;
    margin-left: 54%; @if($row->image==null || trim($row->image)=='') display: none; @endif">
                        <div class="form-group">
            
                          <div class="col-sm-offset-3 col-sm-9">
                           <!-- <button type="submit" class="btn btn-default">Submit</button>-->
                     <center><input type="submit" id="success-btn"   class="btn btn-primary site-btn submit-button" value="Submit"></center>
                          </div>
                        </div>
            @endforeach
                      </form>
                      @if($row->image!=null || trim($row->image)!='') 
                     <center> <button class="btn btn-danger" style="margin-left: 25%;" onclick="myFunction()">Delete Logo</button></center>
                      @endif
                    </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script type="text/javascript">
 function fileValidation(){
    var fileInput = document.getElementById('file');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
        alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
        //fileInput.value = '';
         jQuery('.site-btn').attr('disabled',true);
    }
    else{
jQuery('.site-btn').attr('disabled',false);
}
} 
</script>
<script>
   $(".jq_fileUploader").change(function () {
    var fileSize = this.files[0];
    var sizeInMb = fileSize.size/1024;
    var sizeLimit= 51;
    if (sizeInMb > sizeLimit) {

        alert("logo size should not be more than 50 kb");
        jQuery('#success-btn').attr('disabled',true);
    }
    else{
jQuery('#success-btn').attr('disabled',false);
}
  });
</script>
<script type="text/javascript">
  function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script>
function myFunction() {
    document.getElementById("blah").src = "";
    document.getElementById("blah").style = "display:none;";
     document.getElementById("imagadd").value = "";
    
}
</script>
<script type="text/javascript">
 jQuery(function(){

  var url1="{{url('check_Pedestal_Number_unique')}}";

     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get(url1,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>

</body>
</html>