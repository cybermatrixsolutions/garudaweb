@extends('backend-includes.app')


@section('content')

<div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Sms Setting </h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          
         
          <li><a href="#">Sms Setting </a></li>
        </ul>
      </div>
    </div>

<div class="row"  style="margin-top: 40px;;">
	
 <div class="row">
 	 <center>@if(Session::has('success'))
                   <font style="color:red">{!!session('success')!!}</font>
               @endif
        </center>
      <div class="col-md-12">
        <div class="table-responsive">
           <div class="col-md-8">
        
              
              <form class="form-horizontal" action="{{Route('smssetting.update')}}" method="post"  onsubmit="return confirm('Do you want to Continue?');">
              {{ csrf_field() }}
            
               <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Username</label>
                    <div class="col-sm-8">
                      <input type="text"  class="form-control" name="Username" value="{{$dataInfo->Username}}"/>
                    </div>
               </div>

            
                 

              <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Password</label>
                    <div class="col-sm-8">
                       <input type="password" class="form-control" name="Password" value="{{$dataInfo->Password}}"/>
                    </div>
               </div>

               <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">URL</label>
                    <div class="col-sm-8">
                      <input type="text"  class="form-control"  name="URL" value="{{$dataInfo->URL}}"/>
                    </div>
               </div>
            
               <div class="form-group">
                <label class="control-label col-sm-8" ></label>
                    <div class="col-sm-3">
                     <input type="submit"  class="btn btn-primary submit-button  pull-left" value="Save"></

                    </div>
               </div>
             </form>     



            </div>
        </div>
      </div>
    </div>



@endsection