<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<style type="text/css">
  span.select2.select2-container.select2-container--default.select2-container--above{
    width: 100%!important;
  }
  .select2-container {
   
    width: 100% !important;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #211d1d !important;
   
}
</style>

</head>

<body class="sidebar-mini fixed">
<div class="wrapper"> 
<!-- Navbar-->
<!-- Navbar-->

 @include('backend-includes.header')

      <!-- Side-Nav-->

  @include('backend-includes.sidebar')
  
<div class="content-wrapper">
  <div class="page-title">
    <div>
      <h1><i class="fa fa-dashboard"></i>&nbsp; Other Item Bill Entry</h1>
    </div>
    <div>
      <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

        <li><a href="#"> Other Item Bill Entry</a></li>
      </ul>
    </div>
  </div>

   
  <div class="row">
    <div class="col-md-12 col-sm-12">
      <br/>
      <br/>
      <br/>
                 <div class="">
               @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
             @endif
                <center>@if(Session::has('success'))
                    <font style="color:red">{!!session('success')!!}</font>
                  @endif
                </center>
                
              </div>
             <form class="form-horizontal" id="form12" action="{{'save_lube_slip_entry'}}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="RO_code" id="RO_code" value="{{Auth::user()->getRocode->RO_code}}">

         
        
        <table class="table table-striped no-footer">
          <thead>
            <tr>
              <th>Customer Name</th>
              <th>Vehicle Reg No</th>
              <th>Select Item</th>
               <th>Bill No.</th>
              <th>Bill Date</th>
              <th>Shift</th>
              <th>Qty Billed</th>
               <th>Price incl GST</th>
              <th>Net Value<a style="float:right" id="addNew" class="btn btn-success" href="#">+</a></th>
            </tr>
          </thead>
          <tbody id="parant-app-row">
           <tr class='parent-tr' id="parant-row">
            <td>  
            <select class="form-control customer_code" required="required"  name="customer_code[]"></select>
            </td>

            <td>
          <select class="form-control registration_number"   required="required" name="Vehicle_Reg_No[]">                
           </select>

            </td>
            <td>
                <select class="form-control item_code"  required="required" name="item_code[]">
                             
                                                    @foreach($item_id as $item)
                                                        <option value="{{$item->id}}"
                                                                >{{$item->Item_Name}}
                                                              </option>
                                                    @endforeach
                            </select>
            </td>
            <td>

              <input type="text" class="form-control slip_detail"   name="slip_detail[]" id="slip_detail" placeholder="Slip No.">
              <span class="error_span" style="color: red" ></span>

              </td>
            <td>
                <input class="form-control datepicker1"  required="required" name="request_date[]"  type="text" />
        
            </td>
             <td>
               <select class="form-control shift" name="shift[]" required="required"></select>

            </td>
            <td>
               
              <input type="text" step="0.01" style="text-align: right;"  data-qt="0" class="form-control numeriDesimal petroldiesel_qt"  required="required"   name="petroldiesel_qty[]" placeholder="Quantity Value ">
            </td>
            <td>
               
                <input class="form-control itemprice numeriDesimal" style="text-align: right;" required="required" name="itemprice[]"  type="text" />
                <input type="hidden" class="itempricedefault" name="itempriceDefault[]" value="" >
        
            </td>

              <td>
                <a href='#' style="display: none;" class='deleteNew'>X</a>
               
                <input type="text" readonly class="form-control netvalue" name="netvalue[]" value="" >
        
            </td>
           </tr>
           
          </tbody>
        </table>
      
        <div class="form-group">
          <div>
            <button type="button" class="btn btn-default pull-right" id="tbnhide">Submit</button>
          </div>
        </div>
      </form>
                  
      </div>
    </div>
  </div>
<!-- Javascripts-->
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.js"></script>

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script  src="js/index.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
   $('.table-responsive').css( "overflow", "inherit" );
});

jQuery(".numeriDesimal").keyup(function()
  {
    var $this = jQuery(this);
    $this.val($this.val().replace(/[^\d.]/g, ''));
    });
$('.table-responsive').on('hide.bs.dropdown', function () {
   $('.table-responsive').css( "overflow", "auto" );
})
</script>

</script>

<script type="text/javascript">
 jQuery(function(){
   var currentP=jQuery('#parant-row');

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
              vil.addRow();
              vil.formsub();
              vil.getcrg();
              vil.slipunique();

               jQuery(document).on('keyup','.petroldiesel_qt',function(){
                    currentP=jQuery(this).parents('.parent-tr');
                    vil.netvalue();
              });
               
              jQuery(document).on('keyup','.itemprice',function(){
                    currentP=jQuery(this).parents('.parent-tr');
                    vil.netvalue();
              });

              jQuery(".datepicker1").datepicker({ 
                    autoclose: true, 
                    todayHighlight: true,
                    format: 'dd/mm/yyyy',
              }).on('changeDate', function(){
                       //currentP=jQuery(this).parents('.parent-tr');
                       //vil.getItemPrice();
                       vil.getShift();
                       
                    });
              vil.getItemPrice();
              vil.getShift();
           },

 slipunique:function () {
                  jQuery(document).on('mouseout','.slip_detail',function () {  
                     currentP=jQuery(this).parents('.parent-tr');
                   // alert(current);

                      var slip_detail = jQuery(this).val();
                      var customer=currentP.find('.customer_code').val();
                       var RO_code = jQuery('#RO_code').val();
                          currentP.find('.error_span').html('');
                          if(jQuery.trim(slip_detail)!=''){


                              jQuery.get("{{route('slipdetail')}}",{
                                  slip_detail:slip_detail,
                                      RO_code:RO_code,
                                      customer:customer,
                                  '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                              },function(response){
                                  if(response=="OK")
                                  {
                                      currentP.find('.error_span').html('');
                                      jQuery('#tbnhide').attr("disabled",false);
                                  }
                                  else
                                  {
                                      currentP.find('.error_span').html(response);
                                      jQuery('#tbnhide').attr("disabled",true);

                                  }
                              });

                           }
                  });
              },


           selectRo:function(){
            jQuery('#RO_code').on('change',function(){
                  vil.getcom();
                  currentP=jQuery(this).parents('.parent-tr');
              });
            jQuery(document).on('change','.customer_code',function(){
                  currentP=jQuery(this).parents('.parent-tr');
                  console.log(currentP);
                  vil.getcrg();
              });
             jQuery(document).on('change','.registration_number',function(){
                  currentP=jQuery(this).parents('.parent-tr');
                  //vil.getcapcity();
              });
             jQuery(document).on('change','.item_code',function(){
                  currentP=jQuery(this).parents('.parent-tr');
                  vil.getItemPrice();
              });
           
           },
           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#RO_code').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                  
                   currentP.find('.customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:currentP.find('.customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                   
                  currentP.find('.registration_number').html(opt);
                    //vil.getcapcity();
                   // vil.selectFuleType();
               });
           },
           addRow:function(){
            jQuery('#addNew').on('click',function(e){
              e.preventDefault();
              var cnt=jQuery('#parant-row').clone();
              cnt.removeAttr('id').appendTo('#parant-app-row').find('input,select').each(function(){
                             
                             if(jQuery(this).hasClass('slip_detail')){
                                jQuery(this).val('');
                             }else if(jQuery(this).hasClass('datepicker1')){
                                jQuery(this).val('');
                             }else if(jQuery(this).hasClass('itemprice')){
                              jQuery(this).val('');
                             }else if(jQuery(this).hasClass('petroldiesel_qt')){
                              jQuery(this).val('');
                             }
                              
                              jQuery(this).prev().show();
                              jQuery(this).next().html('');
                             
                          });
              /*console.log(cnt);
              jQuery('#parant-app-row').append("<tr class='parent-tr'>"+cnt+"</tr>");
              */
                jQuery(".datepicker1").datepicker({ 
                      autoclose: true, 
                      todayHighlight: true,
                      format: 'dd/mm/yyyy'
                }).on('changeDate', function(){
                       //currentP=jQuery(this).parents('.parent-tr');
                       //vil.getItemPrice();
                       //console.log('hello');
                       vil.getShift();
                    });

                currentP=jQuery('#parant-app-row').last();
                vil.getItemPrice();



            });


            jQuery(document).on('click','.deleteNew',function(e){
              e.preventDefault();

               if(jQuery(this).parents('.parent-tr').index()!=0)
                  jQuery(this).parents('.parent-tr').remove();
                
            });

            
           },
           formsub:function(){
                jQuery('#tbnhide').on('click',function(){
                    var flg=true;
                    var q=true;
                    jQuery('#parant-app-row').find("input, textarea, select").each(function(){
                     
                      //if(!jQuery(this).hasClass('shift'))
                       if(jQuery.trim(jQuery(this).val())==''){
                          jQuery(this).css('border-color','#e27777');
                          flg=false;
                       }else{
                         jQuery(this).css('border-color','');
                       }

                       if(jQuery(this).hasClass('itemprice')){
                           if(jQuery.trim(jQuery(this).val())=='' || jQuery.trim(jQuery(this).val())==0){
                              jQuery(this).css('border-color','#e27777');
                              flg=false;
                           }else{
                             jQuery(this).css('border-color','');
                           }
                         }
                      
                        if(jQuery(this).hasClass('petroldiesel_qty')){
                            console.log('helo');
                            console.log(jQuery(this).data('qt'));
                            console.log(parseInt(jQuery(this).val()));
                          if(parseInt(jQuery(this).data('qt'))<parseInt(jQuery(this).val())){
                             jQuery(this).css('border-color','#e27777');
                             flg=false;
                             q=false;
                           }
                        }
                    });
                   
                   if(q==false){

                      alert("Please Enter Below Capacity Limit ");
                   }

                    if(flg==true && q==true){
                      jQuery('#form12').submit();
                    }

                });
           },
            getItemPrice:function(){
             var item=currentP.find('.item_code').val();
            
             

                 jQuery.get('getietmpricebydate/'+item,{
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                    
                     currentP.find('.itemprice').val(data);
                     currentP.find('.itempricedefault').val(data);
                     //alert(data);
                   });
               
           },
             getShift:function(){
             
             var request_date=currentP.find('.datepicker1').val();

             if(request_date){

                 jQuery.get('getShiftByDate',{
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                    'request_date':request_date,
                   },function(data){
                    console.log(data);
                      var str='';
                      jQuery.each(data,function(i,v){
                         str+='<option value="'+i+'">'+v+'</option>';
                      });
                     currentP.find('.shift').html(str);
                   });
               }
           },
           netvalue:function(){
            
               var netvalue=0;

               var price=parseFloat(currentP.find('.itemprice').val());

               var qty=parseFloat(currentP.find('.petroldiesel_qt').val());

               console.log(parseFloat(price)+'==='+parseFloat(qty));

                if(!isNaN(qty) && !isNaN(price))
                  netvalue=qty*price;
                 
               currentP.find('.netvalue').val(netvalue);


           },

     }
     vil.init();
  });
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
 $('#tags').select2({
  
    tags: true,
    tokenSeparators: [','], 
    placeholder: "Add your tags here"
});


</script>
</body>
</html>