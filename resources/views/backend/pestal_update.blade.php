<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
<style>
  .slimScrollDiv {
   height:834px!important;
   }

</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Nozzles  Update
</h1>
      </div>
      <div>
        <ul class="breadcrumb">
       <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="{{url('/pedestal')}}">Nozzles 
</a></li>
        </ul>
      </div>
    </div>
	                        <div class="">
							       <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                            </div>  
                             @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <!-- Modal -->
              <div class="row" style="margin-top:-15px !important;">
            @foreach($pedestalData as $row)
            <form name="myForm" class="form-horizontal" action="{{url('pedestal_edit')}}/{{$row->id}}" enctype="multipart/form-data" method="post" onsubmit="return validateForm()">
         {{ csrf_field() }}
          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Tank No.</label>
                          <div class="col-sm-9">
                            <select class="form-control" id="tank" disabled name="Tank_id">
                              @foreach($tank_master as $tank) 
                              <option value="{{$tank->id}}" @if($row->Tank_id == $tank->id) selected @endif >{{$tank->Tank_Number}}</option>
                              
                               @endforeach
                              
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Pedestal Number</label>
                         <div class="col-sm-9">
                           <select class="form-control" id="Pedestal_id" disabled name="Pedestal_id">
                            @foreach($pedestal_master as $pedestel) 
                              <option value="{{$pedestel->id}}" @if($row->Pedestal_id == $pedestel->id) selected @endif >{{$pedestel->Pedestal_Number}}</option>
                              
                               @endforeach
                             
                            </select>
                          
                          </div>
                        </div>
                        
                        
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Nozzles No <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input type="text" data-table="tbl_pedestal_nozzle_master"  data-colum="Nozzle_Number" data-mass="Nozzles No Already Exist" data-id="{{$row->id}}" class="form-control checkunique " id="" name="Nozzle_Number" value="{{$row->Nozzle_Number}}" placeholder="Nozzle_Number">
                          </div>
                         
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Fuel Type <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control" disabled id="Pedestal_id" name="fuel_type">
                            @foreach($fuel_type as $fuel) 
                              <option value="{{$fuel->id}}" disabled @if($row->fuel_type == $fuel->id) selected @endif >{{$fuel->Item_Name}}</option>
                              
                               @endforeach
                             
                            </select>
                            
                          </div>
                        </div>
                        <input type="hidden" name="fuel_type" value="{{$row->fuel_type}}">
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Opening Reading <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control numeriDesimal" id="" name="Opening_Reading" value="{{$row->Opening_Reading}}" placeholder="Opening Reading">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Opening Date <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input class="form-control" id="datepicker" name="Opening_Date" value="{{date('d/m/Y',strtotime($row->Opening_Date))}}"  type="text" />
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Remarks</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="" name="Remarks"  value="{{$row->Remarks}}" placeholder="Remarks">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Active From Date</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="datepickers" name="Active_From_Date"   value="{{Carbon\Carbon::parse($row->Active_From_Date)->format('d/m/Y')}}" placeholder="Remarks">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <input type="submit" id="id-form-submit"  value="Submit" class="btn btn-default submit-button"> 
                          </div>
                        </div>
                      </form>
                        @endforeach
                    </div>
            
          </div>

        </div>
      </div>
    </div>
   
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script type="text/javascript">
  $(function () {
  $("#datepicker").datepicker({ 
        autoclose: true, 
        
        format: 'dd/mm/yyyy'
      
      
  }).datepicker();
});
  $(function () {
  $("#datepickers").datepicker({ 
        autoclose: true, 
        
        format: 'dd/mm/yyyy'
      
  }).datepicker();
});
   jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>

<script>
function validateForm() {
    var Nozzle_Number = document.forms["myForm"]["Nozzle_Number"].value;
    var Opening_Reading = document.forms["myForm"]["Opening_Reading"].value;
 
    if (Nozzle_Number == "") {
        alert("Please Enter Nozzle Number !!");
       
        return false;
    }
    if (Opening_Reading == "") {
        alert("Please Enter Opening Reading !!");
         
        return false;
    }
}
</script>
<script type="text/javascript">
 jQuery(function(){

  var url1="{{url('check_Pedestal_Number_unique')}}";

     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get(url1,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });

 

            $(document).ready(function() {
    $("#id-form-submit").on("click", function(){
        return confirm("Do you want to Continue? ");
    });
});


</script>
</body>
</html>