<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.cs')}}s" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">

<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">

<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')  
  
  
  
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Tax Structure</h1>
      </div>
      <div>
        <ul class="breadcrumb">
           <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          
          <li><a href="#">Taxs </a></li>
        </ul>
      </div>
    </div>
  
       
                        <div class="">
                     
                            </div>
    
  
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <center>
            @if(Session::has('success'))
                 <font style="color:red">{!!session('success')!!}</font>
              @endif

               @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            </center>
          <div class="col-md-6 col-sm-4 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Tax</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
          
          
                      <form class="form-horizontal" id="form" action="{{'add_tax'}}" enctype="multipart/form-data" method="post" onsubmit="return confirm('Do you want to Continue?');">
                        <input type="hidden" name="_token" value="{{ csrf_token()}}"/> 
                         
                         
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">State</label>
                          <div class="col-sm-8">
                         
                             <select class="form-control"  id="stats1" name="name" @if (Auth::user()->user_type==3) disabled @endif>

                               @foreach($states as $state)
                                  <option @if (Auth::user()->getRocode!=null && Auth::user()->getRocode->state==$state->id) selected @endif  value="{{$state->statecode}}"
                                          >{{$state->name}}</option>
                              @endforeach
                             </select>
                          </div>
                         
                        </div>

                         <div class="form-group" ng-app="">
                          <label class="control-label col-sm-4" for="email">Select Interstate/Intrastate</label>
                          <div class="col-sm-8">
                            <select class="form-control name " id="strate_type"  name="strate_type" required/>
                              <option value="interstate">Interstate</option>
                              <option value="intrastate">Intrastate</option>
                              <option value="both">Both</option>
                            </select>
                          </div>
                        </div>
                         <hr/>
                         <div class="col-sm-12 text-center" for="pwd"> <h6>For Printing on Invoice</h6></div>
                        <hr/>
                        <div class="form-group" ng-app="">
                          <label class="control-label col-sm-4" for="email">Tax Type</label>
                          <div class="col-sm-8">
                            <select class="form-control name " id="text_type"  name="Tax_Type" required/>
                              <option value="GST">GST</option>
                              <option value="VAT">VAT</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group" id="GST_Type">
                          <label class="control-label col-sm-4" id="GST_Type_name" for="Gst Type">GST Type</label>
                          <div class="col-sm-8">
                            <select class="form-control name" id="GST_Type_input" name="GST_Type"/>
                              <option value="CGST">CGST</option>
                              <option value="UT/SGST">UT/SGST</option>
                              <option value="IGST">IGST</option>
                              <option value="SPCESS">SPCESS</option>
                              <option value="SCCESS">SCCESS</option>
                            </select>
                          </div>
                        </div>

                       <!--  <div class="form-group" ng-app="">
                          <label class="control-label col-sm-4" for="email">Row/Column</label>
                          <div class="col-sm-8">
                            <select class="form-control name" id="Row"  name="row_columns" required/>

                              <option value="row">Row</option>
                              <option value="columns">Column</option>
                              
                            </select>
                          </div>
                        </div>
 -->
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="Gst Type">Row Position</label>
                          <div class="col-sm-8">
                            <select class="form-control name" id="text_type"  name="position"/>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group" id="Tax_Codehde">
                          <label class="control-label col-sm-4" for="pwd">Tax Rate</label>
                          <div class="col-sm-8">
                            <select class="form-control" id="Tax_Code" name="tax_rate" required/>

                            @if(isset($type_list) && !empty($type_list))
                            @foreach($type_list as $list)
                            
                              <option value="{{$list->rate}}">{{$list->rate}}%</option>
                            
                            @endforeach
                            @endif
                            
                            </select>
                           
                          </div>
                        </div>
                        <div class="form-group" id="vattaxrate">
                          <label class="control-label col-sm-4" for="pwd">Vat Tax Rate</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control Tax_Code" id="vatTax_Code"   name="vat_tax_rate"  placeholder=""/>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Generate Tax Code</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="firstname" name="Tax_Code" readonly placeholder=""/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Description for Printing</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="Description" name="Description" placeholder="Description" required/>
                          </div>
                        </div>
                        <hr/>
                       
                          <div class="col-sm-12" for="pwd" style="text-align: center;"><h6>For Calculation of Tax</h6></div>

                        
                        <hr/>


                        <div class="form-group">
                          <label class="col-sm-4 text-center" for="pwd">Tax Percentage on Basic Price</label>
                          <div class="col-sm-8">
                            <input type="text"  class="form-control numeriDesimal" id="" name="Tax_percentage" required>
                            
                            

                          </div>
                        </div>

                    
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-8">
                           <!-- <button type="submit" class="btn btn-default">Submit</button>-->
                <center><input type="submit" id="success-btn" class="btn btn-primary site-btn submit-button" value="Submit"></center>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>S.No</th>
                <th>Tax Code</th>
                <th>Tax Type</th>
                <th>Tax percentage</th>
                 <th>Tax Description</th>
                <th>Status</th>
                <!-- <th>Actions</th> -->
              </tr>
            </thead>
            <tbody>
      
       <?php $i=0; ?>
      
                 @foreach($getTax as $row) 
      
              <?php $i++ ;?>
              <tr>
        <td>{{$i}}</td>
                <td scope="row">{{$row->Tax_Code}}</td>
                <td>{{$row->Tax_Type}}</td>
                <td>{{$row->Tax_percentage}}%</td>
                <td>{{$row->Description}}</td>
        <td> <?php if($row->is_active==0){?>
                   <a title="Deactive" href="{{URL('taxActive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to activate?');">

                     <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
                   
                  <?php }else{ ?>

                  
                    <a  title="Active" href="{{URL('taxDeactive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to deactivate?');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

             <?php } ?> 
          </td>
        
              <!--   <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">

                      <li><a href="dip_stick_update/{{$row->id}}">view</a></li>
                    <!--   @if($row->is_active==1)
                      <li><a href="tax_update/{{$row->id}}">Edit</a></li>
                      @endif -->

                    <!--  <li><a href="tax_delete/{{$row->id}}" onClick="return confirm('Are you sure?');">Delete</a></li>-->
                   <!-- </ul>
                  </div>-->
              <!-- </td> -->
              </tr>
        @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
 



<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
             vil.getTexOfTax();
             vil.setvat();
             vil.numeriDesimal();
             // vil.addTaxRate();
             vil.addTaxRateOnClick();
             //vil.checkGst();
             vil.getstcode();
             jQuery(document).on('change','#text_type',function(){
                vil.setvat();
                vil.setgst();
                vil.setgst();
             });
             jQuery(document).on('change','#stats1',function(){
                vil.setvat();
             });
             jQuery(document).on('change','#GST_Type_input',function(){
                vil.setvat();
             });

             jQuery(document).on('change','#Tax_Code',function(){
                vil.setvat();
             });
             jQuery(document).on('keyup','.Tax_Code',function(){
                vil.setvat();
             });

             

              vil.setgst();

           },

            getstcode:function(){
            jQuery('#stats1').on('change',function(){
                    console.log('hello');
                  vil.getTexOfTax();
              });
          },
          numeriDesimal:function()
          {
                jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });
          },
           setvat:function(){

                        var v=jQuery('#text_type').val();
                         var sta='';
                        if(v=='VAT')
                          sta=jQuery('#stats1').val()+jQuery('#GST_Type_input').val()+jQuery('#vatTax_Code').val();
                        else
                         sta=jQuery('#stats1').val()+jQuery('#GST_Type_input').val()+jQuery('#Tax_Code').val();
                       
                    

                      jQuery('#firstname').val(sta);

                      
           },
           setgst:function(){
                     var v=jQuery('#text_type').val();
                     var d='';
                     if(v=='GST'){
                        v='<option value="CGST">CGST</option><option value="UT/SGST">UT/SGST</option><option value="IGST">IGST</option><option value="SPCESS">SPCESS</option><option value="SCCESS">SCCESS</option>';
                        jQuery('#GST_Type_name').html('GST Type');
                     }else{
                        jQuery('#GST_Type_name').html('VAT Type');
                        v='<option value="VAT">VAT</option><option value="ADVAT">ADVAT</option><option value="SCVAT">SCVAT</option><option value="CSVAT">CSVAT</option>';

                     }
                     jQuery('#GST_Type_input').html(v);
                    vil.setvat();
                     var v=jQuery('#text_type').val();
                     if(v=='VAT'){
                        jQuery('#Tax_Codehde').hide();
                       
                       
                        jQuery('#vattaxrate').show();
                     }else{
                        jQuery('#Tax_Codehde').show();
                         
                       
                         jQuery('#vattaxrate').hide();
                     }
           },
         
           selectRo:function(){

            jQuery('#firstname').on('blur',function(){
                
                  udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique',{
                  table:'tbl_tax_master',
                  colum:'Tax_Code',
                  unik:udat,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                    console.log(data);
                   if(data=='true'){
                      alert('Tax Code already exists Enter Other!');
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },

          //  addTaxRate:function()
          //  {
          //   jQuery.get('getTaxRate',{
          //       taxtype:jQuery('#text_type').val(),
          //       '_token': jQuery('meta[name="csrf-token"]').attr('content'),
          //      },function(data){

          //        var opt;
          //         jQuery.each(data, function(index,value){

          //            opt+='<option value="'+index+'">'+value+'</option>';
          //         });
          //           console.log(opt);
                   
          //       jQuery('#Tax_Code').html(opt);
                
          //      });
          // },
          addTaxRateOnClick:function()
           {
             jQuery('#text_type').on('change',function(){
            jQuery.get('getTaxRate',{
                taxtype:jQuery('#text_type').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt;
                
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'%</option>';
                  });
                    console.log(opt);
                   
                    jQuery('#Tax_Code').html(opt);
                 
                 

                  
                
               });
          });
          },

           getTexOfTax:function(){
            
             jQuery.get('getTaxoftax',{
                statecode:jQuery('#stats1').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='<option></option>';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#Tax_of_Tax').html(opt);
                
               });
           },
           checkGst:function(){
            jQuery('#success-btn').on('click',function(e){

                if(jQuery('#text_type').val()=='GST'){
                  e.preventDefault();

                    var gst_type=jQuery('#stats1').val()+jQuery('#GST_Type_input').val();

                    jQuery.get("{{url('checkgst')}}/"+gst_type,{
                     '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                    },function(data){

                        if(data=='true'){
                            alert(jQuery('#stats1 option:selected').text()+' '+jQuery('#GST_Type_input option:selected').text()+' Already Exists');
                        }else{
                          jQuery('#form').submit();
                        }
                    });
                }  
            });
           },
          
     }
     vil.init();
  });



</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>