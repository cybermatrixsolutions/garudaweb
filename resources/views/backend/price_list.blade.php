
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<style type="text/css">
  span.select2.select2-container.select2-container--default.select2-container--above{
    width: 100%!important;
  }
  .select2-container {
   
    width: 100% !important;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #211d1d !important;
   
}
</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
<!-- Navbar-->
<!-- Navbar-->

 @include('backend-includes.header')

      <!-- Side-Nav-->

  @include('backend-includes.sidebar')
  
<div class="content-wrapper">
  <div class="page-title">
    <div>
      <h1><i class="fa fa-dashboard"></i>&nbsp;RO Items </h1>
    </div>
    <div>
      <ul class="breadcrumb">
       <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
        <li><a href="#">RO Items </a></li>
      </ul>
    </div>
  </div>
   <div class="">
      
      <ul>
        @foreach($errors->all() as $error)
          <li style="color: red">{{ $error}}</li>
        @endforeach
      </ul>
     
      
    </div>
  <div class="row"><br><br>
     <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
    <div class="col-md-6 col-sm-6 vat">
    	<a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a>
    </div>
    <div class="col-md-6 col-sm-6 vat">
 
     
    
        <div class="pull-right"> 
          <div>
            @if(Auth::user()->user_type==3)
                
                     <form class="form-inline" action="{{'exmportcsv'}}" method="post" enctype="multipart/form-data">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <div class="form-group">
                     <label for="email"File(csv)></label>
                     <input type="file" id="inputfile" required="required"   placeholder="File(csv)" name="file" style="display: inline; width: auto;">
                     </div>
                     <div class="form-group">
                     <input  type="submit" value="Submit" style="margin-left: -83px;background-color: #00786a;color: #fff;">
                     </div>
                     </form>
                     <a href="all-tweets-csv" class="btn btn-primary">Export as CSV</a>
                     @else
                      <form class="form-inline" action="{{'exmportcsv'}}" method="post" enctype="multipart/form-data">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <div class="form-group">
                     <label for="email"File(csv)></label>
                     <input type="file" id="inputfile" required="required"   placeholder="File(csv)" name="file" style="display: inline; width: auto;">
                     </div>
                     <div class="form-group">
                     <input  type="submit" value="Submit" style="margin-left: -83px;background-color: #00786a;color: #fff;">
                     </div>
                     </form>
                     <a href="all-price-list-csv-file" class="btn btn-primary">Export as CSV</a>
                   
                     @endif

          </div>
        
          <!-- Modal -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                  <h4 class="modal-title" id="myModalLabel">RO Item</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <form class="form-horizontal" action="{{'save_price_list'}}" method="post">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-group">
                        <label class="control-label col-sm-3" for="pwd">Outlet Name</label>
                        <div class="col-sm-9">
                           <select class="form-control" required="required" name="price_ro_code" id="sel1">
                            
                                                  @foreach($datas as $ro_master)
                                                  <option name="company_id" value="{{$ro_master->RO_code}}">{{$ro_master->pump_legal_name}}</option>
                                                  @endforeach
                          </select>
                        </div>
                      </div>
                        <div class="form-group">
                        <label class="control-label col-sm-3" for="pwd">Item Code</label>
                        <div class="col-sm-9">
                           <input type="text" required="required" class="form-control" name="price_item_list" id="" placeholder="item code">
                          
                        </div>
                      </div>
                       <!--  <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Price</label>
                        <div class="col-sm-9">
                      	   <input type="number" required="required" class="form-control" name="price" id="" placeholder="Price">
                        </div>
                      </div> -->
                      
                      <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Open Stock</label>
                        <div class="col-sm-9">
                           <input type="number" required="required" class="form-control " id="" name="volume_liter" placeholder="Open Stock">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Item Name</label>
                        <div class="col-sm-9">
                           <input type="text" required="required" class="form-control" id="" name="item_name" placeholder="Item Name">
                        </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Stock Group</label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="Stock_Group" id="sel1">
                             
                                                    @foreach($stock_group as $stock)
                                                        <option  value="{{$stock->id}}"
                                                                >{{$stock->Group_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Unit of Measure</label>
                          <div class="col-sm-9">
                                <select class="form-control" required="required" name="Unit_of_Measure" id="Unit_of_Measure">
                             
                                                    @foreach($unit_measure as $unit)
                                                        <option  value="{{$unit->id}}"
                                                                >{{$unit->Unit_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Active From Date</label>
                          <div class="col-sm-9">
                             <input type="text" required="required" class="form-control datepicker" name="Active_From_Date" id="" placeholder="Active From Date">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Tail Unit</label>
                          <div class="col-sm-9">
                             <input type="number"  class="form-control" required="required" id="" name="Tail_Unit" placeholder="Tail Unit">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Conversion</label>
                          <div class="col-sm-9">
                             <input type="number"   class="form-control" required="required" name="Conversion" id="" placeholder="Conversion">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Alternate Unit</label>
                          <div class="col-sm-9">
                             <input type="number"  class="form-control"   name="Alternate_Unit" id="" placeholder="Alternate Unit">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">LOB</label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="LOB" id="sel1">
                             
                                          @foreach($lob as $lobc)
                                    <option  value="{{$lobc->lob_name}}">
                                            {{$lobc->lob_name}}</option>
                                @endforeach
                            </select>
                           
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Brand</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control"  name="brand" id="" placeholder="brand">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Tax</label>
                          <div class="col-sm-9">
                            <select class="form-control" id="tags" multiple="multiple" required="required" name="tax[]">
                             
                           
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">HSN/SAC</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control"  name="hsncode" id="" placeholder="hsncode" required="required">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">GST/VAT</label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="gst_vat" id="sel1">
                               
                                   <option value="GST">GST</option>
                                   <option value="VAT">VAT</option>
                                                
                            </select>
                          </div>
                        </div>

                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
     
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="table-responsive">
        <table class="table table-striped" id="myTable">
          <thead>
            @if(isset($price_list) && count($price_list)  != 0)
            <tr>
              <th>S.No</th>
              <th>Pump Legal Name</th>
              <th>Item Name</th>
             <!--  <th>Price</th> -->
              <th>Active From Date</th>
              <th>Status</th>
               <th>Actions</th>
          </thead>
          <tbody>
            <?php  $i=0;?>
             @foreach($price_list as $list)
             <?php $i++;?>
            <tr>
              <td>{{$i}}</td>
              <td scope="row">{{$list->pump_name}}</td>
              <td>{{$list->Item_Name}}</td>
              <!--  <td>{{$list->Price}}</td> -->
               <td>{{Carbon\Carbon::parse($list->Active_From_Date)->format('d/m/Y')}}</td>
                
                <td> <?php if($list->is_active==1){?>

                   <a  title="Active" href="pricemanagement/active/{{$list->id}}/0" onclick="return confirm('Do you want to deactivate?');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="pricemanagement/active/{{$list->id}}/1" onclick="return confirm('Do you want to activate?');">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
          </td>
              <td><div class="btn-group">
                  <button type="button" class="btn btn-danger">Action</button>
                  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                  <ul class="dropdown-menu" role="menu">
                     @if($list->is_active==1)
                    <li><a href="price/edit/{{$list->id}}">Edit</a></li>
                    @endif
                     <li><a href="price/view/{{$list->id}}">View</a></li>
                    <!-- <li><a href="delete_price/{{$list->id}}" onclick="return confirm('are you sure to delete?');">Delete</a></li> -->
                  </ul>
                </div>
             </td>
            </tr>
             @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Javascripts--> 

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script>

<!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> --> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="js/index.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
 $('#tags').select2({
  data: [],
    tags: true,
    tokenSeparators: [','], 
    placeholder: "Select Tax"
});

</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
   $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
   $('.table-responsive').css( "overflow", "auto" );
})
</script>
 @if(isset($price_list) && count($price_list)  != 0)
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
@endif
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
             vil.getcrg();
              

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcrg:function(){
            
             jQuery.get('getrocodetx',{
                rocode:jQuery('#sel1').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';

                  


                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                  
                jQuery('#tags').html(opt);

               });
           },

     }
     vil.init();
  });
 jQuery(function () {

    jQuery('#inputButton').on('click',function(){
        var filename=jQuery('#inputfile').val();
        var valid_extensions = /(\.csv)$/i;   
        if(valid_extensions.test(filename))
        { 
          
          var volumedip=jQuery('#volumedip').val();
          var discretiondip=jQuery('#discretiondip').val();
           if(jQuery.trim(volumedip)==''){
            alert('Please Inser Dip Charts Volume Type First');
           }else if(jQuery.trim(discretiondip)==''){
            alert('Please Inser Dip Charts Discretion First');
           }else{

            jQuery('#form1').submit();

           }
          
        }
        else
        { 
           alert('please select file in csv format');

           return false;
        }
    });
});
</script>
</body>
</html>