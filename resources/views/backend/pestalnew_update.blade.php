<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Pedestal Update
</h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/nozzles')}}
">Pedestal 
</a></li>
        </ul>
      </div>
    </div>
    <div class="">
<center>@if(Session::has('success'))
             <font style="color:red">{!!session('success')!!}</font>
          @endif</center>
      </div> 
        <div class="row">
            @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

	
    <div class="row">
      <div class="col-md-12 col-sm-12">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <!-- Modal --> 
            
            <form name="myForm" class="form-horizontal" action="{{url('pedestalnew_edit')}}/{{$getpess->id}}" enctype="multipart/form-data" method="post" onsubmit="return validateForm()">
               {{ csrf_field() }}
               <input type="hidden" id="sel1" name="RO_code" value="{{Auth::user()->getRocode->RO_code}}">
                      
                      <!--   <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Pump Legal Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" id="sel1" name="RO_code">
                             @foreach($getPedesta as $row) 
                              <option @if($getpess->RO_code==$row->RO_code) selected @endif value="{{$row->RO_code}}">{{$row->pump_legal_name}}</option>
                              
                               @endforeach
                            </select>
                          </div>
                        </div> -->


                        
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Pedestal Number <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                           <input type="text" class="form-control checkunique numeriDesimal" data-id="{{$getpess->id}}" data-mass="Pedestal Number Already Exist" data-table="tbl_pedestal_master"  data-colum="Pedestal_Number" placeholder="Pedestal_Number" name="Pedestal_Number" value="{{$getpess->Pedestal_Number}}">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">No. of Nozzles <span style="color:#f70b0b;">*</span> </label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control numeriDesimal" placeholder="No_of_Nozzles" name="No_of_Nozzles" value="{{$getpess->No_of_Nozzles}}" >
                          </div>
                        </div>
                       
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" id="id-form-submit" class="btn btn-default submit-button" >Submit</button>
                          </div>
                        </div>
                      
                   
                      </form>
                
                    </div>
                      
            
          </div>

        </div>
      </div>
    </div>
   
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
/* jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });*/
</script>
<script>
function validateForm() {
    var x = document.forms["myForm"]["Pedestal_Number"].value;
    var y = document.forms["myForm"]["No_of_Nozzles"].value;
    if (x == "") {
        alert("Please Enter Pedestal Number !!");
       
        return false;
    }
    if (y == "") {
        alert("Please Enter Nozzles No. !!");
         
        return false;
    }
}
</script>
<script type="text/javascript">
 jQuery(function(){
          
        var url2="{{url('check_Pedestal_Number_unique')}}";
     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get(url2,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });

 
            $(document).ready(function() {
    $("#id-form-submit").on("click", function(){
        return confirm("Do you want to Continue? ");
    });
});
</script>
</body>
</html>