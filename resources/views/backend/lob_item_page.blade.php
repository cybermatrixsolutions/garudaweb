<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<style type="text/css">
  span.select2.select2-container.select2-container--default.select2-container--above{
    width: 100%!important;
  }
  .select2-container {
   
    width: 100% !important;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #211d1d !important;
   
}
</style>

</head>

<body class="sidebar-mini fixed">
<div class="wrapper"> 
<!-- Navbar-->
<!-- Navbar-->

 @include('backend-includes.header')

      <!-- Side-Nav-->

  @include('backend-includes.sidebar')
  
<div class="content-wrapper">
  <div class="page-title">
    <div>
      <h1><i class="fa fa-dashboard"></i>&nbsp;LOB Item Selection</h1>
    </div>
    <div>
      <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

        <li><a href="#">LOB Item Selection</a></li>
      </ul>
    </div>
  </div>
   <div class="">
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
      
    </div>
              <div class="row">
                <div class="col-md-12 col-sm-12">

         
         
                  <h4 class="modal-title" id="myModalLabel">LOB Item Selection</h4>
               
                
                 
                    <form class="form-horizontal" action="{{'save_selected_lob'}}" method="post">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-group">

<!-- 
                      <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Pump Legal Name</label>
                          <div class="col-sm-9">
                              <select class="form-control" required="required" name="RO_code" id="sel1">
                                  @foreach($pump_legal_name_list as $ro_master)
                                      <option  value="{{$ro_master->RO_code}}"
                                              @if(isset($ro_code) and $ro_code == $ro_master->RO_code) selected @endif >{{$ro_master->pump_legal_name}}</option>
                                  @endforeach
                              </select>
                          </div>
                        </div> -->

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">LOB</label>
                          <div class="col-sm-9">
                         
                              <select class="form-control" required="required" name="lob_name[]"   id="tags" multiple="multiple">
                               
                                  @foreach($lob_item_list as $lob_list)
                                        @if(isset($get_lob))
                                       
                                      <option value="{{$lob_list->lob_name}}"
                                         @foreach($get_lob as $lob)
                                              @if($lob->lob_name == $lob_list->lob_name) selected @endif @endforeach >{{$lob_list->lob_name}}</option>
                                              
                                              
                                              @else
                                              <option value="{{$lob_list->lob_name}}"
                                              >{{$lob_list->lob_name}}</option>
                                              @endif
                                  @endforeach
                              </select>
                           

                          </div>
                        </div>
                        
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </form>
                 
                </div>
              </div>
  <div class="row">
    <div class="col-md-12">
      <div class="table-responsive">
        <table class="table table-striped" id="myTable">
          <thead>
            @if(isset($lube_list) && count($lube_list)  != 0)
            <tr>
              <th>S.No.</th>
              <th>Pump legal Name</th>
              <th>Customer Name</th>
              <th>Items</th>
              <th>Request Date</th>
              <th>Execution date</th>
              
              <th>Vehicle Reg No</th>
               
              <th>Status</th>
               <th>Actions</th>
          </thead>
          <tbody>
            <?php $i=0;?>
             @foreach($lube_list as $list)
             <?php $i++;?>
            <tr>
              <td>{{$i}}</td>
              <td scope="row">{{$list->pump_legal_name}}</td>
              <td>{{$list->Customer_Name}}</td>
               <td>{{$list->Item_Name}}</td>
               <td>{{Carbon\Carbon::parse($list->request_date)->format('d-m-Y')}}</td>
               <td>@if($list->Execution_date != null ){{Carbon\Carbon::parse($list->Execution_date)->format('d-m-Y')}} @endif</td>
              
              
             
               <td>{{$list->Vehicle_Reg_No}}</td>
                
               <td>  @if($list->is_active==1)

                   <a  title="Active" href="lube_request/active/{{$list->id}}/0"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  @else

                   <a title="Deactive" href="lube_request/active/{{$list->id}}/1"> 

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                @endif 
          </td>
              <td><div class="btn-group">
                  <button type="button" class="btn btn-danger">Action</button>
                  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                  <ul class="dropdown-menu" role="menu"> @if($list->is_active==1)
                     <li><a href="lube_request/view/{{$list->id}}">View</a></li>
                     @endif
                  <!--  <li><a href="lube_request/delete/{{$list->id}}" onclick="return confirm('are you sure to delete?');">Delete</a></li>-->
                  </ul>
                </div>
             </td>
            </tr>
             @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Javascripts-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.js"></script>

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script>


<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="js/index.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
   $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
   $('.table-responsive').css( "overflow", "auto" );
})
</script>

</script>

<script type="text/javascript">
 jQuery(function(){
  

     var vil={
           init:function(){
             vil.selectRo();
              //vil.getcom();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#sel1').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
               });
           },

     }
     vil.init();
  });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
 $('#tags').select2({
 
    tags: true,
    tokenSeparators: [','], 
    placeholder: "Add LOB"
});


</script>

</body>
</html>