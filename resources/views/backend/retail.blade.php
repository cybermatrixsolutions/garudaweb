<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Retail Outlet Management</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Retail Outlet management</a></li>
        </ul>

      </div>
    </div>
      <div class="">

        </div>  
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
           <center>@if(Session::has('success'))
               <font style="color:red">{!!session('success')!!}</font>
            @endif

             @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

          </center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style=" width: 1100px;">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Retail Outlet Management</h4>
                  </div>
                  <div class="modal-body" style="padding: 0px !important;">
                    <div class="row">
                      <form class="form-horizontal" action="{{'add_retail'}}" enctype="multipart/form-data"  method="post">
                        <div class="col-md-6">
                         {{ csrf_field() }}
					    
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Principal Company</label>
                          <div class="col-sm-8">
                            
                           	 <select class="form-control" id="" name="company_code" required/>
                                @foreach($getPrincipal as $row) 
                              <option value="{{$row->company_code}}">{{$row->company_name}}</option>
                              
                               @endforeach
                             </select>
                          </div>
                        </div>
                        <!--<div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">RO Code</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="" name="RO_code" placeholder="RO Code">
                          </div>
                        </div>-->
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Pump Legal Name</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="" name="pump_legal_name" placeholder="Pump Legal Name" required/>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Select Pump Type</label>
                          <div class="col-sm-8">
                            <select class="form-control" name="Ro_type">
                              <option value="1">Normal</option>
                              <option value="2">Convenient</option>
                              <option value="3">Both</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Address Line 1</label>
                          <div class="col-sm-8">
                            <textarea class="form-control" rows="1"  id="comment" name="pump_address" required/></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Address Line 2</label>
                          <div class="col-sm-8">
                            <textarea class="form-control" rows="1"  id="comment" name="address_2"/></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Address Line 3</label>
                          <div class="col-sm-8">
                            <textarea class="form-control" rows="1"  id="comment" name="address_3"/></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Country</label>
                          <div class="col-sm-8">
                             <select class="form-control" disabled="disabled" id="country" name="country">
                               @foreach($countries_list as $country)
                                  <option @if($country->id==101) selected @endif  value="{{$country->id}}"
                                          >{{$country->name}}</option>
                              @endforeach
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">State</label>
                          <div class="col-sm-8">
                             <select class="form-control" id="state" name="state">
                               
                             </select>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">City</label>
                          <div class="col-sm-8">
                             <select class="form-control" id="city" name="city">
                               
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">PIN Code</label>
                          <div class="col-sm-8">
                              <input type="text" class="form-control" id="" name="pin_code" maxlength="6"   pattern="[0-9]{6}" title="Five digit zip code" placeholder="pin code" required/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Phone Number</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="" name="phone_no" placeholder="Phone Number"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Mobile(+91)</label>
                          <div class="col-sm-8">
                            <input type="text" required="required" data-table="tbl_ro_master" data-colum="mobile" data-mass="Mobile No. Already Exit" class="form-control GST_TIN number" id="" pattern="[789][0-9]{9}" name="mobile" placeholder="Mobile" maxlength="10" required/>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="website">Customer Care No.</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="website" name="customer_care" placeholder="Customer Care No." />
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="website">Website</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" name="website" placeholder="Website" />
                          </div>
                        </div>
                        <!--<div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">IMEI No</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="" name="IMEI_No" placeholder="IMEI No" required/>
                          </div>
                        </div>-->
                       
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd"> Email / Login ID</label>
                          <div class="col-sm-8">
                            <input type="email" class="form-control GST_TIN" data-table="tbl_ro_master" data-colum="Email" data-mass="Email Id Already Exit" id="email" name="Email" placeholder="Email" required/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Password</label>
                          <div class="col-sm-8">
                             <input type="password" class="form-control" id="" name="password" placeholder="password">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Lube License</label>
                          <div class="col-sm-8">
                            <input type="text" required="required" class="form-control" id=""  name="Lube_License" placeholder="Lube License" required/>
                          </div>
                        </div>
                         
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Pump License</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="" name="Pump_License" placeholder="Pump License" required/>
                          </div>
                        </div>
                         <div class="form-group">
                            <div class="col-sm-7">
                              <label class="control-label col-sm-7" for="GST Invoice Prefix" >GST Invoice Prefix</label>
                              <div class="col-sm-5">
                                <input type="text" required="required" minlength="1" maxlength="4" class="form-control" id="invoice_prefix"  name="GST_prefix" placeholder="GST Invoice Prefix" required/>
                              </div>
                            </div>
                            <div class="col-sm-5">
                              <label class="control-label col-sm-7" for="Serial No.">Serial No.</label>
                              <div class="col-sm-5">
                                <input type="number" required="required"  class="form-control" value="1" name="GST_serial" placeholder="Serial No." required/>
                              </div>
                            </div>
                          <div class="clearfix"></div>
                     

                        <div class="form-group">
                            <div class="col-sm-7">
                              <label class="control-label col-sm-7" for="VAT Invoice Prefix">VAT Invoice Prefix</label>
                              <div class="col-sm-5">
                                <input type="text" required="required" minlength="1" maxlength="4" class="form-control" id="VAT_prefix"  name="VAT_prefix" placeholder="VAT Invoice Prefix" required/>
                              </div>
                            </div>
                            <div class="col-sm-5">
                              <label class="control-label col-sm-7" for="Serial No.">Serial No.</label>
                              <div class="col-sm-5">
                                <input type="number" required="required" value="1" class="form-control"  name="VAT_serial" placeholder="Serial No." required/>
                              </div>
                            </div>
                          
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">VAT No.</label>
                          <div class="col-sm-8">
                            <input type="text" step="0.01" maxlength="15"  class="form-control" id="" name="VAT_TIN" placeholder="VAT "/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">GST No.</label>
                          <div class="col-sm-8">
    
                            <input type="text" style="text-transform: uppercase" class="form-control GST_TIN" id="gst" name="GST_TIN" placeholder="GST No." pattern="[0-9]{2}[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}[(0-9)(a-z)(A-Z)]{3}" data-table="tbl_ro_master" value="" data-colum="GST_TIN" maxlength="15" data-mass="GST number Already Exit"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">PAN</label>
                          <div class="col-sm-8">
                            <input type="text" style="text-transform: uppercase" pattern="[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}"  class="form-control GST_TIN" id="pan" name="PAN_No" placeholder="PAN No" required maxlength="10" data-table="tbl_ro_master" value="" data-colum="PAN_No" maxlength="10" data-mass="PAN number Already Exit"/>
                          </div>
                        </div>
                      </div>

                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <!--<button type="submit" class="btn btn-default">Submit</button>-->
						                <center><input type="submit" id="success-btn" disabled class="btn btn-primary site-btn submit-button" value="Submit" onclick="return gstAndPan()"></center>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>S.No</th>
                <th>Principal Company Name
                 <th>RO Code</th>
                <th>Pump Legal Name</th>
                <th>Pump Type</th>
                <th>Status</th>
				
                 <th>Actions</th>
              </tr>
            </thead>
            <tbody>
			
			
			<?php $i=0; ?>
			
	               @foreach($getRetail as $row) 
			
	            <?php $i++ ;?>
              <tr>
			      <td>{{$i}}</td>
                <td scope="row">{{$row->company_name}}</td>
                <td>{{$row->RO_code}}</td>
                <td>{{$row->pump_legal_name}}</td>
                <td> @if($row->Ro_type == 1) Normal @endif
                      @if($row->Ro_type == 2) Convenient @endif
                      @if($row->Ro_type == 3) Both @endif 
                           </td>
				        <td> <?php if($row->is_active==1){?>

								   <a  title="Active" href="{{URL('retailDeactive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to deactivate? if you deactive this all relation will be deactivated.');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

									<?php }else{ ?>

									 <a title="Deactive" href="{{URL('retailActive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to activate?');">

									  <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

									  </a>
						 <?php } ?> 
					</td>
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="retails_view/{{$row->id}}">View</a></li>
                       @if($row->is_active==1)

                      <li><a href="retail_update/{{$row->id}}" >Edit</a></li>
					            @endif
                    <!--  <li><a href="retail_delete/{{$row->id}}" onClick="return confirm('Are you sure?');">Delete</a></li>-->
                    </ul>
                  </div></td>
              </tr>
			  @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
<script type="text/javascript">

 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();


              jQuery('form').on('submit',function(){
                   var gst=jQuery('#gst').val();
                   var gst=gst.toUpperCase();
                   var pan=jQuery("#pan").val();
                   var pan=pan.toUpperCase();
                   gst= gst.substring(2,12);
                  


                   if(jQuery.trim(gst)!='' && gst!=pan){
                     alert('Please Enter valid GST');
                      jQuery("#success-btn").attr('disabled',false);

                      return false;
                   }

                   

              });
             
         
           },
           selectRo:function(){

            jQuery('#company_code').change(function(){
                
                  udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique',{
                  table:'tbl_ro_master',
                  colum:'company_code',
                  unik:udat,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                    console.log(data);
                   if(data=='true'){
                      alert('Company code already exists Enter Other!');
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#country').on('change',function(){
                  vil.getcom();
              });
            jQuery('#state').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get('get_country_state',{
                
                rocode:jQuery('#country').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                 

                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#state').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('get_state_city',{
                rocode:jQuery('#state').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';

                   if(data.length==0)
                   {

                     opt+='<option value="'+jQuery('#state option:selected').val()+'">'+jQuery('#state option:selected').text()+'</option>';
                   }
                   else{


                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   }
                jQuery('#city').html(opt);

               });
           },

     }
     vil.init();
  });
</script>
<script>
jQuery(function(){
     var url="{{url('check_Pedestal_Number_unique')}}";
     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.GST_TIN').on('blur',function(){
                  var id=0;

                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   id=jQuery(this).data('id');
                    
                   udat=jQuery(this).val();

                    if(jQuery.trim(udat)=='')
                      return false;

                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);

                    if(colum=='PAN_No')
                      jQuery('.submit-button').attr('disabled',true);

                   }else{

                    if(colum=='PAN_No')
                        jQuery('.submit-button').attr('disabled',false);
                    
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>

<script type="text/javascript">
  function gstAndPan()
  {
    var gst= document.getElementById('gst').val();
    alert(gst);

  }
  $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
</script>
</body>
</html>