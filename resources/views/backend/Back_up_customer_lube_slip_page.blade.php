<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
   @include('backend-includes.header')

        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Customer Slip Entry Item </h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Customer Slip Entry Item</a></li>
        </ul>
      </div>
    </div>

    <div class="row">
      <br/>
      <br/>
      <br/>
       <div class="">
         @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
       @endif
          
           <center>
            @if(Session::has('success'))
               <font style="color:red">{!!session('success')!!}</font>
            @endif
          </center>

        </div>
       <div id="item-div" style="display:none">
           <select class="form-control multiselect-ui addselect" multiple="multiple" required="required" name="item_code[]">
                            @foreach($item_id as $item)
                                <option value="{{$item->id}}"
                                        >{{$item->Item_Name}}
                                      </option>
                            @endforeach
             </select>

        </div>

      <form class="form-horizontal" id="form12" action="{{'save_slip_detail_data'}}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="RO_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
         <!-- <div class="form-group">
          <label class="control-label col-sm-3" for="email">Pump Legal Name</label>
          <div class="col-sm-9">
            <select class="form-control" required="required" name="RO_code" id="RO_code">
             
                                    @foreach($datas as $ro_master)
                                        <option  value="{{$ro_master->RO_code}}"
                                                >{{$ro_master->pump_legal_name}}</option>
                                    @endforeach
            </select>
          </div>
        </div> -->
       
        <table class="table table-striped no-footer">
          <thead>
            <tr>
              <th>Customer Name</th>
              <th>Vehicle Reg No</th>
              <th>Select Item</th>
              <th>Slip No.  <a style="float:right" id="addNew" class="btn btn-success" href="#">+</a></th>
            </tr>
          </thead>
          <tbody id="parant-app-row">
           <tr class='parent-tr' id="parant-row">
            <td>  
              @if(Auth::user()->user_type==4)
                          
                               <input type="hidden" name="customer_code" value="{{ Auth::user()->customer->Customer_Code }}" id="customer_code">{{ Auth::user()->customer->Customer_Code }}

                          @else
                             <select class="form-control"  name="customer_code" id="customer_code">         
                            </select>
                            @endif
            </td>

            <td>
               <select class="form-control" required="required" name="Vehicle_Reg_No" id="registration_number">                
                            </select>

            </td>
            <td class="item-td">
                 <select class="form-control multiselect-ui tags" multiple="multiple" required="required" name="item_code[]" >
                            @foreach($item_id as $item)
                                <option value="{{$item->id}}"
                                        >{{$item->Item_Name}}
                                      </option>
                            @endforeach
                  </select>
            </td>
          
            <td>
               <a href='#' style="position: absolute;right: 7px;margin-top: -9px;" class='deleteNew'>X</a>
               <input type="text" class="form-control slip_detail"   name="slip_detail[]" placeholder="Slip Detail">
             
            </td>
           </tr>
           
          </tbody>
        </table>
      
      
      
        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="button" class="btn btn-default" id="tbnhide">Submit</button>
          </div>
        </div>
      </form>
   

   
 </div>
</div>
<!-- Javascripts--> 

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="{{URL::asset('js/index.js')}}"></script> 
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  /*jQuery(document).ready(function(){
    jQuery('#myTable').DataTable();
});*/
</script>
<script type="text/javascript">
 jQuery(function(){
  

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
              vil.addRow();
              vil.formsub();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#sel1').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
               });
           },
           addRow:function(){
            jQuery('#addNew').on('click',function(e){
              e.preventDefault();
             jQuery('#item-div .addselect').addClass('tags');
              var cnts=jQuery('#item-div').html();
              var cnt=jQuery('#parant-row').html();
              jQuery('#parant-app-row').append("<tr class='parent-tr'>"+cnt+"</tr>");
              jQuery('.item-td').replaceWith('<td class="item-td">'+cnts+'</td>');
              jQuery('#item-div .addselect').removeClass('tags');
                
                 jQuery('.tags').select2({
                    tags: true,
                    tokenSeparators: [','], 
                    placeholder: "Add your tags here"
                });

            });

            jQuery(document).on('click','.deleteNew',function(e){
              e.preventDefault();
              
              jQuery(this).parents('.parent-tr').remove();
                
            });

            
           },

     }
     vil.init();
  });
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
 /*$('.tags').select2({
  
    tags: true,
    tokenSeparators: [','], 
    placeholder: "Add your tags here"
});
*/
$('.tags').select2({
  
    tags: true,
    tokenSeparators: [','], 
    placeholder: "Add your tags here"
});
</script>
</body>
</html>