   <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css"/>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<title>Garruda</title>
<style type="text/css">
	.bootstrap-timepicker-widget.dropdown-menu.timepicker-orient-left.timepicker-orient-top.open {
    width: 2px;
}
th{
 font-size: 12px!important;
}
th,td{
	text-align:center!important;
}
.table-container {
    height:350px;
}
.fixedtable {
    display: flex;
    flex-flow: column;
    height: 100%;
    width: 100%;
}
.fixedtable thead {
    /* head takes the height it requires, 
    and it's not scaled when table is resized */
    flex: 0 0 auto;
    width: calc(100% - 0.9em);
}
.fixedtable tbody {
    /* body takes all the remaining available space */
    flex: 1 1 auto;
    display: block;
    overflow-y: scroll;
}
.fixedtable tbody tr {
    width: 100%;
}
.fixedtable thead,
.fixedtable tbody tr {
    display: table;
    table-layout: fixed;
}


</style>
</head>
  <body class="sidebar-mini fixed">
  <div class="wrapper"> 
    <!-- Navbar-->
    
     @include('backend-includes.header')
   
          <!-- Side-Nav-->
    
      @include('backend-includes.sidebar')
    
    <div class="content-wrapper">
      <div class="page-title">
        <div>
          <h1><i class="fa fa-dashboard"></i>&nbsp; Fuel Price Update
          </h1>
        </div>
        <div>
          <ul class="breadcrumb">
            <li>
              <form name="myForm" method="post" class="form-inline" action="{{url('fuel_price_management')}}">
                 {{ csrf_field() }}
                        
                          
                 <div class="form-group">
                <label for="Filter">Filter</label>
                <input type="text" class="form-control datepickernew"  placeholder="Date" name="date" value="@if(isset($search_date)) {{
                                 Carbon\Carbon::parse($search_date)->format('d/m/Y')}}@endif">
                </div>
                
                <button type="submit" class="btn btn-default " style="margin-right">Go</button>
                        
                     
            </form>
            </li>
            <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
           <!--  <li><a href="{{url('/nozzles')}}
  ">      
  </a></li> -->


          </ul>
        </div>
      </div>
      <div class="">
          
      </div>
`      <div class="row">
              @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
              @endif

  	
      <div class="row">
        <center>
            @if(Session::has('success'))
                       <font style="color:red">{!!session('success')!!}</font>
                    @endif
            </center>
        <div class="col-md-12 col-sm-12">
       
            <div class="col-xs-12 vat">
             <!--  <div class="pull-right"> 
          <div>
            @if(Auth::user()->user_type==3)

                     <form class="form-inline" action="{{'update_csv_datafuel'}}" method="post" enctype="multipart/form-data">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <div class="form-group">
                     <label for="email"File(csv)> </label>
                     <input type="file" id="inputfile" required="required"   placeholder="File(csv)" name="file" style="display: inline; width: auto;">
                     </div>
                     <div class="form-group">
                     <input  type="submit" value="Submit" style="margin-left: -83px;background-color: #00786a;color: #fff;">
                     </div>
                     </form>
                     <a href="getallfuel" class="btn btn-primary">Export as CSV</a>
                     @endif
          </div>
        </div> -->
              
              
              
            @if(isset($fuel_type) && count($fuel_type))

            <form name="myForm" class="form-inline" id="form-prchange" action="{{url('save_fuel_price')}}" enctype="multipart/form-data" method="post">
            	 {{ csrf_field() }}
            	<div class="row">
                <div class="col-md-12">
                  <div>
				  <div class="table-container">	
                    <table class="table fixedtable table-striped">
                      <thead>
                        <tr>
                          <th>Fuel</th>
                          <th>Old Price<br>(Tax Included)</th>
                          <th>Old Date/Time</th>
                         
                          <th>Current Price</th>
                            <th>Date/Time</th>



                          <th>New Update Price<br>(Tax Included)</th>
                           
                        </tr>
                      </thead>
                      <tbody>
                        <tr>

                          <td> Date Time </td>
                           <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                           
                           
                            <td> 
                            <input type="text" style="width: 60%;margin-bottom:10px;" class="form-control datepickernew" value="@if(isset($search_date)){{
                                 Carbon\Carbon::parse($search_date)->format('d/m/Y')}}@endif"  placeholder="Date" name="effective_date">
                           <input type="text" id="timepicker1" style="width: 60%;" class="form-control "   placeholder="Time" name="time">
                            </td>
                        </tr>
                         
                      	@foreach($fuel_type as $fuel)
                           
                        <tr>

                          <td>
                          	@if(isset($fuel->getpricelist))
                          	 <input type="hidden" name="namenew[]"  value="{{$fuel->getpricelist->id}}" >{{$fuel->getpricelist->Item_Name}}
                          	@else
                          	    <input type="hidden" name="namenew[]"  value="{{$fuel->id}}" > {{$fuel->Item_Name}}
                          	@endif

                           <!--  @if(isset($fuel->getpricelist))
                            <input type="hidden" name="namenew[]" value="{{$fuel->getpricelist->id}}">
                            @else
                               <input type="hidden" name="namenew[]" value="{{$fuel->id}}">
                            @endif -->
                          </td>
                          <td>
                            @if(isset($fuel->getpricelist))
                            <input type="number" step="0.01" style="width:50%" value="@if(isset($oldprice[$fuel->getpricelist->id])){{bcadd($oldprice[$fuel->getpricelist->id]['price'], 0, 2)}}@endif" disabled>
                            @else

                               <input type="number" step="0.01" style="width:50%" disabled value="@if(isset($oldprice[$fuel->id])){{bcadd($oldprice[$fuel->id]['price'], 0, 2)}}@endif">
                            @endif
                            
                          </td>
                           
                           
                           
                             <td>

                              @if(isset($fuel->getpricelist))

                                @if(isset($oldprice[$fuel->getpricelist->id]))
                                  <input type="text" class="form-control inputwith" value="@if(isset($oldprice[$fuel->getpricelist->id])){{
                                     Carbon\Carbon::parse($oldprice[$fuel->getpricelist->id]['effective_date'])->format('d/m/Y H:i:s A')}}@endif 
                                 "  placeholder="Date" name="Effective_Date" disabled>
                                 @else
                                  <input  type="text" class="form-control inputwith" value=""  placeholder="Date" name="Effective_Date" disabled>
                               
                                 @endif
                               @else
                                   @if(isset($oldprice[$fuel->id]))
                                    <input type="text" class="form-control inputwith" value="@if(isset($oldprice[$fuel->id])){{
                                       Carbon\Carbon::parse($oldprice[$fuel->id]['effective_date'])->format('d/m/Y H:i:s A')}}@endif 
                                   "  placeholder="Date" name="Effective_Date" disabled>
                                   @else
                                    <input  type="text" class="form-control inputwith" value=""  placeholder="Date" name="Effective_Date" disabled>
                                 
                                   @endif

                                @endif
                             </td>
                          
                            
                             
                          
                           <td>
                            @if(isset($fuel->getpricelist))
                            <input type="number" step="0.01" style="width:50%" value="{{bcadd($fuel->price, 0, 2)}}" disabled>
                            @else

                               <input type="number" step="0.01" style="width:50%" disabled value="@if(isset($fuel->price)){{bcadd($fuel->price->price, 0, 2)}}@endif">
                            @endif
                            
                          </td>

                          <td> 
                           
                           @if($fuel->price!=null & isset($fuel->price->effective_date))

                              <input type="text" class="form-control inputwith" value="{{
                                 Carbon\Carbon::parse($fuel->price->effective_date)->format('d/m/Y H:i:s A')}} 
                             "  placeholder="Date" name="Effective_Date" disabled>
                          
                            @else
                              <input  type="text" class="form-control inputwith" value="{{
                                 Carbon\Carbon::parse($fuel->effective_date)->format('d/m/Y H:i:s A')}}"  placeholder="Date" name="Effective_Date" disabled>
                             
                            @endif
                            </td>
                        
                           
                          <td>
                          	@if(isset($fuel->getpricelist))
                          	<input type="text" class="numeriDesimal" style="width:50%" name="Price[]" value="{{bcadd($fuel->price, 0, 2)}}">

                          	@else

                          	   <input type="text" class="numeriDesimal" style="width:50%"  name="Price[]" value="@if(isset($fuel->price)){{bcadd($fuel->price->price, 0, 2)}}@endif">
                               
                          	@endif
                          	 @if(isset($fuel->getpricelist))
                            <input type="hidden" name="namenewaa[]"  value="{{$fuel->getpricelist->id}}" >
                            @else
                               <input type="hidden" name="namenewaa[]"  value="{{$fuel->id}}" >
                            @endif
                          </td>
                          
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
               </div>

                @if(!isset($search_date))
                @if($getcur->count()==0)
                  <button type="button" class="btn btn-default pull-right button-prchange">Submit</button>
                @endif
                @endif

            </form>
            @else
              <h2 style="text-align: center;"> No Record Found </h2>

            @endif
      
          </div>
                        
              
            </div>

          </div>
        </div>
      </div>
     
    </div>
</div>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="js/index.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.js">
</script>

<script type="text/javascript">
            $('#timepicker1').timepicker({
              showSeconds:true,

              defaultTime:'06:00:00 AM'
            });

            $(function () {
              var date = new Date();
              var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
              var end = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            $(".datepickernew").datepicker({ 
                  autoclose: true, 
                  minDate: "0",
                  todayHighlight: true,
                  format: 'dd/mm/yyyy',
                  endDate: "today",
                  defaultDate:'dd/mm/yyy',
                  //startDate: '-0m'
                 
                  
            });
           // $(".datepickernew").datepicker("setDate", today);
          });
        </script>
        <script type="text/javascript">
          
                    jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });
             
        </script>

       <script type="text/javascript">
        jQuery(function(){

          var app={
                  init:function(){
                    app.chechPrice();
                  },
                  chechPrice:function(){
                    jQuery('.button-prchange').on('click',function(){
                      var flg=false;
                        jQuery('.table-container').find('.numeriDesimal').each(function(){

                          var p= parseInt(jQuery(this).val());
                            
                            if(isNaN(p) || p<=0){
                              flg=false;
                              jQuery(this).css('border-color','#ff1a1a');
                              console.log(p);
                               console.log('hello');
                            }else{
                              jQuery(this).css('border-color','');
                              flg=true;
                            }
                           
                        });
                      
                         if(flg==false){
                            alert('Please Enter Price of Item');
                         }

                         if(flg){
                             
                              if(confirm("Do you want to proceed ?")){
                                jQuery('#form-prchange').submit();
                              } 
                         }

                    });
                  },
          };
          app.init();

        })

       </script>
</body>
</html>