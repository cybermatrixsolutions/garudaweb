@extends('backend-includes.app')

@section('content')
   <style type="text/css">
  span.select2.select2-container.select2-container--default.select2-container--below{
     width: 100%;
     }
     .select2-container--default .select2-selection--multiple {
    
    width: 365px;
}
   </style>
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Services Master</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Services Master</a></li>
        </ul>
      </div>

    </div>
    <div class="">
      
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" >Service</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
            <form class="form-horizontal"  action="{{Route('itemservices')}}"  method="post" onsubmit="return confirm('Do you want to Continue?');">
                          {{ csrf_field() }}
                          <input type="hidden" name="Ro_code" id="pump" class="form-control" value="{{Auth::user()->getRocode->RO_code}}">
                         <!--  <div class="form-group">
                            <label class="control-label col-sm-4" for="Type">Pump Legal Name</label>
                            <div class="col-sm-8">
                          <select name="Ro_code" id="pump" class="form-control">
                            	@foreach($data1 as $RoMaster)
                              		<option value="{{$RoMaster->RO_code}}"> {{$RoMaster->pump_legal_name}}</option>
                                @endforeach
                            </select>
                            </div>
                         </div> -->
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="Reading">Services Name</label>
                          <div class="col-sm-6">
                             <input type="text"  name="ServicesName" class="form-control"  placeholder="Services Name">
                           </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="Reading">HSN/SAC Code <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-6">
                             <input type="text" name="hsn_code" class="form-control" required  placeholder="HSN/SAC Code">
                           </div>
                        </div>
                         
                         <div class="form-group">
                            <label class="control-label col-sm-4" for="Type">Select Tax Type</label>
                            <div class="col-sm-8">
                          <select name="taxtype" id="taxtype"  class="form-control">
                             
                                  <option value="GST">GST</option>
                                                                
                            </select>
                            </div>
                         </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="TankReading volume">Select Tax <span style="color:#f70b0b;">*</span></label>
                           <div class="col-sm-8">
                            <select class="form-control" id="tagsq" multiple="multiple" required="required" name="tax[]">
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <input  type="submit" class="btn btn-default submit-button"  value="Submit">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--end add box-->
    <!--list contant-->
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>S.No.</th>
                 <th>Services Name</th>
                 <th>Date</th>
                 <th>Tax</th>
            </thead>
            <tbody>
              <?php $i=0; ?>
      
               @foreach($services as $row) 
      
              <?php $i++ ;?>
               <tr>
                <td>{{$i}}</td>
                <td>{{$row->name}}</td>
                <td>{{Carbon\Carbon::parse($row->created_at)->format('d/m/Y')}}</td>
                <td>@if($row->gettax!=null) <ul style="background: #ededf5;"> @foreach($row->gettax as $tax) <li ><span class="left">{{$tax->Tax_Code}} </span><span class="right"> {{$tax->Tax_percentage}}%</span></li> @endforeach </ul>  @endif</td>
                
                </tr>
               @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
             vil.getcrg();
             vil.gettaxtype();
           },
           selectRo:function(){
            jQuery('#pump').on('change',function(){
                  vil.getcrg();
              });
            jQuery('#taxtype').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcrg:function(){
            
             jQuery.get('getrocodesevices',{
                rocode:jQuery('#pump').val(),
                taxtype:jQuery('#taxtype').val(),

                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';

                  


                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                  
                jQuery('#tagsq').html(opt);

               });
             $('#tagsq').select2({
  data: [],
    tagsq: true,
    tokenSeparators: [','], 
    placeholder: "Select Tax"
});
           },

     }
     vil.init();
  });
</script>


@endsection