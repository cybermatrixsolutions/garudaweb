
    <!-- Bootstrap -->
  
	<!--font family-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">

	<style>
    <?php $uniuntiretary=['AN','CH','DN','DD','DL','LD','PY'];
      $rostate='';
      $cusstate='';

    ?>
body{
	margin:0px;
	padding:0px;
	background:#fff;
	font-family: "Open Sans", sans-serif;
	font-size:14px;
	color:#222;

}
h1,h2,h3,h4,h5,h6{
	margin:0px;
}
.padding{
	padding-right:10px;
}
.header_top{
	background:#;	
}
.header_top h1{
	font-size:24px;
	font-weight:600;
	padding:15px 0 ;
}
.titlehead{
	
	margin:0 auto;
	margin-bottom:10px !important;
}
.logosection{
	background:#fff;
	padding:20px;
}
.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    padding: 4px;
    line-height: 1.6;
    vertical-align: top;
    /* border-top: 1px solid #eee; */
    border: 1px solid #444;
	
}

address{
	margin-bottom:0px;
}
small{
	font-size:10px !important;
}
.clear{
	
	clear:both;
}
.vertical{

	vertical-align: top !important;
}

</style>

<table class="table" width="100%">

  <tbody>
  	 <tr rowspan="2">
  	 	<td colspan="11" align="center"><h1>Tax Invoice</h1></td>
  	 </tr>
    <tr>

      <td rowspan="2"  width="20px"><a href="#"><img src="{{URL::asset('images')}}/logo.png"></a></td>
      <td rowspan="2" colspan="2">
		
         
         <p style="line-height:24px">
			<strong>{{Auth::user()->getRocode->pump_legal_name}}</strong><br>
			{{Auth::user()->getRocode->pump_address}}<br>
			{{Auth::user()->getRocode->getcity->name}} - {{Auth::user()->getRocode->pin_code}}<br>
		   GSTIN/UIN: {{Auth::user()->getRocode->GST_TIN}}<br>
			State Name : {{Auth::user()->getRocode->getstate->name}}<br>
			<?php $rostate=Auth::user()->getRocode->getstate->statecode; ?>
		
		
	   </p>
	</td>
      <td colspan="4">Invoice No.<br><strong><?php echo  $invoice_no;?></strong></td>
      <td colspan="4">Dated<br><strong><?php echo date(' d/M/Y'); ?>
     </strong>
	  </td>
   </tr>
    <tr >
      <td   colspan="5" >Mode/Terms of Payment </td>
      <td  colspan="3" >Immediate</td>
    </tr>
	
	<!--middle-->
	
	<tr>
     
      <td  colspan="11">
		
		 <p style="line-height:24px">
		Buyer<br>
		
			<strong>{{$tbl_invoice->company_name}}</strong><br>
			{{$tbl_invoice->address_one}}<br>
			@if(trim($tbl_invoice->address_two)!='')
			{{$tbl_invoice->address_two}}<br>
			@endif

			@if(trim($tbl_invoice->address_three)!='')
			{{$tbl_invoice->address_three}}<br>
			@endif
             
			{{$tbl_invoice->getcity->name}} - {{$tbl_invoice->pin_no}}<br>

			Contact : Mr/Mrs. {{$tbl_invoice->Customer_Name}} &nbsp;&nbsp;&nbsp; Ph : {{$tbl_invoice->Phone_Number}}<br>
			GSTIN/UIN: {{$tbl_invoice->gst_no}}<br>
			State Name : {{$tbl_invoice->getstate->name}}<br>
			Place of Supply : {{$tbl_invoice->getstate->name}}<br>
			<?php $cusstate=$tbl_invoice->getstate->statecode;?>
		</p>
		</td>
      
   
   
</tbody>
</table>

          	<table class="table" style=" background:#fff;">
	
	<tbody>

	<tr>
      <td align="right">SI No.</td>
      <td width="40%">Description of Goods</td>
      <td width="3%">HSN/SAC</td>
	  <td align="center"> GST %</td>
      <td align="center">MRP</td>
	  <td align="center">VAT %</td>
      <td align="center">QTY</td>
	  <td align="center">Rate</td>
      <td align="center">per</td>
	  <td align="center">Disc. %</td>
      <td align="right" width="20%" style="padding-right:15px">Amount (Rs.)</td>
   
    </tr>

	<tr>
      <td ></td>
      <td><b><small>Indent No / Transaction ID / Date / Vehicle No. / Qty/</small></b></td>
      <td></td>
	  <td></td>
      <td></td>
	  <td></td>
      <td></td>
	  <td></td>
      <td></td>
	  <td></td>
      <td></td>
   
    </tr>
   <?php $petroldiesel_qty=0; $i=1; $amount=0; $y=0; $z=0;?>
   <?php $gst=[]; $taxblevalue=[];?>
   @foreach($transactionModels as $key=>$transactionModel)
 
		<tr>
	      <td ><?php echo $i;  $i++; ?></td>
	      <td> <!--Description of Goods-->
	      	<strong>{{$key}}</strong><br>
			
				@foreach($transactionModel as $singletransactionModel)
				    <?php $y++; $z++; ?>

					{{$singletransactionModel->Request_id}} / {{date('d/m/Y',strtotime($singletransactionModel->trans_date))}}/{{$singletransactionModel->Vehicle_Reg_No}}<br>
				@endforeach
			
		  </td>
	      <td align="right" ><br/>
	      	<!--HSN/SAC-->
	      	    @foreach($transactionModel as $singletransactionModel)
					{{$singletransactionModel->hsncode}}<br>
				@endforeach
	      </td>
		  <td align="right" ><br/>
		  	<!--GST %-->
		  	  @foreach($transactionModel as $singletransactionModel)
			    
               <?php $pargst=0;?>
                 @if($rostate==$cusstate)
                     <!--same state ro and customer-->
		             @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereNotIn('GST_Type',['SPCESS','SCCESS'])->whereIn('strate_type',['intrastate','both']) as $tax)
	                          <?php $pargst=$pargst+$tax->tax_name; ?>
					 @endforeach

				 @else
                    <!--diff state ro and customer-->
				    @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereNotIn('GST_Type',['SPCESS','SCCESS'])->whereIn('strate_type',['interstate','both']) as $tax)
	                          <?php $pargst=$pargst+$tax->tax_name; ?>
					 @endforeach

				 @endif
                         @if($pargst!=0)
	                        {{$pargst}}% <br/>
	                      @endif 
			@endforeach
		  </td>
	      <td align="right" >
	      	<br/>
	      	<!--MRP/ Marginal-->
	      	@foreach($transactionModel as $singletransactionModel)
				{{$singletransactionModel->item_price}}<br>
			@endforeach

	      </td>
		  <td>
		  	<!---VAT %-->
		  	
		  </td>
	      <td align="right" >
	      	<br/>
	      	<!--Quantity-->
	      	@foreach($transactionModel as $singletransactionModel)
			1<br>
			<?php $petroldiesel_qty=$petroldiesel_qty+1;?>
			@endforeach
	      	
		  <td>
		  	<!--Rate-->
		  	
		  </td>
	      <td>
	      	<!--per-->
	      	@foreach($transactionModel as $singletransactionModel)
			<!-- {{$singletransactionModel->petroldiesel_type}} --><br>
			@endforeach
	      </td>
		  <td><!--Disc. %-->
		  </td>
	      <td align="right" style="padding-right:15px">
	      	<br/>
	      	<!--Amount-->
             
	      	@foreach($transactionModel as $singletransactionModel)
			     <?php $pargst=0;?>
               
                 @if($rostate==$cusstate)
                     <!--same state ro and customer-->
		             @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both']) as $tax)
	                          <?php $pargst=$tax->Tax_percentage+$pargst; ?>
					 @endforeach

				 @else
                    <!--diff state ro and customer-->
				    @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both']) as $tax)
	                          <?php $pargst=$tax->Tax_percentage+$pargst; ?>
					 @endforeach

				 @endif
                        
                        <?php $sub=0;?>

				        <?php if($pargst!=0){

				        			$sub=$singletransactionModel->item_price/(1+$pargst/100);
				                }else{

				                	$sub=$singletransactionModel->item_price;
				                } 
				        ?>
							
							<?php $taxblevalue[$singletransactionModel->id]=$sub;
							 $amount=$sub+$amount;?>

						  {{bcadd($sub, 0, 2)}}<br/>
			@endforeach

	      	
	      	
	      </td>
	   
	    </tr>

	@endforeach
	
	<tr>
      <td></td>
      <td>
			<table style="width:100%">
            <tr>
              <td>
              	<table style="width:100%">
                  <tr>
                    <td class="vertical"><strong style="font-weight:500">Less :</strong></td><br>
                    </tr>
                    <tr>
                    <td class="vertical" style="padding-top:-10px"><strong style="font-weight:500">Add :</strong></td>
                  </tr>
                  
              	</table>
			   </td>

				

					
					 @foreach($transactionModels as $key=>$transactionModel)
      
						@foreach($transactionModel as $singletransactionModel)
						    
			                 @if($rostate==$cusstate)
			                     <!--same state ro and customer-->
					             @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both']) as $tax)
				                          <?php $gst[$tax->Description]=$tax->position;?>
								 @endforeach

							 @else
			                    <!--diff state ro and customer-->
							    @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both']) as $tax)
				                          <?php $gst[$tax->Description]=$tax->position; ?>
								 @endforeach

							 @endif
			                        
			                  
						@endforeach

					@endforeach
					
						<?php asort($gst)?>
					
				<td style="padding-top:18px;">
				<table style="width:100%">
					<tr>
                    <td align="right" style="font-size:14px"><strong>Discount</strong></td>
                    </tr>
                    @foreach($gst as $key=>$gsts)
						
						<tr>
                         <td style="font-size:13px" align="right"><strong>{{$key}}</strong></td>
                         </tr>
                    @endforeach
                   
                  
				</table>

			</td>
             </tr>

          </table>
	  </td>
      <td><br><br><br>
			<table style="width:100%">
			<tr><td>&nbsp;</td></tr>
            <tr>
              <td></td>
            </tr>
          </table>
	  </td>
	  <td>
	  <br><br><br>
			<table style="width:100%">
			<tr><td>&nbsp;</td></tr>
            <tr>
              <td></td>
            </tr>
          </table>
	  </td>
      <td></td>
	  <td></td>
      <td></td>
	  <td></td>
      <td></td>
	  <td></td>
      <td>
		<table style="width:100%">
            <tr style="width:100%">
            	<?php $tamount=$amount;
            	$gstprice=[];?>
             
					 @foreach($transactionModels as $key=>$transactionModel)
      
						@foreach($transactionModel as $singletransactionModel)
						    
			                 @if($rostate==$cusstate)
			                     <!--same state ro and customer-->
					             @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both']) as $tax)
				                          <?php 
				                          $itemtaval=$taxblevalue[$singletransactionModel->id];
				                           if(isset($gstprice[$tax->Description])){
				                          	   
                                               $gstprice[$tax->Description]=$gstprice[$tax->Description]+$taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100;
				                          
				                          }else{
				                          	
				                          		$gstprice[$tax->Description]=$taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100; 
				                          }
				                          ?>
								 @endforeach

							 @else
			                    <!--diff state ro and customer-->
							    @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both']) as $tax)
				                  <?php  
				                        $itemtaval=$taxblevalue[$singletransactionModel->id];
				                       
				                        if(isset($gstprice[$tax->Description])){
				                          	   
                                               $gstprice[$tax->Description]=$gstprice[$tax->Description]+$itemtaval*$tax->Tax_percentage/100;
				                          
				                          }else{
				                          	
				                          		$gstprice[$tax->Description]=$itemtaval*$tax->Tax_percentage/100; 
				                          }
				                          ?>
				                @endforeach

							 @endif
			                        
						@endforeach

					   @endforeach
                 <td> 
                 	<table style="width:100%">
                 		<tr style="width:100%"><td align="right" style="padding-right:11px"><strong>{{bcadd(round($tamount,2), 0, 2)}}</strong></td></tr>
                 		<tr style="width:100%"><td align="right" style="padding-right:11px"><strong>{{bcadd(round($discount,2), 0, 2)}}</strong></td></tr>
                 		@foreach($gst as $key=>$gsts)
                 		<?php $amount=$gstprice[$key]+$amount?>
                 		<tr style="width:100%"><td align="right" style="padding-right:11px"><strong>{{bcadd(round($gstprice[$key],2), 0, 2)}}</strong></td></tr>
                 		@endforeach
                 	</table>
                 	
              </td>
            </tr>
          </table>
	  </td>
	
    </tr>
	<tr>
      <td ></td>
      <td align="right">Total</td>
      <td></td>
	  <td></td>
      <td></td>
	  <td></td>
      <td align="right" style="padding-right:11px"><strong>{{$petroldiesel_qty}}</strong></td>
	  <td></td>
      <td></td>
	  <td></td>
      <td align="right" style="padding-right:11px"><strong>{{bcadd(round($amount-$discount,2), 0, 2)}}</strong></td>
	
    </tr>
  </tbody>
</table>


	<!--main section-->
	

<!--footer section-->
<?php 
   /* $sums=round($amount,2);          
   $no = round($sums);
   $point = round($sums - $no, 2) * 100;*/

    $sums=round($amount-$discount,2); 
     $va=explode('.',$sums);
     if(isset($va[1]) && intval($va[1])>50){
       $no = round($sums);
       $no = $no-1;
       $point = intval($va[1]);
     }else{
     	$no = round($sums);

     	if(isset($va[1]))
     	  $point = intval($va[1]);
        else
     	 $point = 0;
     }
   
   
   
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array('0' => '', '1' => 'One', '2' => 'Two',
    '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
    '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
    '10' => 'Sen', '11' => 'Eleven', '12' => 'Twelve',
    '13' => 'Thirteen', '14' => 'Fourteen',
    '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
    '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
    '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
    '60' => 'Sixty', '70' => 'Seventy',
    '80' => 'Eighty', '90' => 'Ninety');
   $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $sums = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($sums) {
        $plural = (($counter = count($str)) && $sums > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($sums < 21) ? $words[$sums] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($sums / 10) * 10]
            . " " . $words[$sums % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode('', $str);
  $points = ($point) ?
    "." . $words[$point / 10] . " " . 
          $words[$point = $point % 10] : '';

			 ?>
	<?php $data=[];?>

    @foreach($transactionMs as $key=>$transactionModel)
			
			@foreach($transactionModel as $singletransactionModel)
			   
			   <?php 

			       $data[$singletransactionModel->hsncode]=array(

			       												);
			   ?>
	
	     @endforeach
     @endforeach
	
<table class="table" width="100%" >
	
	<tbody>
	<tr>
      <td  colspan="{{(count($gst)*2)+7}}" >
	  <div class="pull-left">
		Amount Chargeable (in words)<br/><strong>
		@if($points)
		  INR  <?php  echo $result . "Rupees  " . str_replace('.','And ', $points) . " Paise"; ?> Only</strong>
		@else
		  INR  <?php  echo $result . "Rupees  "; ?> Only</strong>
		@endif
	  </div>
	  <div class="pull-right"><i>E. & O.E</i></div><br/><br/><br/>
		
	 </td>
     
   
    </tr>

	<tr>
    
      <td colspan="5" rowspan="2" align="center" width="40%"><strong>HSN/SAC</strong><br></td>
      <td rowspan="2" align="center">Taxable Value (Rs.)</td>
      @foreach($gst as $key=>$gsts)
      <?php $gst[$key]=0; ?>
	  <td colspan="2" align="center">{{$key}}</td>
	  @endforeach
      
	  <td  rowspan="2" align="right" style="padding-right:15px">Total Tax Amount (Rs.)</td>
    
    </tr>
	<tr>

	 @foreach($gst as $key=>$gsts)
	  <td>Rate</td>
      <td align="right" style="padding-right:5px" >Amount (Rs.)</td>
	  @endforeach
    </tr>
     @foreach($transactionMs as $key=>$transactionModel)
 
			<tr>
				<td colspan="5" >{{$key}}</td>
			
			  
		      <td align="right" style="padding-right:15px"> 
		      	   <?php $tottex=0;?>
		      	   @foreach($transactionModel as $singletransactionModel)
		      	   <?php $tottex=$taxblevalue[$singletransactionModel->id]+$tottex;?>
		      	    
		      	   @endforeach
		      	   {{round($tottex,2)}}
		      </td>
              
		      @foreach($gst as $key=>$gsts)
                         
					  <td align="right" style="padding-right:5px"><!--tax %-->
					  	<?php $tottex=0;?>
		                    @foreach($transactionModel as $singletransactionModel)
                                
				      	       @if($rostate==$cusstate)
				                     <!--same state ro and customer-->
						             @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both'])->where('Description', $key) as $tax)
					                          <?php $tottex=$tax->Tax_percentage;  ?> 
					                          <?php $gst[$key]=($taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100)+$gst[$key]; ?>
									 @endforeach

								 @else
				                    <!--diff state ro and customer-->
								    @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both'])->where('Description', $key) as $tax)
					                          <?php $tottex=$tax->Tax_percentage;  ?> 
					                          <?php $gst[$key]=($taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100)+$gst[$key]; ?>
									 @endforeach

								@endif

				      	    @endforeach
				      	    {{round($tottex,2)}} %
					  </td>

				      <td align="right" style="padding-right:5px"> <!--amout -->
                           <?php $tottex=0;?>
		                    @foreach($transactionModel as $singletransactionModel)

				      	       @if($rostate==$cusstate)
				                     <!--same state ro and customer-->
						             @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both'])->where('Description', $key) as $tax)
					                             <?php $tottex=($taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100)+$tottex; ?>
									
									 @endforeach

								 @else
				                    <!--diff state ro and customer-->
								    @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both'])->where('Description', $key) as $tax)
					                        
					                        <?php $tottex=($taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100)+$tottex; ?>

									 @endforeach

								@endif
								
				      	    @endforeach
				      	    {{bcadd($tottex, 0, 2)}} 
				      </td>
			     @endforeach
			  
			   
		      <td align="right" style="padding-right:15px"> 
		      	   <?php $itamount=0;?>
		      	   @foreach($transactionModel as $singletransactionModel)
                               
				      	       @if($rostate==$cusstate)
				                     <!--same state ro and customer-->
						             @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both']) as $tax)
					                          <?php $itamount=($taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100)+$itamount; ?> 
									            
									 @endforeach

								 @else
				                    <!--diff state ro and customer-->
								    @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both']) as $tax)
					                    <?php $itamount=($taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100)+$itamount; ?> 
									    
									 @endforeach

								@endif
								
                             
				    @endforeach
				    {{ bcadd($itamount, 0, 2)}}<br/>
		      </td>
		   
	      
	      </tr>
     @endforeach
	
	<tr>
	  <td colspan="5"   align="right"><strong>Total</strong></td>
      <td></td>
	    <?php $taxamount=0;?>
         @foreach($gst as $key=>$gsts)
         <?php $taxamount=$gsts+$taxamount;?>
		  <td></td>
          <td align="right" style="padding-right:15px"><b>{{round($gsts,2)}}</b></td>
		  @endforeach
       <td align="right" style="padding-right:12px"><b>{{bcadd($taxamount, 0, 2)}} </b></td>
    </tr>

    <!--vihicle vies tax-->
    <tr style="border-style:none; border-left: 1px solid ;border-right: 1px solid ;">
	  <td style="border-style:none; border-left: 1px solid ;border-right: 1px solid ;" colspan="{{(count($gst)*2)+7}}"> &nbsp;</td>
    </tr>
     <tr style="border-style:none; border-left: 1px solid ;border-right: 1px solid ;">
	  <td style="border-style:none; border-left: 1px solid ;border-right: 1px solid ;" colspan="{{(count($gst)*2)+7}}"> &nbsp;</td>
    </tr>
    <tr>
    
      <td colspan="5" rowspan="2" align="center"><strong>VEHICLE NO.</strong><br></td>
      <td rowspan="2" align="center">Taxable Value (Rs.)</td>
      @foreach($gst as $key=>$gsts)
      <?php $gst[$key]=0; ?>
	  <td colspan="2" align="center">{{$key}}</td>
	  @endforeach
      
	  <td rowspan="2" align="right"  style="padding-right:15px">Total Tax Amount (Rs.)</td>
    
    </tr>
	<tr>

	 @foreach($gst as $key=>$gsts)
	  <td>Rate</td>
      <td align="right" style="padding-right:5px">Amount (Rs.)</td>
	  @endforeach
    </tr>
     @foreach($transactionvihicles as $key=>$transactionModel)
 
			<tr>
				<td colspan="5">{{$key}}</td>
			
			  
		      <td align="right" style="padding-right:15px"> 
		      	   <?php $tottex=0;?>
		      	   @foreach($transactionModel as $singletransactionModel)
		      	   <?php $tottex=$taxblevalue[$singletransactionModel->id]+$tottex;?>
		      	    
		      	   @endforeach
		      	   {{round($tottex,2)}}
		      </td>
              
		      @foreach($gst as $key=>$gsts)
                         
					  <td align="right" style="padding-right:5px"><!--tax %-->
					  	<?php $tottex=0;?>
		                    @foreach($transactionModel as $singletransactionModel)
                                
				      	       @if($rostate==$cusstate)
				                     <!--same state ro and customer-->
						             @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both'])->where('Description', $key) as $tax)
					                          <?php $tottex=$tax->Tax_percentage;  ?> 
					                          <?php $gst[$key]=($taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100)+$gst[$key]; ?>
									 @endforeach

								 @else
				                    <!--diff state ro and customer-->
								    @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both'])->where('Description', $key) as $tax)
					                          <?php $tottex=$tax->Tax_percentage;  ?> 
					                          <?php $gst[$key]=($taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100)+$gst[$key]; ?>
									 @endforeach

								@endif

				      	    @endforeach
				      	    {{round($tottex,2)}} %
					  </td>

				      <td align="right" style="padding-right:5px"> <!--amout -->
                           <?php $tottex=0;?>
		                    @foreach($transactionModel as $singletransactionModel)

				      	       @if($rostate==$cusstate)
				                     <!--same state ro and customer-->
						             @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both'])->where('Description', $key) as $tax)
					                             <?php $tottex=($taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100)+$tottex; ?>
									
									 @endforeach

								 @else
				                    <!--diff state ro and customer-->
								    @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both'])->where('Description', $key) as $tax)
					                        
					                        <?php $tottex=($taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100)+$tottex; ?>

									 @endforeach

								@endif
								
				      	    @endforeach
				      	    {{bcadd($tottex, 0, 2)}} 
				      </td>
			     @endforeach
			  
			   
		      <td align="right" style="padding-right:15px"> 
		      	   <?php $itamount=0;?>
		      	   @foreach($transactionModel as $singletransactionModel)
                               
				      	       @if($rostate==$cusstate)
				                     <!--same state ro and customer-->
						             @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both']) as $tax)
					                          <?php $itamount=($taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100)+$itamount; ?> 
									            
									 @endforeach

								 @else
				                    <!--diff state ro and customer-->
								    @foreach($singletransactionModel->getItemName->gettex->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both']) as $tax)
					                    <?php $itamount=($taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100)+$itamount; ?> 
									    
									 @endforeach

								@endif
								
                             
				    @endforeach
				    {{ bcadd($itamount, 0, 2)}}<br/>
		      </td>
		   
	      
	      </tr>
     @endforeach
	
	<tr>
	  <td colspan="5"  align="right"><strong>Total</strong></td>
      <td></td>
	    <?php $taxamount=0;?>
         @foreach($gst as $key=>$gsts)
         <?php $taxamount=$gsts+$taxamount;?>
		  <td></td>
          <td align="right" style="padding-right:15px"><b>{{round($gsts,2)}}</b></td>
		  @endforeach
       <td align="right" style="padding-right:15px"><b>{{bcadd($taxamount, 0, 2)}}</b></td>
    </tr>
    

	<tr>
	  <td colspan="{{(count($gst)*2)+7}}">Terms & Conditions :</td>
    </tr>
	<tr style="border-bottom: 1px solid ;">
	  
	  <td colspan="{{(count($gst)*2)+2}}" rowspan="3" style="padding:20px 8pxborder-bottom: 1px solid #444 !important;">Terms & Conditions :</td>
	  <td colspan="5" rowspan="3" style="padding:20px 8px;border-bottom: 1px solid #444 !important;"><strong>for {{Auth::user()->getRocode->pump_legal_name}} @ {{Auth::user()->getRocode->pump_address}}</strong>
	  <br><br>
	  <i>Authorised Signatory</i>
	  </td>
    </tr>
	

  </tbody>
</table>

<p style="text-align:center">This is a Computer Generated Invoice</p>

				
		
