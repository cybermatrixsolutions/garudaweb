<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Invoice</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
 <!-- Navbar-->
  
    @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Manager Management</h1>
      </div>
      <div>
        <ul class="breadcrumb">
           <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Manager List</a></li>
        </ul>
      </div>
    </div>
    
                           <div class="">  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
      
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Manager Name</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal" action="{{url('manager/create')}}" method="post">
                         {{ csrf_field() }}
                        
                      <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">RO Code</label>
                          <div class="col-sm-9">
                            <select name="RO_code" id="sel1" class="form-control">
                            	@foreach($data1 as $row) 
                              <option value="{{$row->RO_code}}">{{$row->RO_code}}</option>
                               @endforeach
                            </select>
                          </div>
                        </div>
                        
                       
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Manager Name</label>
                          <div class="col-sm-9">
                            <input type="text" required="required" class="form-control" name="manager_name" id="manager_name" placeholder="Manager Name ">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Phone Number</label>
                          <div class="col-sm-9">
                            <input type="text"  class="form-control" name="manager_phone" id="manager_phone" placeholder="Phone Number ">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Mobile</label>
                          <div class="col-sm-9">
                            <input type="text" pattern="[789][0-9]{9}"  required="required" class="form-control" name="manager_mobile" id="manager_mobile" placeholder="Mobile">
                          </div>
                        </div>

                        <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">IMEI No</label>
                          <div class="col-sm-9">
                            <input type="text"   class="form-control" name="manager_IMEI" id="IMEI_No" placeholder="IMEI No">
                          </div> -->
                        <!-- </div> -->

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Email</label>
                          <div class="col-sm-9">
                            <input type="email" required="required" class="form-control" id="email"  name="manager_EMAIL" placeholder="Email">
                          </div>
                        </div>
                         
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>S.No.</th>
                <th>RO Code</th>
                <th>Manager Name</th>
                
                <th>Mobile No</th>
                <th>Status</th>
                 <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=0;?>
              @foreach($RoManager as $RManager)
              <?php $i++;?>
              <tr>
                <td>{{$i}}</td>
                <td scope="row">{{$RManager->RO_code}}</td>
                <td>{{$RManager->manager_name}}</td>
               
                <td>{{$RManager->manager_mobile}}</td>
                <td> <?php if($RManager->is_active==1){?>

                   <a  title="Active" href="manager/active/{{$RManager->id}}/0"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="manager/active/{{$RManager->id}}/1">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
          </td>
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                    
                      @if($RManager->is_active==1)
                      <li><a href="{{url('manager/edit/'.$RManager->id)}}">Edit</a></li>
                       @endif
                       <!-- <li><a href="{{url('manager/delete/'.$RManager->id)}}">Delete</a></li> -->
                    </ul>
                  </div></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Javascripts--> 
<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>



</body>
</html>