<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
  #errmsg
{
color: red;
}
</style>
<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  

  <!-- Side-Nav-->
 
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Lube Entry List </h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Lube Entry List </a></li>
        </ul>
      </div>
    </div>
     <div class="">

                </div>
                 <div class="">
         <center>@if(Session::has('success'))
                       <font style="color:red">{!!session('exitdata')!!}</font>
                    @endif</center>
      </div>

    <div class="row">
              
      <div class="col-md-12 col-sm-12">
        <div class="row">
           <center>@if(Session::has('success'))
                       <font style="color:red">{!!session('success')!!}</font>
                    @endif</center>
                    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
          
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <form class="form-horizontal form-inline" name="myForm" action="{{'customerlubelist'}}" enctype="multipart/form-data" method="post">
       
            
             {{ csrf_field() }}
        <div class="row">
         <div class="col-md-12">
         

          <div class="form-group">
             <label class="control-label"  for="email">Customer Name</label>&nbsp;&nbsp;
                 <select class="form-control"  name="customer_code" id="sel1">
                 
                        @if(isset($customer_lubelist))
                      @foreach($customer_lubelist as $customer_slips)

                       <option value="{{$customer_slips->customer_code}}">{{$customer_slips->getcompanyname->company_name}}</option>
                      @endforeach
                      @endif
                    </select>
          </div>

          &nbsp;&nbsp;
                        <div class="form-group">
                          <label class="control-label"  for="email">From</label>&nbsp;&nbsp;
                           <input type="text"  class="form-control datepickers" required name="fromdate" placeholder="from date"/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                              <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                                <input  type="text" class="form-control datepickers" name="to_dae" placeholder="To Date"/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                                <input type="submit" class="btn btn-default" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                        </div>
            </div>
        </div>
 </form> 
        <div class="table-responsive">
         @if(isset($customer_lubelist))

          <table class="table table-striped" id="myTable">

            <thead>
              <tr>
                <th>S.no.</th>
                <th>Company Name</th>
                <th>Vehicle Reg No</th>
                <th>Fuel Type</th>
                <th>Dispense Date</th>
                <th>Quantity</th>
             
                <th style="width: 100px;">Actions</th>
               </tr>
            </thead>
            <tbody>
          <?php $i=0; ?>
           @foreach($customer_lubelist as $customer_lubelists)
           <?php $i++ ;?>
              <tr>
              <td>{{$i}}</td>
                <td>{{$customer_lubelists->getcompanyname->company_name}}</td>  
                <td>{{$customer_lubelists->Vehicle_Reg_No}}</td>  
                <td>{{$customer_lubelists->getitemname->Item_Name}}</td>  
                <td>{{Carbon\Carbon::parse($customer_lubelists->request_date)->format('d/m/Y')}}</td> 
                <td>{{$customer_lubelists->petroldiesel_qty}}</td>  
               
                    <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> 
                  <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                    <li><a href="{{'editlubelist'}}/{{$customer_lubelists->id}}">Edit</a></li>
                    </ul>
                  </div></td>
              </tr>
          
      @endforeach
            </tbody>
          </table>
        @endif
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script>
<script type="text/javascript">
  $(function () {
  $(".datepickers").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        defaultDate:'dd/mm/yyy',
        format: 'dd/mm/yyyy',
  }).datepicker('update', new Date());
});

</script>
<script type="text/javascript">
 
jQuery(function(){
   jQuery('#type1').on('change',function(){
        var a = document.getElementById("type2");
    var b = document.getElementById("type1");

    if (a.options[a.selectedIndex].value == b.options[b.selectedIndex].value) {
        alert("Please Choose Select unit symble");
         jQuery('.site-btn').attr('disabled',true);
    } else{
       jQuery('.site-btn').attr('disabled',false);


    }
   }); 
});
</script>
<script type="text/javascript">
  $(function() {
     $('.row_dim').hide(); 
    $('#type').change(function(){
        if($('#type').val() == 'Compound') {
            $('.row_dim').show(); 
             //jQuery('#utint').css("display","none")
        } else {
            //$('#utint').show();
            jQuery('.row_dim').css("display","none")
        } 
    });
    
});
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>

<script type="text/javascript">
  
 $(document).ready(function () {
  //called when key is pressed in textbox
  $("#quantity").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});
</script>
<script>
  jQuery(function(){
     var url="{{url('check_Pedestal_Number_unique')}}";
     var vil={
           init:function(){
             vil.selectRo();
             vil.gstCodeChk();
         
           },

           gstCodeChk:function(){

            jQuery('#chk_gst_code').on('click',function(){
                  var id=0;
                   var table=jQuery('#quantity').data('table');
                   var colum=jQuery('#quantity').data('colum');
                   var mess=jQuery('#quantity').data('mess');

                   // id=jQuery(this).data('id');
                    
                  udat=jQuery('#quantity').val();

                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mess);

                   }else{
                    jQuery('#form_state').submit();
                   }
                       
                   });
              });
            
           
           },

          
           
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                   var colum=jQuery(this).data('colum');
                   var mess=jQuery(this).data('mess');

                   // id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();

                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mess);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }

     vil.init();
  });
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>