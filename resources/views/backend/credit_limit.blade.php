<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<title>Garruda</title>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Credit Limits</h1>
      </div>
      <div>
        <ul class="breadcrumb">
           <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Credit Limits</a></li>
        </ul>
      </div>
    </div>
    <div class="">
        
        </div> 
         @if ($errors->any())
        <div class="alert alert-danger">
           <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
           </ul>
       </div>
      @endif 
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <center>@if(Session::has('success'))
             <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!--  <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div> -->
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Credit Limit</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal"  action="{{'credit_limitaa'}}" enctype="multipart/form-data" method="post"  onsubmit="return confirm('Do you want to Continue?');">
                           {{ csrf_field() }}
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Outlet Name</label>
                          <div class="col-sm-8">
                            <select class="form-control" required="required" readonly id="sel1" name="Ro_code">
                               @foreach($data1 as $row) 
                              <option value="{{$row->RO_code}}">{{$row->pump_legal_name}}</option>
                              
                               @endforeach
                            </select>
                          </div>
                        </div>
                       <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Customer</label>
                          <div class="col-sm-8">
                            <select class="form-control" required="required" id="customer_Code" name="Customer_Code">
                             
                            </select>
                          </div>
                        </div>
                         <!-- <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Vehicle</label>
                          <div class="col-sm-8">
                            <select class="form-control" required="required" id="Vehicle_Reg_no" name="Vehicle_reg_no">
                             
                            </select>
                          </div>
                        </div> -->
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="Limit Value">Limit Value (Rs.)</label>
                          <div class="col-sm-8">
                           	<input type="text" required="required" class="form-control" placeholder="Limit Value" name="Limit_Value" />
                          </div>.
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="Handling Fee">Handling Fee (%)</label>
                          <div class="col-sm-8">
                            <input type="number" required="required" class="form-control" placeholder="Handling Fee" name="handlingfee" />
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-4" for="Discount">Discount (%)</label>
                          <div class="col-sm-8">
                            <input type="number" required="required" class="form-control" placeholder="Discount" name="Discount" />
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">No. of Days</label>
                          <div class="col-sm-8">
                            <input type="number" required="required" class="form-control" placeholder="No of Days" name="no_of_days" />
                          </div>
                        </div>
                       
                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <input type="submit" value="submit" class="btn btn-default">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped"  id="myTable">
            <thead>
              <tr>
                <th>S.no.</th>
               <!--  <th>OutletName</th> -->
                <th>Customer  Name</th>
                <th>Limit Value</th>
                <th>No. of Days</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
               <?php $i=0; ?>
      
                 @foreach($data as $row) 
      
              <?php $i++ ;?>    
              <tr>
                <td >{{$i}}</td>
               <!--  <td>{{$row->pump_name}}</td> -->
                <td>{{$row->getcustomer->company_name}}</td>
                
                <td>{{$row->Limit_Value}}</td>
                <td>{{$row->no_of_days}}</td>
                
               
                 
                  <td> <?php if($row->is_active==1){?>

                   <a  title="Active" href="credit_limit/active/{{$row->id}}/0" onclick="return confirm('Do you want to deactivate?');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="credit_limit/active/{{$row->id}}/1" onclick="return confirm('Do you want to activate?');">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
          </td>
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      <!--<li><a href="#">View</a></li>-->
                      @if($row->is_active==1)
                      <li><a href="credit_limit/edit/{{$row->id}}">Edit</a></li>
                      @endif
                     <!-- <li><a href="credit_limit/delete/{{$row->id}}">Delete</a></li>-->
                    </ul>
                  </div></td>
              </tr>
               @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="js/index.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_Code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get('getrocode',{
                
                rocode:jQuery('#sel1').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_Code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_Code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#Vehicle_Reg_no').html(opt);
               });
           },

     }
     vil.init();
  });
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
</body>
</html>