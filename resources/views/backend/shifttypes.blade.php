@extends('backend-includes.app')

@section('content')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Shift Type</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#"> Shift Type</a></li>
        </ul>
      </div>
    </div>
     <div class="">
      
      <ul>
        @foreach($errors->all() as $error)
          <li style="color: red">{{ $error}}</li>
        @endforeach
      </ul>
     
      
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
           <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
               <div class="">
      
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      
    </div>

            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document" style=" width: 480px;">
            
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Shift Type</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal" action="{{Route('shifttypestore')}}" method="post" onsubmit="return confirm('Do you want to Continue?');" enctype="multipart/form-data" autocomplete="off">
                         {{ csrf_field() }}
                       
                          <input type="hidden" name="RO_Code" value="{{Auth::user()->getRocode->RO_code}}">

                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Shift Type<span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                           <!--  <input type="text" required="required" class="form-control type" name="type" id="" placeholder="Shift Type"> -->
                           <select  name="shift_type" required="required" class="form-control type" >
                             

                          <option value="regular">Regular</option>
                          <option value="flexi">Flexi</option>
                           </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Shift Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="text" required="required" class="form-control type" name="type" id="" placeholder="Shift Type">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Start Time <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="text" required="required" class="form-control " name="start_time" id="datepickerabh" value="" placeholder="Start Time">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">End Time <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                              <input type="text" required="required" class="form-control" name="  end_time" id="datepickerapp" value="" placeholder="End Time">
                        </div>
                      </div>
                  
                     
                        <div class="form-group">
                          <div class="col-sm-offset-9 col-sm-3">
                            <button type="submit" class="btn btn-default submit-button" id="id-shift-type" onclick="return gstAndPan()"  >Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">

      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
             @if(isset($Shifttype) && count($Shifttype)  != 0)
            <thead>
              <tr>
                <th>S.No</th>
              
                <th>Shift Type</th>
                <th>Shift Name</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Action</th>
                
              </tr>
            </thead>
            <tbody>
              <?php $i=0;?>
               @foreach($Shifttype as $type)

               <?php $i++;?>
              <tr>
                <td>{{$i}}</td> 
              <td scope="row">{{$type->shift_type}}</td>
                <td scope="row">{{$type->type}}</td>
                
                <td scope="row">{{$type->start_time}}</td>
                <td scope="row">{{$type->end_time}}</td>
                  
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      
                      <li><a href="{{'shifttypeedit'}}/{{$type->id}}">Edit</a></li>
                   
                    </ul>
                  </div>
               </td>
              </tr>
             @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>


<script type="text/javascript">
  function  gstAndPan() {
        var mobile = document.getElementById("mobile").value;
        var pattern = /^[0]?[789]\d{9}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
            return true;
        }
        alert("It is not valid mobile number.input 10 digits number!");
        return false;

  
    }
  function myFunction() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
  $(function () {

  $("#datepickerabh").datetimepicker({ 
        
                  format:' HH:mm:ss '
  })
  $("#datepickerapp").datetimepicker({ 

                  format:' HH:mm:ss '
    

  })

});
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
             vil.checkdesignation();

             jQuery('#ToDate').on('blur',function(){
                 vil.CalculateDiff();
             });
               vil.checkdesignation();
              jQuery('#designation').on('change',function(){
                  vil.checkdesignation();
              });
           },
           // selectRo:function(){

           //  jQuery('.type').on('blur',function(){
               
           //        type=jQuery(this).val();
           //        jQuery.get('check_Pedestal_Number_unique',{
           //        table:type,
                 
               
           //          '_token': jQuery('meta[name="csrf-token"]').attr('content'),
           //         },function(data){
                   
           //         if(data=='true'){
           //            alert(mass);
           //          jQuery('.submit-button').attr('disabled',true);

           //         }else{
           //          jQuery('.submit-button').attr('disabled',false);
           //         }
                       
           //         });
           //    });
            
           
           // },
           CalculateDiff:function() {
 
                    if(jQuery("#FromDate").val()!="" && jQuery("#ToDate").val()!=""){
                       
                      /*var From_date = new Date(jQuery("#FromDate").val());
                      var To_date = new Date(jQuery("#ToDate").val());
                      var diff_date =  To_date - From_date;
                       
                      var years = Math.floor(diff_date/31536000000);
                      var months = Math.floor((diff_date % 31536000000)/2628000000);
                      var days = Math.floor(((diff_date % 31536000000) % 2628000000)/86400000);
                      
                        if(months<18){
                           alert('Age is not Less than 18 Years');
                        }*/

                      }
                      else{
                          alert("Please select dates");
                          return false;
                      }
          },
          checkdesignation:function(){
           
              var pre=jQuery('#designation  option:selected').data('pre');
              console.log(pre);
              var d=0;
              jQuery('#reporting_to').children('option').each(function(i,v){
                      console.log(jQuery(this).data('pre'));
                      if(jQuery(this).data('pre')!=pre){
                          jQuery(this).hide();
                      }else{
                         if(d==0){
                            d=jQuery(this).val();
                         }
                         jQuery(this).show();
                      }
              });
               
              //jQuery('#reporting_to option[value="0"').show();
              jQuery('#reporting_to option[value="'+d+'"').prop('selected', 'selected').change();
           },
          
     }
     vil.init();
  });


</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();

             $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});

    
    });

  
</script>
@endsection


