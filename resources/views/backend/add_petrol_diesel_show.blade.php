<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">

<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Request Fuel  Views </h1>
      </div>
      <div>
        <ul class="breadcrumb">
           <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/add_petrol_diesel')}}
">Request Fuel</a></li>
        </ul>
      </div>
    </div>
	
	
	                         <div class="">  <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                             </div>
	
	
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
         
          
                      <form class="form-horizontal" >
            
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Outlet Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" disabled="disabled" required="required" name="RO_code" id="sel1">

                                @foreach($datas as $ro_master)
                                    <option name="company_id" value="{{$ro_master->RO_code}}"
                                             @if($request_petrol_diesel->RO_code == $ro_master->RO_code) selected @endif >{{$ro_master->pump_legal_name}}</option>
                                @endforeach
                            </select>
                          </div>
                        </div>
                       
                      
                          <div class="form-group">
                            <label class="control-label col-sm-3" for="email">Vehicle Reg No </label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" disabled="disabled" name="Request_Value" value="{{$request_petrol_diesel->Vehicle_Reg_No}}" id="" placeholder="Request Value ">
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-sm-3" for="pwd">Request Fuel Type</label>
                            <div class="col-sm-9">
                              <select class="form-control" disabled="disabled" id="Pedestal_id" name="PetrolDiesel_Type">
                                @foreach($fuel_type as $fuel) 
                                <option value="{{$fuel->id}}" @if($request_petrol_diesel->PetrolDiesel_Type == $fuel->id) selected @endif>{{$fuel->Item_Name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          @if($request_petrol_diesel->getTransaction!=null && $request_petrol_diesel->getTransaction->getItemName!=null)
                          

                          <div class="form-group">
                            <label class="control-label col-sm-3" for="pwd">Delivered Fuel Type</label>
                            <div class="col-sm-9">
                              <select class="form-control" disabled="disabled" id="Pedestal_id" name="PetrolDiesel_Type">
                               
                                <option value="{{$request_petrol_diesel->getTransaction->getItemName}}" selected >{{$request_petrol_diesel->getTransaction->getItemName->Item_Name}}</option>
                                
                              </select>
                            </div>
                          </div>
                          @endif
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Request Type</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" disabled="disabled" name="Request_Value" value="{{$request_petrol_diesel->Request_Type}}" id="" placeholder="Request Value ">
                          </div>
                        </div>
                        
                          @if($request_petrol_diesel->Request_Type!='Full Tank')
                          <div class="form-group">
                            <label class="control-label col-sm-3" for="email">Request Value</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" disabled="disabled" name="Request_Value" value="{{$request_petrol_diesel->Request_Value}}" id="" placeholder="Request Value ">
                            </div>
                          </div>
                          @endif
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Delivered Value</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" disabled="disabled" name="Request_Value" @if($request_petrol_diesel->getTransaction!=null) value="{{$request_petrol_diesel->getTransaction->petroldiesel_qty}}" @endif id="" placeholder="Request Value ">
                          </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-3" for="pwd">Request Date</label>
                            <div class="col-sm-9">
                             <input type="text" class="form-control" disabled="disabled" name="Request_Value" value="{{Carbon\Carbon::parse($request_petrol_diesel->request_date)->format('d/m/Y h:i:s a')}}" id="" placeholder="Request Value ">
                            </div>
                          </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Execution Date</label>
                          <div class="col-sm-9">
                            <input class="form-control" disabled="disabled" value="@if($request_petrol_diesel->Execution_date!=null) {{date('d/m/Y h:i:s a',strtotime($request_petrol_diesel->Execution_date))}}@endif" name="request_date"  type="text" />
                          </div>
                        </div>

                        <div class="form-group" style="visibility: hidden;">
                          <label class="control-label col-sm-3" for="email">Customer Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" disabled="disabled" required="required" name="customer_code" id="customer_code">
                            </select>
                          </div>
                        </div>

                      </form>
                    
          </div>
        </div>
      </div>
    </div>
	
	
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script type="text/javascript">
 jQuery(function(){

  var url1="{{url('getrocodes')}}";
  var url2="{{url('getregierids')}}";

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get(url1,{
                
                rocode:jQuery('#sel1').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get(url2,{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
               });
           },

     }
     vil.init();
  });
</script>
</body>
</html>