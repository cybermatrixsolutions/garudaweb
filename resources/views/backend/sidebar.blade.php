<!--  <script type="text/javascript">
    var selector = '.treeview li';

$(selector).on('click', function(){
    $(selector).removeClass('active');
    $(this).addClass('active');
});
  </script>-->
  <aside class="main-sidebar hidden-print">
    <section class="sidebar">
      <a class="logo" href="#">
        <img src="{{URL::to('/')}}/images/logo/garruda.jpg" alt="garruda" title="garruda">
      </a>
     <div class="user-panel">
       
        <div >
           
          <!--@if(Auth::user()->user_type==2)
          
          <p class="designation" style="color: #fff; font-size:13px; text-align: center; word-wrap: break-word;">Admin</p>
          @elseif(Auth::user()->user_type==3)
          <p class="designation" style="color: #fff; font-size:13px; text-align: center; word-wrap: break-word;">Retail Outlet</p>
           @elseif(Auth::user()->user_type==4)
           <p class="designation" style="color: #fff; font-size:13px; text-align: center; word-wrap: break-word;">Customer</p>
           @endif -->
        </div>
      </div>
      <!-- Sidebar Menu-->
      <ul class="sidebar-menu nav">
         @if(Auth::user()->user_type==1 || Auth::user()->user_type==2)

         <li class="treeview active"><a href="#"><i class="fa fa-dashboard"></i><span> Basic Config</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"> RO Config</i></a>
                  <ul>
                     <li><a href="{{URL('roconfig')}}"><i class="fa fa-circle-o"></i><span> General </span></a></li>
               
                     <li><a href="{{URL('masterpayment')}}"><i class="fa fa-circle-o"></i><span> Payment Modes</span></a></li>
                  </ul>
                </li>

                <li><a href="#"><i class="fa fa-circle-o"> Items Config</i></a>
                  <ul>
                     <li><a href="{{URL('unit_of_measure')}}"><i class="fa fa-circle-o"></i><span> Unit of Measures</span></a></li>
                     <li><a href="{{URL('stock_item_group')}}"><i class="fa fa-circle-o"></i><span> Stock Item Groups</span></a></li>
                   
                  </ul>
                </li>
                <li><a href="#"><i class="fa fa-circle-o"> Other Config</i></a>
                  <ul>
                     <li><a href="{{URL('taxrates')}}"><i class="fa fa-circle-o"></i><span> Tax Config</span></a></li>
                
                     <li><a href="{{URL('lob_master_page')}}"><i class="fa fa-circle-o"></i><span> LOB</span></a></li>
                     <li><a href="{{URL('add_state_page')}}"><i class="fa fa-circle-o"></i><span> States </span></a></li>
                   
                  </ul>
                </li>
                <li><a href="#"><i class="fa fa-circle-o"> Vehicle</i></a>
                  <ul>
                    <li><a href="{{URL('vechilemake')}}"><i class="fa fa-circle-o"></i><span> Vehicle Make</span></a></li> 
                    <li><a href="{{URL('vechilemodel')}}"><i class="fa fa-circle-o"></i><span> Vehicle Model </span></a></li>
                  </ul>
                </li>
                 <li><a href="#"><i class="fa fa-circle-o"> Industry/Depart</i></a>
                  <ul>
                    <li><a href="{{URL('industry')}}"><i class="fa fa-circle-o"></i><span> Industry /Department</span></a></li> 
                    <!--<li><a href="{{URL('department')}}"><i class="fa fa-circle-o"></i><span> Department </span></a></li>-->
                  </ul>
                </li>
                 <li><a href="{{URL('designation')}}"><i class="fa fa-circle-o"></i><span> Role </span></a></li>
                  <li><a href="{{URL('accounting')}}"><i class="fa fa-circle-o"></i><span> Accounts Integration</span></a></li>


              

              </ul>
          </li>
          <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span> Other Config</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu nav">
                <li><a href="{{URL('principal_listing')}}"><i class="fa fa-circle-o"></i> Principal Company</a></li>

              <li><a href="#"><i class="fa fa-circle-o"> Stock Items</i></a>
                  <ul>
                     <li><a href="{{URL('stock_item_management')}}"><i class="fa fa-circle-o"></i><span> Common Items</span></a></li>
                      <li><a href="{{URL('dipCharts')}}"><i class="fa fa-circle-o"></i> Dip Chart </a></li>
                   
                  </ul>
                </li>
                 <li><a href="{{URL('retail')}}"><i class="fa fa-circle-o"></i> Retail Outlet </a></li>
                 
              </ul>
          </li>
       
        @endif

       @if(Auth::user()->user_type==1 || Auth::user()->user_type==3)
       <?php  
             $rocode='';
              $confirm=0;
              $id=0;
             if (Auth::user()->getRocode!=null) {
                $rocode=Auth::user()->getRocode->RO_code;
                $confirm=Auth::user()->getRocode->confirm;
                $id=Auth::user()->getRocode->id;
             }

          $lov=App\RoLobSelectionModel::where('RO_code',$rocode)->get();?>
          @if( $lov->count()>0 && $confirm==1)
         
            <li class="treeview active"><a href="#"><i class="fa fa-dashboard"></i><span> Basic Config</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu nav">
                <li><a href="#"><i class="fa fa-circle-o"> RO Config</i></a>
                  <ul>
                       <li class=""><a href="{{URL('outletconfig')}}"><i class="fa fa-circle-o" aria-hidden="true"></i><span> Fuel Price Change </span><i class="fa fa-angle-right"></i></a></li>
                
                       <li><a href="{{URL('retail_update/'.$id)}}"><i class="fa fa-circle-o"></i><span> RO Info </span></a></li>
                       <li><a href="{{URL('roLobUpdate')}}"><i class="fa fa-circle-o"></i><span> LOB Info </span></a></li>
                        
                    
                  </ul>
                </li>

                <li><a href="#"><i class="fa fa-circle-o"> Tax Config </i></a>
                  <ul>
                      <li><a href="{{URL('tax')}}"><i class="fa fa-circle-o"></i><span> Tax Structure </span></a></li>
                       <li><a href="{{URL('item_services')}}"><i class="fa fa-circle-o"></i><span> Services Master</span></a></li>
                      <li><a href="{{URL('item_tax_allo')}}"><i class="fa fa-circle-o"></i> Item Tax Config </a></li>
           
                  </ul>
                </li>
                <li><a href="{{URL('persoonel')}}"><i class="fa fa-bars"></i><span> Personnel & Staff </span></a></li>
                <li><a href="{{URL('price_list')}}"><i class="fa fa-circle-o"></i><span> Stock Item Master </span></a></li>

                <li><a href="#"><i class="fa fa-circle-o"> Customer Master</i></a>
                  <ul>
                    <li><a href="{{URL('Customer')}}"><i class="fa fa-circle-o"></i> Customer</a></li>
                    <li><a href="{{URL('vehiclemanagement')}}"><i class="fa fa-circle-o"></i>  Vehicle </a></li>
                    <li><a href="{{URL('driver')}}"><i class="fa fa-circle-o"></i>  Driver </a></li>
                    <li><a href="{{URL('credit_limit')}}"><i class="fa fa-circle-o"></i> Credit Limits Update</a></li>
                    <li><a href="{{URL('qr_code')}}"><i class="fa fa-circle-o"></i> Vehicle QR Code </a></li>
                   
                  </ul>
                </li>
                <li><a href="#"><i class="fa fa-circle-o"> Infra Master</i></a>
                  <ul>
                     <li><a href="{{URL('tank')}}"><i class="fa fa-circle-o"></i><span> Tanks</span></a></li>
                     <li><a href="{{URL('nozzles')}}"><i class="fa fa-circle-o"></i> Pedestals</a></li>
                     <li><a href="{{URL('pedestal')}}"><i class="fa fa-circle-o"></i> Nozzles </a></li>
                     

                    
                  </ul>
                </li>

              </ul>
          </li>


          <li class="treeview active"><a href="#"><i class="fa fa-dashboard"></i><span> Transactions </span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu nav">
                <li><a href="#"><i class="fa fa-circle-o"> Customer Transactions </i></a>
                  <ul>
                       <li><a href="{{URL('customer_slip_entry_page')}}"><i class="fa fa-circle-o"></i><span> Fuel Slip Entry</span></a></li>
                       <li><a href="{{URL('customer_slip_entry_lube_page')}}"><i class="fa fa-circle-o"></i><span> Other Item Slip Entry</span></a></li>
                       <li><a href="{{URL('ro_transaction_list')}}"><i class="fa fa-circle-o"></i><span> View & Alter Slip Entry</span></a></li>
        
                  </ul>
                </li>
                <li><a href="#"><i class="fa fa-circle-o"> Billing & Settlement </i></a>
                  <ul>
                      <li><a href="{{URL('invoiceindex')}}"><i class="fa fa-circle-o"></i> Create Fuel & Items Bills </a></li>
                      <li><a href="{{URL('customer_commision')}}"><i class="fa fa-circle-o"></i> Create Services Bill</a></li>
                      <li><a href="{{URL('invoicePendingCollection')}}"><i class="fa fa-circle-o"></i><span>Bills Settlement</span></a></li>
                      <li><a href="{{URL('slipPendingPrinting')}}"><i class="fa fa-circle-o"></i><span>Commission Settlement</span></a></li>
           
                  </ul>
                </li>

              </ul>
          </li>

        @endif
        @endif
       
         @if(Auth::user()->user_type==1 || Auth::user()->user_type==4 && Session::has('ro'))

          <li class="treeview active"><a href="#"><i class="fa fa-dashboard"></i><span> Basic Config </span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu nav">
                <li><a href="{{url('customerprofile')}}/{{Auth::user()->id}}"><i class="fa fa-circle-o"></i><span> Customer Info </span></a></li>
              </ul>
          </li>
          <li class="treeview active"><a href="#"><i class="fa fa-dashboard"></i><span> Masters </span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu nav">
                  <li><a href="{{URL('customer_vehiclemanagement')}}"><i class="fa fa-circle-o"></i>  Vehicle </a></li>
                  <li><a href="{{URL('customer_driver')}}"><i class="fa fa-circle-o"></i> Driver </a></li>
              </ul>
          </li>
          <li class="treeview active"><a href="#"><i class="fa fa-dashboard"></i><span> Transactions </span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu nav">
                  <li><a href="{{URL('add_petrol_diesel')}}"><i class="fa fa-circle-o"></i><span> Request Fuel</span></a></li>
                  <li><a href="{{URL('lube_request')}}"><i class="fa fa-circle-o"></i><span> Request Other Items </span></a></li>
              </ul>
          </li>
          <li class="treeview active"><a href="#"><i class="fa fa-dashboard"></i><span> Reports </span><i class="fa fa-angle-right"></i></a>
             
          </li>
        
         @endif

          @if(Auth::user()->user_type==5 || Auth::user()->user_type==3 )
          <?php  
             $rocode='';
             if (Auth::user()->getRocode!=null) {
                $rocode=Auth::user()->getRocode->RO_code;
             }

          $lov=App\RoLobSelectionModel::where('RO_code',$rocode)->get();?>
          @if( $lov->count()>0 && $confirm==1 || Auth::user()->user_type==5)
           

           <li class="treeview active"><a href="#"><i class="fa fa-dashboard"></i><span> RO Operations </span><i class="fa fa-angle-right"></i></a>
              
              <ul class="treeview-menu nav">
                  <li><a href="{{URL('shift/create')}}"><i class="fa fa-circle-o"></i><span> Shift Allocation </span></a></li>
                  <li><a href="{{URL('shift/closer')}}"><i class="fa fa-circle-o"></i><span> Nozzle CMR</span></a></li>
                  <li><a href="#"><i class="fa fa-circle-o"></i><span> Shift Settlement</span> <i class="fa fa-angle-right"></i></a>
                    <ul class="treeview-menu nav">
                      <li><a href="{{URL('shift/settlement')}}"><i class="fa fa-circle-o"></i><span>Fuel Settlements</span></a></li>
                      <li><a href="{{URL('shift/settlementLueb')}}"><i class="fa fa-circle-o"></i><span>Lub Settlements</span></a></li>
                      
                    </ul>
					
                   
                     
                  </li>
				  
				   <li><a href="{{URL('tankRiding')}}"><i class="fa fa-circle-o"></i><span> Dip Readings</span></a></li>
			     
                   @if(Auth::user()->user_type==3 )				   
				    <li><a href="#"><i class="fa fa-circle-o"></i><span> RO price list</span> <i class="fa fa-angle-right"></i></a>
                    <ul class="treeview-menu nav">
                      <li><a href="{{URL('fuel_price_management')}}"><i class="fa fa-circle-o"></i> Fuel Price </a></li>
                      <li><a href="{{URL('Any_price_management')}}"><i class="fa fa-circle-o"></i> Item Price </a></li>
                    </ul> 
                  </li>
				    @endif 
                  <li class="treeview active"><a href="{{URL('customernewtransactions')}}"><i class="fa fa-dashboard"></i><span> Customer Request </span></a>					
             
              </ul>

           </li>
              
              </li>
              
             <li class="treeview active"><a href="#"><i class="fa fa-dashboard"></i><span> Reports </span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu nav">
                <li><a href="{{URL('ro_nozzel_reading')}}"><i class="fa fa-circle-o"></i> Nozzle CMR Report </a></li>
           
                
              </ul>
          </li>

         @endif
         @endif

      </ul>
    </section>
  </aside>