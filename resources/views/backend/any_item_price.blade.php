   <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.min.css"/>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<title>Garruda</title>
<style type="text/css">
	.bootstrap-timepicker-widget.dropdown-menu.timepicker-orient-left.timepicker-orient-top.open {
    width: 2px;
}
input{
  max-width: 100px;
}
</style>
</head>
  <body class="sidebar-mini fixed">
  <div class="wrapper"> 
    <!-- Navbar-->
    
     @include('backend-includes.header')
   
          <!-- Side-Nav-->
      
      @include('backend-includes.sidebar')
<?php 
      $itemName='';
      $subgrup='';
?>
    <div class="content-wrapper">
      <div class="page-title">
        <div>
          <h1><i class="fa fa-dashboard"></i>&nbsp; Item Price Update
          </h1>
        </div>
         <div class="form-group" style="padding-right: 30px; margin-bottom:0px;">

           <form name="myForm" id="onchangegroup" style="padding:8px 0" class="form-inline" action="{{url('Any_price_management')}}" enctype="multipart/form-data" method="get">
                 
                <label for="email">Select Item Group </label>
                <?php if(isset($_GET['itemName'])){ $itemName=$_GET['itemName'];}else{$itemName=$groupName;} ?>
                <select class="form-control itemName" name="itemName" id="groupName">
                    @foreach($group as $ngroup)
                      <option @if($itemName==$ngroup->id) selected @endif value="{{$ngroup->id}}">{{$ngroup->Group_Name}}</option>
                     @endforeach
                </select>


                 <label for="email">Select Item SubGroup </label>
                <?php if(isset($_GET['subgrup'])){ $subgrup=$_GET['subgrup'];}else{$subgrup=$subgrup;} ?>
                <select class="form-control itemName" name="subgrup" id="itemName" data-id="{{$subgrup}}">
                    
                </select>

                </div>
                 </form>
                              <?php 
    

      if(!isset($_GET['itemName'])){ 
        $groups=$group->first();
        $itemName=$groups->id;
      }
?>
      <!--          
        <div>
          <ul class="breadcrumb">
            <li>

              <form name="myForm" class="form-inline" action="{{url('Any_price_management?itemName='.$itemName)}}" enctype="multipart/form-data" method="post">
                 {{ csrf_field() }}
                        
               

                 <div class="form-group">
                <label for="email">Date</label>
                <input type="text" class="form-control datepickernew form_datetime" data-date-format="dd-mm-yyyy" placeholder="Date" name="date" value="@if(isset($search_date)){{$search_date}}@endif">
                </div>
                
                <button type="submit" class="btn btn-default ">Go</button>
                        
                     
            </form>
            </li>
            <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
         

          </ul>
        </div> -->
      </div>
      <div class="">
         
      </div> 
      <div class="row" style="margin-top: -120px;">
              @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
              @endif
              

  	
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="row">
            <div class="col-xs-12 vat">
              <div class="row">
                <div class="col-5">
              <div class="pull-right"> 
          <div>
            @if(Auth::user()->user_type==3)

               <form class="form-inline" style="margin-top: 49px;" action="{{'update_csv_data'}}" method="post" name="f" onsubmit="return check(this)" enctype="multipart/form-data">
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <div class="form-group">
               <label for="email"File(csv)> </label>
               <input type="file" id="inputfile" required="required"   placeholder="File(csv)" name="file" style="display: inline; width: auto;">
                <input  type="submit" value="Submit" style="margin-right:140px;background-color: #00786a;color: #fff;">
               </div>
               
               </form>
               <a href="get_all_item?itemName={{$itemName}}&subgrup={{$subgrup}}" style="    position: absolute;
    top: 95px;
    right: 26%;" class="btn btn-primary">Export as CSV</a>
             @endif
          </div>
          </div>
        </div>
         <div class="col-5">
          </div>
        </div>
             <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
              
              <!-- Modal --> 
              
               <center>
            @if(Session::has('success'))
                       <font style="color:red">{!!session('success')!!}</font>
                    @endif
            </center>
              <div> <span style="color:red;padding-left: 15px;"> * All Prices are inclusive tax </span></div>
            @if(isset($fuel_type) and count($fuel_type))

            <form name="myForm" id="myForm-update" class="form-inline" action="{{url('save_any_price?itemName='.$itemName)}}" enctype="multipart/form-data" method="post"  onsubmit="return confirm('Do you want to Continue?');">
            	 {{ csrf_field() }}
            	<div class="row">
                <div class="col-md-12">
                 <div class="col-md-12">
                  <div class="table-responsive" >
                    <table class="table table-striped" id="myTable">
                      <thead>
                        <tr>
                          <th>Item</th>
                          <th>Future Price</th>
                          <th>Future Date</th>
                          <th>Current Price</th>
                          <th>Current Date</th>
                          
                          <th>New Update Price</th>
                           
                        </tr>
                      </thead>
                      <tbody>
                        <tr>

                          <td>&nbsp;Date</td> 
                          <td></td>
                        
                          <td> 

                           </td>
                           <td></td>
                            <td><a href="#" id="copy-previous-day-price" style="background: #fff;color: #0312ff;">Copy previous day price</a></td>
                            
                           <td>
                          <input type="text" class="form-control datepickernew" data-date-format="dd-mm-yyyy" value="@if(isset($search_date)){{$search_date}}@endif"  placeholder="Date" name="effective_date">
                       </td>

                        </tr>


                      	@foreach($fuel_type as $fuel)
                           
                        <tr class="item-row">

                          <td>
                          	@if(isset($fuel->getpricelist))
                          	<input type="hidden" name="namenew[]" value="{{$fuel->getpricelist->id}}">{{$fuel->getpricelist->Item_Name}}
                          	@else
                          	   <input type="hidden" name="namenew[]" value="{{$fuel->id}}">{{$fuel->Item_Name}}
                          	@endif

                          </td>
                          <td>
                            @if(isset($fuel->getpricelist))
                            <input type="number" step="0.01"  value="@if(isset($oldprice[$fuel->getpricelist->id])){{bcadd($oldprice[$fuel->getpricelist->id]['price'], 0, 2)}}@endif" disabled>
                            @else

                               <input type="number" step="0.01"  disabled value="@if(isset($oldprice[$fuel->id])){{bcadd($oldprice[$fuel->id]['price'], 0, 2)}}@endif">
                            @endif
                            
                          </td>
                          
                            <td>
                              @if(isset($fuel->getpricelist))
                                  @if(isset($oldprice[$fuel->getpricelist->id]))
                                  <input type="text" class="form-control inputwith" value="@if(isset($oldprice[$fuel->id])){{
                                     Carbon\Carbon::parse($oldprice[$fuel->getpricelist->id]['effective_date'])->format('d/m/Y')}}@endif 
                                 "  placeholder="Date" name="Effective_Date" disabled>
                                 
                                 @else
                                  <input type="text" disabled class="form-control inputwith" value=""  placeholder="Date" name="Effective_Date" disabled>
                                 
                                 @endif
                              @else
                                  @if(isset($oldprice[$fuel->id]))
                                  <input type="text" class="form-control inputwith" value="@if(isset($oldprice[$fuel->id])){{
                                     Carbon\Carbon::parse($oldprice[$fuel->id]['effective_date'])->format('d/m/Y')}}@endif 
                                 "  placeholder="Date" name="Effective_Date" disabled>
                                 
                                 @else
                                  <input type="text" disabled class="form-control inputwith" value=""  placeholder="Date" name="Effective_Date" disabled>
                                 
                                 @endif
                             @endif
                          </td>
                          
                          <td > 
                            @if(isset($fuel->getpricelist))
                            <input type="number" disabled step="0.01" class="oldprice" name="oldprice[]" value="{{$fuel->price}}">
                            @else

                               <input style="text-align:right" type="number" class="currentprice" disabled step="0.01" name="oldprice[]" value="@if(isset($fuel->price)){{bcadd($fuel->price->price, 0, 2)}}@endif">
                            @endif
                          </td>


                            
                            @if($fuel->price!=null & isset($fuel->price->effective_date))
                             <td> 
                              <input  type="text" class="form-control datepickernew" value="{{
                                 Carbon\Carbon::parse($fuel->price->effective_date)->format('d/m/Y')}}
                             "  placeholder="Date" name="Effective_Date" disabled>
                             </td>
                           
                             
                            @else
                            <td>
                              <input  type="text" class="form-control datepickernew" value="{{
                                 Carbon\Carbon::parse($fuel->effective_date)->format('d/m/Y')}}"  placeholder="Date" name="Effective_Date" disabled>
                             
                            </td>
                            @endif
                           
                          <td>
                          	
                          	   <input style="text-align:right" type="text"  class="numeriDesimal newprice" name="Price[]" value="{{bcadd(0, 0, 2)}}">

                          </td>
                          
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                  </div>

                </div>
               </div>
              
                <div class="col-md-12">
              <button type="submit" id="form-submit" class="btn btn-default pull-right ">Submit</button>

            </form>
            @endif
      
          </div>
                        
              
            </div>

          </div>
        </div>
      </div>
     
    </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="js/index.js"></script> 
<script>
function check(form){ 
 if (!/\.csv$/.test(form.file.value)){
  alert('Please select file in csv format')
  return false
  }
 else return true
}
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.js">
</script>
<script type="text/javascript">
          
                    jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });
             
        </script>

<script type="text/javascript">
            jQuery('#timepicker1').timepicker({defaultTime:'06:00 AM'});

            jQuery(function () {

              var app={
                      init:function(){
                           jQuery(".datepickernew").datepicker({ 
                                autoclose: true, 
                                todayHighlight: true,
                                format: 'dd/mm/yyyy',
                                startDate: '-0m',
                                
                          });

                          jQuery('.itemName').on('change',function(){
                                 jQuery('#onchangegroup').submit();
                          });
                          jQuery('#groupName').on('change',function(){
                              app.getsubgroup();
                          });
                          app.getsubgroup();
                          //app.update_add();
                          app.copyPreviousDayPrice();
                      },
                      copyPreviousDayPrice:function(){
                        
                        jQuery('#copy-previous-day-price').on('click',function(){
                          jQuery('.item-row').each(function(){
                             var currentprice= jQuery(this).find('.currentprice').val();
                              jQuery(this).find('.newprice').val(currentprice);

                          });
                        });
                      },
                      getsubgroup:function(){
                          var groupNam=jQuery('#groupName').val();
                          var sl=jQuery('#itemName').data('id');
                          jQuery.get('getsugroup/'+groupNam,{
                
                            groupNam:groupNam,
                           
                            '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                           },function(data){
                          
                             var opn='<option value="">Select Sub Group</option>';
                             jQuery.each(data,function(i,v){
                              if(sl==i)
                                 opn+='<option selected value="'+i+'">'+v+'</option>';
                               else
                                 opn+='<option value="'+i+'">'+v+'</option>';
                             });
                             jQuery('#itemName').html(opn);
                          });
                      },
                      update_add:function(){
                          jQuery('#form-submit').on('click',function(){
                             var flg=true;
                              var chek=false;
                             jQuery('#myTable').find('input[type="checkbox"]').each(function(){

                                if(jQuery(this).prop("checked") == true){
                                    var Currentinput=jQuery(this).prev();
                                    var l=0;
                                    var ival=Currentinput.val();
                                    if(typeof(ival.split('.')[1]) != "undefined" && ival.split('.')[1] !== null)
                                      l=ival.split('.')[1].length

                                    if(jQuery.trim(ival)=='' || l>2 || parseInt(ival)== 0){
                                      Currentinput.css('border-color','red');
                                      flg=false;
                                    }else{
                                      Currentinput.css('border-color','');
                                    }

                                    chek=true;
                                }

                             });
                            
                             if(flg && chek){

                                    if(confirm("All transaction will be affected for this date. do you want to continue ?")) {
                                        jQuery('#myForm-update').submit();
                                    } 
                             }

                            if(chek===false){
                               alert('Please select atleast one item from List');
                             }
                              
                          });
                    },

              };
            app.init();
          });


    jQuery(document).ready(function() {
            jQuery('#myTable').DataTable({
              
            });
        });

        </script>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        //$('#myTable').DataTable();
    });
</script>
</body>
</html>