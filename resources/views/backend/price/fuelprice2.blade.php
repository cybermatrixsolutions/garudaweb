@extends('backend-includes.app')

@section('content')
 <div class="content-wrapper">
  <div class="page-title">
    <div>
      <h1><i class="fa fa-dashboard"></i>&nbsp; Fuel Price Update
      </h1>
    </div>
    <div>
      <ul class="breadcrumb">
        <li>
           <form name="myForm" method="get" class="form-inline" action="{{url('fuel_price_management')}}">
               {{ csrf_field() }}    
              <div class="form-group">
                <label for="Filter">Filter</label>
                <input type="text" class="form-control datepickerabh"  placeholder="Date" name="date" value="@if(isset($search_date)) {{
                                 Carbon\Carbon::parse($search_date)->format('d/m/Y')}}@endif">
              </div>
              
              <button type="submit" class="btn btn-default ">Go</button>
          </form>
        </li>

        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
    
      </ul>
    </div>
  </div>
   
   <div class="row" style="padding-top: 80px;padding-right: 20px;">
      
         <div class="col-12 ">
           @if($date!=null)
             <span style="color:red;padding-left: 5px;"> * All Prices are inclusive tax </span>
                
           @endif
          </div>
         <div class="col-12 " style="padding-left: 20px;">
             @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
              @endif
             <center>
             
                @if(Session::has('success'))
                       <font style="color:red">{!!session('success')!!}</font>
                    @endif
            </center>
               @if($date!=null)
               <form name="myForm" id="myForm-update"  onsubmit="return confirm('Do you want to Continue?');" class="form-inline"  @if(isset($dateitem) && $dateitem->count()<=0) action="{{url('save_fuel_price')}}" @else action="{{url('update_fuel_price')}}" @endif enctype="multipart/form-data" method="post">
               {{ csrf_field() }}
               <input type="hidden" name='date' value="{{date("Y-m-d",strtotime($date))}}">
                <div class="row" >
                 <div class="col-12 " >
                 
                <div class="col-12 " >
                <table class="table table-striped" id="table-parent">
                      <thead>
                        <tr>
                          <th rowspan="2">
                             <span style="margin-top: 10px;position: absolute;">Fuel</span>
                             <div class="pull-right">
                              <span>Date</span><br>
                              <span>Time</span>
                             </div>
                          </th>
            <?php $amountpr=0; ?>
              @if(isset($dateofprices))
                            @foreach($dateofprices as $key=>$dateofprice)
                            <th class="date-price-header">

                                <?php $dates= new \Carbon\Carbon($key); ?>
                             <span class="date-price-header-1">
                                {{$dates->format('d/m/Y')}}
                             </span><br>
                              <span class="date-price-header-1">
                                {{$dates->format('h:i:s a')}}
                             </span>
                          </th>
                            @endforeach
                          @endif
             
                           @if(isset($dateitem) && $dateitem->count()<=0)
                             <?php $ndates= new \Carbon\Carbon($date); ?>
                                 <th>
                                    <p class="date-price-header-1" style="margin: 0">
                                      {{$ndates->format('d/m/Y')}}
                                   </p>
                                    <p class="date-price-header-1" style="margin: 0">
                                      {{date('h:i:s a',strtotime($roconfigTime))}}
                                   </p>
                                    <p class="date-price-header-1" style="margin: 0">
                                      <a href="#" id="copy-previous-day-price" style="color: #dca79e;">Copy previous day price</a>
                                   </p>
                                 </th>
                            @endif

            
                        </tr>
                      </thead>
                      <tbody>
                       
                      @foreach($TableItemPriceListModels as $TableItemPriceListModel)
                        <tr class="item-row"> 
                            <input type="hidden" name="item[]" value="{{$TableItemPriceListModel->id}}">
                              <td>{{$TableItemPriceListModel->Item_Name}}</td>
                              @if(isset($dateofprices))
                                  @foreach($dateofprices as $key=>$dateofprice)
                              <td>

                                
                                  <?php 
                                  $amountpr=0;
                                  
                                  $pr1=$TableItemPriceListModel->itemPrices()->where('effective_date',$key)->first(); ?>
                                    @if($date==date('Y-m-d',strtotime($key)))
                                          @if($pr1!=null)
                                          
                                          <input type="hidden" name="itemPrId[]" value="{{$pr1['id']}}">
                                           <input class="price-check numeriDesimal" type="text" name="price[]" value="{{bcadd($pr1['price'], 0, 2)}}">
                                          @else
                                          <input type="text" name="price[]" class="price-check numeriDesimal" value="{{bcadd(0, 0, 2)}}">
                                          @endif
                                    @else
                                        @if($pr1!=null)
                                            {{bcadd($pr1['price'], 0, 2)}}
                                             <?php $amountpr=bcadd($pr1['price'], 0, 2);?>
                                           @else
                                             {{bcadd(0, 0, 2)}}
                                              <?php $amountpr=bcadd(0, 0, 2);?>
                                      @endif
                                    @endif
                                
                             </td>
                                @endforeach
                              @endif

                               @if(isset($dateitem) && $dateitem->count()<=0)
                                 <td colspan="4">
                                   <input type="text" data-old="{{$amountpr}}" name="price[]" class="price-check numeriDesimal" value="{{bcadd(0, 0, 2)}}">
                                 </td>
                               @endif

                        </tr>
                      @endforeach
                     </tbody>
                    </table>
                      <button id="form-submit" type="button" class="btn btn-default pull-right ">Submit</button>
                    </div>
                     </div>
                </form>
             @endif

         </div>
    </div>

</div>
@endsection
@section('script')
<script>
jQuery(function(){
  app={
    init:function(){
         jQuery(".datepickerabh").datepicker({ 
                format: 'dd/mm/yyyy',
                endDate: '<?php if($enddate!=null && date("Y-m-d",strtotime($enddate))!=date("Y-m-d")){echo date("d/m/Y",strtotime($enddate)+86400);}else{ echo date("d/m/Y");} ?>',
                autoclose: true

          });
               jQuery(".numeriDesimal").on('keyup',function() {
                        var creant = jQuery(this);
                        creant.val(creant.val().replace(/[^\d.]/g, ''));
                    });


         app.add();
         app.copyPreviousDayPrice();

    },
    copyPreviousDayPrice:function(){
                        
                        jQuery('#copy-previous-day-price').on('click',function(){
                          jQuery('.item-row').each(function(){
                             var currentprice= jQuery(this).find('.price-check').data('old');
                           
                             jQuery(this).find('.price-check').val(currentprice);
                            

                          });
                        });
    },
    add:function(){
      jQuery('#form-submit').on('click',function(){
         var flg=true;
         jQuery('#table-parent').find('.price-check').each(function(){
             var l=0;
             var ival=jQuery(this).val();
            if(typeof(ival.split('.')[1]) != "undefined" && ival.split('.')[1] !== null)
              l=ival.split('.')[1].length

            if(jQuery.trim(ival)=='' || l>2 || parseInt(ival)== 0){
              jQuery(this).css('border-color','red');
              flg=false;
            }else{
              jQuery(this).css('border-color','');
            }

         });

         if(flg){

                if(confirm("All transaction will be affected for this date. do you want to continue ?")) {
                    jQuery('#myForm-update').submit();
                } 
         }
          
      });
      
    },

  };
  app.init();
});
</script>
@endsection