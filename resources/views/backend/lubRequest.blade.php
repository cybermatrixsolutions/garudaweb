<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<style type="text/css">
  span.select2.select2-container.select2-container--default.select2-container--above{
    width: 100%!important;
  }
  .select2-container {
   
    width: 100% !important;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #211d1d !important;
   
}
.btn-success {
    padding: 1px 6px;
}
.nopadding{
  padding: 0px!important;
}
.form-horizontal .control-label {
    text-align: right;
    margin-bottom: 0;
    padding-top: 3px;
    background: #00786a;
    display: block;
    text-align: left;
    padding-left: 15px;
    color: #fff;
    padding: 11px;
}
</style>

</head>

<body class="sidebar-mini fixed">
<div class="wrapper"> 
<!-- Navbar-->
<!-- Navbar-->

 @include('backend-includes.header')

      <!-- Side-Nav-->

  @include('backend-includes.sidebar')
  
<div class="content-wrapper">
  <div class="page-title">
    <div>
      <h1><i class="fa fa-dashboard"></i>&nbsp;Lube Request</h1>
    </div>
    <div>
      <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

        <li><a href="#">Lube Request</a></li>
      </ul>
    </div>
  </div>
   <div class="">
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      </div>
      <div class="row card" style="margin-top:80px;">
           <form class=" form-horizontal" action="{{'save_lube_request'}}" method="post">
        <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
        <div class="col-md-6 col-sm-6">
          <div class="row" style="margin: 0px!important">
          <!-- Modal -->    <div class="col-sm-4 nopadding">
                            <label class="control-label" for="email">Select Vehicle No.</label>
                                <select class="form-control registration_number" required="required" name="Vehicle_Reg_No" id="registration_number">                
                            </select>
                            </div>
                             <div class=" col-sm-4 nopadding">
                            <label class="control-label" for="email">Driver Mobile Number</label>
                               <input type="text" class="form-control number Request_Value_input" maxlength="10"  required="required" name="mobile1" id="Request_Value_input"  placeholder="Driver Mobile Number">               
                          
                            </div>
                             <input type="hidden" name="customer_mobile" id="id-customer-mobile" value="{{ $Customer_Mobile }}">

                             <div class=" col-md-4 nopadding">
                            <label class="control-label">Other Mobile Number</label>
                             

                            <select class="form-control number Request_Value_input" maxlength="10"  required="required" name="mobile"   placeholder="Driver Mobile Number" id="other_mobile">   
                            <option value="{{$Customer_Mobile}}">{{$Customer_Mobile}} - ({{$Customer_Name}})</option>
                             @foreach($luberequest_Mobile_no as $mobileno)
                                 
                                
                                <option value="{{$mobileno->Driver_Mobile}}">{{$mobileno->Driver_Mobile}} - ({{$mobileno->Driver_Name}})</option>
                             
                                      @endforeach            
                          
                            </div>  
                           <!-- <div class="form-group">
                            <div class="row">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button style="margin-top: 20px;"  type="submit" class="btn btn-default pull-right" onclick="return getmobilenumber()">Submit</button>
                        </div>
                         </div>
                      </div>   -->
                        
                  <div class="">
                   
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       @if(Auth::user()->user_type==3)

                               <input type="hidden" name="RO_code" value="{{ Auth::user()->customer->RO_code }}" id="sel1">{{ Auth::user()->customer->RO_code }}

                          @else
                              <select class="form-control" style="visibility:hidden;" name="RO_code" id="sel1">
                               
                                  @foreach($datas as $ro_master)
                                      <option name="company_id" value="{{$ro_master->RO_code}}"
                                              >{{$ro_master->pump_legal_name}}</option>
                                  @endforeach
                              </select>
                            @endif
                      @if(Auth::user()->user_type==3)
                          
                               <input type="hidden" name="customer_code" value="{{ Auth::user()->customer->Customer_Code }}" id="customer_code">{{ Auth::user()->customer->Customer_Code }}

                          @else
                             <select class="form-control" style="visibility:hidden;"  name="customer_code" id="customer_code">  
                             <option value="{{Auth::user()->customer->Customer_Code}}">{{Auth::user()->customer->company_name}}</option>       
                            </select>
                            @endif
         </div>
         </table>
           </div>
           </div>
             <div class="col-md-6 col-sm-6">
                      <table class="table table-striped no-footer">
          <thead>
            <tr>
            
              
              <th>Select Item</th>
            
              <th>Quantity<a style="float:right" id="addNew" class="btn btn-success" href="#">+</a></th>

            </tr>
          </thead>
          <tbody id="parant-app-row">
           <tr class='parent-tr' id="parant-row">
           
            <td>  
                <select class="form-control"  required="required" name="item_id[]">
                             
                                                    @foreach($item_id as $item)
                                                        <option value="{{$item->id}}"
                                                                >{{$item->Item_Name}}
                                                              </option>
                                                    @endforeach
                            </select>
            </td>
            <td>
             <a href='#' style="position: absolute;right: 7px;margin-top: -9px;" class='deleteNew'>X</a>
              <input type="number" step="0.01" data-qt="0" class="form-control"  required="required"   name="petroldiesel_qty[]" placeholder="Quantity Value ">
            </td>
           </tr>
           
          </tbody>
        </table>
      
                        </div>
      <div class="form-group">
                            <div class="row">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button style="margin-top: -37px; margin-right: 25px;"  type="submit" class="btn btn-default pull-right" onclick="return getmobilenumber()">Submit</button>
                        </div>
                         </div>
                      </div>  

                     
                    </form>
                  </div>
            
            

  <div class="row">
    <div class="col-md-12">
      <div class="table-responsive">
        <table class="table table-striped" id="myTable">
          <thead>
            @if(isset($lube_list) && count($lube_list)  != 0)
            <tr>
              <th>S.No.</th>
              <!-- <th>Outlet Name</th>
              <th>Customer Name</th> -->
              <th>Items</th>
              <th>Request Date</th>
              <th>Execution date</th>
              
              <th>Vehicle Reg No</th>
               
              <th>Status</th>
               <th>Actions</th>
          </thead>
          <tbody>
            <?php $i=0;?>
             @foreach($lube_list as $list)
             <?php $i++;?>
            <tr>
              <td>{{$i}}</td>
              <!-- <td scope="row">{{$list->pump_legal_name}}</td>
              <td>{{$list->Customer_Name}}</td> -->
               <td>{{$list->Item_Name}}</td>
               <td>{{Carbon\Carbon::parse($list->request_date)->format('d/m/Y h:i:s')}}</td>
               <td>@if($list->Execution_date != null ){{Carbon\Carbon::parse($list->Execution_date)->format('d/m/Y')}} @endif</td>
              
              
             
               <td>{{$list->Vehicle_Reg_No}}</td>
                
               <td>  @if($list->is_active==1)

                   <a  title="Active" href="lube_request/active/{{$list->id}}/0" onclick="return confirm('Do you want to deactivate?');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  @else

                   <a title="Deactive" href="lube_request/active/{{$list->id}}/1" onclick="return confirm('Do you want to activate?');"> 

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                @endif 
          </td>
              <td><div class="btn-group">
                  <button type="button" class="btn btn-danger">Action</button>
                  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                  <ul class="dropdown-menu" role="menu">
                     <li><a href="lube_request/view/{{$list->id}}">View</a></li>
                     
                  <!--  <li><a href="lube_request/delete/{{$list->id}}" onclick="return confirm('are you sure to delete?');">Delete</a></li>-->
                  </ul>
                </div>
             </td>
            </tr>
             @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>

<!-- Javascripts-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.js"></script>

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script>


<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="js/index.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
   $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
   $('.table-responsive').css( "overflow", "auto" );
})
 function  getmobilenumber() {
        var mobile = getElementsByClassName("Request_Value_input");
        var pattern = /^[0]?[789]\d{9}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
            return true;
        }
        alert("It is not valid mobile number.input 10 digits number!");
        return false;
    }
    $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
</script>

</script>

<script type="text/javascript">
 jQuery(function(){
    var currentP=jQuery('#parant-row');

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
              vil.getFuel();
               vil.addRow();
              vil.formsub();
               vil.Registration_Number();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
            
            jQuery('#registration_number').on('change',function(){
                 
                 vil.Registration_Number();
              });
           
          
           },
            addRow:function(){
            jQuery('#addNew').on('click',function(e){
              e.preventDefault();
              var cnt=jQuery('#parant-row').html();
              jQuery('#parant-app-row').append("<tr class='parent-tr'>"+cnt+"</tr>");
              
                jQuery(".datepicker").datepicker({ 
                      autoclose: true, 
                      todayHighlight: true,
                      format: 'dd/mm/yyyy'
                }).datepicker('update', new Date());
            });


            jQuery(document).on('click','.deleteNew',function(e){
              e.preventDefault();
              
              jQuery(this).parents('.parent-tr').remove();
                
            });

            
           },
           formsub:function(){
                jQuery('#tbnhide').on('click',function(){
                    var flg=true;
                    var q=true;
                    jQuery('#parant-app-row').find("input, textarea, select").each(function(){
                     
                       if(jQuery.trim(jQuery(this).val())==''){
                          jQuery(this).css('border-color','#e27777');
                          flg=false;
                       }else{
                         jQuery(this).css('border-color','');
                       }
                      
                        if(jQuery(this).hasClass('petroldiesel_qty')){
                            console.log('helo');
                            console.log(jQuery(this).data('qt'));
                            console.log(parseInt(jQuery(this).val()));
                          if(parseInt(jQuery(this).data('qt'))<parseInt(jQuery(this).val())){
                             jQuery(this).css('border-color','#e27777');
                             flg=false;
                             q=false;
                           }
                        }
                    });
                   
                   if(q==false){

                      alert("Please Enter Below Capacity Limit ");
                   }

                    if(flg==true && q==true){
                      jQuery('#form12').submit();
                    }

                });
           },

           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#sel1').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html();

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                  
                jQuery('#registration_number').html(opt);
                 vil.Registration_Number();
                 vil.getFuel();
               });
           },
           Registration_Number:function(){
             jQuery.get('getregierRegistration_Number',{
                registration_number:jQuery('#registration_number').val(),
                 RO_code:jQuery('#sel1').val(),
                customer_code:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                    console.log('get driver mobile'+data);

                    var customer_mobile=$('#id-customer-mobile').val();
                    console.log('customer_mobile'+customer_mobile);
               // jQuery('.Request_Value_input').val(data);
               if(data){

                 jQuery('#Request_Value_input').val(data);
                  jQuery('#other_mobile').val(data).change();
               }else{

                 jQuery('#Request_Value_input').val(customer_mobile);
                  jQuery('#other_mobile').val(customer_mobile).change();
               }
                /* jQuery("#other_mobile option").filter(function(){
                  return (jQuery(this).text() == jQuery('#Request_Value_input').val());
                 }).prop('selected',true);*/
                 

               });
           },

           getFuel:function(){
             var ro=jQuery('#sel1').val();
              var cus=jQuery('#customer_code').val();
             jQuery.get('getfueltype/'+ro+'/'+cus+'/LUBE',{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#tags').html(opt);
               });
           },

     }
     vil.init();
  });
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
 $('#tags').select2({
  
    tags: true,
    tokenSeparators: [','], 
    placeholder: "Slect Item "
});


</script>
</body>
</html>