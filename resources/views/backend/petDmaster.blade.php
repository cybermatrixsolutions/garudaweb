<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Invoice</title>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
 <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Petrol Diesel Price </h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Petrol Diesel Master List</a></li>
        </ul>
      </div>
    </div>
   <div class="">
     <center>@if(Session::has('success'))
           <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
    </div>  
   @if ($errors->any())
<div class="alert alert-danger">
  <ul>
      @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
      @endforeach
  </ul>
</div>
@endif
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Petrol Diesel Price </h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal" action="{{'addpetDmaster'}}" enctype="multipart/form-data" method="post">
                         {{ csrf_field() }}
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">RO Code</label>
                          <div class="col-sm-9">
                            <select class="form-control" id="RO_code" name="RO_code">
                             @foreach($data1 as $row) 
                              <option value="{{$row->RO_code}}">{{$row->RO_code}}</option>
                              
                               @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Petrol Diesel Type</label>
                          <div class="col-sm-9">
                          <select class="form-control" id="petroldes" name="petroldiesel_type"></select>
                          </div>
                        </div>
                         
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Price</label>
                          <div class="col-sm-9">
                            <input type=number step=0.01 class="form-control" id="" name="Price" placeholder="Price" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Price Date</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control datepicker"  id="" name="Price_Date" placeholder="Price Date" required>
                          </div>
                        </div>
            
                          
                        
                         <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>S.No.</th>
                <th>RO Code</th>
                <th>Price</th>
                 <th>Petrol Diesel Type</th>
                 
                 <th>Price_Date</th>
                 <th>Status</th>
                <th>Actions</th>
              </tr>
            <tbody>
               <?php $i=0; ?>
               @foreach($data as $rar)
               <?php $i++;?>
              <tr>
                <td>{{$i}}</td>
                <td>{{$rar->RO_code}}</td>
                <td>{{$rar->Price}}</td>
                <td>{{$rar->type}}</td>
                <td>{{Carbon\Carbon::parse($rar->Price_Date)->format('d-m-Y')}}</td>
                <td> <?php if($rar->is_active==1){?>

                   <a  title="Active" href="petDmaster/active/{{$rar->id}}/0"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="petDmaster/active/{{$rar->id}}/1"> 

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
          </td>
               
               
                
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                    @if($rar->is_active==1)
                     <li><a href="PDmaster_update/{{$rar->id}}" >Edit</a></li>
                   @endif
                     <!--  <li><a href="petDmaster_delete/{{$rar->id}}" onClick="return confirm('Are you sure?');">Delete</a></li> -->
                    </ul>
                  </div></td>
              </tr>
               @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Javascripts--> 
<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script> 
<!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
 --><script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="js/index.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#RO_code').on('change',function(){
                  vil.getcom();
              });
           
           
           },
           getcom:function(){
             
               jQuery.get('getpetmaster',{
                
                rocode:jQuery('#RO_code').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#petroldes').html(opt);

                   
               });
           },
          

     }
     vil.init();
  });
</script>

</body>
</html>