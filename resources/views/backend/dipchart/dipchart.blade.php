@extends('backend-includes.app')

@section('content')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Dip Chart</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Dip chart</a></li>
        </ul>
      </div>

    </div>
   <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
           <div class="">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" >Add Dip Chart</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                     <form class="form-horizontal" id="form1"  action="{{route('dipCharts')}}" name="f" onsubmit="return check(this)" enctype="multipart/form-data" method="post">
                          {{ csrf_field() }}
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="Type">Measurement Type</label>
                            <div class="col-sm-8">
                            <select name="lenght_type" class="form-control">
                              <option value="cm"> CM </option>
                               <option value="mm"> MM </option>
                            </select>
                            </div>
                         </div>

                         <div class="form-group">
                          <label class="control-label col-sm-4" for="DipChart volume">Dip Chart Volume Type <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-6">
                              
                            <input type="text" id="volumedip" required="required" class="form-control"  placeholder="Dip Charts volume" name="volume"> 
                          
                          </div>
                         
                         
                         
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-4" for="Discretion">Dip Chart Description <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <textarea required="required" id="discretiondip" class="form-control" placeholder="Dip Charts Discretion" name="discretion"> </textarea>
                        	     </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="File">File(csv) <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-4">
                             <input type="file" id="inputfile" required="required" class="form-control"  placeholder="File(csv)" name="file">
                          </div>
                          <div class="col-sm-3">
                          <a href="{{url('download/csv')}}">Export as CSV</a>
                        </div>
                        </div>

                       

                    
                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <input  type="button" id="inputButton" class="btn btn-default submit-button"  value="Submit">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--end add box-->
    <!--list contant-->
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>S.No.</th>
                 <th>Dip Chart Volume Type</th>
                 <th>Measurement Type</th>
                 <th>Dip Chart Description</th>
                 <th>Date</th>
                 <!--<th>Status</th>-->
                 <th>Actions</th>
            </thead>
            <tbody>
              <?php $i=0; ?>
      
              @foreach($dipcharts as $dipchart) 
      
              <?php $i++ ;?>
               <tr>
                <td>{{$i}}</td>
                <td>{{$dipchart->volume}}</td>
                <td>{{strtoupper($dipchart->lenght_type)}}</td>
                <td>{{$dipchart->discretion}}</td>
                 <td>{{date('d/m/Y',strtotime($dipchart->created_at))}}</td>
                  
                 <!-- <td> @if($dipchart->is_active==1)

                   <a  title="Active" href="{{route('dipCharts')}}/active/{{$dipchart->id}}/0" onclick="return confirm('Do you want to deactivate?');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  @else

                   <a title="Deactive" href="{{route('dipCharts')}}/active/{{$dipchart->id}}/1" onclick="return confirm('Do you want to activate?');">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
                  @endif
                </td>-->

                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      <!--<li><a href="#">View</a></li>-->
                     
                      <li><a href="{{route('dipCharts')}}/view/{{$dipchart->id}}" >View</a></li>
                      </ul>
                  </div></td>
              </tr>
               @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
function check(form){ 
 if (!/\.csv$/.test(form.file.value)){
  alert('.csv only')
  return false
  }
 else return true
}
</script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('#myTable').DataTable();
});

jQuery(function () {

    jQuery('#inputButton').on('click',function(){
        var filename=jQuery('#inputfile').val();
        var valid_extensions = /(\.csv)$/i;   
        if(valid_extensions.test(filename))
        { 
          
          var volumedip=jQuery('#volumedip').val();
          var discretiondip=jQuery('#discretiondip').val();
           if(jQuery.trim(volumedip)==''){
            alert('Please Insert Dip Chart Volume Type First');
           }else if(jQuery.trim(discretiondip)==''){
            alert('Please Insert Dip Charts Description First');
           }else{

            jQuery('#form1').submit();

           }
          
        }
        else
        { 
           alert('please select file in csv format');

           return false;
        }
    });
});
</script>

@endsection