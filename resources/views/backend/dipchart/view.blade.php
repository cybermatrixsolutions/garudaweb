@extends('backend-includes.app')

@section('content')


<div class="content-wrapper">
	<div class="page-title">
	<div>
	<h1><i class="fa fa-dashboard"></i>&nbsp;Dip Charts View</h1>
	</div>
	<div>
	<ul class="breadcrumb">
	<li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
	  <li><a href="{{route('dipCharts')}}">Dip charts</a></li>
	</ul>
</div>

    </div>
	<div class="row">
	      <div class="col-md-12">
	        <div class="table-responsive">
	          <table class="table table-striped" id="myTable">
	            <thead>
	              <tr>
	                <th>S.No.</th>
	                 <th>Dip</th>
	                 <th>Vol</th>
	                 <th>Diff/mm</th>
	            </thead>
	            <tbody>
	             <?php $i =1; ?>
	              @foreach($dipcharts as $dipchart) 
	     
	               <tr>
	                <td>{{$i}}</td>

	                <td>{{$dipchart->dip}}</td>
	                <td>{{$dipchart->vol}}</td>
	                <td>{{$dipchart->diff_mm}}</td>

	                 <?php $i++; ?>
	              </tr>
	               @endforeach
	            </tbody>
	          </table>
	        </div>
	      </div>
	    </div>
</div>



@endsection
@section('script')

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>

@endsection
