<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>

</head>

<body class="sidebar-mini fixed">

<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Tank  Update
</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="{{url('/tank')}}
">Tank 
 </a></li>
        </ul>
      </div>
    </div>
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row" style="margin-top:-15px !important;">
                         @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
             @endif
             <div class="">
                
               
                <center>@if(Session::has('success'))
                    <font style="color:red">{!!session('success')!!}</font>
                  @endif</center>
                
              </div>
         
          
                      <form name="myForm" class="form-horizontal" action="{{url('tank_edit_data')}}/{{$tank_mangement_data->id}}" enctype="multipart/form-data" method="post" onsubmit="return confirm('Do you want to Continue?');">
            
                <input type="hidden" name="_token" value="{{ csrf_token()}}"/>
                <input type="hidden" name="RO_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
               <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Pump Legal Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" disabled name="RO_code" id="sel1">
                             
                                                    @foreach($datas as $ro_master)
                                                        <option name="company_id" value="{{$ro_master->RO_code}}"
                                                                 @if($tank_mangement_data->RO_code == $ro_master->RO_code) selected @endif>{{$ro_master->pump_legal_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div> -->
            
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Tank Number <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                                        <input type="text" required="required"   class="form-control checkunique" name="Tank_Number" id=""
                                               data-table="tbl_tank_master"  data-colum="Tank_Number" data-id="{{$tank_mangement_data->id}}" data-mass="Tank Number Already Exist" required="required"  value="{{$tank_mangement_data->Tank_Number}}" placeholder="Tank Number">
                                    </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Fuel Type</label>
                          <div class="col-sm-9">
                             <select class="form-control" id="Pedestal_id" name="fuel_type">
                            @foreach($fuel_type as $fuel) 
                              <option value="{{$fuel->id}}" @if($tank_mangement_data->fuel_type == $fuel->id) selected @endif >{{$fuel->Item_Name}}</option>
                              
                               @endforeach
                             
                            </select>
                            
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Capacity <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input type="text" required="required" class="form-control numeriDesimal" name="Capacity" id="capacity" value="{{$tank_mangement_data->Capacity}}" placeholder="Capacity">
                          </div>
                        </div>
                        
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Unit of Measure <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                                <select class="form-control" required="required" name="Unit_of_Measure" id="Unit_of_Measure">
                             
                                                    @foreach($unit_measure as $unit)
                                                        <option  value="{{$unit->id}}"
                                                                @if($tank_mangement_data->Unit_of_Measure==$unit->id) selected @endif  >{{$unit->Unit_Symbol}}-{{$unit->Unit_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Opening Stock</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control numeriDesimal" name="opening_reading" id="openstock" value="{{$tank_mangement_data->Opening_Reading}}" placeholder="Opening Stock">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Opening Date </label>
                          <div class="col-sm-9">

                            <input class="form-control" id="datepicker" name="opening_date"value="{{date('d/m/Y',strtotime($tank_mangement_data->Opening_Date))}}" type="text" />

                          </div>
                        </div>
                      
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Remarks</label>
                          <div class="col-sm-9">
                            <input type="text"  class="form-control" name="remarks" value="{{$tank_mangement_data->Remarks}}" id="" placeholder="Remarks">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Active From Date</label>
                          <div class="col-sm-9">

                            <input type="text" class="form-control" id="datepickers" name="active_from_date" value="{{date('d/m/Y',strtotime($tank_mangement_data->Active_From_Date))}}" placeholder="Active From Date">

                          </div>
                        </div>
                        <div class="form-group">
            
                          <div class="col-sm-offset-3 col-sm-9">
                           <!-- <button type="submit" class="btn btn-default">Submit</button>-->
              <center><input type="submit" id="success-btn" onclick="return validateForm()" class="btn btn-primary site-btn submit-button" value="Submit"></center>
                          </div>
                        </div>
           
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>
	
	
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script type="text/javascript">
  function validateForm() {
             //alert("hello");
     var capacity = document.getElementById("capacity").value;
     var openstock = document.getElementById("openstock").value;
     
    if(float(openstock) > float(capacity) ){
       alert("Wrong Opening Stock  !!");
        return false;
    }
     
}
</script>
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script type="text/javascript">



   $(function () {
  $("#datepickers").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
  $(function () {
  $("#datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
   jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "inherit" );
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "auto" );
    })
</script>
<script type="text/javascript">
    jQuery(function(){
        var url1 ="{{url('check_Pedestal_Number_unique')}}";
        var vil={
            init:function(){
                vil.selectRo();

            },
            selectRo:function(){

                jQuery('.checkunique').on('blur',function(){
                    var id=0;
                    var table=jQuery(this).data('table');
                    var mass=jQuery(this).data('mass');
                    var colum=jQuery(this).data('colum');
                    id=jQuery(this).data('id');

                    udat=jQuery(this).val();
                    jQuery.get(url1,{
                        table:table,
                        colum:colum,
                        unik:udat,
                        id:id,

                        '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                    },function(data){

                        if(data=='true'){
                            alert(mass);
                            jQuery('.submit-button').attr('disabled',true);

                        }else{
                            jQuery('.submit-button').attr('disabled',false);
                        }

                    });
                });


            },

        }
        vil.init();
    });
</script>

</body>
</html>