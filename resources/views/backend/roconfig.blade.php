<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">

<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style type="text/css">
  #errmsg
{
color: red;
}
</style>
<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  

  <!-- Side-Nav-->

  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;General</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">General</a></li>
        </ul>
      </div>
    </div>
    
                 

    <div class="row">
              
      <div class="col-md-12 col-sm-12">
        <div class="row">
           <center>@if(Session::has('success'))
                       <font style="color:red">{!!session('success')!!}</font>
                    @endif</center>
                    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif   
<div class="row">
                      <form class="form-horizontal" action="{{url('admincofigsave')}}" method="post">
                      {{ csrf_field() }}

                       @foreach($AdminconfigtModels as $AdminconfigtModel)
                        <div class="form-group"  id="{{$AdminconfigtModel->field_name}}_container">
                          <label class="control-label col-sm-4" for="email">{{$AdminconfigtModel->field_lable}}</label>
                          <div class="col-sm-4">
                            <input type="hidden" name="field[]" value="{{$AdminconfigtModel->field_name}}">
                            @if($AdminconfigtModel->input_type=='checkbox')
                                <input style="margin-top:15px;" type="checkbox" name="{{$AdminconfigtModel->field_name}}" id="{{$AdminconfigtModel->field_name}}" @if($AdminconfigtModel->value!=null)checked @endif/> 
                             @elseif($AdminconfigtModel->input_type=='datetimepicker')
                              <?php $ndates= new \Carbon\Carbon($AdminconfigtModel->value); ?>
                                <input type="text" class="form-control" id="datetimepicker1"  required="required" name="{{$AdminconfigtModel->field_name}}" id="{{$AdminconfigtModel->field_name}}" value="{{$ndates->format('d/m/Y h:s A')}}"  placeholder="Value"/> 
                            @else
                               <input type="text" class="form-control" required="required" name="{{$AdminconfigtModel->field_name}}" id="{{$AdminconfigtModel->field_name}}" value="{{$AdminconfigtModel->value}}"  placeholder="Value"/> 
                            @endif
                            <!-- <input type="text" class="form-control" id="datetimepicker1"  required="required" name="{{$AdminconfigtModel->field_name}}" value="{{$AdminconfigtModel->value}}"  placeholder="Value"/>  -->
                          </div>
                        </div>
                       @endforeach

                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-4">
                     <center><input type="submit"  class="btn btn-primary  submit-button"  value="Submit"></center>
                          </div>
                        </div>
                      </form>
                    </div>   
             
           
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            
          </div>
        </div>
      </div>
    </div>
   
  </div>
</div>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 


<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
$(function() {
	$('#datetimepicker1').datetimepicker({
		defaultDate:'1/01/2018 06:00:00 AM',
		format:'DD/MM/YYYY HH:mm:ss A',
	});
	
	setManualSlipBookOption();
	
	$(document).on("click","#use_manual_slip_book",function(e){
		setManualSlipBookOption();
	});
});
function setManualSlipBookOption(){
	if($("#use_manual_slip_book").is(":checked")){
		$("#slip_book_container").show();
		//$("#slip_book").prop('checked', true);
	}
	else{
		$("#slip_book").prop('checked', false);
		$("#slip_book_container").hide();
	}
}
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});
$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<footer>
  <div class="footer-sec" style="margin-top: 35px !important;">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
</body>
</html>