<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Customer Fuel Edit</h1>
      </div>
      <div>
        <ul class="breadcrumb">
           <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('ro_transaction_list')}}">Transaction List</a></li>
        </ul>
      </div>
    </div>
  
  
                           <div class="">  
                             </div>
  
  
  
  
    <div class="row" style="margin-top: 50px;">

      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           
            <div class="row" style="margin-top: -31px !important;">
             @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                      <form class="form-horizontal" action="{{url('slipupdated')}}/{{$customer_slip->id}}" enctype="multipart/form-data" method="post">
            
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="customer_driver_ro_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
                        @if($customer_slip->trans_mode =="manual")
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Company Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control" required="required" name="customer_code" id="customer_code" data-customer="{{$customer_slip->customer_code}}">
                              
                                                   
                            </select>
                          </div>
                          </div>
                          @else

                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Company Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control"  required="required" name="customer_code" id="customer_code" data-customer="{{$customer_slip->customer_code}}">
                              
                                                   
                            </select>
                          </div>
                          </div>
                         
                          @endif



                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Vehicle Number <span style="color:#f70b0b;">*</span> </label>
                          <div class="col-sm-9">
                            <select class="form-control "   required="required" name="Vehicle_Reg_No" id="registration_number">
                              
                                                   
                            </select>

                             </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Fuel Type<span style="color:#f70b0b;">*</span> </label>
                          <div class="col-sm-9">
                            
                            <select class="form-control "   required="required" id="Fuel_Type" name="Fuel_Type" data-fuel="{{$customer_slip-> item_code}}">
                              @foreach($fuel_type as $fuel_types)
                              <option value="{{$fuel_types->id}}" @if($fuel_types->id == $customer_slip-> item_code) selected @endif>{{$fuel_types->Item_Name}}</option>
                              @endforeach
                                                   
                            </select>

                             </div>
                        </div>
                        
                        
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Request Date <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control datepickers" required="required" name="request_date" id="" value="{{Carbon\Carbon::parse($customer_slip->request_date)->format('d/m/Y')}}" placeholder="Driver Name">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Quantity <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control"  required="required" name="petroldiesel_qty" id="" value="{{$customer_slip->petroldiesel_qty}}" placeholder="Driver Name">
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Price <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                              <select class="form-control" id="priceitem" style="text-align: right;width: 100px;" required="required" name="price">
                                <option value="{{$customer_slip->item_price}}">{{$customer_slip->item_price}}</option>
                               </select>
                           </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Slip No. <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" readonly  value="{{$customer_slip->Request_id}}" placeholder="Driver Name">
                          </div>
                        </div>
                        
                        
            
                       
                        <div class="form-group">
            
                          <div class="col-sm-offset-3 col-sm-9">
                           <!-- <button type="submit" class="btn btn-default">Submit</button>-->
              <center><input type="submit" id="success-btn" class="btn btn-primary site-btn" value="Submit" onclick="return getmobilevalid()"></center>
                          </div>
                        </div>
           
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>
  
  
  </div>
</div>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 


<script type="text/javascript">
  /*$(function () {
  $(".datepickers").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        defaultDate:'dd/mm/yyy',
        format: 'dd/mm/yyyy',
  }).datepicker('update');
});*/

</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
function  getmobilevalid() {
        var mobile = document.getElementById("mobile").value;
        var pattern = /^[0]?[789]\d{9}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
            return true;
        }
        alert("It is not valid mobile number.input 10 digits number!");
        return false;
    }
    $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
</script>



<script type="text/javascript">
  jQuery(function(){
        var url1="{{url('getrocode')}}";
        var url2="{{url('getregierids')}}";
        var url3="{{url('getietmpricebydate')}}";
        var ty="{{$customer_slip->petrol_or_lube}}";
     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
              vil.getItemPrice();
              jQuery(".datepickers").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true,
                  defaultDate:'dd/mm/yyy',
                  format: 'dd/mm/yyyy',
            }).datepicker('update').on('changeDate', function(){
                       //currentP=jQuery(this).parents('.parent-tr');
                       vil.getItemPrice();
                       //console.log('hello');
                    });

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#Fuel_Type').on('change',function(){
                   vil.getItemPrice();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });

             jQuery('#registration_number').on('change',function(){
                 vil.getFuel();
              });

           },
           getcom:function(){
               jQuery.get(url1,{
                rocode:jQuery('#sel1').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){
                   var customer=jQuery('#customer_code').data('customer');
                     if(customer==index)
                        opt+='<option selected value="'+index+'">'+value+'</option>';
                      else
                      opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);
                   vil.getcrg();
               });
           },
           getcrg:function(){

             jQuery.get(url2,{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var registration=jQuery('#customer_code').data('registration');
                 var opt='';
                  jQuery.each(data, function(index,value){

                     if(registration==index)
                        opt+='<option selected value="'+value+'">'+value+'</option>';
                      else
                      opt+='<option value="'+value+'">'+value+'</option>';

                  });
                  
                   
                jQuery('#registration_number').html(opt);
                vil.getFuel();
               });
           },
            getItemPrice:function(){
               var item=jQuery('#Fuel_Type').val();
                var request_date;

              if(ty==1)
                request_date=jQuery('.datepickers').val();
 
                 jQuery.get(url3+'/'+item,{
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                    'request_date':request_date,
                   },function(data){
                    var str='';
                     var flg=false;
                      jQuery.each(data,function(i,v){
                  
                         flg=true;
                         str+='<option value="'+v+'">'+v+'</option>';
                      });
                      
                      jQuery('#priceitem').html(str);

                     if(flg){
                      
                      jQuery('#success-btn').attr('disabled',false);
                     }else{
                      
                      jQuery('#success-btn').attr('disabled',true);
                     }


                   });
               
           },
            getFuel:function(){
              var ro=jQuery('#sel1').val();
              var cus=jQuery('#customer_code').val();
              var registration_number=jQuery('#registration_number').val();
              
             jQuery.get("{{url('getfueltype')}}/"+ro+"/"+cus+"/FUEL",{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                registration_number:registration_number,
               },function(data){
                 var sfuel=jQuery('#Fuel_Type').data('fuel');
                 var opt='';
                  jQuery.each(data, function(index,value){
                     
                     if(sfuel==index)
                      opt+='<option selected value="'+index+'">'+value+'</option>';
                      else
                      opt+='<option value="'+index+'">'+value+'</option>';
                  });
                   
                   
                jQuery('#Fuel_Type').html(opt);
               });
           },


     }
     vil.init();
  });
</script>


</body>
</html>
