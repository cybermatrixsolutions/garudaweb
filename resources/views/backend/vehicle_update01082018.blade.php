<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')  
  
 
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Vehicle  Update</h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
<li><a href="{{url('/vehiclemanagement')}}
">Vehicle </a></li>
        </ul>
      </div>
    </div>
    <br>
    <br>
     <div class="">
                             <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                            </div>  
                             @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
     <div class="row">
           <div class="col-md-6 col-sm-8 col-xs-12 vat">
            <form class="form-horizontal" action="{{url('/vehiclemanagement/update/'.$VechilManage->id)}}" enctype="multipart/form-data" method="post">
              {{ csrf_field() }}
                        
                         
                         
                         
                            <input type="hidden" name="Ro_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
                            <!-- <select class="form-control" required="required" readonly id="sel1" name="Ro_code">
                              @foreach($data1 as $row) 
                              <option value="{{$row->RO_code}}" @if($VechilManage->Ro_code == $row->RO_code) selected @endif >{{$row->pump_legal_name}}</option>
                              
                               @endforeach
                            </select> -->
                         
                       
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="Customer">Customer Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                              <select class="form-control" data-value="{{$VechilManage->Customer_code}}" required="required" id="customer_code" name="Customer_code">
                                </select>
                           
                          </div>
                        </div>
                        
                       <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Customer code</label>
                         <div class="col-sm-9">
                            <input type="text" class="form-control" id="" name="Customer_code" value="{{$VechilManage->Customer_code}}" placeholder="" readonly>
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Registration Number <span style="color:#f70b0b;">*</span></label>
                           <div class="col-sm-9">
                            <?php 
                                 $mgs='';
                                if($QR_Code!=null)
                                {
                                  $mgs="readonly";
                                }
                            ?>
                            <input type="text" class="form-control checkunique" data-id="{{$VechilManage->id}}" data-mass="Registration Number Already Exist" data-table="tbl_customer_vehicle_master" <?php echo $mgs;?>  data-colum="Registration_Number" data-customercol="Customer_code" id="" name="Registration_Number" value="{{$VechilManage->Registration_Number}}" placeholder="" required>
                          </div>
                         
                         
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Only Pre-Authorised</label>
                          <div class="col-sm-8">
                             <input type="checkbox" @if($VechilManage->pre_authori==1 ) checked @endif name="pre_authori" value="1"><label>Pre-Authorised</label>
                          </div>
                        </div>


                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Fuel Type</label>
                          <div class="col-sm-9">
                             <select class="form-control" name="Fuel_Type" id="Fuel_Type">
                              
                               </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">RC Valid Upto </label>
                          <div class="col-sm-9">
                             <input type="text"  @if($VechilManage->rc_valide!=null)  value="{{Carbon\Carbon::parse($VechilManage->rc_valide)->format('d/m/Y')}}" @endif class="form-control" id="datepicker"  name="rc_valide">
                          </div>
                        </div> 

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Insurance Company</label>
                          <div class="col-sm-9">
                             <select class="form-control"  name="industry_company" id="industry">
                              @foreach($IndustryDepartment as $IndustryDepartments)
                               <option value="{{$IndustryDepartments->value}}" @if($IndustryDepartments->value == $VechilManage->industry_company) selected @endif>{{$IndustryDepartments->value}}</option>
                                   @endforeach
                             
                                            </select>
                           
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Insurance due date</label>
                          <div class="col-sm-9">
                             <input type="text" @if($VechilManage->insurance_due_date!=null) value="{{Carbon\Carbon::parse($VechilManage->insurance_due_date)->format('d/m/Y')}}"  @endif  class="form-control" id="datepickerss" placeholder="insurance_due_date" name="insurance_due_date">
                          </div>
                        </div> 
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">PUC Date</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" @if($VechilManage->puc_date!=null)  value="{{Carbon\Carbon::parse($VechilManage->puc_date)->format('d/m/Y')}}"  @endif id="datepickers" placeholder="PUC Date" name="puc_date">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Make <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                         <select class="form-control make1" required="required" id="make1"  name="Make">
                              @foreach($vehicleMakeList as $row) 
                              <option value="{{$row->id}}"  @if($VechilManage->Make==$row->id) selected @endif>{{$row->name}}</option>
                               @endforeach
                             <!--   <option value="Others">Others</option> -->
                            </select>
                             <input type="text"  class="form-control other" value="{{$VechilManage->Make}}" id="other" placeholder="Make"  name="Makeother">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Model <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control Model1" required="required" id="Model1" data-id="{{$VechilManage->Model}}" name="Model">
                             <!--  @foreach($vehicleModelList as $row) 
                              <option value="{{$row->name}}" @if($VechilManage->Model==$row->name) selected @endif>{{$row->name}}</option>
                               @endforeach -->
                             <!--   <option value="other">Others</option> -->
                            </select>
                             <input type="text"  class="form-control others" value="{{$VechilManage->Model}}" id="others" placeholder="Model name"  name="modelother">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Colour</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control"  value="{{$VechilManage->van_color}}" id="" placeholder="Van Color" name="van_color">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="Capacity">Capacity <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            
                             <input type="number" readonly required="required" class="form-control" id="capacity" value="{{$VechilManage->capacity}}" placeholder="Capacity" name="capacity">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <input type="submit"  value="Update" class="btn btn-default submit-button"  > 
                          </div>
                        </div>
                      </form>
                      
                    </div>
                  </div>
    </div>
	
	
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $(".other").hide();
   
});
  $('.make1').change(function() {
    var selected = $(this).val();
    if(selected == 'Others'){
      $('.other').show();
    }
    else{
      $('.other').hide();
    }
});
  $(document).ready(function(){
    $(".others").hide();
   
});
  $('.Model1').change(function() {
    var selected = $(this).val();
    if(selected == 'other'){
      $('.others').show();
    }
    else{
      $('.others').hide();
    }
});
  $(function () {
  $("#datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
  $(function () {
  $("#datepickers").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
  $(function () {
  $("#datepickerss").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script type="text/javascript">
  jQuery(function(){
        var url1="{{url('getrocode')}}";
        var url2="{{url('getregierid')}}";
     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
               vil.getmodel();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
             jQuery('#make1').on('change',function(){
                  vil.getmodel();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
               jQuery.get(url1,{
                rocode:jQuery('#sel1').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                var d=jQuery('#customer_code').data('value');
                  jQuery.each(data, function(index,value){
                    
                    if(d==index){
                      opt+='<option selected value="'+index+'">'+value+'</option>';
                    }else{
                      opt+='<option value="'+index+'">'+value+'</option>';
                    }
                     
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);
                   vil.getcrg();
               });
           },
           getcrg:function(){

             jQuery.get(url2,{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                jQuery('#Registration_Number').val(data);
               });
           },
           getItemByItem:function(){
               
                jQuery.get('{{Route("getItemByModel")}}',{
                model:jQuery('#Model1').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                  console.log(data);
                var opt='';
                  jQuery.each(data['items'], function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });

                  jQuery('#Fuel_Type').html(opt);
                   jQuery('#capacity').val(data['capacity']);

               });

           },
            getmodel:function(){
               jQuery.get("{{url('getmodelnameupdate')}}",{
                make1:jQuery('#make1').val(),

                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                 var s=jQuery('#Model1').data('id');
                  jQuery.each(data, function(index,value){
                    if(s==index)
                     opt+='<option selected value="'+index+'">'+value+'</option>';
                   else
                    opt+='<option value="'+index+'">'+value+'</option>';

                  });
                    console.log(opt);
                   
                   jQuery('#Model1').html(opt);
                   vil.getItemByItem();
                   vil.getcrg();
               });
           },

     }
     vil.init();
  });
</script>
<script type="text/javascript">
 jQuery(function(){
          
        var url2="{{Route('NumberUnique')}}";
     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   var customercol=jQuery(this).data('customercol');
                  var customer=jQuery('#customer_code').val();
                
                   id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get(url2,{
                  table:table,
                  colum:colum,
                  customer:customer,
                  customercol:customercol,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>
</body>
</html>