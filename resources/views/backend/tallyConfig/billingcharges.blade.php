<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
  #errmsg
{
color: red;
}
</style>
<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  

  <!-- Side-Nav-->

  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Periodic Separate Billing Charges</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Periodic Separate Billing Charges</a></li>
        </ul>
      </div>
    </div>  
 
      <div class="">
         <center>@if(Session::has('success'))
                       <font style="color:red">{!!session('exitdata')!!}</font>
                    @endif</center>
      </div>

    <div class="">
              
      <div class="col-md-12 col-sm-12">
        <div class="row">
           <center>@if(Session::has('success'))
                       <font style="color:red">{!!session('success')!!}</font>
                    @endif</center>
                    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif    

             
           
   

        </div>
      </div>
    </div>
     @if($Tally_Model_check->count()>0)
      <div class="">
    
        <div class="list-heading">
           <div class="row" style="margin-top: 10px;">
              <div class="col-md-8">
                @if($Tally_Model->count()>0)
      @foreach($Tally_Model->unique('Config_name') as $Tally_Models)
            <h4 style="text-align: center;">{{$Tally_Models->Config_name}}</h4>
      @endforeach
      @endif
      </div>
      <div class="col-md-4">
      </div>
      </div>
      </div>
    </div>
    <div class="">
    
        <div class="list-heading">
          <div class="row" style="margin-top: 0px!important;">
              <div class="col-md-8">
                <h6 style="margin-left: 30%;">GARRUDA Value</h6>
              </div>
              <div class="col-md-4">
                 <h6 style="    margin-left: -53%;">Tally Value</h6>
              </div>
          </div>
        </div>
        <form class="form-horizontal"   action="{{route('updatetally.BillingCharges')}}" method="post"  onsubmit="return confirm('Do you want to Continue?');">
				<ul id="sortablePaymentMode" style="margin-top:10px;padding:0px;">
					@foreach($Tally_Model as $Tally_Models)
						<li  id="item-{{$Tally_Models->id}}" class="card" style="padding:8px 15px 8px 15px; margin-bottom:8px;list-style:none;width:70%;">
              {{ csrf_field() }}
              <div class="" style="margin-bottom: 33px;">
		            @if($Tally_Models->Config_header == 'Customer')
                <div class="col-md-2" style="margin-top: 8px;">
							{{$Tally_Models->Config_header}}
            </div>
            
            <div class="col-md-5">
              <input type="hidden" name="id[]" value="{{$Tally_Models->id}}">
              <input type="show" readonly="readonly" class="form-control" name="Garruda_name[]" value="{{$Tally_Models->Garruda_name}}">
        

            </div>
            <div class="col-md-5">
              <input type="text" class="form-control" name="Tally_name[]" value="{{$Tally_Models->Tally_name}}" placeholder="Tally Config Name">
                </div>
              @endif
              @if($Tally_Models->Config_header == 'Discount')
                <div class="col-md-2" style="margin-top: 8px;">
              {{$Tally_Models->Config_header}}
            </div>
            
            <div class="col-md-5">
              <input type="hidden" name="id[]" value="{{$Tally_Models->id}}">
              <input type="show" class="form-control"  readonly="readonly" name="Garruda_name[]" value="{{$Tally_Models->Garruda_name}}">
                </div>
            <div class="col-md-5">
              <input type="text" class="form-control" name="Tally_name[]" value="{{$Tally_Models->Tally_name}}" placeholder="Tally Config Name">

            </div>
              @endif
                 @if($Tally_Models->Config_header == 'Sales')
                <div class="col-md-2" style="margin-top: 8px;">
              {{$Tally_Models->Config_header}}
            </div>
            
            <div class="col-md-5">
              <input type="hidden" name="id[]" value="{{$Tally_Models->id}}">
              <input type="show" class="form-control" readonly="readonly" name="Garruda_name[]" value="{{$Tally_Models->Garruda_name}}">
                </div>
            <div class="col-md-5">
              <input type="text" class="form-control" name="Tally_name[]" value="{{$Tally_Models->Tally_name}}" placeholder="Tally Config Name">

            </div>
              @endif
            
                 @if($Tally_Models->Config_header == 'Taxes')
                <div class="col-md-2" style="margin-top: 8px;">
              {{$Tally_Models->Config_header}}
            </div>
            
            <div class="col-md-5">
              <input type="hidden" name="id[]" value="{{$Tally_Models->id}}">
               <input type="show" class="form-control" readonly="readonly" name="Garruda_name[]" value="{{$Tally_Models->Garruda_name}}">
                   </div>
            <div class="col-md-5">
              <input type="text" class="form-control" name="Tally_name[]" value="{{$Tally_Models->Tally_name}}" placeholder="Tally Config Name">

            </div>
              @endif

                 @if($Tally_Models->Config_header == 'Others')
                <div class="col-md-2" style="margin-top: 8px;">
              {{$Tally_Models->Config_header}}
            </div>
            
            <div class="col-md-5">
              <input type="hidden" name="id[]" value="{{$Tally_Models->id}}">
              <input type="show" class="form-control" readonly="readonly" name="Garruda_name[]" value="{{$Tally_Models->Garruda_name}}">
                 </div>
            <div class="col-md-5">
              <input type="text" class="form-control" name="Tally_name[]" value="{{$Tally_Models->Tally_name}}" placeholder="Tally Config Name">

            </div>
              @endif
            </div>

						</li>
					@endforeach	
				</ul>
        <div class="col-md-4">
        </div>
                 <div class="col-md-8">
              <input type="submit" class="btn btn-primary" value="Update">

            </div>
			</form>
  
			</div>
      @endif
		</div>
    </div>
  </div>
</div>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src="{{URL::asset('js/capitalize.js')}}"></script>

<script type="text/javascript">
jQuery(function(){
   jQuery('#type1').on('change',function(){
        var a = document.getElementById("type2");
    var b = document.getElementById("type1");

    if (a.options[a.selectedIndex].value == b.options[b.selectedIndex].value) {
        alert("Please Choose Select unit symble");
         jQuery('.site-btn').attr('disabled',true);
    } else{
       jQuery('.site-btn').attr('disabled',false);


    }
   }); 
});
</script>
<script type="text/javascript">
  $(function() {
     $('.row_dim').hide(); 
    $('#type').change(function(){
        if($('#type').val() == 'Compound') {
            $('.row_dim').show(); 
             //jQuery('#utint').css("display","none")
        } else {
            //$('#utint').show();
            jQuery('.row_dim').css("display","none")
        } 
    });
    
});
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>

<script type="text/javascript">
  
 $(document).ready(function () {
  //called when key is pressed in textbox
  $("#quantity").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }

   });
});
</script>
<script>
  jQuery(function(){
     var url="{{url('check_Pedestal_Number_unique')}}";
     var vil={
           init:function(){
             vil.selectRo();
             vil.gstCodeChk();
				
				$('#sortablePaymentMode').sortable({
					axis: 'y',
					update: function (event, ui) {
						var sorted = $(this).sortable('serialize');
						console.log(sorted);
						
						$.ajax({
							headers: {
								'X-CSRF-TOKEN': '{{ csrf_token() }}'
							},
							data: sorted,
							type: 'POST',
							url: "{{route('updatePaymentModeOrder')}}",
							dataType : 'json',
							success : function(response){
								if(response && response.success){
									console.log(response);
								}
							},
							error : function(err){
							}
						});
						
					}
				});
           },

           gstCodeChk:function(){

            jQuery('#chk_gst_code').on('click',function(){
                  var id=0;
                   var table=jQuery('#quantity').data('table');
                   var colum=jQuery('#quantity').data('colum');
                   var mess=jQuery('#quantity').data('mess');

                   // id=jQuery(this).data('id');
                    
                  udat=jQuery('#quantity').val();

                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mess);

                   }else{
                    jQuery('#form_state').submit();
                   }
                       
                   });
              });
            
           
           },

          
           
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                   var colum=jQuery(this).data('colum');
                   var mess=jQuery(this).data('mess');

                   // id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();

                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mess);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }

     vil.init();
  });
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>