@extends('backend-includes.app')

@section('content')

  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Tally Data Reset</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
         <li><a href="#"> Tally Data Reset</a></li>
       </ul>
     </div>
   </div>



   <div class="row">
     <div class="">
      
      <ul>
        @foreach($errors->all() as $error)
          <li style="color: red">{{ $error}}</li>
        @endforeach
      </ul>
     


   
        <div class="col-md-12" style="margin-top: 40px;;">
           <center  style="text-align: center;margin-top: 10px;">@if(Session::has('success'))
          <font style="color:red;">{!!session('success')!!}</font>
        @endif</center>
            <form class="form-horizontal form-inline" name="myForm" action="{{URL('tallyConfigRest')}}" enctype="multipart/form-data" method="post"  onsubmit="return confirm('Do you want to Continue?');">
  
                     {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">

                       <div class="form-group">
                        <label for="sel1">Select list:</label>
                        <select class="form-control" id="sel1" name="selected_reset_value">
                          <option value="">Select option</option>
                          <option value="GST SLIP INVOICES">GST SLIP INVOICES</option>
                          <option value="GST BILL">GST BILL</option>
                          <option value="VAT BILL">VAT BILL</option>

                          <option value="FUEL SALES">FUEL SALES</option>
                          <option value="SHIFT SETTLEMENT">SHIFT SETTLEMENT</option>
                        </select>
                      </div>


                        
                        <div class="form-group">
                          <label class="control-label"  for="from">From <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                           <input type="text"  lass="form-control" id="sync_start_date" name="fromdate" placeholder="from date" required/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                          <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                            <input  type="text"  class="form-control" id="sync_end_date" name="to_date" placeholder="To Date" required/>&nbsp;&nbsp;
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default" id="checkdate" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                        </div>

                    </div>
                </div>
            </form> 
        </div>

    </div>


</div>
@endsection
@section('script')
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 

<script type="text/javascript">

jQuery(function(){
    var enddated=new Date();
    var todate=new Date();
 
      var app={
               
              init:function(){
               
                
                app.fromDate();
                app.to_date();
             
              },
              fromDate:function(){
                
                jQuery("#sync_start_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         endDate: new Date(),
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     jQuery("#sync_end_date").val('');
                      todate=new Date();
                     enddated=jQuery(this).val();
                  
                  

                     app.to_date();
                    });
              },
              to_date:function(){
                  jQuery("#sync_end_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         startDate:enddated,
                         
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).datepicker('setStartDate',enddated);

                    /* alert(enddatedAfterDay);
                     alert(enddated);*/
              },
             
      };

      app.init();
});



        
</script>
@endsection