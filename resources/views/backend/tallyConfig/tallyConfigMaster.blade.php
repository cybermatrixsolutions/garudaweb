@extends('backend-includes.app')

@section('content')

  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Tally Config</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
         <li><a href="#">Tally</a></li>
       </ul>
     </div>
   </div>



   <div class="row">

      <div class="col-md-12" style="margin-top: 100px;">

            <div class="col-md-4">

           
                     <a href="{{route('tallyConfigMasterPersonnel')}}" class="btn btn-default" id="id-export-personel" >Export Personnel in csv</a>
         

            </div>
            <div class="col-md-4">
   
                  <a href="{{route('tallyConfigMasterInventoryMaster')}}" id="id-export-inventory-master" class="btn btn-default" >Export Inventory Master in csv</a>
           
            </div>
            <div class="col-md-4">

                  <a href="{{route('tallyConfigMasterCustomerMaster')}}" id="id-export-Customer-master" class="btn btn-default" >Export Customer Master in csv</a>
        

            </div>
      </div>
  </div>

</div>
@endsection
@section('script')

<script type="text/javascript">



         $(document).ready(function() {
    $("#id-export-inventory-master").on("click", function(){
        return confirm("Do you want to Continue? ");
    });
});
             $(document).ready(function() {
    $("#id-export-personel").on("click", function(){
        return confirm("Do you want to Continue? ");
    });
});
                 $(document).ready(function() {
    $("#id-export-Customer-master").on("click", function(){
        return confirm("Do you want to Continue? ");
    });
});





</script>
@endsection