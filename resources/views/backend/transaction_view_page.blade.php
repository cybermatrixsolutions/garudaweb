<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
<style type="text/css">
  input.form-control.abhi {
    border: 0px;
    background-color: #E5E5E5;
}

</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Transaction View</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/transaction_list')}}">Transactions Request List</a></li>
        </ul>
      </div>
    </div>
  
  
                           <div class="">  <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                             </div>
 
  <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              
               @if(isset($transaction_data) && count($transaction_data)  != 0)
              <tr>
                <th>S.No.</th>
                
                <th>Outlet Name</th>
                <th>Slip No.</th>
                <th>Vehicle Reg No</th>
                <th>Item Name</th>
                <th>Item Price</th>
                <th>Type</th>
                <th>Petrol or Diesel Type</th>
                <th>Petrol Or Diesel Qty</th>
                <th>Amount</th>
                <th>Transaction Id</th>
                <th>Request ID</th>
                <th>Transaction Date</th>
               
            </thead>


            <tbody>
              <?php $i=0;?>
               @foreach($transaction_data as $list)
               <?php $i++;?>
              <tr>
                <td>{{$i}}</td>
              
                <td>{{$list->getro->pump_legal_name}}</td>
                <!--  <td>{{$list->tax_amount}}</td> -->
                 <td>{{$list->slip_detail}} </td>
                 <td>{{$list->Vehicle_Reg_No}} </td>
                 <td>{{$list->getItemName->Item_Name}}</td>
                 <td>{{$list->item_price}}</td>
                 <td>@if($list->petrol_or_lube==1) Petrol @else Lube @endif</td>
                 <td>{{$list->petroldiesel_type}}</td>
                 <td>{{$list->petroldiesel_qty}}</td>
                 <td>{{(round($list->item_price*$list->petroldiesel_qty,2))}}</td>
                 <td>{{$list->trans_id}}</td>
                 <td>{{$list->Request_id}}</td>
                 <td>{{Carbon\Carbon::parse($list->trans_date)->format('d/m/Y')}}</td>
              
              </tr>
              @endforeach
              @else
                 <h1>no record found</h1>
              @endif
            </tbody>

          </table>
        </div>
      </div>
    </div>
  
  
  
  
  </div>
</div>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
</body>
</html>