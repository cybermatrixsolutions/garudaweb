<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Credit Limit Update</h1>
      </div>
      <div>
        <ul class="breadcrumb">

        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/credit_limit')}}"> Credit Limit</a></li>
        </ul>
      </div>
    </div>
    <div class="">
        <center>@if(Session::has('success'))
             <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
        </div>  

         @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">

                      <form class="form-horizontal"  action="{{url('credit_limit/update/'.$Credit_limit->id)}}" enctype="multipart/form-data" method="post"  onsubmit="return confirm('Do you want to Continue?');">
                          {{ csrf_field() }}
                       <!-- <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Outlet Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <select class="form-control" required="required" id="sel1" name="Ro_code" value="{{$Credit_limit->RO_code}}">
                               @foreach($data1 as $row) 
                              <option @if($row->RO_code==$Credit_limit->Ro_code) selected @endif value="{{$row->RO_code}}">{{$row->pump_legal_name}}</option>
                              
                               @endforeach
                            </select>
                          </div>
                        </div> -->
                        <input type="hidden" name="Ro_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
                       
                      
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Credit Limit (Rs.)</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control txtboxToFilter" placeholder="Credit Limit" onchange="validateFloatKeyPress();" id="creidt" name="Limit_Value" value="{{$Credit_limit->Limit_Value}}" />
                          </div>
                        </div>

                         <div class="form-group">
                          <label class="control-label col-sm-4" for="Handling Fee">Handling Fuel Fee (%) <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="text"  class="form-control txtboxToFilter" value="{{$Credit_limit->handling_fee}}" placeholder="Handling Fee" name="handlingfee" />
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-4" for="Discount">Discount Fuel  (%)</label>
                          <div class="col-sm-8">
                            <input type="text"  value="{{$Credit_limit->Discount}}" class="form-control txtboxToFilter" placeholder="Discount" name="Discount" />
                          </div>
                        </div> 
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="Handling Fee">Handling Lube Fee (%) <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control txtboxToFilter" value="{{$Credit_limit->handlinglubefee}}" placeholder="Handling Fee" name="handlinglubefee" />
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-4" for="Discount">Discount Lube  (%)</label>
                          <div class="col-sm-8">
                            <input type="text"  class="form-control txtboxToFilter" value="{{$Credit_limit->Discountlube}}" placeholder="Discount" name="Discountlube" />
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">No. of Days <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" required="required" value="{{$Credit_limit->no_of_days}}"  id="decimal"  onchange="numberofdecimal();" class="form-control" placeholder="No of Days" name="no_of_days" />
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="Handling Fee">Bill Order By <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <select class="form-control" required="required" name="billing_mode">
                                <option @if($Credit_limit->store_billing_mode=='1') selected @endif value="1">Slipwise</option>
                                <option  @if($Credit_limit->store_billing_mode=='2') selected @endif value="2">Vehiclewise</option>
                            </select>
                          </div>
                        </div>
                        
                         <div class="form-group" style="visibility: hidden;">
                          <label class="control-label col-sm-4" for="email">Customer Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <select class="form-control"  required="required" id="customer_Code" name="Customer_Code" data-value="{{$Credit_limit->Customer_Code}}">
                             
                            </select>
                          </div>
                        </div>



                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <input type="submit" value="update" id="buttontn" class="btn btn-default">
                          
                        </div>
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
 

<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">
   function validateFloatKeyPress() {
    var v = document.getElementById("creidt").value;
    if(v.length>7){
      alert("Please under 0-9999999 value");
     document.getElementById("buttontn").disabled = true;
    }
    else{
       document.getElementById("buttontn").disabled = false;
    }

 
}
function numberofdecimal() {
    var v = document.getElementById("decimal").value;
    if(v.length>2){
      alert("Please under 0-99 value");
     document.getElementById("buttontn").disabled = true;
    }
    else{
       document.getElementById("buttontn").disabled = false;
    }

 
}
  $(function () {
  $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
         format: 'dd/mm/yyyy'
  });
});
</script>

<script type="text/javascript">
  jQuery(function(){
        var url1="{{url('getrocode')}}";
        var url2="{{url('getregierids')}}";
     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
                       $(".txtboxToFilter").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_Code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
               jQuery.get(url1,{
                rocode:jQuery('#sel1').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var sla=jQuery('#customer_Code').data('value');
                var opt='';
                  jQuery.each(data, function(index,value){

                    if(sla==index)
                       opt+='<option selected value="'+index+'">'+value+'</option>';
                     else
                       opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_Code').html(opt);
                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get(url2,{
                rocode:jQuery('#customer_Code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                  var sla=jQuery('#Vehicle_Reg_no').data('value');
                 var opt='';
                  jQuery.each(data, function(index,value){

                    if(sla==index)
                       opt+='<option selected value="'+index+'">'+value+'</option>';
                     else
                       opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#Vehicle_Reg_no').html(opt);
               });
           },

     }
     vil.init();
  });
</script>



<!-- <script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#sel1').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_Code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_Code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#Vehicle_Reg_no').html(opt);
               });
           },

     }
     vil.init();
  });
</script> -->
</body>
</html>