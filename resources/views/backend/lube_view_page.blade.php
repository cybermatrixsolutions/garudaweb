<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<style type="text/css">
  span.select2.select2-container.select2-container--default.select2-container--above{
    width: 100%!important;
  }
  .select2-container {
   
    width: 100% !important;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #211d1d !important;
   
}
</style>
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Request Other Item</h1>
      </div>
      <div>
        <ul class="breadcrumb">
            <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/lube_request')}}
"> Request Other Items</a></li>
        </ul>
      </div>
    </div>
  
  
                           <div class="">  <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                             </div>
  
  
  
  
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row">
          
         
          
                      <form class="form-horizontal" >
            
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-group">


                      <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Outlet Name</label>
                          <div class="col-sm-9">
                          @if(Auth::user()->user_type==3)

                               <input type="hidden" name="RO_code" disabled="disabled" value="{{ Auth::user()->customer->RO_code }}" id="sel1">{{ Auth::user()->customer->RO_code }}

                          @else
                              <select class="form-control" disabled="disabled" name="RO_code" id="sel1">
                               
                                  @foreach($datas as $ro_master)
                                      <option name="company_id" value="{{$ro_master->RO_code}}"
                                              @if($lube_data->RO_code == $ro_master->RO_code) selected @endif >{{$ro_master->pump_legal_name}}</option>
                                  @endforeach
                              </select>
                            @endif

                          </div>
                        </div>
                         
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Vehicle Reg No </label>
                          <div class="col-sm-9">
                             <input class="form-control" disabled="disabled" value="{{$lube_data->Vehicle_Reg_No}}"  type="text" />

                             </div>
                        </div>
                          

                            <div class="form-group">
                              <label class="control-label col-sm-3" for="email">Select Item</label>
                              <div class="col-sm-9">
                                <select class="form-control  multiselect-ui" disabled="disabled" multiple="multiple" required="required" name="item_id[]" id="tags">
                                    @foreach($item_id as $item)
                                        <option value="{{$item->id}}"
                                                 @if($lube_data->item_id == $item->id) selected @endif>{{$item->Item_Name}}
                                              </option>
                                    @endforeach
                                </select>
                              </div>
                            </div>


                          <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Request Type</label>
                          <div class="col-sm-9">
                             <select class="form-control" required="required" name="Request_Type" id="sel1">                
                             <option value="Full Tank">Full Tank</option> 
                             <option value="Ltr">Ltr</option>                  
                             <option value="">Rs</option>                  
                            </select>
                          </div>
                        </div> -->
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Request Value</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" required="required" name="Request_Value" value="{{$lube_data->quantity}}" disabled="disabled">
                          </div>
                          </div>

                         @if($lube_data->getTransaction!=null && $lube_data->getTransaction()->where('item_code',$lube_data->item_id)->first()!=null && $lube_data->Execution_date!=null)
                           <?php $DeliverVal=$lube_data->getTransaction()->where('item_code',$lube_data->item_id)->first(); ?>
                           <div class="form-group">
                            <label class="control-label col-sm-3" for="email">Delivered Value</label>
                            <div class="col-sm-9">
                               <input type="text" class="form-control"  value="{{$DeliverVal->petroldiesel_qty}}" disabled="disabled">
                            </div>
                          </div>
                          @else
                            <div class="form-group">
                            <label class="control-label col-sm-3" for="email"></label>
                            <div class="col-sm-9">
                               Not Delivered
                            </div>
                          </div>
                          @endif


                           <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Request Date</label>
                          <div class="col-sm-9">
                            <input class="form-control" disabled="disabled" value="{{date('d/m/Y h:i:s a',strtotime($lube_data->request_date))}}" name="request_date"  type="text" />
                          </div>
                        </div>
                  
                           <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Execution Date</label>
                          <div class="col-sm-9">
                           
                            <input class="form-control" disabled="disabled"  @if($lube_data->Execution_date!=null) value="{{date('d/m/Y h:i:s a',strtotime($lube_data->Execution_date))}}" @endif name="request_date"  type="text" />
                            
                          </div>
                        </div>
                     <div class="form-group" style="visibility: hidden;">
                          <label class="control-label col-sm-3" for="email">Company Name</label>
                          <div class="col-sm-9">
                            @if(Auth::user()!=null && Auth::user()->user_type==4)
                          
                               <input type="hidden" name="customer_code" value="{{ Auth::user()->customer->Customer_Code }}" id="customer_code">{{ Auth::user()->customer->Customer_Name }}

                          @else
                             <select class="form-control" disabled="disabled"  name="customer_code" id="customer_code">         
                            </select>
                            @endif
                          </div>
                          </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
</div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.js"></script>

<script src="{{url::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{url::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{url::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{url::asset('js/main.js')}}"></script>


<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="{{url::asset('js/index.js')}}"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
   $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
   $('.table-responsive').css( "overflow", "auto" );
})
</script>

</script>

<script type="text/javascript">
 jQuery(function(){

  var url1 = "{{url('getrocodes')}}";
  var url2 = "{{url('getregierids')}}";

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get(url1,{
                
                rocode:jQuery('#sel1').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get(url2,{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
               });
           },

     }
     vil.init();
  });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
 $('#tags').select2({
 
    tags: true,
    tokenSeparators: [','], 
    placeholder: "Add your tags here"
});


</script
</body>
</html>