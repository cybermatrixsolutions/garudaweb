<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Garruda</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
  <style>
  body{
	font-family: 'Open Sans', sans-serif;
	font-size:13px;
  }
  
  .table td, .table th {
    padding: 4px;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
}

	p{
		margin-bottom:0px !important;
	}

  </style>
  </head>
  <body>
	
					<center><h3><b>TAX / GST Invoice</b></h3></center>
					<table class="table table-bordered">
						<tr>
							<td width="200" class="text-center logos" style="padding-top:25px;"><img src="img/Indian_Oil.png" width="120"/></td>
							<td colspan="8">
								<table class="text-center" width="100%">
									<tr>
										<td><h2 style="font-weight:500;">
											  <b>{{Auth::user()->getRocode->pump_legal_name}}</b>
										    </h2>
										</td>
										</tr>
										<tr>
										<td><p>{{Auth::user()->getRocode->pump_address}}</p></td>
										</tr>
										<tr>
										<td><p>BhikaijiCama Place</p></td>
										</tr>
										<tr>
											<td>
											  <p>
											  	{{Auth::user()->getRocode->getcity->name}} - {{Auth::user()->getRocode->pin_code}}
											  </p>
											</td>
										</tr>
										<tr>
										<td><p>GSTIN/UIN: {{Auth::user()->getRocode->GST_TIN}}, TIN No : 07102453081, PAN NO : {{Auth::user()->getRocode->PAN_No}}</p></td>
										</tr>
										<tr>
										<td>
											<?php $rostate=Auth::user()->getRocode->getstate->statecode; ?>
											<p>State Name :  {{Auth::user()->getRocode->getstate->name}}, Code : {{$rostate}}</p></td>
										</tr>
								</table>
							</td>
							<td width="200" class="text-center logos" style="padding-top:25px;">
								<img src="{{URL::asset('images')}}/logo.png" width="170"/>
							</td>
						</tr>
						<tr>
							<td colspan="8">Buyer</td>
							<td>Invoice No.</td>
							<td>Dated</td>
						</tr>
						<tr>
							
							<th colspan="8" style="font-size:16px; font-weight:700">{{$tbl_invoice->company_name}}</th>
							<th style="font-size:16px;">{{$invoice_no}}</th>
							<th style="font-size:16px;">{{date(' d-M-Y')}}</th>
						</tr>
						<tr>
							<td colspan="8">
								{{$tbl_invoice->address_one}} 

								@if(trim($tbl_invoice->address_two)!='')
								{{$tbl_invoice->address_two}} 
								@endif

								@if(trim($tbl_invoice->address_three)!='')
								{{$tbl_invoice->address_three}}
								@endif
							</td>
							<td>Mode/Terms of Payment</td>
							<td><strong>Immediate</strong></td>
						</tr>
						<tr>
							<td colspan="8">
								{{$tbl_invoice->getcity->name}} - {{$tbl_invoice->pin_no}}
							</td>

							<td></td>
							<td></td>
						</tr>
						<tr>                         
							<td colspan="4">Contact : @if(trim($tbl_invoice->gender)=='Female') Ms @else Mr @endif . {{$tbl_invoice->Customer_Name}}</td>
							<td colspan="4">Ph : {{$tbl_invoice->Phone_Number}}</td>
							<td>Billing Period - From</td>
							<td>Billing Period - To</td>
						</tr>
						<tr>
							<?php $cusstate=$tbl_invoice->getstate->statecode;?>
							<td colspan="4">State Name : {{$tbl_invoice->getstate->name}} Code : {{$cusstate}}</td>
							<td colspan="4">PAN No. : {{$tbl_invoice->pan_no}}</td>
							<td style="font-size:16px;"><strong>15-09-2017</strong></td>
							<td style="font-size:16px;"><strong>30-09-2017</strong></td>
						</tr>
						<tr>
							<td colspan="11">
								<table class="table table-bordered">
									<tr>
							<th width="1%;">S.No.</th>
							<th>Slip Date</th>
							<th>Slip No.</th>
							<th>Vehicle No.</th>
							<th>Product Sold</th>
							<th>HSN / SAC Code</th>
							<th>VAT / GST Rate</th>
							<th>Qty Sold</th>
							<th>Rate</th>
							<th>Amount</th>
						</tr>
						<tr>
							<td width="1%;" align="right">1</td>
							<td>9/15/2017</td>
							<td>00001</td>
							<td>DL-1CB-1000</td>
							<td>Petrol Normal</td>
							<td></td>
							<td align="right">27%</td>
							<td align="right">25</td>
							<td align="right">74</td>
							<td align="right">1850</td>
						</tr>
						<tr>
							<td width="1%;" align="right">2</td>
							<td>9/15/2017</td>
							<td>00002</td>
							<td>HR-26BR-7722</td>
							<td>Diesel Normal</td>
							<td></td>
							<td align="right">16.75%</td>
							<td align="right">24</td>
							<td align="right">60</td>
							<td align="right">1440</td>
						</tr>
						<tr>
							<td width="1%;" align="right">3</td>
							<td>9/16/2017</td>
							<td>00003</td>
							<td>DL-3CQ-0639</td>
							<td>Diesel Normal</td>
							<td></td>
							<td align="right">16.75%</td>
							<td align="right">14</td>
							<td align="right">61</td>
							<td align="right">854</td>
						</tr>
						<tr>
							<td width="1%;" align="right">4</td>
							<td>9/15/2017</td>
							<td>00002</td>
							<td>HR-26BR-7722</td>
							<td>Diesel Normal</td>
							<td></td>
							<td align="right">16.75%</td>
							<td align="right">24</td>
							<td align="right">60</td>
							<td align="right">1440</td>
						</tr>
						<tr>
							<td width="1%;" align="right">5</td>
							<td>9/15/2017</td>
							<td>00002</td>
							<td>HR-26BR-7722</td>
							<td>Diesel Normal</td>
							<td></td>
							<td align="right">16.75%</td>
							<td align="right">24</td>
							<td align="right">60</td>
							<td align="right">1440</td>
						</tr>
						<tr>
							<td width="1%;" align="right">6</td>
							<td>9/15/2017</td>
							<td>00002</td>
							<td>HR-26BR-7722</td>
							<td>Diesel Normal</td>
							<td></td>
							<td align="right">16.75%</td>
							<td align="right">24</td>
							<td align="right">60</td>
							<td align="right">1440</td>
						</tr>
						<tr>
							<td width="1%;" align="right">7</td>
							<td>9/15/2017</td>
							<td>00002</td>
							<td>HR-26BR-7722</td>
							<td>Diesel Normal</td>
							<td></td>
							<td align="right">16.75%</td>
							<td align="right">24</td>
							<td align="right">60</td>
							<td align="right">1440</td>
						</tr>
						<tr>
							<td width="1%;" align="right">8</td>
							<td>9/15/2017</td>
							<td>00002</td>
							<td>HR-26BR-7722</td>
							<td>Diesel Normal</td>
							<td></td>
							<td align="right">16.75%</td>
							<td align="right">24</td>
							<td align="right">60</td>
							<td align="right">1440</td>
						</tr>
						<tr>
							<td width="1%;" align="right">9</td>
							<td>9/15/2017</td>
							<td>00002</td>
							<td>HR-26BR-7722</td>
							<td>Diesel Normal</td>
							<td></td>
							<td align="right">16.75%</td>
							<td align="right">24</td>
							<td align="right">60</td>
							<td align="right">1440</td>
						</tr>
						<tr>
							<td width="1%;" align="right">10</td>
							<td>9/15/2017</td>
							<td>00002</td>
							<td>HR-26BR-7722</td>
							<td>Diesel Normal</td>
							<td></td>
							<td align="right">16.75%</td>
							<td align="right">24</td>
							<td align="right">60</td>
							<td align="right">1440</td>
						</tr>
						<tr>
							<td width="1%;" align="right">11</td>
							<td>9/15/2017</td>
							<td>00002</td>
							<td>HR-26BR-7722</td>
							<td>Diesel Normal</td>
							<td></td>
							<td align="right">16.75%</td>
							<td align="right">24</td>
							<td align="right">60</td>
							<td align="right">1440</td>
						</tr>
						<tfoot>
							<tr>
								<td colspan="7" align="right" style="font-size:18px;"><strong><i>Total Incl. Taxes</i></strong></td>
								<td align="right">670</td>
								<td align="right"></td>
								<td align="right" style="font-size:18px;">46387.50</td>
							</tr>
							<tr>
								<td width="1%"></td>
								<td>Less :</td>
								<td colspan="3" align="right"><strong>Discount</strong></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td width="1%"></td>
								<td>Add :</td>
								<td colspan="3" align="right"><strong>Surcharge / Billing Charges</strong></td>
								<td align="right"><strong>6789</strong></td>
								<td align="right"><strong>2.50%</strong></td>
								<td></td>
								<td></td>
								<td align="right">1159.69</td>
							</tr>
							<tr>
								<td width="1%"></td>
								<td></td>
								<td colspan="3" align="right"><strong>CGST</strong></td>
								<td align="right"><strong></strong></td>
								<td align="right"><strong>9%</strong></td>
								<td></td>
								<td></td>
								<td align="right">104.37</td>
							</tr>
							<tr>
								<td width="1%"></td>
								<td></td>
								<td colspan="3" align="right"><strong>SGST</strong></td>
								<td align="right"><strong></strong></td>
								<td align="right"><strong>9%</strong></td>
								<td></td>
								<td></td>
								<td align="right">104.37</td>
							</tr>
							<tr>
								<td width="1%"></td>
								<td></td>
								<td colspan="3" align="right"><strong>Rounded Off</strong></td>
								<td align="right"><strong></strong></td>
								<td align="right"><strong></strong></td>
								<td></td>
								<td></td>
								<td align="right">0.07</td>
							</tr>
							<tr>
								<td colspan="7" align="right" style="font-size:18px;"><strong>GRAND TOTAL</strong></td>
								<td align="right"></td>
								<td align="right"></td>
								<td align="right" style="font-size:18px;"><strong>47756.00</strong></td>
							</tr>
							<tr>
								<td colspan="9">Amount Chargeable (in words)</td>
								<td align="right"><i>E. & O.E</i></td>
							</tr>
						</tfoot>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="11">
								<table class="table table-bordered">
									<tr>
										<th colspan="9">INR Forty Seven Thousand Seven Hundred Fifty Six Only</th>
										<th></th>
									</tr>
									<tr>
										<th>Product Summary</th>
										<th>Volume</th>
										<th>Taxable  Amt</th>
										<th>VAT %</th>
										<th>VAT</th>
										<th>SGST</th>
										<th>CGST</th>
										<th>IGST</th>
										<th>Cess</th>
										<th>Total Amount</th>
									</tr>
									<tr>
										<td>Petrol Normal</td>
										<td align="right">306</td>
										<td align="right">18152.17</td>
										<td align="right">27%</td>
										<td align="right">4901.09</td>
										<td align="right"></td>
										<td align="right"></td>
										<td align="right"></td>
										<td align="right"></td>
										<td align="right">23053.26</td>
									</tr>
									<tr>
										<td>Petrol Speed</td>
										<td align="right">42</td>
										<td align="right">2590.55</td>
										<td align="right">27%</td>
										<td align="right">699.45</td>
										<td align="right"></td>
										<td align="right"></td>
										<td align="right"></td>
										<td align="right"></td>
										<td align="right">3290.00</td>
									</tr>
									<tr>
										<td>Diesel Normal</td>
										<td align="right">236</td>
										<td align="right">12428.48</td>
										<td align="right">16.75%</td>
										<td align="right">2081.77</td>
										<td align="right"></td>
										<td align="right"></td>
										<td align="right"></td>
										<td align="right"></td>
										<td align="right">14510.25</td>
									</tr>
									<tr>
										<td>Diesel Premium</td>
										<td align="right">86</td>
										<td align="right">4740.04</td>
										<td align="right">16.75%</td>
										<td align="right">793.96</td>
										<td align="right"></td>
										<td align="right"></td>
										<td align="right"></td>
										<td align="right"></td>
										<td align="right">5534.00</td>
									</tr>
									<tr>
										<td align="right"><strong>Total</strong></td>
										<td align="right"><strong>670.000</strong></td>
										<td align="right"><strong>37911.24</strong></td>
										<td align="right"></td>
										<td align="right"><strong>8476.26</strong></td>
										<td align="right"><strong>0.00</strong></td>
										<td align="right"><strong>0.00</strong></td>
										<td align="right"><strong>0.00</strong></td>
										<td align="right"><strong>0.00</strong></td>
										<td align="right"><strong>46387.50</strong></td>
									</tr>
								</table>
							</td>
						</tr>
						
						<tr>
							<td colspan="11">
								<table class="table table-bordered">
									<tr>
										<th colspan="10">&nbsp;</th>
									</tr>
									<tr>
										<th>Name</th>
										<th>HSC / SAC</th>
										<th>Taxable  Amt</th>
										<th>GST %</th>
										<th>VAT</th>
										<th>SGST</th>
										<th>CGST</th>
										<th>IGST</th>
										<th>Cess</th>
										<th>Total Amount</th>
									</tr>
									<tr>
										<td>Surcharge / Billing Charges</td>
										<td align="right">67891234</td>
										<td align="right">1159.69</td>
										<td align="right">18%</td>
										<td align="right"></td>
										<td align="right">104.37</td>
										<td align="right">104.37</td>
										<td align="right"></td>
										<td align="right"></td>
										<td align="right">1368.43</td>
									</tr>
									<tr>
										<td align="right"><strong>Total</strong></td>
										<td align="right"><strong></strong></td>
										<td align="right"><strong>1159.69</strong></td>
										<td align="right"></td>
										<td align="right"><strong>0.00</strong></td>
										<td align="right"><strong>104.37</strong></td>
										<td align="right"><strong>104.37</strong></td>
										<td align="right"><strong>0.00</strong></td>
										<td align="right"><strong>0.00</strong></td>
										<td align="right"><strong>1368.43</strong></td>
									</tr>
									<tr>
										<td align="right" colspan="2"><strong>GRAND TOTAL</strong></td>
										<td align="right"><strong>39070.93</strong></td>
										<td align="right"><strong></strong></td>
										<td align="right"><strong>8476.26</strong></td>
										<td align="right"><strong>104.37</strong></td>
										<td align="right"><strong>104.37</strong></td>
										<td align="right"><strong>0.00</strong></td>
										<td align="right"><strong>0.00</strong></td>
										<td align="right"><strong>47755.94</strong></td>
									</tr>
									<tr>
										<td colspan="10"><strong>Our Bank Details : A/c Name : Petrol Pump @ Bhikaiji Cama Place, A/c No : 123456789012345 Bank : HDFC Bank Ltd, IFSC Code : HDFC 123456789</strong></td>
									</tr>
									<tr>
										<td colspan="10"><strong>Terms & Conditions : 1. In case is not made within the due date, interest @18% shall be charged. 2. All disputes subject to Delhi Jurisdiction</strong></td>
									</tr>
									<tr>
										<td colspan="10"><strong>'3. Complaint, if any, regarding this invoice must be raised within 7 days from the date of the bill.</strong></td>
									</tr>
									<tr>
										<td colspan="9"><strong>Customer's Seal and Signature</strong></td>
										<td><strong>for Petrol Pump @ Bhikaiji Cama Place</strong></td>
									</tr>
									<tr>
										<td colspan="9"><strong></strong></td>
										<td><strong>Authorised Signatory</strong></td>
									</tr>
									<tr>
										<td colspan="10" class="text-center"><strong>This is a Computer Generated Invoice</strong></td>
									</tr>
								</table>
							</td>
						</tr>
						
					</table>
			
  </body>
</html>