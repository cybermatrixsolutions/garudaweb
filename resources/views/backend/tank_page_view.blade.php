<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>

</head>
<script>
function validateForm() {

    var x = document.forms["myForm"]["Tank_Number"].value;
    var Capacity = document.forms["myForm"]["Capacity"].value;

    var opening_date = document.forms["myForm"]["Opening_Reading"].value;
    var active_from_date = document.forms["myForm"]["active_from_date"].value;
    if (x == "") {
        alert("Please Enter  Tank_Number !!");
        return false;
    }
    if (Capacity == "") {
        alert("Please Enter Capacity !!");
        return false;
    }
    if (opening_date == "") {
        alert("Please Enter Opening Reading !!");
        return false;
    }
    if (active_from_date == "") {
        alert("Please Enter Active From Date !!");
        return false;
    }
}
</script>
<body class="sidebar-mini fixed">

<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Tank  View
</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="{{url('/tank')}}
">Tank 
 </a></li>
        </ul>
      </div>
    </div><div class="">
      
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
      
    </div>
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row" style="margin-top:-15px !important;">
          
         
          
                      <form name="myForm" class="form-horizontal" action="{{url('tank_edit_data')}}/{{$tank_mangement_data->id}}" enctype="multipart/form-data" method="post" onsubmit="return validateForm()">
            
                <input type="hidden" name="_token" value="{{ csrf_token()}}"/>
              
                <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Pump Legal Name</label>
                          <div class="col-sm-9">
                            <select class="form-control"  disabled="disabled" name="RO_code" id="sel1">
                             
                                                    @foreach($datas as $ro_master)
                                                        <option name="company_id" value="{{$ro_master->RO_code}}"
                                                                 @if($tank_mangement_data->RO_code == $ro_master->RO_code) selected @endif>{{$ro_master->pump_legal_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div> -->
            
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Tank Number</label>
                          <div class="col-sm-9">
                            <input type="text" required="required" disabled="disabled" class="form-control" name="Tank_Number" id="" value="{{$tank_mangement_data->Tank_Number}}" placeholder="Tank Number">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Capacity</label>
                          <div class="col-sm-9">
                            <input type="text" required="required" disabled="disabled" class="form-control" name="Capacity" id="" value="{{$tank_mangement_data->Capacity}}" placeholder="Capacity">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Fuel Type</label>
                          <div class="col-sm-9">
                             <select class="form-control" disabled="disabled" id="Pedestal_id" name="fuel_type">
                            @foreach($fuel_type as $fuel) 
                              <option value="{{$fuel->id}}" @if($tank_mangement_data->fuel_type == $fuel->id) selected @endif >{{$fuel->Item_Name}}</option>
                              
                               @endforeach
                             
                            </select>
                            
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Unit of Measure</label>
                          <div class="col-sm-9">
                                <select class="form-control" disabled="disabled" required="required" name="Unit_of_Measure" id="Unit_of_Measure">
                             
                                                    @foreach($unit_measure as $unit)
                                                        <option  value="{{$unit->id}}"
                                                                @if($tank_mangement_data->Unit_of_Measure==$unit->id) selected @endif  >{{$unit->Unit_Symbol}}-{{$unit->Unit_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Opening Stock</label>
                          <div class="col-sm-9">
                            <input type="text" disabled="disabled" required="required" class="form-control" name="opening_reading" id="" value="{{$tank_mangement_data->Opening_Reading}}" placeholder="Opening Reading">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Opening Date</label>
                          <div class="col-sm-9">
                            <input class="form-control datepicker" disabled="disabled" name="opening_date"value="{{Carbon\Carbon::parse($tank_mangement_data->Opening_Date)->format('d/m/Y')}}" type="text" />
                          </div>
                        </div>

                          <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Remarks</label>
                          <div class="col-sm-9">
                            <input type="text"  class="form-control" disabled="disabled" name="remarks" value="{{$tank_mangement_data->Remarks}}" id="" placeholder="Email">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Active From Date</label>
                          <div class="col-sm-9">
                            <input type="text" disabled="disabled" class="form-control datepicker" name="active_from_date" value="{{Carbon\Carbon::parse($tank_mangement_data->Active_From_Date)->format('d/m/Y')}}" placeholder="Active From Date">
                          </div>
                        </div>
                        <div class="form-group">
            
                          <div class="col-sm-offset-3 col-sm-9">
                           <!-- <button type="submit" class="btn btn-default">Submit</button>-->
              <center><a href="{{url('tank')}}" class="btn btn-primary">Back</a></center>
                          </div>
                        </div>
           
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>
	
	
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>

</body>
</html>