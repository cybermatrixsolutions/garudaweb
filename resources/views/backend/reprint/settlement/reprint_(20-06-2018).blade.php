
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSS-->
<!-- Font-icon css-->
<style type="text/css"  media="print">
  

    @page  
    {
         size: A5;
    
  }

</style>
<style type="text/css" media="print">
@page  {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>
<style type="text/css">
@media  print {
    #myPrntbtn {
        display :  none;
    }
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
   
   padding: 0px;
   margin: 0px;
    vertical-align: top;
    border-top: transparent;
}

  .table{
     border-collapse: collapse;
   font-family: arial, sans-serif;

    width: 148.5mm;
    margin: 0 auto;
    margin-top: 60px;
    position: relative;
    border:1px solid #ccc !important;
    border-bottom:1px solid #ccc !important;
  }



hr{
  border-top: 1px solid #1d0d0d;
  width: 100%;
}
</style>
</head>
  <body class="table">
    <?php $str='';
                     $amount=0;
                     $netamount=0;
                     $testamount=0;
                     ?>
        <table width="100%">
          @foreach($shift->getPadestal as $padestal)
            <tr>
               <th colspan="6" align="left" style="text-align: left;padding-top: 25px;padding-bottom: 10px;padding-left: 10px;"><p>Padestal No. : {{$padestal->Pedestal_Number}}</p></th>
        
            </tr>
      <tr><td colspan="6"><hr></td></tr>
      <tr>
               <th style="width:16%;padding-bottom: 10px;padding-top: 10px;">Nozzle No.</th> 
               <th style="width:16%;padding-bottom: 10px;padding-top: 10px;">Fuel Type</th>
               <th style="width:18%;padding-bottom: 10px;padding-top: 10px;">OMR</th>
               <th style="width:18%;padding-bottom: 10px;padding-top: 10px;">CMR</th>
               <th style="width:18%;padding-bottom: 10px;padding-top: 10px;">Test Volume</th>
               <th style="width:18%;padding-bottom: 10px;padding-top: 10px;">Net Sales QTY</th>
            </tr>
      <tr><td colspan="6"><hr></td></tr>
      @foreach($padestal->getNozzle as $getNozzle)
      <?php $nore=$getNozzle->getReading()->where('shift_id',$shift->id)->first(); ?>
      <tr class="parentcmr">
                <td style="text-align:center;padding-bottom: 10px;padding-top: 10px;">{{$getNozzle->Nozzle_Number}}</td> 
                <td style="text-align:center;padding-bottom: 10px;padding-top: 10px;">{{$getNozzle->getItem->Item_Name}}</td>
                <td style="text-align:center;padding-bottom: 10px;padding-top: 10px;">{{number_format((float)$nore['Nozzle_Start'], 3, '.', '')}}</td>
                <td style="text-align:center;padding-bottom: 10px;padding-top: 10px;">{{number_format((float)$nore['Nozzle_End'], 3, '.', '')}}</td>
                <?php $netamount=($nore['reading']*$getNozzle->getItem->price->price)+$netamount;
                        $testamount=($nore['reading']*$getNozzle->getItem->price->price)+$testamount;
                  ?>
                <td style="text-align:center;padding-bottom: 10px;padding-top: 10px;"> {{number_format((float)$nore['test'], 3, '.', '')}} </td>
                <?php $sb=$nore['Nozzle_End']-$nore['Nozzle_Start'];?>
                <?php
                        $chetqty1=number_format((float)$nore['test'], 3, '.', ''); 
            
                         $chetqty=number_format((float)$sb-$chetqty1, 3, '.', ''); 
                  ?>
                <td style="text-align:center;padding-bottom: 10px;padding-top: 10px;">{{$chetqty}}</td>
            </tr>
          @endforeach

        @endforeach
    
      <tr><td colspan="6"><hr></td></tr>
        </table>
        <h4>FUEL TYPEWISE NET SALES</h4>
    <table width="100%">
      <tbody>
        
      <tr><td colspan="4"><hr></td></tr>
      <tr>
               <th style="width:16%;padding-bottom: 10px;padding-top: 10px;">Fuel Type</th>
               <th style="width:18%;padding-bottom: 10px;padding-top: 10px;">Net Sales QTY</th>
               <th style="width:18%;padding-bottom: 10px;padding-top: 10px;">Rate</th>
               <th style="width:18%;padding-bottom: 10px;padding-top: 10px;">Total</th>
            </tr>
      <tr><td colspan="4"><hr></td></tr>  
      <?php $str.=','.$padestal->Pedestal_Number; 


          ?>
          

          <?php 
          $shift_id=$shift->id;
          $shift_manager=$shift->shift_manager;

          $variable = DB::table('tbl_ro_nozzle_reading')
          ->join('tbl_pedestal_nozzle_master', TRIM('tbl_ro_nozzle_reading.Nozzle_No') , TRIM('tbl_pedestal_nozzle_master.Nozzle_Number'))
          ->join('tbl_price_list_master', 'tbl_price_list_master.id', 'tbl_pedestal_nozzle_master.fuel_type')
          ->join('tbl_item_price_list', 'tbl_price_list_master.id', 'tbl_item_price_list.item_id')
          ->join('shift_pedestal', 'shift_pedestal.pedestal_id', 'tbl_pedestal_nozzle_master.Pedestal_id')
          ->join('shifts', 'shifts.id', 'shift_pedestal.shift_id')
          ->select('tbl_ro_nozzle_reading.Nozzle_No', DB::raw('(tbl_ro_nozzle_reading.Nozzle_End - tbl_ro_nozzle_reading.Nozzle_Start ) - tbl_ro_nozzle_reading.test as qty'),'tbl_ro_nozzle_reading.reading','tbl_ro_nozzle_reading.price','tbl_ro_nozzle_reading.Nozzle_End','tbl_price_list_master.item_name')

            // ->where('tbl_ro_nozzle_reading.Reading_by',$shift_manager)
          ->where('tbl_item_price_list.is_active',1)
          ->where('tbl_price_list_master.is_active',1)
          ->where('tbl_pedestal_nozzle_master.is_active',1)
          ->where('shifts.id',$shift_id)
          ->where('shifts.fuel',1)
          ->where('shifts.shift_manager',$shift_manager)
          ->where('tbl_ro_nozzle_reading.shift_id',$shift_id)
          ->whereNotNull('tbl_ro_nozzle_reading.Nozzle_End')
          ->get();

            // dd($variable, $shift_id,$shift_manager);
          $Item_Name='';
          $totalprices=0;
          $item_name2 = array();
          ?>
          @foreach($variable as $value)
          <?php   
          $item_name[] = $value->item_name;
          $item_name2 =  array_unique($item_name);

          ?>
          @endforeach  
          @foreach($item_name2 as $item_name_new)
          <?php
          $chetqty_previous=0;

          ?>
          @foreach($variable as $variabless)
          <?php


          if ($item_name_new == $variabless->item_name) {
            $Item_Names = $variabless->item_name;
            $chetqty_previous = ($variabless->qty)+($chetqty_previous);

            $chetqty_previous=number_format((float)$chetqty_previous, 3, '.', '');

            $price = $variabless->price;
          }

          ?>
          @endforeach  
          <tr class="parentcmr">
                <td style="text-align:center;padding-bottom: 10px;padding-top: 10px;">{{$Item_Names}}</td> 
                <td style="text-align:center;padding-bottom: 10px;padding-top: 10px;">{{$chetqty_previous}}</td> 
                <td style="text-align:center;padding-bottom: 10px;padding-top: 10px;">{{$price}}</td> 
                <?php  
                    $totalprice = ($price)*($chetqty_previous);
                ?>
                <td style="text-align:center;padding-bottom: 10px;padding-top: 10px;">{{number_format((float)$totalprice, 2, '.', '')}}</td> 
          </tr>      
                    <?php 
            $totalprices = $totalprice+$totalprices;
          ?>
          @endforeach          
      </tbody>
    </table>       
        <table width="100%">
          <tbody>
                     <?php 


         $shift_id = $shift->id;


         $var = DB::table('tbl_customer_transaction')
         ->join('tbl_price_list_master', 'tbl_customer_transaction.item_code', 'tbl_price_list_master.id')
         ->join('tbl_stock_item_group', 'tbl_stock_item_group.id', 'tbl_price_list_master.Stock_Group')
         ->select(DB::raw('sum(tbl_customer_transaction.petroldiesel_qty*tbl_customer_transaction.item_price) AS total_sales'))
         ->where('tbl_stock_item_group.Group_Name','LUBES')
         ->where('tbl_customer_transaction.shift_id',$shift_id)
         ->where('tbl_customer_transaction.petrol_or_lube',2)
         ->get();

         $vara = DB::table('tbl_customer_transaction')
         ->join('tbl_price_list_master', 'tbl_customer_transaction.item_code', 'tbl_price_list_master.id')
         ->join('tbl_stock_item_group', 'tbl_stock_item_group.id', 'tbl_price_list_master.Stock_Group')
         ->select(DB::raw('sum(tbl_customer_transaction.petroldiesel_qty*tbl_customer_transaction.item_price) AS total_sales'))
         ->where('tbl_stock_item_group.Group_Name','OTHERS')
         ->where('tbl_customer_transaction.shift_id',$shift_id)
         ->where('tbl_customer_transaction.petrol_or_lube',2)
         ->get();

         ?>
         @foreach($var as $vars)

                <tr >
                  <td align="right"  colspan="5"></td>
                  <td align="right" style="padding-top: 10px;padding-bottom: 10px;">
                    <span class="paymen_mode"></span>
                    <span class="paymen_ref_no" style="padding-right:30px;font-weight:bold;">Total Lubes Sales:  </span>
                    <span class="paymen_amount" style="padding-right:30px;">{{number_format((float)$vars->total_sales, 2, '.', '')}}</span>
                  </td>
                </tr>
                   @endforeach
           @foreach($vara as $vars1)
        
                <tr >
                  <td align="right"  colspan="5"></td>
                  <td align="right" style="padding-top: 10px;padding-bottom: 10px;">
                    <span class="paymen_mode"></span>
                    <span class="paymen_ref_no" style="padding-right:30px;font-weight:bold;">Total Other Sales:  </span>
                    <span class="paymen_amount" style="padding-right:30px;">{{number_format((float)$vars1->total_sales, 2, '.', '')}}</span>
                  </td>
                </tr>
                   @endforeach
          </tbody>
        </table> 
    <table width="100%">
      <tbody>
              <?php $amount=0; ?>
     
      @if($shift->settelment!=null && $shift->settelment->getPayments!=null)

        <tr >
          <td align="right"  colspan="5"></td>
          <td align="right" style="padding-top: 10px;padding-bottom: 10px;">
            <span class="paymen_mode"></span>
            <span class="paymen_ref_no" style="padding-right:30px;font-weight:bold;">Total Sales: </span>
            <span class="paymen_amount" style="padding-right:30px;">{{number_format((float)$shift->settelment->Nozzle_Amount, 2, '.', '')}}</span>
          </td>
        </tr>
           <tr >
            <td  colspan="5"></td>
            <td align="right" style="padding-top: 20px;padding-bottom: 10px;" >
        
        
                <span class="paymen_mode" style="padding-left: 0px;padding-right: 4px;font-weight: bold;border-right: 1px solid black;">Payment mode</span>
                <span class="paymen_ref_no" style="padding-left: 5px;padding-right: 5px;font-weight: bold;border-right: 1px solid black;">Ref No.</span>
                <span class="paymen_mode" style="padding-left: 2px;padding-right: 5px;font-weight: bold;border-right: 1px solid black;">Garruda Receipt</span>
                <span class="paymen_mode" style="padding-left: 10px;padding-right: 15px;font-weight: bold;">Manual Receipt | Amount</span>
              <!--   <span class="paymen_amount" style="padding-left: 10px;padding-right: 15px;font-weight: bold;">Amount</span> -->
            </td>
          </tr>


                <tr >
                      <td align="" colspan="5"></td>
                      <td align="left" style="padding-top: 10px;padding-bottom: 10px;">
                          <span class="paymen_mode" style="width: 120px;display: inline-block;text-align:left;border-right:1px solid;">Credit Sales</span>
                          <span class="paymen_ref_no" style="width: 68px;display: inline-block;text-align:right;border-right:1px solid; box-sizing: border-box;padding-right: 10px;"></span>
                          <span class="paymen_ref_no" style="width: 130px;display: inline-block;text-align:right;border-right:1px solid;box-sizing: border-box;padding-right: 10px;">0.00</span>
                          <span class="paymen_ref_no" style="width: 136px;display: inline-block;text-align:right;border-right:1px solid; box-sizing: border-box;padding-right: 10px;">0.00</span>                                
                          <span class="paymen_amount" style="width: 65px;display: inline-block;text-align:right;margin-right:10px;box-sizing: border-box;">{{number_format((float)$shift->settelment->Credit_fuel, 2, '.', '')}}</span>
                        </td>
                </tr>


        @foreach($shift->settelment->getPayments as $Payment)
          <?php $amount=$Payment->pivot->amount+$amount;?>
      <tr >
            <td align="" colspan="5"></td>
            <td align="left" style="padding-top: 10px;padding-bottom: 10px;">
                <span class="paymen_mode"  style="width: 120px;display: inline-block;text-align:left;border-right:1px solid;">{{$Payment->name}}</span>
                <span class="paymen_ref_no" style="width: 68px;display: inline-block;text-align:right;border-right:1px solid; box-sizing: border-box;padding-right: 10px;">{{$Payment->pivot->ref_no}}</span>
                <span class="paymen_ref_no" style="width: 130px;display: inline-block;text-align:right;border-right:1px solid;box-sizing: border-box;padding-right: 10px;">0.00</span>
                <span class="paymen_ref_no" style="width: 136px;display: inline-block;text-align:right;border-right:1px solid; box-sizing: border-box;padding-right: 10px;">{{number_format((float)$Payment->pivot->amount, 2, '.', '')}}</span>                                
                <span class="paymen_amount" style="width: 65px;display: inline-block;text-align:right;margin-right:10px;box-sizing: border-box;">{{number_format((float)$Payment->pivot->amount, 2, '.', '')}}</span>
              </td>
      </tr>
      @endforeach
    
            <tr >
            <td  colspan="5" style="padding-top: 10px;padding-bottom: 10px;" ></td>
            <td align="right" style="padding-top: 25px;padding-bottom: 10px;"  >
                <span class="paymen_mode"></span>
                <span class="paymen_ref_no" style="padding-right:30px;font-weight:bold;">Total Amount: </span>
                <span class="paymen_amount" style="padding-right:12px;font-weight:bold;">{{number_format((float)$amount, 2, '.', '')}}</span>
            </td>
          </tr>

           <tr >
            <td  colspan="5" style="padding-top: 10px;padding-bottom: 10px;" ></td>
            <td align="right" style="padding-top: 10px;padding-bottom: 10px;"  >
                <span class="paymen_mode"></span>
                <span class="paymen_ref_no" style="padding-right:30px;font-weight:bold;">Short/Excess: </span>
                <span class="paymen_amount" style="padding-right:12px;font-weight:bold;">{{number_format((float)$shift->settelment->Diff, 2, '.', '')}}</span>
            </td>
          </tr>

              @endif
      
    </tbody>

    </table>
                 @if($shift->getTanksReading!=null && $shift->getTanksReading->count()>0)       
       <table width="100%" align="" >
        <tr> 
            
       <th colspan="6" align="left" style="text-align:left;padding-top: 10px;padding-bottom: 10px;padding-left: 10px;"><p>Dip Readings</p></th> 
        </tr>
    <tr><td colspan="6"><hr style="border-top: 1px solid #1d0d0d;width:100%;"></td></tr>
        <tr> 
            <th style="text-align: center;padding-top: 10px;padding-bottom: 10px;">Tank Name </th>
            <th style="text-align: center;padding-top: 10px;padding-bottom: 10px;">Fuel Type </th>
            <th style="text-align: center;padding-top: 10px;padding-bottom: 10px;">Unit of Measure </th>
            <th style="text-align: center;padding-top: 10px;padding-bottom: 10px;">Reading</th>
          
        </tr>
    <tr><td colspan="6"><hr style="border-top: 1px solid #1d0d0d;width:100%;"></td></tr>
               @foreach($shift->getTanksReading as $getTanksReading)
    <tr> 
      <td style="text-align: center;padding-top: 10px;padding-bottom: 10px;">{{$getTanksReading->getTank->Tank_Number}} </td>
            <td style="text-align: center;padding-top: 10px;padding-bottom: 10px;">UNLEADED DIESEL - IOCL </td>
            <td style="text-align: center;padding-top: 10px;padding-bottom: 10px;">{{$getTanksReading->getfueltypes->Item_Name}} </td>
            <td style="text-align: center;padding-top: 10px;padding-bottom: 10px;">{{$getTanksReading->getunitmesure->Unit_Symbol}}</td>
            <td style="text-align: center;padding-top: 10px;padding-bottom: 10px;">{{$getTanksReading->value}}</td>
        </tr>
        @endforeach
  
    </table>
    @endif
      <br>
  
  <center>
    <input type="button"  id="myPrntbtn" class="btn btn-success site-btn" onclick="window.print();" value="Print" / style="padding: 8px;padding-left: 20px;padding-right: 20px;">
  </center> 


  <script type="text/javascript">
          function PrintDiv() {

              var contents = document.getElementById("qrcodeprintContents").innerHTML;

              var frame1 = document.createElement('iframe');
              frame1.name = "frame1";
              frame1.style.position = "absolute";
              frame1.style.top = "-1000000px";
              document.body.appendChild(frame1);
              var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
              frameDoc.document.open();
              frameDoc.document.write('<html><head><title>QR Print</title>');
              frameDoc.document.write('</head><body>');
              frameDoc.document.write(contents);
              frameDoc.document.write('</body></html>');
              frameDoc.document.close();
              setTimeout(function () {
                  window.frames["frame1"].focus();
                  window.frames["frame1"].print();
                  document.body.removeChild(frame1);
              }, 500);
              return false;
          }
      </script>

  </body>
</html>