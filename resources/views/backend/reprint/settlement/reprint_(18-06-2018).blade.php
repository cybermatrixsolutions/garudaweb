<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSS-->
<!-- Font-icon css-->
<style type="text/css"  media="print">
  

    @page 
    {
         size: A7;
 
}

</style>
<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>
<style type="text/css">
@media print {
    #myPrntbtn {
        display :  none;
    }
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
   
   padding: 0px;
   margin: 0px;
    vertical-align: top;
    border-top: transparent;
}

  .table{
    height: 210mm;
    width: 148.5mm;
    margin: 0 auto;
    margin-top: 60px;
    position: relative;
    border:1px solid #ccc !important;
    border-bottom:1px solid #ccc !important;
  }

.table .paymen_mode{
   min-width: 200px;
}
.table .paymen_ref_no{
 min-width: 200px;
}
.table .paymen_amount{
 min-width: 200px;
}

</style>
</head>
<body class="table">
         <?php $str='';
                     $amount=0;
                     $netamount=0;
                     $testamount=0;
                     ?>
    <table width="100%">
        @foreach($shift->getPadestal as $padestal)
         
             <tr>
               <th colspan="6" align="left"><p>Padestal No. : {{$padestal->Pedestal_Number}}</p></th> 
            </tr>

            <tr>
               <th>Nozzle No.</th> 
               <th>Fuel Type</th>
               <th style="text-align:right; width:13%;">Fuel Price</th>
               <th style="text-align:right; width:18%;">Volume Out</th>
               <th style="text-align:right; width:18%;">Test Amount</th>
               <th style="text-align:right; width:18%;">Net Sales</th>
            </tr>
           
            @foreach($padestal->getNozzle as $getNozzle)
               <?php $nore=$getNozzle->getReading()->where('shift_id',$shift->id)->first(); ?>
                <tr class="parentcmr">
                  <td>{{$getNozzle->Nozzle_Number}}</td> 
                  <td>{{$getNozzle->getItem->Item_Name}}</td>
                   <td style="text-align:right;">{{$getNozzle->getItem->price->price}}</td>
                   <?php $sb=$nore['Nozzle_End']-$nore['Nozzle_Start'];?>
                   <td style="text-align:right;">{{$sb}}</td>
                 
                  <?php $netamount=($nore['reading']*$getNozzle->getItem->price->price)+$netamount;
                        $testamount=($nore['reading']*$getNozzle->getItem->price->price)+$testamount;
                  ?>
                  <td style="text-align:right;"> {{$nore['test']*$getNozzle->getItem->price->price}} </td>
                  <td style="text-align:right;">{{$nore['reading']*$getNozzle->getItem->price->price}}</td>
                </tr>
             @endforeach

        @endforeach
    </table>


    <table width="100%">
   

    <tbody>

      <?php $amount=0; ?>
     
      @if($shift->settelment!=null && $shift->settelment->getPayments!=null)
          <tr >
            <td  colspan="5"></td>
            <td align="right" >
                <span class="paymen_mode"></span>
                <span class="paymen_ref_no">Total Nozzle Sales: </span>
                <span class="paymen_amount">{{$shift->settelment->Nozzle_grass}}</span>
            </td>
          </tr>

           <tr >
            <td  colspan="5"></td>
            <td align="right" >
                <span class="paymen_mode"></span>
                <span class="paymen_ref_no">Credit Sales Fuels:  </span>
                <span class="paymen_amount">{{$shift->settelment->Credit_fuel}}</span>
            </td>
          </tr>

           <tr >
            <td  colspan="5"></td>
            <td align="right" >
                <span class="paymen_mode">Payment mode:</span>
                <span class="paymen_ref_no">Ref No. </span>
                <span class="paymen_amount">Amount:</span>
            </td>
          </tr>

        @foreach($shift->settelment->getPayments as $Payment)

          <?php $amount=$Payment->pivot->amount+$amount;?>
          <tr >
            <td align="right" colspan="5"></td>
            <td align="right" >
                 
                <span class="paymen_mode">{{$Payment->name}}</span>
                <span class="paymen_ref_no" style="width:200px"> 
                 {{$Payment->pivot->ref_no}}
                </span>
                <span class="paymen_amount" style="width:200px">
                   {{$Payment->pivot->amount}}
                </span>
              
            </td>
          </tr>
        @endforeach
          <tr >
            <td  colspan="5"></td>
            <td align="right" >
                <span class="paymen_mode"></span>
                <span class="paymen_ref_no">Total Amount: </span>
                <span class="paymen_amount">{{$amount}}</span>
            </td>
          </tr>

           <tr >
            <td  colspan="5"></td>
            <td align="right" >
                <span class="paymen_mode"></span>
                <span class="paymen_ref_no">Difference: </span>
                <span class="paymen_amount">{{$shift->settelment->Diff}}</span>
            </td>
          </tr>

        
      @endif

    </tbody>

    </table>
    @if($shift->getTanksReading!=null && $shift->getTanksReading->count()>0)
                
       <table width="100%" align="left" >
        <tr> 
            <th><h3>  Dip Readings</h3> </th>
        </tr>
       
        <tr> 
            <th>Tank Name </th>
            <th>Fuel Type </th>
            <th>Unit of Measure </th>
            <th>Reading</th>
          
        </tr>
       
           @foreach($shift->getTanksReading as $getTanksReading)
             <tr> 
                <td>{{$getTanksReading->getTank->Tank_Number}} </td>
                <td>{{$getTanksReading->getfueltypes->Item_Name}} </td>
                <td>{{$getTanksReading->getunitmesure->Unit_Symbol}} </td>
                <td align="right">{{$getTanksReading->value}}</td>
              
            </tr>
            @endforeach
       
       </table>
    @endif
  <br>
  <br>
  <center>
    <input type="button"  id="myPrntbtn" class="btn btn-success site-btn" onclick="window.print();" value="Print" />
  </center> 


  <script type="text/javascript">
          function PrintDiv() {

              var contents = document.getElementById("qrcodeprintContents").innerHTML;

              var frame1 = document.createElement('iframe');
              frame1.name = "frame1";
              frame1.style.position = "absolute";
              frame1.style.top = "-1000000px";
              document.body.appendChild(frame1);
              var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
              frameDoc.document.open();
              frameDoc.document.write('<html><head><title>QR Print</title>');
              frameDoc.document.write('</head><body>');
              frameDoc.document.write(contents);
              frameDoc.document.write('</body></html>');
              frameDoc.document.close();
              setTimeout(function () {
                  window.frames["frame1"].focus();
                  window.frames["frame1"].print();
                  document.body.removeChild(frame1);
              }, 500);
              return false;
          }
      </script>

  </body>
</html>