@extends('backend-includes.app')

@section('content')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Reprint Settlement</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Reprint Settlement</a></li>
        </ul>
      </div>
    </div>

    <div  style="margin-top: 41px;">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


      <div class="col-md-10 col-md-offset-1 card" style="padding:10px!important;">
          <form class="form-horizontal" target="_blank" id="form2" action="{{route('reprint_settlement')}}" method="post">
              {{ csrf_field() }}

              <input type="hidden" name="type" value="1">
              @if(Auth::user()->user_type==3)
                  
                  <label>Select shift Manager</label>&nbsp;
                  
                  
                  <select style="width: 20%; display:inline!important;" class="form-control" id="shift_manager" name="shift_manager"  required="required">
                    <option value="">Select Shift Manager</option>
                      @foreach($shiftmanager as $shiftmanagers)
                      <option value="{{$shiftmanagers->id}}">{{$shiftmanagers->Personnel_Name}}</option>
                      @endforeach
                  </select>
                   
                  
                    <label>Start Date</label>
                  
                  
                    <input style="width: 20%; display:inline!important;" type="text" class="form-control datepicker" name="date" id="start_date" />
                  
                  
                    <label>End Date</label>
                  
                  
                    <input type="text" style="width: 20%; display:inline!important;" class="form-control datepicker" name="date" id="end_date" />
                  
                  <input type="button" class="btn btn-primary " value="Go" id="go">
              @endif
              <br>
              <br>
                 
                 <label id="siftname1">Select shift</label>&nbsp; 
                  <select style="width:43%; display:inline !important;" name="siftname" class="form-control"  id="siftname">
                    </select>   
                       
              @if(Auth::user()->user_type==3)
             
                  <input type="submit" class="btn btn-primary abhishek" value="Print" id="print">
              
              @endif

          </form>
      </div>

 
        
 
    </div>
  </div>
@endsection
@section('script')
<script type="text/javascript">
jQuery(function(){

    app={
          init:function(){
            jQuery('#go').on('click',function(){

              var id=jQuery('#shift_manager').val();
              
              if(id) 
                {
                  app.getShift();
                }else{
                  alert("Please select shift first.");
                }
               
               // jQuery('#form2').submit();
            });

             jQuery('#shift_manager').on('change',function(){
             
               // jQuery('#form2').submit();
            jQuery('#siftname').hide();
            jQuery('#siftname1').hide();
            jQuery('#print').hide();
            });
            jQuery('#siftname1').hide();

                  jQuery('#siftname').hide();
                  jQuery('#print').hide();
          
          },
          getShift:function(){

              var id=jQuery('#shift_manager').val();
              var start_date=jQuery('#start_date').val();
              var end_date=jQuery('#end_date').val();
              
              // alert(id);

              jQuery.get("{{route('reprint.get_all_shift')}}",{
                id:id,start_date:start_date,end_date:end_date,
              '_token': jQuery('meta[name="csrf-token"]').attr('content'),
              },function(data){
                var str='';
                jQuery.each(data,function(i,v){
                  str+='<option value="'+i+'">'+v+'</option>';
                });
                // alert(str);
                if (str=='') {
                   jQuery('#siftname1').hide();
                  jQuery('#siftname').hide();
                  jQuery('#print').hide();
                  alert("No shift available between theese date.");
                }else{

                  jQuery('#siftname1').show();
                  jQuery('#siftname').show();
                  jQuery('#print').show();
                  // jQuery('#go').hide();

                jQuery('#siftname').html(str);
                   
                }


              });
          },
        }

    app.init();

});
        function PrintDiv() {

            var contents = document.getElementById("qrcodeprintContents").innerHTML;

            var frame1 = document.createElement('iframe');
            frame1.name = "frame1";
            frame1.style.position = "absolute";
            frame1.style.top = "-1000000px";
            document.body.appendChild(frame1);
            var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
            frameDoc.document.open();
            frameDoc.document.write('<html><head><title>QR Print</title>');
            frameDoc.document.write('</head><body>');
            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                document.body.removeChild(frame1);
            }, 500);
            return false;
        }
    </script>

@endsection
