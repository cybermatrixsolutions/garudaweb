@extends('backend-includes.app')

@section('content')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Reprint Settlement</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Reprint Settlement</a></li>
        </ul>
      </div>
    </div>

    <div  style="margin-top: 41px;">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


      <div class="col-md-10 col-md-offset-1 card" style="padding:10px!important;">
          <form class="form-horizontal" id="form2" action="{{route('reprint_settlement')}}" method="post">
              {{ csrf_field() }}

              <input type="hidden" name="type" value="1">
              @if(Auth::user()->user_type==3)
                  <label>Select shift Manager</label>&nbsp;
                  <select style="width:20%; display:inline!important;" class="form-control" id="shift_manager" name="shift_manager"  required="required">
                      @foreach($shiftmanager as $shiftmanagers)
                      <option value="{{$shiftmanagers->id}}">{{$shiftmanagers->Personnel_Name}}</option>
                      @endforeach
                  </select>&nbsp;&nbsp;
              @endif
                 
                 <label id="siftname1">Select shift</label>&nbsp; 
                  <select style="width:40%; display:inline !important;" name="siftname" class="form-control"  id="siftname">
                    </select>           
              @if(Auth::user()->user_type==3)
                  <input type="submit" class="btn btn-primary abhishek" value="Go">
              @endif

          </form>
      </div>

 
        
 
    </div>
  </div>
@endsection
@section('script')
<script type="text/javascript">
jQuery(function(){

    app={
          init:function(){
            jQuery('#shift_manager').on('change',function(){
               app.getShift();
            });
            // app.getShift();
              //             jQuery('#siftname1').hide();
              // jQuery('#siftname').hide();
          },
          getShift:function(){

              var id=jQuery('#shift_manager').val();
              // alert(id);

              jQuery.get("{{route('reprint.get_all_shift')}}",{
                id:id,
              '_token': jQuery('meta[name="csrf-token"]').attr('content'),
              },function(data){
                var str='';
                jQuery.each(data,function(i,v){
                  str+='<option value="'+i+'">'+v+'</option>';
                });

                jQuery('#siftname').html(str);

              });
          },
        }

    app.init();

});
        function PrintDiv() {

            var contents = document.getElementById("qrcodeprintContents").innerHTML;

            var frame1 = document.createElement('iframe');
            frame1.name = "frame1";
            frame1.style.position = "absolute";
            frame1.style.top = "-1000000px";
            document.body.appendChild(frame1);
            var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
            frameDoc.document.open();
            frameDoc.document.write('<html><head><title>QR Print</title>');
            frameDoc.document.write('</head><body>');
            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                document.body.removeChild(frame1);
            }, 500);
            return false;
        }
    </script>

@endsection
