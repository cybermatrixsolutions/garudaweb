
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSS-->
<!-- Font-icon css-->
<style type="text/css"  media="print">
  

    @page  
    {
         size: A7;
		
	}

</style>
<style type="text/css" media="print">
@page  {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>
<style type="text/css">
@media  print {
    #myPrntbtn {
        display :  none;
    }
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
   
   padding: 0px;
   margin: 0px;
    vertical-align: top;
    border-top: transparent;
}

  .table{
     border-collapse: collapse;
	 font-family: arial, sans-serif;
    height: auto;
    width: 148.5mm;
    margin: 0 auto;
    margin-top: 60px;
    position: relative;
   
    border-bottom:1px solid #ccc !important;
  }



hr{
  border-top: 1px solid #1d0d0d;
  width: 100%;
}
</style>
</head>
	<body class="table">
		  <?php $str='';
                     $amount=0;
                     $netamount=0;
                     $testamount=0;
                     $totalprices=0;
                     ?>



		<table width="100%" style=" border: 1px solid;">

			<tr>
				<td>
					<table width="100%" style="">
						 <tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 10px;padding-bottom: 0px;padding-left: 10px;"><img src="{{URL::asset('')}}{{$Principal->image}}" onerror="this.src='../images/logo/garruda.jpg';" alt="garruda" title="garruda" width="70" height="50px" ></img></p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;"><p>@if(!empty($detail->pump_name)){{$detail->pump_name}}@endif</p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 100;text-transform: uppercase;"><p>{{$detail->pump_addetail}}</p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>{{$CityModel->name}},{{$StateManagementModel->name}},{{$MastorRO->pin_code}}</p></th>
						</tr>
						
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
					<table width="100%">	
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;text-transform:upercase;">Shift Start Date & Time<span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;text-align:left;text-transform:upercase;">{{date_format($shift->created_at,"d-m-Y H:i:s")}}</td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;text-transform:upercase;">Shift Close Date & Time<span style="float:right;">:</span></td> 
							<?php
									 
									$date = date('d-m-Y H:i:s', strtotime(trim($shift->closer_date)));

									 // dd($date);
							?>
							<td width="70%" style="font-size: 12px;padding-left: 10px;text-align:left;text-transform:upercase;">{{$date}}</td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;text-transform:upercase;">Shift Managrs <span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;text-transform:upercase;">{{$shift->getPersonnelname->Personnel_Name}}</td>
						</tr>

						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>
			@if($shift->type!=2)

		
			<tr>
				<td>
					<table width="100%">	

						<tr>
							<th style="width:12%;font-size: 13px;font-weight: 700;text-transform: uppercase;">
								<p style="margin: 0px;text-align: left;margin-left: 10px;">nozzle no.</p>
							</th> 
						<!-- 	<th style="width:12%;font-size: 13px;font-weight: 700;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-left: 10px;">fuel type</p>
							</th> -->
							<th style="width:8%;font-size: 13px;font-weight: 700;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-left: 10px;">omr</p>
							</th>
							<th style="width:7%;font-size: 13px;font-weight: 700;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-left: 10px;">cmr</p>
							</th>
							<th style="width:15%;font-size: 13px;font-weight: 700;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-left: 10px;">test volume</p>
							</th>
							<th style="width:16%;font-size: 13px;font-weight: 700;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-left: 10px;">net sales qty</p>
							</th>
						</tr>
					</table>
			
				</td>
			
			</tr>
			
			<tr>
				<td>

@foreach($shift->getPadestal as $padestal)
              <p><b>Pedestal No. : {{$padestal->Pedestal_Number}}</b></p>


							          <?php $str.=','.$padestal->Pedestal_Number; ?>

							           <?php

							         $padData=$padestal->getNozzle->where('is_active',1)->sortBy('Nozzle_Number');
							         $padDataFuelWise=$padData->groupBy('fuel_type');
							         ?>

							             @foreach($padDataFuelWise as $key => $padValueFuelWise)

							          <?php $itemName=DB::table('tbl_price_list_master')
							                  ->where('RO_code',$padestal->RO_code)
							                  ->where('id',$key)
							                  ->value('Item_Name');

							                  ?>

					<table width="100%">	

							


                                  <h5 style="font-weight: 700;">{{$itemName}}</h5>


						@foreach($padValueFuelWise as $getNozzle)
						<?php $nore=$getNozzle->getReading()->where('shift_id',$shift->id)->first(); ?>
						<tr>

							<th style="width:12%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: left;margin-left: 10px;">{{$getNozzle->Nozzle_Number}}</p>
							</th> 
						<!-- 	<th style="width:12%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align:left;margin-left: 10px;">{{$getNozzle->getItem->Item_Name}}</p>
							</th> -->
							<th style="width:8%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-right: 15px;">{{number_format((float)$nore['Nozzle_Start'], 3, '.', '')}}</p>
							</th>
							<th style="width:7%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-right: 12px;">{{number_format((float)$nore['Nozzle_End'], 3, '.', '')}}</p>
							</th>
							 <?php $netamount=($nore['reading']*$getNozzle->getItem->price->price)+$netamount;
                        $testamount=($nore['reading']*$getNozzle->getItem->price->price)+$testamount;
                  ?>
							<th style="width:15%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-right: 12px;">{{number_format((float)$nore['test'], 3, '.', '')}}</p>
							</th>
							<?php $sb=$nore['Nozzle_End']-$nore['Nozzle_Start'];?>
				                <?php
				                        $chetqty1=number_format((float)$nore['test'], 3, '.', ''); 
				            
				                         $chetqty=number_format((float)$sb-$chetqty1, 3, '.', ''); 
				                  ?>
							<th style="width:16%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-left: 10px;">{{$chetqty}}</p>
							</th>
						</tr>
						   @endforeach
                         @endforeach
       
					</table>


                   @endforeach


			
				</td>
			
			</tr>
			






















			@endif
			@if($shift->type!=2)
			<tr>
				<td>
					<table width="100%">	
						<tr>
							<th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;">
								<p style="margin:0px;font-size:13px;text-transform: uppercase;"><b>Fuel typewise net sales</b></p>
							</th>
						</tr>
			
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>

			<tr>
				<td>
					<table width="100%">	
						<tr>
							<th style="width:25%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: left;margin-left: 10px;">fuel type</p>
							</th> 
							<th style="width:25%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align:left;margin-left: 10px;">net sales</p>
							</th>
							<th style="width:25%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-right: 15px;">rate</p>
							</th>
							<th style="width:25%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-right: 12px;">total</p>
							</th>
							
						</tr>
						
					</table>
			
				</td>
			
			</tr>
			
			
			@endif

			@if($shift->type!=2)
          <?php 
          $shift_id=$shift->id;
          $shift_manager=$shift->shift_manager;

          $variable = DB::table('tbl_ro_nozzle_reading')
          ->join('tbl_pedestal_nozzle_master', TRIM('tbl_ro_nozzle_reading.Nozzle_No') , TRIM('tbl_pedestal_nozzle_master.Nozzle_Number'))
          ->join('tbl_price_list_master', 'tbl_price_list_master.id', 'tbl_pedestal_nozzle_master.fuel_type')
          ->join('tbl_item_price_list', 'tbl_price_list_master.id', 'tbl_item_price_list.item_id')
          ->join('shift_pedestal', 'shift_pedestal.pedestal_id', 'tbl_pedestal_nozzle_master.Pedestal_id')
          ->join('shifts', 'shifts.id', 'shift_pedestal.shift_id')
          ->select('tbl_ro_nozzle_reading.Nozzle_No', DB::raw('(tbl_ro_nozzle_reading.Nozzle_End - tbl_ro_nozzle_reading.Nozzle_Start ) - tbl_ro_nozzle_reading.test as qty'),'tbl_ro_nozzle_reading.reading','tbl_ro_nozzle_reading.price','tbl_ro_nozzle_reading.Nozzle_End','tbl_price_list_master.item_name')

            // ->where('tbl_ro_nozzle_reading.Reading_by',$shift_manager)
          ->where('tbl_item_price_list.is_active',1)
          ->where('tbl_price_list_master.is_active',1)
          ->where('tbl_pedestal_nozzle_master.is_active',1)
          ->where('shifts.id',$shift_id)
          ->where('shifts.fuel',1)
          ->where('shifts.shift_manager',$shift_manager)
          ->where('tbl_ro_nozzle_reading.shift_id',$shift_id)
          ->whereNotNull('tbl_ro_nozzle_reading.Nozzle_End')
          ->get();

            // dd($variable, $shift_id,$shift_manager);
          $Item_Name='';
          $totalprices=0;
          $item_name2 = array();
          ?>
          @foreach($variable as $value)
          <?php   
          $item_name[] = $value->item_name;
          $item_name2 =  array_unique($item_name);

          ?>
          @endforeach  
          @foreach($item_name2 as $item_name_new)
          <?php
          $chetqty_previous=0;

          ?>
          @foreach($variable as $variabless)
          <?php


          if ($item_name_new == $variabless->item_name) {
            $Item_Names = $variabless->item_name;
            $chetqty_previous = ($variabless->qty)+($chetqty_previous);

            $chetqty_previous=number_format((float)$chetqty_previous, 3, '.', '');

            $price = $variabless->price;
          }

          ?>
          @endforeach 

          
			<tr>
				<td>
					<table width="100%">	
						<tr>
							<th style="width:25%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: left;margin-left: 10px;">{{$Item_Names}}</p>
							</th> 
							<th style="width:25%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align:right;margin-right: 40%;">{{$chetqty_previous}}</p>
							</th>
							<th style="width:25%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-right: 15px;">{{$price}}</p>
							</th>
									<?php  
                    $totalprice = ($price)*($chetqty_previous);
                ?>
							<th style="width:25%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-right: 12px;">{{number_format((float)$totalprice, 2, '.', '')}}</p>
							</th>
							
					
			
						</tr>
						
					</table>
			
				</td>
			
			</tr>
			







		 <?php 
            $totalprices = $totalprice+$totalprices;
            $totalprices = number_format((float)$totalprices, 2, '.', '');
          ?>
          @endforeach  
          @endif


			<tr>
				<td>
					<table width="100%">	
						
				
						                 <?php 


         $shift_id = $shift->id;


         $var = DB::table('tbl_customer_transaction')
         ->join('tbl_price_list_master', 'tbl_customer_transaction.item_code', 'tbl_price_list_master.id')
         ->join('tbl_stock_item_group', 'tbl_stock_item_group.id', 'tbl_price_list_master.Stock_Group')
         ->select(DB::raw('sum(tbl_customer_transaction.petroldiesel_qty*tbl_customer_transaction.item_price) AS total_sales'))
         ->where('tbl_stock_item_group.Group_Name','LUBES')
         ->where('tbl_customer_transaction.shift_id',$shift_id)
         ->where('tbl_customer_transaction.petrol_or_lube',2)
         ->get();

         $vara = DB::table('tbl_customer_transaction')
         ->join('tbl_price_list_master', 'tbl_customer_transaction.item_code', 'tbl_price_list_master.id')
         ->join('tbl_stock_item_group', 'tbl_stock_item_group.id', 'tbl_price_list_master.Stock_Group')
         ->select(DB::raw('sum(tbl_customer_transaction.petroldiesel_qty*tbl_customer_transaction.item_price) AS total_sales'))
         ->where('tbl_stock_item_group.Group_Name','OTHERS')
         ->where('tbl_customer_transaction.shift_id',$shift_id)
         ->where('tbl_customer_transaction.petrol_or_lube',2)
         ->get();


         ?>
    
						@foreach($var as $vars)
						<tr>
							<td  width="80%" style="font-size: 13px;padding-left: 10px;text-transform:upercase;text-align:right;">Total Lube Sales<span style="float:right;">:</span></td> 
							<td  width="20%" style="font-size: 13px;padding-left: 10px;text-align:left;text-transform:upercase;text-align:right;">
								<p style="margin: 0px;text-align: right;margin-right: 12px;">{{number_format((float)$vars->total_sales, 2, '.', '')}}</p></td>
						</tr>

						  @endforeach
						@foreach($vara as $vars1)
						<tr>
							<td  width="80%" style="font-size: 13px;padding-left: 10px;text-transform:upercase;text-align:right;">Total Other Sales<span style="float:right;">:</span></td> 
							<td width="20%" style="font-size: 13px;padding-left: 10px;text-align:left;text-transform:upercase;text-align:right;">
								<p style="margin: 0px;text-align: right;margin-right: 12px;">{{number_format((float)$vars1->total_sales, 2, '.', '')}}</p>
							</td>
						</tr>
						@endforeach
						
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>
			      <?php $amount=0; ?>
     
      @if($shift->settelment!=null && $shift->settelment->getPayments!=null)
			<tr>
				<td>
					<?php
           $total_lubes_sales = number_format((float)$vars1->total_sales, 2, '.', '');
           $total_other_sales = number_format((float)$vars->total_sales, 2, '.', '');
           $netamount = number_format((float)$totalprices+($total_lubes_sales+$total_other_sales), 2, '.', '');

           ?>
		
					<table width="100%">	
						<tr>
							<td   width="80%" style="font-size: 12px;padding-left: 10px;text-transform: uppercase;text-align:right;">Total Sales</td> 
							<td  width="20%" style="font-size: 13px;padding-left: 10px;text-transform:upercase;text-align:right;">
								<p style="margin: 0px;text-align: right;margin-right: 12px;">{{number_format($netamount, 2, '.', '')}}</p></td>
						</tr>
						
						
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
					<table width="100%">	
						<tr>
							<th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;">
								<p style="margin:0px;font-size:13px;text-transform: uppercase;"><b>settlement</b></p>
							</th>
						</tr>
			
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>

			<?php      
            $customer_transaction = DB::table('tbl_customer_transaction')
            ->select(DB::raw('sum(petroldiesel_qty*item_price) AS total_sales'),'id')
            ->where('shift_id',$shift_id)
            ->where('cust_name','credit')
            ->where('trans_mode','auto')
            ->first();

            $customer_transaction_manual = DB::table('tbl_customer_transaction')
            ->select(DB::raw('sum(petroldiesel_qty*item_price) AS total_sales'),'id')
            ->where('shift_id',$shift_id)
            ->where('cust_name','credit')
            ->where('trans_mode','SLIPS')
            ->first();

            $previous_multi1 = 0;

           
            ?>
              <?php 

          $customer_transactions1 = number_format($customer_transaction->total_sales, 2, '.', '');
          $customer_transactions_manual1 = number_format($customer_transaction_manual->total_sales, 2, '.', '');
                // dd($customer_transaction);
          $customer_transactions_total1 = $customer_transactions1 +  $customer_transactions_manual1;
          ?>
					<tr>
				<td>
					<table width="100%">	
						<tr>
							<th style="width:18%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: left;margin-left: 10px;">payment mode </p>
							</th> 
							<th style="width:15%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: left;margin-left: 39px;"> ref.no</p>
							</th> 
							<th style="width:16%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-left: 10px;">garruda receipt</p></th>
							<th style="width:16%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-left: 10px;">manual receipt</p>
							</th>
							<th style="width:16%;font-size: 13px;font-weight: 100;text-transform: uppercase;">
								<p style="margin: 0px;text-align: right;margin-left: 10px;">total receipt</p>
							</th>
							
						</tr>
					</table>
			
				</td>
			
			</tr>
			
			
			<tr>
				<td>
					<table width="100%">	
						<tr>
							<th style="width:16%;font-size: 13px;font-weight: 100;">
								<p style="margin: 0px;text-align: left;margin-left: 10px;">Credit</p>
							</th> 
							<th style="width:16%;10px;font-size: 13px;font-weight: 100;">
								<p style="margin: 0px;text-align: right;margin-right: 10px;">0.00</p>
							</th>
							<th style="width:16%;10px;font-size: 13px;font-weight: 100;">
								<p style="margin: 0px;text-align: right;margin-right: 10px;">{{$customer_transactions1}}</p>
							</th>
							<th style="width:16%;font-size: 13px;font-weight: 100;">
								<p style="margin: 0px;text-align: right;margin-right: 10px;">{{$customer_transactions_manual1}}</p>
							</th>
							<th style="width:16%;font-size: 13px;font-weight: 100;">
								<p style="margin: 0px;text-align: right;margin-right: 10px;">{{$customer_transactions_total1}}</p>
							</th>
							
						</tr>
					</table>
			
				</td>
			
			</tr>

          <?php $str=''; 

     
          $paymentmode_sattlement_new = DB::table('tpl_payment_mode') 
          ->leftJoin('paymentmode_sattlement','tpl_payment_mode.id','=','paymentmode_sattlement.paymentmode_id')
          ->leftJoin('tbl_shift_settlement','tbl_shift_settlement.id','=','paymentmode_sattlement.sattlement_id')

          ->select('tpl_payment_mode.id','tpl_payment_mode.name','paymentmode_sattlement.garruda_receipt','paymentmode_sattlement.manual_receipt','paymentmode_sattlement.amount','paymentmode_sattlement.ref_no' )
          ->where('tbl_shift_settlement.shift_id','=',$shift_id)
          ->where('tpl_payment_mode.IsActive',1)
          ->groupBy('tpl_payment_mode.name')
          ->orderBy('tpl_payment_mode.order_number')
          ->get();



        $str2='';
          ?>
			<?php $amount1 = 0; ?>
			@foreach($paymentmode_sattlement_new as $key => $Payment)

			<?php $amount=$Payment->amount;
				$amount = number_format((float)$amount, 2, '.', '');
				$amount1+=$amount;
			?>

			<tr>
				<td>
					<table width="100%">	
						<tr>
							<th style="width:16%;font-size: 13px;font-weight: 100;"><p style="margin: 0px;text-align: left;margin-left: 10px;">{{$Payment->name}}</p></th> 
							<th style="width:16%;font-size: 13px;font-weight: 100;">
								<p style="margin: 0px;text-align: right;margin-right: 10px;">{{$Payment->ref_no}}</p>
							</th>
							<th style="width:16%;font-size: 13px;font-weight: 100;">
								<p style="margin: 0px;text-align: right;margin-right: 10px;">{{number_format((float)$Payment->garruda_receipt, 2, '.', '')}}</p>
							</th>
							<th style="width:16%;font-size: 13px;font-weight: 100;"><p style="margin: 0px;text-align: right;margin-right: 10px;">{{number_format((float)$Payment->manual_receipt, 2, '.', '')}}</p></th>
							<th style="width:16%;font-size: 13px;font-weight: 100;"><p style="margin: 0px;text-align: right;margin-right: 10px;">{{number_format((float)$Payment->amount, 2, '.', '')}}</p></th>
							
						</tr>
					</table>
			
				</td>
			
			</tr>
			
			  @endforeach
			<tr>
				<td>
					
		<?php 
        $amount2 = number_format(($amount1)+($customer_transactions_total1), 2, '.', '');
        $different = number_format(($amount2)-($netamount), 2, '.', '');

        ?>

					<table width="100%">	
						<tbody>
						<tr>
							<td width="80%" style="font-size: 12px;padding-left: 10px;text-transform: uppercase;text-align: right;">Total receipt</td> 
							<td width="20%" style="font-size: 12px;text-align: right;"><p style="margin: 0px;text-align: right;margin-right: 10px;">{{number_format((float)$amount2, 2, '.', '')}}</p></td>
						</tr>
						<tr>
							<td width="80%" style="font-size: 12px;padding-left: 10px;text-transform: uppercase;text-align: right;">short/excess</td> 
							<td width="20%" style="font-size: 12px;text-align: right;"><p style="margin: 0px;text-align: right;margin-right: 10px;">{{number_format((float)$different, 2, '.', '')}}</p></td>
						</tr>
						
						<tr><td colspan="6"><hr></td></tr>
					</tbody></table>
				</td>
			</tr>
			@endif
			@if($shift->getTanksReading!=null && $shift->getTanksReading->count()>0)
			<tr>
				<td>
					<table width="100%">	
						<tbody><tr>
							<th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;">
								<p style="margin:0px;font-size:13px;text-transform: uppercase;">tank dip reading</p>
							</th>
						</tr>
			
						<tr><td colspan="6"><hr></td></tr>
					</tbody></table>
				</td>
			</tr>
			
			<tr>
				<td>
					<table width="100%">	
						<tbody>
							@foreach($shift->getTanksReading as $getTanksReading)
							<tr>
								<td width="30%" style="font-size: 12px;padding-left: 10px;text-transform: uppercase;">tank no.<span style="float:right;">:</span></td> 
								<td width="70%" style="font-size: 12px;padding-left: 10px;text-align:left;text-transform: uppercase;">{{$getTanksReading->getTank->Tank_Number}}</td>
							</tr>
							<tr>
								<td width="30%" style="font-size: 12px;padding-left: 10px;text-transform: uppercase;">fuel type<span style="float:right;">:</span></td> 
								<td width="70%" style="font-size: 12px;padding-left: 10px;text-transform: uppercase;">{{$getTanksReading->getfueltypes->Item_Name}}</td>
							</tr>
							<tr>
								<td width="30%" style="font-size: 12px;padding-left: 10px;text-transform: uppercase;">unit of messure<span style="float:right;">:</span></td> 
								<td width="70%" style="font-size: 12px;padding-left: 10px;">{{$getTanksReading->getunitmesure->Unit_Symbol}}</td>
							</tr>
							<tr>
								<td width="30%" style="font-size: 12px;padding-left: 10px;text-transform: uppercase;">capacity<span style="float:right;">:</span></td> 
								<td width="70%" style="font-size: 12px;padding-left: 10px;text-transform: uppercase;">{{$getTanksReading->capacity}}</td>
							</tr>
							<tr>
								<td width="30%" style="font-size: 12px;padding-left: 10px;text-transform: uppercase;">volume<span style="float:right;">:</span></td> 
								<td width="70%" style="font-size: 12px;padding-left: 10px;text-transform: uppercase;">{{$getTanksReading->value}}</td>
							</tr>
							<tr><td colspan="6"><hr></td></tr>
							 @endforeach
						</tbody>
					</table>
				</td>
			</tr>
			 @endif
			
			
			<tr>
				<td>
					<table width="100%">		
						<tr>
							<th colspan="6" align="center" style="text-align: center;padding-top: 0px;padding-bottom: 10px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>Powered by : www.garruda.co.in</p></th>
						</tr>
			
					</table>
				</td>
			</tr>
		</table>
        	
			
		   	
		
		
		
		
		
		
		
		
		
             
      
      <br>
  
  <center>
    <input type="button"  id="myPrntbtn" class="btn btn-success site-btn" onclick="window.print();" value="Print" / style="padding: 8px;padding-left: 20px;padding-right: 20px;">
  </center> 


  <script type="text/javascript">
          function PrintDiv() {

              var contents = document.getElementById("qrcodeprintContents").innerHTML;

              var frame1 = document.createElement('iframe');
              frame1.name = "frame1";
              frame1.style.position = "absolute";
              frame1.style.top = "-1000000px";
              document.body.appendChild(frame1);
              var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
              frameDoc.document.open();
              frameDoc.document.write('<html><head><title>QR Print</title>');
              frameDoc.document.write('</head><body>');
              frameDoc.document.write(contents);
              frameDoc.document.write('</body></html>');
              frameDoc.document.close();
              setTimeout(function () {
                  window.frames["frame1"].focus();
                  window.frames["frame1"].print();
                  document.body.removeChild(frame1);
              }, 500);
              return false;
          }
      </script>

  </body>
</html>