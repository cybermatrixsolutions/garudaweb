@extends('backend-includes.app')
@section('content')
<style>
button.dt-button, div.dt-button, a.dt-button {
    
    background-image: linear-gradient(to bottom, #00786a 0%, #00786a 100%)!important;
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,StartColorStr='white', EndColorStr='#00786a');
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    text-decoration: none;
    outline: none;
    color:white!important;
    }
    .dt-buttons {
    float: right!important;
}

.dataTables_wrapper .dataTables_filter {
float: left;
text-align: left;
}
</style>

  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Delivery Slip Fuel</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Delivery Slip Fuel</a></li>
        </ul>
      </div>
    </div>

    <div  style="margin-top: 41px;">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

     <?php

    $fromdate=date('d/m/Y');
    $to_date=date('d/m/Y');
    if(isset($_GET['to_date'])){
      $to_date=$_GET['to_date'];
    }

    if(isset($_GET['fromdate'])){
      $fromdate=$_GET['fromdate'];
    }

    ?>

     <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal form-inline" name="myForm" action="{{Route('reprint.delivery-slip-fuel')}}" enctype="multipart/form-data" method="get">
  
                     {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="form-group">
                          <label class="control-label"  for="from">From <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                           <input type="text"  @if(isset($_GET['fromdate'])) value="{{$_GET['fromdate']}}" @endif class="form-control" id="fromdate" name="fromdate" placeholder="from date" required/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                          <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                            <input  type="text" @if(isset($_GET['to_date'])) value="{{$_GET['to_date']}}" @endif class="form-control" id="to_date" name="to_date" placeholder="To Date" required/>&nbsp;&nbsp;
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default" id="checkdate" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                        </div>

                    </div>
                </div>
            </form> 
        </div>

    </div>





      <div class="card" style="padding:10px!important;">
         <!--  <form class="form-horizontal" action="" method="GET">
				<div class="form-group row">
					<div class="col-md-3">
						<label>Select Transaction Date</label>
					</div>
					<div class="col-md-3">
						<input type="text" class="form-control datepicker" name="date"/>
					</div>
					<div class="col-md-3">
						<input type="submit" class="btn btn-primary" value="Go">
					</div>
					
				</div>
          </form> -->
		  
		  @if($transactionList)
			<div class="table-responsive">
			  <table class="table table-striped" id="myTable">
				<thead>
					<th>Date.</th>
					<th>E-Slip No</th>
					<th>Customer Name</th>
					<th>State</th>
					<th>Amount</th>

					
					<th>Actions</th>
				</thead>
				<tbody>
				   @foreach($transactionList as $row)
					  <tr>
					  	<td>{{date("d/m/Y",strtotime($row->trans_date))}}</td>
					  		<td>{{$row->invoice_number}}</td>
					  	<td>{{$row->getCustomerMaster->company_name}}</td>
					  	<td>{{$row->getCustomerMaster->getstate->name}}</td>
					  	<td>{{bcadd($row->invoice_amount,0,2)}}</td>
					
					
						<td>
              <form action="{{route('reprint.delivery-slip-fuel-print')}}" method="POST" target="_blank">
              {{ csrf_field() }}
              <input type="hidden" name="id" value="{{$row->id}}"/>
              <button type="submit" class="btn btn-primary">Preview</button>
              </form
            </td>
				  @endforeach
				</tbody>
			  </table>
			</div>
			@endif
      </div>
  </div>
@endsection
@section('script')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>



<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">
  
  $(document).ready(function() {
       $('#myTable').DataTable( {
         "dom": 'lBfrtip',


          "processing": true,
         buttons: [
           
            { extend: 'excelHtml5',text: 'Export in CSV', footer: true , filename: ''+Date('Y-m-d')+'-reprint-delivery-fuel-slip-invoice'},
            
        ],  paging: false,


       
            
       
    });
});
</script>



<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
		$('.datepicker').datepicker("setDate","<?php if(isset($urlVars['date'])){ echo $urlVars['date'];}?>");
    });


    jQuery(function(){
    var enddated=new Date();
    var todate=new Date();
   
      var app={
               
              init:function(){
               
                
                app.fromDate();
                app.to_date();
             
              },
              fromDate:function(){
                
                jQuery("#fromdate").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         endDate: new Date(),
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     jQuery("#to_date").val('');
                      todate=new Date();
                     enddated=jQuery(this).val();
                   
                    var enddatedAfterDay = $('#fromdate').datepicker('getDate');
                    enddatedAfterDay.setDate(enddatedAfterDay)

                     app.to_date();
                    });
              },
              to_date:function(){

             
              
                  
                  jQuery("#to_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         startDate:enddated,
                         
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).datepicker('setStartDate',enddated).datepicker('setEndDate',enddatedAfterDay);

                    /* alert(enddatedAfterDay);
                     alert(enddated);*/
              },

      };

      app.init();
});
</script>
@endsection
