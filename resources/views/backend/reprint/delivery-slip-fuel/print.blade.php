
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSS-->
<!-- Font-icon css-->
<style type="text/css"  media="print">
  

    @page  
    {
         size: A7;
		
	}

</style>
<style type="text/css" media="print">
@page  {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>
<style type="text/css">
@media  print {
    #myPrntbtn {
        display :  none;
    }
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
   
   padding: 0px;
   margin: 0px;
    vertical-align: top;
    border-top: transparent;
}

  .table{
     border-collapse: collapse;
	 font-family: arial, sans-serif;
    height: auto;
    width: 148.5mm;
    margin: 0 auto;
    margin-top: 60px;
    position: relative;
   
    border-bottom:1px solid #ccc !important;
  }



hr{
  border-top: 1px solid #1d0d0d;
  width: 100%;
}
</style>
</head>
	<body class="table">
		
		<table width="100%" style=" border: 1px solid;">
			<tr>
				<td>
					<table width="100%" style="">
						 <tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 10px;padding-bottom: 0px;padding-left: 10px;"><img src="{{URL::asset('')}}{{$Principal->image}}" onerror="this.src='../images/logo/garruda.jpg';" alt="garruda" title="garruda" width="70" height="50px" ></img></img></p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;"><p>{{$roInfo->pump_legal_name}}</p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 100;text-transform: uppercase;"><p>{{$roInfo->pump_address}}</p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>{{$roInfo->getcity->name}}, {{$roInfo->getstate->name}}-{{$roInfo->pin_code}}</p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 100;text-transform: uppercase;"><p>gst/uin : {{$roInfo->GST_TIN}}</p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 100;text-transform: uppercase;"><p>vat tin no : {{$roInfo->VAT_TIN}}</p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>State Name : {{$roInfo->getstate->name}}, Code : {{$roInfo->getstate->gstcode}}</p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>Reprint Credit Delivery Advice</p></th>
						</tr>
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%" style="">
						<tr>
							<th colspan="6" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>Del-Slip No. : {{$transactionInfo->invoice_number}}<span style="float: right;padding-right:10px;"> Dt:{{date('d-m-Y', strtotime($transactionInfo->trans_date))}}</span></p></th>
						</tr>

						<tr><td colspan="6"><hr></td></tr>
					
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
						<table width="100%">	
						<tr>
							<td  width="" style="font-size: 12px;padding-left: 10px;text-transform:upercase;font-weight:bold;">{{$transactionInfo->getCustomerMaster->company_name}}</td> 
							
						</tr>
						<tr>
							
							<th colspan="6" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>{{$transactionInfo->getCustomerMaster->address_one}}, {{$transactionInfo->getCustomerMaster->address_two}}, {{$transactionInfo->getCustomerMaster->getstate->name}} -{{$transactionInfo->getCustomerMaster->pin_no}}. State Code :{{$transactionInfo->getCustomerMaster->getstate->gstcode}}</p></th>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Driver <span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;">@if(!empty($transactionInfo->getdrivercustomer->Driver_Name)) {{$transactionInfo->getdrivercustomer->Driver_Name}} @endif</td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;text-transform:upercase;">GSTIN<span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;text-align:left;text-transform:upercase;">{{$transactionInfo->getCustomerMaster->gst_no}}</td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;text-transform:upercase;">PAN<span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;text-transform:upercase;">{{$transactionInfo->getCustomerMaster->pan_no}}</td>
						</tr>
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>
					
			<tr>
				<td>
					
		
					<table width="100%">	
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Sales Agent <span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;">@if(!empty($transactionInfo->getTrangections->Personnel_Name)){{$transactionInfo->getTrangections->Personnel_Name}} @endif</td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Vehicle No <span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;">{{$transactionInfo->Vehicle_Reg_No}}</td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Driver <span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;"></td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Mobile No<span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;">{{$transactionInfo->Driver_Mobile}}</td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Req No. <span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;">{{$transactionInfo->Request_id}} <span>Dt:{{date('d-m-Y', strtotime($transactionInfo->trans_date))}}</span></td>
						</tr>
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
					<table width="100%">		
						<tr>
							<th colspan="6" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>Product Delivered</p></th>
						</tr>
						<tr>
							<th colspan="6" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;text-transform:uppercase;"><p>{{$transactionInfo->getItemName->Item_Name}}</p></th>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Price. <span style="float:right;">:</span> </td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;"> 
								@if(!empty($transactionInfo->getItemName->price->price))
								{{$transactionInfo->getItemName->price->price}}
								@endif
							</td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Qty. Ord <span style="float:right;">:</span> </td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;"> 
								@if(!empty($transactionInfo->getfuelRequest->Request_Type))
								{{$transactionInfo->getfuelRequest->Request_Type}}, {{$transactionInfo->getfuelRequest->Request_Value}}
								@endif
							</td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Qty. Deld <span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;">{{$transactionInfo->petroldiesel_qty}} Ltr</td>
						</tr>
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
					<table width="100%">		
						<tr>
							<th colspan="6" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100; font-weight: bold;"><p>Terms & Condition</p></th>
						</tr>
						<tr>
							<th colspan="6" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>{{$roInfo->getdetails->TC_Delivery_Slip}}</p></th>
						</tr>
						<tr>
							<th colspan="6" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>{{$roInfo->getdetails->TC_Delivery_Slip2}}</p></th>
						</tr>
			
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
					<table width="100%">		
						<tr>
							<th colspan="6" align="left" style="text-align: left;padding-top: 10px;padding-bottom: 10px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>Rates applicable on the date & time of delivery will be charged in the periodic bill</p></th>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
					
					<table width="100%">				
						<tr>
						   <th colspan="6" align="right" style="text-align: right;padding-top: 0px;padding-bottom: 10px;padding-left: 10px;font-size: 12px;font-weight: 100;padding-right: 10px;"><p>for {{$roInfo->pump_legal_name}}</p></th>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
					<table width="100%">		
						<tr>
							<th colspan="6" align="center" style="text-align: center;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>Computer Generated Invoice</p></th>
						</tr>
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
					<table width="100%">		
						<tr>
							<th colspan="6" align="center" style="text-align: center;padding-top: 0px;padding-bottom: 10px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>Powered by : www.garruda.co.in</p></th>
						</tr>
			
					</table>
				</td>
			</tr>
		</table>
        	
			
		   	
		
		
		
		
		
		
		
		
		
             
      
      <br>
  
  <center>
    <input type="button"  id="myPrntbtn" class="btn btn-success site-btn" onclick="window.print();" value="Print" / style="padding: 8px;padding-left: 20px;padding-right: 20px;">
  </center> 


  <script type="text/javascript">
          function PrintDiv() {

              var contents = document.getElementById("qrcodeprintContents").innerHTML;

              var frame1 = document.createElement('iframe');
              frame1.name = "frame1";
              frame1.style.position = "absolute";
              frame1.style.top = "-1000000px";
              document.body.appendChild(frame1);
              var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
              frameDoc.document.open();
              frameDoc.document.write('<html><head><title>QR Print</title>');
              frameDoc.document.write('</head><body>');
              frameDoc.document.write(contents);
              frameDoc.document.write('</body></html>');
              frameDoc.document.close();
              setTimeout(function () {
                  window.frames["frame1"].focus();
                  window.frames["frame1"].print();
                  document.body.removeChild(frame1);
              }, 500);
              return false;
          }
      </script>

  </body>
</html>