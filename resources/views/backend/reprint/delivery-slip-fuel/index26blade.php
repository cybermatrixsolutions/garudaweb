@extends('backend-includes.app')
@section('content')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Delivery Slip Fuel</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#"> Delivery Slip Fuel</a></li>
        </ul>
      </div>
    </div>

    <div  style="margin-top: 41px;">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

      <div class="card" style="padding:10px!important;">
          <form class="form-horizontal" action="" method="GET">
				<div class="form-group row">
					<div class="col-md-3">
						<label>Select Transaction Date</label>
					</div>
					<div class="col-md-3">
						<input type="text" class="form-control datepicker" name="date"/>
					</div>
					<div class="col-md-3">
						<input type="submit" class="btn btn-primary" value="Go">
					</div>
					
				</div>
          </form>
		  
		  @if($transactionList)
			<div class="table-responsive">
			  <table class="table table-striped" id="myTable">
				<thead>
					<th>Invoice No.</th>
					<th>Invoice Date.</th>
					<th>Actions</th>
				</thead>
				<tbody>
				   @foreach($transactionList as $row)
					  <tr>
						<td>{{$row->invoice_number}}</td>
						<td>{{date("d/m/Y",strtotime($row->trans_date))}}</td>
						<td>
							<form action="{{route('reprint.delivery-slip-fuel-print')}}" method="POST" target="_blank">
							{{ csrf_field() }}
							<input type="hidden" name="id" value="{{$row->id}}"/>
							<button type="submit" class="btn btn-primary">Print</button>
							</form
						</td>
					  </tr>
				  @endforeach
				</tbody>
			  </table>
			</div>
			@endif
      </div>
  </div>
@endsection
@section('script')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
		$('.datepicker').datepicker("setDate","<?php if(isset($urlVars['date'])){ echo $urlVars['date'];}?>");
    });
</script>
@endsection
