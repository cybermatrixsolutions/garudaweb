<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSS-->
<!-- Font-icon css-->
<style type="text/css"  media="print">
  

    @page  
    {
         size: A7;
		
	}

</style>
<style type="text/css" media="print">
@page  {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>
<style type="text/css">
@media  print {
    #myPrntbtn {
        display :  none;
    }
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
   
   padding: 0px;
   margin: 0px;
    vertical-align: top;
    border-top: transparent;
}

  .table{
     border-collapse: collapse;
	 font-family: arial, sans-serif;
    height: auto;
    width: 148.5mm;
    margin: 0 auto;
    margin-top: 60px;
    position: relative;
   
    border-bottom:1px solid #ccc !important;
  }



hr{
  border-top: 1px solid #1d0d0d;
  width: 100%;
}
.mytdClass{

	text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 600;
}
.mythtdClass{

	text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;
}
</style>
</head>
	<body class="table">


		
		<table width="100%" style=" border: 1px solid;">
			<tr>
				<td>
					<table width="100%" style="">
						 <tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 10px;padding-bottom: 0px;padding-left: 10px;"><img src="{{URL::asset('')}}{{$Principal->image}}" onerror="this.src='../images/logo/garruda.jpg';" alt="garruda" title="garruda" width="70" height="50px" ></img></img></p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;"><p>{{$roInfo->pump_legal_name}}</p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 100;text-transform: uppercase;"><p>{{$roInfo->pump_address}}   </p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>{{$roInfo->getcity->name}}, {{$roInfo->getstate->name}}-{{$roInfo->pin_code}}</p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 100;text-transform: uppercase;"><p>gst/uin : {{$roInfo->GST_TIN}}</p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 100;text-transform: uppercase;"><p>vat tin no : {{$roInfo->VAT_TIN}}</p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>State Name : {{$roInfo->getstate->name}}, Code : {{$roInfo->getstate->gstcode}}</p></th>
						</tr>
						<tr>
						   <th colspan="6" align="left" style="text-align: center;padding-top: 0px;padding-bottom: 5px;padding-left: 10px;font-size: 12px;font-weight: 700;"><p>Duplicate GST Invoice</p></th>
						</tr>
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%" style="">
						<tr>


							<th colspan="6" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>Invoice No. : {{$transactionInfo->invoice_number}}<span style="float: right;padding-right:10px;"> Dt:{{date('d-m-Y', strtotime($transactionInfo->trans_date))}}</span></p></th>
						</tr>

						<tr><td colspan="6"><hr></td></tr>
					
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
						<table width="100%">	
						<tr>
							<td  width="" colspan="6"style="font-size: 12px;padding-left: 10px;text-transform:upercase;font-weight:bold;">
								@if($transactionInfo->customer_code!="Walkin")
				                    {{$transactionInfo->getCustomerMaster->company_name}}
				                  @else 
				                Walkin @endif


								
							</td> 
							
						</tr>
						<tr>
							
							<th colspan="6" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p> 
								@if($transactionInfo->customer_code!="Walkin")

							 {{$transactionInfo->getCustomerMaster->address_one}}, {{$transactionInfo->getCustomerMaster->address_two}}, 
								{{$transactionInfo->getCustomerMaster->getcity->name}},{{$transactionInfo->getCustomerMaster->getstate->name}} -{{$transactionInfo->getCustomerMaster->pin_no}}. State Code :{{$transactionInfo->getCustomerMaster->getstate->gstcode}}</p></th>

								  @else 
				                {{$transactionInfo->address}},
				                {{$transactionInfo->city}},
				                {{$transactionInfo->state}},
				                {{$transactionInfo->pincode}}
				                 @endif
						</tr>
						
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;text-transform:upercase;">GSTIN<span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;text-align:left;text-transform:upercase;">
								@if($transactionInfo->customer_code!="Walkin")
								{{$transactionInfo->getCustomerMaster->gst_no}}</td>
								 @else 

								  {{$transactionInfo->Cust_GST }}
				                 
				                @endif
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;text-transform:upercase;">PAN<span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;text-transform:upercase;">
								@if($transactionInfo->customer_code!="Walkin")
								{{$transactionInfo->getCustomerMaster->pan_no}}
								@else 
				                  {{$transactionInfo->pan }}
				                @endif
							</td>
						</tr>
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>
					
			<tr>
				<td>
					
		
					<table width="100%">	
						
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Vehicle No <span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;">
									@if($transactionInfo->customer_code!="Walkin")
								{{$transactionInfo->Vehicle_Reg_No}}
								@else 
				                   {{$transactionInfo->Vehicle_Reg_No}}
				                @endif

							</td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Driver <span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;">
						
								@if(!empty($transactionInfo->getdrivercustomer->Driver_Name)) {{$transactionInfo->getdrivercustomer->Driver_Name}} @endif
                            

							</td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Mobile No<span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;">
								
								  {{$transactionInfo->Driver_Mobile}}
								 
							</td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Req No. <span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;">{{$transactionInfo->Request_id}} <span>Dt:{{date('d-m-Y', strtotime($transactionInfo->trans_date))}}</span></td>
						</tr>
						<tr>
							<td  width="30%" style="font-size: 12px;padding-left: 10px;">Sales Agent <span style="float:right;">:</span></td> 
							<td width="70%" style="font-size: 12px;padding-left: 10px;">

							

								@if(!empty($transactionInfo->getTrangections->Personnel_Name)){{$transactionInfo->getTrangections->Personnel_Name}} @endif

								
							</td>
						</tr>
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>
			
			

			<tr>

				<td>
					<table width="100%">	
                        <p style="font-size: 12px"><strong></strong></p>

						<tr>
							<th colspan="2" align="left" class="mytdClass">Product Delivered</th>
							<th colspan="2" align="left" class="mytdClass">(HSN/SAC)</th>
							<th colspan="2" align="left"class="mytdClass">Qty.</th>
							<th colspan="2" align="left" class="mytdClass">Rate</th>
							<th class="mytdClass" style="text-align: right!important;padding-left:">Amount</th>
						</tr>
                 <?php $delTotalAmount=0;
                 $totalRoundoff=0; ?>


				@foreach($transactionDetails as $transaction)

                <?php 
                $delTotalAmount=$delTotalAmount+$transaction->taxable_value;

                $totalRoundoff=$totalRoundoff+$transaction->invoice_roundoff;

                 ?>
						<tr style="font-weight:bold">
							<th colspan="2" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>{{$transaction->getItemName->Item_Name}} </p></th>
							<th colspan="2" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p> ({{$transaction->getItemName->hsncode}})</p></th>
							<th colspan="2" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>{{$transaction->petroldiesel_qty}}</p></th>
							<th colspan="2" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>{{$transaction->taxbefore_price}}</p></th>
							<th colspan="2" align="left" style="text-align: right;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>{{$transaction->taxable_value}}</p></th>
						</tr>

						@endforeach
				    <tr class="mytdClass"> 
				    	<th colspan="6"></th>
				    	<th colspan="6" style="text-align: right!important;padding-left:">Total : {{bcadd($delTotalAmount,0,2)}}</th>
				    </tr>
				    @if($totalCgst>0)
				    <tr class="mytdClass"> 
				    	<th colspan="6"></th>
				    	<th colspan="6" style="text-align: right!important">CGST : {{bcadd($totalCgst,0,2)}}</th>
				    </tr>
				    <tr class="mytdClass" > 
				    	<th colspan="6"></th>
				    	<th colspan="6" style="text-align: right!important">SGST : {{bcadd($totalSgst,0,2)}}</th>
				    </tr>
				    @endif
				    @if($totalIgst>0)
				    <tr class="mytdClass"> 
				    	<th colspan="6"></th>
				    	<th colspan="6" style="text-align: right!important">IGST : {{bcadd($totalIgst,0,2)}}</th>
				    </tr>
				    @endif
				    <tr class="mytdClass"> 
				    	<th colspan="6"></th>
				    	<th colspan="6" style="text-align: right!important">R/Off : {{$transaction->invoice_roundoff}}</th>
				    </tr>
				    <tr class="mytdClass" > 
				    	<th colspan="6"></th>
				    	<th colspan="6" style="margin-top: 10px!important;text-align: right!important">Due Amount : {{bcadd($transaction->invoice_amount,0,2)}}</th>
				    </tr>
						
					</table>
				</td>
			</tr>

			<tr>

				<td>
					<table width="100%">	
                      <tr><td colspan="12"><hr></td></tr>

						<tr>
							<th  align="left"  class="mytdClass">(HSN/SAC)</th>
							<th align="left" class="mytdClass">(%)</th>
							<th align="left" class="mytdClass"><p>Amt</p></th>
							
							<th class="mytdClass">CGST</th>
							<th align="left" class="mytdClass">SGST</th>
							<th class="mytdClass">IGST</th>
							<th class="mytdClass"">CESS</th>
							<th  align="left" class="mytdClass" style="text-align: right!important;padding-left:">Total</th>
						</tr>
						<?php
                     $totalCgstFinal=0;
				     $totalSgstFinal=0;
				     $totalIgstFinal=0;
				  
				     $hsnamountFinal=0;
				     $hsnamount_taxFinal=0;
				     ?>
					@foreach($transactionHsnWise as $key=> $transactionHsnWisedata)

					<?php 
				     $totalCgst=0;
				     $totalSgst=0;
				     $totalIgst=0;
				     $per=0;
				     $hsnamount=0;
				     $hsnamount_tax=0;
				      ?> 
 
							@foreach($transactionHsnWisedata as $transaction)
                           <?php 


                          $per=$per+$transaction->Tax_percentage;
					        if($transaction->GST_Type=="UT/SGST") {
                            $totalSgst=$totalSgst+$transaction->Tax_Value;
                           
                          } 

                          if($transaction->GST_Type=="CGST") {
                           $totalCgst=$totalCgst+$transaction->Tax_Value;
                            
                          } 
                          if($transaction->GST_Type=="IGST") {
                            $totalSgst=$totalIgst+$transaction->Tax_Value;
                           
                          } 
                          $hsnamount=$transaction->taxable_value;
                          $hsnamount_tax= $hsnamount+$totalSgst+$totalCgst+$totalIgst;



                          ?>

	
							@endforeach

							<?php

							   $hsnamountFinal=$hsnamountFinal+$hsnamount;
	                           $hsnamount_taxFinal=$hsnamount_taxFinal+$hsnamount_tax;
	                           $totalCgstFinal=$totalCgstFinal+$totalCgst;
	                           $totalSgstFinal=$totalSgstFinal+$totalSgst;
	                           $totalIgstFinal=$totalIgstFinal+$totalIgst;
                          
							?>

					<tr>
							<th  align="left" class="mythtdClass">{{$key}}</th>
							<th align="left" class="mythtdClass">{{bcadd($per,0,1)}}</th>
							<th align="left" class="mythtdClass"><p>{{bcadd($hsnamount,0,2)}}</p></th>
						
							<th class="mythtdClass">{{bcadd($totalCgst,0,2)}}</th>
							<th align="left" class="mythtdClass">{{bcadd($totalSgst,0,2)}}</th>
							<th class="mythtdClass">{{bcadd($totalIgst,0,2)}}</th>
							<th class="mythtdClass">0.00</th>
							<th  align="left" class="mythtdClass" style="text-align: right!important;padding-left:">{{bcadd($hsnamount_tax,0,2)}}</th>
						</tr>

					@endforeach

					<tr><td colspan="12"><hr></td></tr>

					<tr>
							<th  align="left" class="mytdClass">Total : </th>
							<th align="left" class="mytdClass"></th>
							<th align="left" class="mytdClass"><p>{{bcadd($hsnamountFinal,0,2)}}</p></th>
							
							<th class="mytdClass">{{bcadd($totalCgstFinal,0,2)}}</th>
							<th align="left" class="mytdClass">{{bcadd($totalSgstFinal,0,2)}}</th>
							<th class="mytdClass">{{bcadd($totalIgstFinal,0,2)}}</th>
							<th class="mytdClass">0.00</th>

							<th  align="left" class="mytdClass" style="text-align: right!important;padding-left:">{{bcadd($hsnamount_taxFinal,0,2)}}</th>
						</tr>
				
						
					</table>
				</td>
			</tr>
			<tr> <td >
					<table width="100%">	
							<th  align="left" class="mytdClass">Payment Mode :  {{$transactionInfo->payment_mode_name}}</th>
						</table>
				</td>
							
			</tr>
			
			
			
			<tr>
				<td>
					<table width="100%">		
						<tr>
							<!-- <th colspan="6" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100; font-weight: bold;"><p>Terms & Condition</p></th> -->
						</tr>
						<tr>
							<th colspan="6" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>
								

								{{$roInfo->getdetails->TC_for_GST_Invoice_Slip}}</p></th>
						</tr>
						<tr>
							<th colspan="6" align="left" style="text-align: left;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>{{$roInfo->getdetails->TC_for_GST_Invoice_Slip2}}</p></th>
						</tr>
			
					</table>
				</td>
			</tr>
			
			<tr>
				<!-- td>
					<table width="100%">		
						<tr>
							<th colspan="6" align="left" style="text-align: left;padding-top: 10px;padding-bottom: 10px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>Rates applicable on the date & time of delivery will be charged in the periodic bill</p></th>
						</tr>
					</table>
				</td> -->
			</tr>
			
			<tr>
				<td>
					
					<table width="100%">				
						<tr>
						   <th colspan="6" align="right" style="text-align: right;padding-top: 0px;padding-bottom: 10px;padding-left: 10px;font-size: 12px;font-weight: 100;padding-right: 10px;"><p>for {{$roInfo->pump_legal_name}}</p></th>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
					<table width="100%">		
						<tr>
							<th colspan="6" align="center" style="text-align: center;padding-top: 0px;padding-bottom: 0px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>Computer Generated Invoice</p></th>
						</tr>
						<tr><td colspan="6"><hr></td></tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
					<table width="100%">		
						<tr>
							<th colspan="6" align="center" style="text-align: center;padding-top: 0px;padding-bottom: 10px;padding-left: 10px;font-size: 12px;font-weight: 100;"><p>Powered by : www.garruda.co.in</p></th>
						</tr>
			
					</table>
				</td>
			</tr>
		</table>
        	
	
             
      
      <br>
  
  <center>
    <input type="button"  id="myPrntbtn" class="btn btn-success site-btn" onclick="window.print();" value="Print" / style="padding: 8px;padding-left: 20px;padding-right: 20px;">
  </center> 


  <script type="text/javascript">
          function PrintDiv() {

              var contents = document.getElementById("qrcodeprintContents").innerHTML;

              var frame1 = document.createElement('iframe');
              frame1.name = "frame1";
              frame1.style.position = "absolute";
              frame1.style.top = "-1000000px";
              document.body.appendChild(frame1);
              var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
              frameDoc.document.open();
              frameDoc.document.write('<html><head><title>QR Print</title>');
              frameDoc.document.write('</head><body>');
              frameDoc.document.write(contents);
              frameDoc.document.write('</body></html>');
              frameDoc.document.close();
              setTimeout(function () {
                  window.frames["frame1"].focus();
                  window.frames["frame1"].print();
                  document.body.removeChild(frame1);
              }, 500);
              return false;
          }
      </script>

  </body>
</html>