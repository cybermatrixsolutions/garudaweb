<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Customer  View</h1>
      </div>
      <div>
        <ul class="breadcrumb">
           <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/Customer')}}">Customer </a></li>
        </ul>
      </div>
    </div>
	
	
	                         <div class="">  <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                             </div>
	
	
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <form class="form-horizontal" action="{{url('customer_update')}}/{{$customer_mangement_data->id}}" enctype="multipart/form-data" method="post">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row" style="margin-top:-15px!important;">
          
         
          
                      
            
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Pump Legal Name</label>
                          <div class="col-sm-9">
                              <select class="form-control" disabled="disabled" required="required" name="customer_ro_code" id="sel1" readonly>
                              <option></option>
                                                     @foreach($datas as $ro_master)
                                                        <option name="company_id" value="{{$ro_master->RO_code}}"
                                                                @if($customer_mangement_data->RO_code == $ro_master->RO_code) selected @endif>{{$ro_master->pump_legal_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div> -->
                     <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Customer Code</label>
                          <div class="col-sm-9">
                             <input type="text" required="required" class="form-control" id="" name="Customer_Code" value="{{$customer_mangement_data->Customer_Code}}" placeholder="Customer Code" readonly>
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Customer Name</label>
                          <div class="col-sm-9">
                             <input type="text" disabled="disabled" required="required" class="form-control" id="" name="company_name" value="{{$customer_mangement_data->company_name}}"  placeholder="Customer Name">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Contact Name</label>
                          <div class="col-sm-9">
                             <input type="text" disabled="disabled" required="required" class="form-control" id="" name="coustomer_name"  value="{{$customer_mangement_data->Customer_Name}}"  placeholder="Customer Name">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Gender</label>
                          <div class="col-sm-9">
                             <input type="text" disabled="disabled" required="required" class="form-control" id="" name="coustomer_name"  value="{{$customer_mangement_data->gender}}">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Email/Login ID</label>
                          <div class="col-sm-9">
                             <input type="text" disabled="disabled" required="required" class="form-control" id="" name="coustomer_name"  value="{{$customer_mangement_data->Email}}"  placeholder="Customer Name">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Phone Number</label>
                          <div class="col-sm-9">
                             <input type="number" disabled="disabled" class="form-control" id="" name="Phone_Number" value="{{$customer_mangement_data->Phone_Number}}"  placeholder="Phone Number">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Mobile(+91)</label>
                          <div class="col-sm-9">
                             <input type="number" disabled="disabled" required="required" class="form-control" id="" name="mobile_no" value="{{$customer_mangement_data->Mobile}}"  placeholder="Mobile No.">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Customer Security Deposit</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" id="" name="imei_no" value="{{$customer_mangement_data->CustomerDeposit}}"  placeholder="" disabled="disabled">
                          </div>
                        </div>
                         <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Security Deposit(rs.)</label>
                          <div class="col-sm-9">
                             <input type="text"  disabled="disabled" class="form-control" name="Security_Deposit"  value="{{$customer_mangement_data->Security_Deposit}}" placeholder="Security Deposit">
                          </div>
                        </div> -->
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Industry Vertical</label>
                          <div class="col-sm-9">
                            <input type="text"  readonly  class="form-control" placeholder="Industry Vertical" name="industry_vertical" value="{{$customer_mangement_data->industry_vertical}}" />
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Department</label>
                          <div class="col-sm-9">
                            <input type="text"  class="form-control" placeholder="Department" readonly name="Department" value="{{$customer_mangement_data->Department}}"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Address Line 1</label>
                          <div class="col-sm-9">
                             <textarea cols="1" disabled="disabled" required="required"  class="form-control" name="address_one">{{$customer_mangement_data->address_one}}</textarea>
                          </div>
                          </div>
                         <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">IMEI No</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" id="" name="imei_no" value="{{$customer_mangement_data->IMEI_No}}"  placeholder="IMEI No" disabled="disabled">
                          </div>
                        </div> -->
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row" style="margin-top:-15px!important;">
                           
                           <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Address Line 2</label>
                          <div class="col-sm-9">
                             <textarea cols="1" disabled="disabled" class="form-control" name="address_two">{{$customer_mangement_data->address_two}}</textarea>
                          </div>
                          </div>
                           <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Address Line 3</label>
                          <div class="col-sm-9">
                             <textarea cols="1" disabled="disabled" class="form-control" name="address_three">{{$customer_mangement_data->address_three}}</textarea>
                          </div>
                          </div>
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Country</label>
                          <div class="col-sm-9">
                             <select class="form-control" disabled="disabled" id="country" name="country">
                               @foreach($countries_list as $country)
                                <option  value="{{$country->id}}"
                                        @if($customer_mangement_data->country==$country->id) selected @endif >{{$country->name}}</option>
                                   @endforeach
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">State</label>
                          <div class="col-sm-9">
                             <select class="form-control" disabled="disabled" id="state" name="state">
                                @foreach($state_list as $state)
                                <option  value="{{$state->id}}"
                                        @if($customer_mangement_data->state==$state->id) selected @endif >{{$state->name}}</option>
                                   @endforeach
                             </select>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">City</label>
                          <div class="col-sm-9">
                             <select class="form-control" disabled="disabled" id="city" name="city">
                                @foreach($city_list as $city)
                                <option  value="{{$city->id}}"
                                        @if($customer_mangement_data->city==$city->id) selected @endif >{{$city->name}}</option>
                                   @endforeach
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">PIN Code</label>
                          <div class="col-sm-9">
                             <input type="text" disabled="disabled" required="required" value="{{$customer_mangement_data->pin_no}}"  pattern="[0-9]{6}" class="form-control" name="pin_no" id="" placeholder="PIN">
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="">Aadhaar No</label>
                          <div class="col-sm-9">
                              <input type="text" disabled="disabled" class="form-control" name="aadhaar_no" id="aadhaar_no" value="{{$customer_mangement_data->Aadhaar_no}}"  placeholder="Aadhaar No">
                          </div>
                          </div>
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Aadhaar Photo </label>
                          <div class="col-sm-9">
                            
                            <br><img src="{{URL::asset('')}}/{{$customer_mangement_data->Aadhaar_img}}" style="width: 250px; height: 150px;">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">VAT</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control" name="vat" disabled="disabled" value="{{$customer_mangement_data->vat}}" placeholder="vat">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">PAN</label>
                          <div class="col-sm-9">
                             <input type="text" required="required" disabled="disabled" value="{{$customer_mangement_data->pan_no}}" max="10" pattern="[A-Z]{5}\d{4}[A-Z]{1}" class="form-control" name="pan_no" id="pan" placeholder="PIN">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">GSTIN</label>
                          <div class="col-sm-9">
                             <input type="text" required="required" disabled="disabled" pattern="[0-9]{2}[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}\d{1}[(a-z)(A-Z)]{1}\d{1}" value="{{$customer_mangement_data->gst_no}}" class="form-control" name="gst_no" id="gst" placeholder="GST No ">
                          </div>
                        </div>
                         
                          
                        
           
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>

    </div>
	
	<div id="gethomeurl" data-url="{{url('/')}}">
  </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
             // vil.getcom();

           },
           selectRo:function(){
            jQuery('#country').on('change',function(){
                  vil.getcom();
              });
            jQuery('#state').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
               var url=jQuery('#gethomeurl').data('url');
               jQuery.get(url+'/get_country_state',{
                
                rocode:jQuery('#country').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                 

                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#state').html(opt);

                  vil.getcrg();
               });
           },
           getcrg:function(){
            var url=jQuery('#gethomeurl').data('url');
             jQuery.get(url+'/get_state_city',{
                rocode:jQuery('#state').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';

                   if(data.length==0)
                   {

                     opt+='<option value="'+jQuery('#state option:selected').val()+'">'+jQuery('#state option:selected').text()+'</option>';
                   }
                   else{


                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   }
                   console.log();
                jQuery('#city').html(opt);

               });
           },

     }
     vil.init();
  });
</script>
</body>
</html>