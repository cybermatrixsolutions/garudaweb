<!DOCTYPE html> 
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Stock Item View</h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/price_list')}}">Stock Item Master</a></li>
        </ul>
      </div>
    </div>
	
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
      
   
	
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
          <form class="form-horizontal" action="{{url('price_update')}}/{{$price_list->id}}" enctype="multipart/form-data" method="post">
            
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
     
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Pump Legal Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" disabled="disabled" name="price_ro_code" id="sel1">
                              @foreach($datas as $ro_master)
                                                    
                              <option name="company_id" value="{{$ro_master->RO_code}}"
                             @if($price_list->RO_Code == $ro_master->RO_code) selected @endif>{{$ro_master->pump_legal_name}}</option>
                             @endforeach
                            </select>
                          </div>
                        </div> -->
                        
                        <div class="form-group">
                        <label class="control-label col-sm-3" for="pwd">Item Code</label>
                        <div class="col-sm-9">
                           <input type="text" disabled="disabled" required="required" class="form-control " id="" name="price_item_list" value="{{$price_list->Item_Code}}" placeholder="Volume Liter">

                         
                        </div>
                      </div>
                       
                       
                     
                      <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Item Name</label>
                        <div class="col-sm-9">
                           <input type="text" disabled="disabled" required="required" class="form-control" id="" name="item_name" value="{{$price_list->Item_Name}}" placeholder="Item Name">
                        </div>
                      </div>

                      <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Stock Group</label>
                          <div class="col-sm-9">
                            <select class="form-control" disabled="disabled" required="required" name="Stock_Group" id="sel1">
                             
                                                    @foreach($stock_group as $stock)
                               <option  value="{{$stock->id}}" @if($price_list->Stock_Group==$stock->id) selected @endif >{{$stock->Group_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Sub  Group</label>
                          <div class="col-sm-9">
                            <select class="form-control" disabled="disabled" required="required" name="Stock_Group" id="sel1">
                             
                                                    @foreach($stock_group as $stock)
                               <option  value="{{$stock->id}}" @if($price_list->Sub_Stock_Group==$stock->id) selected @endif >{{$stock->Group_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Unit of Measure</label>
                          <div class="col-sm-9">
                                <select class="form-control" disabled="disabled" required="required" name="Unit_of_Measure" id="Unit_of_Measure">
                             
                                                    @foreach($unit_measure as $unit)
                                                        <option  value="{{$unit->id}}"
                                                                @if($price_list->Unit_of_Measure==$unit->id) selected @endif  >{{$unit->Unit_Symbol}}-{{$unit->Unit_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                       
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Tail Unit</label>
                          <div class="col-sm-9">
                             <input type="number" disabled="disabled"  class="form-control" value="{{$price_list->Tail_Unit}}" required="required" id="" name="Tail_Unit" placeholder="Tail Unit">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Conversion</label>
                          <div class="col-sm-9">
                             <input type="number" disabled="disabled"   class="form-control" value="{{$price_list->Conversion}}" required="required" name="Conversion" id="" placeholder="Conversion">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Alternate Unit</label>
                          <div class="col-sm-9">
                             <input type="number" disabled="disabled"  class="form-control" value="{{$price_list->Alternate_Unit}}"  required="required" name="Alternate_Unit" id="" placeholder="Alternate Unit">
                          </div>
                        </div>
                      </div>
                    
                      <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
      
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">LOB</label>
                          <div class="col-sm-9">
                             <input type="text"  disabled="disabled" value="{{$price_list->LOB}}" class="form-control" required="required" name="LOB" id="" placeholder="lob">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Brand</label>
                          <div class="col-sm-9">
                             <input type="text"  disabled="disabled" value="{{$price_list->brand}}" class="form-control" required="required" name="brand" id="" placeholder="brand">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">HSN/SAC</label>  
                          <div class="col-sm-9">
                             <input type="text"  disabled="disabled" value="{{$price_list->hsncode}}" class="form-control" required="required" name="brand" id="" placeholder="hsncode">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">GST/VAT</label>
                          <div class="col-sm-9">
                             <input type="text"  disabled="disabled" value="{{$price_list->gst_vat}}" class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Tax</label>
                          <div class="col-sm-9">
                            <select class="form-control" id="tags" readonly multiple="multiple" required="required" name="tax[]">
                             
                            @foreach($rod as $taxa)
                                <option value="">{{$taxa->Tax_Code}}</option>
                            @endforeach
                            </select>
                          </div>
                        </div>
                         <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Open Stock</label>
                        <div class="col-sm-9">
                           <input type="number" disabled="disabled" required="required" class="form-control " id="" name="Open Stock" value="{{$price_list->Volume_ltr}}" placeholder="Volume Liter">
                        </div>
                      </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Active From Date</label>
                          <div class="col-sm-9">
                             <input type="text" disabled="disabled" required="required" class="form-control datepicker" name="Active_From_Date" id="" placeholder="Active From Date" value="{{Carbon\Carbon::parse($price_list->Active_From_Date)->format('d/m/Y')}}">
                          </div>
                        </div>
                        
                      </form>
                    </div>
          </div>
        </div>
      </div>

    </div>
	</div>
	
  </div>
  <footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

<script>
  $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>