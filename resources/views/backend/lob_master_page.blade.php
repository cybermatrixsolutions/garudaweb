<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
   @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
  <!-- Side-Nav-->
  
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;LOB </h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">LOB </a></li>
        </ul>
      </div>
    </div>
  
                          <div class="">
                   
                            </div>
  
  
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
            <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!--  <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div> -->
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Add LOB </h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
          
          
                      <form class="form-horizontal" id="form1"  action="{{'save_lob_data'}}" enctype="multipart/form-data" method="post">
               <input type="hidden" name="_token" value="{{ csrf_token()}}"/> 
            
                        <div class="form-group">
            
                          <label class="control-label col-sm-3" for="email">LOB Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                           <input type="text" class="form-control "  data-table="tbl_lob_master" data-colum="lob_name" data-mass="LOB Name Already Exist" style="text-transform: uppercase;" id="checkunique" name="lob_name" placeholder="LOB Name" required/>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <!--<button type="submit" class="btn btn-default">SUBMIT</button>-->
              <center><input type="button" id="success-btn" class="btn btn-primary site-btn submit-button" value="Submit"></center>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
             @if(isset($lob_list_master) && count($lob_list_master) > 0)
            <thead>
              <tr>
                <th>S.No</th>
                <th>LOB Name</th>
               <!--  <th>Created Date</th> -->
                <th>Status</th>
               <!--  <th>Actions</th> -->
              </tr>
            </thead>
            <tbody>
      <?php $i=0; ?>
      
              @foreach($lob_list_master as $row) 
        
           <?php $i++ ;?>
      
            <tr>
          <td>{{$i}}</td>
                <td scope="row">{{$row->lob_name}}</td>
               <!--  <td>{{Carbon\Carbon::parse($row->created_at)->format('d/m/Y ')}}</td> -->
         <td> @if($row->lob_name!=='LUBES' && $row->lob_name!=='FUELS')<?php if($row->is_active==1){?>

                   <a  title="Active" href="lob_master/active/{{$row->id}}/0" onclick="return confirm('Do you want to deactivate?');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="lob_master/active/{{$row->id}}/1" onclick="return confirm('Do you want to activate?');">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
             @endif
          </td>
            </tr>
        
        @endforeach
        @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
@if(isset($lob_list_master) && count($lob_list_master) > 0)
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
@endif
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('#Group_Code').on('blur',function(){
                
                  udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique',{
                  table:'tbl_stock_item_group',
                  colum:'Group_Code',
                  unik:udat,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                    console.log(data);
                   if(data=='true'){
                      alert('Group Code already exists Enter Other!');
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });



</script>
<script type="text/javascript">
 jQuery(function(){

  var url1="{{url('check_Pedestal_Number_unique')}}";

     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('#success-btn').on('click',function(){

                    
                  var id=0;
                  var table=jQuery('#checkunique').data('table');
                  var mass=jQuery('#checkunique').data('mass');
                   var colum=jQuery('#checkunique').data('colum');
                   id=jQuery('#checkunique').data('id');
                    
                 var  udat=  jQuery('#checkunique').val().toUpperCase();
                  
                  if(jQuery.trim(udat)==''){
                    alert('Lob name required!');
                     return false;
                  } 

                  jQuery.get(url1,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                  
                      alert(mass);
                      
                    

                   }else{
                    console.log('hh');
                    jQuery('#form1').submit();
                    
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>