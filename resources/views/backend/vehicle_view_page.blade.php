<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')  
  
 
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Vehicle  View</h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
<li><a href="{{url('/vehiclemanagement')}}
">Vehicle </a></li>
        </ul>
      </div>
    </div>
    <br>
    <br>
     <div class="">
                             <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                            </div>  
                             @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
     <div class="row">
           <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <form class="form-horizontal" action="{{url('/vehiclemanagement/update/'.$VechilManage->id)}}" enctype="multipart/form-data" method="post">
              {{ csrf_field() }}
                        
                         <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Outlet Name</label>
                          <div class="col-sm-9">
                            
                            <select class="form-control" disabled="disabled" required="required" id="sel1" name="Ro_code">
                              @foreach($data1 as $row) 
                              <option value="{{$row->RO_code}}" @if($VechilManage->Ro_code == $row->RO_code) selectd @endif >{{$row->pump_legal_name}}</option>
                              
                               @endforeach
                            </select>
                          </div>
                        </div> -->
                        <input type="hidden" name="Ro_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Customer Name</label>
                          <div class="col-sm-9">
                              <select class="form-control" data-value="{{$VechilManage->Customer_code}}" disabled="disabled" required="required" id="customer_code" name="Customer_code">
                                </select>
                           
                          </div>
                        </div>
                       <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Customer code</label>
                         <div class="col-sm-9">
                            <input type="text" class="form-control" id="" name="Customer_code" value="{{$VechilManage->Customer_code}}" placeholder="" readonly>
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Registration Number</label>
                          <div class="col-sm-9">
                            <input type="text" disabled="disabled" class="form-control checkunique" data-mass="Registration Number Already Exist" data-table="tbl_customer_vehicle_master"  data-colum="Registration_Number" id="" name="Registration_Number" value="{{$VechilManage->Registration_Number}}" placeholder="">
                          </div>
                         
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Only Pre-Authorised</label>
                          <div class="col-sm-9">
                             <input type="checkbox" disabled="disabled" @if($VechilManage->pre_authori==1 ) checked @endif name="pre_authori" value="1"><label>Pre-Authorised</label>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Fuel Type</label>
                          <div class="col-sm-9">
                             <select class="form-control" disabled="disabled" name="Fuel_Type">
                               @foreach($fuel_type as $fuel) 
                              <option value="{{$fuel->id}}" @if($VechilManage->Fuel_Type == $fuel->id) selected @endif >{{$fuel->Item_Name}}</option>
                              
                               @endforeach
                               </select>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">RC Valid Upto</label>
                          <div class="col-sm-9">
                             <input type="text" disabled="disabled" @if($VechilManage->rc_valide!=null) value="{{Carbon\Carbon::parse($VechilManage->rc_valide)->format('d/m/Y')}}"@endif  class="form-control datepicker" id="" placeholder="Enter Date" name="insurance_due_date">
                          </div>
                        </div> 
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Insurance due date</label>
                          <div class="col-sm-9">
                             <input type="text" disabled="disabled" required="required" @if($VechilManage->insurance_due_date!=null) value="{{Carbon\Carbon::parse($VechilManage->insurance_due_date)->format('d/m/Y')}}" @endif class="form-control datepicker" id="" placeholder="Enter Date" name="insurance_due_date">
                          </div>
                        </div> 
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">PUC Date</label>
                          <div class="col-sm-9">
                             <input type="text" disabled="disabled" class="form-control datepicker" @if($VechilManage->puc_date!=null) value="{{Carbon\Carbon::parse($VechilManage->puc_date)->format('d/m/Y')}}"  @endif id="" placeholder="PUC Date" name="puc_date">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Make</label>
                          <div class="col-sm-9">
                            <input type="text" disabled="disabled" class="form-control" id="" name="Make" value="{{$VehicleMakeModel->name}}" placeholder="Make">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Model</label>
                          <div class="col-sm-9">
                            <input type="text" disabled="disabled" class="form-control" id="" name="Model" value="{{$VehicleModel->name}}" placeholder="Opening Reading">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Colour</label>
                          <div class="col-sm-9">
                             <input type="text" disabled="disabled" class="form-control datepicker"  value="{{$VechilManage->van_color}}" id="" placeholder="Van Color" name="van_color">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="Capacity">Capacity</label>
                          <div class="col-sm-9">
                             <input type="number" disabled="disabled" required="required" class="form-control" value="{{$VechilManage->capacity}}" placeholder="Capacity" name="capacity">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Insurance Company<span style="color:red;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control"  disabled name="industry_company" id="industry">
                              @foreach($IndustryDepartment as $IndustryDepartments)
                               <option value="{{$IndustryDepartments->value}}" @if($IndustryDepartments->value == $VechilManage->industry_company) selected @endif>{{$IndustryDepartments->value}}</option>
                                   @endforeach
                             
                                            </select>
                           
                          </div>
                        </div>
                      </form>
                      
                    </div>
                  </div>
    </div>
	
	
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script type="text/javascript">
  jQuery(function(){
        var url1="{{url('getrocode')}}";
        var url2="{{url('getregierids')}}";
     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
               jQuery.get(url1,{
                rocode:jQuery('#sel1').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                var d=jQuery('#customer_code').data('value');
                  jQuery.each(data, function(index,value){
                    
                    if(d==index){
                      opt+='<option selected value="'+index+'">'+value+'</option>';
                    }else{
                      opt+='<option value="'+index+'">'+value+'</option>';
                    }
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);
                   vil.getcrg();
               });
           },
           getcrg:function(){

             jQuery.get(url2,{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                jQuery('#Registration_Number').val(data);
               });
           },

     }
     vil.init();
  });
</script>
<script type="text/javascript">
 jQuery(function(){
          
        var url2="{{url('check_Pedestal_Number_unique')}}";
     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   //id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get(url2,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>
</body>
</html>