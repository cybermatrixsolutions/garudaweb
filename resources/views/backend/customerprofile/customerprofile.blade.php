<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/animate.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">

      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Customer Info Update</h1>

      </div>
       <div class="">  <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                             </div>
      <div>
        <ul class="breadcrumb">
           <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/Customer')}}">Customer Info Update</a></li>
        </ul>
      </div>
    </div>
	
	
	                        
	
	
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12 vat">
            <div class="card animated  fadeInLeftBig">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row" style="margin-top:-10px !important;">
          
         
          
                      <form class="form-horizontal" action="{{url('customerprofileupdate')}}/{{$customer_mangement_data->id}}" enctype="multipart/form-data" method="post">
            
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Company Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <input type="text" required="required" readonly class="form-control" id="" name="company_name" value="{{$customer_mangement_data->company_name}}"  placeholder="Customer Name">
                             <input type="hidden" name="RO_code" value="{{$customer_mangement_data->RO_code}}">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Contact Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <input type="text" required="required" class="form-control" id="" name="coustomer_name"  value="{{$customer_mangement_data->Customer_Name}}"  placeholder="Customer Name">
                          </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-4" for="pwd">Gender <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <select class="form-control" required="required" name="gender">
                             <option value="Male"  @if($customer_mangement_data->gender == "Male") selected @endif>Male</option>
                             <option value="Female" @if($customer_mangement_data->gender == "Female") selected @endif>Female</option>
                           </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Email/Login ID <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <input type="email" class="form-control" id="" required="required" name="email" value="{{$customer_mangement_data->Email}}"  placeholder="Email">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Phone Number</label>
                          <div class="col-sm-8">
                             <input type="number" class="form-control" id="" name="Phone_Number" value="{{$customer_mangement_data->Phone_Number}}"  placeholder="Phone Number">
                          </div>
                        </div>
                       
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Mobile(+91) <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <input type="text" required="required" readonly class="form-control number" id="mobile" name="mobile_no" value="{{$customer_mangement_data->Mobile}}"  maxlength="10"  placeholder="Mobile No.">
                          </div>
                        </div>
                        
                      
                             <input type="hidden" class="form-control" id="" name="CustomerDeposit" value="{{$customer_mangement_data->CustomerDeposit}}"  placeholder="">
                         
                        
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">VAT</label>
                          <div class="col-sm-8">
                             <input type="text"  class="form-control" name="vat"  value="{{$customer_mangement_data->vat}}" placeholder="vat">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="email">PAN </label>
                          <div class="col-sm-8">
                             <input type="text" id=""  style="text-transform: uppercase;"  value="{{$customer_mangement_data->pan_no}}"  pattern="[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}" class="form-control" name="pan_no"  placeholder="PAN">
                          </div>
                        </div>
                         
                          <div class="form-group">
                          <label class="control-label col-sm-4" for="email">GSTIN</label>
                          <div class="col-sm-8">
                             <input type="text" id=""  style="text-transform: uppercase;" value="{{$customer_mangement_data->gst_no}}"  pattern="[0-9]{2}[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}[(0-9)(a-z)(A-Z)]{3}" class="form-control" name="gst_no"  placeholder="GST No. ">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Address Line 1 <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <textarea cols="1" required="required"  class="form-control" name="address_one">{{$customer_mangement_data->address_one}}</textarea>
                          </div>
                          </div>
                      </div>
                          <div class="col-md-6">
                          
                           <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Address Line 2</label>
                          <div class="col-sm-8">
                             <textarea cols="1" class="form-control" name="address_two">{{$customer_mangement_data->address_two}}</textarea>
                          </div>
                          </div>
                           <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Address Line 3</label>
                          <div class="col-sm-8">
                             <textarea cols="1" class="form-control" name="address_three">{{$customer_mangement_data->address_three}}</textarea>
                          </div>
                          </div>
                          <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Country</label>
                          <div class="col-sm-8">
                             <select class="form-control" disabled="disabled" id="country" name="country">
                               @foreach($countries_list as $country)
                                <option  value="{{$country->id}}"
                                        @if($customer_mangement_data->country==$country->id) selected @endif >{{$country->name}}</option>
                                   @endforeach
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">State <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <select class="form-control" id="state" name="state" data-v="{{$customer_mangement_data->state}}">
                                @foreach($state_list as $state)
                                <option  value="{{$state->id}}"
                                        @if($customer_mangement_data->state==$state->id) selected @endif >{{$state->name}}</option>
                                   @endforeach
                             </select>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">City</label>
                          <div class="col-sm-8">
                             <select class="form-control" id="city" name="city" data-v="{{$customer_mangement_data->city}}">
                                @foreach($city_list as $city)
                                <option  value="{{$city->id}}"
                                        @if($customer_mangement_data->city==$city->id) selected @endif >{{$city->name}}</option>
                                   @endforeach
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">PIN Code <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <input type="text" required="required" value="{{$customer_mangement_data->pin_no}}"  pattern="[0-9]{6}" class="form-control" name="pin_no" id="" placeholder="PIN Code">
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Industry<span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <select class="form-control"  name="industry_vertical" id="industry">
                            @foreach($Industry as $Industrys)
                              <option value="{{$Industrys->value}}" @if($Industrys->value==$customer_mangement_data->industry_vertical) selected @endif>{{$Industrys->value}}</option>
                             @endforeach          
                             </select>
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Department<span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <select class="form-control"  name="Department">
                               @foreach($Department as $Departments)
                              <option value="{{$Departments->value}}" @if($Departments->value==$customer_mangement_data->Department) selected @endif>{{$Departments->value}}</option>
                             @endforeach
                                    
                                    
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
            
                          <div class="col-md-offset-4 col-sm-8">
                           <!-- <button type="submit" class="btn btn-default">Submit</button>-->
              <input type="submit" id="success-btn" class="btn btn-primary site-btn" value="Submit"  onclick="return validmobile()">
                          </div>
                        </div>
                        
                         </div>

                       </div>
                          
                        
           </div>
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>

    </div>
	
	<div id="gethomeurl" data-url="{{url('/')}}">
  </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script>
  function  validmobile() {
        var mobile = document.getElementById("mobile").value;
        var pattern = /^[0]?[789]\d{9}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
            return true;
        }
        alert("It is not valid mobile number.input 10 digits number!");
        return false;
    }
  $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
  $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})

</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
               vil.getvertical();


               jQuery('form').on('submit',function(){
                  
                  if($("#gst").val() == '') {
                    
                    return true;
                  }

                  else{

                 var gst=jQuery('#gst').val();
                   var gst=gst.toUpperCase();
                   var pan=jQuery("#pan").val();
                   var pan=pan.toUpperCase();
                   gst= gst.substring(2,12);

                   if(gst!=pan){
                     alert('Please Enter valid GST');
                      jQuery("#success-btn").attr('disabled',false);

                      return false;
                   }

                  }
                   

                   

              });

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getvertical:function(){
             
               jQuery.get("{{url('get_vertical')}}",{
                
                industry:jQuery('#industry').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                 
                  var selc= jQuery('#departement').data('dep');
                  jQuery.each(data, function(index,value){
                    
                    if(selc==index)
                     opt+='<option selected value="'+index+'">'+value+'</option>';
                   else
                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#departement').html(opt);
                  
               });
           },
           getcom:function(){
             
               jQuery.post("{{url('getrocode')}}",{
                
                rocode:jQuery('#sel1').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get("url('getregierids')",{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
               });
           },

     }
     vil.init();
  });
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#country').on('change',function(){
                  vil.getcom();
              });
            jQuery('#state').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get("{{url('get_country_state')}}",{
                
                rocode:jQuery('#country').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                 
                  var selc=jQuery('#state').data('v');
                  jQuery.each(data, function(index,value){

                    if(selc==index)
                     opt+='<option selected value="'+index+'">'+value+'</option>';
                    else
                      opt+='<option value="'+index+'">'+value+'</option>';

                  });
                    console.log(opt);
                   
                   jQuery('#state').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get("{{url('get_state_city')}}",{
                rocode:jQuery('#state').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                 var selc=jQuery('#city').data('v');
                 var opt='';

                   if(data.length==0)
                   {

                     opt+='<option value="'+jQuery('#state option:selected').val()+'">'+jQuery('#state option:selected').text()+'</option>';
                   }
                   else{


                  jQuery.each(data, function(index,value){

                     if(selc==index)
                     opt+='<option selected value="'+index+'">'+value+'</option>';
                    else
                      opt+='<option value="'+index+'">'+value+'</option>';

                  });
                    console.log(opt);
                   }
                jQuery('#city').html(opt);

               });
           },

     }
     vil.init();
  });
</script>
</body>
</html>