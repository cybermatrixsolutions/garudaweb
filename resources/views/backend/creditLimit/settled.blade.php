@extends('backend-includes.app')

@section('content')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Settled</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('invoicePendingCollection')}}"> Bills Settlement </a></li>
        </ul>
      </div>

    </div>
    <div class="">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
  
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">

            <div class="">
                  
                  <div class="modal-body">
                    <div class="row">
                     <form class="form-horizontal" id="form1"  action="{{url('/bill/settled/'.$invoiceCustomer->id)}}" enctype="multipart/form-data" method="post"  onsubmit="return confirm('Do you want to Continue?');">
                          {{ csrf_field() }}
                          <div class="form-group">
                            <label class="control-label col-sm-3" for="Payment Mode">Payment Mode</label>
                            <div class="col-sm-9">
                            <select name="payment_mode" id="payment_mode" class="form-control">
                              
                               <option value=" Credit Card">  Credit Card </option>
                               <option value="Cash"> Cash </option>
                               <option value="Cheque"> Cheque </option>
                               <option value="NEFT"> NEFT </option>
                            </select>
                            </div>
                         </div>
                         <div class="form-group"  style="display:none" id="bank_name">
                          <label class="control-label col-sm-3" for="Payment Date "> Bank Name </label>
                          <div class="col-sm-9">
                              
                            <input type="text" class="form-control"  placeholder="Bank Name" name="bank_name"> 
        
                          </div>
                        </div>
                         <div class="form-group" style="display:none" id="Cheque_no">
                          <label class="control-label col-sm-3" for="Payment Date "> Cheque NO. </label>
                          <div class="col-sm-9">
                              
                            <input type="text" class="form-control"  placeholder="Cheque NO." name="Cheque_no"> 
        
                          </div>
                        </div>
                        
                         <div class="form-group"  style="display:none" id="Cheque_date">
                          <label class="control-label col-sm-3" for="Payment Date "> Cheque Date </label>
                          <div class="col-sm-9">
                              
                            <input type="text" class="form-control datepicker"  placeholder="ChequeDdate" name="Cheque_date"> 
                          
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="Payment Date "> Payment Date </label>
                          <div class="col-sm-9">
                              
                            <input type="text" id="payment_date" required="required" class="form-control datepicker"  placeholder="Payment Date" name="payment_date"> 
                          
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <input  type="submit" id="inputButton" class="btn btn-default submit-button"  value="Submit">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('#myTable').DataTable();
});

jQuery(function(){

  var app={
          init:function(){

              jQuery('#payment_mode').on('change',function(){
                  
                   jQuery('#bank_name').hide();
                   jQuery('#Cheque_no').hide();
                   jQuery('#Cheque_date').hide();
                   if(jQuery(this).val()=='Cheque'){
                     jQuery('#Cheque_no').show();
                     jQuery('#Cheque_date').show();
                   }
                   if(jQuery(this).val()=='NEFT'){
                     jQuery('#bank_name').show();
                   }

                   
              });
          },
  };
  app.init();

});

</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script>
<script type="text/javascript">
  $(function () {
  $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        defaultDate:'dd/mm/yyy',
        format: 'dd/mm/yyyy',
  });
});

</script>


@endsection