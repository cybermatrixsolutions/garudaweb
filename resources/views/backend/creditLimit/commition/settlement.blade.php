<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
<style type="text/css">
  input.form-control.abhi {
    border: 0px;
    background-color: #E5E5E5;
}

</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Commission Settlement </h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{route('slipPendingPrinting')}}">Commission Settlement</a></li>
        </ul>
      </div>
    </div>
  
  
       <div class="">  <center>@if(Session::has('success'))
               <font style="color:red">{!!session('success')!!}</font>
            @endif</center>
         </div>
 <div class="row">
    <div class="col-md-12" style="margin-top: 79px;;">
    <form class="form-horizontal form-inline" name="myForm" action="{{route('slipPendingPrinting')}}" enctype="multipart/form-data" method="post">
       
            
             {{ csrf_field() }}
        <div class="row">
         <div class="col-md-12">
          <div class="form-group">
             <label class="control-label"  for="email">Settlement Status</label>&nbsp;&nbsp;
                 <select class="form-control" required name="Settlement" id="Settlement">
                    <option @if($type==0) selected @endif value="0">Unsettled</option> 
                     <option @if($type==1) selected @endif value="1">Settled</option> 
                   
                  </select>
          </div>

          <div class="form-group">
             <label class="control-label"  for="email">Customer Name</label>&nbsp;&nbsp;
                 <select class="form-control" required name="RO_code" id="sel1">
                    
                    @if(isset($tbl_customer_transaction))
                      @foreach($tbl_customer_transaction as $getrocodecust)
                       <option @if($customer==$getrocodecust->Customer_Code) selected @endif  value="{{$getrocodecust->Customer_Code}}">{{$getrocodecust->company_name}}</option>
                      @endforeach
                    @endif
                    </select>
          </div>

          &nbsp;&nbsp;
                        <div class="form-group">
                          <label class="control-label"  for="email">From</label>&nbsp;&nbsp;
                           <input type="text" required class="form-control datepicker" name="fromdate" placeholder="from date"/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                              <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                                <input required type="text" class="form-control datepicker" name="to_dae" placeholder="To Date"/>&nbsp;&nbsp;
                            </div>
                        <div class="form-group">
                                <input type="submit" class="btn btn-default" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                            </div>
            </div>
        </div>
 </form> 
    </div>
    </div>
  <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          @if(isset($Customerinvoices))
          <table class="table table-striped" id="myTable">
            <thead>
              
              <tr>
                <th>S.No.</th>
                <th>Invoice NO</th>
                <th>Customer Name</th>
                 <th>Amount</th>
                <th>Date</th>
                <th> Settlement </th>
                <th> Action </th>

            </thead>
                      
             <tbody>
               <?php $i=0; ?>
               @foreach($Customerinvoices as $invoice)
                
               <?php $i++;?>
              <tr>
                <td>{{$i}}</td>
                <td scope="row">{{$invoice->cus_invoice_no}}</td>
                <td scope="row">{{$invoice->getcustomer->Customer_Name}}</td>
                <td scope="row">{{$invoice->amount}}</td>
                <td scope="row">@if($invoice->settlement_date!=null){{date('d/m/Y',strtotime($invoice->settlement_date))}} @endif</td>
                <td scope="row">@if($invoice->settlement_status==0) Unsettled @else Settled @endif </td>
                
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      <li>@if($invoice->settlement_status==0) <a href="commition/settled/{{$invoice->id}}">Settled</a> @else <a href="commition/view/{{$invoice->id}}">View</a> @endif</li>
                    </ul>
                  </div>
               </td>
              </tr>
              @endforeach
              
            </tbody>
           
          </table>
          @endif
       
          </div>
         </div>
      </div>
    </div>
  
  
  
  
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script>
<script type="text/javascript">
  $(function () {
  $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        defaultDate:'dd/mm/yyy',
        format: 'dd/mm/yyyy',
  }).datepicker('update');
});

</script>

<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
</body>
</html>