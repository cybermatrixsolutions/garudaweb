
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<style type="text/css">
  span.select2.select2-container.select2-container--default.select2-container--above{
    width: 100%!important;
  }
  .select2-container {
   
    width: 100% !important;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #211d1d !important;
   
}
.dataTables_length{
  float:left;
}
</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
<!-- Navbar-->
<!-- Navbar-->

 @include('backend-includes.header')


      <!-- Side-Nav-->

  @include('backend-includes.sidebar')
  
<div class="content-wrapper">
  <div class="page-title">
    <div>
      <h1><i class="fa fa-dashboard"></i>&nbsp;Stock Item Master</h1>
    </div>
    <div>
      <ul class="breadcrumb">
       <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
        <li><a href="#"> Stock Item Master</a></li>
      </ul>
    </div>
  </div>
   <div class="">
      
   
     
      
    </div>
  <div class="row"><br><br>
     <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
           <ul>
        @foreach($errors->all() as $error)
          <li style="color: red">{{ $error}}</li>
        @endforeach
      </ul>
    <div class="col-md-6 col-sm-6 vat">
      <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a>
    </div>
    <div class="col-md-6 col-sm-6 vat">
 
     
    
        <div class="pull-right"> 
          <div>
            @if(Auth::user()->user_type==3)
                
                     <form class="form-inline" action="{{'exmportcsv'}}" name="f" onsubmit="return check(this)"  method="post" enctype="multipart/form-data">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <div class="form-group">
                     <label for="email"File(csv)></label>
                     <input type="file" id="inputfile" required="required"   placeholder="File(csv)" name="file" style="display: inline; width: auto;">
                     </div>
                     <div class="form-group">
                     <input  class="btn btn-primary" type="submit" value="Submit" id="id-submit"  style="margin-left: -83px;background-color: #00786a;color: #fff; padding: 6px 15px;
    height: auto;">
                     </div>
                     </form>
                     <a href="stock-item-master-csv" style="position: absolute;
    top: 0px;
    right: 54%;
    padding-top: 5px; height:auto;" class="btn btn-primary">Export as CSV</a>
                     @else
                      <form class="form-inline" action="{{'exmportcsv'}}"   method="post" enctype="multipart/form-data" onsubmit="return confirm('Do you want to Continue?');">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <div class="form-group">
                     <label for="email"File(csv)></label>
                     <input type="file" id="inputfile" required="required"   placeholder="File(csv)" accept=".csv" name="file" style="display: inline; width: auto;">
                     </div>
                     <div class="form-group">
                     <input  class="btn btn-primary" type="submit" value="Submit" style="margin-left: -83px;background-color: #00786a;color: #fff;">
                     </div>
                     </form>
                     <a href="all-price-list-csv-file"  class="btn btn-primary">Export as CSV</a>
                   
                     @endif

          </div>
        
          <!-- Modal -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style=" width: 1100px;">
              <div class="modal-content">
                <div class="modal-header">
                  <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                  <h4 class="modal-title" id="myModalLabel"> Stock Item Master </h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <form class="form-horizontal" action="{{'save_price_list'}}" method="post" onsubmit="return confirm('Do you want to Continue?');">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-6">
                           <input type="hidden" name="price_ro_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
                      <!-- <div class="form-group">
                        <label class="control-label col-sm-3" for="pwd">Pump Legal Name</label>
                        <div class="col-sm-9">
                           <select class="form-control" required="required" name="price_ro_code" id="sel1">
                            
                                                  @foreach($datas as $ro_master)
                                                  <option name="company_id" value="{{$ro_master->RO_code}}">{{$ro_master->pump_legal_name}}</option>
                                                  @endforeach
                          </select>

                        </div>
                      </div> -->
                        <div class="form-group">
                        <label class="control-label col-sm-3" for="pwd">Item Code <span style="color:#f70b0b;">*</span></label>
                        <div class="col-sm-9">
                           <input type="text" required="required" class="form-control" name="price_item_list" id="" placeholder="item code">
                          
                        </div>
                      </div>
                       <!--  <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Price</label>
                        <div class="col-sm-9">
                           <input type="number" required="required" class="form-control" name="price" id="" placeholder="Price">
                        </div>
                      </div> -->
                      
                      
                      <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Item Name <span style="color:#f70b0b;">*</span></label>
                        <div class="col-sm-9">
                           <input type="text" required="required" class="form-control" id="" name="item_name" placeholder="Item Name">
                        </div>
                      </div>
                     <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Stock Group <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="Stock_Group" id="stockgroup">
                                                  <option>Select Group</option>
                                                    @foreach($stock_group as $stock)
                                                        <option  value="{{$stock->id}}"
                                                                >{{$stock->Group_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Sub Group <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="Sub_Stock_Group" id="subgroup">
             
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Unit of Measure <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                                <select class="form-control" required="required" name="Unit_of_Measure" id="Unit_of_Measure">
                             
                                                    @foreach($unit_measure as $unit)
                                                        <option  value="{{$unit->id}}"
                                                                >{{$unit->Unit_Symbol}}-{{$unit->Unit_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Tail Unit</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control numeriDesimal"  id="" name="Tail_Unit" placeholder="Tail Unit">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Conversion</label>
                          <div class="col-sm-9">
                             <input type="text"   class="form-control numeriDesimal"  name="Conversion" id="" placeholder="Conversion">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Alternate Unit</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control numeriDesimal"   name="Alternate_Unit" id="" placeholder="Alternate Unit">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        
                        
                        <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">LOB <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="LOB" id="sel1">
                             
                                          @foreach($lob as $lobc)
                                    <option  value="{{$lobc->lob_name}}">
                                            {{$lobc->lob_name}}</option>
                                @endforeach
                            </select>
                           
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Brand</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control" name="brand" id="" placeholder="brand">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">GST/VAT <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="gst_vat" id="taxttype">
                               
                                   <option value="GST">GST</option>
                                   <option value="VAT">VAT</option>
                                                
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Tax</label>
                          <div class="col-sm-9">
                            <select class="form-control" id="tags" multiple="multiple" required="required" name="tax[]">
                             
                           
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">HSN/SAC</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control"  name="hsncode" id="hsncode" placeholder="hsncode">
                          </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Open Stock</label>
                        <div class="col-sm-9">
                           <input type="text"  class="form-control numeriDesimal " id="" name="volume_liter" placeholder="Open Stock">
                        </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Active From Date</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control datepickers" value="01/04/2017" name="Active_From_Date" id="" placeholder="Active From Date">
                          </div>
                        </div>

                         <div class="form-group">
                        <label class="control-label col-sm-3" for="Price">Price</label>
                        <div class="col-sm-9">
                           <input type="text"  class="form-control numeriDesimal " name="item_price" placeholder="Item Price">
                        </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-sm-3" for="Effected  Date">Effected  Date</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control datepickers" value="01/04/2017" name="effected_date" id="" placeholder="Active From Date">
                          </div>
                        </div>

                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button type="submit" id="submit-id" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
     
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="table-responsive">
        <table class="table table-striped" id="myTable">
          <thead>
            @if(isset($price_list) && count($price_list)  != 0)
            <tr>
              <th>S.No</th>
             <th>Item Code</th>
              <th>Item Name</th>
              <th>LOB</th>
              <th>Brand</th>
              <th>Stock Group</th>
              <th>Unit of Symbol</th>
              <th>Active From Date</th>
              <th>Status</th>
              <th>Actions</th>
          </thead>
          <tbody>
            <?php  $i=0;?>
             @foreach($price_list as $list)
             <?php $i++;?>
            <tr>
              <td>{{$i}}</td>
              <td scope="row">{{$list->Item_Code}}</td>
              <td>{{$list->Item_Name}}</td>
              <td>{{$list->LOB}}</td>
              <td>{{$list->brand}}</td>
              <td>{{$list->getgroup->Group_Name}}</td>
              <td>{{$list->UnitOfMeasure->Unit_Symbol}}</td>

          
              <!--  <td>{{$list->Price}}</td> -->
               <td>{{Carbon\Carbon::parse($list->Active_From_Date)->format('d/m/Y')}}</td>
                
                <td> <?php if($list->is_active==1){?>

                   <a  title="Active" href="pricemanagement/active/{{$list->id}}/0" onclick="return confirm('Do you want to deactivate?');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="pricemanagement/active/{{$list->id}}/1" onclick="return confirm('Do you want to activate?');">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 

          </td>
              <td><div class="btn-group">
                  <button type="button" class="btn btn-danger">Action</button>
                  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                  <ul class="dropdown-menu" role="menu">
                     @if($list->is_active==1)
                    <li><a href="price/edit/{{$list->id}}">Edit</a></li>
                    @endif
                     <li><a href="price/view/{{$list->id}}">View</a></li>
                    <!-- <li><a href="delete_price/{{$list->id}}" onclick="return confirm('are you sure to delete?');">Delete</a></li> -->
                  </ul>
                </div>
             </td>
            </tr>
             @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script>
function check(form){ 
 if (!/\.csv$/.test(form.file.value)){
  alert('Please select file in csv format')
  return false
  }

 else return true
}
</script>
<!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> --> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script  src="js/index.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
 $('#tags').select2({
  data: [],
    tags: true,
    tokenSeparators: [','], 
    placeholder: "Select Tax"
});

</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
   $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
   $('.table-responsive').css( "overflow", "auto" );
})
</script>
 @if(isset($price_list) && count($price_list)  != 0)
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
      $(function () {
  $(".datepickers").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
</script>
@endif
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
             vil.getcrg();
             vil.checkNumericDesimal();
              

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcrg();
              });
             jQuery('#taxttype').on('change',function(){
                  vil.getcrg();
              });
            jQuery('#stockgroup').on('change',function(){
                  vil.getstock();
              });

             jQuery('#hsncode').on('keyup',function(){
                var hsn= jQuery(this).val();
                if(jQuery.trim(hsn).length>8){
                  jQuery('#submit-id').attr('disabled',true);
                  jQuery(this).css('border-color','red');
                  alert('max length is 8 digit');
                }else{
                  jQuery('#submit-id').attr('disabled',false);
                   jQuery(this).css('border-color','');
                }

              });
           },
           getcrg:function(){
            
             jQuery.get('getrocodetx',{
                rocode:jQuery('#sel1').val(),
                taxttype:jQuery('#taxttype').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';

                  


                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                  
                jQuery('#tags').html(opt);

               });
           },
              checkNumericDesimal:function () {
                    jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });
                },
           getstock:function(){
             
               jQuery.get('getstrocksubgroupprice',{
                
                stockgroup:jQuery('#stockgroup').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#subgroup').html(opt);

                   vil.getcrg();
               });
           },

     }
     vil.init();
  });
 jQuery(function () {

    jQuery('#inputButton').on('click',function(){
        var filename=jQuery('#inputfile').val();
        var valid_extensions = /(\.csv)$/i;   
        if(valid_extensions.test(filename))
        { 
          
          var volumedip=jQuery('#volumedip').val();
          var discretiondip=jQuery('#discretiondip').val();
           if(jQuery.trim(volumedip)==''){
            alert('Please Inser Dip Charts Volume Type First');
           }else if(jQuery.trim(discretiondip)==''){
            alert('Please Inser Dip Charts Discretion First');
           }else{

            jQuery('#form1').submit();

           }
          
        }
        else
        { 
           alert('please select file in csv format');

           return false;
        }
    });
});

          $(document).ready(function() {
    $("#id-submit").on("click", function(){
        return confirm("Do you want to Continue? ");
    });
});
</script>
</body>
</html>