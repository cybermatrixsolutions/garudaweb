<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
   @include('backend-includes.header')

        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Drivers </h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Drivers</a></li>
        </ul>
      </div>
    </div>
    <div class="">
       
        </div> 
        
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
           @if ($errors->any())
        <div class="alert alert-danger">
           <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
           </ul>
       </div>
      @endif 
           <center>@if(Session::has('success'))
             <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Driver management</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal" onsubmit="return confirm('Do you want to Continue?');" action="{{'save_customber_driver_data'}}" method="post" enctype="multipart/form-data"  >
                          {{ csrf_field() }}
                          <input type="hidden" name="customer_driver_ro_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
                         <!-- <div class="form-group">

                          <label class="control-label col-sm-3" for="email">Pump Legal Name</label>
                          <div class="col-sm-9">
                        	  <select class="form-control" required="required" name="customer_driver_ro_code" id="sel1">
                             
                                                    @foreach($datas as $ro_master)
                                                        <option name="company_id" value="{{$ro_master->RO_code}}"
                                                                >{{$ro_master->pump_legal_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div> -->
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Customer Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                        	   <select class="form-control" required="required" name="customer_code" id="customer_code">
                              
                                                   
                            </select>
                          </div>
                          </div>

                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Registration Number </label>
                          <div class="col-sm-9">
                            <select class="form-control"   name="Registration_Number" id="registration_number" >
                              
                                                   
                            </select>

                             </div>
                        </div>
                        

                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Driver Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                        	   <input type="text"  class="form-control" required="required" name="driver_name" id="" placeholder="Driver Name">
                          </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-3" for="pwd">Gender <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control" required="required" name="gender">
                             <option value="Female">Female</option>
                             <option value="Male">Male</option>
                           </select>
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Email </label>
                          <div class="col-sm-9">
                             <input type="email"  class="form-control"  name="email" id="" placeholder="Email">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Licence No. </span></label>
                          <div class="col-sm-9">
                        	   <input type="text" data-table="tbl_customer_vehicle_driver" data-colum="Driver_Licence_No" data-mass="Licence No. Already Exist" class="form-control checkunique"  name="licence_no" id="" placeholder="Licence Number">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Valid Upto </label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control" name="valid_upto" id="datepicker" placeholder="Valid Upto">
                          </div>
                        </div>
                       
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Mobile(+91) <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                        	   <input type="text" data-table="tbl_customer_vehicle_driver" data-colum="Driver_Mobile" data-mass="Mobile No. Already Exist" data-customercol="customer_code"  required="required" class="form-control checkunique number" name="Driver_Mobile" id="mobile" maxlength="10" placeholder="Mobile ">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Aadhaar No </label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="aadhaar_no" id="aadhaar_no" placeholder="Aadhaar No">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Aadhaar Photo </label>
                          <div class="col-sm-9">
                            <input type="file"  class="form-control" name="aadhaar_img" id="aadhaar_img" placeholder="Aadhaar hoto">
                          </div>
                        </div>
                          
                        <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Opening Date</label>
                          <div class="col-sm-9">
                            <input class="form-control datepicker" required="required" name="Operating_date"  type="text" />
                          </div>
                        </div> -->
                          
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default submit-button" disabled="disabled" onclick="return getmobilevalid()">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
               @if(isset($customer_driver_list) && count($customer_driver_list)  != 0)
                <th>S.No.</th>
                <!-- <th>Outlet Name</th> -->
                <th>Customer Name</th>
                <th>Registration No.</th>
                <th>Driver Name</th>
                <th>Mobile</th>
                <th>Status</th>
                 <th>Actions</th>
            </thead>
            <tbody>
              <?php $i=0;?>
               @foreach($customer_driver_list as $list)
              
              <?php $i++;?>
              <tr>
                <td>{{$i}}</td>
               <!--  <td scope="row">{{$list->pump_name}}</td> -->
                <td>{{$list->company_name}}</td>
                <td>{{$list->Registration_Number}}</td>
                 <td>{{$list->Driver_Name}}</td>
                 <td>{{$list->Driver_Mobile}}</td>
               
                 <td> <?php if($list->is_active==1){?>

                   <a  title="Active" href="driver/active/{{$list->id}}/0" onclick="return confirm('Do you want to deactivate?');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="driver/active/{{$list->id}}/1" onclick="return confirm('Do you want to activate?');">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
          </td>
                <td><div class="btn-group">
                    <button type="button" @if($list->getCustomer->confirm=='1') class="btn btn-success" @else class="btn btn-danger" @endif >Action</button>
                    <button type="button" @if($list->getCustomer->confirm=='1') class="btn btn-success dropdown-toggle" @else class="btn btn-danger dropdown-toggle" @endif  data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      @if($list->is_active==1 & $list->getCustomer->confirm=='0')
                      <li><a href="customer_driver/edit/{{$list->id}}">Edit</a></li>
                      @endif
                      <li><a href="customer_driver/view/{{$list->id}}">View</a></li>
                     <!-- <li><a href="customer_driver/delete/{{$list->id}}" onclick="return confirm('are you sure to delete?');">Delete</a></li>-->
                    </ul>
                  </div>
               </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

<script  src="js/index.js"></script> 
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#sel1').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  opt+='<option value="">--Select Registration Number--</option>';
                  jQuery.each(data, function(index,value){

                    
                     opt+='<option value="'+value+'">'+value+'</option>';
                  });

                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
               });
           },

     }
     vil.init();
  });
</script>

<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                  var colum=jQuery(this).data('colum');
                  var customercol=jQuery(this).data('customercol');
                  var customer=jQuery('#customer_code').val();
                   //id=jQuery(this).data('id');
                    
                 // udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique_cust',{
                  table:table,
                  colum:colum,
                  customer:customer,
                  customercol:customercol,
                 // unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(function () {
  $("#datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
    function  getmobilevalid() {
        var mobile = document.getElementById("mobile").value;
        var pattern = /^[0]?[789]\d{9}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
            return true;
        }
        alert("It is not valid mobile number.input 10 digits number!");
        return false;
    }
    $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
</script>
</body>
</html>