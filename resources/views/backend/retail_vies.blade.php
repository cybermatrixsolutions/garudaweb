<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
<style>
  .slimScrollDiv {
   height:834px!important;
   }

</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Retail Outlet Management View</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="{{url('/retail')}}">Retail Outlet management</a></li>
        </ul>
      </div>
    </div>
                          <div class="">
                     <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                            </div>  
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <!-- Modal -->
            <div class="row">
          
          @foreach($retailData as $row)
          
                      <form class="form-horizontal" action="{{url('retail_edit')}}/{{$row->id}}" enctype="multipart/form-data" method="post">
               <input type="hidden" name="_token" value="{{ csrf_token()}}"/> 
                        
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Principal Company</label>
                          <div class="col-sm-8">
                            
                             <select class="form-control" disabled="disabled" id="" name="company_code" required/>
                                @foreach($getPrincipal as $row1) 
                              <option value="{{$row1->company_code}}" @if($row->company_code == $row1->company_code) selected @endif>{{$row1->company_name}}</option>
                              
                               @endforeach
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Pump Legal Name</label>
                          <div class="col-sm-8">
                            <input type="text" disabled="disabled" class="form-control" id="" name="pump_legal_name" value="{{$row->pump_legal_name}}" placeholder="Pump Legal Name" required/>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Pump Address 1</label>
                          <div class="col-sm-8">
                            <textarea class="form-control" disabled="disabled" rows="1"  id="comment" name="pump_address"  required/>{{$row->pump_address}}</textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Pump Address 2</label>
                          <div class="col-sm-8">
                            <textarea class="form-control" disabled="disabled" rows="1"  id="comment" name="address_2"  required/>{{$row->address_2}}</textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Pump Address 3</label>
                          <div class="col-sm-8">
                            <textarea class="form-control" disabled="disabled" rows="1"  id="comment" name="address_3"  required/>{{$row->address_3}}</textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Country</label>
                          <div class="col-sm-8">
                             <select class="form-control" disabled="disabled" disabled="disabled" id="country" name="country"  value="{{$row->country}}">
                               @foreach($countries_list as $country)
                                  <option @if($country->id==101) selected @endif  value="{{$country->id}}"
                                          >{{$country->name}}</option>
                              @endforeach
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">State</label>
                          <div class="col-sm-8">
                             <select class="form-control" disabled="disabled" id="state" name="state"  data-value="{{$row->state}}" >
                               
                             </select>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">City</label>
                          <div class="col-sm-8">
                             <select class="form-control" disabled="disabled" id="city" name="city"  data-value="{{$row->city}}" >
                               
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">PIN Code</label>
                          <div class="col-sm-8">
                              <input type="text" disabled="disabled" class="form-control" id="" name="pin_code"   pattern="[0-9]{6}" title="Five digit zip code" placeholder="pin code" value="{{$row->pin_code}}" required/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Phone Number</label>
                          <div class="col-sm-8">
                            <input type="text" disabled="disabled" class="form-control" id="" name="phone_no" value="{{$row->phone_no}}" placeholder="Phone Number" required/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Mobile(+91)</label>
                          <div class="col-sm-8">
                            <input type="text" disabled="disabled" data-table="tbl_ro_master" data-colum="mobile" data-mass="Mobile No. Already Exit" data-id="{{$row->id}}" class="form-control checkunique" id="" name="mobile" value="{{$row->mobile}}" placeholder="Mobile" required/>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="website">Customer Care No.</label>
                          <div class="col-sm-8">
                            <input type="text" disabled="disabled" class="form-control" value="{{$row->customer_care}}" id="website" name="customer_care" placeholder="Customer Care No." />
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="website">Website</label>
                          <div class="col-sm-8">
                            <input type="text"  disabled="disabled" class="form-control" value="{{$row->website}}" id="website" name="website" placeholder="Website" />
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Email as login ID</label>
                          <div class="col-sm-8">
                            <input type="email" disabled="disabled" data-table="tbl_ro_master" data-colum="Email" data-mass="Email Id Already Exit" data-id="{{$row->id}}" class="form-control checkunique" id="email" name="Email" value="{{$row->Email}}" placeholder="Email" required/>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Password</label>
                          <div class="col-sm-8">
                             <input type="password" class="form-control" id="" disabled="disabled" name="password" value="{{bcrypt($row->user->password)}}">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Lube License</label>
                          <div class="col-sm-8">
                            <input type="text" disabled="disabled" class="form-control" id=""  name="Lube_License" value="{{$row->Lube_License}}" placeholder="Lube License" required/>
                          </div>
                        </div>
                         
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Pump License</label>
                          <div class="col-sm-8">
                            <input type="text" disabled="disabled" class="form-control" id="" name="Pump_License" value="{{$row->Pump_License}}" placeholder="Pump License" required/>
                          </div>
                        </div>
                         <div class="form-group">
                            <div class="col-sm-7">
                              <label class="control-label col-sm-7" for="GST Invoice Prefix" >GST Invoice Prefix</label>
                              <div class="col-sm-5">
                                <input type="text" disabled="disabled" required="required" minlength="4" maxlength="4" class="form-control" id="invoice_prefix"  name="GST_prefix" value="{{$row->GST_prefix}}" placeholder="GST Invoice Prefix" required/>
                              </div>
                            </div>
                            <div class="col-sm-5">
                              <label class="control-label col-sm-7" for="Serial No.">Serial No.</label>
                              <div class="col-sm-5">
                                <input type="number" disabled="disabled" required="required"  class="form-control" value="{{$row->GST_serial}}" name="GST_serial" placeholder="Serial No." required/>
                              </div>
                            </div>
                          
                        </div>

                        <div class="form-group">
                            <div class="col-sm-7">
                              <label class="control-label col-sm-7" for="VAT Invoice Prefix">VAT Invoice Prefix</label>
                              <div class="col-sm-5">
                                <input type="text" disabled="disabled" required="required" minlength="4" value="{{$row->VAT_prefix}}" maxlength="4" class="form-control" id="VAT_prefix"  name="VAT_prefix" placeholder="VAT Invoice Prefix" required/>
                              </div>
                            </div>
                            <div class="col-sm-5">
                              <label class="control-label col-sm-7" for="Serial No.">Serial No.</label>
                              <div class="col-sm-5">
                                <input type="number" disabled="disabled" required="required" value="{{$row->VAT_serial}}" class="form-control"  name="VAT_serial" placeholder="Serial No." required/>
                              </div>
                            </div>
                          
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">VAT No.</label>
                          <div class="col-sm-8">
                            <input type="text" disabled="disabled"  class="form-control" id="" name="VAT_TIN" value="{{$row->VAT_TIN}}" placeholder="VAT TIN" required/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">GSTIN</label>
                          <div class="col-sm-8">
                            <input type="text" style="text-transform: uppercase" disabled="disabled" class="form-control checkunique" data-table="tbl_ro_master" data-colum="GST_TIN" data-mass="GST Already Exit" data-id="{{$row->id}}" id="gst" name="GST_TIN" value="{{$row->GST_TIN}}" placeholder="GST TIN" required pattern="[0-9]{2}[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}\d{1}[(a-z)(A-Z)]{1}\d{1}"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">PAN</label>
                          <div class="col-sm-8">
                            <input type="text" style="text-transform: uppercase" disabled="disabled" class="form-control checkunique" data-table="tbl_ro_master" data-colum="PAN_No" data-mass="PAN No Already Exit" data-id="{{$row->id}}" id="pan" name="PAN_No" value="{{$row->PAN_No}}" placeholder="PAN No" required  pattern="[A-Z]{5}\d{4}[A-Z]{1}"/>
                          </div>
                        </div>
                        
                      </form>
            @endforeach
                    </div>
            
          </div>

        </div>
      </div>
    </div>
   
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 

<script>
  $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">
 jQuery(function(){
     var url1="{{url('get_country_state')}}";
     var url2="{{url('get_state_city')}}";
     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#country').on('change',function(){
                  vil.getcom();
              });
            jQuery('#state').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get(url1,{
                
                rocode:jQuery('#country').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  var d=jQuery('#state').data('value');

                  jQuery.each(data, function(index,value){
                    if(d==index){
                       opt+='<option selected value="'+index+'">'+value+'</option>';
                    }else{
                        opt+='<option value="'+index+'">'+value+'</option>';
                      }
                     
                  });
                    console.log(opt);
                   
                   jQuery('#state').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get(url2,{
                rocode:jQuery('#state').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';

                 var d=jQuery('#city').data('value');

                   

                   if(data.length==0)
                   {
                    
                     opt+='<option value="'+jQuery('#state option:selected').val()+'">'+jQuery('#state option:selected').text()+'</option>';
                   }
                   else{


                  jQuery.each(data, function(index,value){

                     if(d==index){
                       opt+='<option selected value="'+index+'">'+value+'</option>';
                    }else{
                        opt+='<option value="'+index+'">'+value+'</option>';
                      }

                  });
                    
                   }
                jQuery('#city').html(opt);

               });
           },

     }
     vil.init();
  });
</script>
<script type="text/javascript">
 jQuery(function(){

  var url1="{{url('check_Pedestal_Number_unique')}}";

     var vil={
           init:function(){
             vil.selectRo();

              jQuery('form').on('submit',function(){
                   var gst=jQuery('#gst').val();
                   var pan=jQuery("#pan").val();
                   gst= gst.substring(2,12);

                   if(gst!=pan){
                     alert('Please Enter valid GST');
                      jQuery("#success-btn").attr('disabled',false);

                      return false;
                   }

                   

              });
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get(url1,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>

</body>
</html>