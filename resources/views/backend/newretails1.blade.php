<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--bootstrap default css-->
    <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
    <link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
    <link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <title>Garruda</title>
    <style>
  .slimScrollDiv {
   height:834px!important;
   }

        .ui-tabs ul li {
    float: left;
    width: auto;
    margin-top: 10px;
    list-style: none;
    background: #00786a;
    padding: 5px;
    margin-right: 2px;
    
}
.ui-tabs ul li a{
    color: #fff !important;
}
.ui-tabs-active{
      background: #fff !important;
      border: 1px solid #ccc;
      border-bottom: none;
     color: #000 !important;
}

.ui-tabs ul li a {
    background:transparent !important;
    padding: 6px 10px;
    margin-right: 5px;
    border-radius: 2px;
}
  
  .ui-tabs ul .ui-tabs-active a{
     color: #000 !important;
}     
    </style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper">
    <!-- Navbar-->

@include('backend-includes.header')

<!-- Side-Nav-->

    @include('backend-includes.sidebar')

    <div class="content-wrapper">
        <div class="page-title">
            <div>
                <h1><i class="fa fa-dashboard"></i>&nbsp;Add Retail Outlet</h1>
            </div>
            <div>
                <ul class="breadcrumb">
                    <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

                    <li><a href="#">Retail Outlet </a></li>
                </ul>

            </div>
        </div>
        

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <center>@if(Session::has('success'))
                            <font style="color:red">{!!session('success')!!}</font>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    </center>
                    <div class="col-md-6 col-sm-6 col-xs-12 vat">
                        <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>

                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document" style=" width: 1100px;">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                                        <h4 class="modal-title" id="myModalLabel">Retail Outlet</h4>
                                    </div>
                                    <div class="modal-body" style="padding: 0px !important;">
                                      
                                            <form class="form-horizontal" action="{{'add_retail'}}" enctype="multipart/form-data" method="post" novalidate>
                         <input type="hidden" name="_token" value="{{ csrf_token()}}"/> 

                        <div id="tabs-n">
                            <div class="row">
                                <div class="col-md-12"> 
                                  <ul class="ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header">
                                    <li><a style="pointer-events: none;" href="#tabs-1">Info</a></li>
                                    <li><a style="pointer-events: none;" href="#tabs-2">Configuration</a></li>
                                     <li><a style="pointer-events: none;" href="#tabs-3">Bank Details and T&C </a></li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                     <li style="background-color: #403333;margin-left: 91px;border-radius: 8px 8px 8px;padding: 2px;"><input type="text" readonly name="" id="copyprincple"></li>
                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                     <li style="background-color: #403333;margin-left: 91px;border-radius: 8px 8px 8px;padding: 2px;"><input type="text" readonly name="" style="width: 276px;" id="copycompany"></li>
                                  </ul>
                                </div>
                           <div id="tabs-1">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Principal Company </label>
                                    <div class="col-sm-8">
                                      
                                       <select class="form-control" id="printpp" name="company_code" required/>
                                          @foreach($getPrincipal as $row1) 
                                        <option value="{{$row1->company_code}}">{{$row1->company_name}}</option>
                                        
                                         @endforeach
                                       </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Outlet Name <span style="color:#f70b0b;">*</span></label>
                                    <div class="col-sm-8">
                                      <input type="text" class="form-control" id="Outlet" name="pump_legal_name" placeholder="Outlet Name" required/>
                                    </div>
                                  </div>
                                    <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Select Outlet Type <span style="color:#f70b0b;">*</span></label>
                                    <div class="col-sm-8">
                                      <select class="form-control" name="Ro_type">
                                        <option  value="1">Petrol Pump</option>
                                        <option   value="2">Other Retail</option>
                                    
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Outlet Address 1 <span style="color:#f70b0b;">*</span></label>
                                    <div class="col-sm-8">
                                      <textarea class="form-control" rows="1"  id="comment" name="pump_address"  required/></textarea>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Outlet Address 2</label>
                                    <div class="col-sm-8">
                                      <textarea class="form-control" rows="1"  id="comment" name="address_2"  /></textarea>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Outlet Address 3</label>
                                    <div class="col-sm-8">
                                      <textarea class="form-control" rows="1"  id="comment" name="address_3"  /></textarea>
                                    </div>
                                  </div>
                                 <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Country</label>
                          <div class="col-sm-8">
                             <select class="form-control" disabled="disabled" id="country" name="country">
                               @foreach($countries_list as $country)
                                  <option @if($country->id==101) selected @endif  value="{{$country->id}}"
                                          >{{$country->name}}</option>
                              @endforeach
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">State<span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <select class="form-control" id="state" name="state" required>
                               
                             </select>
                          </div>
                        </div>
                                   <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">City</label>
                                    <div class="col-sm-8">
                                       <select class="form-control" id="city" name="city">
                                         
                                       </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">PIN Code <span style="color:#f70b0b;">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control number" id="pin_code" name="pin_code"   pattern="[0-9]{6}]" title="Five digit zip code" placeholder="pin code"  required/ maxlength="6">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Phone Number</label>
                                    <div class="col-sm-8">
                                      <input type="text" class="form-control" id="" name="phone_no"  placeholder="Phone Number" />
                                    </div>
                                  </div>
                                   <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Mobile(+91) <span style="color:#f70b0b;">*</span></label>
                                    <div class="col-sm-8">
                                      <input type="text" data-table="users" data-colum="mobile" data-mass="Mobile No. Already Exit" class="form-control number checkunique" id="mobile" maxlength="10" name="mobile"  placeholder="Mobile" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                   
                                    <div class="form-group">
                                      <label class="control-label col-sm-4" for="pwd">Email as login ID <span style="color:#f70b0b;">*</span></label>
                                      <div class="col-sm-7">
                                        <input type="email" data-table="users" data-colum="Email" data-mass="Email Id Already Exit" class="form-control checkunique" id="email" name="Email"  placeholder="Email" required/>
                                      </div>
                                    </div>  
                                     <div class="form-group">
                                          <label class="control-label col-sm-4" for="pwd">Password <span style="color:#f70b0b;">*</span> </label>
                                          <div class="col-sm-7">
                                              <input type="password" class="form-control" id="myInput" name="password" placeholder="password" required><input type="checkbox" onclick="myFunction('myInput')">Show Password
                                          </div>
                                      </div>

                                    <h4> RO Principal (Owner) Contact Person</h4>
                                         <hr/>
                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd"> Name <span style="color:#f70b0b;">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control " name="Contact_Person_Name" placeholder="Contact Person Name" required/>
                                            </div>
                                         </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd"> Email <span style="color:#f70b0b;">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="email" class="form-control" id="emailcody" name="Contact_Person_Email" required placeholder="Email" readonly/>
                                            </div>
                                         </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd"> Mobile <span style="color:#f70b0b;">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control number" id="copymobile" maxlength="10" name="Contact_Person_Mobile" readonly placeholder="Contact Person Mobile" required/>
                                            </div>
                                         </div>

                                         <h4>RO Secondary Contact Person</h4>
                                         <hr/>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd">Name<span style="color:#f70b0b;">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control " id="Secondary_Person_Name" name="Secondary_Person_Name"  placeholder="Contact Person Name" required/>
                                            </div>
                                         </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd"> Email <span style="color:#f70b0b;">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="email" class="form-control " id="Secondary_Person_Email" name="Secondary_Person_Email"  placeholder="Email" required/>
                                            </div>
                                         </div>
										 <!--
                                          <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd">Password </label>
                                            <div class="col-sm-7">
                                                <input type="password" class="form-control" id="myInputSecondary" name="passwordSecondary" placeholder="password"><input type="checkbox" onclick="myFunction('myInputSecondary')">Show Password
                                            </div>
                                          </div>
										-->
                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="pwd"> Mobile <span style="color:#f70b0b;">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control number" maxlength="10" name="Secondary_Person_Mobile" placeholder="Contact Person Mobile" required/>
                                            </div>
                                         </div>

                                   <!--end col 6-->
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <!--<button type="submit" class="btn btn-default">Submit</button>-->
                                        <center><input type="button" class="btn btn-primary" id="next_1" value="NEXT" data-id="1"></center>
                                    </div>

                                </div>
                            <!--end tab 1-->
                           </div>
                           <div id="tabs-2">
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-sm-4" for="website">Customer Care No.</label>
                                  <div class="col-sm-8">
                                    <input type="text" class="form-control"  id="website" name="customer_care" placeholder="Customer Care No." />
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="control-label col-sm-4" for="website">Website</label>
                                  <div class="col-sm-8">
                                    <input type="text"  class="form-control"  id="website" name="website" placeholder="Website" />
                                  </div>
                                </div>
                                
                              
                                <div class="form-group">
                                  <label class="control-label col-sm-4" for="pwd">Lube License </label>
                                  <div class="col-sm-8">
                                    <input type="text" class="form-control" id=""  name="Lube_License"  placeholder="Lube License" />
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-4" for="pwd">Outlet License </label>
                                  <div class="col-sm-8">
                                    <input type="text" class="form-control" id="" name="Pump_License"  placeholder="Pump License"/>
                                  </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5"></div>
                                    <div class="col-md-3">Start from</div>
                                    <div class="col-md-2">Serial no. Width</div>
                                    <div class="col-md-2">Reset FY in</div>
                                </div>
                                
                             <div class="row leftln-rigstln">
                                  <div class="col-md-5">
                                       <div class="form-group">
                                    
                                        <label class="control-label col-sm-7" for="GST Invoice Prefix" >GST Invoice Prefix <span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                          <input type="text" required="required" minlength="1" maxlength="4" class="form-control" id="invoice_prefix"  name="GST_prefix"  placeholder="GST Invoice Prefix" required/>
                                       
                                      </div>
                                       </div>
                                  </div>
                                    <div class="col-md-3">
                                          <div class="form-group">
                                       <input type="text" required="required" maxlength="8"   class="form-control quantity number leftln" value="1" name="GST_serial"  placeholder="Starting No." required/>
                                  </div>
                                   </div>
                                    <div class="col-md-2">
                                          <div class="form-group">
                                     
                                        <select class="form-control rigstln" name="GST_serial_lenght">
                                              <option value="3">3</option>
                                              <option value="4">4</option>
                                              <option value="5">5</option>
                                              <option value="6">6</option>
                                              <option value="7">7</option>
                                              <option value="8">8</option>
                                          </select>
                                   
                                    </div>
                                  </div>
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        <input type="checkbox" name="GST_serial_fy" value="1">
                                   </div>
                                  </div>
                             </div>
                              <div class="row leftln-rigstln">
                                   <div class="col-md-5">
                                        <div class="form-group">
                                     
                                        <label class="control-label col-sm-7" for="VAT Invoice Prefix">VAT Invoice Prefix <span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                          <input type="text" required="required" minlength="1" maxlength="4" class="form-control" id="VAT_prefix"  name="VAT_prefix"  placeholder="VAT Invoice Prefix" required/>

                                      </div>
                                       </div>
                                   </div>
                                    <div class="col-md-3">
                                          <div class="form-group">
                                       <input type="text" required="required" maxlength="8"   class="form-control quantity number leftln" value="1" name="VAT_serial"  placeholder="Starting No." required/>
                                  </div>
                                   </div>
                                    <div class="col-md-2">
                                       <div class="form-group">
                                        <select class="form-control rigstln" name="VAT_serial_lenght">
                                              <option value="3">3</option>
                                              <option value="4">4</option>
                                              <option value="5">5</option>
                                              <option value="6">6</option>
                                              <option value="7">7</option>
                                              <option value="8">8</option>
                                          </select>
                                   
                                    </div>
                                  </div>
                                    <div class="col-md-2">
                                      <div class="form-group">
                                        <input type="checkbox" name="VAT_serial_fy" value="1">
                                   </div>
                                  </div>
                              </div>

                               
                                       
                                       <div class="col-sm-6">
                                 <div class="form-group">
                                    <div class=" col-md-offset-4 col-md-4">
                                  </div>
                                    </div>
                                    </div>
                                    </div>
                             

                            <!--end col 6-->
                           

                            <div class="col-md-6">
                              
                                  <div class="form-group">
                                    <label class="control-label col-sm-4" for="SAP Code">Principle ERP code</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control"  name="SAP_Code" placeholder="Principle ERP code" />
                                  </div>
                                  </div>
                                <div class="form-group">
                                  <label class="control-label col-sm-4" for="pwd">VAT No.</label>
                                  <div class="col-sm-7">
                                    <input type="text" class="form-control" maxlength="15"  id="" name="VAT_TIN"  placeholder="VAT TIN" />
                                  </div>
                                </div>
                                 
                                <div class="form-group" >
                                                            <label class="control-label col-sm-4" for="pwd">PAN </label>
                                                            <div class="col-sm-7">
                                                                <input type="text" class="form-control GST_TIN" id="pan" name="PAN_No" placeholder="PAN No"  maxlength="10"  maxlength="10" />
                                                            </div>

                                                             <input type="hidden" name="" value="" id="getstatecode" ng-bind="name">
                                                             
                                                           
                                                              
                                                        </div>
                              <div class="form-group">
                                                            <label class="control-label col-sm-4" for="pwd">GSTIN </label>
                                                            <div class="col-sm-8">
                                                               <div class="row">
                                                              
                                                                 <input type="hidden"  readonly  value="" class="form-control" id="getsta"  name="GST_TIN"  />
                                                             
                                                               <div class="col-sm-5" style="padding-right:0px;    width:49.666667%;text-transform: uppercase;">
                                                                 <input type="text"  readonly  value="" class="form-control" id="views"   />
                                                              </div>
                                                              
                                                             <div class="col-sm-4" style="padding-left:0px">
                                                                <input type="text"  class="form-control GST_TIN" id="gst" name="GST_ddTIN" placeholder="GST No." maxlength="3" /><br>
                                                            </div>

                                                               
                                                        </div>
                                                    </div>
                                                        </div>
                                                        
                                 
                                <div class="row">
                                    <div class="col-md-5"></div>
                                    <div class="col-md-3">Start from</div>
                                    <div class="col-md-2">Serial no. Width</div>
                                    <div class="col-md-2">Reset FY in</div>
                                </div>
                                 <div class="row leftln-rigstln">
                                    <div class="col-md-5">
                                  <div class="form-group">
                                        <label class="control-label col-sm-7" for="GST Invoice Prefix" >Delivery Slip Prefix <span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="text" required="required" minlength="1" maxlength="4" class="form-control" id="Delivery_Slip_Prefix"  name="Delivery_Slip_Prefix"   placeholder="Delivery Slip Prefix" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                     <div class="form-group">
                                    <input type="text" required="required" maxlength="8"   class="form-control quantity number leftln" value="1" name="Delivery_serial"  placeholder="Starting No." required/>
                                            
                                           
                                </div>
                                </div>
                                <div class="col-md-2">
                                     <div class="form-group">
                                    <select class="form-control rigstln" name="D_serial_no_lenght">
                                              <option value="3">3</option>
                                              <option value="4">4</option>
                                              <option value="5">5</option>
                                              <option value="6">6</option>
                                              <option value="7">7</option>
                                              <option value="8">8</option>
                                          </select><br>
                                         
                                </div>
                                 </div>
                                <div class="col-md-2">
                                     <div class="form-group">
                                          <input type="checkbox" name="D_no_fy" value="1">
                    
                                </div>
                                     </div>
                            </div>
                              <div class="row leftln-rigstln">
                                  <div class="col-md-5">
                                      <div class="form-group">
                                 
                                        <label class="control-label col-sm-7" for="VAT Invoice Prefix">GST Invoice Slip Prefix<span style="color:#f70b0b;">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="text" required="required" minlength="1" maxlength="4" class="form-control" id="GST_Slip_Prefix"   name="GST_Slip_Prefix" placeholder="GST Invoice Slip Prefix" required/>
                                        </div>
                                  
                                    </div>
                                  </div>
                                   <div class="col-md-3">
                                     <div class="form-group">
                                       <input type="text" required="required" maxlength="8"   class="form-control quantity number leftln" value="1" name="GST_Slip_serial"  placeholder="Starting No." required/>
                                   </div>
                                     </div>
                                   <div class="col-md-2">
                                     <div class="form-group">
                                        <select class="form-control rigstln" name="GST_Slip_lenght">
                                              <option value="3">3</option>
                                              <option value="4">4</option>
                                              <option value="5">5</option>
                                              <option value="6">6</option>
                                              <option value="7">7</option>
                                              <option value="8">8</option>
                                          </select>
                                   </div>
                                     </div>
                                   <div class="col-md-2">
                                     <div class="form-group">
                                        <input type="checkbox" name="GST_Slip_fy" value="1">
                                   </div>
                                     </div>
                              </div>
                            <!--end col 6-->
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <!--<button type="submit" class="btn btn-default">Submit</button>-->
                                    <center><input type="button" class="btn btn-primary nextbutton"  value="Previous" data-id="0"> 
                                      <input type="button" class="btn btn-primary" value="NEXT" id="next-2" data-id="2"></center>
                                </div>

                            </div>
                            <!--end tab 2-->
                           </div>

                           <div id="tabs-3">
                                
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="Bank Name">Bank Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="Bank_Name" name="Bank_Name"  placeholder="Bank Name"  />
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="Bank Branch">Bank Branch</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="Bank_Branch" name="Bank_Branch" placeholder="Bank Branch">
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="Account Name">Account Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="Account_Name" name="Account_Name" placeholder="Account Name"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="Account Number">Account Number</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" maxlenght="10" maxlenght="16" id="Account_Number" name="Account_Number"  placeholder="Account Number"/>
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="IFSC Code">IFSC Code</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" maxlength="11" id="IFSC_Code" name="IFSC_Code" placeholder="IFSC Code"/>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for Delivery Slip">T&C for Delivery Slip 1</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  rows="1" id="TC_Delivery_Slip" name="TC_Delivery_Slip"  maxlength="40"></textarea>
                                               
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for Delivery Slip">T&C for Delivery Slip 2</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  rows="1" id="TC_Delivery_Slip2" name="TC_Delivery_Slip2"  maxlength="40"></textarea>
                                               
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for GST Invoice Slip">T&C for GST Invoice Slip 1</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  rows="1"  maxlength="40" id="TC_for_GST_Invoice_Slip" name="TC_for_GST_Invoice_Slip"></textarea>
                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for GST Invoice Slip">T&C for GST Invoice Slip 2</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control"  rows="1"  maxlength="40" id="TC_for_GST_Invoice_Slip" name="TC_for_GST_Invoice_Slip2"></textarea>
                                                
                                            </div>
                                        </div>
                        

                                    </div>

                                    <div class="col-md-6">
                                       

                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for VAT Invoice">T&C for VAT Invoice 1 </label>
                                            <div class="col-sm-7">
                                                 <textarea class="form-control"  rows="1"  maxlength="250" id="TC_for_VAT_Invoice" name="TC_for_VAT_Invoice"></textarea>
                                               
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for VAT Invoice">T&C for VAT Invoice 2</label>
                                            <div class="col-sm-7"> 
                                                 <textarea class="form-control"  rows="1"  maxlength="250" id="TC_for_VAT_Invoice" name="TC_for_VAT_Invoice2"></textarea>
                                               
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for GST Invoice">T&C for GST Invoice 1</label>
                                            <div class="col-sm-7">
                                                 <textarea class="form-control"  rows="1" maxlength="250" id="TC_for_GST_Invoice" name="TC_for_GST_Invoice"></textarea>
                                                
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="control-label col-sm-4" for="T&C for GST Invoice">T&C for GST Invoice 2</label>
                                            <div class="col-sm-7">
                                                 <textarea class="form-control"  rows="1" maxlength="250" id="TC_for_GST_Invoice" name="TC_for_GST_Invoice2"></textarea>
                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                    <label class="control-label col-sm-4" for="Image of RO VAT Registration">Image of RO VAT Registration </label>
                                    <div class="col-sm-4">
                                          
                                        <input type="file"  class="form-control" id="" name="VAT_image" placeholder="Image of RO VAT Registration "/>
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="Image of RO VAT Registration">Image of RO PAN Card </label>
                                    <div class="col-sm-4">
                                        <input type="file"  class="form-control" id="" name="PAN_image" placeholder="Image of RO PAN Card"/>
                                    </div>
                                     
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="Image of RO GST Registration">Image of RO GST Registration </label>
                                    <div class="col-sm-4">
                                        <input type="file"  class="form-control" id="" name="GST_image" placeholder="Image of RO Pan Card"/>
                                    </div>
                                    
                                </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-4 col-sm-8">
                                            <!--<button type="submit" class="btn btn-default">Submit</button>-->
                                            <center><input type="button" class="btn btn-primary nextbutton" value="Previous" data-id="1"> <input type="submit" id="success-btn"  class="btn btn-primary site-btn submit-button" value="Submit" ></center>
                                        </div>

                                    </div>

                                </div>
                       </div>
                       </div>
                      </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="col-md-6 col-sm-6 col-xs-12 vat">
            <a href="{{route('download_newretails1')}}" class="btn btn-primary pull-right btn-sm RbtnMargin" >Download CSV File</a>
</div>
                    </div>
                </div>
            </div>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                  
                    <table class="table table-striped" id="myTable">
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Principal Company Name
                            <th>RO Code</th>
                            <th>Outlet Name</th>
                            <th>Outlet Type</th>
                            <th>Status</th>

                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>


                        <?php $i=0; ?>

                        @foreach($getRetail as $row)

                            <?php $i++ ;?>
                            <tr>
                                <td>{{$i}}</td>
                                <td scope="row">{{$row->company_name}}</td>
                                <td>{{$row->RO_code}}</td>
                                <td>{{$row->pump_legal_name}}</td>
                                <td> @if($row->Ro_type == 1) Petrol Pump @endif
                                      @if($row->Ro_type == 2) Other Retail @endif
                                     
                                </td>
                                <td> <?php if($row->is_active==1){?>

                                    <a  title="Active" href="{{URL('retailDeactive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to deactivate? if you deactive this all relation will be deactivated.');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                                    <?php }else{ ?>

                                    <a title="Deactive" href="{{URL('retailActive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to activate?');">

                                        <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                                    </a>
                                    <?php } ?>
                                </td>
                                <td><div class="btn-group">
                                        <button type="button" class="btn @if($row->confirm==1) btn-success @else btn-danger @endif">Action</button>
                                        <button type="button" class="btn @if($row->confirm==1) btn-success @else btn-danger @endif dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="retails_view/{{$row->id}}">View</a></li>
                                            @if($row->is_active==1 && $row->confirm==0)

                                                <li><a href="retail_update/{{$row->id}}" >Edit</a></li>
                                             @endif
                                        <!--  <li><a href="retail_delete/{{$row->id}}" onClick="return confirm('Are you sure?');">Delete</a></li>-->
                                        </ul>
                                    </div></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts-->
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script>
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script>
<script src="{{URL::asset('js/main.js')}}"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
  //called when key is pressed in textbox
  $(".quantity").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});
    function myFunction(ID) {
    var x = document.getElementById(ID);
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }

}
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
<script type="text/javascript">

    jQuery(function(){

        var vil={
            init:function(){
                vil.selectRo();


                jQuery('form').on('submit',function(){
                    
                     var account=jQuery('#Account_Number').val();

                    if(jQuery.trim(account)!='' && account.length>16){
                        alert('Please Enter valid Account Number');
                        jQuery('#Account_Number').css('border-color','red');
                        jQuery("#success-btn").attr('disabled',false);

                        return false;
                    }
                    var IFSC_Code=jQuery('#IFSC_Code').val();
                    if(jQuery.trim(IFSC_Code)!='' && IFSC_Code.length>11){
                        alert('Please Enter valid IFSC Code');
                        jQuery('#IFSC_Code').css('border-color','red');
                        jQuery("#success-btn").attr('disabled',false);

                        return false;
                    }



                });


            },
            selectRo:function(){

                jQuery('#company_code').change(function(){

                    udat=jQuery(this).val();
                    jQuery.get('check_Pedestal_Number_unique',{
                        table:'tbl_ro_master',
                        colum:'company_code',
                        unik:udat,

                        '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                    },function(data){
                        console.log(data);
                        if(data=='true'){
                            alert('Company code already exists Enter Other!');
                            jQuery('.submit-button').attr('disabled',true);

                        }else{
                            jQuery('.submit-button').attr('disabled',false);
                        }

                    });
                });


            },
           

        }
        vil.init();
    });
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "inherit" );
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "auto" );
    })
</script>
<script type="text/javascript">
    jQuery(function(){

        var vil={
            init:function(){
                vil.selectRo();
                vil.getcom();
                $("#tabs-n").tabs();
                vil.nextbutton();
                vil.getemail();
                vil.getmobile();
                vil.getprincple();
                vil.getOutlet();
                vil.checkvalidat();
                
            },
            selectRo:function(){
                jQuery('#country').on('change',function(){
                    vil.getcom();
                });
                jQuery('#pan').keyup(function(){
                vil.setvat();
                });
                jQuery('#gst').keyup(function(){
                vil.setvat();
                });
                jQuery('#state').on('change',function(){
                    vil.getcrg();
                     vil.getstatecode();
                });
                jQuery('#email').keyup(function(){
                vil.getemail();
                });
                jQuery('#mobile').on('keyup',function(){
                    vil.getmobile();
                });
                jQuery('#printpp').on('change',function(){
                    vil.getprincple();
                });
                jQuery('#Outlet').on('keyup',function(){
                    vil.getOutlet();
                });

            },
            nextbutton:function(){
                 jQuery('.nextbutton').on('click',function(){
                   var id=jQuery(this).data('id');
                   id=id;
                   //jQuery('#tabs-n ul').tabs('select',2); 
                   jQuery("#tabs-n").tabs("option", "active", id);
                });

                 
                
            },
            getemail:function(){
                      var  sta=jQuery('#email').val();
                      jQuery('#emailcody').val(sta);
             },
             getOutlet:function(){
                      var  sta=jQuery('#Outlet').val();
                      jQuery('#copycompany').val(sta);
                      jQuery('#Account_Name').val(sta);

             },
            getprincple:function(){
                      var  sta=jQuery('#printpp').val(); 
                      jQuery('#copyprincple').val(sta);
             },
            getmobile:function(){
                      var  sta=jQuery('#mobile').val();
                      jQuery('#copymobile').val(sta);
                    },
             setvat:function(){

                         
                        //var sta=jQuery('#pan').val(); 
                        var  sta=jQuery('#getstatecode').val()+jQuery('#pan').val()+jQuery('#gst').val();
                         var  view=jQuery('#getstatecode').val()+jQuery('#pan').val();
                       
                     
                      jQuery('#getsta').val(sta);
                       jQuery('#views').val(view);

                      
           },
            getcom:function(){

                jQuery.get('get_country_state',{

                    rocode:jQuery('#country').val(),

                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                },function(data){
                    var opt='';


                    jQuery.each(data, function(index,value){

                        opt+='<option value="'+index+'">'+value+'</option>';
                    });
                    console.log(opt);

                    jQuery('#state').html(opt);
                    vil.getstatecode();
                    vil.getcrg();
                });
            },
            getstatecode:function(){
               
                jQuery.get('getstatecoded',{

                    state:jQuery('#state').val(),

                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                },function(data){
                     console.log(data+'hello');
                    jQuery('#getstatecode').val(data);

                   
                });
            },
            getcrg:function(){

                jQuery.get('get_state_city',{
                    rocode:jQuery('#state').val(),
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                },function(data){

                    var opt='';

                    if(data.length==0)
                    {

                        opt+='<option value="'+jQuery('#state option:selected').val()+'">'+jQuery('#state option:selected').text()+'</option>';
                    }
                    else{


                        jQuery.each(data, function(index,value){

                            opt+='<option value="'+index+'">'+value+'</option>';
                        });
                        console.log(opt);
                    }
                    jQuery('#city').html(opt);

                });
            },
            checkvalidat:function(){
              jQuery("#next_1").on('click',function(){
                 
                     var flg=true;
                    jQuery('#tabs-1').find('input,textarea').each(function(){
                         if(jQuery(this).attr('required')){

                            var v=jQuery(this).val();
                            if(jQuery.trim(v)==''){
                              jQuery(this).css('border-color','red');
                               flg=false;
                            }else{
                               jQuery(this).css('border-color','');
                            }
                             
                              if(jQuery(this).attr('type')=='email'){

                                if(!vil.validateEmail(v,'email')) { 
                                    jQuery(this).css('border-color','red');
                                     flg=false;
                                 }
                              }else if(jQuery(this).attr('name')=='Secondary_Person_Mobile' || jQuery(this).attr('name')=='mobile'){
                                 
                                 if(!vil.validateEmail(v,'mobile') || v.length!=10) { 
                                    jQuery(this).css('border-color','red');
                                     flg=false;
                                 }

                              }else if(jQuery(this).attr('name')=='pin_code'){
                                 
                                 if(!vil.validateEmail(v,'pin_code')) { 
                                    jQuery(this).css('border-color','red');
                                     flg=false;
                                 }
                              }

                              
                         }
                    });
                    

                   if(flg){
                       var id=jQuery(this).data('id');
                       jQuery("#tabs-n").tabs("option", "active", id);
                   }

              });

             jQuery("#next-2").on('click',function(){
                     var pre=jQuery(this);
                     var flg=true;
                    jQuery('#tabs-2').find('input').each(function(){

                         var v=jQuery(this).val();
                         if(jQuery(this).attr('required')){
                            
                            if(jQuery.trim(v)==''){
                              jQuery(this).css('border-color','red');
                               flg=false;
                            }else{
                               jQuery(this).css('border-color','');
                            }
                         }

                        if(jQuery(this).attr('name')=='PAN_No'){
                           
                           console.log(v);
                           if(jQuery.trim(v)!='')
                            if(!vil.validateEmail(v,'PAN_No')) { 
                                    jQuery(this).css('border-color','red');
                                     flg=false;
                            }else{

                               jQuery(this).css('border-color','');
                            }
                        }
                         
                    });
                    var atr=false;
                    jQuery('.leftln-rigstln').each(function(){

                          var leftln=jQuery(this).find('.leftln').val();
                          var rigstln=jQuery(this).find('.rigstln').val();
                            //console.log(jQuery(this).find('.leftln').length);
                            console.log(leftln.length);
                            
                          if(leftln.length>rigstln){
                            jQuery(this).find('.leftln').css('border-color','red');
                            atr=true;
                            flg=false;
                          }else{
                            jQuery(this).find('.leftln').css('border-color','');
                          }
                    });
                    
                    if(atr){
                      alert('Please Enter in  Start from  Lenght Not Greater Serial no. Width');
                    }

                   if(flg){

                       var id=pre.data('id');
                       jQuery("#tabs-n").tabs("option", "active", id);
                   }



              });




            },
            validateEmail:function(vl,ty) {
               var emailReg;
              if(ty=='email')
               var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

              if(ty=='mobile')
               emailReg = /^[0-9-+]+$/;
              
              if(ty=='pin_code')
                emailReg=/^[0-9]{6}$/;

                if(ty=='PAN_No')
                  emailReg=/^[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}$/;
                
              return emailReg.test( vl );
            },
           


        }
        vil.init();
    });
</script>
<script>
    jQuery(function(){
        var url="{{url('check_Pedestal_Number_unique')}}";
        var vil={
            init:function(){
                vil.selectRo();

            },
            selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                 var par=jQuery(this);
                  var id=0;
                  var table=jQuery(this).data('table');
                   var colum=jQuery(this).data('colum');
                   var mess=jQuery(this).data('mass');

                   id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                     par.css('border-color','red');
                      alert(mess);
                    jQuery('.submit-button').attr('disabled',true);
                      
                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                    par.css('border-color','');
                   }
                       
                   });
              });
            
           
           },


        }
        vil.init();
    });
</script>

<script type="text/javascript">
    function gstAndPan()
    {
        var gst= document.getElementById('gst').val();
        alert(gst);

    }
    $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
     
});
    function  gstAndPan() {
        var mobile = document.getElementById("mobile").value;
        var pattern = /^[0]?[789]\d{9}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
            return true;
        }
        alert("It is not valid mobile number.input 10 digits number!");
        return false;
    }


</script>
</body>
</html>