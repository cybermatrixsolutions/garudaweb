
<!DOCTYPE html> 
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css--> 
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Stock Item  Update</h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/price_list')}}">Stock Item Master </a></li>
        </ul>
      </div>
    </div>
	
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
      
   
	
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
          <form class="form-horizontal" action="{{url('price_update')}}/{{$price_list->id}}" enctype="multipart/form-data" method="post"  onsubmit="return confirm('Do you want to Continue?');">
            
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row" style="margin-top: -15px !important;">
          
         
          
                    
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           <input type="hidden" name="price_ro_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
                        <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Pump Legal Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="price_ro_code" id="sel1">
                              @foreach($datas as $ro_master)
                                                    
                              <option name="company_id" value="{{$ro_master->RO_code}}"
                             @if($price_list->RO_Code == $ro_master->RO_code) selected @endif>{{$ro_master->pump_legal_name}}</option>
                             @endforeach
                            </select>
                          </div>
                        </div> -->
                        <div class="form-group">
                        <label class="control-label col-sm-3" for="pwd">Item Code <span style="color:#f70b0b;">*</span></label>
                        <div class="col-sm-9">
                          <input type="text" required="required" class="form-control " id="" name="price_item_list" value="{{$price_list->Item_Code}}" placeholder="Volume Liter">

                         
                        </div>
                      </div>
                      
                       
                   
                      <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Item Name <span style="color:#f70b0b;">*</span></label>
                        <div class="col-sm-9">
                           <input type="text" required="required" class="form-control" id="" name="item_name" value="{{$price_list->Item_Name}}" placeholder="Item Name">
                        </div>
                      </div>

                      <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Stock Group <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="Stock_Group" id="stockgroup">
                             
                                                    @foreach($stock_group as $stock)
                               <option  value="{{$stock->id}}" @if($price_list->Stock_Group==$stock->id) selected @endif >{{$stock->Group_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Sub Group <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control" name="Sub_Stock_Group" id="subgroupaa" data-select="{{$price_list->Sub_Stock_Group}}">
             
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Unit of Measure <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                                <select class="form-control" required="required" name="Unit_of_Measure" id="Unit_of_Measure">
                             
                                                    @foreach($unit_measure as $unit)
                                                        <option  value="{{$unit->id}}"
                                                                @if($price_list->Unit_of_Measure==$unit->id) selected @endif  >{{$unit->Unit_Symbol}}-{{$unit->Unit_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                      
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Tail Unit </label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control numeriDesimal" value="{{$price_list->Tail_Unit}}"  id="" name="Tail_Unit" placeholder="Tail Unit">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Conversion</label>
                          <div class="col-sm-9">
                             <input type="text"   class="form-control numeriDesimal" value="{{$price_list->Conversion}}"  name="Conversion" id="" placeholder="Conversion">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Alternate Unit</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control numeriDesimal" value="{{$price_list->Alternate_Unit}}"  name="Alternate_Unit" id="" placeholder="Alternate Unit">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row" style="margin-top: -15px !important;">
                       <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">LOB <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="LOB" id="sel1">
                             
                                          @foreach($lob as $lobc)
                                    <option  value="{{$lobc->lob_name}}"
                                             @if($price_list->LOB == $lobc->lob_name) selected @endif>{{$lobc->lob_name}}</option>
                                @endforeach
                            </select>
                            
                            
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Brand</label>
                          <div class="col-sm-9">
                             <input type="text"  value="{{$price_list->brand}}" class="form-control"  name="brand" id="" placeholder="brand">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">GST/VAT <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="gst_vat"  id="taxttype">
                               
                                   <option value="GST"  @if($price_list->gst_vat == 'GST') selected @endif>GST</option>
                                   <option value="VAT" @if($price_list->gst_vat == 'VAT') selected @endif>VAT</option>
                                                
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Tax</label>
                          <div class="col-sm-7">
                            <select class="form-control" id="tags" multiple="multiple"  name="tax[]" disabled>
                             
                            @foreach($taxmaster as $tax)
                                <option value="{{$tax->id}}" @if(in_array($tax->id,$rod->where('is_active',1)->pluck('tax_id')->toArray())) selected @endif>{{$tax->Tax_Code}}</option>
                            @endforeach
                            </select>
                          </div>
                        </div> 
                        
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">HSN/SAC</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control"  value="{{$price_list->hsncode}}" name="hsncode" id="hsncode" placeholder="hsncode">
                          </div>
                        </div>
                          <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Open Stock</label>
                        <div class="col-sm-9">
                           <input type="text"  class="form-control numeriDesimal" id="" name="volume_liter" value="{{$price_list->Volume_ltr}}" placeholder="Open Stock">
                        </div>
                      </div>
                         
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Active From Date</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control " name="Active_From_Date" id="datepicker" placeholder="Active From Date" value="{{Carbon\Carbon::parse($price_list->Active_From_Date)->format('d/m/Y')}}">
                          </div>
                        </div>
                       
                       
                        <div class="form-group">
            
                          <div class="col-sm-offset-3 col-sm-9">
                           <!-- <button type="submit" class="btn btn-default">Submit</button>-->
                           <center><input type="submit" id="success-btn" class="btn btn-primary site-btn" value="Submit"></center>
                          </div>
                        </div>
           
                      </form>
                    </div>
          </div>
        </div>
      </div>

    </div>
	</div>
	
  </div>
  <footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
           

            $(function () {
            $("#datepicker").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true,
                  format: 'dd/mm/yyyy',
            });
          });
        </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
 $('#tags').select2({
   data: [],
    tags: true,
    tokenSeparators: [','], 
    placeholder: "select Tax"
});


</script>
<script>
  $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})


</script>


<script type="text/javascript">
 jQuery(function(){
     var url ="{{url('getrocodetx')}}";
     var vil={
           init:function(){
             vil.selectRo();
             vil.getcrg();
                vil.getstock();
                vil.checkNumericDesimal();

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcrg();
              });
            jQuery('#taxttype').on('change',function(){
                  vil.getcrg();
              });
            jQuery('#stockgroup').on('change',function(){
                  vil.getstock();
              });
           },
            checkNumericDesimal:function () {
                    jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });

                    jQuery('#hsncode').on('keyup',function(){
                          var hsn= jQuery(this).val();
                          if(jQuery.trim(hsn).length>8){
                            jQuery('#success-btn').attr('disabled',true);
                            jQuery(this).css('border-color','red');
                            alert('max length is 8 digit');
                          }else{
                            jQuery('#success-btn').attr('disabled',false);
                             jQuery(this).css('border-color','');
                          }

                    });
                },
           getcrg:function(){
            
             jQuery.get(url,{
                rocode:jQuery('#sel1').val(),
                taxttype:jQuery('#taxttype').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';

                  


                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                  
                jQuery('#tags').html(opt);

               });
           },
           getstock:function(){
             
               jQuery.get("{{url('/getstockupdategroup')}}",{
                
                stockgroup:jQuery('#stockgroup').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var sr=jQuery('#subgroupaa').data('select');
                var opt='';
                  jQuery.each(data, function(index,value){
                       if(sr==index)
                        opt+='<option selected value="'+index+'">'+value+'</option>';
                      else
                         opt+='<option value="'+index+'">'+value+'</option>';

                  });
                    console.log(opt);
                   
                   jQuery('#subgroupaa').html(opt);

                   vil.getcrg();
               });
           },


     }
     vil.init();
  });
</script>

</body>
</html>