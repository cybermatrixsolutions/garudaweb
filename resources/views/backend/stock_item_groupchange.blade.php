<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
   @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
  <!-- Side-Nav-->
  
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Stock Item Group Update</h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('stock_item_group')}}">Stock Item Group</a></li>
        </ul>
      </div>
    </div>
	
	                        <div class="">
							       <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                            </div>
	
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
           
           
          @foreach($stockItemData as $row)
                      <form class="form-horizontal"  action="{{url('stock_item_group_edit')}}/{{$row->id}}" enctype="multipart/form-data" method="post">
               <input type="hidden" name="_token" value="{{ csrf_token()}}"/> 
            <!-- 
                        <div class="form-group">
            
                          <label class="control-label col-sm-3" for="email">Group Code</label>
                          <div class="col-sm-9">
                           <input type="text" class="form-control checkunique" id="" name="Group_Code" value="{{$row->Group_Code}}" placeholder="Group Code" required data-table="tbl_stock_item_group" data-colum="Group_Code" data-mess="Group Code Already Exits" data-id="{{$row->id}}"/>
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Group Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" id="" name="Group_Name" value="{{$row->Group_Name}}" placeholder="Group Name" required/>
                          </div>
                        </div>
                        @if($row->parent==0)
                        <input type="hidden" name="category" value="0">
                        @else
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Parent</label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="category">
                              <option value="0">Select Parent</option>
                              @foreach($getStockItem->where('parent',0) as $rows)
                                @if($row->id!=$rows->id)
                                  <option  value="{{$rows->id}}" @if($rows->id==$row->parent) selected @endif >{{$rows->Group_Name}}</option>
                                  @endif
                              @endforeach
                            </select>
                          </div>
                        </div>
                        @endif
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <!--<button type="submit" class="btn btn-default">SUBMIT</button>-->
                        <center><input type="submit" id="success-btn" class="btn btn-primary site-btn submit-button" value="Submit"></center>
                          </div>
                        </div>
                      </form>
            @endforeach
                    </div>
            <!-- Modal -->
           
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script> 
<script>
  jQuery(function(){
     var url="{{url('check_Pedestal_Number_unique')}}";
     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                   var colum=jQuery(this).data('colum');
                   var mess=jQuery(this).data('mess');

                   id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mess);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>