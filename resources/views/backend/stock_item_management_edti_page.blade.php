<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Common Item  Update 
</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/stock_item_management')}}">Common Item 
 </a></li>
        </ul>
      </div>
    </div>
	
	
	                         <div class=""> @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
      
    </div>
	
	
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
      
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row" style="margin-top:-32px!important;">
          
         <form class="form-horizontal" action="{{url('stock_update')}}/{{$personal_mangement_data->id}}" enctype="multipart/form-data" method="post">
            
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
          
                       <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Principal Company Name</label>
                         <div class="col-sm-8">
                            <select disabled="disable" class="form-control" name="Company_Code" id="sel1">
                             
                                                    @foreach($principle_company as $principle)
                                                        <option value="{{$principle->company_code}}"
                                                                @if($personal_mangement_data->Company_Code == $principle->company_code) selected @endif >{{$principle->company_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Stock Item Code <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <input type="text" disabled="disable"  required="required"   class="form-control checkunique" data-table="tbl_stock_item_master"  data-colum="Stock_Item_Code" data-id="{{$personal_mangement_data->id}}" data-mass="Stock Item Code Already Exist"  name="Stock_Item_Code" value="{{$personal_mangement_data->Stock_Item_Code}}" id="" placeholder="Stock Item Code" readonly>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Stock Item Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <input type="text" required="required" class="form-control" name="Stock_Item_Name" value="{{$personal_mangement_data->Stock_Item_Name}}" id="" placeholder="Stock Item Code">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Stock Group <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <select class="form-control" required="required" name="Sub_Stock_Group" id="dtockgrpuco">
             
                                    @foreach($stock_group as $stock)
                                        <option @if($stock->id==$personal_mangement_data->Stock_Group) selected @endif value="{{$stock->id}}"
                                                >{{$stock->Group_Name}}</option>
                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Sub Group <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <select class="form-control" name="Stock_Group" id="substocks" data-id="{{$personal_mangement_data->Sub_Stock_Group}}">
                            
                            </select>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Unit of Measure <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                                <select class="form-control" name="Unit_of_Measure" id="sel1">
                             
                                                    @foreach($unit_measure as $unit)
                                                        <option  value="{{$unit->id}}"
                                                                 @if($personal_mangement_data->Unit_of_Measure == $unit->id) selected @endif>{{$unit->Unit_Symbol}}-{{$unit->Unit_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Active From Date</label>
                          <div class="col-sm-8">
                          
                             <input type="text" class="form-control datepickers" name="Active_From_Date" value="{{Carbon\Carbon::parse($personal_mangement_data->Active_From_Date)->format('d/m/Y')}}" id="" >
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Tail Unit</label>
                          <div class="col-sm-8">
                             <input type="text"  class="form-control numeriDesimal" id="" readonly name="Tail_Unit" value="{{$personal_mangement_data->Tail_Unit}}" placeholder="Tail Unit">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Conversion</label>
                          <div class="col-sm-8">
                             <input type="text" class="form-control numeriDesimal" readonly name="Conversion" id="" value="{{$personal_mangement_data->Conversion}}" placeholder="Conversion">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Alternate Unit</label>
                          <div class="col-sm-8">
                             <input type="text"  class="form-control numeriDesimal" readonly name="Alternate_Unit" id="" value="{{$personal_mangement_data->Alternate_Unit}}" placeholder="Alternate Unit">
                          </div>
                        </div>
                         <!-- <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">LOB</label>
                          <div class="col-sm-8">
                            <input type="hidden" name="LOB" value="{{$personal_mangement_data->LOB}}">
                            <input type="text" class="form-control" disabled value="{{$personal_mangement_data->LOB}}">

                          
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Brand</label>
                          <div class="col-sm-8">
                             <input type="text"  class="form-control" value="{{$personal_mangement_data->brand}}" name="brand" id="" placeholder="brand">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">HSN/SAC</label>
                          <div class="col-sm-8">
                             <input type="text"  class="form-control" maxlength="10"  value="{{$personal_mangement_data->hsncode}}" name="hsncode" id="myField" placeholder="hsncode">
                          </div>
                        </div>
                     
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">GST/VAT</label>
                          <div class="col-sm-8">
                            <select class="form-control" required="required" name="gst_vat">
                               
                                   <option @if($personal_mangement_data->gst_vat == 'GST') selected @endif value="GST">GST</option>
                                   <option @if($personal_mangement_data->gst_vat == 'VAT') selected @endif value="VAT">VAT</option>
                                                
                            </select>
                          </div>
                        </div>

                        <div class="form-group">
            
                          <div class="col-sm-offset-3 col-sm-9">
                           
              <center><input type="submit" id="success-btn" class="btn btn-primary site-btn submit-button" value="Submit"></center>
                          </div>
                        </div>
           
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script type="text/javascript">


         

  $(function () {
  $(".datepickers").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
</script>

<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})

    $("#myField").keyup(function() {
    $("#myField").val(this.value.match(/[0-9]*/));
</script>
<script type="text/javascript">
 jQuery(function(){

  var url1="{{url('check_Pedestal_Number_unique')}}";

     var vil={
           init:function(){
             vil.selectRo();
             vil.getstockedtit();
           },
          selectRo:function(){
              jQuery('#dtockgrpuco').on('change',function(){
                  vil.getstockedtit();
              });
            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get(url1,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          getstockedtit:function(){
              jQuery.get("{{url('/getstrocksubgroupedit')}}",{
             
                
                dtockgrpuco:jQuery('#dtockgrpuco').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                 var s=jQuery('#substocks').data('id');
                  jQuery.each(data, function(index,value){
                    if(s==index)
                     opt+='<option selected value="'+index+'">'+value+'</option>';
                   else
                    opt+='<option value="'+index+'">'+value+'</option>';

                  });
                    console.log(opt);
                   
                   jQuery('#substocks').html(opt);

                   vil.getcrg();
               });
           },
     }
     vil.init();
  });
</script>-->

<script type="text/javascript">
  jQuery(".numeriDesimal").keyup(function() {
 var $this = jQuery(this);
 $this.val($this.val().replace(/[^\d.]/g, ''));
        });
</script>
</body>
</html>