<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
<style type="text/css">
  input.form-control.abhi {
    border: 0px;
    background-color: #E5E5E5;
}

</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Create Fuel & Items Bills</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/transaction_list')}}"> Create Fuel & Items Bills</a></li>
        </ul>
      </div>
    </div>
  
  
                           <div class="">  <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                             </div>
 <div class="row">
    <div class="col-md-11 col-md-offset-1" style="margin-top: 79px;;">
    <form class="form-horizontal form-inline" name="myForm" action="{{'invoiceindex'}}" enctype="multipart/form-data" method="post">
       
            
             {{ csrf_field() }}
        <div class="row">
         <div class="col-md-12">
          <div class="form-group">
            <label class="control-label" for="pwd">Select Type <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
             <select class="form-control" name="fule_type" >
                     <option value="2">GST</option>
                      <option value="1">VAT</option>
              </select>

             </div>
          <div class="form-group">
             <label class="control-label"  for="email">Customer Name <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                 <select class="form-control" required name="RO_code" id="sel1">
                    
                    @if(isset($tbl_customer_transaction))
                      @foreach($tbl_customer_transaction as $getrocodecust)
                       <option  value="{{$getrocodecust->Customer_Code}}">{{$getrocodecust->company_name}}</option>
                      @endforeach
                    @endif
                    </select>
          </div>

          &nbsp;&nbsp;
                        <div class="form-group">
                          <label class="control-label"  for="email">From <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                           <input type="text" required class="form-control" id="datepicker"  name="fromdate" placeholder="from date"/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                              <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                                <input required type="text" class="form-control" id="datepickers" name="to_dae" placeholder="To Date"/>&nbsp;&nbsp;
                            </div>
                        <div class="form-group">
                                <input type="submit" class="btn btn-default" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                            </div>
            </div>
        </div>
 </form> 
    </div>
    </div>
  <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          @if(isset($Customerinvoices))
          <table class="table table-striped" id="myTable">
            <thead>
              
              <tr>
                <th>S.No.</th>
                <th>Customer Name</th>
                <th>Invoice NO</th>
               
                <th>Date</th>
                <th>Download PDF</th>
                
               
            </thead>
                      
             <tbody>
               <?php $i=0; ?>
               @foreach($Customerinvoices as $invoice)
                
               <?php $i++;?>
              <tr>
                <td>{{$i}}</td>
                <td scope="row">{{$invoice->getcustomer->Customer_Name}}</td>
                <td scope="row">{{$invoice->invoice_no}}</td>
                <td scope="row">{{date('d/m/Y',strtotime($invoice->created_at))}}</td>
                
                <td scope="row"><a href="{{$invoice->invoice_path}}" target="_blank">Download</a></td>
               
              </tr>
              @endforeach
              
            </tbody>
           
          </table>
          @endif
       
          </div>
         </div>
      </div>
    </div>
  
  
  
  
  </div>
</div>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
  $(function () {
  $("#datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        defaultDate:'dd/mm/yyy',
        format: 'dd/mm/yyyy',
  }).datepicker('update');
});
  $(function () {
  $("#datepickers").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        defaultDate:'dd/mm/yyy',
        format: 'dd/mm/yyyy',
  }).datepicker('update');
});

</script>

<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
</body>
</html>