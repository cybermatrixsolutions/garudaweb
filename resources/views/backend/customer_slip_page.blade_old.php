<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
   @include('backend-includes.header')

        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Fuel Slip Entry</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Fuel Slip Entry</a></li>
        </ul>
      </div>
    </div>

       <div class="error-class" >
         @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
       @endif
          
           <center>
            @if(Session::has('success'))
               <font style="color:red">{!!session('success')!!}</font>
            @endif
          </center>

        </div>
                         
      <form class="form-horizontal form-horizontal-error" id="form12" action="{{'save_slip_detail_data'}}" method="post" >
          {{ csrf_field() }}
          <input type="hidden" name="RO_code" id="RO_code" value="{{Auth::user()->getRocode->RO_code}}">
         
        
        <table class="table table-striped no-footer">
          <thead>
            <tr>
              <th>Customer Name</th>
              <th>Vehicle Reg No</th>
              <th>Fuel Type</th>
              <th>Slip No</th>
              <th>Dispense Date</th>
              <th>Shift</th>
              <th>Quantity </th>
              <th>Price </th>
              <th>Net Value <a style="float:right" id="addNew" class="btn btn-success" href="#">+</a></th>
            </tr>
          </thead>
          <tbody id="parant-app-row">
           <tr class='parent-tr' id="parant-row">
            <td>  
              <select class="form-control customer_code" required="required"  name="customer_code[]"></select>
            </td>

            <td>
               <select class="form-control registration_number" required="required" name="Vehicle_Reg_No[]" ></select>

            </td>
            <td>
                <select class="form-control fuel" required="required" name="Fuel_Type[]">
                   @foreach($fuel_type as $fuel) 
                  <option value="{{$fuel->id}}" >{{$fuel->Item_Name}}</option>
                  
                   @endforeach
               </select>
            </td>
           
            <td>
               <input type="text" class="form-control slip_detail numeriDesimal"   name="slip_detail[]" placeholder="Slip Detail">
               <span class="error_span" style="color: red" ></span>
            </td>
            <td>
                <input class="form-control datepicker1"  required="required" name="request_date[]"  type="text" />
        
            </td>
            <td>
               <select class="form-control shift" name="shift[]" required="required"></select>

            </td>
          
            <td>
              <input style="text-align: right;" type="text" step="0.01" data-qt="0" class="form-control petroldiesel_qty numeriDesimal" required="required"   name="petroldiesel_qty[]" placeholder="Quantity Value ">
            </td>

            <td>
               
               <input class="form-control itemprice" readonly style="text-align: right;width: 100px;" required="required" name="itemprice[]" type="text" />
        
            </td>

            <td>
               <a href='#' style="display: none;" class='deleteNew'>X</a>
      
               <input readonly class="form-control netvalue" style="text-align: right;width: 100px;" required="required" name="netvalue[]" type="text" />
        
            </td>

           </tr>
           
          </tbody>


        </table>
      

      
      
        <div class="form-group">
          <div>
            <button type="button" class="btn btn-default pull-right" id="tbnhide">Submit</button>
          </div>
        </div>
      </form>
   

   
 </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script  src="{{URL::asset('js/index.js')}}"></script> 
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

 jQuery(".numeriDesimal").keyup(function()
  {
    var $this = jQuery(this);
    $this.val($this.val().replace(/[^\d.]/g, ''));
    });
</script>
<script>
jQuery('.table-responsive').on('show.bs.dropdown', function () {
     jQuery('.table-responsive').css( "overflow", "inherit" );
});

jQuery('.table-responsive').on('hide.bs.dropdown', function () {
     jQuery('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">





 jQuery(function(){
      var capacitys =0;
      var capacity ;
      var currentP=jQuery('#parant-row');
      var vil={
           init:function(){
              vil.selectRo();
              vil.getcom();
              vil.reqType();
              vil.addRow();
              vil.formsub();
              vil.slipunique();
            

               jQuery(document).on('keyup','.petroldiesel_qty',function(){
                    currentP=jQuery(this).parents('.parent-tr');
                    vil.netvalue();
              });

               jQuery(".datepicker1").datepicker({ 
                    autoclose: true, 
                    todayHighlight: true,
                    format: 'dd/mm/yyyy',
              }).on('changeDate', function(){
                       currentP=jQuery(this).parents('.parent-tr');
                       vil.getItemPrice();
                       vil.getShift();
                    });


           },
           /*it is used for make RO_code uniqe for multiple field by vijay*/
         slipunique:function () {
                 jQuery(document).on('mouseout','.slip_detail',function () {  
                    var currentP=jQuery(this).parents('.parent-tr');
                   // alert(current);
                      var customer=currentP.find('.customer_code').val();
                      var slip_detail = jQuery(this).val();
                      if(jQuery.trim(slip_detail)!=''){


                       var RO_code = jQuery('#RO_code').val();
                          currentP.find('.error_span').html('');
                          jQuery.get("{{route('slipunique')}}",{
                              slip_detail:slip_detail,
                              RO_code:RO_code,
                              customer:customer,
                              '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                          },function(response){
                              if(response=="OK")
                              {
                                  currentP.find('.error_span').html('');
                                  jQuery('#tbnhide').attr("disabled",false);
                              }
                              else
                              {
                                  currentP.find('.error_span').html(response);
                                  jQuery('#tbnhide').attr("disabled",true);

                              }
                          });
                      }
                  });
              },
             addRow:function(){
                        jQuery('#addNew').on('click',function(e){
                          e.preventDefault();
                          /*var cnt=jQuery('#parant-row').html();
                          jQuery('#parant-app-row').append("<tr class='parent-tr'>"+cnt+"</tr>");
                          //currentP=jQuery('#parant-app-row').last();
                          currentP=jQuery('#parant-app-row tr:last-child');*/

                          var cnt=jQuery('#parant-row').clone();
                          cnt.removeAttr('id').appendTo('#parant-app-row').find('input,select').each(function(){
                             
                             if(jQuery(this).hasClass('slip_detail')){
                                jQuery(this).val('');
                             }else if(jQuery(this).hasClass('datepicker1')){
                                jQuery(this).val('');
                             }else if(jQuery(this).hasClass('itemprice')){
                              jQuery(this).html('');
                             }else if(jQuery(this).hasClass('petroldiesel_qty')){
                              jQuery(this).val('');
                             }
                              
                              jQuery(this).prev().show();
                               jQuery(this).next().html('');
                             
                          });
                          
                             
                               console.log(currentP);

                            jQuery(".datepicker1").datepicker({ 
                                  autoclose: true, 
                                  todayHighlight: true,
                                  format: 'dd/mm/yyyy'
                            }).on('changeDate', function(){
                                   currentP=jQuery(this).parents('.parent-tr');
                                   vil.getItemPrice();
                                   vil.getShift();
                                   console.log('hello');
                                });

                            
                            //vil.getcapcity();
                        });


                        jQuery(document).on('click','.deleteNew',function(e){
                          e.preventDefault();
                          
                          if(jQuery(this).parents('.parent-tr').index()!=0)
                            jQuery(this).parents('.parent-tr').remove();
                            
                        });

                        
                       },


           selectRo:function(){

            jQuery('#RO_code').on('change',function(){
                  vil.getcom();
                  currentP=jQuery(this).parents('.parent-tr');
              });
            jQuery(document).on('change','.customer_code',function(){
                  currentP=jQuery(this).parents('.parent-tr');
                  
                  vil.getcrg();
              });
             jQuery(document).on('change','.registration_number',function(){
                  currentP=jQuery(this).parents('.parent-tr');
                  //vil.getcapcity();
                  vil.getFuel();
              });

             jQuery(document).on('keyup change','.petroldiesel_qty',function(){
                  var qt=jQuery(this).data('qt');
                  var vt=jQuery(this).val();
                //vil.getcpcity();
                     //console.log(vt);
                     //console.log(qt);
                    /*if(vt>qt){

                      alert("Please Enter Below Capacity Limit ");

                    }else{

                       jQuery('#tbnhide').show();
                    }*/
             });

             jQuery(document).on('change','.fuel',function(){
                  currentP=jQuery(this).parents('.parent-tr');
                   vil.getItemPrice();
              });

             
              /*jQuery('.petroldiesel_type').on('change',function(){
                    parent=jQuery(this);
                var petroldieselty = jQuery('#petroldiesel_type').val();
                 if ('Rs'==petroldieselty) {


                 } 

                 else{
                  jQuery('#petroldiesel_qty').val("");
                 }
              vil.getcpcity();

             });*/

            console.log(currentP);
           },
           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#RO_code').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                  
                   currentP.find('.customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:currentP.find('.customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                   
                  currentP.find('.registration_number').html(opt);
                    //vil.getcapcity();
                    vil.selectFuleType();
                     vil.getFuel();
               });
           },
           getcapcity:function(){
            
             jQuery.get('getcapcitys',{
                registration_number:currentP.find('.registration_number').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
             
                 
                 capacitys = parseInt(data);
                 console.log(capacitys);
                   
                 currentP.find('.petroldiesel_qty').val(data);
                 currentP.find('.petroldiesel_qty').data('qt',data);
              
                 vil.selectFuleType();
               });
           },
          
         getcpcity:function(){
              var rcapacity=parseInt(currentP.find('.petroldiesel_qty').val());
              var petroldiesel_type=currentP.find('.petroldiesel_type').val();   
              if('Rs'!=petroldiesel_type){
                  
                  
                    if(rcapacity>=capacitys){
                       jQuery('#tbnhide').hide();
                      alert("Please Enter Below Capacity Limit ");
                    }
                    else{
                              jQuery('#tbnhide').show();
                            }
                  }
                  else{
                     
                    jQuery('#tbnhide').show();
                  }
               },
           selectFuleType:function(){
            var registration_number=currentP.find('.registration_number').val();
             jQuery.get('getVihicleFuleType/'+registration_number,{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                //jQuery('#fuel select').val(data);
                //currentP.find('.fuel option[value='+data+']').attr('selected','selected');
                currentP.find('.fuel').val(data).change();
               });
           },

           reqType:function(){
               jQuery('#Request_Value').hide();
              jQuery('#request_type').on('change',function(){
                    var t=jQuery(this).val();
                    if(t=='Full Tank'){
                      jQuery('#Request_Value').hide();
                      jQuery('#Request_Value_input').attr('required',false);
                    }
                    else{
                      jQuery('#Request_Value').show();
                      jQuery('#Request_Value_input').attr('required',true);

                    }

              });
           },
          
           formsub:function(){
                jQuery('#tbnhide').on('click',function(){
                    var flg=true;
                    var q=true;
                    jQuery('#parant-app-row').find("input, textarea, select").each(function(){
                     
                       if(!jQuery(this).hasClass('shift'))
                       if(jQuery.trim(jQuery(this).val())==''){
                          jQuery(this).css('border-color','#e27777');
                          flg=false;
                       }else{
                         jQuery(this).css('border-color','');
                       }

                        if(jQuery(this).hasClass('itemprice')){
                           if(jQuery.trim(jQuery(this).val())=='' || jQuery.trim(jQuery(this).val())==0){
                              jQuery(this).css('border-color','#e27777');
                              flg=false;
                           }else{
                             jQuery(this).css('border-color','');
                           }
                         }

                        /*if(jQuery(this).hasClass('petroldiesel_qty')){
                           console.log(parseInt(jQuery(this).data('qt'))+'=='+parseInt(jQuery(this).val()));
                          if(parseInt(jQuery(this).data('qt'))<parseInt(jQuery(this).val())){
                             jQuery(this).css('border-color','#e27777');
                             flg=false;
                             q=false;
                           }
                        }*/
                    });
                   
                   if(q==false){

                      alert("Please Enter Below Capacity Limit ");
                   }

                    if(flg==true && q==true){
                      jQuery('#form12').submit();
                    }

                });
           },
           getItemPrice:function(){
             var item=currentP.find('.fuel').val();
             var request_date=currentP.find('.datepicker1').val();

             if(request_date){
                  currentP.find('.itemprice').html('');
                 jQuery.get('getietmpricebydate/'+item,{
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                    'request_date':request_date,
                   },function(data){
                        if(data==0){
                           alert('Please Enter Price for the Date');
                           currentP.find('.itemprice').css('border-color','#e27777');
                           currentP.find('.datepicker1').css('border-color','#e27777');
                        }else{
                            currentP.find('.itemprice').css('border-color','');
                           currentP.find('.datepicker1').css('border-color','');
                            var str='';
                            /*jQuery.each(data,function(i,v){
                               str+='<option value="'+v+'">'+v+'</option>';
                            });*/
                           currentP.find('.itemprice').val(data);
                           vil.netvalue();
                        }
                     
                   });
               }
           },
            getShift:function(){
             
             var request_date=currentP.find('.datepicker1').val();

             if(request_date){

                 jQuery.get('getShiftByDate',{
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                    'request_date':request_date,
                   },function(data){
                    console.log(data);
                      var str='';
                      jQuery.each(data,function(i,v){
                         str+='<option value="'+i+'">'+v+'</option>';
                      });
                     currentP.find('.shift').html(str);
                   });
               }
           },
           getFuel:function(){
              var ro=jQuery('#RO_code').val();
              var cus=currentP.find('.customer_code').val();
              var registration_number=currentP.find('.registration_number').val();
              
             jQuery.get('getfueltype/'+ro+'/'+cus+'/FUEL',{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                registration_number:registration_number,
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                   
                   
                currentP.find('.fuel').html(opt);
               });
           },
           netvalue:function(){
            
               var netvalue=0;
               var price=parseFloat(currentP.find('.itemprice').val());
               var qty=parseFloat(currentP.find('.petroldiesel_qty').val());

                if(!isNaN(qty) && !isNaN(price))
                  netvalue=qty*price;
                 
                 netvalue.toFixed(2);
               currentP.find('.netvalue').val(netvalue.toFixed(2));


           },

     }
     vil.init();
  });
</script>
</body>
</html>