<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  

  <!-- Side-Nav-->

  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Unit of Measures</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Unit of Measures</a></li>
        </ul>
      </div>
    </div>
     <div class="">
	       
                </div>
                 <div class="">
                <center>@if(Session::has('success'))
                       <font style="color:red">{!!session('exitdata')!!}</font>
                    @endif</center>
      </div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
          <center>@if(Session::has('success'))
                       <font style="color:red">{!!session('success')!!}</font>
                    @endif</center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal> -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Add Unit of Measure	</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal" id="form1" action="{{'add_unit_measure'}}" enctype="multipart/form-data" method="post"  novalidate>
					  
					      <input type="hidden" name="_token" value="{{ csrf_token()}}"/> 
                       <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Simple / Compound <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                           <select class="form-control" id="type"  name="Simple_Compound" required/>
                              <option value="Simple">Simple</option>
                              <option value="Compound">Compound</option>
                              
                            </select>  
                          </div>
                        </div>
                        <div class="row" id="arow_dim">
                          <div class="form-group row_dim">
                          <label class="control-label col-sm-4" for="pwd">Select First Unit</label>
                          <div class="col-sm-7">

                           <select class="form-control" id="type2"  name="first_unit" required/>
                              <option value="">Select First unit</option>
                           @foreach($getUnitMeasure as $row) 
                        
                              <option value="{{$row->Unit_Symbol}}">{{$row->Unit_Symbol}}</option>
                            
                             @endforeach
                            </select>  
                          </div>
                        </div>
                        <div class="form-group row_dim">
                          <label class="control-label col-sm-4" for="pwd">Select Second Unit</label>
                          <div class="col-sm-7">
                           <select class="form-control" id="type1"  name="second_unit" required/>
                            <option value="">Select Second unit</option>
                             @foreach($getUnitMeasure as $row) 
                            
                              <option value="{{$row->Unit_Symbol}}">{{$row->Unit_Symbol}}</option>
                            
                             @endforeach
                              
                             
                            </select>  
                          </div>
                        </div>
                         
                         <div class="form-group row_dim">
                          <label class="control-label col-sm-4" for="pwd">Convetions <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                          <div class="row">
                            <div class="col-sm-5">
                             <input type="number"  min="0" class="form-control checkunique number" 
                               name="conv_1"  required/> 
                            </div>
                            <div class="col-sm-5">
                             <input type="number" min="0" class="form-control checkunique number" 
                               name="conv_2" placeholder="Unit Symbol" required 
                               /> 
                            </div>
                          </div> 
                          </div>
                        </div>
                      
                       
                      </div>

                        <div id="utint">
                       <div class="form-group">
                          <label class="control-label col-sm-4" for="Unit Symbol">Unit Symbol <span style="color:#f70b0b;">*</span>	</label>
                          <div class="col-sm-8">
                          	<input type="text"  required class="form-control checkunique" id="Unit_Symbol"  name="Unit_Symbol" placeholder="Unit Symbol" required data-table="tbl_unit_of_measure" data-colum="Unit_Symbol" data-mess="Unit Symbol Already Exits"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4"  for="Unit Name">Unit Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                          	<input type="text" required class="form-control checkunique" name="Unit_Name" id="Unit_Name" placeholder="Unit Name" required data-table="tbl_unit_of_measure" data-colum="Unit_Name" data-mess="Unit Name Already Exits"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="No. of Decimals">No. of Decimals <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="number" step="1"   min="0" max="9" required class="form-control number" name="No_of_Decimals" id="No_of_Decimals" value="0" placeholder="No_of_Decimals" required/>
                            
                          </div>
                        </div>
                      </div>
                       
                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <!--<button type="submit" class="btn btn-default">Submit</button>-->
							               <center><input type="button" id="success-btn" class="btn btn-primary site-btn submit-button" value="Submit"></center>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
		   <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <a href="{{route('download_csv_unitofmeasure')}}" class="btn btn-primary pull-right btn-sm RbtnMargin" >Download CSV File</a>


          </div>

		  
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
			     <th>S.No</th>
                <th>Unit Symbol</th>
                <th>Unit Name</th>
                <th>No.of Decimals</th>
                <th>Simple/Compound</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
			<?php $i=0; ?>
			
	            @foreach($getUnitMeasure as $row) 
				
	         <?php $i++ ;?>
			
              <tr>
			  <td>{{$i}}</td>
                <td scope="row">{{$row->Unit_Symbol}}</td>
                <td>{{$row->Unit_Name}}</td>
                <td>{{$row->No_of_Decimals}}</td>
                <td>{{$row->Simple_Compound}}</td>
					<td> <?php if($row->is_active==1){?>

								   <a  title="Active" href="{{URL('unitMeasureDeactive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to deactivate?');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

									<?php }else{ ?>

									<a title="Deactive" href="{{URL('unitMeasureActive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to activate?');">

									  <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

									  </a>
						 <?php } ?> 
					</td>
				
                    <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                       <!--<li><a href="#">View</a></li>-->
                        @if($row->is_active==1)
                          <li><a href="unit_measure_update/{{$row->id}}">Edit</a></li>
                        @endif
					  
                    <!--  <li><a href="unit_measure_delete/{{$row->id}}" onClick="return confirm('Are you sure?');">Delete</a></li>-->
                    </ul>
                  </div></td>
              </tr>
			  
			 @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src="{{URL::asset('js/capitalize.js')}}"></script> 
<script type="text/javascript">
jQuery(function(){
   jQuery('#type1').on('change',function(){
        var a = document.getElementById("type2");
    var b = document.getElementById("type1");

    if (a.options[a.selectedIndex].value == b.options[b.selectedIndex].value) {
        alert("Please Choose Select unit symble");
         jQuery('.site-btn').attr('disabled',true);
    } else{
       jQuery('.site-btn').attr('disabled',false);


    }
   }); 
});
$(".number").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

$('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
</script>
<script type="text/javascript">
  $(function() {
     $('.row_dim').hide(); 
    $('#type').change(function(){
        if($('#type').val() == 'Compound') {
            $('.row_dim').show(); 
             //jQuery('#utint').css("display","none")
        } else {
            //$('#utint').show();
            jQuery('.row_dim').css("display","none")
        } 
    });
    
});
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
<script>
  jQuery(function(){
     var url="{{url('check_Pedestal_Number_unique')}}";
     var vil={
           init:function(){
             vil.selectRo();
             vil.validation();
             jQuery('#No_of_Decimals').on('keyup',function(){
              var daarray=[0,1,2,3,4];
                 var v=jQuery(this).val();
                 
                 if(jQuery.trim(v)!='')
                 if(jQuery.inArray(parseInt(v),daarray)!=-1){
                    jQuery(this).css('border-color','');
                    jQuery('#success-btn').attr('disabled',false);
                 }else{
                   
                  alert('Please Enter value between 0 to 4');
                  jQuery(this).css('border-color','#ff0000');
                  jQuery('#success-btn').attr('disabled',true);
                 }

             });
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                   var colum=jQuery(this).data('colum');
                   var mess=jQuery(this).data('mess');

                   id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mess);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
           validation:function(){
               jQuery('#success-btn').on('click',function(e){
                  e.preventDefault();
                  var Unit_Symbol=jQuery('#Unit_Symbol').val();
                  var Unit_Name=jQuery('#Unit_Name').val();
                  var No_of_Decimals=jQuery('#No_of_Decimals').val();
                  var data='';
                  if(jQuery.trim(Unit_Symbol)=='')
                    data+='Please Inter Unit Symbol\n';

                   if(jQuery.trim(Unit_Name)=='')
                    data+='Please Inter Unit Name\n';

                   if(jQuery.trim(No_of_Decimals)=='')
                    data+='Please Inter No. of Decimals\n';

                  if(jQuery.trim(data)==''){
                   jQuery('#form1').submit();
                  }else{
                    alert(data);
                  }

               });
           },
          
     }
     vil.init();
  });
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>