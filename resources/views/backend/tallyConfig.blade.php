@extends('backend-includes.app')

@section('content')

  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Tally Config</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
         <li><a href="#">Tally</a></li>
       </ul>
     </div>
   </div>



   <div class="row">

    <div class="col-md-12">
      <div class="">

        <div class="row">
          <form class="form-horizontal" action="{{route('storeTally')}}" method="post" enctype="multipart/form-data" 
 onsubmit="return confirm('Do you want to Continue?');">
               {{csrf_field()}}
            <div class="form-group">
              <label class="control-label col-sm-4" for="pwd">Use Tally Integration for Accounting </label>
              <div class="col-sm-4">
                <input type="checkbox" style="margin-top:15px;" id="tally_update_status" name="tally_update_status"   placeholder=""  />
              </div>
            </div>
            <div class="form-group tally_integration">
              <label class="control-label col-sm-4" for="pwd">Tally Serial Number </label>
              <div class="col-sm-4">
                <input type="text" class="form-control" id="tally_serial_number" name="tally_serial_number" value="{{$roCode->tally_serial_number}}"  placeholder=""  />
              </div>
            </div>

              
              <div class="form-group tally_integration">
              <label class="control-label col-sm-4" for="pwd">Tally Release </label>
              <div class="col-sm-4">
                <input type="text" class="form-control" id="tally_release" name="tally_release" value="{{$roCode->tally_release}}"  placeholder=""  readonly />
              </div>
            </div>
              <div class="form-group tally_integration">
              <label class="control-label col-sm-4" for="pwd">Edition</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" id="edition" name="edition" value="{{$roCode->edition}}"  placeholder=""  readonly/>
              </div>
            </div>
            <div class="form-group tally_integration">
              <label class="control-label col-sm-4" for="pwd">Tss Expiry </label>
              <div class="col-sm-4">
            

                   <input type="text" class="form-control" id="tssexpiry" name="tssexpiry" placeholder="" value="@if($roCode->tssexpiry&&$roCode->tssexpiry!='0000-00-00'){{date('d/m/Y',strtotime($roCode->tssexpiry))}}@else @endif" readonly />
              </div>
            </div>
            <div class="form-group tally_integration">
              <label class="control-label col-sm-4" for="pwd">Add Sync Start Date  </label>
              <div class="col-sm-4">
                <input type="text" class="form-control" id="sync_start_date" name="sync_start_date" placeholder="" value="@if($roCode->sync_start_date){{date('d/m/Y',strtotime($roCode->sync_start_date))}}@else @endif"/>
              </div>
            </div>
             <div class="form-group tally_integration">
              <label class="control-label col-sm-4" for="pwd">Add Sync End Date  </label>
              <div class="col-sm-4">
                <input type="text" class="form-control" id="sync_end_date" name="sync_end_date" placeholder="" value="@if($roCode->sync_end_date){{date('d/m/Y',strtotime($roCode->sync_end_date))}}@else @endif"/>
              </div>
            </div>
            <div class="form-group tally_integration">
              <label class="control-label col-sm-4" for="pwd">Last Sync Date </label>
              <div class="col-sm-4">
                <input type="text" class="form-control"  id="sync_last_date" name="sync_last_date" placeholder="" value="@if($roCode->sync_last_date){{date('d/m/Y',strtotime($roCode->sync_last_date))}}@else @endif" readonly />
              </div>
            </div>
            <div class="form-group tally_integration">
              <label class="control-label col-sm-4" for="pwd">Ro Code</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" readonly="readonly" name="RO_code" value="{{$roCode->RO_code}}"  placeholder="" id="RO_code" />
              </div>
            </div>
            <div class="form-group tally_integration">
              <label class="control-label col-sm-4" for="pwd">Secret Key</label>
              <div class="col-sm-4">
                <input type="text" class="form-control"  name="secret_key" value="{{$roCode->secret_key}}" readonly="readonly" id="secret_key" placeholder="hexcode"/>
              </div>
            @if(empty($roCode->secret_key))
              <div class="form-group  g_key tally_integration" >
                <div class="control-label col-sm-4 " style="    margin-top: -22px;">
                  <center>
                    <input type="button"  class="btn btn-primary " id="generate_key" value="Generate Key" >
                  </center>
                </div>
              </div>
              @else
               <!-- <div class="form-group c_key tally_integration"> -->
                <div class=" control-label col-sm-4" style="    margin-top: -8px;">
                <center>
                  <input type="button"  class="btn btn-primary " id="generate_key" value="change Key"  >
                </center>
               </div>
              <!-- </div> -->
            @endif 
            </div>
           
             <div class="form-group  ">
                <div class=" control-label col-sm-12">
                <center><input type="submit"  class="btn btn-primary "  value="Submit"  ></center>
               </div>
              </div>


          </form>
        </div>   

      </div>
    </div>
  </div>

</div>
@endsection
@section('script')
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 

<script type="text/javascript">

jQuery(function(){
    var enddated=   jQuery("#sync_start_date").val();
    var todate=new Date();
 
      var app={
               
              init:function(){
               
                
                app.fromDate();
                app.to_date();
                app.end_date();
             
              },
              fromDate:function(){
                
                jQuery("#sync_start_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         endDate: new Date(),
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     jQuery("#sync_end_date").val('');
                      todate=new Date();
                     enddated=jQuery(this).val();
                    /* enddatedAfterDay = jQuery(this).val();
                     enddatedAfterDay.setDate(enddated.getDate() + 30);
                      alert(enddatedAfterDay);
                  var date2 = $('#fromdate').datepicker('getDate', '+30d'); 


*/
                  

                     app.to_date();
                    });
              },
              to_date:function(){

              
              
                  
                  jQuery("#sync_end_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         startDate:enddated,
                         
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).datepicker('setStartDate',enddated);

                    /* alert(enddatedAfterDay);
                     alert(enddated);*/
              },
              end_date:function(){

              
              
                  
                 /* jQuery("#sync_last_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         startDate:enddated,
                         
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).datepicker('setStartDate',enddated);*/

                    /* alert(enddatedAfterDay);
                     alert(enddated);*/
              },

      };

      app.init();
});





















$(function () {

$("#tally_update_status").click(function() {
    if ($(this).is(":checked")) 
    {
        $("#tally_update_status").val('1');

    }else{
        $("#tally_update_status").val('0');
    }
});



            <?php 
              if ($roCode->tally_update_status!=1) {
                ?>
                $(".tally_integration").hide();
                $("#tally_update_status").prop('checked',false);
                $("#tally_update_status").val('0');
                <?php
              }else{ ?>
                $(".tally_integration").show();
                $("#tally_update_status").prop('checked',true);
                $("#tally_update_status").val('1');
            <?php  }
            ?>
            <?php 
              if (!empty($roCode->sync_last_date)) {
                ?>
                $("#tally_serial_number").prop('readonly', true);
                <?php
              }else{ ?>
                $("#tally_serial_number").prop('readonly', false);
            <?php  }
            ?>
        $("#tally_update_status").click(function () {
            if ($(this).is(":checked")) {
                $(".tally_integration").show();
            } else {
                $(".tally_integration").hide();
            }
        });
    });


$(function () {
  $("#datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        defaultDate: new Date,
         format: 'dd/mm/yyyy'
  }).datepicker('defaultDate', 'today');
});

         jQuery(document).on('click','#generate_key',function(event){
                   var msg = "Secret key generated Successfully!!";
                     // event.preventDefault();
                    var RO_code = $("#RO_code").val();
                    jQuery.ajax({
                        url:"{{route('changekey')}}",
                        type:"get",
                        data:{RO_code:RO_code},
                        dataType:'json',
                        success: function(response){
                            if(response.success){
                                // ViewHelpers.notify("success",msg);
                                var secret_key = response.tally;
                                console.log(secret_key);
                                $("#secret_key").val(secret_key);
                            }
                            else{
                                alert('Secret key not generate Successfully!!');
                            }
                        },
                        error: function(err){
                            alert('under working') ;
                        }
                    });
                    return false;
                });


                  $(document).ready(function() {
    $("#generate_key").on("click", function(){
        return confirm("Do you want to Generate Key? ");
    });
});
</script>
@endsection