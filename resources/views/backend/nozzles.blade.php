<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<title>Garruda</title>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<style>
.modal-dialog{
width: 80%;
}
input { 
    text-transform: uppercase;
}
::-webkit-input-placeholder { /* WebKit browsers */
    text-transform: none;
}
:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
    text-transform: none;
}
::-moz-placeholder { /* Mozilla Firefox 19+ */
    text-transform: none;
}
:-ms-input-placeholder { /* Internet Explorer 10+ */
    text-transform: none;
}

#viewdisplay{
    
    width: 500px;
    background: #fff;
    display: none;
    top: 200px !important;
    position: absolute;
    right: 50px;
    z-index: 99999999999;
    box-shadow: 2px 1px 20px #8a8282;
    border-radius: 3px;
}
</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
 <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Pedestals</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
        <li><a href="#">Pedestals</a></li>
        </ul>
      </div>
    </div>
   <div class="">
    
    </div>  
   @if ($errors->any())
<div class="alert alert-danger">
  <ul>
      @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
      @endforeach
  </ul>
</div>
@endif
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
        	 <center>@if(Session::has('success'))
           <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Pedestal</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form name="myForm" class="form-horizontal" action="{{'add_pedestalnew'}}" enctype="multipart/form-data" method="post" onsubmit="return validateForm()">
                        <input type="hidden" id="sel1" name="RO_code"value="{{Auth::user()->getRocode->RO_code}}">
                         {{ csrf_field() }}
                        <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Pump Legal Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" id="sel1" name="RO_code">
                             @foreach($getPedesta as $row) 
                              <option value="{{$row->RO_code}}">{{$row->pump_legal_name}}</option>
                              
                               @endforeach
                            </select>
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Pedestal Number <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                           <input type="text" class="form-control" id="Pedestal_Number"  style="text-transform:uppercase" placeholder="Pedestal Number" name="Pedestal_Number">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">No. of Nozzles <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" id="No_of_Nozzles"  class="form-control numeriDesimal"  style="text-transform:uppercase" placeholder="No of Nozzles" name="No_of_Nozzles">
                          </div>
                        </div>
                       
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" id="id-form-submit"class="btn btn-default submit-button" disabled>Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>S.No.</th>
               <!--  <th>Outlet Name</th> -->
                <th>Pedestal Number</th>
                 <th>No. of Nozzles</th>
                 <th>Status</th>
                <th>Actions</th>
              </tr>
            <tbody>
              <?php $i=0; ?>
      
                 @foreach($getPedestalnew as $row) 
      
              <?php $i++ ;?>
              <tr>
                <td>{{$i}}</td>
                <!-- <td>{{$row->pump_name}}</td> -->
                <td><a href="#" class="viewcharts" data-id="{{$row->id}}">{{$row->Pedestal_Number}}</a></td>
                <td>{{$row->No_of_Nozzles}}</td>
                <td> <?php if($row->is_active==1){?>

                   <a  title="Active" href="{{URL('pedestlnewDeactive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to deactivate?');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="{{URL('pedestlnewActive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to activate?');">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
          </td>
               
               
                
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                     @if($row->is_active==1)

                     <li><a href="pestalnew_update/{{$row->id}}" >Edit</a></li>
                     @endif
                      <!-- <li><a href="pesstalnew_delete/{{$row->id}}" onClick="return confirm('Are you sure?');">Delete</a></li> -->
                    </ul>
                  </div></td>
              </tr>
                 @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Javascripts--> 
<div id="viewdisplay">
  <div class="modal-header">
    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span id="chartdelete">×</span></div>
    <h4 class="modal-title" >Pedestal No.<span id="psss"></span> Details</h4> <span id="psss"></span>
  </div>
  <div id="dipchart" style="height: 300px !important;overflow-y: scroll;"></div>

</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="js/index.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
 jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });
 jQuery("#Pedestal_Number").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().trim());
                    });
</script>
<script type="text/javascript">
 jQuery(function(){
     var viewdipcharts="{{url('viewdipcharts')}}";
     var vil={
           init:function(){
             vil.selectRo();
            

             jQuery('.viewcharts').on('click',function(){
                 jQuery('#psss').html(jQuery(this).html());
                 var opt='';
                  var dip=jQuery(this).data('id');
                  jQuery.get(viewdipcharts+'/'+dip,{
                     '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                        
                  },function(data){
                      
                    jQuery('#dipchart').html(data);
                    jQuery('#viewdisplay').css('display','block');
                    
                  });
                  jQuery('#chartdelete').on('click',function(){
              jQuery('#viewdisplay').hide();
             });
             
             });
           },
           selectRo:function(){

            jQuery('#Pedestal_Number').on('blur',function(){
                   var colum1val=jQuery('#sel1').val();
                  udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique',{
                  table:'tbl_pedestal_master',
                  colum:'Pedestal_Number',
                  unik:udat,
                  colum2:'RO_code',
                  colum12val:colum1val,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert('Pedestal Number already exists Enter Other!');
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>


<script>
function validateForm() {
    var x = document.forms["myForm"]["Pedestal_Number"].value;
    var y = document.forms["myForm"]["No_of_Nozzles"].value;
    if (x == "") {
        alert("Please Enter Pedestal Number !!");
       
        return false;
    }
    if (y == "") {
        alert("Please Enter Nozzles No. !!");
         
        return false;
    }
}
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });

             $(document).ready(function() {
    $("#id-form-submit").on("click", function(){
        return confirm("Do you want to Continue? ");
    });
});
</script>

</body>
</html>