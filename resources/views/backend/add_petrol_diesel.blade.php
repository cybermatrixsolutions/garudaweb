<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">

<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
   @include('backend-includes.header')

        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Request Fuel  </h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Request Fuel </a></li>
        </ul>
      </div>
    </div>
     <div class="">
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
    
      
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
            <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
          @endif</center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Request Fuel  </h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal" action="{{'save_petrol_diesel_data'}}" method="post">
                          {{ csrf_field() }}
                         <div class="form-group">
                          <input type="hidden" class="form-control" name="vehicle_capcitys"  id="getcapcitys">
                          <label class="control-label col-sm-3" for="email">Outlet Name<span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="RO_code" id="RO_code">
                             
                                                    @foreach($datas as $ro_master)
                                                        <option name="company_id" value="{{$ro_master->RO_code}}"
                                                                >{{$ro_master->pump_legal_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                         
                       

                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Vehicle Reg No <span style="color:#f70b0b;">*</span> </label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="Vehicle_Reg_No" id="registration_number">
                              
                                                   
                            </select>

                             </div>
                        </div>

                       <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Fuel Capacity <span style="color:#f70b0b;">*</span> </label>
                          <div class="col-sm-9" >
                              <input type="text" class="form-control"   id="getcapcitys" disabled>
                         
                          </div>
                        </div> -->

                            
                            <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Fuel Type</label>
                          <div class="col-sm-9">
                             <select class="form-control" id="fuel_type" name="PetrolDiesel_Type">
                              <!--  @foreach($fuel_type as $fuel) 
                              <option value="{{$fuel->id}}">{{$fuel->Item_Name}}</option>
                              
                               @endforeach -->
                             
                            </select>
                            
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Request Type <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control" required="required" id="request_type" name="Request_Type" id="sel1">                
                             <option value="Full Tank">Full Tank</option> 
                             <option value="Ltr">Ltr</option>                  
                             <option value="Rs">Rs</option>                  
                            </select>
                          </div>
                        </div>
                         <div class="form-group" id="Request_Value">
                          <label class="control-label col-sm-3" for="Request Value">Request Value <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control"   name="Request_Value" id="Request_Value_inputaa" placeholder="Request Value " >
                          </div>
                          </div>
                          <div class="form-group" id="Request_Value">
                          <label class="control-label col-sm-3" for="Request Value">Driver Mobile Number <span style="color:#f70b0b;">*</span> </label>
                          <div class="col-sm-9">
                  <input type="text" maxlength="10" class="form-control number" value="@if(isset($Customer_Code->Driver_Mobile)){{$Customer_Code->Driver_Mobile}}@endif"  name="mobile" id="Request_Value_input"  placeholder="Driver Mobile Number" disabled>
                          </div>
                          </div>
                          
                      <div class="form-group">
                          <label class="control-label col-sm-3" >Alternate Mobile <span style="color:#f70b0b;">*</span> </label>
                             <div class="col-sm-9">
                            
                              <select class="form-control" required="required" name="current_driver_mobile" id="other_mobile">
                                 <option value="{{$Customer_Mobile}}">{{$Customer_Mobile}} - ({{$Customer_Name}})</option>

                              @foreach($customer_driver_list as $list)
                           

                             
                                
                                <option value="{{$list->Driver_Mobile}}">{{$list->Driver_Mobile}} - ({{$list->Driver_Name}})</option>
                             
                                      @endforeach             
                               </select>

                              </div>
                        </div>

                          <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Request Date</label>
                          <div class="col-sm-9">
                            <input class="form-control datepicker" required="required" name="request_date"  type="text" />
                          </div>
                        </div> -->
                        
                        
                         
                          
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                          
                            <button type="submit" class="btn btn-default" onclick="return getmobilevalid()">Submit</button>
                          </div>
                        </div>
                        <div class="form-group" style="visibility: hidden;">
                          <label class="control-label col-sm-3" for="email">Company Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control" required="required" readonly name="customer_code" id="customer_code">
                               <option value="{{Auth::user()->customer->Customer_Code}}">{{Auth::user()->customer->company_name}}</option>
                               
                                                   
                            </select>
                          </div>
                          </div>

                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div>
          <table class="table table-striped" id="myTable">

            <thead>
               @if(isset($add_petrol_diesel) && count($add_petrol_diesel)  != 0)
              <tr>
                <th>S.No.</th>
                <th>Request Date</th>
                <th>Request Value</th>
                <th>Vehicle Reg No</th>
                <th>RequestType</th>
                <th>Execution date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=0;?>
               @foreach($add_petrol_diesel as $list)
               <?php $i++;?>
              <tr>
                <td>{{$i}}</td>   
                 <td>{{Carbon\Carbon::parse($list->request_date)->format('d/m/Y h:i:s a')}}</td>
                 <td>{{$list->Request_Value}}</td>
                 <td>{{$list->Vehicle_Reg_No}}</td>
                 <td>{{$list->Request_Type}}</td>
                 <td>@if($list->Execution_date != null){{Carbon\Carbon::parse($list->Execution_date)->format('d/m/Y h:i:s a')}} @endif</td>
                 <td>
                  <div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      
                      <li><a href="add_petrol_diesel/view/{{$list->id}}">View</a></li>
                      @if($list->Execution_date == null)
                        <li><a href="add_petrol_diesel/delete/{{$list->id}}" onclick="return confirm('are you sure to delete?');">Delete</a></li>
                      @endif
                    </ul>
                  </div>
               </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="{{URL::asset('js/index.js')}}"></script> 
@if(isset($add_petrol_diesel) && count($add_petrol_diesel)  != 0)
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('#myTable').DataTable();
});
  
</script>
@endif
<script>
jQuery('.table-responsive').on('show.bs.dropdown', function () {
     jQuery('.table-responsive').css( "overflow", "inherit" );
});

jQuery('.table-responsive').on('hide.bs.dropdown', function () {
     jQuery('.table-responsive').css( "overflow", "auto" );
})

// function  getmobilevalid() {


//         var mobile = document.getElementById("Request_Value_input").value;
       
//         var pattern = /^[0]?[789]\d{9}$/;
//         if (pattern.test(mobile)) {
//            // alert("Your mobile number : " + mobile);
//             return true;
//         }
//         alert("It is not valid mobile number.input 10 digits number!");
//         return false;
//     }
</script>

<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
              vil.reqType();
              //vil.getFuel();
            

           },
           selectRo:function(){
            jQuery('#RO_code').on('change',function(){
                    console.log('hello');
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
                  
              });
            jQuery('#registration_number').on('change',function(){
                  vil.getcapcitys();
                  vil.Registration_Number();
                   vil.getFuel();
                  
              });
           
           },
           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#RO_code').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+=';';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html();

                   vil.getcrg();
               });
           },
            getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
                vil.getcapcitys();
                vil.getFuel();
                vil.Registration_Number();
               });
           },
           getFuel:function(){
              var ro=jQuery('#RO_code').val();
              var cus=jQuery('#customer_code').val();
              var registration_number=jQuery('#registration_number').val();
              
             jQuery.get('getfueltype/'+ro+'/'+cus+'/FUEL',{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                registration_number:registration_number,
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#fuel_type').html(opt);
               });
           },
           reqType:function(){
               jQuery('#Request_Value').hide();
              jQuery('#request_type').on('change',function(){
                    var t=jQuery(this).val();
                    if(t=='Full Tank'){
                      jQuery('#Request_Value').hide();
                      jQuery('#Request_Value_inputaa').attr('required',false);
                    }
                    else{
                      jQuery('#Request_Value').show();
                      jQuery('#Request_Value_inputaa').attr('required',true);

                    }

              });
           },
           getcapcitys:function(){
           
             jQuery.get('getcapcitys',{
                registration_number:jQuery('#registration_number').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                 jQuery('#getcapcitys').val(data);
               });
           },
           Registration_Number:function(){
             jQuery.get('getregierRegistration_Number',{
                registration_number:jQuery('#registration_number').val(),
                RO_code:jQuery('#RO_code').val(),
                customer_code:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                    console.log(data);
               // jQuery('.Request_Value_input').val(data);
               //alert(JSON.stringify(data));
                 jQuery('#Request_Value_input').val(data);
                 //jQuery("#other_mobile").val(jQuery('#Request_Value_input').val());
                 
                 /*jQuery("#other_mobile option").filter(function(){
                  return (jQuery(this).text() == jQuery('#Request_Value_input').val());
                 }).prop('selected',true);*/
                 jQuery('#other_mobile').val(data).change();

               });
           },

     }
     vil.init();
  });
 
    $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
</script>
</body>
</html>