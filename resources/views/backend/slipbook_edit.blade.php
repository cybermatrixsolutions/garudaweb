@extends('backend-includes.app')

@section('content')
<div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Slip Book Edit </h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{route('slipbooklist')}}">Slip Book List </a></li>
        </ul>
      </div>
    </div>
	
	 <div class="row" style="margin-top:50px !important;">
		<div class="col-md-9 ">
				<ul>
					@foreach($errors->all() as $error)
					  <li style="color: red">{{ $error}}</li>
					@endforeach
				  </ul>
				   <center style="text-align: center;">@if(Session::has('success'))
					<font style="color:red;">{!!session('success')!!}</font>
					@endif</center>

			<form class="form-horizontal" action="{{url('slipbook/update')}}/{{$dataInfo->id}}" enctype="multipart/form-data" method="post" id="parant-app-row"  onsubmit="return confirm('Do you want to Continue?');">
			
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				
	  
				<!--  
				<div class="form-group">
				  <label class="control-label col-sm-3">Customer Name</label>
				  <div class="col-sm-9">
					 
				  </div>
				</div>
				-->
				<div class='parent-tr' id="parant-row">
					<div class="form-group">
					  <label class="control-label col-sm-3">Date of issue <span style="color:#f70b0b;">*</span></label>
					  <div class="col-sm-9">
						 <input type="text" class="form-control datepicker" name="Slip_issue_date" value="{{$dataInfo->Slip_issue_date}}" required="required">
					  </div>
					</div>
					
					<div class="form-group">
					  <label class="control-label col-sm-3">No of books <span style="color:#f70b0b;">*</span></label>
					  <div class="col-sm-9">
						 <input type="text" class="form-control" name="Slip_no_of_book" value="{{$dataInfo->Slip_no_of_book}}" required="required">
					  </div>
					</div>
					
					<div class="form-group">
					  <label class="control-label col-sm-3">Slip Start <span style="color:#f70b0b;">*</span></label>
					  <div class="col-sm-9">
						 <input type="text" class="form-control input-slip-start" name="Slip_start" value="{{$dataInfo->Slip_start}}" required="required">
					  </div>
					</div>
					
					<div class="form-group">
					  <label class="control-label col-sm-3">Slip End <span style="color:#f70b0b;">*</span></label>
					  <div class="col-sm-9">
						 <input type="text" class="form-control input-slip-end" name="Slip_end" value="{{$dataInfo->Slip_end}}" required="required">
					  </div>
					</div>
					
					<div class="form-group">
					  <label class="control-label col-sm-3">No Of Slip  <span style="color:#f70b0b;">*</span></label>
					  <div class="col-sm-9">
						 <input type="text" class="form-control input-slip-number" name="no_of_slip" value="{{$dataInfo->no_of_slip}}" required="required">
					  </div>
					</div>
					<div class="form-group">
					  <label class="control-label col-sm-3">&nbsp;</label>
					  <div class="col-sm-9">
						 <button type="submit" class="btn btn-primary">Save</button>
					  </div>
					</div>
				</div>	

			</form>
		</div>
    </div>
</div>
@endsection

@section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
 $(function () {
  $("#datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        defaultDate:'dd/mm/yyy',
        format: 'dd/mm/yyyy',
  }).datepicker('update');
  
   var currentP=jQuery('#parant-row');
   
   jQuery(document).on('click','#parant-app-row input',function(event){
	  currentP = jQuery(this).parents('.parent-tr');
	  
	});
				
  jQuery(document).on('keyup','.input-slip-start,.input-slip-end',function(event){
	var currentSlipStart=0,currentSlipEnd=0,currentSlipNumber=0;
	currentSlipStart = parseInt(currentP.find('.input-slip-start').val());
	currentSlipEnd = parseInt(currentP.find('.input-slip-end').val());
	if(currentSlipEnd > currentSlipStart){
		currentSlipNumber = parseInt(((currentSlipEnd - currentSlipStart)+1));
	}
	currentP.find('.input-slip-number').val(currentSlipNumber);
	console.log("currentSlipStart:"+currentSlipStart+"===currentSlipEnd:"+currentSlipEnd+"===currentSlipNumber:"+currentSlipNumber);
});
});
</script>
@endsection