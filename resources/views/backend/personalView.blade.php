<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Personnel & Staff View </h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/persoonel')}}"> Personnel & Staff  </a></li>
        </ul>
      </div>
    </div>
	
	 <div class="">
      
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
      
    </div>
	
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row"  style="margin-top:-15px !important;">
          
         <form class="form-horizontal" action="{{url('personal_update')}}/{{$personal_mangement_data->id}}" enctype="multipart/form-data" method="post" onsubmit="return confirm('Do you want to Continue?');">
            
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
          
                       <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Pump Legal Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="personal_ro_code" id="sel1" disabled>
                              
                                                    @foreach($datas as $ro_master)
                                                        <option name="company_id" value="{{$ro_master->company_code}}"
                                                                @if($personal_mangement_data->RO_Code == $ro_master->RO_code) selected @endif>{{$ro_master->pump_legal_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div> --> 
                        <div class="form-group">
                           <label class="control-label col-sm-3" for="pwd">Personal Name</label>
                          <div class="col-sm-9">
                            <input disabled  type="text" class="form-control" name="personl_name" id="" value="{{$personal_mangement_data->Personnel_Name}}" placeholder="Personal Name">
                          </div>
                        </div>
                         <div class="form-group">
                           <label class="control-label col-sm-3" for="pwd">Gender</label>
                          <div class="col-sm-9">
                              <input disabled  type="text" class="form-control" name="personl_name" id="" value="{{$personal_mangement_data->gender}}" placeholder="Personal Name">
                          </div>
                        </div>
                      
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Role</label>
                          <div class="col-sm-9">
                             <select disabled  class="form-control" name="designation" id="sel1" disabled>
                             
                                                    @foreach($designation as $desig)
                                                        <option name="company_id" value="{{$desig->id}}"
                                                                @if($personal_mangement_data->Designation == $desig->id) selected @endif>{{$desig->Designation_Name}}</option>
                                                    @endforeach
                            </select>
                            
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Reporting To</label>
                          <div class="col-sm-9">
                            <select disabled  class="form-control" name="reporting_to" id="reporting_to">
                                                   <option name="company_id" value="0"
                                                                >no</option>
                                                    @foreach($personal_list as $personnel)
                                                        <option name="company_id" value="{{$personnel->id}}"
                                                                 @if($personal_mangement_data->Reporting_To == $personnel->id) selected @endif>{{$personnel->Personnel_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Date of Birth</label>
                          <div class="col-sm-9">
                            <input disabled  type="text" class="form-control datepicker" name="d_o_b" value="{{date('d/m/Y',strtotime($personal_mangement_data->Date_of_Birth))}}" id="" placeholder="Date of Birth">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Date of Appointment</label>
                          <div class="col-sm-9">
                            <input disabled  type="text" class="form-control datepicker" name="date_of_appointment" value="{{date('d/m/Y',strtotime($personal_mangement_data->Date_of_Appointment))}}" id="" placeholder="Date of Appointment">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Mobile(+91)</label>
                          <div class="col-sm-9">
                            <input disabled  type="text" class="form-control" name="mobile" id="" value="{{$personal_mangement_data->Mobile}}" placeholder="Mobile">
                          </div>

                        </div> 
                        <div class="form-group">
                          <label class="control-label col-sm-3" >Aadhaar No</label>
                          <div class="col-sm-9">
                            <input disabled  type="text" class="form-control" name="mobile" id="" value="{{$personal_mangement_data->Aadhaar_no}}" placeholder="Mobile">
                          </div>

                        </div> 
                        <div class="form-group">
                          <label class="control-label col-sm-3" >Aadhaar Photo</label>
                          <div class="col-sm-9">
                            <img src="{{URL::asset('')}}/{{$personal_mangement_data->Aadhaar_img}}" style="width: 250px; height: 150px;">
                          </div>

                        </div> 
                        

                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Email/Login ID</label>
                          <div class="col-sm-9">
                            <input disabled  type="email" class="form-control" name="Email" value="{{$personal_mangement_data->Email}}" placeholder="Email">
                          </div>
                        </div>
                      
           
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>
	
	
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script type="text/javascript">
  $(function () {
  $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  })
});
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>