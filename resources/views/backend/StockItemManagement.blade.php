<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>
</head>

<body class="sidebar-mini fixed">

<div class="wrapper"> 
  <!-- Navbar-->
  
   @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
 

  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Common Items </h1>
      </div>
      <div>
        <ul class="breadcrumb">
           <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#"> Common Items </a></li>
        </ul>
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
         <div class="">
      
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      
    </div>
          <div class="col-md-12 col-sm-12 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add" ><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="pull-right"> 
          <div>
                     <form class="form-inline" action="{{'stocksave'}}" method="post" name="f" onsubmit="return check(this)" enctype="multipart/form-data">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <div class="form-group">
                     <label for="email"File(csv)</label>
                     <input type="file" id="inputfile" required="required"   placeholder="File(csv)" name="file" style="display: inline; width: auto;">
                     </div>
                     <div class="form-group">
                     <input  type="submit" value="Submit" style="margin-left: -83px;background-color: #00786a;color: #fff;">
                     </div>
                     </form>
                     <a href="emportstocka" style="position: absolute;
    top: 12px;
    right: 27%; padding-top: 9px;" class="btn btn-primary">Export as CSV</a>
          </div>
        </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" style="width: 1000px;" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Add  Common Item </h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal"  action="{{url('save_stock_item_management')}}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Principal Company Name <span style="color:#f70b0b;">*</span></label>
                         <div class="col-sm-8">
                            <select class="form-control" required="required" name="Company_Code" id="Company_Code">
                                                    
                                                    @foreach($principle_company as $principle)
                                                        <option value="{{$principle->company_code}}"
                                                                >{{$principle->company_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Stock Item Code <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <input type="text"  class="form-control" required="required" name="Stock_Item_Code" id="Stock_Item_Code" placeholder="Stock Item Code">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Stock Item Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <input type="text"  class="form-control" required="required" name="Stock_Item_Name" id="Stock_Item_Name" placeholder="Stock Item Name">
                          </div>
                        </div>
                    
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Stock Group <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">

                              <div id="othergroup" style="display:none">
                                 <option value="">Select Group</option>
                                    @foreach($stock_group as $stock)
                                        <option  value="{{$stock->id}}"
                                                >{{$stock->Group_Name}}</option>
                                    @endforeach
                              </div>
                              <div id="fuelgroup" style="display:none">
                                  <option value="">Select Group</option>
                                  @foreach($tock_groupfuel as $tock_groupf)
                                        <option  value="{{$tock_groupf->id}}"
                                                >{{$tock_groupf->Group_Name}}</option>
                                    @endforeach
                                    
                              </div>

                            <select class="form-control" required="required" name="Sub_Stock_Group" id="stockgroup">
                                     <option value="">Select Group</option>
                                   
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Sub Group <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <select value="" class="form-control" required="required" name="Stock_Group" id="subgroup">
             
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Unit of Measure <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                                <select class="form-control" required="required" name="Unit_of_Measure" id="Unit_of_Measure">
                                                    <option value="" >Select Unit of Measure</option>
                                                    @foreach($unit_measure as $unit)
                                                        <option  value="{{$unit->id}}"
                                                                >{{$unit->Unit_Symbol}}-{{$unit->Unit_Name}}</option>
                                                    @endforeach
                              </select>
                          </div>
                        </div>
                       
                       <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Tail Unit</label>
                          <div class="col-sm-8">
                             <input type="text"  class="form-control numeriDesimal" readonly  id="" name="Tail_Unit" placeholder="Tail Unit">
                          </div>
                        </div>


                        </div>
                        <div class="col-sm-6">

                        
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Conversion</label>
                          <div class="col-sm-8">
                             <input type="text"   class="form-control numeriDesimal" readonly  name="Conversion" id="" placeholder="Conversion">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Alternate Unit</label>
                          <div class="col-sm-8">
                             <input type="text"  class="form-control numeriDesimal" readonly name="Alternate_Unit" id="" placeholder="Alternate Unit">
                          </div>
                        </div>

                        <!--  <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">LOB <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">

                            <div id="otherItem" style="display:none">
                               <option selected  value="">Select LOB</option>
                                  @foreach($lob as $lobc)
                                      <option  value="{{$lobc->lob_name}}"
                                              >{{$lobc->lob_name}}</option>
                                  @endforeach
                            </div>
                            <div id="fuelItem" style="display:none">
                               <option selected value="">Select LOB</option>
                               <option  value="FUEL">FUEL</option>
                               <option  value="LUBES">LUBES</option>
                                
                            </div>

                             <select class="form-control" required="required" name="LOB" id="LOB">
                                 <option  value="">Select LOB</option>
                                  @foreach($lob as $lobc)
                                      <option  value="{{$lobc->lob_name}}"
                                              >{{$lobc->lob_name}}</option>
                                  @endforeach
                            </select>
                          </div>
                        </div> -->

                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Brand</label>
                          <div class="col-sm-8">
                             <input type="text"  class="form-control" name="brand" id="" placeholder="brand">
                          </div>
                        </div>
                         <!--  <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Tax</label>
                          <div class="col-sm-8">
                            <select class="form-control" required="required" name="tax">
                             
                            @foreach($taxmaster as $tax)
                                <option  value="{{$tax->Tax_Code}}">{{$tax->Tax_Code}}</option>
                            @endforeach
                            </select>
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">HSN/SAC</label>
                          <div class="col-sm-8">
                             <input type="text"  class="form-control" maxlength="10"  name="hsncode" id="myField" placeholder="hsncode">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">GST/VAT</label>
                          <div class="col-sm-8">
                            <select class="form-control" required="required" name="gst_vat">
                                   <option value="GST">GST</option>
                                   <option value="VAT">VAT</option>              
                            </select>
                          </div>
                        </div>

                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Active From Date</label>
                          <div class="col-sm-8">
                             <input type="text"  class="form-control datepickers" value="01/04/2017" name="Active_From_Date" id="" placeholder="Active From Date">
                          </div>
                        </div>

                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-10 col-sm-2">
                            <input  type="submit" id="success-btn" class="btn btn-default site-btn" name="submit" value="submit">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">

          <table class="table table-striped"  id="myTable">

            <thead>
               @if(isset($stock_list) && count($stock_list)  != 0)
              <tr>
                <th>S.No.</th>
                <th>Principal Company Name</th>
                <th>Stock Item Code</th>
                <th>Stock Item Name</th>
                <th>LOB</th>
                <th>Brand</th>
                
                <th>Stock Group</th>
                <th>Unit of Symbol</th>
                <!-- <th>Status</th> -->
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
             <?php $i=0; ?>
              @foreach($stock_list as $list)
                 <?php $i++ ;?>    
              <tr>
                <td>{{$i}}</td>
                <td scope="row">{{$list->company_name}}</td>
                <td>{{$list->Stock_Item_Code}}</td>
                <td>{{$list->Stock_Item_Name}}</td>
                 <td>{{$list->LOB}}</td>
                 <td>{{$list->brand}}</td>
                
                <td>{{$list->Group_Name}}</td>
                <td>{{$list->Unit_Symbol}}</td>
               
       
         
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      @if($list->is_active===1)
                      <li><a href="stock_edit_page/{{$list->id}}">Edit</a></li>
                      @endif
                      <li><a href="stock_view_page/{{$list->id}}">View</a></li>
                    
                    </ul>
                  </div></td>
              </tr>
               @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script>
@if($stock_list->count()>0)
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
@endif
<script>
function check(form){ 
 if (!/\.csv$/.test(form.file.value)){
  alert('Please select file in csv format')
  return false
  }
 else return true
}
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
             vil.checkPrincipal();
             
             jQuery("#myField").keyup(function() {
                jQuery("#myField").val(this.value.match(/[0-9]*/));
              });
            
             jQuery('#Company_Code').on('change',function(){
                 vil.checkPrincipal();
             });

              jQuery('#myField').on('keyup',function(){
                          var hsn= jQuery(this).val();
                          if(jQuery.trim(hsn).length>8){
                            jQuery('#success-btn').attr('disabled',true);
                            jQuery(this).css('border-color','red');
                            alert('max length is 8 digit');
                          }else{
                            jQuery('#success-btn').attr('disabled',false);
                             jQuery(this).css('border-color','');
                          }

                    });
           },
           selectRo:function(){
            jQuery('#stockgroup').on('change',function(){
                  vil.getstock();
              });
            jQuery('#Stock_Item_Code').on('blur',function(){
                
                  udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique',{
                  table:'tbl_stock_item_master',
                  colum:'Stock_Item_Code',
                  unik:udat,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                    console.log(data);
                   if(data=='true'){
                      alert('Stock Code already exists Enter Other!');
                    jQuery('.site-btn').attr('disabled',true);

                   }else{
                    jQuery('.site-btn').attr('disabled',false);
                   }
                       
                   });
              });
           },
           getstock:function(){
             
               jQuery.get('getstrocksubgroup',{
                
                stockgroup:jQuery('#stockgroup').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='<option value=""> Select Sub Group</option>';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#subgroup').html(opt);

                   vil.getcrg();
               });
           },
           checkPrincipal:function(){
                var sel='';
                var stact='';

                if(jQuery.trim(jQuery('#Company_Code').val())=='GEN'){
                   sel=jQuery('#otherItem').html();
                   stact=jQuery('#othergroup').html();
                 
                }else{
                    stact=jQuery('#fuelgroup').html();
                    sel=jQuery('#fuelItem').html();
                }

              jQuery('#stockgroup').html(stact);
              jQuery('#LOB').html(sel);
           },

          
     }
     vil.init();
  });



</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

<script  src="{{URL::asset('js/index.js')}}"></script> 
<script>
  $(function () {
  $(".datepickers").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});

  $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script type="text/javascript">
  jQuery(".numeriDesimal").keyup(function() {
 var $this = jQuery(this);
 $this.val($this.val().replace(/[^\d.]/g, ''));
        });
</script>
</body>
</html>