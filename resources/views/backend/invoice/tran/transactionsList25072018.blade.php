<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
<style type="text/css">
  input.form-control.abhi {
    border: 0px;
    background-color: #E5E5E5;
}

</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  <form class="form-horizontal form-inline" name="myForm" action="{{url('invoicepdf')}}" enctype="multipart/form-data" method="post">
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Customer Transactions List </h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Customer Transactions List</a></li>
        </ul>
      </div>
    </div>
  
  
       <div class="">  <center>@if(Session::has('success'))
               <font style="color:red">{!!session('success')!!}</font>
            @endif</center>
       </div>

       
 <div class="row">
    <div class="col-md-11 col-md-offset-1" style="margin-top:40px;;">

<div  style="margin-left: -84px;" class="pull-left"><!-- Discount (%) : --> 
  <input type="hidden" name="Discount" @if($Credit_limit!=null)  value="@if($fule_type==1){{$Credit_limit->Discount}}@else{{$Credit_limit->Discountlube}}@endif" @else value="0" @endif> 
  <input type="hidden" name="fule_type" value="{{$fule_type}}"> 
 
<!--  Order By : <select name="order_by">
    <option value="1">Slip Order</option>
     <option value="2">Vehicle Order</option>
  </select> -->
</div>
  {{ csrf_field() }}
  <div class="pull-right" onclick="clicked();" id="abhi" ><input  name="pdf" type="submit" id="download_pdf" class="btn btn-primary " value="Generate Bill"></div>
  
  <div class="pull-right" id="abhi"style="margin-right: 5px;margin-left: 5 px;" ><input name="pdf" type="submit" id="preview_pdf" class="btn btn-primary " value="Preview"></div>
    </div>
    </div>
  <div class="row">

      <div class="col-md-12">

        <div class="table-responsive">
            <input type="hidden" name="current_bill_date" value="{{$current_bill_date}}">
          <input type="checkbox" id="checkAll">Check All
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>Select Item</th>
                <th>Date</th>   
                <th>Customer Name</th>
                <th>Slip No. / Invoice No.</th>
                <th>Vehicle No.</th>
                <th>Item Name</th>
                <th style="text-align:right">Qty Sold</th>
                <th style="text-align:right">Price</th>
                   
            </thead>
            <tbody>
             
             @foreach($transactionModels as $row) 
                <tr class="parent-tr"> 
                  <input type="hidden" name="from_dates" value="{{$from_date}}">
                  <input type="hidden" name="to_dates" value="{{$to_date}}">
                  <input type="hidden" name="RO_code" value="{{$row->customer_code}}">
                  <input type="hidden" name="fule_type" value="{{$row->petrol_or_lube}}">
                 
               <td><input type="checkbox" value="{{$row->id}}" name="cus_id[]" class="ckeckbax"></td>
               <td >{{Carbon\Carbon::parse($row->trans_date)->format('d/m/Y')}}</td>
               <td>{{$row->getCustomerMaster->company_name}}</td>
               <td>{{$row->invoice_number}}</td>
               <td>{{$row->Vehicle_Reg_No}}</td>
               <td>{{$row->Item_Name}}</td>
               <td style="text-align:right">{{number_format($row->petroldiesel_qty,3)}}</td>
               <td style="text-align:right">{{number_format($row->item_price,2)}} </td>
              
               </tr>
              

            @endforeach
          </div>
         </div>
      </div>
    </div>
  </div>
</div>
 </form>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script>
<script type="text/javascript">
  $(function () {
  $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  }).datepicker('update');
});

</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
             vil.getcom();

             jQuery('#download_pdf').on('click',function(){

                  jQuery('.parent-tr').each(function(){
                    
                     if(jQuery(this).find('.ckeckbax').is(':checked')){
                      console.log('hello');
                        jQuery(this).hide();
                        
                     }
                  });
             });
          
           },
           selectRo:function(){
            jQuery('#customer_code').on('change',function(){
                  vil.getcom();
              });
           },
           getcom:function(){
                var customer=jQuery('#customer_code').val();
               jQuery.get("{{url('getcustomerinvoice')}}/"+customer,{
                
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                
                   jQuery('#invoices_data').html(data);
                   jQuery('#myTable').DataTable();
               });
           },
          

     }
     vil.init();
  });
  $(document).ready(function() {
  $('#myTable').DataTable({
              
              buttons: [
                  'csv'
                  ],
                 
                  "aaSorting": [],
              dom: 'Blfrtip',
              "lengthMenu": [[50,100, 200, 300], [50,100, 200,300]]
            });
});
</script>
<script language="JavaScript">
$("#checkAll").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 });
</script>

<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script type="text/javascript">
    function clicked() {
       if (confirm('Do you want to genrate bill?')) {
           yourformelement.submit();
       } else {
           return false;
       }
    }
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


<!-- var array = table.find('tr').each(function (i,ids) {
               if($(this).find('input').is(':checked')) {
                   var $tds = $(this).find('td'),
                               ids = $tds.eq(8).text();
                               myArray.push(ids);
               }
           }); -->

</body>
</html>