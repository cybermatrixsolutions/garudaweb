<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
<style type="text/css">
  input.form-control.abhi {
    border: 0px;
    background-color: #E5E5E5;
}
</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  

<div class="content-wrapper">
  <div class="page-title">
    <div>
      <h1><i class="fa fa-dashboard"></i>&nbsp; Create Fuel & Items Bills</h1>
    </div>
    <div>
      <ul class="breadcrumb">
       <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
        <li><a href="{{url('/transaction_list')}}"> Create Fuel & Items Bills</a></li>
      </ul>
    </div>
  </div>

 <div class="row">
    <div class="col-md-12" style="margin-top: 40px;;">
       <div class="">  
    <center>
      @if(Session::has('success'))
         <font style="color:red">{!!session('success')!!}</font>
      @endif
    </center>
  </div>
    <form class="form-horizontal form-inline" name="myForm" action="{{'invoiceindex'}}" enctype="multipart/form-data" method="post">
       
            
             {{ csrf_field() }}
        <div class="row">
         <div class="col-md-12">
              <div class="form-group">
                <label class="control-label" for="pwd">Select Type <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                 
                 <select class="form-control" id="fule_type" name="fule_type" @if($GSTlastBilledDate!=null) data-gst="{{date('d/m/Y',strtotime($GSTlastBilledDate))}}" @endif  @if($VATlastBilledDate!=null) data-vat="{{date('d/m/Y',strtotime($VATlastBilledDate))}}" @endif >
                          <option value="2">GST</option>
                          <option value="1">VAT</option>
                  </select>

               </div>

              <div class="form-group">
                <label  class="control-label" for="pwd">Last billed date</label>&nbsp;&nbsp;
                  <input required type="text" readonly class="form-control" id="last_billed_date" name="last_billed_date" @if($GSTlastBilledDate!=null) value="{{date('d/m/Y',strtotime($GSTlastBilledDate))}}" @endif />&nbsp;&nbsp;
              </div>

               <div class="form-group">
                <label   class="control-label" for="pwd">Current bill date</label>&nbsp;&nbsp;
                  <input required type="text" class="form-control" id="current_bill_date" name="current_bill_date" @if($GSTlastBilledDate!=null) value="{{date('d/m/Y',strtotime($GSTlastBilledDate))}}" @endif />&nbsp;&nbsp;
              </div>

              <div class="form-group">
                 <label class="control-label"  for="email">Customer Name <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                     <select class="form-control" required name="RO_code" id="id-select-customer">
                        
                        @if(isset($tbl_customer_transaction))
                          @foreach($tbl_customer_transaction as $getrocodecust)
                           <option  value="{{$getrocodecust->Customer_Code}}">{{$getrocodecust->company_name}}</option>
                          @endforeach
                        @endif
                        </select>
              </div>

              &nbsp;&nbsp;
             <!--  <div class="form-group">
                <label class="control-label"  for="email">From <span style="color:#f70b0b;">*</span></label>&nbsp;&nbsp;
                 <input type="text" required class="form-control" id="from_date" name="fromdate" placeholder="from date"/>&nbsp;&nbsp;
              </div> -->
              <div class="form-group">
                    <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                      <input required type="text" class="form-control" id="to_date" name="to_dae" placeholder="To Date"/>&nbsp;&nbsp;
              </div>
              <div class="form-group">
                  <input type="submit" class="btn btn-default" id="checkdate" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
              </div>
            </div>
        </div>
 </form> 
    </div>
    </div>
  
  </div>
</div>

<div id="get_date" data-startDate="@if($GSTlastBilledDate!=null){{date('d/m/Y',strtotime($GSTlastBilledDate))}}@else''@endif" data-fromdate="@if($GSTlastBilledDate!=null){{date('d/m/Y',strtotime($GSTlastBilledDate))}}@endif"  data-todate="@if($GSTlastBilledDate!=null){{date('d/m/Y',strtotime($GSTlastBilledDate))}}@endif" ></div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
 


jQuery(function(){
  //get customer

 


        jQuery('#to_date').on('change',function(){
         var date= $('#to_date').val();
         console.log(date);

           

            var fule_typ=$('#fule_type').val();
          
              jQuery.get("{{route('getCustByDate')}}",{
                date:date,fule_type:fule_typ,
              '_token': jQuery('meta[name="csrf-token"]').attr('content'),
              },function(data){
                console.log(data);

                if(data){
                var str='';
                jQuery('#id-select-customer').empty();
                jQuery.each(data,function(i,v){
                  str+='  <option  value='+v.Customer_Code+'>'+v.company_name+'</option>';
                });
              }

                jQuery('#id-select-customer').html(str);
                   
                
        });
  
});










     jQuery('#checkdate').on('click',function(){
        
         if(DateCheck()==false)
          return false;

     });

     function DateCheck()
      {
        var StartDate= document.getElementById('from_date').value;
        var EndDate= document.getElementById('to_date').value;
        var eDate = new Date(EndDate);
          console.log(eDate);
        var sDate = new Date(StartDate);
        if(StartDate!= '' && StartDate!= '' && sDate> eDate)
          {
          alert("Please ensure that the to Date is greater than or equal to the from Date.");
          return false;
          }
      }






    
var startDate='';
var fromdate='';
var todate='';
var enddated='';

if(jQuery('#get_date').data('startDate')!='')
   startDate=jQuery('#get_date').data('startDate');

if(jQuery('#get_date').data('fromdate')!=''){
  fromdate=jQuery('#get_date').data('fromdate');
  enddated=fromdate;
}
 

if(jQuery('#get_date').data('todate')!='')
   todate=jQuery('#get_date').data('todate');

      var app={
               
              init:function(){
                app.datePicker();
                app.to_date();
                app.fromDate();
                jQuery('#fule_type').on('change',function(){

                        var type=jQuery(this).val();

                        if(type==2){
                           startDate=jQuery(this).data('gst');
                           todate=jQuery(this).data('gst');
                         }else{
                          startDate=jQuery(this).data('vat');
                          todate=jQuery(this).data('vat');
                          
                         }
                          todate=startDate;
                          fromdate=startDate;
                          app.to_date();
                          app.fromDate();
                          app.datePicker();

                         jQuery("#current_bill_date").val(startDate);
                         jQuery("#last_billed_date").val(startDate);


                        app.datePicker();
                        app.to_date();
                       // app.fromDate();
                  });
              },
              datePicker:function(){
                  
                  jQuery("#current_bill_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                        startDate:fromdate,
                        endDate: new Date(),
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     todate=jQuery(this).val();
                     fromdate=jQuery(this).val();
                     app.to_date();
                     //app.fromDate();

                    }).datepicker('setStartDate',fromdate);
              },
              fromDate:function(){
                 jQuery("#from_date").val('');
                jQuery("#from_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         endDate: new Date(),
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     enddated=jQuery(this).val();
                     app.to_date();
                    }).datepicker('setEndDate',fromdate);
              },
              to_date:function(){
                  
                  jQuery("#to_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                       
                         endDate: todate,
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).datepicker('setEndDate',todate);

              },

      };

      app.init();
});

</script>

<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
</body>
</html>