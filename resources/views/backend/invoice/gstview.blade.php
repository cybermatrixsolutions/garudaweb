<!--  @extends('backend-includes.app')
@section('content') -->
  <style>
  body{
	
	font-size:10px;
	color: #000;
	line-height:12px;
  }
  h1,h2,h3,h4,h5,h6{
  	margin:0;
  	padding:0;
  }
  .table {
    border-collapse: collapse;
     }
     p{

		margin: 0px;
	}
  .table td {
    padding: 1px;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
}
.table th {
    padding: 0px;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
}

table.table-bordered td, .table-bordered th {
    border: 1px solid #dee2e6;
}
table.table-bordered th{
    text-align: center!important;
}

.table-bordered .removeborder{
    border: transparent!important;
}
.table-bordered .leftborder{
	border-left: 1px solid #dee2e6!important;
}
.table-bordered .borderbottom{
	border-bottom:1px solid transparent!important;
}
	p{
		margin-bottom:0px !important;
	}
.removebordertr td{
	 border: transparent!important;
}

.table-bordered .leftborder1{
   border-left: 1px solid #dee2e6!important;
}
@media  print {
 
#pdfPreview{
        display :  none;
    }
    
}
@media  print {

    #id-back{
        display :  none;
    }
  
}
@media  print {

     #id-pdfPreview{
        display :  none;
    }
  
}
@media  print {

    #myPrntbtn{
        display :  none;
    }
  
}
@media  print {

    #myPrntbtn{
        display :  none;
    }
  
}
  </style>
  <?php $uniuntiretary=['AN','CH','DN','DD','DL','LD','PY'];
      $rostate='';
      $cusstate='';
      
    ?>
    <?php $cusstate=$tbl_invoice->getstate->statecode;?>
    <?php $rostate=Auth::user()->getRocode->getstate->statecode; ?>
  </head>

  <body>

  	<div class="content-wrapper" id="id-content-wrapper">
<?php $comiGst=[]; $comiGstA=[]; $compar=[]; $gstPE=0;
$texNameviseCommition=[];
$texNamevise=[];
$seviceName='';
$sevicehsn='';
$state_type='intrastate';
?>
@if($rostate!=$cusstate)
    <?php  $state_type='interstate'; ?>
 @endif

@if($sevice!=null)
       <?php $seviceName=$sevice->name;
             $sevicehsn=$sevice->hsn;
        ?>
		@if($rostate!=$cusstate)
          
		     @foreach($sevice->gettax->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both']) as $gettaxs)
			     	<?php $comiGst[$gettaxs->Description]=$gettaxs->position;
			     	       $comiGstA[$gettaxs->Description]=$gettaxs->Tax_percentage*$handling_amount/100;
			     	       $gstPE=$gettaxs->tax_name+$gstPE;
			     	       $compar[$gettaxs->Description]=$gettaxs->Tax_percentage;
			     	       $texNameviseCommition[$gettaxs->GST_Type]['amount']=$gettaxs->Tax_percentage*$handling_amount/100;
			     	        $texNameviseCommition[$gettaxs->GST_Type]['tax_percentage']=$gettaxs->Tax_percentage;
			     	 ?>
		      	 @endforeach

		@else
		      @foreach($sevice->gettax->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both']) as $gettaxs)
			     	<?php $comiGst[$gettaxs->Description]=$gettaxs->position;
			     	      $comiGstA[$gettaxs->Description]=$gettaxs->Tax_percentage*$handling_amount/100;
			     	      $compar[$gettaxs->Description]=$gettaxs->Tax_percentage;
			     	       $gstPE=$gettaxs->tax_name+$gstPE;
			     	       $texNameviseCommition[$gettaxs->GST_Type]['amount']=$gettaxs->Tax_percentage*$handling_amount/100;
			     	        $texNameviseCommition[$gettaxs->GST_Type]['tax_percentage']=$gettaxs->Tax_percentage;
			     	?>
		      	  @endforeach

		@endif

@endif

<?php asort($comiGst)?>

	
					<!-- <center style="font-size:10px; font-weight:700">TAX / GST Invoice view </center> -->
					<table class="table table-bordered" width="100%">
					
						<tr>
							<td  class="" style="padding:0px;width:80px">
								@if(Auth::user()->getRocode!=null)
								<img style="width:90px; height:90px;" src="{{Auth::user()->getRocode->principal->image}}" />
								@endif
							</td>
							<td colspan="8" class=""  >
								<table class="removeborder table-bordered header" width="100%"style="text-align: center;padding: 0px !important">
									<tr>
										<td class="removeborder" style="font-size:13px; font-weight:600">
											  {{Auth::user()->getRocode->pump_legal_name}}
										    
										</td>
										</tr>
										<tr>
										<td class="removeborder" ><p>{{Auth::user()->getRocode->pump_address}}</p></td>
										</tr>
										<tr>
										<td class="removeborder" ></td>
										</tr>
										<tr>
											<td class="removeborder" >
											  <p>
											  	{{Auth::user()->getRocode->getcity->name}} - {{Auth::user()->getRocode->pin_code}}
											  </p>
											</td>
										</tr>
										<tr>
										<td class="removeborder" ><p>GSTIN/UIN: {{Auth::user()->getRocode->GST_TIN}}, TIN No : {{Auth::user()->getRocode->VAT_TIN}}
										,PAN NO : {{Auth::user()->getRocode->PAN_No}}</p></td>
										</tr>
										<tr>
										<td class="removeborder" >
											<?php $rostate=Auth::user()->getRocode->getstate->statecode; ?>
											<p>State Name :  {{Auth::user()->getRocode->getstate->name}}, Code : {{Auth::user()->getRocode->getstate->gstcode}}</p></td>
										</tr>
								</table>
							</td>
							<td   class="text-center logos" style="padding:5px;width:80px">
								<?php  if($roImageUrl){ ?>
							     <img src="{{$roImageUrl}}" width="100" height="80" alt="RO Image"/>
						      <?php } ?>
							</td>
						</tr>
						<tr>
							<td colspan="8" class="removeborder leftborder">Buyer</td>
							<td  class="borderbottom">Invoice No.</td>
							<td class="borderbottom">Dated</td>
						</tr>
						<tr>
							
							<td colspan="8" class="removeborder leftborder" style="font-size:12px; font-weight:600">{{$tbl_invoice->company_name}}</td>
							<td  >PROFORMA</td>
							<td  ><strong>{{date('d-M-Y',strtotime($current_bill_date))}}</strong></td>
						</tr>
						<tr>
							<td colspan="8" class="removeborder leftborder">
								{{$tbl_invoice->address_one}} 

								@if(trim($tbl_invoice->address_two)!='')
								{{$tbl_invoice->address_two}} 
								@endif

								@if(trim($tbl_invoice->address_three)!='')
								{{$tbl_invoice->address_three}}
								@endif
							</td>
							<td  class="borderbottom">Mode/Terms of Payment</td>
							<td class="borderbottom"><strong>Immediate</strong></td>
						</tr>
						<tr>
							<td colspan="8" class="removeborder leftborder">
								{{$tbl_invoice->getcity->name}} - {{$tbl_invoice->pin_no}}
							</td>

							<td></td>
							<td></td>
						</tr>
						<tr>                         
							<td colspan="4"  class="removeborder leftborder">Contact : @if(trim($tbl_invoice->gender)=='Female') Ms @else Mr @endif . {{$tbl_invoice->Customer_Name}}</td>
							<td colspan="4"  class="removeborder">Ph : {{$tbl_invoice->Phone_Number}}</td>
							<td style="width:25%" class="borderbottom"></td>
							<td style="width:15%" class="borderbottom">Billing Upto</td>
						</tr>
						<tr>
							
							<td colspan="4"  class="removeborder leftborder">State Name : {{$tbl_invoice->getstate->name}} Code : {{$tbl_invoice->getstate->gstcode}}</td>
							<td colspan="4"  class="removeborder">PAN No. : {{$tbl_invoice->pan_no}}</td>
							<td  class="borderbottom"><strong></strong></td>
							<td class="borderbottom"><strong>{{str_replace('/', '-',$to_dates)}}</strong></td>
						</tr>
						<tr>
							
							<td colspan="4"  class="removeborder">Place of Supply : {{$tbl_invoice->getstate->name}}</td>
							<td colspan="4"  class="removeborder">GSTIN No. : {{$tbl_invoice->gst_no}}</td>
							
							<td style="font-size:16px;"></td>
							<td style="font-size:16px;"></td>
						</tr>
					  </table>
						
						<table class="table table-bordered"width="100%" style="border: 1px solid #dee2e6; margin-top: 10px">
						<tr>
							<th width="1%;">S.No.</th>
							<th >Inv.Date</th>
							<th>GST	Inv. No.</th>
							<th width="13%;">Vehicle No.</th>
							<th width="20%;">Product	Sold</th>
							
							<th>Qty	Sold</th>
							<th width="10%;">Nett Value</th>
							<th>HSN / SAC Code</th>
							<th>Bill chgs %</th>
							<th>Billing	Charges</th>
						</tr>
						<?php $petroldiesel_qty=0; $i=1; $amount=0; $toAmount=0; ?>
					   <?php $gst=[]; 
					         $taxblevalue=[];
					         $gstprice=[];
                             $gstper=[];
                             $Gamount=0;
					   ?>
                        
					    
					     @foreach($transactionss as $singletransactionModel)
					        <?php $pargst=0;?>
							<tr class="removebordertr">
								<td width="1%;" align="right"><?php echo $i;  $i++; ?></td>
								<td align="center">{{date('d/m/Y',strtotime($singletransactionModel->trans_date))}}</td>
								<td>{{$singletransactionModel->invoice_number}}</td>
								<td>{{$singletransactionModel->Vehicle_Reg_No}}</td>
								<td>{{$singletransactionModel->Item_Name}}</td>
								
								<td align="right">
									{{bcadd(round($singletransactionModel->petroldiesel_qty,3), 0, 3)}}
			                        <?php $petroldiesel_qty=$singletransactionModel->petroldiesel_qty+$petroldiesel_qty;?>
								</td>
								
								<td align="right" width="10%;">
									<?php $sub=0;?>

									@foreach($singletransactionModel->gettex->where('Tax_Code','!=', 'VAT') as $tax)
					                          <?php $pargst=$tax->Tax_percentage+$pargst; ?>
					                          <?php $gst[$tax->Description]=$tax->position;?>


									 @endforeach

								        <?php

								        if($pargst!=0){
								        	$sub=($singletransactionModel->item_price*$singletransactionModel->petroldiesel_qty)/(1+$pargst/100);
								        }
                                        if(isset($taxblevalue[$singletransactionModel->id]))
                                        	$taxblevalue[$singletransactionModel->id]=$taxblevalue[$singletransactionModel->id]+$sub;
                                        else
                                        	$taxblevalue[$singletransactionModel->id]=$sub;


                                        $amount=$sub+$amount;
                                         
                                         $toAmount=$sub+$toAmount;
								        ?>

								        @foreach($singletransactionModel->gettex->where('Tax_Code','!=', 'VAT') as $tax)
				                          <?php 
				                          $itemtaval=$taxblevalue[$singletransactionModel->id];
				                           if(isset($gstprice[$tax->Description])){
				                          	   $gstper[$tax->Description]=$gstper[$tax->Description]+$tax->tax_name;
                                               $gstprice[$tax->Description]=$gstprice[$tax->Description]+$taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100;
				                          
				                          }else{
				                          	    $gstper[$tax->Description]=$tax->tax_name;
				                          		$gstprice[$tax->Description]=$taxblevalue[$singletransactionModel->id]*$tax->Tax_percentage/100; 
				                          }
				                          ?>
								        @endforeach
                                       <?php $Gamount=$Gamount+($singletransactionModel->petroldiesel_qty*$singletransactionModel->item_price);?>
								    {{ bcadd($singletransactionModel->petroldiesel_qty*$singletransactionModel->item_price, 0, 2)}}
								</td>
								<td align="center">{{$sevicehsn}}</td>
								<td align="right">{{$handling_fee}}%</td>
								<td align="right"> @if($handling_amount!=0) {{ bcadd(($singletransactionModel->petroldiesel_qty*$singletransactionModel->item_price)*$handling_fee/100, 0, 2)}} @else {{ bcadd(0, 0, 2)}} @endif</td>
							</tr>
							@endforeach
							<tr class="removebordertr"><td colspan="10">&nbsp; &nbsp;</td></tr>
							<tr class="removebordertr"><td colspan="10">&nbsp; &nbsp;</td></tr>

							
						
						<tfoot>
							@if($discountPer!=0)
							<tr >
								<td colspan="5" align="right" ><strong><i>Total Incl. Taxes</i></strong></td>
								<td align="right">{{bcadd(round($petroldiesel_qty,3), 0, 3)}} </td>
								<td align="right" style="border-left: transparent!important;" ><strong>{{bcadd(round($Gamount,2), 0, 2)}}</strong></td>
								<td colspan="3" align="right" style="border-right: transparent!important;">{{bcadd(round($handling_amount,2), 0, 2)}}</td>
							</tr>
							@endif
							<tr class="removebordertr leftborder1">
								<td width="1%"></td>
								<td class="leftborder1">Less :</td>
								<td class="leftborder1" colspan="3" align="right"><strong>Discount</strong></td>
								<td ></td>
								<td align="right"><strong>{{$discountPer}}%</strong></td>
								<td></td>
								<td class="leftborder1"></td>
								

								<td align="right"><?php 
								if(!isset($mainInput['preview-billing-charges'])){
								$discount = $handling_amount*$discountPer/100;
							     }
								?>

							- {{bcadd(round($discount,2), 0, 2)}}</td>
							</tr>
							<tr class="removebordertr leftborder1">
								<td width="1%"></td>
								<td class="leftborder1">Add :</td>
								<td class="leftborder1" colspan="3" align="right"><strong>Billing Charges</strong></td>
								<td align="right"></td>
								<td align="right"><strong></strong></td>
								<td align="right"></td>
								<td class="leftborder1"></td>
								<td align="right">{{bcadd(round($handling_amount,2), 0, 2)}}</td>
							</tr>
							
							<?php 
							    asort($gst);
							    $n=0;

							?>
						  <?php $ams=0;?>
                          
                            
							 @foreach($comiGst as $k=>$comiGsts)
                            <?php $ams=round($comiGstA[$k],2)+$ams;?>
							<tr class="removebordertr leftborder1">
								<td width="1%"></td>
								<td class="leftborder1"></td>
								<td class="leftborder1" colspan="3" align="right"><strong>{{$k}}</strong></td>
								<td align="right"><strong></strong></td>
								<td align="right"><strong>{{$compar[$k]}}%</strong></td>
								<td align="right"></td>
								<td class="leftborder1"></td>
								<td align="right">{{bcadd(round($comiGstA[$k],2), 0, 2)}}</td>
							</tr>
							@endforeach
							
							<tr class="removebordertr leftborder1">
								<td width="1%"></td>
								<td class="leftborder1"></td>
								<td class="leftborder1" colspan="3" align="right"><strong>Rounded Off</strong></td>
								<td align="right"></td>
								<td align="right"><strong></strong></td>
								<td align="right"></td>
								<td class="leftborder1"></td>
								<td align="right"> 
									<?php 
											$amw=($handling_amount+$ams)-$discount;
								  			$ron=round($amw)-$amw
								  			?> {{bcadd(round($ron,2), 0, 2)}}</td>
							</tr>
							<tr>
								<td colspan="8" align="right" ><strong>TOTAL BILL VALUE</strong></td>
								
								
								<td colspan="2" align="right" style="border-left: transparent!important;"><strong>{{bcadd(round($amw), 0, 2)}}</strong></td>
							</tr>
							<tr>
								<td colspan="8" align="right" ><strong>VALUE OF ITEMS SOLD</strong></td>
								
								
								<td colspan="2" align="right" style="border-left: transparent!important;"><strong>{{bcadd(round($Gamount,2), 0, 2)}}</strong></td>
							</tr>
							<tr>
								<td colspan="8" align="right" ><strong>GRAND TOTAL DUE</strong></td>
								
								
								<td colspan="2" align="right" style="border-left: transparent!important;"><strong>{{bcadd(round(($Gamount+round($amw)),2), 0, 2)}}</strong></td>
							</tr>
							
						</tfoot>
						</table>

					
<?php 
   
$amw=abs($amw);
    $sums=bcadd(round($amw), 0, 2); 
    
     $va=explode('.',$sums);
     if(isset($va[1]) && intval($va[1])>50){
       $no = round($sums);
       $no = $no-1;
       $point = intval($va[1]);
     }else{
     	$no = round($sums);

     	if(isset($va[1]))
     	  $point = intval($va[1]);
        else
     	 $point = 0;
     }
   
   
   
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array('0' => '', '1' => 'One', '2' => 'Two',
    '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
    '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
    '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
    '13' => 'Thirteen', '14' => 'Fourteen',
    '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
    '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
    '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
    '60' => 'Sixty', '70' => 'Seventy',
    '80' => 'Eighty', '90' => 'Ninety');
   $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $sums = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($sums) {
        $plural = (($counter = count($str)) && $sums > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($sums < 21) ? $words[$sums] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($sums / 10) * 10]
            . " " . $words[$sums % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode('', $str);
  $points = ($point) ?
    "." . $words[$point / 10] . " " . 
          $words[$point = $point % 10] : '';

?>
						
		<table class="table table-bordered"width="100%" style="border: 1px solid #dee2e6; margin-top: 10px">
			<tr>
					<td colspan="{{(count($comiGst)+count($gst)*2)+3}}">Amount Chargeable (in words)</td>
					<td colspan="2" align="right"><i>E. & O.E</i></td>
			</tr>
			<tr>
				<td colspan="{{(count($comiGst)+count($gst)*2)+5}}">
                 <strong>
                @if($handling_amount==0)
                     Rs. Zero Rupees Only
                 @else

					@if($points)
					  Rs. <?php  echo $result . "Rupees  " . str_replace('.','And ', $points) . " Paise"; ?> Only</strong>
					@else
					  Rs. <?php  echo $result . "Rupees  "; ?> Only
					@endif

				@endif
				 </strong>
				</td>
					
			</tr>
			
			
		<?php $am=0;?>
		
        	@if($sevice!=null)
			<tr>

				<th width="15%;">Name</th>
				<th>HSC / SAC</th>
				<th>Taxable  Amt</th>
				<th>GST %</th>
				@foreach($comiGst as $k=>$comiGsts)
				<th>{{$k}}</th>
				@endforeach
				@foreach($gst as $key=>$gsts)
				<th align="right" colspan="2">&nbsp;&nbsp;</th>
				@endforeach
				
				<th width="10%;">Total Amount</th>
			</tr>
			<tr>
				          
				<td>{{$sevice->name}}</td>
				<td align="right">{{$sevice->hsn}}</td>
				<td align="right">{{bcadd(round($handling_amount,2), 0, 2)}}</td>
				<td align="right">{{$gstPE}}%</td>
				@foreach($comiGst as $k=>$comiGsts)
				<td align="right">{{bcadd(round($comiGstA[$k],2), 0, 2)}}</td>
				@endforeach
				@foreach($gst as $key=>$gsts)
				<td align="right" colspan="2">&nbsp;&nbsp;</td>
				@endforeach
				
				<td align="right"> 
					<?php $am=0;?>
					@foreach($comiGst as $k=>$comiGsts)
					   <?php $am=round($comiGstA[$k],2)+$am;?>
					@endforeach
					{{bcadd(round($handling_amount+$am,2), 0, 2)}}
				</td>
			</tr>
			<tr>
				<td align="right"><strong>Total</strong></td>
				<td align="right"><strong></strong></td>
				<td align="right"><strong>{{bcadd(round($handling_amount,2), 0, 2)}}</strong></td>
				<td align="right"></td>
				@foreach($comiGst as $k=>$comiGsts)
				<td align="right"><strong>{{bcadd(round($comiGstA[$k],2), 0, 2)}}</strong></td>
				@endforeach
				@foreach($gst as $key=>$gsts)
				<td align="right" colspan="2"><strong>&nbsp;&nbsp;</strong></td>
				@endforeach
				
				<td align="right"><strong>{{bcadd(round($handling_amount+$am,2), 0, 2)}}</strong></td>
			</tr>
			<tr>
				<td align="right" colspan="2"><strong>GRAND TOTAL</strong></td>
				<td align="right"><strong>{{bcadd(round($handling_amount,2), 0, 2)}}</strong></td>
				<td align="right"><strong></strong></td>
				@foreach($comiGst as $k=>$comiGsts)
				<td align="right"><strong>{{bcadd(round($comiGstA[$k],2), 0, 2)}}</strong></td>
				@endforeach
				@foreach($gst as $key=>$gsts)
				<td align="right" colspan="2"><strong>&nbsp;&nbsp;</strong></td>
				@endforeach
				
				
				<td align="right"><strong>{{bcadd(round($am+$handling_amount,2), 0, 2)}}</strong></td>
			</tr>

			@endif

		
     
	
        <!--vihicle-->
        
		   <tr rowspan="2"><td colspan="{{(count($comiGst)+count($gst)*2)+5}}">&nbsp;&nbsp;</td></tr>
			<tr>
				<th>Vehicle Summary</th>
				<th>Amount</th>
				<th>&nbsp; &nbsp;</th>
                <th>&nbsp; &nbsp;</th>
               @foreach($gst as $key=>$gsts)
	                <?php $gst[$key]=0; ?>
					<th >
						&nbsp; &nbsp;
					</th>
					<th >
						&nbsp; &nbsp;
					</th>
                @endforeach

				 @foreach($comiGst as $k=>$comiGsts)
				   <th>&nbsp; &nbsp;</th>
				@endforeach
				<th width="10%;">Total Amount</th>
			</tr>
			<?php $totalQTR=0;
				  $totalTPRS=0;
				  $tamount=0;
			?>
			@foreach($transactionvihicles as $keys=>$transactionModel)

				<tr>
					<td>{{$keys}}</td>
					<td align="right">
						<?php $qtr=0;?>
				      	   @foreach($transactionModel as $singletransactionModel)
				      	   <?php $qtr=($singletransactionModel->petroldiesel_qty*$singletransactionModel->item_price)+$qtr;?>
				      	   @endforeach
		      	   		
		      	   		{{bcadd(round($qtr,2), 0, 2)}}
		      	   		<?php $totalQTR=$qtr+$totalQTR;?>

					</td>
					<td align="right"> 
						<?php $tottex=0;?>
				      	   @foreach($transactionModel as $singletransactionModel)
				      	   <?php $tottex=$taxblevalue[$singletransactionModel->id]+$tottex;?>
				      	    
				      	   @endforeach
		      	   		
		      	   		<?php $totalTPRS=$tottex+$totalTPRS;?>
		      	    </td>
		      	    <td></td>

                    @foreach($gst as $key=>$gsts)
						<td align="right"> &nbsp; &nbsp;</td>
						<td align="right">&nbsp; &nbsp;</td>
					@endforeach

					 @foreach($comiGst as $k=>$comiGsts)
					   <td></td>
					@endforeach
					<td align="right">

						  <?php $to=0;?>
				      	   @foreach($transactionModel as $singletransactionModel)
		                             <?php $to=$singletransactionModel->petroldiesel_qty*$singletransactionModel->item_price+$to; ?>
						    @endforeach
				    		<?php $tamount=$to+$tamount;?>
				    		{{bcadd(round($to,2), 0, 2)}}

					</td>
				</tr>
				<?php

                   /* $InvoiceVihicleSumary= new App\InvoiceVehicleSummary();
                    $InvoiceVihicleSumary->invoice_no=$invoice_no;
                    $InvoiceVihicleSumary->vihcle_no=$keys;
                    $InvoiceVihicleSumary->amount=$to;
                    $InvoiceVihicleSumary->save();*/
			     ?>
			@endforeach
			<tr>
				<td align="right"><strong>Total</strong></td>
				<td align="right"><strong>{{bcadd(round($totalQTR,2), 0, 2)}}</strong></td>
				<td align="right"><strong>&nbsp; &nbsp;</strong></td>
				 <td></td>

				<?php $taxamount=0;?>
               @foreach($gst as $key=>$gsts)
              
               <?php $taxamount=$gstprice[$key]+$taxamount;?>
				<td align="right">&nbsp; &nbsp;</td>
				<td align="right"><strong>&nbsp; &nbsp;</strong></td>
                @endforeach

				@foreach($comiGst as $k=>$comiGsts)
					   <td>&nbsp; &nbsp;</td>
				 @endforeach
				<td align="right"><strong>{{bcadd(round($tamount,2), 0, 2)}}</strong></td>
			</tr>
	
			<tr>
				<th colspan="{{(count($comiGst)+count($gst)*2)+5}}">&nbsp;</th>
			</tr>




			<tr>
				



				<td colspan="8" align="right" style="font-size: 17px;font-weight: bold;text-align: right"><strong>TOTAL PAYABLE AMOUNT</strong></td>
								
								
								<td colspan="4" align="right" style="border-left: transparent!important;border-left: transparent!important;font-size: 17px;font-weight: bold;text-align: right"><strong>{{bcadd(round(($Gamount+round($amw)),2), 0, 2)}}</strong></td>
			</tr>
	
			<tr>
				<th colspan="{{(count($comiGst)+count($gst)*2)+5}}">&nbsp;</th>
			</tr>

		  </table>
		  <table class="table table-bordered">
			<tr>
				<td colspan="{{(count($comiGst)+count($gst)*2)+5}}"><strong>Our Bank Details : A/c Name : {{Auth::user()->getRocode->getdetails->Account_Name}}, A/c No : {{Auth::user()->getRocode->getdetails->Account_Number}} Bank : {{Auth::user()->getRocode->getdetails->Bank_Name}}, IFSC Code : {{Auth::user()->getRocode->getdetails->IFSC_Code}}</strong></td>
			</tr>
			
			<tr>
				<td colspan="{{(count($comiGst)+count($gst)*2)+5}}"><strong>Terms & Conditions :{{Auth::user()->getRocode->getdetails->TC_for_GST_Invoice}} {{Auth::user()->getRocode->getdetails->TC_for_GST_Invoice2}}</strong></td>
			</tr>
			<tr>
				<td width="50%" colspan="{{(count($comiGst)+count($gst)*2)+2}}"><strong>Customer's Seal and Signature</strong></td>
				<td width="50%" colspan="3" align="right"><strong>for {{Auth::user()->getRocode->pump_legal_name}} <br/> <br/>Authorised Signatory</strong></td>
			</tr>
			
		
		</table>
		<div style="width:100%;">
			<div style="width:50%;float:left;">
				<strong>This is a Computer Generated Invoice</strong>
			</div>
			<div style="width:50%;float:right;text-align: right;">
			<strong>Powered by www.garruda.co.in</strong>
			</div>


		</div>


<!-- start  model  -->



<div class="container">
  
  <!-- <button style="margin-bottom:50px;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#pdfPreview">Change Tax</button> -->

  <div  id="" style="margin-top: 40px;margin-bottom: 30px" class="col-md-12" >
  	<div style="text-align: left" class="col-md-6">

  	<a style="margin-right: 3px;"  id="id-back" href="{{URL('invoiceindex')}}" type="button" class="btn btn-primary ">Back</a>

 
  	<input id ="id-pdfPreview" type="button" class="btn btn-primary " data-toggle="modal" data-target="#pdfPreview" value="Change Discount value or Billing Charges">
  	
  	
  	
    </div>
    <div style="" class="col-md-2 pull-left" style="">
  	 <form class="form-horizontal form-inline" name="myForm" action="{{url('invoicepdf')}}" enctype="multipart/form-data" method="post">
              {{ csrf_field() }}
              <?php 
              $cus_id=[];
              $cus_id=$mainInput['cus_id']; 

              if(isset($mainInput['preview-billing-charges'])){
              ?>

			    <input type="hidden" name="preview-discount"  value="{{$mainInput['preview-discount']}}">

			  <?php 
              }

              if(isset($mainInput['preview-discount'])){
              ?>
			  
			    <input type="hidden" name="preview-billing-charges"  value="{{$mainInput['preview-billing-charges']}}">

			    <?php 
			}
			?>
			  
			  <!-- <button type="submit" class="btn btn-default">Submit</button> -->
            
             <input type="hidden" name="Discount" value="{{$mainInput['Discount']}}"> 
	          <input type="hidden" name="fule_type" value="{{$mainInput['fule_type']}}"> 
	          <input type="hidden" name="current_bill_date" value="{{$mainInput['current_bill_date']}}"> 
	          
	          <input type="hidden" name="from_dates" value="{{$mainInput['from_dates']}}">
	          <input type="hidden" name="to_dates" value="{{$mainInput['to_dates']}}">
	          <input type="hidden" name="RO_code" value="{{$mainInput['RO_code']}}">
	         
	         <?php  foreach($cus_id as $value)
				{ ?>
				  <input type="hidden" name="cus_id[]" value={{$value}}>
				<?php } ?>
	        
	    <input style="margin-left: -100px" onclick="clicked();"  name="pdf" type="submit" class="btn btn-primary " value="Generate Bill">    

         </form>
         </div>	

         <input type="button" style="margin-left: -100px"   id="myPrntbtn" class="btn btn-primary" onclick="printDiv();" value="PROFORMA" >

           <!--  <input type="button"  id="myPrntbtn" class="btn btn-primary" onclick="printDiv();" value="PRINT PROFORMA" style="margin-left: -120px"> -->
 
 
  	
  </div>

  <!-- Modal -->
  <div class="modal fade" id="pdfPreview" role="dialog" style="margin-top: 80px;height:300px">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Charges</h4>
        </div>
        <div class="modal-body" style="height: 250px">

         <form class="form-horizontal form-inline" name="myForm" action="{{url('invoicepdf')}}" enctype="multipart/form-data" method="post">
              {{ csrf_field() }}
              <?php 
              $cus_id=[];
              $cus_id=$mainInput['cus_id']; ?>

              <div class="form-group col-md-12">
			    <label for="Discount" class="col-md-6" >Discount:</label>
			    <input type="text" name="preview-discount" class="form-control col-md-6" id="id-discount" value="{{$discount}}">
			  </div>
			  <div class="form-group col-md-12">
			    <label for="pwd" class="col-md-6">Billing Charges(%):</label>
			    <input type="text" name="preview-billing-charges" class="form-control col-md-6" id="id-billing-charges" value="{{$handling_fee}}">
			  </div>
			 
			  <!-- <button type="submit" class="btn btn-default">Submit</button> -->
            
             <input type="hidden" name="Discount" value="{{$mainInput['Discount']}}"> 
	          <input type="hidden" name="fule_type" value="{{$mainInput['fule_type']}}"> 
	          <input type="hidden" name="current_bill_date" value="{{$mainInput['current_bill_date']}}"> 
	          
	          <input type="hidden" name="from_dates" value="{{$mainInput['from_dates']}}">
	          <input type="hidden" name="to_dates" value="{{$mainInput['to_dates']}}">
	          <input type="hidden" name="RO_code" value="{{$mainInput['RO_code']}}">
	         
	         <?php  foreach($cus_id as $value)
				{ ?>
				  <input type="hidden" name="cus_id[]" value={{$value}}>
				<?php } ?>
	        
	        
<div class="center-block" id="id-form-preview" style="text-align: center" ><input name="pdf" type="submit" id="preview_pdf" class="btn btn-primary " value="Preview"></div>

         </form>	




        </div><!--  model body end -->
       <!--  <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>
  
</div>



	</div>	<!-- wrap parent -->
  </body>
<script type="text/javascript">
    function clicked() {
       if (confirm('Do you want to genrate bill?')) {
           yourformelement.submit();
       } else {
           return false;
       }
    }
</script>

<script type="text/javascript">
         function printDiv(){
        
			var originalContents = document.body.innerHTML;
    var printReport= document.getElementById('id-content-wrapper').innerHTML;
    document.body.innerHTML = printReport;
    window.print();
    document.body.innerHTML = originalContents;
		}
      </script>

		