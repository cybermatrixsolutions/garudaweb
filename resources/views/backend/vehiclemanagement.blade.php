@extends('backend-includes.app')

@section('content')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Vehicles</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Vehicles </a></li>
        </ul>
      </div>

    </div>
     <div class="">
       
                            </div>  
                           
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
           @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
           <center>@if(Session::has('success'))
             <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Vehicle </h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                     <form class="form-horizontal"  onsubmit="return confirm('Do you want to Continue?');" action="{{'vechstore'}}" enctype="multipart/form-data" method="post" >
                           {{ csrf_field() }}
                           <input type="hidden" class="form-control" id="capacity" placeholder="Capacity" name="capacity">
                      <!-- <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Pump Legal Name</label>
                          <div class="col-sm-8">
                            
                            <select class="form-control" required="required" id="sel1" name="Ro_code">
                              @foreach($data1 as $row) 
                              <option value="{{$row->RO_code}}">{{$row->pump_legal_name}}</option>
                              
                               @endforeach
                            </select>
                          </div>
                        </div> -->
                        <input type="hidden" id="sel1" name="Ro_code" value="{{Auth::user()->getRocode->RO_code}}">
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Customer Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                              <select class="form-control" required="required" id="customer_code" name="Customer_code">
                                </select>
                           
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Registration Number <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <input type="text" required="required" class="form-control checkunique" id="Registration_Number" data-customercol="Customer_code"  data-mass="Registration Number Already Exist" data-table="tbl_customer_vehicle_master"  data-colum="Registration_Number" placeholder="Registration Number" name="Registration_Number">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Only Pre-Authorised</label>
                          <div class="col-sm-8">
                             <input type="checkbox"  name="pre_authori" value="1"><label>Pre-Authorised</label>
                          </div>
                        </div>

                         <div class="form-group">
                          <label class="control-label col-sm-4" for="RC Valid Upto">RC Valid Upto</label>
                          <div class="col-sm-8">
                             <input type="text" class="form-control" id="datepicker" placeholder="rc_valide" name="rc_valide">
                          </div>
                        </div> 
                          <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Insurance Due Date</label>
                          <div class="col-sm-8">
                             <input type="text" class="form-control" id="datepickerss" placeholder="insurance_due_date" name="insurance_due_date">
                          </div>
                        </div>  
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">PUC Date</label>
                          <div class="col-sm-8">
                             <input type="text" class="form-control" id="datepickers" placeholder="PUC Date" name="puc_date">
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Make <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                          <select class="form-control make1" required="required" id="make1"  name="Make">
                              @foreach($vehicleMakeList as $row) 
                              <option value="{{$row->id}}">{{$row->name}}</option>
                               @endforeach
                            <!--    <option value="Others">Others</option> -->
                            </select>
                             <input type="text"  class="form-control other" id="other" placeholder="Make"  name="Makeother">
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-4" for="Model">Model <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">      
                           <select class="form-control Model1" required="required" id="Model1"  name="Model">
                            
                               <!-- <option value="other">Others</option> -->
                            </select>
                             <input type="text"  class="form-control others" id="others" placeholder="Model name"  name="modelother">
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Fuel Type <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <select class="form-control" required="required" name="Fuel_Type" id="Fuel_Type">
                              
                               </select>

                          </div>
                        </div> 
<!-- 
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="Capacity">Fuel Capacity <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <input type="text" readonly required="required" class="form-control" id="capacity" placeholder="Capacity" name="capacity">
                          </div>
                        </div>
 -->
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Colour</label>
                          <div class="col-sm-8">
                          <input type="text" class="form-control" id="" placeholder="Color" name="van_color">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Insurance Company<span style="color:red;">*</span></label>
                          <div class="col-sm-8">
                             <select class="form-control"  name="industry_company" id="industry">
                              @foreach($IndustryDepartment as $IndustryDepartments)
                               <option>{{$IndustryDepartments->value}}</option>
                                   @endforeach
                             
                                            </select>
                           
                          </div>
                        </div>
                       
                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <input  type="submit" class="btn btn-default submit-button" id="id-form-submit" disabled="disabled" value="Submit">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>S.No.</th>
                <!-- <th>Outlet Name</th> -->
                <th>Customer Name</th>
                <th>Registration Number</th>
                <th>Make</th>
                <th>Model</th>
                <th>Fuel Type</th>
                 <th>Status</th>
                 <th>Actions</th>
            </thead>
            <tbody>
              <?php $i=0; ?>
      
                 @foreach($data as $row) 
      
              <?php $i++ ;?>
               <tr>
               
                <td>{{$i}}</td>
                <!-- <td>{{$row->pump_name}}</td> -->
                <td>{{$row->company_name}}</td>
                <td>{{$row->Registration_Number}}</td>
                 <td>{{$row->getmake['name']}}</td>
                  <td>{{$row->getmmodel['name']}}</td>
                  <td>{{$row->type}}</td>
                  <td> <?php if($row->is_active==1){?>

                   <a  title="Active" href="vehiclemanagement/active/{{$row->id}}/0" onclick="return confirm('Do you want to deactivate? if you deactive this all relation will be deactivated');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="vehiclemanagement/active/{{$row->id}}/1" onclick="return confirm('Do you want to activate?');">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
          </td>

                <td><div class="btn-group">
                    <button type="button" @if($row->getCustomer->confirm=='1') class="btn btn-success" @else class="btn btn-danger" @endif>Action</button>
                    <button type="button" @if($row->getCustomer->confirm=='1') class="btn btn-success dropdown-toggle" @else class="btn btn-danger dropdown-toggle" @endif  data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      <!--<li><a href="#">View</a></li>-->
                      @if($row->is_active==1 & $row->getCustomer->confirm=='0')
                      <li><a href="vehiclemanagement/edit/{{$row->id}}" >Edit</a></li>
                      @endif
                      <li><a href="vehiclemanagement/View/{{$row->id}}" >View</a></li>
            
                      <!-- <li><a href="vehiclemanagement/delete/{{$row->id}}" onClick="return confirm('Are you sure?');">Delete</a></li> -->
                    </ul>
                  </div></td>
              </tr>
               @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

<script  src="{{URL::asset('js/index.js')}}"></script> 
<script>
  $(function () {
  $("#datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
  $(function () {
  $("#datepickers").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
  $(function () {
  $("#datepickerss").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $(".other").hide();
   
});
  $('.make1').change(function() {
    var selected = $(this).val();
    if(selected == 'Others'){
      $('.other').show();
    }
    else{
      $('.other').hide();
    }
});
  $(document).ready(function(){
    $(".others").hide();
   
});
  $('.Model1').change(function() {
    var selected = $(this).val();
    if(selected == 'other'){
      $('.others').show();
    }
    else{
      $('.others').hide();
    }
});
  jQuery(function(){

     var vil={
           init:function(){
               vil.selectRo();
               vil.getcom();
               vil.getmodel();
               
           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
             jQuery('#make1').on('change',function(){
                  vil.getmodel();
                 
              });
             jQuery('#Model1').on('change',function(){
                  vil.getItemByItem();
              });

            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getItemByItem:function(){
               
                jQuery.get('getItemByModel',{
                model:jQuery('#Model1').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                var opt='';
                  jQuery.each(data['items'], function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });

                   jQuery('#Fuel_Type').html(opt);
                   jQuery('#capacity').val(data['capacity']);

               });

           },
           getcom:function(){
               jQuery.post('getrocode',{
                rocode:jQuery('#sel1').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);
                   vil.getcrg();
               });
           },
           getcrg:function(){

             jQuery.post('getregierid',{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                //jQuery('#Registration_Number').val(data);
               });
           },
          
           getmodel:function(){
               jQuery.post('getmodelname',{
                make1:jQuery('#make1').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                console.log(data);
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                     
                      
                  });
                    console.log(opt);
                   
                   jQuery('#Model1').html(opt);
                   vil.getcrg();
                   vil.getItemByItem();
               });
           },
     }
     vil.init();
  });
</script>

<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                  var colum=jQuery(this).data('colum');
                  var customercol=jQuery(this).data('customercol');
                  var customer=jQuery('#customer_code').val();
                
                   //id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique_cust',{
                  table:table,
                  colum:colum,
                  customer:customer,
                  customercol:customercol,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});


            $(document).ready(function() {
    $("#id-form-submit").on("click", function(){
        return confirm("Do you want to Continue? ");
    });
});

</script>

@endsection