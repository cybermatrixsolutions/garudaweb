<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.cs')}}s" rel="stylesheet">
<!-- CSS-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">

<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<!-- Font-icon css-->
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
<style type="text/css">
  ul {
    list-style-type: none;
   
}
#itemtam ul{
	margin:0;
	padding:0 0 0 12px;
}
#itemtam ul li input{
	margin-right:12px;
}

.editform{
  margin-top:45px;
  background: #fff;
  padding:15px !important;
}    

.select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #2d2727 !important;
}
</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')  
  
  
  
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Item Tax Config</h1>
      </div>
      <div>
        <ul class="breadcrumb">
           <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          
          <li><a href="#">Tax Config</a></li>
        </ul>
      </div>
    </div>
		 @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="editform">
   
  
       
                        <div class="">
                     <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                            </div>
	 <form class="form-horizontal" action="{{'itemtaxsave'}}" method="post" onsubmit="return confirm('Do you want to Continue?');">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">

   
      <div>
        <input type="hidden" class="form-control" id="pump" name="pump" value="{{Auth::user()->getRocode->id}}">
     <!--  <div class="form-group">
        <label class="control-label col-sm-4" for="pwd">Pump Legel Name</label>
        <div class="col-sm-8">
           <select class="form-control" id="pump" name="pump" required/>
            @foreach($itemprice as $pricetax)
            <option  value="{{$pricetax->id}}"
                    >{{$pricetax->pump_legal_name}}
            </option>
            @endforeach
         </select>

         
        </div>
      </div> -->
   </div>
   
   <div class="col-md-12">
    <div>
      <div class="col-md-4">
      <div class="form-group">
        <label class="control-label col-sm-4" for="pwd">Stock Group</label>
        <div class="col-sm-7">
           <select class="form-control" id="tags" name="Tax_percentage" required/>
          </select>
        </div>
       </div>
     </div>
       <div class="col-md-4">
       <div class="form-group">
        <label class="control-label col-sm-5" for="pwd">Sub Stock Group</label>
        <div class="col-sm-7">
           <select class="form-control" id="subgroup" name="" required/>
          </select>
        </div>
       </div>
     </div>
         <div class="col-md-4">
       <div class="form-group">
        <label class="control-label col-sm-4" for="pwd">Tax Type</label>
        <div class="col-sm-7">
           <select class="form-control" id="typetax" name="typetax" required/>
           <option value="GST">GST</option>
            <option value="VAT">VAT</option>
          </select>
        </div>
      </div>
       </div>
     </div>
  
  <!--  <div class="col-md-2"><div class="form-group">
        <label ><a id="filter" href="#" class="btn btn-primary site-btn submit-button" style="padding: 5px 8px; font-size: 12px; background: red">Filter</a></label>
        
       </div></div> -->
 </div>
 <div class="row">

  
     <div class="col-md-6">
     
      <label class="control-label" style="margin-left: 12px; display:inline" for="pwd">Item Name</label><br>
       <input type="checkbox" name="select-all" id="select-all" style="margin-left:12px; display:inline"/><label class="control-label" 
	   for="pwd" style="margin-left: 12px; display:inline">Select All Item</label>
      <div id="itemtam">
    
       </div>
       <div id="tagsa">
       </div>
   </div>
     <div class="col-md-6">
      <div class="form-group">
        <label class="control-label col-sm-3" for="pwd">Tax</label>
            <div class="col-sm-8">
              <select class="form-control" id="tagsq" multiple="multiple" required="required" name="tax[]">
              </select>
            </div>
       </div>

      
      <div class="form-group">
        <label class="control-label col-sm-3" for="pwd">Effective Date</label>
            <div class="col-sm-8">
              <input type='text' class="form-control datepicker" name="effective_date">
            </div>
       </div>
       <div class="form-group">
        <label class="control-label col-sm-3" for="pwd">HSN Code</label>
            <div class="col-sm-8">
              <input type='text' class="form-control" id="hsncode" name="hsn_code">
            </div>
       </div>
     
     <div class="form-group">
        <label class="control-label col-sm-8" ></label>
            <div class="col-sm-3">
             <input type="submit" id="success-btn" class="btn btn-primary site-btn submit-button  pull-right" value="Submit"></center>

            </div>
       </div>
     
    
    
   </div>

   
   
  </div>

    
   </form>
   
  </div> 

                      
</div>
</div>
<!-- Javascripts--> 

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="js/index.js"></script>
<script type="text/javascript">
 


</script>
<script type="text/javascript">
  $('#select-all').click(function(event) {   
    if(this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;                        
        });
    }
    else{
$(':checkbox').each(function() {
            this.checked = false;                        
        });

    }
});
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
<script type="text/javascript">
 jQuery(function(){
     var url="{{url('getItembyajex')}}";
     var vil={
           init:function(){
             vil.selectRo();
              vil.getcrg();
              //vil.getsubgroup();

                 jQuery('#hsncode').on('keyup',function(){
                          var hsn= jQuery(this).val();
                          if(jQuery.trim(hsn).length>8){
                            jQuery('#success-btn').attr('disabled',true);
                            jQuery(this).css('border-color','red');
                            alert('max length is 8 digit');
                          }else{
                            jQuery('#success-btn').attr('disabled',false);
                             jQuery(this).css('border-color','');
                          }

                    });

           }, 

           
           selectRo:function(){
           	jQuery('#filter').on('click', function(){
           	   vil.getItem();
           });
            /*jQuery('#pump').on('change',function(){
                  vil.getcrg();
              });*/
            jQuery('#subgroup').on('change',function(){
                  vil.getItem();
              });

            jQuery('#tags').on('change',function(){
                vil.getsubgroup();

            });
            

           
           },
           getsubgroup:function(){
            
             jQuery.get('getsubgroup',{
                subgroup:jQuery('#tags').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                 var optq='';

                  jQuery.each(data, function(index,value){

                     opt+='<option  value="'+index+'">'+value+'</option>';
                    
                  });
                    console.log(opt);
                  
                jQuery('#subgroup').html(opt);
                 vil.getItem();
                    
               });
           },
           getcrg:function(){
            
             jQuery.get('getstockcode',{
                rocode:jQuery('#pump').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                 var optq='';

                  jQuery.each(data, function(index,value){

                     opt+='<option  value="'+index+'">'+value+'</option>';
                    
                  });
                    console.log(opt);
                  
                jQuery('#tags').html(opt);
                jQuery('#tagsa').html(optq);
                vil.getsubgroup();
                    //vil.getItem();
               });
           },

           getItem:function(){
            jQuery('#select-all').prop('checked', false);
            var pump=jQuery('#pump').val();
            var tags=jQuery('#subgroup').val();
               jQuery.get(url+'/'+pump+'/'+tags,{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                
                   
                   jQuery('#itemtam').html(data);
                   
               });
           },

     }
     vil.init();
  });
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
             vil.getcrg();
           },
           selectRo:function(){
            jQuery('#pump').on('change',function(){
                  vil.getcrg();
                   });
            jQuery('#typetax').on('change',function(){
                vil.getcrg();

           
                 
              });
           
           },
           getcrg:function(){
            
             jQuery.get('getrocode1',{
                 rocode:jQuery('#pump').val(),
                 typetax:jQuery('#typetax').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';

                jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                  
                jQuery('#tagsq').html(opt);
                // vil.getcrg();

               });

             $('#tagsq').select2({
  data: [],
    tagsq: true,
    tokenSeparators: [','], 
    placeholder: "Select Tax"
});
 
           },

     }
     vil.init();
  });
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>

<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
</body>
</html>