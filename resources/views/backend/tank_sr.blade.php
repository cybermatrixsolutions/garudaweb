<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">

<title>Garruda</title>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<style>
.modal-dialog{
width: 80%;
}
input { 
    text-transform: uppercase;
}
::-webkit-input-placeholder { /* WebKit browsers */
    text-transform: none;
}
:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
    text-transform: none;
}
::-moz-placeholder { /* Mozilla Firefox 19+ */
    text-transform: none;
}
:-ms-input-placeholder { /* Internet Explorer 10+ */
    text-transform: none;
}

#viewdisplay{
    
    width: 500px;
    background: #fff;
    display: none;
    top: 200px !important;
    position: absolute;
    right: 50px;
    z-index: 99999999999;
    box-shadow: 2px 1px 20px #8a8282;
    border-radius: 3px;
}
#tankdisplay{
    
    width: 500px;
    background: #fff;
    display: none;
    top: 200px !important;
    position: absolute;
    right: 50px;
    z-index: 99999999999;
    box-shadow: 2px 1px 20px #8a8282;
    border-radius: 3px;
}
</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Tanks</h1>
      </div>
     
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Tanks</a></li>
        </ul>
      </div>
    </div>

    <div class="">
      
    
      
      
    </div>
    <div class="row">

      <div class="col-md-12 col-sm-12">

        <div class="row">
            <center>
               @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
        @if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Tank</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form name="myForm" class="form-horizontal"  action="{{url('save_tank_management')}}" method="post" onsubmit="return validateForm()">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" class="form-control" name="RO_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
                        <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Pump Legal Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="RO_code" id="sel1">
                             
                                                    @foreach($datas as $ro_master)
                                                        <option name="company_id" value="{{$ro_master->RO_code}}"
                                                                >{{$ro_master->pump_legal_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Tank Number</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="Tank_Number" id="Tank_Number" placeholder="Tank Number">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="Dip chart">Dip Charts</label>
                          <div class="col-sm-8">
                             <select class="form-control" id="dip_charts" name="dipchartd_dis">
                            @foreach($dipchart as $dipchar) 
                              <option value="{{$dipchar->id}}">{{$dipchar->volume}}&nbsp; &nbsp;{{$dipchar->discretion}}</option>
                              
                               @endforeach
                            </select>
                            
                          </div>
                          <div class="col-sm-1">
                              <a href="#" id="viewchart" data-id="1">view</a>
                          </div>
                        </div>

                         
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                           
                              <span style="color:#f70b0b;">* if your dip not exist please contact Garruda support</span>

                             <!-- <a href="{{url('dipCharts')}}" class="btn btn-default">Add New</a> -->
                           <!--  <button type="submit" class="btn btn-default submit-button" disabled="disabled">Submit</button> -->
                          </div>
                        </div>
                        <!-- <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <a href="{{url('dipCharts')}}" class="form-control"  data-id="1">Add New</a>
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Fuel Type</label>
                          <div class="col-sm-9">
                             <select class="form-control" id="Pedestal_id" name="fuel_type">
                            @foreach($fuel_type as $fuel) 
                              <option value="{{$fuel->id}}">{{$fuel->Item_Name}}</option>
                              
                               @endforeach
                             
                            </select>
                            
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Capacity <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input type="text" required class="form-control numeriDesimal" name="Capacity" id="morecap" placeholder="Capacity">
                          </div>
                        </div>
                       
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Unit of Measure<span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                                <select class="form-control" required="required" name="Unit_of_Measure" id="Unit_of_Measure">
                             
                                                    @foreach($unit_measure as $unit)
                                                        <option  value="{{$unit->id}}"
                                                                >{{$unit->Unit_Symbol}}-{{$unit->Unit_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Opening Stock</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control numeriDesimal" name="Opening_Reading" id="lowstock" placeholder="Opening Stock">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Opening Date</label>
                          <div class="col-sm-9">

                            <input class="form-control" id="datepicker" name="opening_date"  type="text" />

                          </div>
                        </div>
                          
                       
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Remarks </label>
                          <div class="col-sm-9">
                            <input type="text"  class="form-control" name="remarks" id="" placeholder="Remarks">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Active From Date</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="datepickers" value="01/04/2017" name="active_from_date" placeholder="Active From Date">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default submit-button" disabled="disabled">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            @if(isset($tank_list) && count($tank_list) > 0)
            <thead>
              <tr>
                <th>S.No.</th>
               <!--  <th>Outlet Name</th> -->
                <th>Tank Number</th>
                 <th>Capacity</th>
                  <th>Opening Date</th>
                  
                  <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
               <?php $i=0; ?>
               @foreach($tank_list as $list)
               <?php $i++;?>
              <tr>
                 <td>{{$i}}</td>
               <!--  <td scope="row">{{$list->pump_name}}</td> -->
                <td><a href="#" class="tankchart" data-id="{{$list->id}}">{{$list->Tank_Number}}</a></td>
                <td>{{$list->Capacity}}</td>

                 <td>{{Carbon\Carbon::parse($list->Opening_Date)->format('d/m/Y')}}</td>

              
                <td> <?php if($list->is_active==1){?>

                   <a  title="Active" href="{{URL('tankDeactive')}}/<?php echo $list->id;?>" onclick="return confirm('Do you want to deactivate? if you deactive this all relation will be deactivated.');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="{{URL('tankActive')}}/<?php echo $list->id;?>" onclick="return confirm('Do you want to activate?');">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
          </td>
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      @if($list->is_active==1)

                      <li><a href="tank_update/{{$list->id}}">Edit</a></li>
                      @endif
                       <li><a href="tank_view_page/{{$list->id}}">View</a></li>
                      <!-- <li><a href="delete_tank/{{$list->id}}"onclick="return confirm('are u sure to delete?')">Delete</a></li> -->
                    </ul>
                  </div></td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
  
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Javascripts--> 
<!--chart view model-->

<div id="viewdisplay">
  <div class="modal-header">
    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span id="chartdelete">×</span></div>
    <h4 class="modal-title" >Dip Chart</h4>
  </div>
  <div id="dipchart" style="height: 300px !important;overflow-y: scroll;"></div>

</div>
<div id="tankdisplay">
  <div class="modal-header">
    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span id="tankdelete">×</span></div>
    <h4 class="modal-title" >Nozels No.<span id="tankde"></span> Details</h4> <span id="tankde"></span>
  </div>
  <div id="tankchart" style="height: 300px !important;overflow-y: scroll;"></div>

</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script> 
<!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
 --><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

<script  src="js/index.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script>
  $(function () {
  $("#datepickers").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
  $(function () {
  $("#datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
function validateForm() {
    var x = document.forms["myForm"]["Tank_Number"].value;
    var y = parseInt(document.forms["myForm"]["Capacity"].value);

    var opening_date = parseInt(document.forms["myForm"]["Opening_Reading"].value);
    var active_from_date = document.forms["myForm"]["active_from_date"].value;
    if (x == "") {
        alert("Please Enter  Tank_Number !!");
        return false;
    }
    if (y == "") {
        alert("Please Enter Capacity !!");
        return false;
    }
    // if (opening_date == "") {
    //     alert("Please Enter Opening Stock !!");
    //     return false;
    // }
    if (active_from_date == "") {
        alert("Please Enter Active From Date !!");
        return false;
    }
    if(opening_date > y ){
       alert("Wrong Opening Stock  !!");
        return false;
    }
    if(opening_date < active_from_date ){

       alert("Please Enter Correct Opening Date !!");
        return false;
    }
}
 jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });


</script>
<script type="text/javascript">
 jQuery(function(){
     var urldip="{{url('get_discreaption')}}";
     var viewdipchart="{{url('viewdipchart')}}";
     var tankchart="{{url('tankchart')}}";
     var vil={
           init:function(){

             jQuery('.tankchart').on('click',function(){
                 jQuery('#tankde').html(jQuery(this).html());
                 var opt='';
                  var dip=jQuery(this).data('id');
                  jQuery.get(tankchart+'/'+dip,{
                     '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                        
                  },function(data){
                      
                    jQuery('#tankchart').html(data);
                    jQuery('#tankdisplay').css('display','block');
                    
                  });
                  jQuery('#tankdelete').on('click',function(){
              jQuery('#tankdisplay').hide();
             });
             
             });
             vil.selectRo();
             vil.getdiscription();
             vil.viewdipchart();



             jQuery('#dipchartd_dis').on('click',function(){
               vil.viewdipchart();
            });

             jQuery('#dip_charts').on('change',function(){
                vil.getdiscription();
             });
             jQuery('#chartdelete').on('click',function(){
              jQuery('#viewdisplay').hide();
             });
             

             jQuery('#viewchart').on('click',function(){

                 var opt='';
                  var dip=jQuery(this).data('id');
                  jQuery.get(viewdipchart+'/'+dip,{
                     '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                        
                  },function(data){
                      
                    jQuery('#dipchart').html(data);
                    jQuery('#viewdisplay').css('display','block');
                    
                  });
                 
             });
           },
           selectRo:function(){

            jQuery('#Tank_Number').on('blur',function(){
                 var colum1val=jQuery('#sel1').val();
                  udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique',{
                  table:'tbl_tank_master',
                  colum:'Tank_Number',
                  colum2:'RO_code',
                  colum12val:colum1val,
                  unik:udat,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert('Tank Number already exists Enter Other!');
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
           getdiscription:function(){
             var opt='';
            var dip=jQuery('#dip_charts').val();
            jQuery.get(urldip,{
               dip:dip,
               '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                  
            },function(data){
                
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  
                  });

                  jQuery('#dipchartd_dis').html(opt);
                  vil.viewdipchart();
                  //jQuery('.viewchart').data('id',jQuery('#dipchartd_dis').val());
                  

            });
            
            jQuery('#dip_charts').on('change',function(){
                  jQuery('.viewchart').data('id',jQuery(this).val());
            });
             
           },
           viewdipchart:function(){
           
              jQuery('#viewchart').data('id',jQuery('#dip_charts').val());
           },


          
     }
     vil.init();
  });
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>
</body>
</html>