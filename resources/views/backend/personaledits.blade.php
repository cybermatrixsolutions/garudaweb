<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Personnel & Staff  Update</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/persoonel')}}"> Personnel & Staff  </a></li>
        </ul>
      </div>
    </div>
  
   <div class="">
    
    
    </div>
  
  
  
    <div class="row">

     
      
      <div class="col-md-12 col-sm-12">
        <div class="row" >
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
     <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
            <div class="row" style="margin-top:-15px !important;">
          
         <form class="form-horizontal" action="{{url('personal_update')}}/{{$personal_mangement_data->id}}" enctype="multipart/form-data" method="post" autocomplete="off" onsubmit="return confirm('Do you want to Continue?');">
            
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="personal_ro_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
          
                      <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Pump Legal Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" disabled="disabled" name="personal_ro_code" id="sel1">
                              
                                                    @foreach($datas as $ro_master)
                                                        <option  value="{{$ro_master->company_code}}"
                                                                @if($personal_mangement_data->RO_Code == $ro_master->RO_code) selected @endif>{{$ro_master->pump_legal_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div> -->
                        <div class="form-group">
                           <label class="control-label col-sm-3" for="pwd">Personal Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="personl_name" id="" value="{{$personal_mangement_data->Personnel_Name}}" placeholder="Personal Name">
                          </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-3" for="pwd">Gender <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control" required="required" name="gender">
                             <option value="Male"  @if($personal_mangement_data->gender == "Male") selected @endif>Male</option>
                             <option value="Female" @if($personal_mangement_data->gender == "Female") selected @endif>Female</option>
                           </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Role <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control" name="designation" id="designation">
                            @foreach($designation as $desig)
                                <option data-pre="{{$desig->Reporting_To_Designation}}" value="{{$desig->id}}"
                                 @if($personal_mangement_data->Designation == $desig->id) selected @endif>{{$desig->Designation_Name}}</option>
                            @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Reporting To <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control" name="reporting_to" id="reporting_to">
                                                   <option name="company_id" value="0"
                                                                >No One</option>
                                                    @foreach($personal_list as $personnel)
                                                        <option data-pre="{{$personnel->Designation}}" value="{{$personnel->id}}"
                                                                 @if($personal_mangement_data->Reporting_To == $personnel->id) selected @endif>{{$personnel->Personnel_Name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Date of Birth <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            @if($personal_mangement_data->Date_of_Birth=='1970-01-01')
                            <input type="text" id="datepicker" class="form-control "  name="d_o_b" id="" placeholder="Date of Birth">@else
                            <input type="text" class="form-control" id="datepicker" onkeypress="return false;" name="d_o_b" value="{{date('d/m/Y',strtotime($personal_mangement_data->Date_of_Birth))}}"  placeholder="Date of Birth">@endif
                          </div>
                        
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Date of Appointment <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                          
                            <input type="text" class="form-control" id="datepickers" onkeypress="return false;" name="date_of_appointment" value="{{date('d/m/Y',strtotime($personal_mangement_data->Date_of_Appointment))}}"  placeholder="Date of Appointment">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Mobile(+91) <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control number" name="mobile" id="mobile" maxlength="10" value="{{$personal_mangement_data->Mobile}}" placeholder="Mobile">
                          </div>

                        </div> 
                        <div class="form-group">
                          <label class="control-label col-sm-3" >Aadhaar No </label>
                          <div class="col-sm-9">
                            @if($personal_mangement_data->Aadhaar_no!='')
                            <input type="text" autocomplete="false" class="form-control" name="aadhaar"  value="{{$personal_mangement_data->Aadhaar_no}}" placeholder="Aadhaar No" >@else
                            <input type="text" autocomplete="false" class="form-control" name="aadhaar"  value="  " placeholder="Aadhaar No" >@endif
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" >Aadhaar Photo </label>
                          <div class="col-sm-9">
                            <input type="file"  class="form-control" name="aadhaar_img" id="aadhaar_img" placeholder="Aadhaar No" value=""><br>
                            <img src="{{URL::asset('')}}/{{$personal_mangement_data->Aadhaar_img}}" style="width: 250px; height: 150px;">
                           
                          </div>
                        </div>


                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Email/Login ID <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input type="email" disabled class="form-control" data-table="tbl_personnel_master"  data-colum="Email" data-mass="Email Id Already Exist" required name="Email" value="{{$personal_mangement_data->Email}}" placeholder="Email">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Password <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input type="Password" id="myInput" value="" class="form-control" name="password" placeholder="Password"><input type="checkbox" onclick="myFunction()">Show Password
                          </div>
                        </div>
                        <div class="form-group">
            
                          <div class="col-sm-offset-3 col-sm-9">
                           <!-- <button type="submit" class="btn btn-default">Submit</button>-->
              <center><input type="submit" id="success-btn" class="btn btn-primary site-btn" value="Submit" onclick="return gstAndPan()"></center>
                          </div>
                        </div>
           
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>
  
  
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    
  $(function () {
  $("#datepickers").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
  $("#datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        format: 'dd/mm/yyyy'
      
  });
});
   
  function myFunction() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

 
  function  gstAndPan() {
        var mobile = document.getElementById("mobile").value;
        var pattern = /^[0]?[789]\d{9}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
            return true;
        }
        alert("It is not valid mobile number.input 10 digits number!");
        return false;
    }
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
             vil.checkdesignation();

             jQuery('#ToDate').on('blur',function(){
                 vil.CalculateDiff();
             });
               vil.checkdesignation();
              jQuery('#designation').on('change',function(){
                  vil.checkdesignation();
              });
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   //id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique',{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
           CalculateDiff:function() {
 
                    if(jQuery("#FromDate").val()!="" && jQuery("#ToDate").val()!=""){
                       
                      /*var From_date = new Date(jQuery("#FromDate").val());
                      var To_date = new Date(jQuery("#ToDate").val());
                      var diff_date =  To_date - From_date;
                       
                      var years = Math.floor(diff_date/31536000000);
                      var months = Math.floor((diff_date % 31536000000)/2628000000);
                      var days = Math.floor(((diff_date % 31536000000) % 2628000000)/86400000);
                      
                        if(months<18){
                           alert('Age is not Less than 18 Years');
                        }*/

                      }
                      else{
                          alert("Please select dates");
                          return false;
                      }
          },
          checkdesignation:function(){
           
              var pre=jQuery('#designation  option:selected').data('pre');
              console.log(pre);
              var d=0;
              jQuery('#reporting_to').children('option').each(function(i,v){
                      console.log(jQuery(this).data('pre'));
                      if(jQuery(this).data('pre')!=pre){
                          jQuery(this).hide();
                      }else{
                         if(d==0){
                            d=jQuery(this).val();
                         }
                         jQuery(this).show();
                      }
              });
               
              //jQuery('#reporting_to option[value="0"').show();
              jQuery('#reporting_to option[value="'+d+'"').prop('selected', 'selected').change();
           },
          
     }
     vil.init();
  });


</script>
<script type="text/javascript">
    $(document).ready(function(){
       

             $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
    });

 
</script>
</body>
</html>