@extends('backend-includes.app')

@section('content')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Tank Inward </h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{URL('tankRiding')}}">Tank Inward </a></li>
        </ul>
      </div>

    </div>
    <div class="">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" >Tank Inward</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                     <form class="form-horizontal"  action="{{route('tankInward')}}/store" enctype="multipart/form-data" method="post">
                          {{ csrf_field() }}
                       <input type="hidden" name="Ro_code" id="Ro_code"  value="{{$rocode}}">

                         <div class="form-group">
                          <label class="control-label col-sm-4" for="Invoice Date">Inward Date</label>
                          <div class="col-sm-8">
                             <input type="text" name="inwarddate" id="inwarddate" class="form-control"  required>
                           </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="Invoice No.">Invoice No.</label>
                          <div class="col-sm-8">
                             <input type="text" name="invoiceno"  id="invoiceno" class="form-control"  required>
                           </div>
                        </div>
                          

                        <div class="form-group">
                          <label class="control-label col-sm-4" for="Invoice Date">Invoice Date</label>
                          <div class="col-sm-8">
                             <input type="text" name="invoicedate" id="invoicedate" class="form-control"  required>
                           </div>
                        </div>

                        
                        <div class="form-group">
                          <label class="control-label col-sm-4" > Fuel Type </label>
                           <div class="col-sm-8">
                            <select name="fuel_type" id="fuel_type" class="form-control" required>

                            @foreach($fuel_types as $fuel_type)
                                <option value="{{$fuel_type->id}}">{{$fuel_type->Item_Name}}</option>
                              @endforeach

                            </select>
                            </div>
                        </div>

                         <div class="form-group">
                          <label class="control-label col-sm-4" for="volume">Volume</label>
                          <div class="col-sm-8">
                             <input type="text" name="volume" value="0" id="volume" class="form-control sumvalue numeriDesimal tankvaluem"  required>
                           </div>
                        </div>
                        <hr>
                        <div id="addTank">
                        </div>

                        <div class="form-group">
                         
                          <div class="col-sm-offset-4 col-sm-8">
                            <input  type="submit" class="btn btn-default submit-button" id="submit" disabled  value="Submit">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--end add box-->
    <!--list contant-->
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>S.No.</th>
                 <th>Inward Date</th>
                
                 
                 <th>Invoice No.</th>
                 <th>Invoice Date</th>
                  <th>Fuel Type</th>
                 <th style="text-align: center;">Inward Volume</th>
                 <th><span class="left">Tank No.</span> <span class="right">Tank value</span></th>
                 <!-- <th>Actions</th> -->
            </thead>
            <tbody>
              <?php $i=0; 

              ?>
      
              @foreach($TankInwards as $TankReading) 
      
              <?php $i++ ;?>
              <tr>
                  <td>{{$i}}</td>
                 <!--  <td>@if($TankReading->shift!=null) {{date('d/m/Y h:sa',strtotime($TankReading->shift->created_at))}}  @endif</td> -->
                 

                    <td>{{date('d/m/Y',strtotime($TankReading->inward_date))}}</td>
<td>{{$TankReading->invoice_no}}</td>
                  <td>{{date('d/m/Y',strtotime($TankReading->invoice_date))}}</td>
                  
                   <td>@if($TankReading->fuel!=null){{$TankReading->fuel->Item_Name}}@endif</td>
                  <td style="text-align: right;">{{bcadd($TankReading->value, 0, 3)}}</td>
                  <td>@if($TankReading->tanks!=null) <ul style="background: #ededf5;"> @foreach($TankReading->tanks as $tank) <li ><span class="left">{{$tank->Tank_Number}} </span><span class="right"> {{bcadd($tank->pivot->value, 0, 3)}}</span></li> @endforeach </ul>  @endif</td>
                 
                
                  <!-- <td>
                    <div class="btn-group">

                        <button type="button" class="btn btn-danger">Action</button>
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> 
                        <span class="sr-only">Toggle Dropdown</span> </button>
                        
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="dipedit/edit/{{$TankReading->id}}" data-fuel="$VehicleModels->fuel_type">Edit</a></li>
                        </ul>
                    
                    </div>
                  </td> -->

              </tr>
               @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>


<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

  jQuery(function(){

        var tankInward={
                 init:function(){
                        tankInward.getTank();
                        tankInward.checkFuelQuantity();
                        jQuery('#fuel_type').on('change',function(){
                           tankInward.getTank();
                        });
   
                        jQuery("#invoicedate").datepicker({ 
                                autoclose: true, 
                                todayHighlight: true,
                                 endDate: new Date(),
                                defaultDate:'dd/mm/yyy',
                                format: 'dd/mm/yyyy',
                          });

                        jQuery("#inwarddate").datepicker({ 
                                autoclose: true, 
                                todayHighlight: true,
                                 endDate: new Date(),
                                defaultDate:'dd/mm/yyy',
                                format: 'dd/mm/yyyy',
                          });

                        jQuery(document).on('keyup',".numeriDesimal",function() {
                              var $this = jQuery(this);
                              $this.val($this.val().replace(/[^\d.]/g, ''));
                        });
                      },
                  getTank:function(){

                      var item=jQuery('#fuel_type').val();
                      var RO_code=jQuery('#Ro_code').val();
                      jQuery.get('get_tank_by_ajex/'+item,{
                        RO_code:RO_code,
                         '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                      },function(data){
                        var tank='';
                         
                         jQuery.each(data,function(i,v){
                         
                            tank+='<div class="form-group">';
                                     tank+='<label class="control-label col-sm-4" for="Reading">'+v+'</label>';
                                      tank+='<div class="col-sm-8">';
                                           tank+='<input type="text" name="tankvaluem[]" class="form-control tankvaluem numeriDesimal">';
                                           tank+='<input type="hidden" name="tanname[]" class="form-control" value="'+i+'">';
                                      tank+='</div></div>';
                              });

                         jQuery('#addTank').html(tank);
                      });
                  },
                  checkFuelQuantity:function(){
                    jQuery(document).on('keyup','.tankvaluem',function(){

                          var value=parseFloat(jQuery('#volume').val());
                          var tankValue=0;
                          jQuery('#addTank').find('.tankvaluem').each(function(){
                           
                           if(jQuery(this).not('.sumvalue')){

                              if(!isNaN(parseFloat(jQuery(this).val())))
                                 tankValue=parseFloat(jQuery(this).val())+tankValue;

                           }
                              });

                          if(tankValue==value && tankValue!=0){
                            jQuery('#submit').removeAttr('disabled');
                          }else{

                            jQuery('#submit').attr('disabled',true);
                          }
                     });
                  },
            }
     tankInward.init();

  });

</script>


@endsection