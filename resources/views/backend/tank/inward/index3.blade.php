@extends('backend-includes.app')

@section('content')

<style type="text/css">
  
  .form-horizontal .form-group {
    margin: 8px 0px !important;

}

   



</style>
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Tank Inward </h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{URL('tankRiding')}}">Tank Inward </a></li>
        </ul>
      </div>

    </div>
    <div class="">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"  ><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="height:80%">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" >Tank Inward</h4>
                  </div>
                  <div class="modal-body" >
                    <div class="row" >
                    <form class="form-horizontal"  action="{{route('tankInward')}}/store" enctype="multipart/form-data" method="post">
                      <!--  <form class="form-horizontal" id="id-form-tankinward"> -->

                          {{ csrf_field() }}
                       <input type="hidden" name="Ro_code" id="Ro_code"  value="{{$rocode}}">

                         <div class="form-group">
                          <label class="control-label col-sm-4" for="Invoice Date">Inward Date</label>
                          <div class="col-sm-8">
                             <input type="text" name="inwarddate" id="inwarddate" class="form-control"  required>
                           </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="Invoice No.">Invoice No.</label>
                          <div class="col-sm-8">
                             <input type="text" name="invoiceno"  id="invoiceno" class="form-control"  required>
                           </div>
                        </div>
                          

                        <div class="form-group">
                          <label class="control-label col-sm-4" for="Invoice Date">Invoice Date</label>
                          <div class="col-sm-8">
                             <input type="text" name="invoicedate" id="invoicedate" class="form-control"  required>
                           </div>
                        </div>



                        <div class="form-group">
                          <label class="control-label col-sm-4" > Fuel Type </label>
                           <div class="col-sm-8">
                            <select name="fuel_type" id="fuel_type" class="form-control" required >

                            @foreach($fuel_types as $fuel_type)
                                <option value="{{$fuel_type->id}}" class="fuel" value="{{$fuel_type->Item_Name}}">{{$fuel_type->Item_Name}}</option>
                              @endforeach

                            </select>
                            </div>
                        </div>                         
                       
                        <div id="addTank" style="overflow-y:scroll; height:200px; line-height: 0.428571 !important;">
                        
                        </div>

                        <div class="form-group">
                         
                          <div class="col-sm-offset-4 col-sm-8">
                            <input  type="submit" class="btn btn-default submit-button" id="id-submit-tankinward" disabled  value="Submit">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--end add box-->
    <!--list contant-->
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>S.No.</th>
                 <th>Inward Date</th>
                
                 
                 <th>Invoice No.</th>
                 <th>Invoice Date</th>
                  <th>Fuel Type</th>
           
                  
                 <th style="text-align: center;">Inward Volume</th>
                 <th><span class="left">Tank No.</span> <span class="right">Tank value</span></th>
                        <th>Action</th>
              <!--    <th>Actions</th> -->
            </thead>
            <tbody>
              <?php $i=0; 

              ?>
      
              @foreach($TankInwards as $TankReading) 

              
              <?php $i++ ;?>
              <tr>
                  <td>{{$i}}</td>
                 <!--  <td>@if($TankReading->shift!=null) {{date('d/m/Y h:sa',strtotime($TankReading->shift->created_at))}}  @endif</td> -->
                 

                    <td>{{date('d/m/Y',strtotime($TankReading->inward_date))}}</td>
             <td>{{$TankReading->invoice_no}}</td>
                  <td>{{date('d/m/Y',strtotime($TankReading->invoice_date))}}</td>
                  
                   <td>@if($TankReading->fuel!=null){{$TankReading->fuel->Item_Name}}@endif</td>
                  <td style="text-align: right;">{{bcadd($TankReading->value, 0, 3)}}</td>
                  <td>@if($TankReading->tanks!=null) <ul style="background: #ededf5;"> @foreach($TankReading->tanks as $tank) <li ><span class="left">{{$tank->Tank_Number}} </span><span class="right"> {{bcadd($tank->pivot->value, 0, 3)}}</span></li> @endforeach </ul>  @endif</td>
                 
                
                <td>
                    <div class="btn-group">

                       
                        <button type="button " class="btn btn-danger EditTankInward dropdown-toggle" data-toggle="modal" data-target="#EditTankInward" data-tankinwardid="{{$TankReading->id}}" data-inward_date="{{date('d/m/Y',strtotime($TankReading->inward_date))}}" data-invoice_date="{{date('d/m/Y',strtotime($TankReading->invoice_date))}}" data-fuel_type="{{$TankReading->fuel_type}}" data-inwardvolme="{{$TankReading->value}}" data-tanknumber="{{$tank->Tank_Number}}" data-invoice_no="{{$TankReading->invoice_no}}"> <i class="fa fa-edit"></i> 
                       

                      <!--   <ul class="dropdown-menu" role="menu">
                          <li><a href="dipedit/edit/{{$TankReading->id}}" data-fuel="$VehicleModels->fuel_type">Edit</a></li>
                        </ul> -->
                    
                    </div>
                  </td>

              </tr>
               @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

 <!-- Edit Tank Inward Modal -->
            <div class="modal fade  " id="EditTankInward" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" >Edit Tank Inward</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                    <form class="form-horizontal"  action="{{route('tankInward')}}/edit" enctype="multipart/form-data" method="post" >
                      <!--  <form class="form-horizontal" id="id-form-tankinward"> -->

                          {{ csrf_field() }}
                       <input type="hidden" name="Ro_code" id="Ro_Code"  value="{{$rocode}}">
                        <input type="hidden" name="tankInwardId" id="id-tankInwardId">




                         <div class="form-group">
                          <label class="control-label col-sm-4" for="Invoice Date">Inward Date</label>
                          <div class="col-sm-8">
                             <input type="text" name="inwarddate" id="inwarddate" class="form-control"  required>
                           </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="Invoice No.">Invoice No.</label>
                          <div class="col-sm-8">
                             <input type="text" name="invoiceno"  id="invoiceno" class="form-control"  required>
                           </div>
                        </div>
                          

                        <div class="form-group">
                          <label class="control-label col-sm-4" for="Invoice Date">Invoice Date</label>
                          <div class="col-sm-8">
                             <input type="text" name="invoicedate" id="invoicedate" class="form-control"  required>
                           </div>
                        </div>



                        <div class="form-group">
                          <label class="control-label col-sm-4" > Fuel Type </label>
                           <div class="col-sm-8">
                            <select name="fuel_type" id="#id-fuel_type-tankinward" class="form-control" required >

                            @foreach($fuel_types as $fuel_type)
                                <option value="{{$fuel_type->id}}" class="fuel" value="{{$fuel_type->Item_Name}}">{{$fuel_type->Item_Name}}</option>
                              @endforeach

                            </select>
                            </div>
                        </div>  


                          <div class="form-group">
                          <label class="control-label col-sm-4" for="volume">Volume</label>
                          <div class="col-sm-8">
                             <input type="text" name="volume"  id="inwardVolme" class="form-control sumvalue numeriDesimal tankvaluem"  required>
                           </div>
                        </div>                        
                       
                        <div id="EditTank">
                        
                        </div>

                        <div class="form-group">
                         
                          <div class="col-sm-offset-4 col-sm-8">
                            <input  type="submit" class="btn btn-default submit-button" id="id-edit-tankinward"  value="Submit">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>





@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>


<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

  jQuery(function(){
    jQuery('#id-submit-tankinward').attr('disabled',true);

        var tankInward={


                 init:function(){
                        tankInward.getTank();
                        tankInward.checkFuelQuantity();
                        

                        jQuery('#fuel_type').on('change',function(){
                           tankInward.getTank();
                        });

                         jQuery(document).on('keyup','.volume',function(){
                       

                          tankInward.checkFuelQuantity();

                         });

                         jQuery(document).on('keyup','.tankvaluem',function(){

                        

                          tankInward.checkFuelQuantity();

                         });
   
                        jQuery("#invoicedate").datepicker({ 
                                autoclose: true, 
                                todayHighlight: true,
                                 endDate: new Date(),
                                defaultDate:'dd/mm/yyy',
                                format: 'dd/mm/yyyy',
                          });

                        jQuery("#inwarddate").datepicker({ 
                                autoclose: true, 
                                todayHighlight: true,
                                 endDate: new Date(),
                                defaultDate:'dd/mm/yyy',
                                format: 'dd/mm/yyyy',
                          });

                        jQuery(document).on('keyup',".numeriDesimal",function() {
                              var $this = jQuery(this);
                              $this.val($this.val().replace(/[^\d.]/g, ''));
                        });
                      },
                  getTank:function(){

                      var item=jQuery('#fuel_type').val();

                      if(item){
                      


                      if ( $("#"+item+"").length) { 
                              
                      $("#"+item+"").remove();
                      $("#remove_tank").remove();

                            }

                      }

                      var Item_Name=$('#fuel_type option[value='+item+']').text();
                 

                      var RO_code=jQuery('#Ro_code').val();
                      jQuery.get('get_tank_by_ajex/'+item,{
                        RO_code:RO_code,
                         '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                      },function(data){
                        console.log(data);
                        var tank='';


                                  

                                    tank+='<div style="border:solid grey 1px">';
                                     
                                    tank+='<fieldset id="'+item+'"  value="'+Item_Name+'">';
                                      tank+='<span    type="text" id="remove_tank" style="float:right;" value="'+item+'" attvalue="'+item+'"> <i class="fa fa-times" aria-hidden="true"></i> </span>';
                                    tank+='<legend style="background:#edeff2;font-size:14px;text-align:center;padding-top:10px;font-weight: bold;"  class="legend">'+Item_Name+'</legend>';
                                    tank+='<div class="form-group">';

                                    tank+='<label class="control-label col-sm-4" for="Reading">Volume </label>';
                                    tank+='<div class="col-sm-8">';


                                    tank+= '<input  type="text" style="" class="form-control volume" name="volume_'+item+'" value="0" id="volume"  required>'; 
                                     tank+= '<input  type="hidden" name="fuel_id" class="form-control" value="'+item+'"id="fuel_type"  required>';                                      
                                    tank+='</div>';
                                    tank+='</div>';    



                         jQuery.each(data,function(i,v){
                                          console.log('inner data'+ i);
                                          console.log('inner data v'+ v);
                                                    
                                          tank+='<div class="form-group">';
                                          tank+='<br><label class="control-label col-sm-4" for="Reading">'+v+'</label>';
                                          tank+='<div class="col-sm-8">';
                                          tank+='<input type="text" name="tankvolume_'+item+'[]" class="form-control tankvaluem numeriDesimal">';
                                          tank+='<input type="hidden" name="tankname_'+item+'[]" class="form-control" value="'+i+'">';
                                          tank+='</div>';
                                          tank+='</div>';
                                         
                                        });

                                        tank+='</fieldset>';

                                       tank+='<div>';   

                                  
                                   
                                         jQuery('#addTank').append(tank);
                       
       
                               });
    
                  },
                  checkFuelQuantity:function(){
                    

                          // var value=parseFloat(jQuery('#volume').val());
                         // alert(value);
                          var value=0;
                          var tankValue=0;
                            jQuery('#addTank').find('.volume').each(function(){
                           
                           if(jQuery(this).not('.sumvaluee')){

                              if(!isNaN(parseFloat(jQuery(this).val())))
                                 value=parseFloat(jQuery(this).val())+value;
                               //alert(value);

                           }
                              });

                          jQuery('#addTank').find('.tankvaluem').each(function(){
                           
                           if(jQuery(this).not('.sumvalue')){

                              if(!isNaN(parseFloat(jQuery(this).val())))
                                 tankValue=parseFloat(jQuery(this).val())+tankValue;
                               //alert(tankValue);

                           }
                              });


                    if(value>0){


                                              if(tankValue==value){
                                                jQuery('#id-submit-tankinward').removeAttr('disabled');
                                              }else{

                                                jQuery('#id-submit-tankinward').attr('disabled',true);
                                              }


                    }
                     
                  },
                

            }
     tankInward.init();




     jQuery('#id-submit-tankinward').on('click',function(){

                       var inwarddate=$('#inwarddate').val();
                        var invoiceno=$('#invoiceno').val();
                        var invoicedate=$('#invoicedate').val();
                        var myarr=[];

                         var $inputs = $('fieldset');
                       /* $inputs.each(function() {
                                      console.log('call========');
                                      // myarr[this.name] = $(this).val();
                                      myarr.push({'volume':$("#volume").val(),


                                    }) ;*/
                                  /*myarr.push ({

                                   

                                     'fuel_id':$(this."input[name='fuel_id']").val();
                                   });


*/

/*$('fieldset').each(function() {
    alert( this.id );

    var volume=$(this).find('input[id^="volume"]').val();
    alert(volume);
});

                        });

                        console.log(myarr);*/

                       /* var myarr=[];
                          array.push ({
                          'fuel_id': producttable.rows().count() ,
                          'volume': $("tfoot tr:eq(0) th:nth-child(2)").val() ,
                        var tankArr=[];


                          'tank': tankArr,
                         
            
                           });*/
   // alert('invoicedate'+invoicedate);
    return;






  });

     $(document).on("click","#remove_tank",function() {
       var item=$('#remove_tank').val();

       var fuel_type=$(this).find('input[id^="fuel_id"]').val();

       var att=  $(this).attr("attvalue") ;
       
       $("#"+att+"").remove();

       $(this).find("#remove_tank").remove();
      });



   });

 

jQuery(function(){
  var EditTankInWard={ 
    init:function(){

      
      jQuery(document).ready(function(){ 
        jQuery('.EditTankInward').on('click',function(){debugger;

          var tankInwardId=jQuery(this).data('tankinwardid');
          //alert(tankInwardId);
          var inward_date=jQuery(this).data('inward_date');
          var invoiceNo=jQuery(this).data('invoice_no');
          var invoice_date=jQuery(this).data('invoice_date');
          var fuel_type=jQuery(this).data('fuel_type');

          var inwardVolme=jQuery(this).data('inwardvolme');
          var tankNumber=jQuery(this).data('tankNumber');
          var RO_code=jQuery('#Ro_Code').val();   

          jQuery("#id-fuel_type-tankinward").val(fuel_type);
         var a= jQuery('#id-tankInwardId').val(tankInwardId);
         alert(a);
          jQuery('#id-inwarddate').val(inward_date);
          jQuery('#id-invoiceno').val(invoiceNo);
          jQuery('#id-invoicedate').val(invoice_date);
          jQuery('#id-inwardvolme').val(inwardVolme);
          
          EditTankInWard.editTankInward(fuel_type);
          });

      });
      jQuery('#id-fuel_type-tankinward').on('change',function(){
        jQuery('#inwardVolme').val('0');
        EditTankInWard.editTankInward();
      });
      jQuery(document).on('keyup','.tankvolume',function(){
        tankInward.checkFuelQuantity();
      });
      jQuery("#invoicedate").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        endDate: new Date(),
        defaultDate:'dd/mm/yyy',
        format: 'dd/mm/yyyy',
      });
      jQuery("#inwarddate").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        endDate: new Date(),
        defaultDate:'dd/mm/yyy',
        format: 'dd/mm/yyyy',
      });
      jQuery(document).on('keyup',".numeriDesimal",function() {
        var $this = jQuery(this);
        $this.val($this.val().replace(/[^\d.]/g, ''));
      });
    },

    editTankInward:function(fuel_type){
      var fuel_type= $( "#id-fuel_type-tankinward" ).val();
      var RO_code=jQuery('#Ro_Code').val();
      var tankInwardId=jQuery('#id-tankinwardId').val();

      var option="";
      

      jQuery.get('get_tank_by_ajex/'+fuel_type,{
        RO_code:RO_code,
        '_token': jQuery('meta[name="csrf-token"]').attr('content'),
      },function(finaldata){
        console.log(finaldata);
        
        var tank='';
        jQuery.each(finaldata,function(i,v){

          
          tank+='<div class="form-group">';
          tank+='<br><label class="control-label col-sm-4" for="Reading">'+v+'</label>';
          fuel_type
         
          tank+='<div class="col-sm-8">';
          tank+='<input type="text" name=" tankvolume[]"  id="tankvolume_'+i+'"  class="form-control tankvaluem numeriDesimal">';
          tank+='<input type="hidden" name="tankname[]" class="form-control" value="'+i+'">';
          tank+='</div>';
          tank+='</div>';
       
        });  
        tank+='</fieldset>';
        tank+='<div>'; 

        
        jQuery('#EditTank').html(tank);
        //alert('yyy'+tankInwardId);
        jQuery.get('get_tank_value_by_ajax/'+tankInwardId,{
            tankInwardId:tankInwardId,
              '_token': jQuery('meta[name="csrf-token"]').attr('content'),
            },function(data){
              console.log(data);
              var arr=[];
              for(var j=0; j<data.length;j++){
                arr=jQuery('#tankvolume_'+data[j].tank_id).val(data[j].value);
              }
              console.log(option);
     
           });
      });
      
    },
    

  }
  
  EditTankInWard.init();

});


 jQuery(document).on('keyup','.tankvaluem',function(){
        var value=parseFloat(jQuery('#inwardVolme').val());
    
        var tankValue=0;
        jQuery('#EditTank').find('.tankvaluem').each(function(){
          if(jQuery(this).not('.sumvalue')){
            if(!isNaN(parseFloat(jQuery(this).val())))
            tankValue=parseFloat(jQuery(this).val())+tankValue;
         
          }
        });
        if(tankValue==value && tankValue!=0){
          jQuery('#id-edit-tankinward').removeAttr('disabled');
        }else{
          jQuery('#id-edit-tankinward').attr('disabled',true);
        }
      });





              


</script>


@endsection