 @extends('backend-includes.app')

@section('content')
<div class="modal-body">
                    <div class="row">
                     <form class="form-horizontal"  action="{{route('TankReadings')}}" enctype="multipart/form-data" method="post">
                          {{ csrf_field() }}
                       <input type="hidden" name="Ro_code" id="Ro_code"  value="{{$rocode}}">
                        
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="TankReading volume"> Tank Number</label>
                           <div class="col-sm-8">
                            <select name="Tank_code" id="Tank_code" class="form-control">
                              @foreach($TankMasterModels as $TankMasterModel)
                              <option @if($TankMasterModel->getDipchart!=null) data-diptype="{{$TankMasterModel->getDipchart->lenght_type}}" data-dipid="{{$TankMasterModel->getDipchart->id}}" @endif value="{{$TankMasterModel->id}}">{{$TankMasterModel->Tank_Number}}</option>
                              @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="Reading"> Capacity</label>
                          <div class="col-sm-8">
                             <input type="text" readonly name="capacity" id="capacity" class="form-control numeriDesimal"  placeholder="capacity">
                           </div>
                        </div>
                       <div class="form-group">
                          <label class="control-label col-sm-4" for="Reading">Unit of Measure</label>
                          <div class="col-sm-8">
                             <input type="text" readonly  name="unit_measure" id="unit_measures" class="form-control numeriDesimal"  placeholder="reading">
                           </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-4" for="Reading"> Fuel Type</label>
                          <div class="col-sm-8">
                             <input type="text" readonly name="fuel_type" value="" id="fuel_types" class="form-control numeriDesimal"  placeholder="reading">
                           </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-4" for="Reading"> Reading Date</label>
                          <div class="col-sm-8">
                             <input type="text" readonly name="reading_date" value=""  class="form-control datepicker"  placeholder="reading">
                           </div>
                        </div>
                        
                          <input type="hidden"  name="fuel_type" id="fuel_type1">
                          <input type="hidden"  name="capacity" id="capacity">
                          <input type="hidden"  name="unit_measure" id="unit_measure">
                          <div class="form-group">
                          <label class="control-label col-sm-4" for="Reading"> Dip Reading</label>
                          <div class="col-sm-4" id="cmval-new">
                             (CM)<input type="text"  name="reading_cm" id="reading_cm" class="form-control numeriDesimal"  placeholder="CM">
                           </div>
                          <div class="col-sm-4"  id="hash-cm-val">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="Reading">Volume</label>
                          <div class="col-sm-8">
                             <input type="text" readonly name="sumvalue" value="0" id="sumvalue" class="form-control sumvalue"  placeholder="">
                           </div>
                        </div>

                        <div class="form-group">
                          <input type="hidden" name="ditype" id="ditype" value="">
                          <div class="col-sm-offset-4 col-sm-8">
                            <input  type="submit" class="btn btn-default submit-button" id="submit"  value="Submit">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
@endsection
@section('script')

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

   jQuery("#reading").keyup(function() {
      if(jQuery('.sumvalue').val() ==0){
            jQuery('#submit').hide();
        }
          jQuery('#submit').show();
               
          
          
   });

 jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });
  jQuery(function(){
     var url="{{url('gettankcode')}}";
     var slect='(MM) <select class="form-control" id="readingcal" name="reading" ><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option></select>'
       var textselect='(MM) <input type="text"  name="reading" id="reading"  class="form-control numeriDesimal mm_mm_val"  placeholder="MM">';                    
     var vil={
           init:function(){
             vil.selectRo();
             vil.getcom();
             vil.gettank();
             var diptype=jQuery('#Tank_code').children("option").filter(":selected").data('diptype');
             var dipid=jQuery('#Tank_code').children("option").filter(":selected").data('dipid');
              
              if(diptype=='cm'){
                 jQuery('#hash-cm-val').html(slect);
                 jQuery('#cmval-new').show();
              }else{
                jQuery('#hash-cm-val').html(textselect);
                jQuery('#cmval-new').hide();
              }
             jQuery('#ditype').val(diptype);

             jQuery(document).on('keyup','.numeriDesimal',function() {
                        var jQuerythis = jQuery(this);
                       jQuerythis.val(jQuerythis.val().replace(/[^\d]+/g,''));
                    });

           },
           selectRo:function(){
            jQuery('#Ro_code').on('change',function(){
                  vil.getcom();
              });
            jQuery('#Tank_code').on('change',function(){
                  vil.gettank();
                  diptype=jQuery('#Tank_code').children("option").filter(":selected").data('diptype');
                  dipid=jQuery('#Tank_code').children("option").filter(":selected").data('dipid');

                    if(diptype=='cm'){
                       jQuery('#hash-cm-val').html(slect);
                       jQuery('#cmval-new').show();
                    }else{
                      jQuery('#hash-cm-val').html(textselect);
                      jQuery('#cmval-new').hide();
                    }
                     jQuery('#ditype').val(diptype);
              });
             jQuery(document).on('change','#readingcal',function(){
                  cm=jQuery('#reading_cm').val();
                  mm=jQuery(this).val();
                  vil.getDipCal(cm,mm);
              });

             jQuery(document).on('keyup','#reading',function(){
                  //vil.gettank();
                  cm=jQuery('#reading_cm').val();
                  mm=jQuery(this).val();
                  vil.getDipCal(cm,mm);
              });

             jQuery('#reading_cm').on('keyup',function(){
                  cm=jQuery(this).val();
                  mm=jQuery('#readingcal').val();
                  vil.getDipCal(cm,mm);
                vil.getDipCal(cm,mm);
             });
           
           },
           getDipCal:function(cm,mm){
                diptype=jQuery('#Tank_code').children("option").filter(":selected").data('diptype');
                dipid=jQuery('#Tank_code').children("option").filter(":selected").data('dipid');
                jQuery('#ditype').val(diptype);
               jQuery.get('gettankdipchartcalculation',{
                cm:cm,
                mm:mm,
                diptype:diptype,
                dipid:dipid,
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                   if(data==0){
                    jQuery('#submit').attr('disabled',true);
                   }else{
                    jQuery('#submit').attr('disabled',false);
                   }
                   jQuery('#sumvalue').val(data);
               });
           },
           gettank:function(){
            jQuery.get('gettankcapacity',{
                
                Tank_code:jQuery('#Tank_code').val(),
                reading:jQuery('.readingcal').val(),

               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                console.log(data);
                jQuery('#capacity').val(data['capacity']);
                jQuery('#fuel_type1').val(data['fueltype']);
                jQuery('#unit_measure').val(data['unit']);
                jQuery('#fuel_types').val(data['Item_Name']);
                jQuery('#unit_measures').val(data['Unit_Name']);
                jQuery('#sumvalue').val(data['sum']);

               });
               

           },
           getcom:function(){
            var rocode=jQuery('#Ro_code').val();
               jQuery.get(url+'/'+rocode,{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   //jQuery('#Tank_code').html(opt);
                    //vil.gettank();
                   
               });
           },
           

     }
     vil.init();
  });
</script>


@endsection