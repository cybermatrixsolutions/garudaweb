@extends('backend-includes.app')

@section('content')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Dip Reading </h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Dip Reading </a></li>
        </ul>
      </div>

    </div>
    <div class="">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" >Dip Reading</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                     <form class="form-horizontal"  action="{{route('TankReadings')}}" enctype="multipart/form-data" method="post">
                          {{ csrf_field() }}
                          <input type="hidden" name="Ro_code" id="Ro_code"  value="{{Auth::user()->getRocode->RO_code}}">
                         <!--  <div class="form-group">
                            <label class="control-label col-sm-4" for="Type">Pump Legal Name</label>
                            <div class="col-sm-8">
                            <select name="Ro_code" id="Ro_code" class="form-control">
                            	@foreach($RoMasters as $RoMaster)
                              		<option value="{{$RoMaster->RO_code}}"> {{$RoMaster->pump_legal_name}}</option>
                                @endforeach
                            </select>
                            </div>
                         </div>
 -->                    
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="TankReading volume"> Tank Number</label>
                           <div class="col-sm-8">
                            <select name="Tank_code" id="Tank_code" class="form-control">
                              
                            </select>
                            </div>
                        </div>

                          <input type="hidden"  name="fuel_type" id="fuel_type1">
                          <input type="hidden"  name="capacity" id="capacity">
                          <input type="hidden"  name="unit_measure" id="unit_measure">
                          <div class="form-group">
                          <label class="control-label col-sm-4" for="Reading"> Dip Reading</label>
                          <div class="col-sm-8">
                          	 <input type="text"  name="reading" class="form-control numeriDesimal"  placeholder="reading">
                           </div>
                        </div>
                        
                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <input  type="submit" class="btn btn-default submit-button"  value="Submit">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--end add box-->
    <!--list contant-->
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>S.No.</th>
                 <th>Pump Legal Name</th>
                 <th>Tank Number</th>
                 <th>Dip Reading</th>
                 <th>Capacity</th>
                 <th>Fuel Type</th>
                 <th>Unit of Measure</th>
                 <th>Volume</th>
                 <th>Date</th>
                
                 <!-- <th>Actions</th> -->
            </thead>
            <tbody>
              <?php $i=0; ?>
      
              @foreach($TankReadings as $TankReading) 
      
              <?php $i++ ;?>
               <tr>
                <td>{{$i}}</td>

                <td>
                  @if($TankReading->getRo!= null)
                  {{$TankReading->getRo->pump_legal_name}}
                  @endif
                </td>
                <td>
                   @if($TankReading->getTank!=  null)
                  {{$TankReading->getTank->Tank_Number}}

                  @endif
                </td>
                <td>{{$TankReading->Reading}}</td>
                <td>{{$TankReading->getcapacity->Capacity}}</td>
                <td>{{$TankReading->fuel_type}}</td>
                <td>{{$TankReading->getunitmesure}}</td>
                 <td>{{$TankReading->value}}</td>
                <td>{{date('d/m/Y',strtotime($TankReading->created_at))}}</td>
              </tr>
               @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
 jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });
  jQuery(function(){
     var url="{{url('gettankcode')}}";
     var vil={
           init:function(){
             vil.selectRo();
             vil.getcom();
             vil.gettank();

           },
           selectRo:function(){
            jQuery('#Ro_code').on('change',function(){
                  vil.getcom();
              });
            jQuery('#Tank_code').on('change',function(){
                  vil.gettank();
              });
           
           },
           gettank:function(){
            jQuery.get('gettankcapacity',{
                
                Tank_code:jQuery('#Tank_code').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                console.log(data);
                jQuery('#capacity').val(data['capacity']);
                 jQuery('#fuel_type1').val(data['fueltype']);
                 jQuery('#unit_measure').val(data['unit']);
               });

           },
           getcom:function(){
           	var rocode=jQuery('#Ro_code').val();
               jQuery.get(url+'/'+rocode,{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#Tank_code').html(opt);
                   
               });
           },
           

     }
     vil.init();
  });
</script


@endsection