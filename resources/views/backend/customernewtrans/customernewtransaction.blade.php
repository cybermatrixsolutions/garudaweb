<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">

<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">

<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
   @include('backend-includes.header')

        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Customer New Transaction </h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Customer New Transaction </a></li>
        </ul>
      </div>
    </div>
    
    
                    <div class="row">
                      <br/>
                      <br/>
                      <br/>
                       <div class="">
                         @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                       @endif
                          
                           <center>
                            @if(Session::has('success'))
                               <font style="color:red">{!!session('success')!!}</font>
                            @endif
                          </center>

                        </div>              
                      <form class="form-horizontal" action="{{'save_slip_detail_data'}}" method="post">
                          {{ csrf_field() }}
                           <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Customer Name</label>
                          <div class="col-sm-9">
                             <select class="form-control" style="width: 37%;" required="required"  name="customer_code" id="customer_code">
                              
                                                   
                            </select>
                          </div>
                          </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Vehicle Reg No </label>
                          <div class="col-sm-9">
                            <select class="form-control" style="width: 37%;" required="required" name="Vehicle_Reg_No" id="registration_number">
                              
                                                   
                            </select>
                          </div>
                        </div>
                          
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default" id="tbnhide">Submit</button>
                          </div>
                        </div>
                         <div class="form-group" style="visibility: hidden;">
                          <label class="control-label col-sm-3" for="email">Pump Legal Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="RO_code" id="RO_code">
                              @foreach($datas as $ro_master)
                                  <option  value="{{$ro_master->RO_code}}">{{$ro_master->pump_legal_name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </form>
                </div>
                 <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>S.No</th>
                <th>Vehicle Reg No</th>
                <th>Item Name</th>
                 <th>Request Type</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
      <?php $i=0; ?>
      
              
        
           <?php $i++ ;?>
      
            <tr>
          <td>{{$i}}</td>
                <td scope="row"></td>
                <td></td>
                <td></td>
                <td></td>
                <td><button type="button" class="btn btn-success">Procced</button>
                  <!-- div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                    
                    </ul>
                    </div> -->
                </td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>

</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="{{URL::asset('js/index.js')}}"></script> 
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('#myTable').DataTable();
});
</script>
<script>
jQuery('.table-responsive').on('show.bs.dropdown', function () {
     jQuery('.table-responsive').css( "overflow", "inherit" );
});

jQuery('.table-responsive').on('hide.bs.dropdown', function () {
     jQuery('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">
 jQuery(function(){
      var capacitys =0;
      var capacity ;
      var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
              vil.reqType();
              console.log('hello');

           },
           selectRo:function(){
            jQuery('#RO_code').on('change',function(){
                    console.log('hello');
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
             jQuery('#registration_number').on('change',function(){
                  vil.getcapcity();
              });

             jQuery('#petroldiesel_qty').on('keyup',function(){
                 
              vil.getcpcity();

             });
              jQuery('#petroldiesel_type').on('change',function(){
                 
                var petroldieselty = jQuery('#petroldiesel_type').val();
                 if ('Rs'==petroldieselty) {


                 } 

                 else{
                  jQuery('#petroldiesel_qty').val("");
                 }
              vil.getcpcity();

             });

            
           },
           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#RO_code').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
                   vil.getcapcity();
                 vil.selectFuleType();
               });
           },
           getcapcity:function(){
            
             jQuery.get('getcapcitys',{
                registration_number:jQuery('#registration_number').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
             
                 
                 capacitys = parseInt(data);
                   
                jQuery('#petroldiesel_qtys').val(data);
              
                 vil.selectFuleType();
               });
           },
          
         getcpcity:function(){
        var rcapacity=parseInt(jQuery('#petroldiesel_qty').val());
        var petroldiesel_type=jQuery('#petroldiesel_type').val();   
             if('Rs'!=petroldiesel_type){
                  
                  
                    if(rcapacity>=capacitys){
                       jQuery('#tbnhide').hide();
                      alert("Please Enter Below Capacity Limit ");
                    }
                    else{
                              jQuery('#tbnhide').show();
                            }
                  }
                  else{
                     
                    jQuery('#tbnhide').show();
                  }
               },
           selectFuleType:function(){
            var registration_number=jQuery('#registration_number').val();
             jQuery.get('getVihicleFuleType/'+registration_number,{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                //jQuery('#fuel select').val(data);
                jQuery('#fuel option[value='+data+']').attr('selected','selected');
               });
           },

           reqType:function(){
               jQuery('#Request_Value').hide();
              jQuery('#request_type').on('change',function(){
                    var t=jQuery(this).val();
                    if(t=='Full Tank'){
                      jQuery('#Request_Value').hide();
                      jQuery('#Request_Value_input').attr('required',false);
                    }
                    else{
                      jQuery('#Request_Value').show();
                      jQuery('#Request_Value_input').attr('required',true);

                    }

              });
           },

     }
     vil.init();
  });
</script>
</body>
</html>