<!DOCTYPE html>
 <html lang="en">
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
   @include('backend-includes.header')

        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Customer Transaction</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Customer Transaction</a></li>
        </ul>
      </div>
    </div>
    
    
                    <div class="row">
                      <br/>
                      <br/>
                      <br/>
                       <div class="">
                         @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                       @endif
                          
                           <center>
                            @if(Session::has('success'))
                               <font style="color:red">{!!session('success')!!}</font>
                            @endif
                          </center>

                        </div>
                                         
                      <form class="form-horizontal" action="{{'othersotetrans'}}" method="post">
                          {{ csrf_field() }}  
                            <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Sales Man</label>
                          <div class="col-sm-6">
                           <select class="form-control" id="Pedestal_id" required="required" name="salesman">
                            @foreach($pedestals as $fuel) 
                              <option value="{{$fuel->id}}">{{$fuel->Personnel_Name}}</option>
                               @endforeach
                            </select>
                          </div>
                        </div>
                            @foreach($petroldiesel_qty as $key => $petroldiesel_qtys)
                           <input type="hidden" readonly class="form-control" value="{{$petroldiesel_qtys}}"  required="required" name="{{ $key}}">
                           @endforeach                 
                             <input type="hidden" class="form-control"   name="mobile" value="{{$driver_reuqest_mobile}}" id="mobile">
                         <div class="form-group" >
                          <label class="control-label col-sm-3" for="Request Value">OTP</label>
                          <div class="col-sm-4">
                          <input type="text" class="form-control"  required="required" name="otp" id="otpsubmit"><br>
                          </div>
                          <div class="col-sm-4">
                            <a href="#" class="btn btn-default" id="sendotp">Re-send OTP</a>
                          </div>
                          </div>
                          <div class="form-group">

                          <div class="col-sm-offset-3 col-sm-4">
                           
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default" id="tbnhide">Submit</button>
                          </div>
                        </div>
                        <div class="form-group" style="visibility: hidden;">
                          <label class="control-label col-sm-3" for="email">Vehicle Reg No </label>
                          <div class="col-sm-9">
                            <input type="hidden" name="customer_code" value="{{$customer_code}}">
                              <input type="hidden" name="request_id" id="request_id" value="{{$request_id}}">
                            <input type="text" class="form-control" readonly required="required"   name="Vehicle_Reg_No" id="petroldiesel_qty" placeholder="Dispense" Value ="{{$Vehicle_Reg_No}}">
                        </div>
                        </div>
                         <div class="form-group" style="visibility: hidden;">
                          <label class="control-label col-sm-3" for="email">Select Item</label>
                          <div class="col-sm-9">
                            <select class="form-control multiselect-ui" multiple="multiple" required="required" name="item_code[]" id="tags">
               
                                      @foreach($item_id as $item)
                                          <option value="{{$item->id}}" @if(array_key_exists($item->id,$itenmName))selected @endif>
                                                 {{$item->Item_Name}}
                                                </option>
                                      @endforeach
                            </select>
                          </div>
                        </div>
                      </form>
   
 </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="{{URL::asset('js/index.js')}}"></script> 
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  /*jQuery(document).ready(function(){
    jQuery('#myTable').DataTable();
});*/
</script>
<script>
jQuery('.table-responsive').on('show.bs.dropdown', function () {
     jQuery('.table-responsive').css( "overflow", "inherit" );
});

jQuery('.table-responsive').on('hide.bs.dropdown', function () {
     jQuery('.table-responsive').css( "overflow", "auto" );
})
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
 $('#tags').select2({
  
    tags: true,
    tokenSeparators: [','], 
    placeholder: "Add your tags here"
});


</script>
<script type="text/javascript">
 jQuery(function(){
      var capacitys =0;
      var capacity ;
      var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
              vil.reqType();
              console.log('hello');

           },
           selectRo:function(){
            jQuery('#RO_code').on('change',function(){
                    console.log('hello');
                  vil.getcom();
              });
             jQuery('#sendotp').on('click',function(){
                  vil.sendotps();
              });
             jQuery('#verifity').on('click',function(){
                  vil.verifity();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
             jQuery('#registration_number').on('change',function(){
                  vil.getcapcity();
              });

             jQuery('#petroldiesel_qty').on('keyup',function(){
                 
              vil.getcpcity();

             });
              jQuery('#petroldiesel_type').on('change',function(){
                 
                var petroldieselty = jQuery('#petroldiesel_type').val();
                 if ('Rs'==petroldieselty) {


                 } 

                 else{
                  jQuery('#petroldiesel_qty').val("");
                 }
              vil.getcpcity();

             });

            
           },
           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#RO_code').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
                   vil.getcapcity();
                 vil.selectFuleType();
               });
           },
           getcapcity:function(){
            
             jQuery.get('getcapcitys',{
                registration_number:jQuery('#registration_number').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
             
                 
                 capacitys = parseInt(data);
                   
                jQuery('#petroldiesel_qtys').val(data);
              
                 vil.selectFuleType();
               });
           },
           sendotps:function(){
        

             jQuery.get('sedndotpother',{
                mobile:jQuery('#mobile').val(),
                request_id:jQuery('#request_id').val(),
                
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },

               function(data){
             
                 alert("OTP Send your Mobile Number");
            
                
               });

           },
           verifity:function(){
        

             jQuery.get('verifity',{
                otpsubmit:jQuery('#otpsubmit').val(),
                // request_id:jQuery('#request_id').val(),
                
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },

               function(data){
             
                 alert("Verify Succes Your OTP ");
              
                
               });
                
           },
          
         getcpcity:function(){
        var rcapacity=parseInt(jQuery('#petroldiesel_qty').val());
        var petroldiesel_type=jQuery('#petroldiesel_type').val();   
             if('Rs'!=petroldiesel_type){
                  
                  
                    if(rcapacity>=capacitys){
                       jQuery('#tbnhide').hide();
                      alert("Please Enter Below Capacity Limit ");
                    }
                    else{
                              jQuery('#tbnhide').show();
                            }
                  }
                  else{
                     
                    jQuery('#tbnhide').show();
                  }
               },
           selectFuleType:function(){
            var registration_number=jQuery('#registration_number').val();
             jQuery.get('getVihicleFuleType/'+registration_number,{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                //jQuery('#fuel select').val(data);
                jQuery('#fuel option[value='+data+']').attr('selected','selected');
               });
           },

           reqType:function(){
               jQuery('#Request_Value').hide();
              jQuery('#request_type').on('change',function(){
                    var t=jQuery(this).val();
                    if(t=='Full Tank'){
                      jQuery('#Request_Value').hide();
                      jQuery('#Request_Value_input').attr('required',false);
                    }
                    else{
                      jQuery('#Request_Value').show();
                      jQuery('#Request_Value_input').attr('required',true);

                    }

              });
           },

     }
     vil.init();
  });
</script>
</body>
</html>