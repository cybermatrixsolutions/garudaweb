<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
   @include('backend-includes.header')

        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Customer Request </h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#"> Customer Request </a></li>
        </ul>
      </div>
    </div>
    
    
                    <div class="row">
                      <br/>
                      <br/>
                      <br/>
                       <div class="">
                         @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                       @endif
                          
                           <center>
                            @if(Session::has('success'))
                               <font style="color:red">{!!session('success')!!}</font>
                            @endif
                          </center>

                        </div>              
                      <form class="form-horizontal" action="{{'customernewtransactions'}}" method="post"  onsubmit="return confirm('Do you want to Continue?');">
                          {{ csrf_field() }}
                           <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Customer Name</label>
                          <div class="col-sm-9">


                             <select class="form-control" style="width: 37%;" required="required"  name="customer_code" id="customer_code" data-id="{{$customer_code}}">
                                      
                            </select>
                          </div>
                          </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Vehicle Reg No </label>
                          <div class="col-sm-9">
                            <select data-id="{{$Vehicle_Reg_No}}" class="form-control" style="width: 37%;" required="required" name="Vehicle_Reg_No" id="registration_number">
                              
                                                   
                            </select>
                          </div>
                        </div>
                          
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default" id="tbnhide">Submit</button>
                          </div>
                        </div>
                         <div class="form-group" style="visibility: hidden;">
                          <label class="control-label col-sm-3" for="email">Pump Legal Name</label>
                          <div class="col-sm-9">
                            <select class="form-control" required="required" name="RO_code" id="RO_code">
                              @foreach($datas as $ro_master)
                                  <option  value="{{$ro_master->RO_code}}">{{$ro_master->pump_legal_name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </form>
                </div>
     
        <div class="row">
      <div class="col-md-12">
           <label class="control-label col-sm-3" for="email"><b>Fuel Request</b></label>
        <div class="table-responsive">
         <form class="form-horizontal" action="{{'createnewtransactionsa'}}" method="get">
                          {{ csrf_field() }}
          <table class="table table-striped" id="myTablea">
            <thead>
              <tr>
                <th>S.No</th>
                <th>Vehicle Reg No</th>
                <th>Request Type</th>
                <th>Request Value</th>
                <th>Driver Mobile</th>
                 <th>Request Date</th>
                <th>Request ID 
               
                </th>
                <th><input type="submit" class="btn btn-info"  name="Procced" value="Procced"> </th>
              </tr>
            </thead>
            <tbody>
      <?php $i=0; ?>
      
               @foreach($AddPetrolDieselModel as $AddPetrolDieselModels)
        
           <?php $i++ ;?>
           
            <tr>
          <td>{{$i}}</td>
                 <input type="hidden" name="customercode" value="{{$AddPetrolDieselModels->customer_code}}">
                 <input type="hidden" name="petrolDiesel_Type"  value="{{$AddPetrolDieselModels->PetrolDiesel_Type}}">
                <td><input type="hidden" name="Vehicleno"  value="{{$AddPetrolDieselModels->Vehicle_Reg_No}}">{{$AddPetrolDieselModels->Vehicle_Reg_No}}</td>
                <td><input type="hidden" name="Request_Type"  value="{{$AddPetrolDieselModels->Request_Type}}">{{$AddPetrolDieselModels->Request_Type}}</td>
                <td><input type="hidden" name="Request_Value"  value="{{$AddPetrolDieselModels->Request_Value}}">{{$AddPetrolDieselModels->Request_Value}}</td>
                 <td><input type="hidden" name="current_driver_mobile"  value="{{$AddPetrolDieselModels->current_driver_mobile}}">{{$AddPetrolDieselModels->current_driver_mobile}}</td>
                <td>{{Carbon\Carbon::parse($AddPetrolDieselModels->request_date)->format('d/m/Y')}}</td>
                <td><input type="hidden" name="request_id"  value="{{$AddPetrolDieselModels->request_id}}">{{$AddPetrolDieselModels->request_id}}</td>
                <td></td>
            </tr>
            @endforeach
            </tbody>
          </table>
          </form>
        </div>
      </div>
    </div>
     <div class="row">
        <div class="col-md-12">
           <label class="control-label col-sm-3" for="email"><b>Other Request<b></label>
        <div class="table-responsive">
            <form action="{{url('othertransaction')}}"  method="get">
                 {{ csrf_field() }}
          <table class="table table-striped" id="myTablea">
            <thead>
              <tr>
                <th>S.No</th>
                <th>Select Item Names</th>
                <th>Vehicle Reg No</th>
                <th>Driver Number</th>
                <th>Quantity</th>
                <th>Request Date</th>
                <th>Request ID</th>

               <th><input type="submit" class="btn btn-info" name="Procced" id="hid" value="Procced"> </th>
              </tr>
            </thead>
            <tbody>
      <?php $i=0; ?>
      
              @foreach($CustomerLubeRequest as $CustomerLubeRequests)
        
           <?php $i++ ;?>
      
              <tr>
                <input type="hidden" name="customer_code" value="{{$CustomerLubeRequests->customer_code}}">
                 <input type="hidden" name="request_id" value="{{$CustomerLubeRequests->request_id}}">
            <td>{{$i}}</td>
                   <td><input type="checkbox" name="item_code[]" value="{{$CustomerLubeRequests->item_id}}">{{$CustomerLubeRequests->itemname->Item_Name}}</td>
                   <td><input type="hidden" name="Vehicle_Reg_No" value="{{$CustomerLubeRequests->Vehicle_Reg_No}}">{{$CustomerLubeRequests->Vehicle_Reg_No}}</td>
    <td><input type="hidden" name="driver_reuqest_mobile"  value="{{$CustomerLubeRequests->current_driver_mobile}}">                {{$CustomerLubeRequests->current_driver_mobile}}</td>
                <td><input type="number"  min="0" data-quty="{{$CustomerLubeRequests->quantity}}"  style="text-align:right;width: 95px;" name="name_{{$CustomerLubeRequests->item_id}}" value="{{$CustomerLubeRequests->quantity}}" class="checkval">
                  <!-- <input type="hidden"  min="0"  style="text-align:right;width: 95px;" name="name_{{$CustomerLubeRequests->item_id}}" value="{{$CustomerLubeRequests->quantity}}" class="realval"></td> -->
                   <td>{{Carbon\Carbon::parse($CustomerLubeRequests->request_date)->format('d/m/Y')}}</td>
                   <td><input type="hidden" name="request_id" value="{{$CustomerLubeRequests->request_id}}">{{$CustomerLubeRequests->request_id}}</td>
                   <td></td>
              </tr>
              @endforeach
              </tbody>
          </table>
        </form>
        </div>
      </div>
    </div>

</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="{{URL::asset('js/index.js')}}"></script> 
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('#myTable').DataTable();
});
   function  getmobilenumber() {
        var checkval = getElementsByClassName("checkval");
     
        // if (checkval!=4444) {
        //     alert("Your mobile number : " + checkval);
        //     return true;
        // }
        alert("It is not valid mobile number.input 10 digits number!");
        return false;
    }
</script>
<script>
jQuery('.table-responsive').on('show.bs.dropdown', function () {
     jQuery('.table-responsive').css( "overflow", "inherit" );
});

jQuery('.table-responsive').on('hide.bs.dropdown', function () {
     jQuery('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">
 jQuery(function(){
      var capacitys =0;
      var capacity ;
      var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
              vil.reqType();
             
              console.log('hello');

           },
           selectRo:function(){
            jQuery('#RO_code').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
             jQuery('#registration_number').on('change',function(){
                  vil.getcapcity();
              });

             jQuery('#petroldiesel_qty').on('keyup',function(){
                 
              vil.getcpcity();
              

             });
             jQuery(document).on('click','#hid',function(){
              var flag=true;
                  jQuery('.checkval').each(function(){
                       var checkval=jQuery(this).val();
                       var realval = jQuery(this).data('quty');
                       if(checkval<=realval){
                          jQuery(this).css('border-color','');
                          
                       }else{
                           jQuery(this).css('border-color','red');
                            flag=false;
                       }
                  });

                  if(flag===false)
                    return false;
             });
             jQuery('.checkval').on('change keyup',function(){
                var checkval=jQuery(this).val();
                var realval = jQuery(this).data('quty');
               
               if(checkval<=realval){
                   
                   jQuery('#hid').attr('disabled',false);
                  }
                  else{
                     alert("Please Not More then Old Quantity");
                   jQuery('#hid').attr('disabled',true);
                  }

                  if(checkval<=realval){
                   
                   jQuery('#hid').attr('disabled',false);
                  }
                 
               
            });
              jQuery('#petroldiesel_type').on('change',function(){
                 
                var petroldieselty = jQuery('#petroldiesel_type').val();
                 if ('Rs'==petroldieselty) {


                 } 

                 else{
                  jQuery('#petroldiesel_qty').val("");
                 }
              vil.getcpcity();

             });

            
           },
           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#RO_code').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                 var customer=jQuery('#customer_code').data('id');
                
                  jQuery.each(data, function(index,value){
                      if(customer==index)
                       opt+='<option selected value="'+index+'">'+value+'</option>';
                     else
                       opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  var Vehicle_Reg=jQuery('#customer_code').data('id');
                  jQuery.each(data, function(index,value){

                    if(Vehicle_Reg==value)
                     opt+='<option selected value="'+value+'">'+value+'</option>';
                   else
                    opt+='<option value="'+value+'">'+value+'</option>';

                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
                   vil.getcapcity();
                 vil.selectFuleType();
               });
           },
           getcapcity:function(){
            
             jQuery.get('getcapcitys',{
                registration_number:jQuery('#registration_number').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
             
                 
                 capacitys = parseInt(data);
                   
                jQuery('#petroldiesel_qtys').val(data);
              
                 vil.selectFuleType();
               });
           },
          
         getcpcity:function(){
        var rcapacity=parseInt(jQuery('#petroldiesel_qty').val());
        var petroldiesel_type=jQuery('#petroldiesel_type').val();   
             if('Rs'!=petroldiesel_type){
                  
                  
                    if(rcapacity>=capacitys){
                       jQuery('#tbnhide').hide();
                      alert("Please Enter Below Capacity Limit ");
                    }
                    else{
                              jQuery('#tbnhide').show();
                            }
                  }
                  else{
                     
                    jQuery('#tbnhide').show();
                  }
               },
            
                  
           selectFuleType:function(){
            var registration_number=jQuery('#registration_number').val();
             jQuery.get('getVihicleFuleType/'+registration_number,{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                //jQuery('#fuel select').val(data);
                jQuery('#fuel option[value='+data+']').attr('selected','selected');
               });
           },

           reqType:function(){
               jQuery('#Request_Value').hide();
              jQuery('#request_type').on('change',function(){
                    var t=jQuery(this).val();
                    if(t=='Full Tank'){
                      jQuery('#Request_Value').hide();
                      jQuery('#Request_Value_input').attr('required',false);
                    }
                    else{
                      jQuery('#Request_Value').show();
                      jQuery('#Request_Value_input').attr('required',true);

                    }

              });
           },

     }
     vil.init();
  });
</script>
</body>
</html>