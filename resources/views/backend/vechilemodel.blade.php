<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
  #errmsg
{
color: red;
}
</style>
<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  

  <!-- Side-Nav-->

  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Vehicle Model </h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Vehicle Model </a></li>
        </ul>
      </div>
    </div>
     <div class="">

                </div>
                 <div class="">
         <center>@if(Session::has('success'))
                       <font style="color:red">{!!session('exitdata')!!}</font>
                    @endif</center>
      </div>

    <div class="row">
              
      <div class="col-md-12 col-sm-12">
        <div class="row">
           <center>@if(Session::has('success'))
                       <font style="color:red">{!!session('success')!!}</font>
                    @endif</center>
                    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal> -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Add Vehicle Model	</h4>
                  </div>  
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal"   action="{{url('save_vechiledata')}}" method="post">
		                  {{ csrf_field() }}
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                           
                            <input type="text" class="form-control"  required="required" name="name" placeholder="enter vehicle model" /> 
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Select Company Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            
                            <select class="form-control"  name="make_id"  required/>
                            
                           @foreach($VehicleMakeModel as $row) 
                        
                              <option value="{{$row->id}}">{{$row->name}}</option>
                            
                             @endforeach
                            </select>  
                           
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Select Fuel Type <span style="color:#f70b0b;">*</span> </label>
                          <div class="col-sm-8">
                            
                            <select class="form-control"  name="fuel_type"  required/>
                            @if($StockItemGroup!=null && $StockItemGroup->children!=null)
                               @foreach($StockItemGroup->children as $children) 
                            
                                  <option value="{{$children->id}}">{{$children->Group_Name}}</option>
                                
                                 @endforeach
                            @endif     
                            </select>  
                           
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Capacity <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="text"   min="0" maxlength="5" class="form-control numeriDesimal"  required="required" name="capacity" placeholder="enter capacity" /> 
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <!--<button type="submit" class="btn btn-default">Submit</button>-->
							     <center><input type="submit"  class="btn btn-primary  submit-button"  value="Submit"></center>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
		  <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <a href="{{route('download_vehicle_mode')}}" class="btn btn-primary pull-right btn-sm RbtnMargin" >Download CSV File</a>
</div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
        
          <table class="table table-striped" id="myTable">

            <thead>
              <tr>
			          <th>S.No</th>
                <th>Company Name</th>
                <th>Model Name</th>
                <th>Fuel Type</th>
                <th>Capacity</th>
               
                <th style="width: 100px;">Actions</th>
               </tr>
            </thead>
            <tbody>
			    <?php $i=0; ?>
	         @foreach($VehicleModel as $VehicleModels)
	         <?php $i++ ;?>
              <tr>
			        <td>{{$i}}</td>

               <td>{{$VehicleModels->getmake['name']}}</td> 
               <td>{{$VehicleModels->name}}</td>	
               <td>{{$VehicleModels->getstockitemname['Group_Name']}}</td>  
               <td>{{$VehicleModels->capacity}}</td>
               <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> 
                  <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                    <li><a href="#" data-toggle="modal" data-target="#myModal{{$VehicleModels->id}}" data-fuel="$VehicleModels->fuel_type"  data-capacity="$VehicleModels->capacity">Edit</a></li>
                    </ul>
                  </div></td>
              </tr>
			      <div class="modal fade" id="myModal{{$VehicleModels->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabelcity">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Update Vehicle Model</h4>
                  </div>  
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal"   action="{{url('update_modeldata')}}/{{$VehicleModels->id}}" method="post">
                      {{ csrf_field() }}
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            
                            <input type="text" class="form-control"  required="required" value="{{$VehicleModels->name}}" name="name" placeholder="enter vehicle model name" /> 
                          </div>
                        </div>

                        <br><br>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Select Company Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            
                            <select class="form-control"  name="first_unit"  required/>
                            
                           @foreach($VehicleMakeModel as $row) 
                        
                              <option @if($row->id==$VehicleModels->make_id) selected @endif value="{{$row->id}}">{{$row->name}}</option>
                            
                             @endforeach
                            </select>  
                           
                          </div>
                        </div>
                        <br><br>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Select Fuel Type <span style="color:#f70b0b;">*</span> </label>
                          <div class="col-sm-8">
                            
                            <select class="form-control"  name="fuel_type"  required/>
                          @if($StockItemGroup!=null && $StockItemGroup->children!=null)
                           @foreach($StockItemGroup->children as $children) 
                        
                              <option @if($children->id==$VehicleModels->fuel_type) selected @endif value="{{$children->id}}">{{$children->Group_Name}}</option>
                            
                             @endforeach
                          @endif   
                            </select>  
                           
                          </div>
                        </div><br><br>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Capacity <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="text"  min="0" maxlength="5" class="form-control numeriDesimal"  required="required" name="capacity" value="{{$VehicleModels->capacity}}" placeholder="enter capacity" /> 
                          </div>
                        </div>
                        <br><br>
                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <!--<button type="submit" class="btn btn-default">Submit</button>-->
              <center><input type="submit"  class="btn btn-primary  submit-button"  value="Update"></center>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
		  @endforeach
            </tbody>
          </table>
       
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src="{{URL::asset('js/capitalize.js')}}"></script> 
<script type="text/javascript">
jQuery(function(){
   jQuery('#type1').on('change',function(){
        var a = document.getElementById("type2");
    var b = document.getElementById("type1");

    if (a.options[a.selectedIndex].value == b.options[b.selectedIndex].value) {
        alert("Please Choose Select unit symble");
         jQuery('.site-btn').attr('disabled',true);
    } else{
       jQuery('.site-btn').attr('disabled',false);


    }
   }); 
});
 jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });
</script>
<script type="text/javascript">
  $(function() {
     $('.row_dim').hide(); 
    $('#type').change(function(){
        if($('#type').val() == 'Compound') {
            $('.row_dim').show(); 
             //jQuery('#utint').css("display","none")
        } else {
            //$('#utint').show();
            jQuery('.row_dim').css("display","none")
        } 
    });
    
});
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>

<script type="text/javascript">
  
 $(document).ready(function () {
  //called when key is pressed in textbox
  $("#quantity").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});
</script>
<script>
  jQuery(function(){
     var url="{{url('check_Pedestal_Number_unique')}}";
     var vil={
           init:function(){
             vil.selectRo();
             vil.gstCodeChk();
         
           },

           gstCodeChk:function(){

            jQuery('#chk_gst_code').on('click',function(){
                  var id=0;
                   var table=jQuery('#quantity').data('table');
                   var colum=jQuery('#quantity').data('colum');
                   var mess=jQuery('#quantity').data('mess');

                   // id=jQuery(this).data('id');
                    
                  udat=jQuery('#quantity').val();

                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mess);

                   }else{
                    jQuery('#form_state').submit();
                   }
                       
                   });
              });
            
           
           },

          
           
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                   var colum=jQuery(this).data('colum');
                   var mess=jQuery(this).data('mess');

                   // id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();

                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mess);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }

     vil.init();
  });
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>