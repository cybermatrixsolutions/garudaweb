<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
<style type="text/css">
  input.form-control.abhi {
    border: 0px;
    background-color: #E5E5E5;
}

</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  <form class="form-horizontal form-inline" name="myForm" action="{{route('customer_commision')}}/save" enctype="multipart/form-data" method="post"  onsubmit="return confirm('Do you want to Continue?');">
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Create Services Bill
 </h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{route('customer_commision')}}">Create Services Bill
</a></li>
        </ul>
      </div>
    </div>
  
  
       <div class="">  <center>@if(Session::has('success'))
               <font style="color:red">{!!session('success')!!}</font>
            @endif</center>
       </div>

       
 <div class="row">
    <div class="col-md-11 col-md-offset-1" style="margin-top: 79px;;">
    
            
             {{ csrf_field() }}
        <div class="row">
         <div class="col-md-12">
          
          <div class="form-group">
             <label class="control-label"  for="Select Type">Select Type</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <select class="form-control" name="fule_type" id="fule_type">
                     <option value="2">GST</option>
                      <option value="1">VAT</option>
              </select>
          </div>
      
          <div class="form-group">
             <label class="control-label"  for="Select Customer">Select Company Name</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <select class="form-control" required name="customer_code" id="customer_code">
                  @if(isset($tbl_customers))
                    @foreach($tbl_customers as $tbl_customer)
                     <option  value="{{$tbl_customer->Customer_Code}}">{{$tbl_customer->company_name}}</option>
                    @endforeach
                  @endif
               </select>
          </div>              
          <div class="form-group">
            <input type="submit" class="btn btn-default" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
          </div>
        </div>
    </div>
 
    </div>
    </div>
  <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <input type="checkbox" id="checkAll">Check All
          
          <table class="table table-striped" id="myTable">
            <thead>
              
              <tr>
               
                <th> </th>
                <th>Invoice NO</th>
                <th>Date</th>
               
               
            </thead>
                      
             <tbody id="invoices_data">
              
              
            </tbody>
           
          </table>
          
       
          </div>
         </div>
      </div>
    </div>
  
  
  
  
  </div>
</div>
 </form>
 <footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script>
<script type="text/javascript">
  $(function () {
  $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  }).datepicker('update');
});

</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
             vil.getcom();
          
           },
           selectRo:function(){
            jQuery('#customer_code').on('change',function(){
                  vil.getcom();
              });
            jQuery('#fule_type').on('change',function(){
                  vil.getcom();
              });
           },
           getcom:function(){
                var customer=jQuery('#customer_code').val();
                var fule_type=jQuery('#fule_type').val();
               jQuery.get("{{url('getcustomerinvoice')}}/"+customer+'/'+fule_type,{
                
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                
                   jQuery('#invoices_data').html(data);
                   jQuery('#myTable').DataTable();
               });
           },
          

     }
     vil.init();
  });
</script>
<script language="JavaScript">
$("#checkAll").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 });
</script>

<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

</body>
</html>