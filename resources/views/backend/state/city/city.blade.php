<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
  #errmsg
{
color: red;
}
</style>
<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  

  <!-- Side-Nav-->

  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; {{$StateManagementModel->name}} </h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Citys</a></li>
        </ul>
      </div>
    </div>
     <div class="">

                </div>
                 <div class="">
         <center>@if(Session::has('success'))
                       <font style="color:red">{!!session('exitdata')!!}</font>
                    @endif</center>
      </div>

    <div class="row">
              
      <div class="col-md-12 col-sm-12">
        <div class="row">
           <center>@if(Session::has('success'))
                       <font style="color:red">{!!session('success')!!}</font>
                    @endif</center>
                    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal> -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel"> City	</h4>
                  </div>  
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal"   action="{{url('save_citydata')}}" method="post">
		                  {{ csrf_field() }}
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <input type="hidden" class="form-control" name="statecode" value="{{$StateManagementModel->id}}" /> 
                            <input type="text" class="form-control"  required="required" name="name" placeholder="city Name" /> 
                          </div>
                        </div>

                       
                       
                    
                       
                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <!--<button type="submit" class="btn btn-default">Submit</button>-->
							<center><input type="submit"  class="btn btn-primary  submit-button"  value="Submit"></center>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
        
          <table class="table table-striped" id="myTable">

            <thead>
              <tr>
			          <th>S.No</th>
                <th>Name</th>
                <th style="width: 100px;">Action</th>
               </tr>
            </thead>
            <tbody>
			<?php $i=0; ?>
			
	            @foreach($CityModel as $row) 
				
	         <?php $i++ ;?>
			
              <tr>
			        <td>{{$i}}</td>
                
                <td>{{$row->name}}</td>		
				
                    <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                       <!--<li><a href="#">View</a></li>-->
                       
                          <li><a href="#" data-toggle="modal" data-target="#myModal{{$row->id}}">Edit</a></li>
                           
                    <!--  <li><a href="unit_measure_delete/{{$row->id}}" onClick="return confirm('Are you sure?');">Delete</a></li>-->
                    </ul>
                  </div></td>
              </tr>
			      <div class="modal fade" id="myModal{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabelcity">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel"> City Management  </h4>
                  </div>  
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal"   action="{{url('update_citydata')}}/{{$row->id}}" method="post">
                      {{ csrf_field() }}
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Name</label>
                          <div class="col-sm-8">
                             <input type="hidden" class="form-control" name="statecode" value="{{$row->id}}" /> 
                            <input type="text" class="form-control"  required="required" value="{{$row->name}}" name="name" placeholder="city Name" /> 
                          </div>
                        </div>
                        <br><br>
                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <!--<button type="submit" class="btn btn-default">Submit</button>-->
              <center><input type="submit"  class="btn btn-primary  submit-button"  value="Update"></center>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
			 @endforeach
            </tbody>
          </table>
       
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script type="text/javascript">
jQuery(function(){
   jQuery('#type1').on('change',function(){
        var a = document.getElementById("type2");
    var b = document.getElementById("type1");

    if (a.options[a.selectedIndex].value == b.options[b.selectedIndex].value) {
        alert("Please Choose Select unit symble");
         jQuery('.site-btn').attr('disabled',true);
    } else{
       jQuery('.site-btn').attr('disabled',false);


    }
   }); 
});
</script>
<script type="text/javascript">
  $(function() {
     $('.row_dim').hide(); 
    $('#type').change(function(){
        if($('#type').val() == 'Compound') {
            $('.row_dim').show(); 
             //jQuery('#utint').css("display","none")
        } else {
            //$('#utint').show();
            jQuery('.row_dim').css("display","none")
        } 
    });
    
});
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>

<script type="text/javascript">
  
 $(document).ready(function () {
  //called when key is pressed in textbox
  $("#quantity").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});
</script>
<script>
  jQuery(function(){
     var url="{{url('check_Pedestal_Number_unique')}}";
     var vil={
           init:function(){
             vil.selectRo();
             vil.gstCodeChk();
         
           },

           gstCodeChk:function(){

            jQuery('#chk_gst_code').on('click',function(){
                  var id=0;
                   var table=jQuery('#quantity').data('table');
                   var colum=jQuery('#quantity').data('colum');
                   var mess=jQuery('#quantity').data('mess');

                   // id=jQuery(this).data('id');
                    
                  udat=jQuery('#quantity').val();

                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mess);

                   }else{
                    jQuery('#form_state').submit();
                   }
                       
                   });
              });
            
           
           },

          
           
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                   var colum=jQuery(this).data('colum');
                   var mess=jQuery(this).data('mess');

                   // id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();

                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mess);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }

     vil.init();
  });
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>