<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<title>Garruda</title>
<style type="text/css">
   tfoot {
    display: table-header-group;
}



element.style {
    width: 115px;
}
::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
    color:#fff;
    opacity: 1; /* Firefox */
}
</style>
<style>
button.dt-button, div.dt-button, a.dt-button {
    
    background-image: linear-gradient(to bottom, #00786a 0%, #00786a 100%)!important;
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,StartColorStr='white', EndColorStr='#00786a');
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    text-decoration: none;
    outline: none;
    color:white!important;
    }
    .dt-buttons {
    float: right!important;
}

.dataTables_wrapper .dataTables_filter {
float: left;
text-align: left;
}
</style>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
   @include('backend-includes.header')

        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; View & Alter Slip Entry </h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          
         
          <li><a href="#"> View & Alter Slip Entry</a></li>
        </ul>
      </div>
    </div>
     <div class="">
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
      
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">

       <div class="row" style="margin-left: -29px;margin-top: 30px !important;">
         <div class="col-md-12">
              <form class="form-horizontal form-inline" name="myForm" action="{{'ro_transaction_list'}}" enctype="multipart/form-data" method="get"  onsubmit="return confirm('Do you want to Continue?');">
              <div class="form-group col-md-2">
                      <select class="form-control" required name="cust_code" id="id-select-customer" placeholder="Customer Name" style="width: 115%;">
                        
                        @if(isset($tbl_customer_data))

                      <option  value="0">Select Customer</option><option  value="All">All</option>
                          @foreach($tbl_customer_data as $getrocodecust)
                          @if($getrocodecust->Customer_Code==$customerCode)

                           <option  selected value="{{$getrocodecust->Customer_Code}}" >{{$getrocodecust->company_name}}</option>
                          @else
                           <option  value="{{$getrocodecust->Customer_Code}}" >{{$getrocodecust->company_name}}</option>
                           @endif
                          @endforeach
                        @endif
                        </select>
              </div>

              
             <div class="form-group col-md-2">
               

                 <input type="text" required class="form-control datepickers" id="from_date" name="from_date" placeholder="From Date" value="{{$from_date_for_view}}"/>
              </div>
              <div class="form-group col-md-2">
                   
                      <input required type="text" class="form-control datepickers" id="to_date" name="to_date" placeholder="To Date" value="{{$to_date_for_view}}"/>
              </div>
              <div class="form-group col-md-2">
                  <input type="submit" class="btn btn-default" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
              </div>
             </form> 
            </div>
        </div>



        <div class="row">
          <!--  <form class="form-horizontal form-inline" name="myForm" action="{{'customerrequestfind'}}" enctype="multipart/form-data" method="post">
       
            
             {{ csrf_field() }}
        <div class="row">
         <div class="col-md-12">
         

          <div class="form-group">
             <label class="control-label"  for="email">Customer Name</label>&nbsp;&nbsp;
                 <select class="form-control"  name="customer_code" id="customer_code">
                      
                        @if(isset($RoCustomertManagement))
                      @foreach($RoCustomertManagement as $RoCustomertManagements)

                       <option value="{{$RoCustomertManagements->Customer_Code}}">{{$RoCustomertManagements->company_name}}</option>
                      @endforeach
                      @endif
                    </select>
          </div>

                 &nbsp;&nbsp;
                      <div class="form-group">
                          <label class="control-label"  for="email">Vehicle No.</label>&nbsp;&nbsp;
                          <select class="form-control" required="required" name="Vehicle_Reg_No" id="registration_number">                
                            </select>
                            &nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                          <label class="control-label"  for="email">Req.No./Slip No.</label>&nbsp;&nbsp;
                          <input type="text"  class="form-control" style="width:123px;" name="slipno" placeholder="req.No./slip no."/>
                            &nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                          <label class="control-label"  for="email">From</label>&nbsp;&nbsp;
                           <input type="text"  class="form-control datepickers" style="width:123px;" required name="fromdate" placeholder="from date"/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                              <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                                <input  type="text" class="form-control datepickers" style="width:123px;" name="to_dae" placeholder="To Date"/>&nbsp;&nbsp;
                        </div>
                        <br>
                        <center>
                        <div class="form-group" style="display: none;">
                                <input type="submit" class="btn btn-default" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                        </div>
                        </center>
            </div>
        </div>
 </form> --> 
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Add Transaction</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
     <div class="row">
      <div class="col-md-12"> 
        <div class="col-md-10"></div>
        <div class="col-md-2"><!-- <a target="_blank" href="{{url('ro_transaction_list/export')}}" class="btn btn-primary">Export in CSV file</a> --></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive" style="height:425px;">
          <table class="table table-striped" id="myTable" style="padding:0px !important;">
            
            <thead style="padding:0px !important;">
              
              <tr style="padding:0px !important;">
                <th>S.No.</th>
              
                <th>Customer Name</th>
                <th>Vehicle number</th>
                <th>Fuel Type</th>
                <th>Slip No</th>
                <th>Slip(Transaction) Date</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Value</th>
                <th>Shift Manager</th>
                <th>Group</th>
                <th>Trans Mode</th>
                <th>Req.No./Slip No.</th>
                <th>Request Date</th>
                
                <th>Action</th>
              </tr>
               @if(count($transaction_data)!=null)
            <tfoot>
              <tr>
                <th>S.No.</th>
               
                <th>Customer Name</th>
                <th>Vehicle number</th>
                <th>Fuel Type</th>
                <th>Slip No</th>
                <th>Slip(Transaction) Date</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Value</th>
                <th>Shift Manager</th>
                <th>Group</th>
                <th>Trans Mode</th>
                <th>Req.No./Slip No.</th>
                <th>Request Date</th>
                
               </tr>
            </tfoot>
            @endif
            </thead>
            
            <tbody>
              @if(count($transaction_data)!=null)
              <?php $i=0;?>
               @foreach($transaction_data as $list)
               <?php $i++;?>
              <tr>
                <td>{{$i}}</td>
                
                <td>@if($list->getCustomername!=null){{$list->getCustomername->company_name}} @endif</td>
                <td>{{$list->Vehicle_Reg_No}}</td>
            
                <td>{{$list->getItemName['Item_Name']}}</td>

                <td>{{$list->slip_detail}}</td>

                 <td>@if($list->trans_date!=null){{Carbon\Carbon::parse($list->trans_date)->format('d/m/Y')}}@else {{Null}}@endif</td>

                <td style="text-align: right;">{{number_format($list->petroldiesel_qty,3)}}</td>

                <td style="text-align: right;">{{number_format($list->item_price,2)}}</td>
                  
                <td style="text-align: right;">{{number_format($list->petroldiesel_qty*$list->item_price,2)}}</td> 
                <td>@if(!empty($list->getShift->getPersonnelname->Personnel_Name)){{$list->getShift->getPersonnelname->Personnel_Name}} @endif</td>

                <td> @if($list->getItemName!=null && $list->getItemName->getgroup!=null){{$list->getItemName->getgroup->Group_Name}} @endif</td>

                <td>{{$list->trans_mode}}</td>

                <td>{{$list->Request_id}}</td>

                <td>@if($list->request_date!=null){{Carbon\Carbon::parse($list->request_date)->format('d/m/Y')}}@else {{Null}}@endif</td>
                
                
                
                <td><div class="btn-group">
                  @if($list->petrol_or_lube==1&&$list->confirm==0)
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                       
                      <li><a href="{{'fuelslipedit'}}/{{$list->id}}">Edit</a></li>
                     
                    </ul>

                  
                       @endif  
                  </div>
               </td>
              </tr>
              @endforeach
                @else
                
               @endif
            </tbody>
             <tfoot align="right">
                  <!--   <td>&nbsp;&nbsp;</td> -->
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                    <td>Total</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td id="id-html-qty" style="text-align:right"></td>
                    <td>&nbsp;&nbsp;</td>
                   
                    <td id="id-html-total" style="text-align:right"></td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                     <!--  <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                     <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td> -->
              </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>

<script type="text/javascript">
 $(document).ready(function() {
    $('#myTable').DataTable( {

    
       "dom": 'lBfrtip',
          "processing": true,
         buttons: [
           
            { extend: 'excelHtml5',text: 'Export in CSV', footer: false , filename: ''+Date('Y-m-d')+'-DailyTotalShiftSales&SettlementReport'},
            
        ],  paging: false,"footerCallback": function ( row, data, start, end, display ) {


          var api = this.api(), data;
 
            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // computing column Total of the complete result 
             /*   var grossTotal = api
                .column( 5 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );*/
        
      var qtyTotal= api
                .column( 6 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
        
      var testTotal = api
                .column( 8  , { page: 'current'})
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
        
      
/*
      var withVatTotal = api
                .column( 13 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );         

          var vatValTotal = api
                .column( 14 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                

                
              var totalValTotal = api
                .column( 15, { page: 'current'}  )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );                 
      
        
            // Update footer by showing the total with the reference of the column index 
      $( api.column( 4).footer() ).html('Total : ');
            $( api.column( 5 , { page: 'current'} ).footer() ).html(grossTotal.toFixed(3) );*/
          $('#id-html-qty').val(api.column( 6, { page: 'current'} ).footer() ).html(qtyTotal.toFixed(3));
             $('#id-html-total').val( api.column( 8 , { page: 'current'} ).footer() ).html(testTotal.toFixed(3));
            
           /* $( api.column( 13, { page: 'current'}  ).footer() ).html(withVatTotal.toFixed(2));
            $( api.column( 14 , { page: 'current'} ).footer() ).html(vatValTotal.toFixed(2));
            $( api.column( 15 , { page: 'current'} ).footer() ).html(totalValTotal.toFixed(2));
       
          */
           },
       
          "lengthMenu": [[100, 250, 500, 1000], [100, 250, 500, 1000]],
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
  
  
</script
<script  src="js/index.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
            
             vil.selectRo();
             
              vil.getcrg();
              /*vil.fromDate();
                          vil.to_date();*/
           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });

                

                  $("#from_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                        
                  });

                  $("#to_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                        defaultDate:'dd/mm/yyy',
                         
                       
                        format: 'dd/mm/yyyy',
                        
                  });
           
           },

          

           fromDate:function(){
                var fromdate=jQuery('#from_date').val();
                var todate=jQuery('#to_date').val();
                jQuery("#from_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                         endDate: new Date(),
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     fromdate=jQuery(this).val();
                     vil.to_date();
                    }).datepicker('setEndDate',fromdate);
              },
              to_date:function(){
                var todate=jQuery('#to_date').val();
                 var fromdate=jQuery('#from_date').val();
                  
                  jQuery("#to_date").datepicker({ 
                        autoclose: true, 
                        todayHighlight: true,
                       
                         startDate: fromdate,
                        defaultDate:'dd/mm/yyy',
                        format: 'dd/mm/yyyy',
                  }).on('changeDate', function(e) {
                     todate=jQuery(this).val();
                     vil.to_date();
                    }).datepicker('setEndDate',todate);



              },
          
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
               });

           },
              

     }
     vil.init();
  });
</script>
</body>
</html>