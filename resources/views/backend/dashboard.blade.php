<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
<style>
  .slimScrollDiv {
   height:834px!important;
   }

        .ui-tabs ul li {
    float: left;
    width: auto;
    margin-top: 10px;
    list-style: none;
    background: #00786a;
    padding: 5px;
    margin-right: 2px;
    
}
.ui-tabs ul li a{
    color: #fff !important;
}
.ui-tabs-active{
      background: #fff !important;
      border: 1px solid #ccc;
      border-bottom: none;
     color: #000 !important;
}

.ui-tabs ul li a {
    background:transparent !important;
    padding: 6px 10px;
    margin-right: 5px;
    border-radius: 2px;
}
  
  .ui-tabs ul .ui-tabs-active a{
     color: #000 !important;
}     
    </style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  <div class="content-wrapper">
 <?php        $is_active=1;
                 if (Auth::user()->user_type==3) {

                    $is_active=Auth::user()->getRocode->is_active;

                 }elseif(Auth::user()->user_type==4){
                    $is_active=Auth::user()->customer->is_active;

                 }elseif(Auth::user()->user_type==5){

                    $is_active=Auth::user()->getPersonel->is_active;
                 }
          ?>


@if($is_active==0 && Auth::user()->user_type==3)

        <p style="color:red;text-align: center;"> There is Some Issue Please Contact Garruda Admin </p>
@elseif($is_active==0 && Auth::user()->user_type==4 && Session::has('ro'))
        <p style="color:red;text-align: center;"> There is Some Issue Please Contact Ro Admin </p>

@elseif($is_active==0 && Auth::user()->user_type==5)
        <p style="color:red;text-align: center;"> There is Some Issue Please Contact Ro Admin </p>        
@else
     @if(Auth::user()->user_type==1 || Auth::user()->user_type==4)
 
           @if(empty(Session::has('ro')))
             <form class="form-horizontal" action="{{'createSession'}}" method="post" onsubmit="return confirm('Do you want to Continue?');">

              {{ csrf_field() }}
              <div class="form-group">
              <label class="control-label col-sm-3" for="email">Outlet Name</label>
              <div class="col-sm-9">
                <select class="form-control" required="required" name="RO_code" id="RO_code">
                   @if(isset($customer_ro))
                      @foreach($customer_ro as $ro)
                          <option name="company_id" value="{{$ro->RO_code}}">{{$ro->pump_name}}_ @if($ro->getro!=null && $ro->getro->getcity!=null){{$ro->getro->getcity->name}} @endif</option>
                      @endforeach
                    @endif
                </select>
              </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                  <button type="submit" class="btn btn-default">Submit</button>
                </div>
              </div>
            </form>
          @endif
    @else
      <h3 align="center">Gateway of GARRUDA</h3>
    @endif
  
  
    @if(Auth::user()->user_type==1 || Auth::user()->user_type==3)
  
        <?php  
         $rocode='';
         $roType='';
         $roPrinciple='';
         $confirm=0;
         $id=0;
       if (Auth::user()->getRocode!=null) {
          $rocode=Auth::user()->getRocode->RO_code;
          $roType=Auth::user()->getRocode->Ro_type;
          $roPrinciple=Auth::user()->getRocode->principal->company_name;
          $confirm=Auth::user()->getRocode->confirm;
          $id=Auth::user()->getRocode->id;
       }

         $countries_list =DB::table('countries')->get();
         $retailData=DB::table('tbl_ro_master')->where('id','=',$id)->get();
         $row=App\RoMaster::where('id',$id)->first();
         $getPrincipal = DB::table('tbl_company_principal_master')->where('is_active', '>=',1)->orderBy('company_name', 'asc')->get();

       
        $lov=App\RoLobSelectionModel::where('RO_code',$rocode)->get();
         $lob_item_list= App\RoLOBItemSelectionModel::where('is_active','1')->get();
        ?>

        @if($lov->count()<= 0 && $confirm==1)
           <form class="form-horizontal" action="{{'save_selected_lob'}}" method="post" onsubmit="return confirm('Do you want to Continue?');">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
               <input type="hidden" name="RO_code"  value="{{$rocode}}">
                <div class="row">
                  <div class="col-md-12">
                      <div class="card" style="padding:10px 15px; margin-bottom:10px;">
                    @if($roType==1)
                     <h4>{{$roPrinciple}} </h4>   
                    @endif
                    <h6>Select LOB'S</h6>
                  </div>
                  </div>
                </div>
                  <div class="row">
                   
                          @foreach($lob_item_list as $lob_list)
                          <div class="col-md-4">
                            <div class="card" style="padding:5px 0 5px 0; margin-bottom:8px;">
                                <div class="row " style="margin:0px!important;">
                                  <div class="col-md-6">
                                      {{$lob_list->lob_name}}
                                  </div>
                                  <div class="col-md-6">

                                  <input style="width:20%" class="pull-right" type="checkbox" name="lob_name[]" value="{{$lob_list->lob_name}}">
                           
                                  </div>
                                </div>

                                </div>
                            </div>
                              @endforeach
                        
                  </div>
                  <div>
                    <span style="padding-top:10px; display: inline-block;">lob can not be removed ones saved</span>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                       <div class="form-group">
                <div class="">
                  <button type="submit" class="btn btn-default">Submit</button>
                </div>
              </div>
          </form>
                    </div>  
                  </div>




        @endif
         
         @if($confirm==0 && $lov->count()>= 0)
            @include('backend.component.roEdite',compact('retailData','countries_list','getPrincipal','row'));
         @endif
         


        @endif
    @endif

  </div>
  
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
 $('#tags').select2({
 
    tags: true,
    tokenSeparators: [','], 
    placeholder: "Add your tags here"
});

</script>

<script type="text/javascript">
 jQuery(function(){
     var url1="{{url('get_country_state')}}";
     var url2="{{url('get_state_city')}}";
     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
               jQuery("#tabs-n").tabs();
                vil.nextbutton();
                vil.getemail();
                vil.getmobile();
                vil.getprincple();
                vil.getOutlet();

           },
           selectRo:function(){
            jQuery('#country').on('change',function(){
                  vil.getcom();
              });
            jQuery('#state').on('change',function(){
                  vil.getcrg();
              });
            jQuery('#email').keyup(function(){
                vil.getemail();
                });
                jQuery('#mobile').on('keyup',function(){
                    vil.getmobile();
                });
                jQuery('#printpp').on('change',function(){
                    vil.getprincple();
                });
                jQuery('#Outlet').on('keyup',function(){
                    vil.getOutlet();
                });
           
           },
           getemail:function(){
                      var  sta=jQuery('#email').val();
                      jQuery('#emailcody').val(sta);
             },
             getOutlet:function(){
                      var  sta=jQuery('#Outlet').val();
                      jQuery('#copycompany').val(sta);
             },
            getprincple:function(){
                      var  sta=jQuery('#printpp').val(); 
                      jQuery('#copyprincple').val(sta);
             },
            getmobile:function(){
                      var  sta=jQuery('#mobile').val();
                      jQuery('#copymobile').val(sta);
                    },
            nextbutton:function(){
                 jQuery('.nextbutton').on('click',function(){
                   var id=jQuery(this).data('id');
                   id=id;
                   //jQuery('#tabs-n ul').tabs('select',2); 
                   jQuery("#tabs-n").tabs("option", "active", id);
                });

                 
                
            },
           getcom:function(){
             
               jQuery.get(url1,{
                
                rocode:jQuery('#country').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  var d=jQuery('#state').data('value');

                  jQuery.each(data, function(index,value){
                    if(d==index){
                       opt+='<option selected value="'+index+'">'+value+'</option>';
                    }else{
                        opt+='<option value="'+index+'">'+value+'</option>';
                      }
                     
                  });
                    console.log(opt);
                   
                   jQuery('#state').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get(url2,{
                rocode:jQuery('#state').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';

                 var d=jQuery('#city').data('value');

                   

                   if(data.length==0)
                   {
                    
                     opt+='<option value="'+jQuery('#state option:selected').val()+'">'+jQuery('#state option:selected').text()+'</option>';
                   }
                   else{


                  jQuery.each(data, function(index,value){

                     if(d==index){
                       opt+='<option selected value="'+index+'">'+value+'</option>';
                    }else{
                        opt+='<option value="'+index+'">'+value+'</option>';
                      }

                  });
                    
                   }
                jQuery('#city').html(opt);

               });
           },

     }
     vil.init();
  });
</script>
<script type="text/javascript">
 jQuery(function(){

  var url1="{{url('check_Pedestal_Number_unique')}}";

     var vil={
           init:function(){
             vil.selectRo();

              jQuery('form').on('submit',function(){
                   var gst=jQuery('#gst').val();
                    var gst=gst.toUpperCase();
                   var pan=jQuery("#pan").val();
                   var pan=pan.toUpperCase();
                   gst= gst.substring(2,12);

                   /*if(jQuery.trim(gst)!='' && gst!=pan){
                     alert('Please Enter valid GST');
                      jQuery("#success-btn").attr('disabled',false);

                      return false;
                   }*/

                   

              });
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   var current=jQuery(this);
                   id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  if(jQuery.trim(udat)!=''){


                  jQuery.get(url1,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);
                      

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
                  }
              });
            
           
           },
          
     }
     vil.init();
  });
</script>
<script type="text/javascript">
   function  gstAndPan() {
        var mobile = document.getElementById("mobile").value;
        var pattern = /^[0]?[789]\d{9}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
            return true;
        }
        alert("It is not valid mobile number.input 10 digits number!");
        return false;
    }
</script>

@if(Auth::user()->user_type==3)
<script>
  $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})

   function gstAndPan()
    {
        var gst= document.getElementById('gst').val();
        alert(gst);

    }
    $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
     
});
</script>

<script type="text/javascript">
 jQuery(function(){
     var url1="{{url('get_country_state')}}";
     var url2="{{url('get_state_city')}}";
      var url3="{{url('getstatecoded')}}";
     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
               jQuery("#tabs-n").tabs();
                vil.nextbutton();
                vil.getemail();
                vil.getmobile();
                vil.getprincple();
                vil.getOutlet();

           },
           selectRo:function(){
            jQuery('#country').on('change',function(){
                  vil.getcom();
              });
            jQuery('#state').on('change',function(){
                  vil.getcrg();
              });
            jQuery('#pan').keyup(function(){
                vil.setvat();
                });
                jQuery('#gst').keyup(function(){
                vil.setvat();
                });

            jQuery('#email').keyup(function(){
                vil.getemail();
                });
                jQuery('#mobile').on('keyup',function(){
                    vil.getmobile();
                });
                jQuery('#printpp').on('change',function(){
                    vil.getprincple();
                });
                jQuery('#Outlet').on('keyup',function(){
                    vil.getOutlet();
                });
           
           },
           getemail:function(){
                      var  sta=jQuery('#email').val();
                      jQuery('#emailcody').val(sta);
             },
             getOutlet:function(){
                      var  sta=jQuery('#Outlet').val();
                      jQuery('#copycompany').val(sta);
             },
            getprincple:function(){
                      var  sta=jQuery('#printpp').val(); 
                      jQuery('#copyprincple').val(sta);
             },
            getmobile:function(){
                      var  sta=jQuery('#mobile').val();
                      jQuery('#copymobile').val(sta);
                    },
            nextbutton:function(){
                 jQuery('.nextbutton').on('click',function(){
                   var id=jQuery(this).data('id');
                   id=id;
                   //jQuery('#tabs-n ul').tabs('select',2); 
                   jQuery("#tabs-n").tabs("option", "active", id);
                });

                 
                
            },
            setvat:function(){
                        //var sta=jQuery('#pan').val(); 
                        var  sta=jQuery('#getstatecode').val()+jQuery('#pan').val()+jQuery('#gst').val();
                         var  view=jQuery('#getstatecode').val()+jQuery('#pan').val();
                       
    
                      jQuery('#getsta').val(sta);
                       jQuery('#views').val(view);

                      
           },
           getstatecode:function(){
               
                jQuery.get(url3,{

                    state:jQuery('#state').val(),

                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                },function(data){
                     console.log(data+'hello');
                    jQuery('#getstatecode').val(data);

                   
                });
            },
           getcom:function(){
             
               jQuery.get(url1,{
                
                rocode:jQuery('#country').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  var d=jQuery('#state').data('value');

                  jQuery.each(data, function(index,value){
                    if(d==index){
                       opt+='<option selected value="'+index+'">'+value+'</option>';
                    }else{
                        opt+='<option value="'+index+'">'+value+'</option>';
                      }
                     
                  });
                    console.log(opt);
                   
                   jQuery('#state').html(opt);
                    vil.getstatecode();
                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get(url2,{
                rocode:jQuery('#state').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';

                 var d=jQuery('#city').data('value');

                   

                   if(data.length==0)
                   {
                    
                     opt+='<option value="'+jQuery('#state option:selected').val()+'">'+jQuery('#state option:selected').text()+'</option>';
                   }
                   else{


                  jQuery.each(data, function(index,value){

                     if(d==index){
                       opt+='<option selected value="'+index+'">'+value+'</option>';
                    }else{
                        opt+='<option value="'+index+'">'+value+'</option>';
                      }

                  });
                    
                   }
                jQuery('#city').html(opt);

               });
           },

     }
     vil.init();
  });
</script>
<script type="text/javascript">
 jQuery(function(){

  var url1="{{url('check_Pedestal_Number_unique')}}";

     var vil={
           init:function(){
             vil.selectRo();
             vil.checkvalidat();

              jQuery('form').on('submit',function(){
                   var gst=jQuery('#gst').val();
                    var gst=gst.toUpperCase();
                   var pan=jQuery("#pan").val();
                   var pan=pan.toUpperCase();
                   gst= gst.substring(2,12);

                  
              });
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   var current=jQuery(this);
                   id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  if(jQuery.trim(udat)!=''){


                  jQuery.get(url1,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);
                      

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
                  }
              });
            
           
           },
           checkvalidat:function(){
              jQuery("#next_1").on('click',function(){
                 
                     var flg=true;
                    jQuery('#tabs-1').find('input,textarea').each(function(){
                         if(jQuery(this).attr('required')){

                            var v=jQuery(this).val();
                            if(jQuery.trim(v)==''){
                              jQuery(this).css('border-color','red');
                               flg=false;
                            }else{
                               jQuery(this).css('border-color','');
                            }
                             
                              if(jQuery(this).attr('type')=='email'){

                                if(!vil.validateEmail(v,'email')) { 
                                    jQuery(this).css('border-color','red');
                                     flg=false;
                                 }
                              }else if(jQuery(this).attr('name')=='Secondary_Person_Mobile' || jQuery(this).attr('name')=='mobile'){
                                 
                                 if(!vil.validateEmail(v,'mobile') || v.length!=10) { 
                                    jQuery(this).css('border-color','red');
                                     flg=false;
                                 }

                              }else if(jQuery(this).attr('name')=='pin_code'){
                                 
                                 if(!vil.validateEmail(v,'pin_code')) { 
                                    jQuery(this).css('border-color','red');
                                     flg=false;
                                 }
                              }

                              
                         }
                    });
                    

                   if(flg){
                       var id=jQuery(this).data('id');
                       jQuery("#tabs-n").tabs("option", "active", id);
                   }

              });

             jQuery("#next-2").on('click',function(){
                     var pre=jQuery(this);
                     var flg=true;
                    jQuery('#tabs-2').find('input').each(function(){

                         var v=jQuery(this).val();
                         if(jQuery(this).attr('required')){
                            
                            if(jQuery.trim(v)==''){
                              jQuery(this).css('border-color','red');
                               flg=false;
                            }else{
                               jQuery(this).css('border-color','');
                            }
                         }

                        if(jQuery(this).attr('name')=='PAN_No'){
                           
                           console.log(v);
                           if(jQuery.trim(v)!='')
                            if(!vil.validateEmail(v,'PAN_No')) { 
                                    jQuery(this).css('border-color','red');
                                     flg=false;
                            }else{

                               jQuery(this).css('border-color','');
                            }
                        }
                         
                    });
                    var atr=false;
                    jQuery('.leftln-rigstln').each(function(){

                          var leftln=jQuery(this).find('.leftln').val();
                          var rigstln=jQuery(this).find('.rigstln').val();
                            //console.log(jQuery(this).find('.leftln').length);
                            console.log(leftln);
                            
                          if(leftln.length>rigstln){
                            jQuery(this).find('.leftln').css('border-color','red');
                            atr=true;
                            flg=false;
                          }else{
                            jQuery(this).find('.leftln').css('border-color','');
                          }
                          if(jQuery.trim(leftln)==''){
                            jQuery(this).find('.leftln').css('border-color','red');
                          }
                    });
                    
                    if(atr){
                      alert('Please Enter in  Start from  Lenght Not Greater Serial no. Lg.');
                    }

                   if(flg){

                       var id=pre.data('id');
                       jQuery("#tabs-n").tabs("option", "active", id);
                   }



              });

            },
            validateEmail:function(vl,ty) {
               var emailReg;
              if(ty=='email')
               var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

              if(ty=='mobile')
               emailReg = /^[0-9-+]+$/;
              
              if(ty=='pin_code')
                emailReg=/^[0-9]{6}$/;

                if(ty=='PAN_No')
                  emailReg=/^[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}$/;
                
              return emailReg.test( vl );
            },
          
     }
     vil.init();
  });
</script>
<script type="text/javascript">
   function  gstAndPan() {
        var mobile = document.getElementById("mobile").value;
        var pattern = /^[0]?[789]\d{9}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
            return true;
        }
        alert("It is not valid mobile number.input 10 digits number!");
        return false;
    }
</script>
@endif
</body>
</html>