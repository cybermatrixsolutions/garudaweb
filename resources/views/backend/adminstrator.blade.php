<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Invoice</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  <header class="main-header hidden-print"><a class="logo" href="index.html">Invoice</a>
    <nav class="navbar navbar-static-top"> 
      <!-- Sidebar toggle button--><a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a> 
      <!-- Navbar Right Menu--> 
      <div class="navbar-custom-menu">
            <ul class="top-nav">
              <!--Notification Menu-->
              <li class="dropdown notification-menu"><a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell-o fa-lg"></i></a>
                <ul class="dropdown-menu">
                  <li class="not-head">You have 4 new notifications.</li>
                  <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                      <div class="media-body"><span class="block">Lisa sent you a mail</span><span class="text-muted block">2min ago</span></div></a></li>
                  <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                      <div class="media-body"><span class="block">Server Not Working</span><span class="text-muted block">2min ago</span></div></a></li>
                  <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                      <div class="media-body"><span class="block">Transaction xyz complete</span><span class="text-muted block">2min ago</span></div></a></li>
                  <li class="not-footer"><a href="#">See all notifications.</a></li>
                </ul>
              </li>
              <!-- User Menu-->
              <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle fa-lg"></i></a>
                <ul class="dropdown-menu settings-menu">
                <li><a href="#"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
                  <li><a href="#"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                  <li><a href="login.html"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
    </nav>
  </header>
  <!-- Side-Nav-->
  <aside class="main-sidebar hidden-print">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image"><img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image"></div>
        <div class="pull-left info">
          <p>John Doe</p>
          <p class="designation">Admin</p>
        </div>
      </div>
      <!-- Sidebar Menu-->
      <ul class="sidebar-menu">
        <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>Principal ID</span><i class="fa fa-angle-right"></i></a>
        	 <ul class="treeview-menu">
                 <li><a href="index.html"><i class="fa fa-circle-o"></i> Principal Code Management</a></li>
				 <li><a href="stock_item_management.html"><i class="fa fa-circle-o"></i><span> Stock Item Management</span></a></li>
             </ul>
        </li>
        <li class="treeview active"><a href="#"><i class="fa fa-laptop"></i><span>Retail Outlet</span><i class="fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
         	 <li><a href="retail.html"><i class="fa fa-circle-o"></i> Retail Outlet Management</a></li>
            <li><a href="owner.html"><i class="fa fa-circle-o"></i> RO Owner Management</a></li>
             <li><a href="manager.html"><i class="fa fa-circle-o"></i> RO Manager Management</a></li>
			<li><a href="persoonel.html"><i class="fa fa-circle-o"></i><span> RO Personnel Management</span></a></li>
            <li><a href="price_list.html"><i class="fa fa-circle-o"></i><span> RO Price List Management</span></a></li>
             <li><a href="Customer.html"><i class="fa fa-circle-o"></i> RO Customer  Management</a></li>
            <li><a href="ownermanagement.html"><i class="fa fa-circle-o"></i> Owner Management</a></li>
              <li><a href="driver.html"><i class="fa fa-circle-o"></i> Customer Driver Management</a></li>
            <li><a href="vehiclemanagement.html"><i class="fa fa-circle-o"></i> Customer Vehicle Management</a></li>
            	<li><a href="qr_code.html"><i class="fa fa-circle-o"></i> QR Code Management</a></li>
            <li><a href="credit_limit.html"><i class="fa fa-circle-o"></i> Customer Credit Limit</a></li>

          </ul>
        </li>
        <li class="treeview"><a href="#"><i class="fa fa-dashboard"></i><span>Master</span><i class="fa fa-angle-right"></i></a>
        	  <ul class="treeview-menu">
               <li><a href="pedestal.html"><i class="fa fa-circle-o"></i> Pedestal Management</a></li>
 				<li><a href="nozzles.html"><i class="fa fa-circle-o"></i> Nozzles Management</a></li>
              		<li><a href="unit_of_measure.html"><i class="fa fa-circle-o"></i><span> Unit of Measure</span></a></li>
           <li><a href="stock_item_group.html"><i class="fa fa-circle-o"></i><span> Stock Item Group</span></a></li>
             <li><a href="designation.html"><i class="fa fa-circle-o"></i><span> Designation Management</span></a></li>
              <li><a href="dip-stick.html"><i class="fa fa-circle-o"></i><span> Dip Stick Management</span></a></li>
                   <li><a href="vehicle.html"><i class="fa fa-circle-o"></i><span> Vehicle Management</span></a></li>
                    <li><a href="tax.html"><i class="fa fa-circle-o"></i><span> Tax Management</span></a></li>
                     <li><a href="accounting.html"><i class="fa fa-circle-o"></i><span> Accounting Ledger</span></a></li>
                     <li><a href="tank.html"><i class="fa fa-circle-o"></i><span> Tank Management</span></a></li>
         	  </ul>
         </li>
      </ul>
    </section>
  </aside>
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Adminstrator Mnagement</h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><i class="fa fa-home fa-lg"></i></li>
          <li><a href="#">Adminstrator management</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Adminstrator management</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal">
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Phone Number</label>
                          <div class="col-sm-9">
                        	   <input type="text" class="form-control" id="" placeholder="Phone Number">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Mobile </label>
                          <div class="col-sm-9">
                        	   <input type="text" class="form-control" id="" placeholder="Mobile ">
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">IMEI No </label>
                          <div class="col-sm-9">
                        	   <input type="text" class="form-control" id="" placeholder="IMEI No ">
                          </div>
                        </div>
                            <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Email</label>
                          <div class="col-sm-9">
                        	   <input type="email" class="form-control" id="" placeholder="Email">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>RO Code</th>
                <th>Customer Code</th>
                <th>Customer Name</th>
                 <th>Actions</th>
            </thead>
            <tbody>
              <tr>
                <td scope="row">adgbf</td>
                <td>nnnfd</td>
                 <td>hnnasn</td>
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">View</a></li>
                      <li><a href="#">Edit</a></li>
                      <li><a href="#">Delete</a></li>
                    </ul>
                  </div>
               </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Javascripts--> 

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>