<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">

<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<title>Garruda</title>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
 <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Nozzles</h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Nozzles</a></li>
        </ul>
      </div>
    </div>
                      <div class="">
                             
                            </div>  
                              @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Nozzles</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form  class="form-horizontal" action="{{'add_pedestal'}}" enctype="multipart/form-data" method="post" >
                      {{ csrf_field() }}

                       <input type="hidden" name="RO_code" id="RO_code" value="{{Auth::user()->getRocode->RO_code}}">
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Tank No. <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control tank" id="tank" name="Tank_id">
                              @foreach($getPedestalsa as $row) 
                              <option value="{{$row->id}}">{{$row->Tank_Number}}</option>
                              
                               @endforeach
                              
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Pedestal Number <span style="color:#f70b0b;">*</span></label>
                         <div class="col-sm-9">
                           <select class="form-control" id="Pedestal_id" name="Pedestal_id">
                            @foreach($getPedestals as $row) 
                              <option value="{{$row->id}}">{{$row->Pedestal_Number}}</option>
                              
                               @endforeach
                             
                            </select>
                          
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Fuel Type<span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input type="text" redaonly class="form-control" id="fuel_types" readonly name="fuel_typedsdsd"/>
                             <input type="hidden" id="fuel_type"  name="fuel_type">

                          </div>
                         
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Nozzles No <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control checkunique" data-table="tbl_pedestal_nozzle_master" data-colum="Nozzle_Number" data-mass="Nozzles No Already Exist" id="" name="Nozzle_Number" placeholder="Nozzle_Number" required/>
                          </div>
                         
                        </div>
                       <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Fuel Type <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control" id="Pedestal_id" name="fuel_type">
                            @foreach($fuel_type as $fuel) 
                              <option value="{{$fuel->id}}">{{$fuel->Item_Name}}</option>
                              
                               @endforeach
                             
                            </select>
                            
                          </div>
                        </div> -->
                         
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Opening Reading <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control numeriDesimal" id="" name="Opening_Reading" placeholder="Opening_Reading"  required/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Opening Date <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input class="form-control" name="Opening_Date" id="datepicker"  type="text" required/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Remarks</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="" name="Remarks" placeholder="Remarks">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Active From Date</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="datepickers" value="01/04/2017"   name="Active_From_Date" placeholder="Active From Date">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <input type="submit"  value="submit" class="btn btn-default submit-button" disabled="disabled"> 
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>S.No.</th>
                <!--<th>RO Code</th>-->
               <th>Nozzles No</th>
               <th>Pedestal Number</th>

                <th>Tank No</th> 
                <th>Fuel Type</th> 

                 <th>Open Reading</th>
                 <th>Opening Date</th>  
               
                
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=0; ?>
      
                 @foreach($getPedestal as $row) 
    
              <?php $i++ ;?>
              <tr>
                <td>{{$i}}</td>
              
                <td>{{$row->Nozzle_Number}}</td>
               <td>{{$row->Pedestal_Number}}</td>

               <td>{{(isset($row->getTank)) ? $row->getTank->Tank_Number : $row->Tank_id}}</td>
               <td>{{($row->getItem !=null) ? $row->getItem->Item_Name: ''}}</td>
               
                 
                 <td>{{$row->Opening_Reading}}</td>
                   <td>{{Carbon\Carbon::parse($row->Opening_Date)->format('d/m/Y')}}</td>
                  
                 <td> <?php if($row->is_active==1){?>

                   <a  title="Active" href="{{URL('pedestlDeactive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to deactivate? if you deactive this all relation will be deactivated.');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="{{URL('pedestlActive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to activate?');">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
          </td>
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                      <!--<li><a href="#">View</a></li>-->
                      @if($row->is_active==1)

                      <li><a href="pestal_update/{{$row->id}}" >Edit</a></li>
                      @endif
                      <li><a href="nozal_show/{{$row->id}}" >View</a></li>
                    <!--  <li><a href="pestal_delete/{{$row->id}}" onClick="return confirm('Are you sure?');">Delete</a></li>-->
                    </ul>
                  </div></td>
              </tr>
        @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script> 
<script src='https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js'></script> 

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

<script  src="js/index.js"></script> 
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script>
  $(function () {
  $("#datepicker").datepicker({ 
        autoclose: true, 
        
        format: 'dd/mm/yyyy'
      
  }).datepicker();
});
  $(function () {
  $("#datepickers").datepicker({ 
        autoclose: true, 
        
        format: 'dd/mm/yyyy'
      
  }).datepicker();
});

  $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
} );
 jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });
</script>
<script>
function validateForm() {
    var Nozzle_Number = document.forms["myForm"]["Nozzle_Number"].value;
    var Opening_Reading = document.forms["myForm"]["Opening_Reading"].value;
 
    if (Nozzle_Number == "") {
        alert("Please Enter Nozzle Number !!");
       
        return false;
    }
    if (Opening_Reading == "") {
        alert("Please Enter Opening Reading !!");
         
        return false;
    }
}
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>

<script type="text/javascript">
 jQuery(function(){

  var url1="{{url('check_Pedestal_Number_unique')}}";

     var vil={
           init:function(){
             vil.selectRo();
             vil.getfuel();
             vil.getfuelname();
             jQuery('#tank').on('change',function(){
                vil.getfuel();
             });
             jQuery('.tank').on('change',function(){
                vil.getfuelname();
             });
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   // id=jQuery(this).data('id');
                      var colum1val=jQuery('#RO_code').val();
                    
                  udat=jQuery(this).val();
                  jQuery.get(url1,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
                  colum2:'RO_code',
                  colum12val:colum1val,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
           },
          getfuel:function(){
            var tnk=jQuery('#tank').val();
             jQuery.get('getTankfuel/'+tnk,{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                console.log(data);
                jQuery('#fuel_type').val(data);
               });

          },
           getfuelname:function(){
            var tnk=jQuery('.tank').val();
             jQuery.get('getTankname/'+tnk,{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                console.log(data);
                jQuery('#fuel_types').val(data);
               });

          },
     }
     vil.init();
  });
</script>
</body>
</html>