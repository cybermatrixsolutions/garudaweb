<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
 <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Vehicle QR Codes</h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Vehicle QR Codes</a></li>
        </ul>
      </div>
    </div>
         <div class="">
       
           </div>  
    
    
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
           <center>
          @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
            @if(Session::has('success'))
             <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">QR Code</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                       <form class="form-horizontal"  action="{{'qr_code'}}" enctype="multipart/form-data" method="post"  onsubmit="return confirm('Do you want to Continue?');">
                           {{ csrf_field() }}
                           <input type="hidden" id="sel1" name="Ro_code" value="{{Auth::user()->getRocode->RO_code}}">
                        <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Pump Legal Name</label>
                          <div class="col-sm-9">

                            <select class="form-control" id="sel1" name="Ro_code">
                             @foreach($data1 as $row) 
                              <option value="{{$row->RO_code}}">{{$row->pump_legal_name}}</option>
                              
                               @endforeach
                            </select>
                          </div>
                        </div> -->
                       <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Company Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control" id="customer_Code" required name="Customer_Code">
                             
                            </select>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Vehicle <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <select class="form-control" id="Vehicle_Reg_no" required name="Vehicle_Reg_no">
                             
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <input type="submit" class="btn btn-success" value="Generate QR">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

           
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>S.No</th>
                <!-- <th>Outlet Name</th> -->
                <th>Customer  Name</th>
                <th>Vehicle Reg. No</th>
                <th>Make</th>
                <th>Modale</th>
                <th>Fuel type</th>
                <th>QR CODE</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=0; ?>

                 @foreach($data as $row) 

      
              <?php $i++ ;?>
              <tr>
                <td>{{$i}}</td>
               <!--  <td>{{$row->pump_name}}</td> -->
                <td>{{$row->getcustomer->company_name}}</td>
                <td>{{$row->Vehicle_Reg_no}}</td>
                 <td>@if($row->getVihicle!=null && $row->getVihicle->getmake!=null) {{$row->getVihicle->getmake->name}} @endif</td>
                  <td>@if($row->getVihicle!=null && $row->getVihicle->getmmodel!=null) {{$row->getVihicle->getmmodel->name}} @endif</td>
                   <td>@if($row->getVihicle!=null && $row->getVihicle->getitem!=null) {{$row->getVihicle->getitem->Item_Name}} @endif</td>

                <td> 
                 @if($row->is_active==1)
                   <a href="{{'print_val/'.$row->id}}" target="blank" >View</a>
                   @else
                   <span style="color:red"> Deactive </span>
                 @endif

                  
                  </td>
               <td> 

                 
                <?php if($row->is_active==1){?>


                   <a id="dereganaretqrcode"  title="Active" href="qr_code/active/{{$row->id}}/0" onclick="return confirm('Do you want to deactivate?');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a id="ganaretqrcode" title="Deactive" href="qr_code/active/{{$row->id}}/1" onclick="return confirm('Do you want to activate?');">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
          </td>

                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                     <!-- <li><a href="#">View</a></li>-->
                      <!--<li><a href="#">Generate QR</a></li>-->
                     <!-- <li><a href="qr_code/delete/{{$row->id}}" onClick="return confirm('Are you sure?');">Delete</a></li>-->
                      <?php if($row->is_active==1){?>
                      <li><a id="reganaretqrcode" href="{{'QrcodeReganaret/'.$row->id}}">Re-Generate</a></li>
                    <?php }else{ }?>

                    </ul>
                  </div></td>
              </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
              jQuery('#dereganaretqrcode').on('click',function(e){

                 if (confirm('You should have confirmed Deactive QR Code')) {
                      console.log('Deactive');
                  } else {
                        e.preventDefault();
                         console.log('sorry');
                  }
              });

              jQuery('#ganaretqrcode').on('click',function(e){

                 if (confirm('You should have confirmed Active QR Code')) {
                      console.log('Deactive');
                  } else {
                        e.preventDefault();
                         console.log('sorry');
                  }
              });
                jQuery('#reganaretqrcode').on('click',function(e){

                 if (confirm('You are sure to confirm Re-Generate QR Code')) {
                      console.log('Deactive');
                  } else {
                        e.preventDefault();
                         console.log('sorry');
                  }
              });
              
           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_Code').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get('getrocodes',{
                
                rocode:jQuery('#sel1').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_Code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_Code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#Vehicle_Reg_no').html(opt);
               });
           },

     }
     vil.init();
  });
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>


</body>
</html>