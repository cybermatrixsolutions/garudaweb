<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- CSS-->
<!-- Font-icon css-->
<style type="text/css"  media="print">
  

    @page 
    {
         size: A7;
 
}

</style>
<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>
<style type="text/css">
@media print {
    #myPrntbtn {
        display :  none;
    }
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 4px !important;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: transparent;
}

  .table{
    height: 293px!important;
    width:396px!important;
    margin: 0 auto;
    margin-top: 60px;
    position: relative;
    border:1px solid #ccc !important;
    border-bottom:1px solid #ccc !important;
  }
  h1,h2,h3,h4,h5,h6{
    margin: 0;
    padding:0;
  }
  .pos{
    position: relative;
    left: -15px;
    z-index: -1;
  }
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top:transparent;
}
p{
  margin: 0;
  padding: 0;

}
.qr{
  position: relative;
  top: -20px;
}
span{
  font-size: 13px;
}

</style>
</head>
<body>

<table class="table">
  <thead>
    <tr>
        <td><img src="{{URL::asset('')}}{{$Principal->image}}" onerror="this.src='../images/logo/garruda.jpg';" alt="garruda" title="garruda" width="70" height="50px" ></td>
        <td><img src="{{URL::asset('')}}{{$detail->images}}" onerror="this.src='../images/logo/garruda.jpg';" alt="garruda" title="garruda" width="70" height="50px"  class="pull-right"></td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td colspan="2">
        <h4 style="text-align: center">@if(!empty($detail->pump_name)){{$detail->pump_name}}@endif</h4>
        <h5 style="text-align: center">{{$detail->pump_addetail}},{{$CityModel->name}},{{$StateManagementModel->name}},{{$MastorRO->pin_code}} </h5>
      </td>
    </tr>
    <tr>
      <td width="35%" class="pos">
                                    <div class="qr">{!! QrCode::size(160)->generate("$Customer->qrcode") !!}
                                     <p  style="text-align:center; margin-top:-15px; font-size:9px;">Issued as Date:</p>
                  
  <p style="text-align:center; font-size:12px;">{{Carbon\Carbon::parse($detail->updated_at)->format('d/m/Y')}}</p></div>
      </td>

      <td  width="65%" style="text-align: right;">
         <span>Issued To:</span>

                  <p  style="font-size:12px;"><b>{{$detail->company_name}}</b></p>
                  <p  style="font-size:10px;"><b>{{$CityModel->name}} {{$StateManagementModel->name}}</b></p>
                   <span>Department:</span>
                    <p><b>{{$detail->department}}</b></p>        
                      <span>Vehicle No:</span>
                    <p><b>{{$detail->Vehicle_Reg_no}}</b></p> 
                    <p style="font-size:10px;"><b>{{$Vechilm->getmake->name}} {{$Vechilm->getmmodel->name}}</b></p>
                    
 
      </td>
     
    </tr>
  </tbody>
<tfoot>
    <tr class="qr">
       <td colspan="2"><p style="text-align: center; margin-top:-26px; font-size: 14px;"> Customer Care No:{{$detail->customer_care}}</p></td>
    </tr>
 </tfoot>
</table><br><br>
 <center><input type="button"  id="myPrntbtn" class="btn btn-success site-btn" onclick="window.print();" value="Print" />
<script src="js/bootstrap.min.js"></script>
</center> 

<script type="text/javascript">
        function PrintDiv() {

            var contents = document.getElementById("qrcodeprintContents").innerHTML;

            var frame1 = document.createElement('iframe');
            frame1.name = "frame1";
            frame1.style.position = "absolute";
            frame1.style.top = "-1000000px";
            document.body.appendChild(frame1);
            var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
            frameDoc.document.open();
            frameDoc.document.write('<html><head><title>QR Print</title>');
            frameDoc.document.write('</head><body>');
            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                document.body.removeChild(frame1);
            }, 500);
            return false;
        }
    </script>

</body>
</html>