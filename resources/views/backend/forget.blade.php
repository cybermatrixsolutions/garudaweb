<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link href="css/login_style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body>
<!--
    you can substitue the span of reauth email for a input with the email and
    include the remember me checkbox
    -->
<div class="container">
  <div class="card card-container"> 
    <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> --> 
   <div class="circle"></div>
    
    <form class="form-signin">
      <span id="reauth-email" class="reauth-email"></span>
      <input type="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus>
      
      
      <button class="btn btn-lg btn-success btn-block btn-signin" type="submit">Submit</button>
    </form>
      <a href="login.html" class="forgot-password">Log In</a> </div>
    <!-- /form --> 
     </div>
    
  <!-- /card-container --> 
</div>
<!-- /container --> 
<!-- Javascripts--> 
<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script>
</body>
</html>