<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
   @include('backend-includes.header')

        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Nozzel Reading </h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          
         
          <li><a href="#">Nozzel Reading </a></li>
        </ul>
      </div>
    </div>
     <div class="">
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
      
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
             <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
             <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Customer Credit Limit</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal"  action="{{'credit_limitaa'}}" enctype="multipart/form-data" method="post">
                           {{ csrf_field() }}
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Pedestel Number</label>
                          <div class="col-sm-8">
                            <select class="form-control" required="required" id="pedestalnum" name="Ro_code">
                               @foreach($getPedestals as $row) 
                              <option value="{{$row->id}}">{{$row->Pedestal_Number}}</option>
                              
                               @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Nozzel Number</label>
                          <div class="col-sm-8">
                             <select class="form-control" required="required" id="pedestalnum" name="Customer_Code">
                             
                            </select>
                          </div>
                        </div>
                       <div class="form-group">
                          <label class="control-label col-sm-4" for="email">CMR</label>
                          <div class="col-sm-8">
                              <input type="text" required="required" class="form-control" placeholder="Limit Value" name="Limit_Value" />
                          
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Reading By</label>
                          <div class="col-sm-8">
                            <select class="form-control" required="required" id="sel1" name="Ro_code">
                               @foreach($personnel as $rows) 
                              <option value="{{$row->Pedestal_Number}}">{{$rows->Personnel_Name}}</option>
                              
                               @endforeach
                            </select>
                          </div>
                        </div>

                          <!-- <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Limit period to date</label>
                          <div class="col-sm-8">
                        <input type="text" required="required" class="form-control datepicker" placeholder="Limit Value" name="Limit_Period_end" />
                          </div>
                        </div> -->
                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <input type="submit" value="submit" class="btn btn-default">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              
              <tr>
                 <th>S.No.</th>
               <!--  <th>Pump Legal Name</th> -->
                <th>Nozzle No</th>
                <th>Nozzle OMR</th>
                <th>Nozzle CMR</th>
                <th>Reading Date</th>
                <th>Reading by</th>
            </thead>
            <tbody>
              <?php $i=0;?>
               @foreach($data as $listreading)
               <?php $i++;?>
              <tr>
                 <td>{{$i}}</td>
                <!-- <td scope="row">{{$listreading->pump_name}}</td> -->
                <td>{{$listreading->Nozzle_No}}</td>
                 <td>{{$listreading->Nozzle_Start}}</td>
                 <td>{{$listreading->Nozzle_End}}</td>
                 <td> @if($listreading->Reading_Date != null){{Carbon\Carbon::parse($listreading->Reading_Date)->format('d/m/Y')}} @endif</td>
                  <!-- <td>{{$listreading->Reading_Date}}</td> -->
                  <td>{{$listreading->Personnel_Name}}</td>
          
              </tr>
              @endforeach
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script  src="js/index.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
</script>

</body>
</html>