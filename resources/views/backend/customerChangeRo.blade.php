@extends('backend-includes.app')

@section('content')
 <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Change Ro  </h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#"> Change Ro  </a></li>
        </ul>
      </div>
    </div>
    <div class="row" style="margin-top:50px">
       <div class="">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

      <form class="form-horizontal" action="{{'createSession'}}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
        <label class="control-label col-sm-3" for="email">Outlet Name</label>
        <div class="col-sm-9">
          <select class="form-control" required="required" name="RO_code" id="RO_code">
             @if(isset($customer_ro))
                @foreach($customer_ro as $ro)
                    <option @if(Session::get('ro')==$ro->RO_code) selected @endif name="company_id" value="{{$ro->RO_code}}">{{$ro->pump_name}}_ @if($ro->getro!=null && $ro->getro->getcity!=null){{$ro->getro->getcity->name}} @endif</option>
                @endforeach
              @endif
          </select>
        </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-default">Submit</button>
          </div>
        </div>
      </form>
    </div> 
  </div>  
@endsection