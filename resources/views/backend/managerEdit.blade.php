<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Manager Update</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('manager')}}">Manager Management </a></li>
        </ul>
      </div>
    </div>
  
  
                           <div class=""> @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
      
    </div>
  
  
  
  
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row">
          
         
          
                      <form class="form-horizontal" action="{{url('manager/edit')}}/{{$roManager->id}}" enctype="multipart/form-data" method="post">
            
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">RO Code</label>
                          <div class="col-sm-9">
                            <select name="RO_code" id="sel1" class="form-control">
                              @foreach($data1 as $row) 
                              <option value="{{$row->RO_code}}" @if($roManager->RO_code == $row->RO_code) selected @endif >{{$row->RO_code}}</option>
                               @endforeach
                            </select>
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Manager Name</label>
                          <div class="col-sm-9">
                            <input type="text" required="required" class="form-control" id="" name="manager_name" value="{{$roManager->manager_name}}" placeholder="Pump Legal Name">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Email</label>
                          <div class="col-sm-9">
                            <input type="email" required="required" class="form-control" id="email" value="{{$roManager->manager_EMAIL}}"  name="manager_EMAIL" placeholder="Email">
                          </div>
                        </div>
                         
                       
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Phone Number</label>
                          <div class="col-sm-9">
                            <input type="text"  class="form-control" id="" name="manager_phone" value="{{$roManager->manager_phone}}" placeholder="Phone Number">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Mobile</label>
                          <div class="col-sm-9">
                            <input type="text" required="required" pattern="[789][0-9]{9}" class="form-control" name="manager_mobile" id="" value="{{$roManager->manager_mobile}}" placeholder="Mobile">
                          </div>
                        </div>

                       <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">IMEI No</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="manager_IMEI" value="{{$roManager->manager_IMEI}}" id="" placeholder="IMEI No">
                          </div>
                        </div> -->
                       

                        <div class="form-group">
            
                          <div class="col-sm-offset-3 col-sm-9">
                           <!-- <button type="submit" class="btn btn-default">Submit</button>-->
              <center><a href="{{url('manager')}}" class="btn btn-primary">Back</a><input type="submit" id="success-btn" class="btn btn-primary site-btn" value="Submit"></center>
                          </div>
                        </div>
           
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>
  
  
  </div>
</div>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>