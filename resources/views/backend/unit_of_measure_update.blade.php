<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  

  <!-- Side-Nav-->

  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Unit of Measure Update</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('unit_of_measure')}}">Unit of Measure </a></li>
        </ul>
      </div>
    </div>
	
	
	
	                        <div class="">
							       <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                            </div>
	
	
	
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
          <!--  <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            <div class="row" style="margin-top:-15px !important;">
          
          @foreach($unitMeasureData as $row)
          <form class="form-horizontal" action="{{url('unit_of_measure')}}/{{$row->id}}" method="post">
          
                     
            <input type="hidden" name="_token" value="{{ csrf_token()}}"/>  
                         <div class="form-group">
                          <label class="control-label col-sm-4" for="pwd">Simple / Compound</label>
                          <div class="col-sm-8">
                           <select class="form-control" id="type"  name="Simple_Compound" required/>
                              <option @if($row->Simple_Compound==$row->id) selected @endif value="{{$row->Simple_Compound}}">{{$row->Simple_Compound}}</option>
                             <!--  <option value="Compound">Compound</option> -->
                             
                            </select>  
                          </div>
                        </div>
                       <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Unit Symbol <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control checkunique"  name="Unit_Symbol" value="{{$row->Unit_Symbol}}" placeholder="Unit Symbol" required data-table="tbl_unit_of_measure" data-colum="Unit_Symbol" data-mess="Unit Symbol Already Exits"/>
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Unit Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control checkunique" name="Unit_Name" value="{{$row->Unit_Name}}" placeholder="Unit Name" required data-table="tbl_unit_of_measure" data-colum="Unit_Name" data-mess="Unit Name Already Exits" data-id="{{$row->id}}"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">No. of Decimals <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                            <input type="text"  min="0" class="form-control number" id="No_of_Decimals" name="No_of_Decimals"  data-v="{{$row->No_of_Decimals}}" value="{{$row->No_of_Decimals}}" placeholder="Unit Name" required/>
                            
                          </div>
                        </div>
                        
                         <!--  <div class="form-group row_dim">
                          <label class="control-label col-sm-4" for="pwd">Select First Unit</label>
                          <div class="col-sm-7">

                           <select class="form-control" id="type2"  name="first_unit"  required/>
                              <option value="">Select First unit</option>
                           @foreach($getUnitMeasure as $row) 
                        
                              <option value="{{$row->Unit_Symbol}}">{{$row->Unit_Symbol}}</option>
                            
                             @endforeach
                            </select>  
                          </div>
                        </div> -->
                       <!--  <div class="form-group row_dim">
                          <label class="control-label col-sm-4" for="pwd">Select Second Unit</label>
                          <div class="col-sm-7">
                           <select class="form-control" id="type1"  name="second_unit" required/>
                            <option value="">Select Second unit</option>
                             @foreach($getUnitMeasure as $row) 
                            
                              <option value="{{$row->Unit_Symbol}}">{{$row->Unit_Symbol}}</option>
                            
                             @endforeach
                              
                             
                            </select>  
                          </div>
                        </div> -->
                        <!--  
                         <div class="form-group row_dim">
                          <label class="control-label col-sm-4" for="pwd">Convetions</label>
                          <div class="col-sm-8">
                          <div class="row">
                            <div class="col-sm-5">
                             <input type="number" class="form-control checkunique" 
                               name="conv_1" value="{{$row->conv_1}}" required/> 
                            </div>
                            <div class="col-sm-5">
                             <input type="number" class="form-control checkunique" 
                               name="conv_2" placeholder="Unit Symbol" value="{{$row->conv_2}}" required 
                               /> 
                            </div>
                          </div> 
                          </div>
                        </div> -->
                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <!--<button type="submit" class="btn btn-default">Submit</button>-->
              <center><input id="success-btn" type="submit" class="btn btn-primary site-btn submit-button" value="Submit"></center>
                          </div>
                        </div>
                      </form> 
            @endforeach
                    </div>
            <!-- Modal -->
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script type="text/javascript">
  $(function() {
     $('.row_dim').hide(); 
    $('#type').change(function(){
        if($('#type').val() == 'Compound') {
            $('.row_dim').show(); 
             //jQuery('#utint').css("display","none")
        } else {
            //$('#utint').show();
            jQuery('.row_dim').css("display","none")
        } 
    });
    
});
  $(".number").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
</script>
<script type="text/javascript">
jQuery(function(){
   jQuery('#type1').on('change',function(){
        var a = document.getElementById("type2");
    var b = document.getElementById("type1");

    if (a.options[a.selectedIndex].value == b.options[b.selectedIndex].value) {
        alert("Please Choose Select unit symble");
         jQuery('.site-btn').attr('disabled',true);
    } else{
       jQuery('.site-btn').attr('disabled',false);


    }
   }); 
});
$('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
</script>
<script>
  jQuery(function(){
     var url="{{url('check_Pedestal_Number_unique')}}";
     var vil={
           init:function(){
             vil.selectRo();

              jQuery('#No_of_Decimals').on('keyup',function(){
              var daarray=[0,1,2,3,4];
                 var v=jQuery(this).val();
                 
                 if(jQuery.trim(v)!='')
                 if(jQuery.inArray(parseInt(v),daarray)!=-1){
                  console.log('tr');
                    jQuery('#success-btn').attr('disabled',false);
                 }else{
                    console.log('fals');
                  alert('Please Enter value between 0 to 4');
                  jQuery(this).val(jQuery(this).data('v'));
                  //jQuery('#success-btn').attr('disabled',true);
                 }

             });
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                   var colum=jQuery(this).data('colum');
                   var mess=jQuery(this).data('mess');

                   id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mess);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>


<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>