<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
<style>
  .slimScrollDiv {
   height:834px!important;
   }

</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Update Petrol Diesel Price Management
</h1>
      </div>
      <div>
        <ul class="breadcrumb">
       <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="{{url('/petDmaster')}}">Petrol Diesel Price Management
</a></li>
        </ul>
      </div>
    </div>
                          <div class="">
                     <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                            </div>  
                             @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row">
                      <form class="form-horizontal" action="{{url('PDmasteredit/edit/')}}/{{$RoPDMaster->id}}" enctype="multipart/form-data" method="post">
                         {{ csrf_field() }}
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">RO Code</label>
                          <div class="col-sm-9">
                            <select class="form-control" id="RO_code" name="RO_code">
                             @foreach($data1 as $row) 
                              <option value="{{$row->RO_code}}">{{$row->RO_code}}</option>
                              
                               @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Petrol Diesel Type</label>
                          <div class="col-sm-9">
                          <select class="form-control" id="petroldes" name="petroldiesel_type">
                            @foreach($data2 as $row) 
                              <option @if($RoPDMaster->id==$row->RO_code) selected @endif value="{{$row->id}}">{{$row->Petrol_Diesel_Type}}</option>
                              
                               @endforeach
                          </select>
                          </div>
                        </div>
                         
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Price</label>
                          <div class="col-sm-9">
                            <input type=number step=0.01 class="form-control" id="" name="Price" value="{{$RoPDMaster->Price}}" placeholder="Price" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Price Date</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control datepicker" id="" name="Price_Date" value="{{$RoPDMaster->Price_Date}}"    placeholder="Price Date" required>
                          </div>
                        </div>
            
                          
                        
                         <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
            
          </div>

        </div>
      </div>
    </div>
   
  </div>
</div>

<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script type="text/javascript">
  $(function () {
  $(".datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  })
});
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
<script type="text/javascript">
 jQuery(function(){
     var url ="{{url('getpetmaster')}}";
     var vil={
           init:function(){
             vil.selectRo();
              //vil.getcom();

           },
           selectRo:function(){
            jQuery('#RO_code').on('change',function(){
                  vil.getcom();
              });
           
           
           },
           getcom:function(){
             
               jQuery.get(url,{
                
                rocode:jQuery('#RO_code').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#petroldes').html(opt);

                   
               });
           },
          

     }
     vil.init();
  });
</script>
</body>
</html>