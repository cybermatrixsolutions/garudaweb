<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
       <h1><i class="fa fa-dashboard"></i>&nbsp;Owner Update</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/owner')}}">Owner List</a></li>
        </ul>
      </div>
    </div>
	
	 <div class="">
      
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
      
    </div>
	
	
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row">
          
         
          
                      <form class="form-horizontal" action="{{url('owner_update')}}/{{$owner_mangement_data->id}}" enctype="multipart/form-data" method="post">
            
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">RO Code</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="RO_code" id="sel1">
                              
                                                    @foreach($datas as $ro_master)
                                                        <option name="company_id" value="{{$ro_master->RO_code}}"
                                                                @if($owner_mangement_data->RO_code == $ro_master->RO_code) selected @endif>{{$ro_master->RO_code}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Owner Name</label>
                          <div class="col-sm-9">
                            <input type="text" required="required" class="form-control" id="" name="owner_name" value="{{$owner_mangement_data->owner_name}}" placeholder="Owner Name ">
                          </div>
                        </div>

                         <div class="form-group">


                          <label class="control-label col-sm-3" for="pwd">Email</label>
                          <div class="col-sm-9">
                            <input type="email" required="required" class="form-control checkunique" name="owner_Email" data-table="tbl_ro_owner_master" data-colum="owner_Email" data-id="{{$owner_mangement_data->id}}" value="{{$owner_mangement_data->owner_Email}}"  id="owner_Email" placeholder="Email">
                          </div>
                        </div>
                         
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Phone Number</label>
                          <div class="col-sm-9">
                            <input type="Number" class="form-control" id="" name="owner_phone" value="{{$owner_mangement_data->owner_phone}}" placeholder="Phone Number">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Mobile</label>
                          <div class="col-sm-9">
                            <input type="text" data-table="tbl_ro_owner_master" data-colum="owner_mobile" data-id="{{$owner_mangement_data->id}}" required="required" pattern="[789][0-9]{9}" class="form-control checkunique" name="owner_mobile" id="" value="{{$owner_mangement_data->owner_mobile}}" placeholder="Mobile" max="10">
                          </div>
                        </div>
                       <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">IMEI No</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="owner_IMEI" value="{{$owner_mangement_data->owner_IMEI}}" id="" placeholder="IMEI No">
                          </div>
                        </div> -->
                        
                        <div class="form-group">

            
                          <div class="col-sm-offset-3 col-sm-9">
                           <!-- <button type="submit" class="btn btn-default">Submit</button>-->
              <center><a href="{{url('owner')}}" class="btn btn-primary">Back</a><input type="submit" id="success-btn" class="btn btn-primary site-btn submit-button" value="Submit" disabled=""></center>
                          </div>
                        </div>
           
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>
	
	
  </div>
</div>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">
 jQuery(function(){
     var url="{{url('check_Pedestal_Number_unique')}}";
     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                   var colum=jQuery(this).data('colum');
                   id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert('Pedestal Number already exists Enter Other!');
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>
</body>
</html>