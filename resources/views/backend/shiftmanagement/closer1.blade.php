@extends('backend-includes.app')

@section('content')
<!--     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/1.4.0/css/bootstrap.min.css"> -->
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Nozzle CMR</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#"> Nozzle CMR</a></li>
        </ul>
      </div>

    </div>
    <div  style="margin-top: 41px;">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

   
    <!--end add box-->
    <!--list contant-->
    <div class="row">
      <div class="col-md-12 card">
        <div class="table-responsive">
         <div class="col-md-12">
         @if(Auth::user()->user_type==3 && $shiftmanager->count()>0)
          <table>
            <form class="form-horizontal" id="form3" action="{{route('shiftCloder')}}" method="get">
              {{ csrf_field() }}
              <tr>
                <td>Select Shift Manager   </td>
                 <td>&nbsp;&nbsp; </td>
                <td> 
                     <select class="form-control" id="shift_manager" name="shift_manager"  required="required">
                        @foreach($shiftmanager as $shiftmanagers)
                        <option @if($id==$shiftmanagers->id) selected @endif value="{{$shiftmanagers->id}}">{{$shiftmanagers->Personnel_Name}}</option>
                        @endforeach
                      </select>

                </td>
                 <td>&nbsp;&nbsp;</td>
                <td><input type="submit" class="btn btn-primary" value="Go"></td>
              </tr>
             </form>
          </table>
          @else
         <center><h4>Please Start Shift  </h4> </center>
          @endif

            @if($Shift!=null)

            <form class="form-horizontal" id="form1" action="{{route('shiftCloder')}}/close" method="post"  onsubmit="return confirm('Do you want to Continue?');">
          {{ csrf_field() }}
          <?php $str='';?>

              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd" style="padding-right: 17px;margin-left: -7%;">Created Date: </label>
                    <div class="col-sm-8">
                       <label class="control-label col-sm-12" for="pwd" style="font-weight: normal;margin-left: -78%;"> {{date_format($Shift->created_at,"d-m-Y H:i:s")}}</label>
                    </div>
               </div>
             <div class="form-group">
                <label class="control-label col-sm-2" for="pwd" style="margin-left: -8%;">Closer Date: </label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="datetimepicker1"  required="required" name="closer_date" value="{{date_format($Shift->created_at,'d-m-Y')}}"  placeholder="Closer Date"/> 
                    </div>
               </div>
          <input type="hidden" name="Shift" value="{{$Shift->id}}">
          <input type="hidden" name="Reading_by" value="{{$shiftmanagers->id}}">
             <input type="hidden" name="flexishiftstatus" value="{{$flexishiftstatus}}"></input>


       @foreach($Shift->getPadestal as $padestal)

       <?php  $getAllNozel=$padestal->getNozzle->where('is_active',1)->where('RO_code',$padestal->RO_code)->sortBy('Nozzle_Number'); 

       //dd($getAllNozel)?>


       @if(!$getAllNozel->isEmpty())

         <h4>Padestal No. : {{$padestal->Pedestal_Number}}</h4>
         <table width="100%" class="td-parent">
          <input type="hidden" name="ro_code" id="no_value" value="{{$padestal->RO_code}}">
          <tr>
            <th>Nozzle No.</th> 
            <th>Fuel Type</th>
            <th>OMR</th>
            <th>CMR</th>
             <th>Vol(Out)</th>
            <th>Test(Vol)</th>
            <th>Net(Vol)</th>
          </tr>
          <?php $str.=','.$padestal->id; 

         // dd($padestal->getNozzle->where('is_active',1)->where('RO_code',$padestal->RO_code)->sortBy('Nozzle_Number'))?>

          
          @foreach($getAllNozel as $getNozzle)

             <?php $nore=$getNozzle->getReading()->where('RO_code',$padestal->RO_code)->where('Nozzle_End',null)->first();

             


        $date_created1 = $Shift->created_at;
           
        $date_created = date_format($date_created1,"Y-m-d");
    

         $date = DB::table('tbl_pedestal_nozzle_master')
         ->join('tbl_item_price_list', 'tbl_item_price_list.item_id', 'tbl_pedestal_nozzle_master.fuel_type')
         ->select('tbl_item_price_list.price as price')
         
         ->whereDate('tbl_item_price_list.effective_date',$date_created)
         ->where('tbl_pedestal_nozzle_master.Nozzle_Number',$getNozzle->Nozzle_Number)
         ->value('price');
  
            ?>
             
            <tr class="parentcmr">
            <td>
              <input name="{{trim($padestal->id)}}_price_{{trim($getNozzle->id)}}"  type="hidden" value="{{$date}}">

              <input name="{{trim($padestal->id)}}_Nozzle_No[]"  type="hidden" value="{{$getNozzle->id}}">{{$getNozzle->Nozzle_Number}}</td> 

           
            
            <td>{{$getNozzle->getItem->Item_Name}}</td>
            <!--<td>{{$getNozzle->Created_date}}</td>-->
            <td><input name="{{trim($padestal->id)}}_Nozzle_start_{{trim($getNozzle->id)}}" disabled type="text" value="@if($nore['Nozzle_Start']=='') {{$getNozzle->Opening_Reading}}@else{{$nore['Nozzle_Start']}}@endif"></td>
             <?php  $chet = $nore['Nozzle_Start']-$nore['Nozzle_Start']?>

            <td><input name="{{trim($padestal->id)}}_Nozzle_End_{{trim($getNozzle->id)}}" class="cmr_reading" type="text" value="@if($nore['Nozzle_Start']==''){{$getNozzle->Opening_Reading}}@else{{$nore['Nozzle_Start']}}@endif" data-start="@if($nore['Nozzle_Start']=='') {{$getNozzle->Opening_Reading}} @else{{$nore['Nozzle_Start']}}@endif"></td>

            <td><input type="text" class="valueOutcheck" disabled name="valueOutcheck" value="{{$chet}}"></td>
            <td><input  class="r_test" type="text" name="{{trim($padestal->id)}}_test_{{trim($getNozzle->id)}}" value="0"></td>

            <td><input readonly class="r_reading" type="text" name="{{trim($padestal->id)}}_reading_{{trim($getNozzle->id)}}" value="0"></td>

            </tr>
           @endforeach
         </table>

         @endif
        @endforeach
        <input type="hidden" name="padestal" value="{{$str}}">
  
        @foreach($Shift->getTanks as $tank)
        <?php

             $totalesInwards=DB::table('tank_tankinwart')->where('tank_id',$tank->id)->where('status',1)->get();

             $TankReading=\App\TankReading::where('Tank_code',$tank->id)->latest()->first();
             
             $tank_stack=0;
             
             if($TankReading!=null){
                $tank_stack=$TankReading->value;
             }

             foreach ($totalesInwards as $totalesInward) {
               $tank_stack=$totalesInward->value+$tank_stack;
             }


        $dipChart = $tank->getDipchart;
        ?>
          <div class="item-tank">
            <h4>Tank Number. : {{$tank->Tank_Number}}</h4>
            <table width="100%" class="td-parent" data-stack="{{$tank_stack}}">
              <tr>
                <th width="150px">Capacity</th> 
                <th width="150px">Unit of Measure</th>
                <th>Fuel Type</th>
                <th>Dip Reading
                  <br/>@if($dipChart->lenght_type == 'cm')<span> CM -  </span>@endif<span style="float:right;padding-right: 15px;"> MM  </span></th>

                <th width="100px">Volume</th>
              </tr>
              <tr  >
                <td><input type="text" readonly class="form-control numeriDesimal"  placeholder="capacity" value="{{$tank->Capacity}}"/></td>
                <td><input type="text" readonly class="form-control numeriDesimal"  placeholder="reading" value="{{$tank->getunitmesure['Unit_Name']}}"></td>
                <td><input type="text" readonly class="form-control numeriDesimal"  placeholder="reading" value="{{$tank->getfueltypes['Item_Name']}}"></td>
                <td width="206px;" >
                  <div>
                    <span style="float:left"><input type="text" name="reading_cm[]" class="form-control numeriDesimal @if($dipChart->lenght_type == 'cm') reading_cm @else reading @endif"  placeholder="CM" value="" style="width:100px;"></span>
                    <span style="float:left">-</span>
                    <span style="float:left">
                    @if($dipChart->lenght_type == 'cm')
                      <select class="form-control reading" name="reading[]" style="width:100px;">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                      </select>
                    @else
                      <input type="text"  name="reading[]" class="form-control numeriDesimal mm_mm_val reading" placeholder="MM" style="width:100px;">
                    @endif
                    </span>
                    
                  </div>
                  
                </td>
                <td> <input type="text" readonly name="sumvalue[]" value="0" class="form-control sumvalue"  placeholder=""></td>
              </tr>
            <input type="hidden" class="tank" name="tank[]" value="{{$tank->id}}"/>
            <input type="hidden" class="capacity" name="capacity[]" value="{{$tank->Capacity}}"/>
            <input type="hidden" class="unit_measure" name="unit_measure[]" value="{{$tank->Unit_of_Measure}}"/>
            <input type="hidden" class="fuel_type" name="fuel_type[]" value="{{$tank->fuel_type }}"/>
            
            <input type="hidden" class="diptype" name="diptype[]"  value="{{$dipChart->lenght_type}}"/>
            <input type="hidden" class="dipid" name="dipid[]"  value="{{$dipChart->id}}"/>
            </table>
            
          
          </div>  
          
        @endforeach 

        
                <div class="form-group">
                  <label class="control-label col-sm-8" ></label>
                            <div class="col-sm-3">
                             <input type="button" id="success-btn" class="btn btn-primary site-btn submit-button  pull-right" value="Submit">
                            </div>
                </div>
        
             </form>
             @else
                @if(Auth::user()->user_type!=3)
                  <p> Start Shift First</p>
               @endif
             @endif

            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    jQuery(function(){
      var current;
      var calUrl="{{url('gettankdipchartcalculation')}}";
      var app={
        init:function(){
           app.reading();
           app.fromDate();
          jQuery('.cmr_reading').on('keyup',function(){
              var start=parseFloat(jQuery(this).data('start'));
              var val=parseFloat(jQuery(this).val());
              jQuery(this).data('diff',val-start);
              jQuery(this).parents('.parentcmr').find('.valueOutcheck').val((val-start).toFixed(3));
              var test= parseFloat(jQuery(this).parents('.parentcmr').find('.r_test').val());
              if(test!=NaN && val!=NaN && start!=NaN)
                jQuery(this).parents('.parentcmr').find('.r_reading').val((val-start-test).toFixed(3));
          });

          jQuery('.r_test').on('keyup',function(){
              var start=parseFloat(jQuery(this).parents('.parentcmr').find('.cmr_reading').data('start'));
              var val=parseFloat(jQuery(this).parents('.parentcmr').find('.cmr_reading').val());
              var test= parseFloat(jQuery(this).val());
  
               var dif=val-start;
              if(test>dif){
                alert('Test Value Allways Less than Diff OMR and CMR');
                jQuery(this).val(0);
                jQuery(this).parents('.parentcmr').find('.r_reading').val((val-start).toFixed(3));
                return false;
              }

              if(test!=NaN && val!=NaN && start!=NaN)
                jQuery(this).parents('.parentcmr').find('.r_reading').val((val-start-test).toFixed(3));
          });

          jQuery('#success-btn').on('click',function(){
              var flag=false;
              var no_value = jQuery('#no_value').val();
              if (no_value == null) 
              {
                jQuery('#form1').submit();
              }
              jQuery('.cmr_reading').each(function(){
                  var start=jQuery(this).data('start');
                  var val=jQuery(this).val();
                   
           
                  if(parseFloat(val)>=parseFloat(start)){
                    flag=true;

                  }else{
                    flag=false;

                    alert('Nozzle CMR is Always Greater of Nozzle OMR');
                    return false;
                  }

              });

              jQuery('.stack-error').each(function(){
              
                 flag=false;
              });
            
            if(flag)
              jQuery('#form1').submit();
          });

          jQuery('#shift_manager').on('change',function(){
              jQuery('#form3').submit();
          });
      
      
      // For tanks reading
      
      
      
        },
         fromDate:function(){
                
               $('#datetimepicker1').datetimepicker({
                  defaultDate:new Date(),
                  format:'DD/MM/YYYY ',
                });
              },
        reading:function(){
              jQuery(document).on('change keyup','.reading',function(){

                  current=jQuery(this).parents('.td-parent');
                  cm=current.find('.reading_cm').val();
                  mm=jQuery(this).val();
                  app.getDipCal(cm,mm);
              });
              jQuery(document).on('keyup','.reading_cm',function(){
                  current=jQuery(this).parents('.td-parent');
                  cm=jQuery(this).val();
                  mm=current.find('.reading').val();
                  app.getDipCal(cm,mm);
              });
        },
        getDipCal:function(cm,mm){

                diptype=current.find('.diptype').val();
                dipid=current.find('.dipid').val();
               
               jQuery.get(calUrl,{
                cm:cm,
                mm:mm,
                diptype:diptype,
                dipid:dipid,
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                   if(data==0){
                    jQuery('#success-btn').attr('disabled',true);
                    current.find('.sumvalue').css('border-color','#ff0000');
                    current.find('.reading_cm').css('border-color','#ff0000');
                    current.find('.reading').css('border-color','#ff0000');
                    current.find('.reading').addClass('stack-error');
                   }else{

                    if(current.data('stack')<data){
                       alert('Incorrect value');
                       jQuery('#success-btn').attr('disabled',true);
                        current.find('.sumvalue').css('border-color','#ff0000');
                        current.find('.reading_cm').css('border-color','#ff0000');
                        current.find('.reading').css('border-color','#ff0000');
                        current.find('.reading').addClass('stack-error');
                    }else{
                      current.find('.sumvalue').val(data);
                      jQuery('#success-btn').attr('disabled',false);
                        current.find('.sumvalue').css('border-color','');
                        current.find('.reading_cm').css('border-color','');
                        current.find('.reading').css('border-color','');
                        current.find('.reading').removeClass('stack-error');
                    }
                   }

                   current.find('.sumvalue').val(data);
               });
           },
      }

      app.init();
    });
</script>

@endsection
@section('style')
<style>
  .card table tr td{
    padding-bottom: 10px !important;
  }
  .card {
    padding-top: 15px !important;
  }
</style>
@endsection