@extends('backend-includes.app')

@section('content')
<!--     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/1.4.0/css/bootstrap.min.css"> -->
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Previous CMR</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#"> Previous CMR</a></li>
        </ul>
      </div>

    </div>
    <div  style="margin-top: 41px;">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


    <!--end add box-->
    <!--list contant-->
    <div class="row">
      <div class="col-md-12 card">
        <div class="table-responsive">
         <div class="col-md-12">



   

            <form class="form-horizontal" id="form1" action="{{route('previouscmr-update')}}" method="post"  onsubmit="return confirm('Do you want to Continue?');">
          {{ csrf_field() }}
        

        @if($reading)

        <?php    $readings=collect($reading)->groupBy('Pedestal_Number');

         ?>

          @foreach($readings as $key => $readingdata1)


         
         <h4>Padestal No. : {{$key}}</h4>
         <table width="100%" class="td-parent">
          
          <tr>
            <th>Nozzle No.</th> 
            <th style="text-align: center">Fuel Type</th>
            <th>OMR</th>
            <th>CMR</th>
             <th>Vol(Out)</th>
            <th>Test(Vol)</th>
            <th>Net(Vol)</th>
        </tr>
           @foreach($readingdata1 as $key => $readingdata)
                 
        <tr class="parentcmr">
      
           

            <td >{{$readingdata->Nozzle_Number}}</td>
            <td>{{$readingdata->item_name}}</td>


            <input name="Nozzle_Number[]" type="hidden" value="{{$readingdata->Nozzle_Number}}">
            <input name="item_name[]" type="hidden" value="{{$readingdata->item_name}}">
            <input name="id[]" type="hidden" value="{{$readingdata->id}}">

            <td><input name="Nozzle_start[]" disabled type="text" value="{{$readingdata->Nozzle_Start}}"></td>
             <?php  $chet = $readingdata->Nozzle_End-$readingdata->Nozzle_Start?>

            <td><input name="Nozzle_End[]" class="cmr_reading" type="text" value="{{$readingdata->Nozzle_End}}" data-start="{{ $readingdata->Nozzle_Start}}" ></td>

            <td><input type="text" class="valueOutcheck" disabled name="valueOutcheck" value="{{bcadd($chet,0,3)}}"></td>
            <td><input  class="r_test" type="text" name="test[]" value="{{bcadd($readingdata->test,0,3)}}"></td>

            <td><input readonly class="r_reading" type="text" name="reading[]" value="0"></td>

            </tr>
             @endforeach
     </table>
 
       
        @endforeach

          @endif
             
                <div class="form-group">
                  <label class="control-label col-sm-8" ></label>
                            <div class="col-sm-3">
                             <input type="button" id="success-btn" class="btn btn-primary site-btn submit-button  pull-right" value="Submit">
                            </div>
                </div>
        
             </form>
           

            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
    jQuery(function(){
      var current;
      var calUrl="{{url('gettankdipchartcalculation')}}";
      var app={
        init:function(){
           app.reading();
           app.fromDate();
          jQuery('.cmr_reading').on('keyup',function(){
              var start=parseFloat(jQuery(this).data('start'));
              var val=parseFloat(jQuery(this).val());
              jQuery(this).data('diff',val-start);
              jQuery(this).parents('.parentcmr').find('.valueOutcheck').val((val-start).toFixed(3));
              var test= parseFloat(jQuery(this).parents('.parentcmr').find('.r_test').val());
              if(test!=NaN && val!=NaN && start!=NaN)
                jQuery(this).parents('.parentcmr').find('.r_reading').val((val-start-test).toFixed(3));
          });

          jQuery('.r_test').on('keyup',function(){
              var start=parseFloat(jQuery(this).parents('.parentcmr').find('.cmr_reading').data('start'));
              var val=parseFloat(jQuery(this).parents('.parentcmr').find('.cmr_reading').val());
              var test= parseFloat(jQuery(this).val());
  
               var dif=val-start;
              if(test>dif){
                alert('Test Value Allways Less than Diff OMR and CMR');
                jQuery(this).val(0);
                jQuery(this).parents('.parentcmr').find('.r_reading').val((val-start).toFixed(3));
                return false;
              }

              if(test!=NaN && val!=NaN && start!=NaN)
                jQuery(this).parents('.parentcmr').find('.r_reading').val((val-start-test).toFixed(3));
          });

          jQuery('#success-btn').on('click',function(){
              var flag=false;
              var no_value = jQuery('#no_value').val();
              if (no_value == null) 
              {
                jQuery('#form1').submit();
              }
              jQuery('.cmr_reading').each(function(){
                  var start=jQuery(this).data('start');
                  var val=jQuery(this).val();
                   
           
                  if(parseFloat(val)>=parseFloat(start)){
                    flag=true;

                  }else{
                    flag=false;

                    alert('Nozzle CMR is Always Greater of Nozzle OMR');
                    location.reload();
                  }

              });

              jQuery('.stack-error').each(function(){
              
                 flag=false;
              });
            
         
          });

        
      
      
      // For tanks reading
      
      
      
        },
         fromDate:function(){
                
               $('#datetimepicker1').datetimepicker({
                  defaultDate:new Date(),
                  format:'DD/MM/YYYY ',
                });
              },
        reading:function(){
              jQuery(document).on('change keyup','.reading',function(){

                  current=jQuery(this).parents('.td-parent');
                  cm=current.find('.reading_cm').val();
                  mm=jQuery(this).val();
                  app.getDipCal(cm,mm);
              });
              jQuery(document).on('keyup','.reading_cm',function(){
                  current=jQuery(this).parents('.td-parent');
                  cm=jQuery(this).val();
                  mm=current.find('.reading').val();
                  app.getDipCal(cm,mm);
              });
        },
        getDipCal:function(cm,mm){

                diptype=current.find('.diptype').val();
                dipid=current.find('.dipid').val();
               
               jQuery.get(calUrl,{
                cm:cm,
                mm:mm,
                diptype:diptype,
                dipid:dipid,
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                   if(data==0){
                    jQuery('#success-btn').attr('disabled',true);
                    current.find('.sumvalue').css('border-color','#ff0000');
                    current.find('.reading_cm').css('border-color','#ff0000');
                    current.find('.reading').css('border-color','#ff0000');
                    current.find('.reading').addClass('stack-error');
                   }else{

                    if(current.data('stack')<data){
                       alert('Incorrect value');
                       jQuery('#success-btn').attr('disabled',true);
                        current.find('.sumvalue').css('border-color','#ff0000');
                        current.find('.reading_cm').css('border-color','#ff0000');
                        current.find('.reading').css('border-color','#ff0000');
                        current.find('.reading').addClass('stack-error');
                    }else{
                      current.find('.sumvalue').val(data);
                      jQuery('#success-btn').attr('disabled',false);
                        current.find('.sumvalue').css('border-color','');
                        current.find('.reading_cm').css('border-color','');
                        current.find('.reading').css('border-color','');
                        current.find('.reading').removeClass('stack-error');
                    }
                   }

                   current.find('.sumvalue').val(data);
               });
           },
      }

      app.init();
    });
</script>

@endsection
@section('style')

@endsection