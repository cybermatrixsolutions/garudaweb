@extends('backend-includes.app')

@section('content')

<div class="content-wrapper">
  <div class="page-title">
    <div>
      <h1><i class="fa fa-dashboard"></i>&nbsp;Shift Settlement Confirm</h1>
    </div>
    <div>
      <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

        <li><a href="#">Shift Settlement Confirm</a></li>
      </ul>
    </div>

  </div>
  <div  style="margin-top: 41px;">
    <center>
      @if(Session::has('success'))
      <font style="color:red">{!!session('success')!!}</font>
      @endif
    </center>
  </div>  

  @if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif

  <div class="">
    <div class="col-md-10 col-md-offset-1 card" style="padding:10px!important;">
      <form class="form-horizontal" id="form2" action="{{route('settlementconfirm')}}" method="get">
        

        <input type="hidden" name="type" value="1">
              @if(Auth::user()->user_type==3)
                  <label>Select shift Manager</label>&nbsp;
                  <select style="width:20%; display:inline!important;" class="form-control" id="shift_manager" name="shift_manager"  required="required">
                     @foreach($shiftmanager as $shiftmanagers)
                    <option @if($id==$shiftmanagers->id) selected @endif value="{{$shiftmanagers->id}}">{{$shiftmanagers->Personnel_Name}}</option>
                    @endforeach
                  </select>&nbsp;&nbsp;
              @endif
                 @if($pendin->count()>0) 
                 <label id="siftname1">Select shift</label>&nbsp; 
                  <select style="width:40%; display:inline !important;" name="siftname" class="form-control"  id="siftname">
                      @foreach($pendin as $pendshift)
          <option @if($Shiftid==$pendshift->id)selected @endif value="{{$pendshift->id}}">{{date('d/m/Y  h:i:s A',strtotime($pendshift->created_at))}}  To {{date('d/m/Y  h:i:s A',strtotime($pendshift->closer_date))}}</option>
          @endforeach
                    </select>  
                     @endif         
              @if(Auth::user()->user_type==3)
                  <input type="submit" class="btn btn-primary abhishek" value="Go" id="go">
              @endif





      </form>
    </div>
  </div>


  <!--end add box-->
  <!--list contant-->

  <div class="" id="parentsalseman">
    <div class="table-responsive" style="padding:10px 0;">
     <div class="col-md-10 col-md-offset-1 card" style="padding:30px">
      @if($shift!=null)


      <form class="form-horizontal" id="form1" action="{{route('settlementconfirm')}}" method="post"  onsubmit="return confirm('Do you want to Continue?');">
        {{ csrf_field() }}
        <?php $str='';
        $amount=0;
        $netamount=0;
        $testamount=0;
        $totalprices=0;
        ?>
        <input type="hidden" name="type" value="{{$shift->type}}">
        <input type="hidden" value="{{$id}}" name="shift_manager">
          <input type="hidden" name="shift_id" value="{{$shift->id}}">
        @if($shift->type!=2)

          <table width="100%" style="margin-left: 50px">
     
          <table style="margin-left: 50px">
          <tr>

            <th  style=" width:10%;"> Nozzle No.</th> 
           <!--  <th>Fuel Type</th> -->
            <th style="width:22%;text-align: center;">OMR</th>
            <th style=" width:22%;text-align: center;">CMR</th>
            <th style=" width:18%; text-align: center;">Test Volume</th>
            <th style="width:18%; text-align: right;">Net Sales QTY</th>
          </tr>
        </table>
        @foreach($shift->getPadestal as $padestal)

        <p><b>Pedestal No. : {{$padestal->Pedestal_Number}}</b></p>


          <?php $str.=','.$padestal->Pedestal_Number; ?>

           <?php

         $padData=$padestal->getNozzle->where('is_active',1)->sortBy('Nozzle_Number');
         $padDataFuelWise=$padData->groupBy('fuel_type');
         ?>

             @foreach($padDataFuelWise as $key => $padValueFuelWise)

          <?php $itemName=DB::table('tbl_price_list_master')
                  ->where('RO_code',$padestal->RO_code)
                  ->where('id',$key)
                  ->value('Item_Name');

                  ?>



              <h5 style="font-weight: 900;">{{$itemName}}</h5>
              <input type="hidden" name="ro_code" value="{{$padestal->RO_code}}">


          @foreach($padValueFuelWise as $getNozzle)
        <?php
        $nore=$getNozzle->getReading()->where('shift_id',$shift->id)->first();
          ?>

          <tr class="parentcmr">
             <table width="100%" style="margin-left: 50px">
            <td  style="width:10%;"><input name=""  type="hidden" value="">{{$getNozzle->Nozzle_Number}}</td> 
        <!--     <td>{{$getNozzle->getItem->Item_Name}}</td> -->

            <td style="width:18%;">{{number_format((float)$nore['Nozzle_Start'], 3, '.', '')}}</td>

            <?php  $chet = $nore['Nozzle_Start']-$nore['Nozzle_Start'];?>

            <td style="width:18%;">{{number_format((float)$nore['Nozzle_End'], 3, '.', '')}}</td>
                <?php $netamount=($nore['reading']*$getNozzle->getItem->price->price)+$netamount;
                        $testamount=($nore['reading']*$getNozzle->getItem->price->price)+$testamount;
                  ?>
            <td style="width:18%;">{{number_format((float)$nore['test'], 3, '.', '')}} </td>
                <?php $sb=$nore['Nozzle_End']-$nore['Nozzle_Start'];?>
                        <?php
                                $chetqty1=number_format((float)$nore['test'], 3, '.', ''); 
                    
                                 $chetqty=number_format((float)$sb-$chetqty1, 3, '.', ''); 
                          ?>
            <td style="right;width:18%;">  {{$chetqty}}</td>
               </table>
          </tr>

           @endforeach

          @endforeach
        </table>
        @endforeach
        <h4>FUEL TYPEWISE NET SALES</h4>
        <table width="100%">
          <input type="hidden" name="ro_code" value="{{$padestal->RO_code}}">
          <input type="hidden" name="shift_id" value="{{$shift->id}}">
          <tr>
            <th>Fuel Type</th>
            <th style="text-align:right; width:25%;">Net Sales QTY</th>
            <th style="text-align:right; width:25%;">Rate</th>
            <th style="text-align:right; width:25%;">Total</th>
          </tr>
  
          <?php 
          $shift_id=$shift->id;
          $shift_manager=$shift->shift_manager;

          $variable = DB::table('tbl_ro_nozzle_reading')
          ->join('tbl_pedestal_nozzle_master', TRIM('tbl_ro_nozzle_reading.Nozzle_No') , TRIM('tbl_pedestal_nozzle_master.Nozzle_Number'))
          ->join('tbl_price_list_master', 'tbl_price_list_master.id', 'tbl_pedestal_nozzle_master.fuel_type')
          ->join('tbl_item_price_list', 'tbl_price_list_master.id', 'tbl_item_price_list.item_id')
          ->join('shift_pedestal', 'shift_pedestal.pedestal_id', 'tbl_pedestal_nozzle_master.Pedestal_id')
          ->join('shifts', 'shifts.id', 'shift_pedestal.shift_id')
          ->select('tbl_ro_nozzle_reading.Nozzle_No', DB::raw('(tbl_ro_nozzle_reading.Nozzle_End - tbl_ro_nozzle_reading.Nozzle_Start ) - tbl_ro_nozzle_reading.test as qty'),'tbl_ro_nozzle_reading.reading','tbl_ro_nozzle_reading.price','tbl_ro_nozzle_reading.Nozzle_End','tbl_price_list_master.item_name')

           // ->where('tbl_ro_nozzle_reading.Reading_by',$shift_manager)
         ->where('tbl_item_price_list.is_active',1)
         ->where('tbl_price_list_master.is_active',1)
        ->where('tbl_pedestal_nozzle_master.is_active',1)
          ->where('shifts.id',$shift_id)
          ->where('shifts.fuel',1)
          ->where('shifts.shift_manager',$shift_manager)
          ->where('tbl_ro_nozzle_reading.shift_id',$shift_id)
          ->whereNotNull('tbl_ro_nozzle_reading.Nozzle_End')
          ->distinct()
          ->get();

            //dd($variable, $shift_id,$shift_manager);
          $Item_Name='';
          $totalprices=0;
          $item_name2 = array();
          ?>
          @foreach($variable as $value)
          <?php   
          $item_name[] = $value->item_name;
          $item_name2 =  array_unique($item_name);

          ?>
          @endforeach  
          @foreach($item_name2 as $item_name_new)
          <?php
          $chetqty_previous=0;

          ?>
          @foreach($variable as $variabless)
          <?php


          if ($item_name_new == $variabless->item_name) {
            $Item_Names = $variabless->item_name;
            $chetqty_previous = ($variabless->qty)+($chetqty_previous);

            $chetqty_previous=number_format((float)$chetqty_previous, 3, '.', '');

            $price = $variabless->price;
          }

          ?>
          @endforeach  

          <tr>
            <td>{{$Item_Names}}</td>

            <td style="text-align:right;"> <input style="text-align:right; background:transparent!important; border:transparent!important;  width:100%;" disabled type="hidden" value=""> {{$chetqty_previous}}</td>

            <td style="text-align:right;">{{$price}}</td>


            <?php  

            $totalprice = ($price)*($chetqty_previous);


            ?>
            <td style="text-align:right;">{{number_format($totalprice, 2, '.', '')}}</td>

          </tr>
          <?php 
            $totalprices = $totalprice+$totalprices;
            $totalprices = number_format((float)$totalprices, 2, '.', '');
          ?>
          @endforeach



        </table>
@endif
        <table  width="100%" style="margin-top: 15px;">
         <?php 


         $shift_id = $shift->id;


         $var = DB::table('tbl_customer_transaction')
         ->join('tbl_price_list_master', 'tbl_customer_transaction.item_code', 'tbl_price_list_master.id')
         ->join('tbl_stock_item_group', 'tbl_stock_item_group.id', 'tbl_price_list_master.Stock_Group')
         ->select(DB::raw('sum(tbl_customer_transaction.petroldiesel_qty*tbl_customer_transaction.item_price) AS total_sales'))
         ->where('tbl_stock_item_group.Group_Name','LUBES')
         ->where('tbl_customer_transaction.shift_id',$shift_id)
         ->where('tbl_customer_transaction.petrol_or_lube',2)
         ->get();

         $vara = DB::table('tbl_customer_transaction')
         ->join('tbl_price_list_master', 'tbl_customer_transaction.item_code', 'tbl_price_list_master.id')
         ->join('tbl_stock_item_group', 'tbl_stock_item_group.id', 'tbl_price_list_master.Stock_Group')
         ->select(DB::raw('sum(tbl_customer_transaction.petroldiesel_qty*tbl_customer_transaction.item_price) AS total_sales'))
         ->where('tbl_stock_item_group.Group_Name','OTHERS')
         ->where('tbl_customer_transaction.shift_id',$shift_id)
         ->where('tbl_customer_transaction.petrol_or_lube',2)
         ->get();

         ?>
         @foreach($var as $vars)

         <tr >
           <td align="right">
             <label>Total Lubes Sale:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <td  style="text-align:right;  width:100px;">
              
              {{number_format((float)$vars->total_sales, 2, '.', '')}}</td></td>
           </tr>
           @endforeach
           @foreach($vara as $vars1)

           <tr >
             <td align="right">
               <label>Total Other Sales:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <td style="text-align:right;  width:100px;">
                {{number_format((float)$vars1->total_sales, 2, '.', '')}}</td></td>
             </tr>
             @endforeach

           </table>

 <?php $amount=0; ?>
     
      @if($shift->settelment!=null && $shift->settelment->getPayments!=null)
          <?php
           $total_lubes_sales = number_format((float)$vars1->total_sales, 2, '.', '');
           $total_other_sales = number_format((float)$vars->total_sales, 2, '.', '');
           $netamount = number_format((float)$totalprices+($total_lubes_sales+$total_other_sales), 2, '.', '');

           ?>

           <table width="100%" style="margin-top: 15px;"> 
            <tr>
              <td align="right" style=""> 
                <label>Total Sales:</label>&nbsp;&nbsp;
                <input  style="text-align:right;  width:100px;" name="Nozzle_Amount" readonly="readonly" class="r_reading" id="totale" style="text-align: right;" data-am="{{$netamount}}" type="text" value="{{number_format($netamount, 2, '.', '')}}">
              
              </td>

            

           </table>

           <table width="100%" align="right" style=": ;">


             <tr >
              <td align="right"></td>
              <td align="right" ><label style="margin-right:40px!important;"> Payment mode:
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ref No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Garruda Receipt &nbsp;&nbsp;&nbsp; Manual Receipt &nbsp;&nbsp;&nbsp; Amount:</label></td> 

            </tr>

      <?php      
            $customer_transaction = DB::table('tbl_customer_transaction')
            ->select(DB::raw('sum(petroldiesel_qty*item_price) AS total_sales'),'id')
            ->where('shift_id',$shift_id)
            ->where('cust_name','credit')
            ->where('trans_mode','auto')
            ->first();

            $customer_transaction_manual = DB::table('tbl_customer_transaction')
            ->select(DB::raw('sum(petroldiesel_qty*item_price) AS total_sales'),'id')
            ->where('shift_id',$shift_id)
            ->where('cust_name','credit')
            ->where('trans_mode','SLIPS')
            ->first();

            $previous_multi1 = 0;
            ?>
              <?php 

          $customer_transactions1 = number_format($customer_transaction->total_sales, 2, '.', '');
          $customer_transactions_manual1 = number_format($customer_transaction_manual->total_sales, 2, '.', '');
                // dd($customer_transaction);
          $customer_transactions_total1 = $customer_transactions1 +  $customer_transactions_manual1;
          ?>

          <tr>
            <td align="right">
            </td>
            <td align="right">
              <input  style="text-align:right; padding-right:50px; background:transparent!important; width:100px;" type="hidden"  value="">&nbsp;&nbsp;&nbsp;Credit Sales
              <input  style="text-align:right; padding-right:50px; background:transparent!important; width:100px;" type="hidden"  name="" value="">&nbsp;&nbsp;&nbsp;
              <input  style="text-align:right;" type="text" name="">

              <input type="text" readonly="" class="cashamounts" id="garruda_receipt" style="text-align:right; background-color:#ebebe4; width:110px;"  type="text" name="garruda_receipt" value="{{$customer_transactions1}}">

              <input type="text" class="" id="manual_receipt"  readonly="" name="manual_receipt" style="text-align:right; background:transparent!important; width:105px;"  value="{{$customer_transactions_manual1}}">

              <input style="text-align:right; background-color:#ebebe4; width:103px;" class="cashamount numeriDesimal" style="text-align:right;" type="text" readonly="" id="totle_value" name="" value="{{$customer_transactions_total1}}">
            </td>

          </tr>





<?php
/* $paymentmode_sattlement_new = DB::table('tpl_payment_mode') 
          ->leftJoin('paymentmode_sattlement','tpl_payment_mode.id','=','paymentmode_sattlement.paymentmode_id')
          ->leftJoin('tbl_shift_settlement','tbl_shift_settlement.id','=','paymentmode_sattlement.sattlement_id')

          ->select('tpl_payment_mode.id','tpl_payment_mode.name','paymentmode_sattlement.garruda_receipt','paymentmode_sattlement.manual_receipt','paymentmode_sattlement.amount','paymentmode_sattlement.ref_no' )
          ->where('tbl_shift_settlement.shift_id','=',$shift_id)
          ->where('tpl_payment_mode.IsActive',1)
          ->groupBy('tpl_payment_mode.name')
          ->orderBy('tpl_payment_mode.order_number')
          ->get();
*/

         /* $paymentmode_sattlement_new = DB::table('tpl_payment_mode') 
          ->leftJoin('tbl_customer_transaction', function($join) use ($shift_id){
            $join->on('tpl_payment_mode.id','=','tbl_customer_transaction.payment_mode')
            ->where('tbl_customer_transaction.shift_id','=',$shift_id);
          })
           ->leftJoin('paymentmode_sattlement','paymentmode_sattlement.paymentmode_id','tpl_payment_mode.id')
         ->select('tpl_payment_mode.id','tpl_payment_mode.name','paymentmode_sattlement.garruda_receipt','paymentmode_sattlement.manual_receipt','paymentmode_sattlement.amount','paymentmode_sattlement.ref_no' )
          ->where('tpl_payment_mode.IsActive',1)
          ->groupBy('tpl_payment_mode.name')
          ->orderBy('tpl_payment_mode.order_number')
          ->get();*/


$str2='';
   
 $amount1 = 0; ?>
          @foreach($paymentmode_sattlement_new as $key => $Payment)

      <?php $amount=$Payment->amount;
        $amount = number_format((float)$amount, 2, '.', '');
        $amount1+=$amount;
      ?>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
          <script type="text/javascript">
           $(document).ready(function(){

            $(document).on("keyup", "#manual_receipt1_{{$Payment->id}}", function() {

             var manual_receip =  parseFloat($("#manual_receipt1_{{$Payment->id}}").val());

             var garruda_receip =  parseFloat($("#garruda_receipt1_{{$Payment->id}}").val());
             var totle_valu = parseFloat(manual_receip+garruda_receip);

                if(isNaN(totle_valu)){
                    totle_valu=0;
                }

             $("#totle_value1_{{$Payment->id}}").val(totle_valu.toFixed(2));


          var amount=0;
         jQuery('.cashamount').each(function(){
           if(jQuery.trim(jQuery(this).val())!='')
            amount=parseFloat(jQuery(this).val())+amount;

          });
         console.log('total amount - '+amount);
         var sub_totale1 =  parseFloat($("#totale").val());
         console.log('sub_totale1' + sub_totale1);

         var diff=amount-sub_totale1;
         console.log('diff' + diff);
         jQuery('#different').val(diff.toFixed(2));
         jQuery('#totle').val(amount.toFixed(2));



           });
          });


        </script> 
    
        <?php 

        $str2.=','.$Payment->id;?>


        <tr>
          <td></td>
          <td align="right">
            <input  style="text-align:right; padding-right:50px; background:transparent!important; width:100px;" type="hidden" name="paymentmode_{{$Payment->id}}" value="{{$Payment->id}}">{{$Payment->name}}&nbsp;&nbsp;&nbsp;

            <input  style="text-align:right;" type="text" name="ref_nom_{{$Payment->id}}" value="{{$Payment->ref_no}}">

            <input type="text" id="garruda_receipt1_{{$Payment->id}}" readonly="readonly" style="text-align:right; background-color:#ebebe4; width:110px;" @if(trim(strtolower($Payment->name))=='cash') @endif type="text" name="" value="{{number_format((float)$Payment->garruda_receipt, 2, '.', '')}}">

            <input class="cashamounts" id="manual_receipt1_{{$Payment->id}}" style="text-align:right; background:transparent!important; width:105px;" @if(trim(strtolower($Payment->name))=='cash') @endif type="text" name="manual_receipt1_{{$Payment->id}}" value="{{number_format((float)$Payment->manual_receipt, 2, '.', '')}}">


            <input style="text-align:right; background-color:#ebebe4; width:103px;" class="cashamount numeriDesimal " style="text-align:right;" id="totle_value1_{{$Payment->id}}" type="text" name="payment_{{$Payment->id}}"  readonly="readonly" value="{{number_format((float)$Payment->amount, 2, '.', '')}}">
          </td>

        </tr>
        <input type="hidden" name="paymentmode_new" value="{{$str2}}">
        @endforeach
   <?php 
        $amount2 = number_format(($amount1)+($customer_transactions_total1), 2, '.', '');
        $different = number_format(($amount2)-($netamount), 2, '.', '');

        ?>
        <tr>
          
          <td></td>
          <td align="right" style="margin-top: 5px;"><br><label>Total Amount:</label>&nbsp;&nbsp;
            <input style="text-align:right;  width:100px;" type="text" style="text-align:right;" readonly id="totle" name="totle" value="{{number_format($amount2, 2, '.', '')}}">
          </td>
        </tr>
        <tr>
          <td></td>
          <td align="right"><label> Short/Excess:</label>&nbsp;&nbsp;
           <input style="text-align:right;   width:100px;" type="text" style="text-align:right;" readonly id="different" name="Diff" value="{{number_format($different, 2, '.', '')}}">
            </td>
         </tr>

       </table>

       <input type="hidden" name="padestal" value="{{$str}}">
       @endif
       @if($shift->getTanksReading!=null && $shift->getTanksReading->count()>0)

       <table width="100%" align="left" >
        <tr> 
          <th><h3>  Dip Readings</h3> </th>



        </tr>

        <tr> 
          <th>Tank Name </th>
          <th>Fuel Type </th>
          <th>Unit of Measure </th>
          <th>Reading</th>


        </tr>

        @foreach($shift->getTanksReading as $getTanksReading)
        <tr> 
          <td>{{$getTanksReading->getTank->Tank_Number}} </td>
          <td>{{$getTanksReading->getfueltypes->Item_Name}} </td>
          <td>{{$getTanksReading->getunitmesure->Unit_Symbol}} </td>
          <td align="right">{{$getTanksReading->value}}</td>

        </tr>
        @endforeach

      </table>
      @endif

      <div class="">
        <label class="control-label col-sm-12" ></label>

        <input style="margin-top:15px;" type="button" id="success-btn" class="btn btn-primary site-btn submit-button  pull-right" value="Submit"></center>

      </div>


    </form>

  </div>

  @else
  @if(Auth::user()->user_type!=3)
  <p> Start Shift First</p>
  @endif
  @endif

</div>
</div>
</div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
  jQuery(".numeriDesimal").keyup(function() { var $this = jQuery(this); $this.val($this.val().replace(/[^\d.]/g, '')); });
  jQuery(function(){
    var app={
      init:function(){
       
        jQuery('.cmr_reading').on('keyup',function(){
          var start=jQuery(this).data('start');
          var val=jQuery(this).val();
          jQuery(this).parents('.parentcmr').find('.r_reading').val(val-start);
        });
        jQuery('#success-btn').on('click',function(){

              //var flag=false;
              var flag=true;
              jQuery('.cmr_reading').each(function(){

                var start=jQuery(this).data('start');
                var val=jQuery(this).val();

                if(parseInt(val)>=parseInt(start)){
                  flag=true;
                     var button = $('#success-btn');
                    // Disable the submit button 
                    button.prop('disabled', true);
                }else{
                  flag=false;

                  alert('Nozzle Close is Always Greater of Nozzle Open');
                  return false;
                }
              });

              if(flag)
                jQuery('#form1').submit();
            });

        jQuery('#siftname').on('change',function(){
          jQuery('#form2').submit();
        });

        jQuery('.cashamounts').on('change keyup blur',function(){
         var amount=0;
         jQuery('.cashamount').each(function(){
           if(jQuery.trim(jQuery(this).val())!='')
            amount=parseFloat(jQuery(this).val())+amount;

        });
         jQuery('#totle').val(amount.toFixed(2));


         var Fule= jQuery('#Fule').val();

         var totale= parseFloat(jQuery('#totale').val());

         var di=totale;
         var dm=amount-di;
         jQuery('#different').val(dm.toFixed(2));
         jQuery('#Diff').val(dm.toFixed(2));

       })
        app.getShift();
      },
     //  getajex:function(){
     //   jQuery('#shift_manager').on('change',function(){

     //    jQuery('#siftname').hide();
     //    jQuery('#siftname1').hide();
     //    jQuery('#parentsalseman').hide();
     //    var id=jQuery(this).val();
     //    jQuery.get('getShiftOfManager/'+id,{

     //      '_token': jQuery('meta[name="csrf-token"]').attr('content'),
     //    },function(data){
     //      var opt='';
     //      jQuery.each(data, function(index,value){

     //        opt+='<option value="'+index+'">'+value+'</option>';
     //      });
     //      console.log(opt);

     //      jQuery('#siftname').html(opt);

     //    });

     //  });
     // },
      getShift:function(){

           jQuery('#shift_manager').on('change',function(){

            jQuery('#siftname').hide();
            jQuery('#siftname1').hide();
            jQuery('#parentsalseman').hide();
            var id=jQuery(this).val();
              jQuery.get("{{route('shift.get_all_shift')}}",{
                id:id,
              '_token': jQuery('meta[name="csrf-token"]').attr('content'),
              },function(data){
                var str='';
                jQuery.each(data,function(i,v){
                  str+='<option value="'+i+'">'+v+'</option>';
                });

                jQuery('#siftname').html(str);
                   
                
              });
              });
          },

   }
   app.init();
 });
</script>

@endsection
@section('style')
<style>
.card {
  padding-top: 10px !important;
}
</style>
@endsection