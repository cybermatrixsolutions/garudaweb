@extends('backend-includes.app')

@section('content')

  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Shift Allocation</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="{{url('shift/create')}}"> Shift Allocation</a></li>
        </ul>
      </div>

    </div>
    <div  style="margin-top: 41px;">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


    <!--end add box-->
    <!--list contant-->
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
           <div class="col-md-8">
            @if($Shift==null || Auth::user()->user_type==3)
            @if($shiftmanager->count()>0)
            <form class="form-horizontal" action="{{route('shiftAllocation')}}/store" method="post"  onsubmit="return confirm('Do you want to Continue?');">
              {{ csrf_field() }}
    
               @if(Auth::user()->user_type==3)

  <input type="hidden" name="flexishiftstatus" value="{{$flexishiftstatus}}"></input>

               <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Select Shift </label>
                    <div class="col-sm-8">
                      <select class="form-control" name="shift_type" id="shift_type" required="required">
                         <option value="">Please Select Shift </option>




  @if($flexishiftstatus=="flexi")


                     @foreach($flexiShifttype as $Shifttypes)
                      <option value="{{$Shifttypes->start_time}},{{$Shifttypes->id}}">{{$Shifttypes->type}} ({{$Shifttypes->start_time}} - {{$Shifttypes->end_time}})</option>
                      @endforeach




  @else

                     @foreach($Shifttype as $Shifttypes)
                      <option value="{{$Shifttypes->start_time}},{{$Shifttypes->id}}">{{$Shifttypes->type}} ({{$Shifttypes->start_time}} - {{$Shifttypes->end_time}})</option>
                      @endforeach

  @endif


                    
                      
                      </select>
                    </div>
               </div>

              <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Select Date </label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="datetimepicker1"  required="required" name="created_at" value=""  placeholder="Date"/> 
                    </div>
               </div>



                <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Select Shift Type</label>
                    <div class="col-sm-8">
                      <select class="form-control" name="type" id="type" required="required">
                         <option value="">Please Select Shift Type</option>

                       @foreach($ShiftType_old as $key =>$ShiftType_olds)
                      <option value="{{$key}}">{{$ShiftType_olds}}</option>
                      @endforeach
                      
                      </select>
                    </div>
               </div>

               

               <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Select Shift Manager </label>
                    <div class="col-sm-8">
                      <select class="form-control" name="shift_manager" id="shift_manager" required="required">
                         <option value="">Please Select Shift Manager</option>
                      @foreach($shiftmanager as $shiftmanagers)
                      <option value="{{$shiftmanagers->id}}">{{$shiftmanagers->Personnel_Name}}</option>
                      @endforeach
                      </select>
                    </div>
               </div>

               

               @endif
                 

              <div class="form-group" id="Padastaldiv">
                <label class="control-label col-sm-3" for="pwd">Select Pedestal </label>
                    <div class="col-sm-8">
                      <select class="form-control" id="Padastal" multiple="multiple" required="required" name="Padastal[]">
                      @foreach($personals as $personals)
                      <option value="{{$personals->id}}">{{$personals->Pedestal_Number}}</option>
                      @endforeach
                      </select>
                    </div>
               </div>

               <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Sales Man</label>
                    <div class="col-sm-8">
                      <select class="form-control" id="salesman" multiple="multiple" required="required" name="salesman[]">
                      @if(Auth::user()->user_type!=3)
                      <option value="{{Auth::user()->getPersonel->id}}">Own</option>
                      @endif

                      @foreach($pedestals as $pedestal)
                      <option value="{{$pedestal->id}}">{{$pedestal->Personnel_Name}}</option>
                      @endforeach
                      </select>
                    </div>
               </div>
			   
			   <div class="form-group" id="tankdiv">
                <label class="control-label col-sm-3" for="pwd">Tank</label>
                    <div class="col-sm-8">
                      <select class="form-control" id="tank" multiple="multiple"  name="tank[]">
                      @foreach($tanks as $tank)
                      <option value="{{$tank->id}}">{{$tank->Tank_Number}}</option>
                      @endforeach
                      </select>
                    </div>
               </div>
              
               <div class="form-group">
                <label class="control-label col-sm-8" ></label>
                    <div class="col-sm-3">
                     <input type="submit" id="success-btn" class="btn btn-primary site-btn submit-button  pull-right" value="Submit"></center>

                    </div>
               </div>
              
             </form>
                @else 
                   <br><br> <center> <h4>Shift Manager Not found, So Please Create Shift Manager</h4></center>

                 @endif
             @else
             <p> Current Shift is Your</p>
             @endif


            </div>
        </div>
      </div>
    </div>
     <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped"  id="myTable">
            <thead>
              <tr>
                <th>S.no.</th>
               
                <th>Shift Date</th>
                <th>Shift Time</th>
                <th>Shift Manager Name</th>
               
                
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
               <?php $i=0; ?>
               @foreach($openshift as $openshifts)
              <?php $i++ ;?>    
              <tr>
                <td >{{$i}}</td>
                <td>{{Carbon\Carbon::parse($openshifts->created_at)->format('d/m/Y')}}</td>
                 <td>{{Carbon\Carbon::parse($openshifts->created_at)->format('H:i:s')}}</td>
                <td>{{$openshifts->getPersonnelname->Personnel_Name}}</td>
                
                <td>
                  <div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                     <li><a href="edit/{{$openshifts->id}}">Edit</a></li>
                   <!--  <li><a href="view/{{$openshifts->id}}">View</a></li> -->
                  </ul>
                  </div>
                </td>
              </tr>
            </tbody>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
   jQuery(function(){
       var app={
              init:function(){
                  app.fromDate();

                jQuery('#Padastal').select2({
                  data: [],
                    tagsq: true,
                    tokenSeparators: [','], 
                    placeholder: "Select Padastal"
                });
                jQuery('#salesman').select2({
                  data: [],
                    tagsq: true,
                    tokenSeparators: [','], 
                    placeholder: "Select salesman"
                });
				        jQuery('#tank').select2({
                  data: [],
                    tagsq: true,
                    tokenSeparators: [','], 
                    placeholder: "Select tank"
                });
                app.setval();

                jQuery(document).on('change','#shift_manager',function(){
                   app.setval();
                });

                jQuery(document).on('change','#type',function(){
                   app.shifttype();
                });
              },
              fromDate:function(){
                
               $('#datetimepicker1').datetimepicker({
                  defaultDate:new Date(),
                  format:'DD/MM/YYYY '
                });
              },
              setval:function(){
                var n=jQuery('#shift_manager').val();
                console.log(n);
                if(jQuery.trim(n)!=''){

                  jQuery('#salesman').val(n).trigger("change.select2");
                  app.getShift();
                }else{
                      jQuery('#salesman').val('').trigger("change.select2");
                     jQuery('#Padastal').val('').trigger("change.select2");
                }
              },
              shifttype:function(){
                var shifttype=jQuery('#type').val();
                // console.log(n);
                if(jQuery.trim(shifttype)==2){

                  // jQuery('#salesman').val(n).trigger("change.select2");
                  jQuery("#Padastal").prop('required',false);
                  jQuery("#Padastaldiv").hide(100);
                  jQuery("#tankdiv").hide(100);
                }else{
                      jQuery('#salesman').val('').trigger("change.select2");
                     jQuery('#Padastal').val('').trigger("change.select2");
                     jQuery("#Padastal").prop('required',true);
                     jQuery("#Padastaldiv").show(100);
                      jQuery("#tankdiv").show(100);
                }
              },
              getShift:function(){
                var shiftManager=jQuery('#shift_manager').val();
                jQuery.get("getPrevShift/"+shiftManager,{
                   '_token': jQuery('meta[name="csrf-token"]').attr('content'),

                },function(data){

                      if(data['salesman'].length>0)
                        jQuery('#salesman').val(data['salesman']).trigger("change.select2");
                     

                      if(data['pedestals'].length>0)
                        jQuery('#Padastal').val(data['pedestals']).trigger("change.select2");

                });

              },
       }

       app.init();
   });
               
</script>

@endsection