@extends('backend-includes.app')

@section('content')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; Nozzle CMR</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#"> Nozzle CMR</a></li>
        </ul>
      </div>

    </div>
    <div  style="margin-top: 41px;">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

   
    <!--end add box-->
    <!--list contant-->
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
         <div class="col-md-8">
            @if($Shift!=null)

            <form class="form-horizontal" action="{{route('shiftAllocation')}}/store" method="post">
              {{ csrf_field() }}
            @foreach($Shift->getPadestal as $padestal)

             <p>$padestal->Pedestal_Number</p>
             <table width="100%">
                <tr>
                  <th>Nozzle No.</th> 
                  <th>Fuel</th> 
                  <th>open</th>
                  <th>close</th>
                </tr>
                
                @foreach($padestal->getNozzle as $getNozzle)
                   <?php $getNozzle->getReading()->where('Nozzle_End',null)->first(); ?>
                  <tr>
                    <td>{{$getNozzle->Nozzle_Number}}</td> 
                    <td>{{dd($getNozzle->getItem)}}</td> 
                    <td><input type="text" value="{{$nore->Nozzle_Start}}"></td>
                    <td><input type="text" value="34"></td>
                  </tr>
                 @endforeach
             </table>
            @endforeach
              

              
               <div class="form-group">
                <label class="control-label col-sm-8" ></label>
                    <div class="col-sm-3">
                     <input type="submit" id="success-btn" class="btn btn-primary site-btn submit-button  pull-right" value="Submit"></center>

                    </div>
               </div>
     
             </form>
             @else
             <p> Start Shift First</p>
             @endif

            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script type="text/javascript">

</script>

@endsection