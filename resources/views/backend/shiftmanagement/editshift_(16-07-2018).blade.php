@extends('backend-includes.app')

@section('content')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Shift Edit</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

           <li><a href="{{url('shift/create')}}">Shift</a></li>
        </ul>
      </div>

    </div>
    <div  style="margin-top: 41px;">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

   
    <!--end add box-->
    <!--list contant-->
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
           <div class="col-md-8">
            @if($shift==null || Auth::user()->user_type==3)
           
            <form class="form-horizontal" action="{{url('shiftupdate')}}/{{$shift->id}}" method="post">
              {{ csrf_field() }}
               @if(Auth::user()->user_type==3)
               <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Select Shift Manager </label>
                    <div class="col-sm-8">
                      <select class="form-control" name="shift_manager" required="required">
                       <option value="{{$shift->getPersonnelname->id}}">{{$shift->getPersonnelname->Personnel_Name}}</option>
                      @foreach($shiftmanager as $shiftmanagers)
                      <option value="{{$shiftmanagers->id}}">{{$shiftmanagers->Personnel_Name}}</option>
                      @endforeach
                      
                      </select>
                    </div>
               </div>

               <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Select Date </label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="datetimepicker1"  required="required" name="created_at" value="{{date_format($shift->created_at,'d-m-Y')}}"  placeholder="Date"/> 
                    </div>
               </div>


              <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Select Shift Type </label>
                    <div class="col-sm-8">
                      <select class="form-control" name="shift_type" id="type" required="required">
                    
                          @foreach($Shifttype as $Shifttypes)
                      <option @if($shift->shift_type==$Shifttypes->id) selected @endif  value="{{$Shifttypes->start_time}},{{$Shifttypes->id}}">{{$Shifttypes->type}} ({{$Shifttypes->start_time}} - {{$Shifttypes->end_time}})</option>
                      @endforeach


                      </select>
                    </div>
               </div>

               @endif
                 

              <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Select Pedestal </label>
                    <div class="col-sm-8">
                      <select class="form-control" id="Padastal" multiple="multiple" required="required" name="Padastal[]">
                      @foreach($shift->getPadestal as $pers)
                      <option selected value="{{$pers->id}}">{{$pers->Pedestal_Number}}</option>
                      @endforeach

                      @foreach($pedestals as $pedestal)
                      <option  value="{{$pedestal->id}}">{{$pedestal->Pedestal_Number}}</option>
                      @endforeach
                      </select>
                    </div>
               </div>
               
               <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Sales Man</label>
                    <div class="col-sm-8">
                      <select class="form-control" id="salesman" multiple="multiple" required="required" name="salesman[]">
                      @if(Auth::user()->user_type!=3)
                      <option value="{{Auth::user()->getPersonel->id}}">Own</option>
                      @endif

                      @foreach($shift->getPersonnel as $pedestal)
                      <option selected value="{{$pedestal->id}}">{{$pedestal->Personnel_Name}}</option>
                      @endforeach

                       @foreach($personals as $person)
                      <option  value="{{$person->id}}">{{$person->Personnel_Name}}</option>
                      @endforeach

                      </select>
                    </div>
               </div>
			   
			   <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Tank</label>
                    <div class="col-sm-8">
                      <select class="form-control" id="tank" multiple="multiple" name="tank[]">


                      @foreach($shift->getTanks as $tank)
                      <option selected value="{{$tank->id}}">{{$tank->Tank_Number}}</option>
                      @endforeach

                       @foreach($tanks as $tank)
                      <option  value="{{$tank->id}}">{{$tank->Tank_Number}}</option>
                      @endforeach

                      </select>
                    </div>
               </div>
              
               <div class="form-group">
                <label class="control-label col-sm-8" ></label>
                    <div class="col-sm-3">
                     <input type="submit" id="success-btn" class="btn btn-primary site-btn submit-button  pull-right" value="Submit"></center>

                    </div>
               </div>
              
             </form>
                @else 
                   <br><br> <center> <h4>Shift Manager Not found, So Please Create Shift Manager</h4></center>

                 @endif
            
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
               $('#Padastal').select2({
                  data: [],
                    tagsq: true,
                    tokenSeparators: [','], 
                    placeholder: "Select Padastal"
                });

               $('#salesman').select2({
                  data: [],
                    tagsq: true,
                    tokenSeparators: [','], 
                    placeholder: "Select salesman"
                });
				$('#tank').select2({
                  data: [],
                    tagsq: true,
                    tokenSeparators: [','], 
                    placeholder: "Select tank"
                });


jQuery(function(){
       var app={
              init:function(){
                app.fromDate();
              },
         fromDate:function(){
                
               $('#datetimepicker1').datetimepicker({
                  
                  format:'DD/MM/YYYY '
                });
              }
            }
          app.init();
        });
</script>

@endsection