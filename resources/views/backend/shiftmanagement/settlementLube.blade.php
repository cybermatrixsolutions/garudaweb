@extends('backend-includes.app')

@section('content')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Lube Settlement</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Lube Settlement</a></li>
        </ul>
      </div>

    </div>
    <div  style="margin-top: 41px;">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
      <div class="col-md-12 card">
          <form class="form-horizontal" id="form2" action="{{route('settlementLueb')}}" method="get">
              {{ csrf_field() }}
                     @if(Auth::user()->user_type==3)
                     <label>Salect shift Manager</label>&nbsp;&nbsp; 
                          <select style="display:inline; width:30%;" class="form-control" id="shift_manager" name="shift_manager"  required="required">
                            @foreach($shiftmanager as $shiftmanagers)
                            <option @if($id==$shiftmanagers->id) selected @endif value="{{$shiftmanagers->id}}">{{$shiftmanagers->Personnel_Name}}</option>
                            @endforeach
                          </select>
                      @endif
                  
                     @if($pendin->count()>0)
                      <label>Select shift</label>&nbsp;&nbsp; 
                
                          <select style="display:inline; width:30%;" class="form-control" name="siftname" id="siftname">

                            @foreach($pendin as $pendshift)
                              <option @if($Shiftid==$pendshift->id)selected @endif value="{{$pendshift->id}}">{{date('d/m/Y  h:i:s A',strtotime($pendshift->created_at))}}  To {{date('d/m/Y  h:i:s A',strtotime($pendshift->closer_date))}}</option>
                            @endforeach
                          </select>
                      
                        @endif
                       @if(Auth::user()->user_type==3)
                      <input type="submit" class="btn btn-primary" value="Go">
                        @endif
                         <input type="hidden"  name="type" value="2">
                
      
            </form>
         </div>
        </div>
          
    <!--end add box-->
    <!--list contant-->
    <div class="row" id="parentsalseman">
      <div class="col-md-12 card">
        <div class="table-responsive" style="border-style:none;">
         <div class="col-md-12">
            @if($Shift!=null)

            <form class="form-horizontal" id="form1" action="{{route('settlement')}}" method="post">
              {{ csrf_field() }}
                     <?php $str='';
                     $amount=0;
                     ?>

                     <input type="hidden" value="{{$id}}" name="shift_manager">
                     <input type="hidden" name="ro_code" value="{{$Shift->ro_code}}">
                     <input type="hidden" name="shift_id" value="{{$Shift->id}}">
                     <input type="hidden" name="Nozzle_Amount" value="0">
                     <input type="hidden" name="Nozzle_grass" value="0">
                     <input type="hidden" name="Nozzle_test" value="0">
                     <input type="hidden" name="type" value="2">

 
                <?php 
                       $famout=0;
                      foreach ($ather as $athers) {
                       $famout=$athers->item_price+$famout;
                      }?>
                      <?php 
                       $credit=0;
                      foreach ($creditsale as $credits) {
                       $credit=$credits->item_price+$credit;
                      }?>
                      
              <table>
                 <tr>
                   

                  <td>Walkin Sales </td> 
                  <td>
                        <input  id="Items"  data-am="{{$famout}}" disabled class="r_reading" style="text-align:right;"  type="text" value="{{$famout}}">
                        <input  name="Credit_lube" type="hidden" value="{{$famout}}">

                  </td>
                   </tr>
                    <tr>
                  <td>Credit Sales </td> 
                  <td>
                        @if(count($credit)!=null)
                        <input  id="Items"  disabled class="r_reading"  style="text-align:right;" type="text" value="{{$credit}}">
                        @endif
                       

                  </td>
                  
                  </tr>

                 <tr>
                  <th>Payment mode</th> 
                  <th>Amount</th>
                </tr>
                
                     <?php $str=''; ?>
                @foreach($PaymentModel as $Payment)

                  <?php $str.=','.$Payment->id;?>
                 <tr>
                  <td><input  type="hidden" name="paymentmode_{{$Payment->id}}" value="{{$Payment->id}}">{{$Payment->name}}</td>
                   <td><input class="cashamount" type="text" style="text-align:right;"  name="payment_{{$Payment->id}}"></td>
                 </tr>
                 @endforeach

                 <tr>
                  <input type="hidden" name="paymentmode" value="{{$str}}">
                  <td>Total Amount </td>
                   <td><input type="text" disabled id="totle" style="text-align:right;"  name="totle"> </td>
                 </tr>
                  <td> Different </td>
                   <td><input type="text" disabled id="different" style="text-align:right;"> <input type="hidden"  id="Diff" name="Diff"></td>
                 </tr>

              </table>

            <input type="hidden" name="padestal" value="{{$str}}">

               <div class="form-group">
                <label class="control-label col-sm-2" ></label>
                    <div class="col-sm-3">
                      @if($famout!=0)
                     <input type="button" id="success-btn" class="btn btn-primary site-btn submit-button" value="Submit"></center>
                      @endif
                    </div>
               </div>
     
             </form>
             @else
                @if(Auth::user()->user_type!=3)
                  <p> Start Shift First</p>
                 @endif
             @endif

            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script type="text/javascript">
    jQuery(function(){
      var app={
        init:function(){
          
          jQuery('#success-btn').on('click',function(){
              var flag=false;
              jQuery('.cashamount').each(function(){
                  
                  var val=jQuery(this).val();
                   
                  if(jQuery.trim(val)!=''){
                    flag=true;
                  }else{
                    flag=false;
                    alert('Nozzle Close is Always Greater of Nozzle Open');
                    return false;
                  }
              });
            
            if(flag)
              jQuery('#form1').submit();
          });

       jQuery('#siftname').on('change',function(){
          jQuery('#form2').submit();
       });

       jQuery('.cashamount').on('keyup',function(){
           var amount=0;
           jQuery('.cashamount').each(function(){
                 if(jQuery.trim(jQuery(this).val())!='')
                amount=parseInt(jQuery(this).val())+amount;

            });
           jQuery('#totle').val(amount);
           
           var Fule= jQuery('#Items').val();

           var di=Fule;
           var dm=amount-di;
           jQuery('#different').val(dm.toFixed(2));
            jQuery('#Diff').val(dm.toFixed(2));
           
       })
       app.getajex();
        },
        getajex:function(){
           jQuery('#shift_manager').on('change',function(){
                jQuery('#parentsalseman').hide();
                var id=jQuery(this).val();
                jQuery.get('getShiftOfManager/'+id,{
                
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){
                    
                      opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#siftname').html(opt);
                   
               });
              
            });
        },

        
      }
      app.init();
    });
</script>

@endsection
@section('style')
<style>
  .card table tr td{
    padding-bottom: 10px !important;
  }
  .card {
    padding-top: 15px !important;
  }
</style>
@endsection