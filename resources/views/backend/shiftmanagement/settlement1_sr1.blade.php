@extends('backend-includes.app')

@section('content')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Fuel Settlement</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Fuel Settlement</a></li>
        </ul>
      </div>

    </div>
    <div  style="margin-top: 41px;">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="">
          <div class="col-md-10 col-md-offset-1 card" style="padding:10px!important;">
          <form class="form-horizontal" id="form2" action="{{route('settlement')}}" method="get">
              {{ csrf_field() }}
         
                    <input type="hidden" name="type" value="1">
                     @if(Auth::user()->user_type==3)
                     <label>Select shift Manager</label>&nbsp;
                          <select style="width:20%; display:inline!important;" class="form-control" id="shift_manager" name="shift_manager"  required="required">
                            @foreach($shiftmanager as $shiftmanagers)
                            <option @if($id==$shiftmanagers->id) selected @endif value="{{$shiftmanagers->id}}">{{$shiftmanagers->Personnel_Name}}</option>
                            @endforeach
                          </select>&nbsp;&nbsp;
                      @endif
                      
                     @if($pendin->count()>0)
                      <label>Select shift</label>&nbsp; 
                          <select style="width:40%; display:inline !important;" name="siftname" class="form-control"  id="siftname">
                            @foreach($pendin as $pendshift)
                              <option @if($Shiftid==$pendshift->id)selected @endif value="{{$pendshift->id}}">{{date('d/m/Y  h:i:s A',strtotime($pendshift->created_at))}}  To {{date('d/m/Y  h:i:s A',strtotime($pendshift->closer_date))}}</option>
                            @endforeach
                          </select>
                        @endif
                       @if(Auth::user()->user_type==3)
                      <input type="submit" class="btn btn-primary abhishek" value="Go">
                        @endif

          
             
     
              
            </form>
              </div>
         </div>
 
        
    <!--end add box-->
    <!--list contant-->
     <div class="" id="parentsalseman">
        <div class="table-responsive" style="padding:10px 0;">
         <div class="col-md-10 col-md-offset-1 card" style="padding:30px">
            @if($Shift!=null)
             
            <form class="form-horizontal" id="form1" action="{{route('settlement')}}" method="post">
              {{ csrf_field() }}
                     <?php $str='';
                     $amount=0;
                     $netamount=0;
                     $testamount=0;
                     ?>

                     <input type="hidden" value="{{$id}}" name="shift_manager">
            @foreach($Shift->getPadestal as $padestal)
             
             <p>Padestal No. : {{$padestal->Pedestal_Number}}</p>
             <table width="100%">
              <input type="hidden" name="ro_code" value="{{$padestal->RO_code}}">
              <input type="hidden" name="shift_id" value="{{$Shift->id}}">
                <tr>
                  <th>Nozzle No.</th> 
                  <th>Fuel Type</th>
                   <th style="text-align:right; width:13%;">Fuel Price</th>
                   <th style="text-align:right; width:18%;">Volume Out</th>
                 <!--  <th>OMR</th>
                  <th>CMR</th>
                  <th>Gross Amount</th> -->
                  <th style="text-align:right; width:18%;">Test Amount</th>
                  <th style="text-align:right; width:18%;">Net Sales</th>
                </tr>
                <?php $str.=','.$padestal->Pedestal_Number; ?>
                @foreach($padestal->getNozzle as $getNozzle)
                   <?php $nore=$getNozzle->getReading()->where('shift_id',$Shift->id)->first();
                    ?>
                  <tr class="parentcmr">
                    <td><input name="{{trim($padestal->Pedestal_Number)}}_Nozzle_No[]"  type="hidden" value="{{$getNozzle->Nozzle_Number}}">{{$getNozzle->Nozzle_Number}}</td> 
                    <td>{{$getNozzle->getItem->Item_Name}}</td>
                     <td style="text-align:right;">{{$getNozzle->getItem->price->price}}</td>
                     <?php $sb=$nore['Nozzle_End']-$nore['Nozzle_Start'];?>
                     <td style="text-align:right;">{{$sb}}</td>
                   
                    <?php $netamount=($nore['reading']*$getNozzle->getItem->price->price)+$netamount;
                          $testamount=($nore['reading']*$getNozzle->getItem->price->price)+$testamount;
                    ?>
                    <td style="text-align:right;"> <input style="text-align:right; background:transparent!important; border:transparent!important;  width:100%; " disabled type="hidden" value="{{$nore['test']*$getNozzle->getItem->price->price}}">{{$nore['test']*$getNozzle->getItem->price->price}} </td>
                    <td style="text-align:right;"> <input style="text-align:right; background:transparent!important; border:transparent!important;  width:100%;" disabled type="hidden" value="{{$nore['reading']*$getNozzle->getItem->price->price}}"> {{$nore['reading']*$getNozzle->getItem->price->price}}</td>
                  </tr>

                 @endforeach
             </table>
            @endforeach

              <table width="100%" style="margin-top: 15px;"> 
                  <tr>
                    <td align="right" style=""> 
                      <label>Total Nozzle Sales:</label>&nbsp;&nbsp;
                      <input  style="text-align:right;  width:100px;" disabled class="r_reading" id="totale" style="text-align: right;" data-am="{{$netamount}}" type="text" value="{{$netamount}}">
                      <input style="text-align:right;   width:100px;" name="Nozzle_Amount"  type="hidden" value="{{$netamount}}">
                      <input style="text-align:right;   width:100px;" name="Nozzle_test"  type="hidden" value="{{$testamount}}">
                      <input style="text-align:right;   width:100px;" name="Nozzle_grass"  type="hidden" value="{{$amount}}">
                    </td>

                      <?php 
                       $fuelamout=0;
                      foreach ($fuels as $fuel) {
                       
                       $fuelamout=($fuel->item_price*$fuel->petroldiesel_qty)+$fuelamout;
                      }?>
                   
              </table>

              <table width="100%" align="right" style=": ;">
                 <tr>
                  <td></td>
                  <td align="right"><label>Credit Sales Fuels:</label>&nbsp;&nbsp;
                       <input style="text-align:right; width:100px;" id="Fule" data-am="{{$fuelamout}}" disabled class="r_reading" style="text-align:right;"  type="text" value="{{$fuelamout}}">
                       <input style="text-align:right;   width:100px;" name="Credit_fuel" type="hidden" value="{{$fuelamout}}">
                       <input style="text-align:right; width:100px;" name="type" type="hidden" value="1">
                  </td> 
                </tr>

                 <tr >
                  <td align="right"></td>
                  <td align="right" ><label style="margin-right:40px!important;">
                    Payment mode:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ref No.
                    &nbsp;&nbsp;&nbsp; Amount:</label></td> 
                
                </tr>
                
                <?php $str=''; ?>
                @foreach($PaymentModel as $Payment)

                  <?php $str.=','.$Payment->id;?>
                 <tr>
                  <td></td>
                   <td align="right">
                    <input  style="text-align:right; padding-right:50px; background:transparent!important; width:100px;" type="hidden" name="paymentmode_{{$Payment->id}}" value="{{$Payment->id}}">{{$Payment->name}}&nbsp;&nbsp;&nbsp;
                    <input style="text-align:right; background:transparent!important; width:100px;" @if(trim(strtolower($Payment->name))=='cash') readonly @endif type="text" name="ref_nom_{{$Payment->id}}">
                   <input style="text-align:right; background:transparent!important; width:100px;" class="cashamount numeriDesimal" style="text-align:right;" type="text" name="payment_{{$Payment->id}}">
                  </td>
                    
                 </tr>
                 @endforeach

                 <tr>
                  <input type="hidden" name="paymentmode" value="{{$str}}">
                  <td></td>
                  <td align="right"><label>Total Amount:</label>&nbsp;&nbsp;
                  <input style="text-align:right;  width:100px;" type="text" style="text-align:right;" disabled id="totle" name="totle">
                  </td>
                 </tr>
                 <tr>
                  <td></td>
                  <td align="right"><label> Difference:</label>&nbsp;&nbsp;
                   <input style="text-align:right;   width:100px;" type="text" style="text-align:right;" disabled id="different" > <input type="hidden"  id="Diff" name="Diff"></td>
                 </tr>

              </table>

                <input type="hidden" name="padestal" value="{{$str}}">

                @if($Shift->getTanksReading!=null && $Shift->getTanksReading->count()>0)
                
                 <table width="100%" align="left" >
                  <tr> 
                      <th><h3>  Dip Readings</h3> </th>
                      
                    

                  </tr>
                 
                  <tr> 
                      <th>Tank Name </th>
                      <th>Fuel Type </th>
                      <th>Unit of Measure </th>
                      <th>Reading</th>
                    

                  </tr>
                 
                     @foreach($Shift->getTanksReading as $getTanksReading)
                       <tr> 
                          <td>{{$getTanksReading->getTank->Tank_Number}} </td>
                          <td>{{$getTanksReading->getfueltypes->Item_Name}} </td>
                          <td>{{$getTanksReading->getunitmesure->Unit_Symbol}} </td>
                          <td align="right">{{$getTanksReading->value}}</td>
                        
                      </tr>
                      @endforeach
                 
                 </table>
              @endif

               <div class="">
                <label class="control-label col-sm-12" ></label>
                     <input style="margin-top:15px;" type="button" id="success-btn" class="btn btn-primary site-btn submit-button  pull-right" value="Submit"></center>

                </div>
             
     
             </form>
            
               </div>
             
             @else
                @if(Auth::user()->user_type!=3)
                  <p> Start Shift First</p>
                 @endif
             @endif

            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script type="text/javascript">
jQuery(".numeriDesimal").keyup(function() { var $this = jQuery(this); $this.val($this.val().replace(/[^\d.]/g, '')); });
    jQuery(function(){
      var app={
        init:function(){
          jQuery('.cmr_reading').on('keyup',function(){
              var start=jQuery(this).data('start');
              var val=jQuery(this).val();
             jQuery(this).parents('.parentcmr').find('.r_reading').val(val-start);
          });
          jQuery('#success-btn').on('click',function(){

              //var flag=false;
              var flag=true;
              jQuery('.cmr_reading').each(function(){
                 alert('hh');
                  var start=jQuery(this).data('start');
                  var val=jQuery(this).val();
                   
                  if(parseInt(val)>=parseInt(start)){
                    flag=true;
                  }else{
                    flag=false;

                    alert('Nozzle Close is Always Greater of Nozzle Open');
                    return false;
                  }
              });
            
            if(flag)
              jQuery('#form1').submit();
          });

       jQuery('#siftname').on('change',function(){
          jQuery('#form2').submit();
       });

       jQuery('.cashamount').on('keyup blur change',function(){
           var amount=0;
           jQuery('.cashamount').each(function(){
                 if(jQuery.trim(jQuery(this).val())!='')
                amount=parseInt(jQuery(this).val())+amount;

            });
           jQuery('#totle').val(amount);

           
           var Fule= jQuery('#Fule').val();

            var totale= jQuery('#totale').val();

           var di=totale-Fule;
           var dm=amount-di;
           jQuery('#different').val(dm.toFixed(2));
            jQuery('#Diff').val(dm.toFixed(2));
           
       })
       app.getajex();
        },
        getajex:function(){
           jQuery('#shift_manager').on('change',function(){
                jQuery('#parentsalseman').hide();
                var id=jQuery(this).val();
                jQuery.get('getShiftOfManager/'+id,{
                
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){
                    
                      opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#siftname').html(opt);
                   
               });
              
            });
        },

        
      }
      app.init();
    });
</script>

@endsection
@section('style')
<style>
  .card {
    padding-top: 10px !important;
  }
</style>
@endsection