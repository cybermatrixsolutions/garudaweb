@extends('backend-includes.app')

@section('content')
    
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Shift Settlement</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Shift Settlement</a></li>
        </ul>
      </div>

    </div>
    <div  style="margin-top: 41px;">
      <center>
            @if(Session::has('success'))
                <font style="color:red">{!!session('success')!!}</font>
            @endif
      </center>
    </div>  

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
         <div class="col-md-8">
          <form class="form-horizontal" id="form2" action="{{route('settlement')}}" method="get">
              {{ csrf_field() }}
              
             <table width="100%">

                  <tr>
                     @if(Auth::user()->user_type==3)
                     <td>Select shift Manager</td> 
                      <td> 
                          <select class="form-control" id="shift_manager" name="shift_manager"  required="required">
                            @foreach($shiftmanager as $shiftmanagers)
                            <option @if($id==$shiftmanagers->id) selected @endif value="{{$shiftmanagers->id}}">{{$shiftmanagers->Personnel_Name}}</option>
                            @endforeach
                          </select>
                       </td>
                      @endif
                  
                     @if($pendin->count()>0)
                      <td>Select shift</td> 
                      <td>
                          <select name="siftname" id="siftname">

                            @foreach($pendin as $pendshift)
                              <option @if($Shiftid==$pendshift->id)selected @endif value="{{$pendshift->id}}">{{date('d/m/Y',strtotime($pendshift->closer_date))}}</option>
                            @endforeach
                          </select>
                        </td>
                        @endif

                        <td><input type="submit" value="Go"></td>
                   </tr>

           </table>
              
            </form>
         </div>
        </div>
      </div>
    </div>     
    <!--end add box-->
    <!--list contant-->
    <div class="row" id="parentsalseman">
      <div class="col-md-12">
        <div class="table-responsive">
         <div class="col-md-8">
            @if($Shift!=null)

            <form class="form-horizontal" id="form1" action="{{route('settlement')}}" method="post">
              {{ csrf_field() }}
              <?php $str='';
                     $amount=0;
                     ?>

                     <input type="hidden" value="{{$id}}" name="shift_manager">
            @foreach($Shift->getPadestal as $padestal)
             
             <p>Padestal No. : {{$padestal->Pedestal_Number}}</p>
             <table width="100%">
              <input type="hidden" name="ro_code" value="{{$padestal->RO_code}}">
              <input type="hidden" name="shift_id" value="{{$Shift->id}}">
                <tr>
                  <th>Nozzle No.</th> 
                  <th>OMR</th>
                  <th>CMR</th>
                  <th>Reading</th>
                  <th>Amount</th>
                </tr>
                <?php $str.=','.$padestal->Pedestal_Number; ?>
                @foreach($padestal->getNozzle as $getNozzle)
                   <?php $nore=$getNozzle->getReading()->where('shift_id',$Shift->id)->first();
                    ?>
                  <tr class="parentcmr">
                    <td><input name="{{trim($padestal->Pedestal_Number)}}_Nozzle_No[]"  type="hidden" value="{{$getNozzle->Nozzle_Number}}">{{$getNozzle->Nozzle_Number}}</td> 
                    <td><input name="{{trim($padestal->Pedestal_Number)}}_Nozzle_start_{{trim($getNozzle->Nozzle_Number)}}" disabled type="text" value="@if($nore['Nozzle_Start']=='') {{$getNozzle->Opening_Reading}} @else {{$nore['Nozzle_Start']}} @endif"></td>
                    <td><input disabled name="{{trim($padestal->Pedestal_Number)}}_Nozzle_End_{{trim($getNozzle->Nozzle_Number)}}" class="cmr_reading" type="text" value="@if($nore['Nozzle_Start']=='') {{$getNozzle->Opening_Reading}} @else {{$nore['Nozzle_End']}} @endif" data-start="@if($nore['Nozzle_Start']=='') {{$getNozzle->Opening_Reading}} @else {{$nore['Nozzle_Start']}} @endif"></td>
                    <td><input disabled class="r_reading" type="text" value="@if($nore['Nozzle_Start']=='') {{$nore['Nozzle_End']-$getNozzle->Opening_Reading}} @else {{$nore['Nozzle_End']-$nore['Nozzle_Start']}} @endif"></td>
                    <td>
                      @if($nore['Nozzle_Start']=='') 
                      <?php $sb=$nore['Nozzle_End']-$getNozzle->Opening_Reading; 

                            $ab=$sb*$getNozzle->getItem->price->price; 
                            $amount=$ab+$amount;
                           ?>
                        

                      @else 
                      <?php 
                      $sb=$nore['Nozzle_End']-$nore['Nozzle_Start'];
                      
                      $ab=$sb*$getNozzle->getItem->price->price; 
                      $amount=$ab+$amount;
                      ?>

                      @endif
                      <input disabled class="r_reading" type="text" value="{{$ab}}"></td>
                     
                  </tr>

                 @endforeach
             </table>
            @endforeach

              <table width="100%" style="margin-top: 15px;"> 
               
                  <tr  >
                    <td colspan="4" align="right"> Total Nozzle Sales </td>
                    <td align="right"> <input  disabled class="r_reading" id="totale" data-am="{{$amount}}" type="text" value="{{$amount}}">
                      <input name="Nozzle_Amount"  type="hidden" value="{{$amount}}">
                    </td>

                    <tr  >
                      <td colspan="4" align="right"> Credit Sales Fules </td>
                      <?php 
                       $famout=0;
                      foreach ($fuels as $fuel) {
                       $famout=$fuel->item_price+$famout;
                      }?>
                      <td align="right"> <input id="Fule" data-am="{{$famout}}" disabled class="r_reading"  type="text" value="{{$famout}}">
                        <input  name="Credit_fuel" type="hidden" value="{{$famout}}">
                    </td>
                    <tr  >
                       <?php 
                       $famout=0;
                      foreach ($ather as $athers) {
                       $famout=$athers->item_price+$famout;
                      }?>

                      <td colspan="4" align="right"> Credit Sales Others </td>
                       <input  name="Credit_lube" type="hidden" value="{{$famout}}">
                      <td align="right"> <input  id="Items"  data-am="{{$famout}}" disabled class="r_reading" type="text" value="{{$famout}}">
                    </td>
                    
                   </tr>
              </table>

              <table>
                 <tr>
                  <th>Payment mode</th> 
                  <th>amount</th>
                </tr>
                
                     <?php $str=''; ?>
                @foreach($PaymentModel as $Payment)

                  <?php $str.=','.$Payment->id;?>
                 <tr>
                  <td><input  type="hidden" name="paymentmode_{{$Payment->id}}" value="{{$Payment->id}}">{{$Payment->name}}</td>
                   <td><input class="cashamount" type="text" name="payment_{{$Payment->id}}"></td>
                 </tr>
                 @endforeach

                 <tr>
                  <input type="hidden" name="paymentmode" value="{{$str}}">
                  <td>Total Amount </td>
                   <td><input type="text" disabled id="totle" name="totle"> </td>
                 </tr>
                  <td> Different </td>
                   <td><input type="text" disabled id="different" > <input type="hidden"  id="Diff" name="Diff"></td>
                 </tr>

              </table>

            <input type="hidden" name="padestal" value="{{$str}}">

               <div class="form-group">
                <label class="control-label col-sm-8" ></label>
                    <div class="col-sm-3">
                     <input type="button" id="success-btn" class="btn btn-primary site-btn submit-button  pull-right" value="Submit"></center>

                    </div>
               </div>
     
             </form>
             @else
                @if(Auth::user()->user_type!=3)
                  <p> Start Shift First</p>
                 @endif
             @endif

            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script type="text/javascript">
    jQuery(function(){
      var app={
        init:function(){
          jQuery('.cmr_reading').on('keyup',function(){
              var start=jQuery(this).data('start');
              var val=jQuery(this).val();
             jQuery(this).parents('.parentcmr').find('.r_reading').val(val-start);
          });
          jQuery('#success-btn').on('click',function(){
              var flag=false;
              jQuery('.cmr_reading').each(function(){
                  var start=jQuery(this).data('start');
                  var val=jQuery(this).val();
                   
                  if(parseInt(val)>=parseInt(start)){
                    flag=true;
                  }else{
                    flag=false;
                    alert('Nozzle Close is Always Greater of Nozzle Open');
                    return false;
                  }
              });
            
            if(flag)
              jQuery('#form1').submit();
          });

       jQuery('#siftname').on('change',function(){
          jQuery('#form2').submit();
       });

       jQuery('.cashamount').on('keyup',function(){
           var amount=0;
           jQuery('.cashamount').each(function(){
                 if(jQuery.trim(jQuery(this).val())!='')
                amount=parseInt(jQuery(this).val())+amount;

            });
           jQuery('#totle').val(amount);

           
           var Fule= jQuery('#Fule').val();

            var totale= jQuery('#totale').val();

           var di=totale-Fule;
           var dm=di-amount;
           jQuery('#different').val(dm.toFixed(2));
            jQuery('#Diff').val(dm.toFixed(2));
           
       })
       app.getajex();
        },
        getajex:function(){
           jQuery('#shift_manager').on('change',function(){
                jQuery('#parentsalseman').hide();
                var id=jQuery(this).val();
                jQuery.get('getShiftOfManager/'+id,{
                
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){
                    
                      opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#siftname').html(opt);
                   
               });
              
            });
        },

        
      }
      app.init();
    });
</script>

@endsection