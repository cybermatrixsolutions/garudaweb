@extends('backend-includes.app')

@section('content')

<div class="content-wrapper">
  <div class="page-title">
    <div>
      <h1><i class="fa fa-dashboard"></i>&nbsp;Settlement</h1>
    </div>
    <div>
      <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

        <li><a href="#">Settlement</a></li>
      </ul>
    </div>

  </div>
  <div  style="margin-top: 41px;">
    <center>
      @if(Session::has('success'))
      <font style="color:red">{!!session('success')!!}</font>
      @endif
    </center>
  </div>  

  @if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif

  <div class="">
    <div class="col-md-10 col-md-offset-1 card" style="padding:10px!important;">
      <form class="form-horizontal" id="form2" action="{{route('settlement')}}" method="get" >
        

        <!-- <input type="hidden" name="type" value="1"> -->
        @if(Auth::user()->user_type==3)
        <label>Select shift Manager</label>&nbsp;
        <select style="width:20%; display:inline!important;" class="form-control" id="shift_manager" name="shift_manager"  required="required">
          @foreach($shiftmanager as $shiftmanagers)
          <option @if($id==$shiftmanagers->id) selected @endif value="{{$shiftmanagers->id}}">{{$shiftmanagers->Personnel_Name}}</option>
          @endforeach
        </select>&nbsp;&nbsp;
         @endif

        @if($pendin->count()>0)
        <label id="siftname1">Select shift</label>&nbsp; 
        <select style="width:40%; display:inline !important;" name="siftname" class="form-control"  id="siftname">
          @foreach($pendin as $pendshift)
          <option @if($Shiftid==$pendshift->id)selected @endif value="{{$pendshift->id}}">{{date('d/m/Y  h:i:s A',strtotime($pendshift->created_at))}}  To {{date('d/m/Y  h:i:s A',strtotime($pendshift->closer_date))}}</option>
          @endforeach
        </select>
       
        @endif
        @if(Auth::user()->user_type==3)
        <input type="submit" class="btn btn-primary abhishek" value="Go">
        @endif

      </form>
    </div>
  </div>


  <!--end add box-->
  <!--list contant-->
  <div class="" id="parentsalseman">
    <div class="table-responsive" style="padding:10px 0;">
     <div class="col-md-10 col-md-offset-1 card" style="padding:30px">
      @if($Shift!=null)
    <?php

      $RO_Code=$Shift->ro_code;
      $Shift_ids=$Shift->id;

      $date_created1 = $Shift->created_at;

      $date_created=date("Y-m-d", strtotime($Shift->transation_date));


     $price = DB::table('tbl_item_price_list')
         ->join('tbl_price_list_master', 'tbl_price_list_master.id', 'tbl_item_price_list.item_id')
         ->join('tbl_stock_item_group', 'tbl_stock_item_group.id', 'tbl_price_list_master.Stock_Group')
         ->select('tbl_item_price_list.price as price')
         
         ->whereDate('tbl_item_price_list.effective_date',$date_created)
         ->where('tbl_price_list_master.RO_Code',$RO_Code)
         ->where('tbl_stock_item_group.Group_Name','FUEL')
         ->value('price');


?>
@if($Shift->type!=2)
@if($price==null)

<script>
  alert("Please fill price first of <?php echo $date_created; ?> date.");
window.location = "{{url('fuel_price_management')}}";</script>
@else
<?php
 // dd($date_created, $Shift_ids);
/* $price_update = DB::table('tbl_ro_nozzle_reading')
         ->join('tbl_pedestal_nozzle_master as t1', 'tbl_ro_nozzle_reading.Nozzle_No', 't1.Nozzle_Number')
         ->join('tbl_pedestal_nozzle_master as t2', 'tbl_ro_nozzle_reading.RO_code', 't2.RO_code')
          ->join('tbl_price_list_master', 'tbl_price_list_master.id', 't1.fuel_type')
         ->join('tbl_item_price_list', 'tbl_item_price_list.item_id', 't1.fuel_type')
         ->join('shifts', 'shifts.id', 'tbl_ro_nozzle_reading.shift_id')
         ->select('tbl_ro_nozzle_reading.id as ids', 'tbl_item_price_list.price as prices','tbl_price_list_master.Item_Name as itemName')
         ->whereDate('shifts.created_at', $date_created)
         ->whereDate('tbl_item_price_list.effective_date', $date_created)
         ->where('shifts.id',$Shift_ids)
          ->where('tbl_ro_nozzle_reading.RO_code',$RO_Code)
          ->distinct()
         ->get();
        dd($price_update);*/

    

          $price_update =DB::select( DB::raw("update tbl_ro_nozzle_reading t1 inner join tbl_pedestal_nozzle_master t2 
                        on t1.Nozzle_No=t2.Nozzle_Number and t1.RO_code=t2.RO_code
                        inner join tbl_price_list_master t3 on t3.id=t2.fuel_type
                        inner join tbl_item_price_list t4 on t4.item_id=t3.id
                        set t1.price=t4.price
                        where date(t4.effective_date)='$date_created' and t1.shift_id='$Shift_ids' ") );





/*foreach ($price_update as $key => $value) {
  
  DB::table('tbl_ro_nozzle_reading')->where('id',$value->ids)->update(['price' =>$value->prices]);
}*/
?>
@endif
@endif

      <form class="form-horizontal" id="form1" action="{{route('settlement')}}" method="post"  onsubmit="return confirm('Do you want to Continue?');">
        {{ csrf_field() }}
        <?php $str='';
        $amount=0;
        $netamount=0;
        $testamount=0;
        $totalprices=0;
        ?>
        <input type="hidden" name="type" value="{{$Shift->type}}">
        <input type="hidden" value="{{$id}}" name="shift_manager">
        <input type="hidden" name="shift_id" value="{{$Shift->id}}">
        @if($Shift->type!=2)

          <table width="100%" style="margin-left: 50px">
     
          <table " style="margin-left: 50px">
          <tr>

            <th  style=" width:10%;"> Nozzle No.</th> 
           <!--  <th>Fuel Type</th> -->
            <th style="width:22%;text-align: center;">OMR</th>
            <th style=" width:22%;text-align: center;">CMR</th>
            <th style=" width:18%; text-align: center;">Test Volume</th>
            <th style="width:18%; text-align: right;">Net Sales QTY</th>
          </tr>
        </table>
        @foreach($Shift->getPadestal as $padestal)

        <p><b>Pedestal No. : {{$padestal->Pedestal_Number}}</b></p>
      
          <?php $str.=','.$padestal->Pedestal_Number; ?>


         <?php

         $padData=$padestal->getNozzle->where('is_active',1)->sortBy('Nozzle_Number');
         $padDataFuelWise=$padData->groupBy('fuel_type');
         ?>

          
          @foreach($padDataFuelWise as $key => $padValueFuelWise)

          <?php $itemName=DB::table('tbl_price_list_master')
                  ->where('RO_code',$padestal->RO_code)
                  ->where('id',$key)
                  ->value('Item_Name');

                  ?>


           <h5 style="font-weight: 900;">{{$itemName}}</h5>
              <input type="hidden" name="ro_code" value="{{$padestal->RO_code}}">


          @foreach($padValueFuelWise as $getNozzle)
          <?php $nore=$getNozzle->getReading()->where('shift_id',$Shift->id)->first();
          ?>

          <tr class="parentcmr" >
             <table width="100%" style="margin-left: 50px">
            <td  style="width:10%;"><input name="{{trim($padestal->Pedestal_Number)}}_Nozzle_No[]"  type="hidden" value="{{$getNozzle->Nozzle_Number}}">{{$getNozzle->Nozzle_Number}}</td> 
           <!--  <td>{{$getNozzle->getItem->Item_Name}}</td> -->



            <?php $sb=$nore['Nozzle_End']-$nore['Nozzle_Start'];?>

            <?php 
            $netamount=($nore['reading']*$price)+$netamount;

            $testamount=($nore['reading']*$price)+$testamount;
            ?>

            <td  style="width:18%;"><input name="{{trim($padestal->Pedestal_Number)}}_Nozzle_start_{{trim($getNozzle->Nozzle_Number)}}" type="hidden"  value="{{$nore['Nozzle_Start']}}">{{number_format((float)$nore['Nozzle_Start'], 3, '.', '')}}</td>

            <?php  $chet = $nore['Nozzle_Start']-$nore['Nozzle_Start'];?>
            <td  style="width:18%;"><input name="{{trim($padestal->Pedestal_Number)}}_Nozzle_End_{{trim($getNozzle->Nozzle_Number)}}" class="cmr_reading" type="hidden" value="@if($nore['Nozzle_End']=='') {{$getNozzle->Opening_Reading}} @else {{$nore['Nozzle_End']}} @endif" data-start="@if($nore['Nozzle_Start']=='') {{$getNozzle->Opening_Reading}} @else {{$nore['Nozzle_Start']}} @endif">@if($nore['Nozzle_End']=='') {{$getNozzle->Opening_Reading}} @else {{number_format((float)$nore['Nozzle_End'], 3, '.', '')}} @endif</td>

            <td  style="width:18%;"> <input style="text-align:right; background:transparent!important; border:transparent!important;  width:100%; " disabled type="hidden" value="{{$nore['test']}}">{{number_format((float)$nore['test'], 3, '.', '')}} </td>

            <?php  //$chetqty1 = $nore['test'];
      
            $chetqty1=number_format((float)$nore['test'], 3, '.', ''); 
      
                   $chetqty=number_format((float)$sb-$chetqty1, 3, '.', ''); 
            ?>

            <td  style="width:18%;"> <input style="text-align:right; background:transparent!important; border:transparent!important;  width:100%;" disabled type="hidden" value="{{$nore['reading']*$price}}"> {{$chetqty}}</td>
             </table>
          </tr>
           @endforeach

          @endforeach
        </table>
        @endforeach

        <h4>FUEL TYPEWISE NET SALES</h4>
        <table width="100%">
          <input type="hidden" name="ro_code" value="{{$padestal->RO_code}}">
         <input type="hidden" name="shift_id" value="{{$Shift->id}}">


         
          <tr>
            <th>Fuel Type</th>
            <th style="text-align:right; width:25%;">Net Sales QTY</th>
            <th style="text-align:right; width:25%;">Rate</th>
            <th style="text-align:right; width:25%;">Total</th>
          </tr>
          <?php $str.=','.$padestal->Pedestal_Number; 


          ?>
          

          <?php 
          $shift_id=$Shift->id;
          $shift_manager=$Shift->shift_manager;



          $variable = DB::table('tbl_ro_nozzle_reading')
          ->join('tbl_pedestal_nozzle_master', TRIM('tbl_ro_nozzle_reading.Nozzle_No') , TRIM('tbl_pedestal_nozzle_master.Nozzle_Number'))
          ->join('tbl_price_list_master', 'tbl_price_list_master.id', 'tbl_pedestal_nozzle_master.fuel_type')
        //  ->join('tbl_item_price_list', 'tbl_price_list_master.id', 'tbl_item_price_list.item_id')
          ->join('shift_pedestal', 'shift_pedestal.pedestal_id', 'tbl_pedestal_nozzle_master.Pedestal_id')
          ->join('shifts', 'shifts.id', 'shift_pedestal.shift_id')
          ->select('tbl_ro_nozzle_reading.Nozzle_No', DB::raw('(tbl_ro_nozzle_reading.Nozzle_End - tbl_ro_nozzle_reading.Nozzle_Start ) - tbl_ro_nozzle_reading.test as qty'),'tbl_ro_nozzle_reading.reading','tbl_ro_nozzle_reading.price','tbl_ro_nozzle_reading.Nozzle_End','tbl_price_list_master.item_name')

            // ->where('tbl_ro_nozzle_reading.Reading_by',$shift_manager)
         // ->where('tbl_item_price_list.is_active',1)
          //->where('tbl_price_list_master.is_active',1)
         // ->where('tbl_pedestal_nozzle_master.is_active',1)
          ->where('shifts.id',$shift_id)
          ->where('shifts.fuel',0)
          ->where('shifts.shift_manager',$shift_manager)
          ->where('tbl_ro_nozzle_reading.shift_id',$shift_id)
          ->whereNotNull('tbl_ro_nozzle_reading.Nozzle_End')
          ->distinct()
          ->get();

          //dd($variable);

            // dd($variable);
          $Item_Name='';
          
          $item_name2 = array();
          ?>
          @foreach($variable as $value)
          <?php   
          $item_name[] = $value->item_name;
          $item_name2 =  array_unique($item_name);

          ?>
          @endforeach  
          @foreach($item_name2 as $item_name_new)
          <?php
          $chetqty_previous=0;

          ?>
          @foreach($variable as $variabless)
          <?php


          if ($item_name_new == $variabless->item_name) {
            $Item_Names = $variabless->item_name;
            $chetqty_previous = ($variabless->qty)+($chetqty_previous);

            $chetqty_previous=number_format((float)$chetqty_previous, 3, '.', '');

            $price = $variabless->price;
          }
          ?>
          @endforeach  

          <tr>
            <td>{{$Item_Names}}</td>

            <td style="text-align:right;"> <input style="text-align:right; background:transparent!important; border:transparent!important;  width:100%;" disabled type="hidden" value=""> {{$chetqty_previous}}</td>

            <td style="text-align:right;">{{$price}}</td>


            <?php  

            $totalprice = ($price)*($chetqty_previous);


            ?>
            <td style="text-align:right;">{{number_format($totalprice, 2, '.', '')}}</td>

          </tr>
          <?php 
            $totalprices = $totalprice+$totalprices;
            $totalprices = number_format((float)$totalprices, 2, '.', '');
          ?>
          @endforeach



        </table>
@endif
        <table  width="100%" style="margin-top: 15px;">
         <?php 


         $shift_id = $Shift->id;


         $var = DB::table('tbl_customer_transaction')
         ->join('tbl_price_list_master', 'tbl_customer_transaction.item_code', 'tbl_price_list_master.id')
         ->join('tbl_stock_item_group', 'tbl_stock_item_group.id', 'tbl_price_list_master.Stock_Group')
         ->select(DB::raw('sum(tbl_customer_transaction.petroldiesel_qty*tbl_customer_transaction.item_price) AS total_sales'))
         ->where('tbl_stock_item_group.Group_Name','LUBES')
         ->where('tbl_customer_transaction.shift_id',$shift_id)
         ->where('tbl_customer_transaction.petrol_or_lube',2)
         ->get();

         $vara = DB::table('tbl_customer_transaction')
         ->join('tbl_price_list_master', 'tbl_customer_transaction.item_code', 'tbl_price_list_master.id')
         ->join('tbl_stock_item_group', 'tbl_stock_item_group.id', 'tbl_price_list_master.Stock_Group')
         ->select(DB::raw('sum(tbl_customer_transaction.petroldiesel_qty*tbl_customer_transaction.item_price) AS total_sales'))
         ->where('tbl_stock_item_group.Group_Name','OTHERS')
         ->where('tbl_customer_transaction.shift_id',$shift_id)
         ->where('tbl_customer_transaction.petrol_or_lube',2)
         ->get();

         ?>


          <?php 
              $fuelamout=0;
              foreach ($fuels as $fuel) {

               $fuelamout=($fuel->item_price*$fuel->petroldiesel_qty);
             }?>
         @foreach($var as $vars)

         <tr >
           <td align="right">
             <label>Total Lubes Sale:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <td  style="text-align:right;  width:100px;">
              <input type="hidden" id="total_lubes_sales" value="{{number_format($vars->total_sales, 2, '.', '')}}">
              {{number_format($vars->total_sales, 2, '.', '')}}</td></td>
           </tr>
           @endforeach
           @foreach($vara as $vars1)

           <tr >
             <td align="right">
               <label>Total Other Sales:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <td style="text-align:right;  width:100px;">
                <input type="hidden" id="total_other_sales" value="{{number_format($vars1->total_sales, 2, '.', '')}}">
                {{number_format($vars1->total_sales, 2, '.', '')}}</td></td>
             </tr>
             @endforeach

           </table>

           
           <?php
           // dd($totalprices);
           
           $total_lubes_sales = number_format((float)$vars1->total_sales, 2, '.', '');
           $total_other_sales = number_format((float)$vars->total_sales, 2, '.', '');
           $netamount = number_format((float)$totalprices+($total_lubes_sales+$total_other_sales), 2, '.', '');
           // dd($totalprices);

           ?>


           <table width="100%" style="margin-top: 15px;"> 
            <tr>
              <td align="right" style=""> 
                <label>Total Sales:</label>&nbsp;&nbsp;
                <input  style="text-align:right;  width:100px;" name="Nozzle_Amount" readonly="readonly" class="r_reading" id="totale" style="text-align: right;" data-am="{{$netamount}}" type="text" value="{{$netamount}}">
                <input style="text-align:right;   width:100px;"   type="hidden" value="{{$netamount}}">
                <input style="text-align:right;   width:100px;" name="Nozzle_test"  type="hidden" value="{{$testamount}}">
                <input style="text-align:right;   width:100px;" name="Nozzle_grass"  type="hidden" value="{{$amount}}">
              </td>

             

           </table>
          
           <table width="100%" align="right" style=": ;">


             <tr >
              <td align="right"></td>
              <td align="right" ><label style="margin-right:40px!important;"> Payment mode:
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ref No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Garruda Receipt &nbsp;&nbsp;&nbsp; Manual Receipt &nbsp;&nbsp;&nbsp; Amount:</label></td> 

            </tr>
            <?php      
            $customer_transaction = DB::table('tbl_customer_transaction')
            ->select(DB::raw('sum(petroldiesel_qty*item_price) AS total_sales'),'id')
            ->where('shift_id',$shift_id)
            ->where('cust_name','credit')
            ->where('trans_mode','auto')
            ->first();

            $customer_transaction_manual = DB::table('tbl_customer_transaction')
            ->select(DB::raw('sum(petroldiesel_qty*item_price) AS total_sales'),'id')
            ->where('shift_id',$shift_id)
            ->where('cust_name','credit')
            ->where('trans_mode','SLIPS')
            ->first();

            $previous_multi1 = 0;
            ?>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    
          <?php 

          $customer_transactions1 = number_format($customer_transaction->total_sales, 2, '.', '');
          $customer_transactions_manual1 = number_format($customer_transaction_manual->total_sales, 2, '.', '');
                // dd($customer_transaction);
          $customer_transactions_total1 = $customer_transactions1 +  $customer_transactions_manual1;
          ?>

          <tr>
            <td align="right">
            </td>
            <td align="right">
              <input  style="text-align:right; padding-right:50px; background:transparent!important; width:100px;" type="hidden"  value="">&nbsp;&nbsp;&nbsp;Credit Sales
              <input  style="text-align:right; padding-right:50px; background:transparent!important; width:100px;" type="hidden"  name="" value="">&nbsp;&nbsp;&nbsp;
              <input  style="text-align:right;" type="text" name="">

              <input type="text" readonly="" class="cashamounts" id="garruda_receipt" style="text-align:right; background-color:#ebebe4; width:110px;"  type="text" name="garruda_receipt" value="{{$customer_transactions1}}">

              <input type="text" class="" id="manual_receipt"  readonly="" name="manual_receipt" style="text-align:right; background:transparent!important; width:105px;"  value="{{$customer_transactions_manual1}}">

              <input style="text-align:right; background-color:#ebebe4; width:103px;" class="cashamount numeriDesimal" style="text-align:right;" type="text" readonly="" id="totle_value" name="" value="{{$customer_transactions_total1}}">
            </td>

          </tr>




          <?php $str=''; 

          $customer_transaction1 = DB::table('tpl_payment_mode') 
          ->leftJoin('tbl_customer_transaction', function($join) use ($shift_id){
            $join->on('tpl_payment_mode.id','=','tbl_customer_transaction.payment_mode')
            ->where('tbl_customer_transaction.shift_id','=',$shift_id);
          })
          ->select(DB::raw('sum(tbl_customer_transaction.petroldiesel_qty*tbl_customer_transaction.item_price) AS total_sales'),'tpl_payment_mode.id','tpl_payment_mode.name')
          ->where('tpl_payment_mode.IsActive',1)
          ->where('tpl_payment_mode.ro_code',$RO_Code)
          ->groupBy('tpl_payment_mode.name')
          ->orderBy('tpl_payment_mode.order_number')
          ->get();

$cash_id=0;

$cash_arr=collect($customer_transaction1)->where('name','CASH')->pluck('id');

if(count($cash_arr)>0){

$cash_id= $cash_arr['0']; 
}


 $str2='';
          ?>

          @foreach($customer_transaction1 as $Payment)

          <script type="text/javascript">
           $(document).ready(function(){

            $(document).on("keyup", "#manual_receipt1_{{$Payment->id}}", function() {
             var manual_receip =  parseFloat($("#manual_receipt1_{{$Payment->id}}").val());

             var garruda_receip =  parseFloat($("#garruda_receipt1_{{$Payment->id}}").val());

             var totle1 =  parseFloat($("#totle").val());

             var sub_totale1 =  parseFloat($("#totale").val());

             var totle_valu = parseFloat(manual_receip+garruda_receip);

             var totle = parseFloat(totle_valu+totle1);

             var sub_totale = parseFloat(sub_totale1-totle);

                if(isNaN(totle_valu)){
                    totle_valu=0;
                }

             $("#totle_value1_{{$Payment->id}}").val(totle_valu.toFixed(2));
              var amount=0;
         jQuery('.cashamount').each(function(){
           if(jQuery.trim(jQuery(this).val())!='')
            amount=parseFloat(jQuery(this).val())+amount;

          });
         
         var sub_totale1 =  parseFloat($("#totale").val());
       
         var totalexp=parseFloat($("#id-totalempAmount").val());
         var totaldeposit=parseFloat($("#id-deposit-amount").val());
         var cashAmount =$('#manual_receipt1_{{$cash_id}}').val();
         var depositAndExpense=totalexp+totaldeposit;

           console.log('amount' + amount);
         console.log('totalexp ' + totalexp);
         console.log('totaldeposit ' + totaldeposit);
         console.log('total sale ' + sub_totale1);
         console.log('cashAmount ' + cashAmount);

          finalamount=amount-depositAndExpense;
           var diff=finalamount-sub_totale1;

           console.log('finalamount ' + finalamount);
         if(parseFloat(depositAndExpense)>parseFloat(cashAmount)){
       

            $('#success-btn').prop('disabled', true);
            alert('Expense amount can not greater than Cash ');
                     return false;


         }else{
           $('#success-btn').prop('disabled', false);
         }
     
         console.log('diff' + diff);
         jQuery('#different').val(diff.toFixed(2));
         jQuery('#totle').val(finalamount.toFixed(2));



           });
          });


        </script> 
        <?php 
        $customer_transactions10 = number_format($Payment->total_sales, 2, '.', ''); 
        $customer_transactions12 = number_format($customer_transaction->total_sales, 2, '.', ''); 
        $final_total = number_format($customer_transactions_total1+$customer_transactions12, 2, '.', '');

        ?>
        <?php $str2.=','.$Payment->id;?>


        <tr>
          <td></td>
          <td align="right">
            <input  style="text-align:right; padding-right:50px; background:transparent!important; width:100px;" type="hidden" name="paymentmode_{{$Payment->id}}" value="{{$Payment->id}}">{{$Payment->name}}&nbsp;&nbsp;&nbsp;

            <input  style="text-align:right;" type="text" name="ref_nom_{{$Payment->id}}" value="">

            <input type="text" id="garruda_receipt1_{{$Payment->id}}" readonly="readonly" style="text-align:right; background-color:#ebebe4; width:110px;" @if(trim(strtolower($Payment->name))=='cash') @endif type="text" name="garruda_receipt1_{{$Payment->id}}" value="{{$customer_transactions10}}">

            <input class="cashamounts" id="manual_receipt1_{{$Payment->id}}" style="text-align:right; background:transparent!important; width:105px;" @if(trim(strtolower($Payment->name))=='cash') @endif type="text" name="manual_receipt1_{{$Payment->id}}" value="">


            <input style="text-align:right; background-color:#ebebe4; width:103px;" class="cashamount numeriDesimal " style="text-align:right;" id="totle_value1_{{$Payment->id}}" type="text" name="payment_{{$Payment->id}}"  readonly="readonly" value="{{number_format($customer_transactions10, 2, '.', '')}}">
          </td>

        </tr>
        @endforeach
        <?php 
        // dd($netamount, $final_total);
        $different = number_format(($final_total)-($netamount), 2, '.', '');

        ?>
      

       </table>

       <input type="hidden" name="padestal" value="{{$str}}">

       @if($Shift->getTanksReading!=null && $Shift->getTanksReading->count()>0)

       <table width="100%" align="left" >
        <tr> 
          <th><h3>  Dip Readings</h3> </th>



        </tr>

        <tr> 
          <th>Tank Name </th>
          <th>Fuel Type </th>
          <th>Unit of Measure </th>
          <th>Reading</th>


        </tr>

        @foreach($Shift->getTanksReading as $getTanksReading)
        <tr> 
          <td>{{$getTanksReading->getTank->Tank_Number}} </td>
          <td>{{$getTanksReading->getfueltypes->Item_Name}} </td>
          <td>{{$getTanksReading->getunitmesure->Unit_Symbol}} </td>
          <td align="right">{{$getTanksReading->value}}</td>

        </tr>
        @endforeach

      </table>
      @endif

<section id="expense-details">
<div>
  <table width="100%">
    <div class="col-md-12">

       <div class="col-md-4">

         
      
      </div>

      <div class="col-md-6">

          <label class="control-label ">Expenses Details: </label>   
      
      </div>



    </div>

   
    <tr>


      <td>
         <form id="myForm">
      <table width="50%" align="right"  >


          <tr>

     
            <th > 
               <label class="control-label " >Reference :</label>
              <input type="text" name="reference" class="form-control " id="id-reference" placeholder="Enter Reference">
            </th>
            <th style=" ">  
                   <label class="control-label "style="text-align:left; "> Exp. Amount :</label>
              <input type="number" class="form-control" name="exp_amount" id="id-amount" placeholder="Enter Amount">
            </th>
            <th style="">

             <div class="control-label "style="height:10px; ">
              <a  id="id_add_row" href="#" style="background: #00786a;
                  color: #fff;
                   padding: 6px 10px;
  
                   border-radius: 2px;
                 margin-left: 8px;"><i class="fa fa-plus-square-o"></i></a>
                 </div>
         
            </th>
          
          </tr>

       </table>
     </form>
  
      </td>
    </tr>
    <tr>
      <td>
      <table width="50%" align="right" id="id-datatable-expense" style="text-align: right">
          
          <tr>
            <th>Reference</th>
            <th style="text-align:right; width:25%;">Exp. Amount</th>
            <th style="text-align:right; width:25%;">Action</th>
          
          </tr>


            <tbody>
            </tbody>

       </table>

     
  
      </td>
</tr>
</table>
   
</div>
      
</section>
  <table width="100%" align="right" >

      <tr>
         
          <td></td> <td align="right" style="margin-top: 5px;"><br><label>Total Expense:</label>&nbsp;&nbsp;
            <input style="text-align:right;  width:100px;" type="text" style="text-align:right;" id="id-totalempAmount" name="total_exp_amount" readonly>
          </td>
        </tr>

      <tr>
        <td></td>




                                <table width="100%">
                                  <div class="col-md-12">
                                     <div class="col-md-4">
             
                                    </div>
                                    <div class="col-md-6">

                                        <label class="control-label ">Deposit Details Amount : </label>   
                                    
                                    </div>
                                  </div>
           
                                  <tr>
                                  <td>
                                       <form id="myForm">
                                    <table width="50%" align="right"  >
                                      <tr>

                                   
                                          <th > 
                                             <label class="control-label " >Reference       :</label>
                                            <input type="text" name="reference_deposit" class="form-control " id="id-reference_deposit" placeholder="Enter Reference">
                                          </th>
                                          <th style=" ">  
                                                 <label class="control-label "style="text-align:left; "> Dep. Amount :</label>
                                            <input type="number" class="form-control" name="exp_amount_deposit" id="id-amount_deposit" placeholder="Enter Amount">
                                          </th>
                                          <th style="">

                                           <div class="control-label "style="height:10px; ">
                                            <a  id="id_add_row_deposit" href="#" style="background: #00786a;
                                                color: #fff;
                                                 padding: 6px 10px;
                                
                                                 border-radius: 2px;
                                               margin-left: 8px;"><i class="fa fa-plus-square-o"></i></a>
                                               </div>
                                       
                                          </th>
                                        
                                        </tr>

                                     </table>
                                   </form>
                                
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                    <table width="50%" align="right" id="id-datatable-deposit" style="text-align: right">
                                        
                                        <tr>
                                          <th>Reference</th>
                                          <th style="text-align:right; width:25%;"> Dep . Amount</th>
                                          <th style="text-align:right; width:25%;">Action</th>
                                        
                                        </tr>


                                          <tbody>
                                          </tbody>

                                     </table>

                                   
                                
                                    </td>
                              </tr>
                              </table>
                             



                  </tr>

</table>

 <table width="100%" align="right">


      <tr>

        
          <td></td>
          <td align="right" style="margin-top: 5px;"><br><label>Total Deposit Amount:</label>&nbsp;&nbsp;
            <input style="text-align:right;  width:100px;" type="text" style="text-align:right;" id="id-deposit-amount" name="deposit_amount_total" readonly>
          </td>
        </tr>
       
        <tr>
          <input type="hidden" name="paymentmode" value="{{$str2}}">
          <td></td>
          <td align="right" style="margin-top: 5px;"><br><label>Total Amount:</label>&nbsp;&nbsp;
            <input style="text-align:right;  width:100px;" type="text" style="text-align:right;" readonly id="totle" name="totle" value="{{number_format($final_total, 2, '.', '')}}">
          </td>
        </tr>
        <tr>
          <td></td>
          <td align="right"><label> Short/Excess:</label>&nbsp;&nbsp;
           <input style="text-align:right;   width:100px;" type="text" style="text-align:right;" readonly id="different" name="Diff" value="{{number_format($different, 2, '.', '')}}">
            </td>
         </tr>



       </table>
      


      <div class="">
        <label class="control-label col-sm-12" ></label>

        <input style="margin-top:15px;" type="button" id="success-btn" class="btn btn-primary site-btn submit-button  pull-right" value="Submit"></center>

      </div>
    </form>
  </div>
  @else
  @if(Auth::user()->user_type!=3)
  <p> Start Shift First</p>
  @endif
  @endif

</div>
</div>
</div>
</div>
</div>

@endsection
@section('script')
<script>




  $(document).ready(function(){
        $("#id_add_row").click(function(){

                     var cashAmount =$('#manual_receipt1_{{$cash_id}}').val();
                     var totalVal =$('#totle_value1_{{$cash_id}}').val();
                     var reference = $("#id-reference").val();
                     var exp_amount = $("#id-amount").val();
                       console.log('cashAmount  - ' + cashAmount);
                       console.log('exp_amount  - ' + exp_amount);



                         if(!cashAmount){

                                 alert('Cash not avilable!.');

                                    return false;


                           }else{


                           if(parseFloat(cashAmount)<parseFloat(exp_amount)){

                                 alert('Expense amount can not greater than Cash ');

                                    return false;

                           }


                           }


            var remain_cash=parseFloat(cashAmount)-parseFloat(exp_amount);
            var remain_total=parseFloat(totalVal)-parseFloat(exp_amount);

           // $('#manual_receipt1_{{$cash_id}}').val(remain_cash.toFixed(2));
            //$('#totle_value1_{{$cash_id}}').val(remain_total.toFixed(2));

            //check old expense total

            if(exp_amount){

            var markup = "<tr><td style='text-align:left'><input name='references[]' value=' " + reference +" ' readonly></td><td><input name= exp_amount[] value='"+ exp_amount +"' readonly>  </td><td><button type='button' style='background-color:#009587' class='removebutton btn-success' data-amountvalue1="+ exp_amount +">  <i class='fa fa-remove'></i></button></td></tr>";
            $("#id-datatable-expense").append(markup);

            }

             var table = $("#id-datatable-expense tbody");
             var totalExpAmount=0;
             console.log('totalExpAmount  - ' + totalExpAmount);
            table.find('tr').each(function (i, el) {
                      var tds = $(this).find('td');
                      var amount;
                      amount = tds.eq(1).find('input').val();
                      console.log('amount  - ' + amount);
                      if(amount){
                           totalExpAmount= parseFloat(totalExpAmount)+parseFloat(amount);
                         }
                       console.log('totalExpAmount  - ' + totalExpAmount);
                      
                          $("#id-reference").val("");
                          $("#id-amount").val("");
              
                  });

              $('#id-totalempAmount').val(totalExpAmount);

              var amount=0;
             jQuery('.cashamount').each(function(){
               if(jQuery.trim(jQuery(this).val())!='')
                amount=parseFloat(jQuery(this).val())+amount;

              });

             var sub_totale1 =  parseFloat($("#totale").val());
            totaldeposit=parseFloat($("#id-deposit-amount").val());
           if(!totaldeposit){

              var totaldeposit=0;
             
           }
             var cashAmount =$('#manual_receipt1_{{$cash_id}}').val();
             var totalexp=parseFloat($("#id-totalempAmount").val());
                
             var depositAndExpense=totalExpAmount+totaldeposit;
              finalamount=amount-depositAndExpense;
              var diff=finalamount-sub_totale1;
               console.log('finalamount ' + finalamount);
             if(depositAndExpense>cashAmount){
              $('#success-btn').prop('disabled', true);
                alert('Expense amount can not greater than Cash ');
                         return false;


             }else{
               $('#success-btn').prop('disabled', false);
             }
 
             console.log('diff' + diff);

             jQuery('#different').val(diff.toFixed(2));
             jQuery('#totle').val(finalamount.toFixed(2));
              
            });
     // Find and remove selected table rows
     $(document).on('click', 'button.removebutton', function () { 

                  amount = $(this).data('amountvalue1');
              
                var totalExpAmount= $('#id-totalempAmount').val();
                var cashAmount =$('#manual_receipt1_{{$cash_id}}').val();
                var reamin_total_exp=parseFloat(totalExpAmount)-parseFloat(amount)
                $('#id-totalempAmount').val(reamin_total_exp);

               var amount=0;
               jQuery('.cashamount').each(function(){
               if(jQuery.trim(jQuery(this).val())!='')
                amount=parseFloat(jQuery(this).val())+amount;

              });

             var sub_totale1 =  parseFloat($("#totale").val());
           var totaldeposit=0;
             totaldeposit=parseFloat($("#id-deposit-amount").val());
           if(!totaldeposit){

              var totaldeposit=0;
             
           }

             var depositAndExpense=reamin_total_exp+totaldeposit;



              finalamount=amount-depositAndExpense;
             var diff=finalamount-sub_totale1;

               console.log('reamin_total_exp '+reamin_total_exp);
                console.log('totaldeposit '+totaldeposit);
                console.log('depositAndExpense '+depositAndExpense);
                console.log('amount '+amount);
            console.log('finalamount ' + finalamount);
             if(parseFloat(depositAndExpense)>parseFloat(cashAmount)){
              $('#success-btn').prop('disabled', true);
                alert('Expense amount can not greater than Cash ');
                         return false;
             }else{
               $('#success-btn').prop('disabled', false);
             }
 
             console.log('diff' + diff);

             jQuery('#different').val(diff.toFixed(2));
             jQuery('#totle').val(finalamount.toFixed(2));
      
      
        $(this).closest('tr').remove();



     return false;
 });
               }); 



  //deposit amount  


   jQuery("#id-deposit-amount").blur(function() { 

                    var  deposit_amount=$("#id-deposit-amount").val();


                     var cashAmount =$('#manual_receipt1_{{$cash_id}}').val();
                     if(isNaN(cashAmount)){
                          cashAmount="";
                      }
                     var totalVal =$('#totle_value1_{{$cash_id}}').val();
                  
                       console.log('cashAmount  - ' + cashAmount);
                      



                         if(!cashAmount){

                                 alert('Cash not avilable!.');

                                    return false;


                           }


                         if(parseFloat(cashAmount)<parseFloat(deposit_amount)){

                                 alert('Deposit Amount amount can not greater than Cash ');

                                    return false;


                           }




                           //

                             var amount=0;
               jQuery('.cashamount').each(function(){
               if(jQuery.trim(jQuery(this).val())!='')
                amount=parseFloat(jQuery(this).val())+amount;

              });





             var sub_totale1 =  parseFloat($("#totale").val());
          

            totaldeposit=parseFloat($("#id-deposit-amount").val());
           if(!totaldeposit){

              var totaldeposit=0;
             
           }

              var totalExpAmount= $('#id-totalempAmount').val();
            

             var depositAndExpense=parseFloat(totalExpAmount)+parseFloat(totaldeposit);



              finalamount=amount-depositAndExpense;
             var diff=finalamount-sub_totale1;

                console.log('totalExpAmount '+totalExpAmount);
                console.log('totaldeposit '+totaldeposit);
                console.log('depositAndExpense '+depositAndExpense);
                console.log('amount '+amount);
                console.log('finalamount ' + finalamount);
             if(parseFloat(depositAndExpense)>parseFloat(cashAmount)){
              $('#success-btn').prop('disabled', true);
                alert('Expense amount can not greater than Cash ');
                         return false;
             }else{
               $('#success-btn').prop('disabled', false);
             }
 
             console.log('diff' + diff);

             jQuery('#different').val(diff.toFixed(2));
             jQuery('#totle').val(finalamount.toFixed(2));


          

      
   });


</script>

<script type="text/javascript">


  $(document).ready(function(){

          $("#id_add_row_deposit").click(function(){

                     var cashAmount_deposit =$('#manual_receipt1_{{$cash_id}}').val();
                     var totalVal =$('#totle_value1_{{$cash_id}}').val();
                     var reference_deposit = $("#id-reference_deposit").val();
                     var exp_amount_deposit = $("#id-amount_deposit").val();
                       console.log('cashAmount_deposit  - ' + cashAmount_deposit);
                       console.log('exp_amount_deposit  - ' + exp_amount_deposit);



                         if(!cashAmount_deposit){

                                 alert('Cash not avilable!.');

                                    return false;


                           }else{

                                 if(parseFloat(exp_amount_deposit)>parseFloat(cashAmount_deposit)){

                                     alert('Expense amount can not greater than Cash ');

                                        return false;
                               }
                           }

            var remain_cash=parseFloat(cashAmount_deposit)-parseFloat(exp_amount_deposit);
            var remain_total=parseFloat(totalVal)-parseFloat(exp_amount_deposit);

           // $('#manual_receipt1_{{$cash_id}}').val(remain_cash.toFixed(2));
            //$('#totle_value1_{{$cash_id}}').val(remain_total.toFixed(2));

            //check old expense total

            if(exp_amount_deposit){

            var markup1 = "<tr><td style='text-align:left'><input name='references_deposit[]' value=' " + reference_deposit +" ' readonly></td><td><input name =deposit_amount[] value='"+ exp_amount_deposit +"' readonly>  </td><td><button type='button' style='background-color:#009587' class='removebutton_deposit btn-success' data-amountvalue2="+ exp_amount_deposit +">  <i class='fa fa-remove'></i></button></td></tr>";
            $("#id-datatable-deposit").append(markup1);

            }

             var table_deposit = $("#id-datatable-deposit tbody");
             var totatDepositAmount=0;            
          
            table_deposit.find('tr').each(function (i, el) {
                      var tds = $(this).find('td');
                      var depamount;
                      depamount = tds.eq(1).find('input').val();
                      console.log('depamount  - ' + depamount);
                      if(depamount){
                           totatDepositAmount= parseFloat(totatDepositAmount)+parseFloat(depamount);
                         }
                       
                      
                          $("#id-reference_deposit").val("");
                          $("#id-amount_deposit").val("");
              
                  });

            console.log('totatDepositAmount  - ' + totatDepositAmount);

             // $('#id-totalempAmount').val(totalExpAmount);
             $('#id-deposit-amount').val(totatDepositAmount);
           var totalExpAmount=$('#id-totalempAmount').val();
           if(!totalExpAmount){

            totalExpAmount=0;
           }


              var amount=0;
             jQuery('.cashamount').each(function(){
               if(jQuery.trim(jQuery(this).val())!='')
                amount=parseFloat(jQuery(this).val())+amount;

              });

             var sub_totale1 =  parseFloat($("#totale").val());
            //totaldeposit=parseFloat($("#id-deposit-amount").val());
           if(!totatDepositAmount){

              var totatDepositAmount=0;
             
           }
             var cashAmount =$('#manual_receipt1_{{$cash_id}}').val();
             
                
             var depositAndExpense=parseFloat(totalExpAmount)+parseFloat(totatDepositAmount);
              finalamount=amount-depositAndExpense;

             console.log('totalExpAmount  - ' + totalExpAmount);
             console.log('depositAndExpense  - ' + depositAndExpense);
              var diff=finalamount-sub_totale1;
               console.log('finalamount ' + finalamount);
             if(parseFloat(depositAndExpense)>parseFloat(cashAmount)){
              $('#success-btn').prop('disabled', true);
                alert('Expense amount can not greater than Cash.');
                         return false;


             }else{
               $('#success-btn').prop('disabled', false);
             }
 
             console.log('diff' + diff);

             jQuery('#different').val(diff.toFixed(2));
             jQuery('#totle').val(finalamount.toFixed(2));
              
            });
     // Find and remove selected table rows
     $(document).on('click', 'button.removebutton_deposit', function () { 

                  amount = $(this).data('amountvalue2');
                   var totaldeposit=parseFloat($("#id-deposit-amount").val());
                   if(!totaldeposit){

                      var totaldeposit=0;
                     
                   }

                    var totalExpAmount=$('#id-totalempAmount').val();
                     if(!totalExpAmount){

                      totalExpAmount=0;
                     }
                        
               // var totalExpAmount= $('#id-totalempAmount').val();
                //var totalDepAmount= $('#id-deposit-amount').val();
                var cashAmount =$('#manual_receipt1_{{$cash_id}}').val();
                var reamin_total_deposit=parseFloat(totaldeposit)-parseFloat(amount)

                $('#id-deposit-amount').val(reamin_total_deposit);

               var amount=0;
               jQuery('.cashamount').each(function(){
               if(jQuery.trim(jQuery(this).val())!='')
                amount=parseFloat(jQuery(this).val())+amount;

              });

             var sub_totale1 =  parseFloat($("#totale").val());
        
            

             var depositAndExpense=parseFloat(reamin_total_deposit)+parseFloat(totalExpAmount);



              finalamount=amount-depositAndExpense;
             var diff=finalamount-sub_totale1;

                console.log('reamin_total_deposit '+reamin_total_deposit);
                console.log('totalExpAmount '+totalExpAmount);
                console.log('depositAndExpense '+depositAndExpense);
                console.log('amount '+amount);
                console.log('finalamount ' + finalamount);
             if(parseFloat(depositAndExpense)>parseFloat(cashAmount)){
              $('#success-btn').prop('disabled', true);
                alert('Expense amount can not greater than Cash. ');
                         return false;
             }else{
               $('#success-btn').prop('disabled', false);
             }
 
             console.log('diff' + diff);

             jQuery('#different').val(diff.toFixed(2));
             jQuery('#totle').val(finalamount.toFixed(2));
      
      
        $(this).closest('tr').remove();



     return false;
 });
  

});

</script>











<script type="text/javascript">
  jQuery(".numeriDesimal").keyup(function() { var $this = jQuery(this); $this.val($this.val().replace(/[^\d.]/g, '')); });
  jQuery(function(){
    var app={
      init:function(){
        jQuery('.cmr_reading').on('keyup',function(){
          var start=jQuery(this).data('start');
          var val=jQuery(this).val();
          jQuery(this).parents('.parentcmr').find('.r_reading').val(val-start);
        });
        jQuery('#success-btn').on('click',function(){

              //var flag=false;
              var flag=true;
              jQuery('.cmr_reading').each(function(){

                var start=jQuery(this).data('start');
                var val=jQuery(this).val();

                if(parseInt(val)>=parseInt(start)){
                  flag=true;
                     var button = $('#success-btn');
                    // Disable the submit button 
                    button.prop('disabled', true);
                }else{
                  flag=false;

                  alert('Nozzle Close is Always Greater of Nozzle Open');
                  return false;
                }
              });

              if(flag)
                jQuery('#form1').submit();
            });

        jQuery('#siftname').on('change',function(){
          jQuery('#form2').submit();
        });

        jQuery('.cashamounts').on('change keyup blur',function(){

         var amount=0;
         jQuery('.cashamount').each(function(){
           if(jQuery.trim(jQuery(this).val())!='')
            amount=parseFloat(jQuery(this).val())+amount;

        });


              totaldeposit=parseFloat($("#id-deposit-amount").val());
           if(!totaldeposit){

              var totaldeposit=0;
             
           }

              var totalExpAmount= $('#id-totalempAmount').val();
               if(!totalExpAmount){

              var totalExpAmount=0;
             
             }

            

             var depositAndExpense=parseFloat(totalExpAmount)+parseFloat(totaldeposit);

             
      

              finalamount=amount-depositAndExpense;



            console.log('totaldeposit '+totaldeposit);
            console.log('totalExpAmount '+totalExpAmount);
            console.log('finalamount '+finalamount);

         jQuery('#totle').val(finalamount.toFixed(2));

         var Fule= jQuery('#Fule').val();

         var totale= parseFloat(jQuery('#totale').val());

         var di=totale;
         var dm=finalamount-di;
         jQuery('#different').val(dm.toFixed(2));
         jQuery('#Diff').val(dm.toFixed(2));

       })
        app.getajex();
      },
      getajex:function(){
       jQuery('#shift_manager').on('change',function(){

        jQuery('#siftname').hide();
        jQuery('#siftname1').hide();
        jQuery('#parentsalseman').hide();
        var id=jQuery(this).val();
        jQuery.get('getShiftOfManager/'+id,{

          '_token': jQuery('meta[name="csrf-token"]').attr('content'),
        },function(data){
          var opt='';
          jQuery.each(data, function(index,value){

            opt+='<option value="'+index+'">'+value+'</option>';
          });
          console.log(opt);

          jQuery('#siftname').html(opt);

        });

      });
     },


   }
   app.init();
 });
</script>

@endsection
@section('style')
<style>
.card {
  padding-top: 10px !important;
}
</style>
@endsection