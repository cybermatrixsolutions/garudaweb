<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Customer Update</h1>
      </div>
      <div>
        <ul class="breadcrumb">
           <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('/Customer')}}">Customer </a></li>
        </ul>
      </div>
    </div>
  
  
                          
  
  
  
  
    <div class="row">

      <div class="col-md-12 col-sm-12">

                      <form class="form-horizontal" action="{{url('customer_update')}}/{{$customer_mangement_data->id}}" enctype="multipart/form-data" method="post"  onsubmit="return confirm('Do you want to Continue?');">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           <!-- <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>-->
            
            <div class="row" style="margin-top:-15px !important;">
          
                    <div class="">  <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                             </div>
          
            
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="customer_ro_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
                      <!-- <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Pucmp Legal Name</label>
                          <div class="col-sm-9">
                              <select class="form-control" style="" required="required" name="customer_ro_code" id="sel1">

                                                     @foreach($datas as $ro_master)
                                                        <option name="company_id" value="{{$ro_master->RO_code}}"
                                                                @if($customer_mangement_data->RO_code == $ro_master->RO_code) selected @endif>{{$ro_master->pump_legal_name}}</option>
                                                    @endforeach
                            </select>
                          </div>
                        </div> -->
                     <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Customer Code</label>
                          <div class="col-sm-9">
                             <input type="text" required="required" class="form-control" id="" name="Customer_Code" value="{{$customer_mangement_data->Customer_Code}}" placeholder="Customer Code" readonly>
                          </div>
                        </div> -->
                     
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Customer Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" required="required" class="form-control" id="" name="company_name" value="{{$customer_mangement_data->company_name}}"  placeholder="Customer Name">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Contact Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" required="required" class="form-control" id="" name="coustomer_name"  value="{{$customer_mangement_data->Customer_Name}}"  placeholder="Customer Name">
                          </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-3" for="pwd">Gender <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control" required="required" name="gender">
                             <option value="Male"  @if($customer_mangement_data->gender == "Male") selected @endif>Male</option>
                             <option value="Female" @if($customer_mangement_data->gender == "Female") selected @endif>Female</option>
                           </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Email/Login ID <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="email" class="form-control" id="" required="required" name="email" value="{{$customer_mangement_data->Email}}"  placeholder="Email">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Phone Number</label>
                          <div class="col-sm-9">
                             <input type="number" class="form-control" id="" name="Phone_Number" value="{{$customer_mangement_data->Phone_Number}}"  placeholder="Phone Number">
                          </div>
                        </div>
                       
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Mobile(+91) <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" required="required" class="form-control number" id="mobile" name="mobile_no" value="{{$customer_mangement_data->Mobile}}"  maxlength="10"  placeholder="Mobile No.">
                          </div>
                        </div>

                        
                      <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Customer Security Deposit</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" id="CustomerDeposit" name="CustomerDeposit" value="{{$customer_mangement_data->CustomerDeposit}}"  placeholder="">
                          </div>
                        </div> 
                        
                          
                           <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Address Line 1 <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <textarea cols="2" required="required"  class="form-control" name="address_one">{{$customer_mangement_data->address_one}}</textarea>
                          </div>
                          </div>
                           <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Address Line 2</label>
                          <div class="col-sm-9">
                             <textarea cols="2" class="form-control" name="address_two">{{$customer_mangement_data->address_two}}</textarea>
                          </div>
                          </div>
                           <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Address Line 3</label>
                          <div class="col-sm-9">
                             <textarea cols="2" class="form-control" name="address_three">{{$customer_mangement_data->address_three}}</textarea>
                          </div>
                          </div>
                            
                        </div>
                        </div>
                        <div class="col-md-6">
                             <div class="row" style="margin-top:-15px !important;">
                              <div class="form-group">
                          <label class="control-label col-sm-3" for="">Aadhaar No</label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" name="aadhaar_no" id="aadhaar_no" value="{{$customer_mangement_data->Aadhaar_no}}" placeholder="Aadhaar No">
                          </div>
                          </div><div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Aadhaar Photo </label>
                          <div class="col-sm-9">
                            <input type="file"  class="form-control" name="aadhaar_img" id="aadhaar_img" placeholder="Aadhaar photo">
                            <br><img src="{{URL::asset('')}}/{{$customer_mangement_data->Aadhaar_img}}" style="width: 250px; height: 150px;">
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Country</label>
                          <div class="col-sm-9">
                             <select class="form-control" disabled="disabled" id="country" name="country">
                               @foreach($countries_list as $country)
                                <option  value="{{$country->id}}"
                                        @if($customer_mangement_data->country==$country->id) selected @endif >{{$country->name}}</option>
                                   @endforeach
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">State <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control" id="state" name="state" data-v="{{$customer_mangement_data->state}}">
                                @foreach($state_list as $state)
                                <option  value="{{$state->id}}"
                                        @if($customer_mangement_data->state==$state->id) selected @endif >{{$state->name}}</option>
                                   @endforeach
                             </select>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">City</label>
                          <div class="col-sm-9">
                             <select class="form-control" id="city" name="city" data-v="{{$customer_mangement_data->city}}">
                                @foreach($city_list as $city)
                                <option  value="{{$city->id}}"
                                        @if($customer_mangement_data->city==$city->id) selected @endif >{{$city->name}}</option>
                                   @endforeach
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">PIN Code <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" required="required" value="{{$customer_mangement_data->pin_no}}"  pattern="[0-9]{6}" class="form-control" name="pin_no" id="" placeholder="PIN Code">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">VAT</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control" name="vat"  value="{{$customer_mangement_data->vat}}" placeholder="vat">
                          </div>
                        </div>
                      <!--    <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Customer Security Deposit(rs.)</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control" name="Security_Deposit"  value="{{$customer_mangement_data->  CustomerDeposit}}" placeholder="Security Deposit">
                          </div>
                        </div> -->
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Industry Vertical <span style="color:red;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control"  name="industry_vertical" id="industry">
                             @foreach($Industry as $Industrys)
                              <option value="{{$Industrys->value}}" @if($Industrys->value==$customer_mangement_data->industry_vertical) selected @endif>{{$Industrys->value}}</option>
                             @endforeach            
                             </select>
                           
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Department <span style="color:red;">*</span></label>
                          <div class="col-sm-9">
                             <select class="form-control"  name="Department">
                               @foreach($Department as $Departments)
                              <option value="{{$Departments->value}}" @if($Departments->value==$customer_mangement_data->Department) selected @endif>{{$Departments->value}}</option>
                             @endforeach
                                    
                             </select>
                           
                          </div>
                        </div>
                       
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">PAN </label>
                          <div class="col-sm-9">
                             <input type="text" id="pan"  style="text-transform: uppercase;"  value="{{$customer_mangement_data->pan_no}}"  pattern="[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}" class="form-control" name="pan_no"  placeholder="PAN">
                          </div>
                        </div>

<input type="hidden" name="" value="{{$customer_mangement_data->state}}" id="getstatecode" ng-bind="name">
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">GSTIN</label>
                          <div class="col-sm-9" style="">
                         
                               <div class="col-sm-8" style="padding-right: 0px">
                              <input type="text" readonly  required="required"  style="text-transform: uppercase;" value="" class="form-control" id="codeviews" required/>
                              <input type="hidden" readonly  required="required" value="" class="form-control" id="getsta"  name="gst_no" data-table="tbl_customer_master" data-colum="gst_no"  data-mass="GST number Already Exit" required/>
                            </div>
                              <div class="col-sm-4" style="padding-left: 0px">
                             <input type="text"  value="{{$customer_mangement_data->gst_code}}" class="form-control GST_TIN" id="gst" name="GST_ddTIN" placeholder="GST No." maxlength="3"  /><br></div>
                        
                        </div> 
                      </div>
                         
                         <!--  <div class="form-group">
                          <label class="control-label col-sm-3" for="email">GSTIN</label>
                          <div class="col-sm-9">
                             <input type="text" id="gst"  style="text-transform: uppercase;" value="{{$customer_mangement_data->gst_no}}"  pattern="[0-9]{2}[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}[(0-9)(a-z)(A-Z)]{2,4}" class="form-control" name="gst_no"  placeholder="GST No. ">
                          </div>
                        </div> -->
                             <!-- <div class="form-group">
                          <label class="control-label col-sm-5" for="email">GSTIN </label>
                          <div class="col-sm-7">
                            <div class="row">
                               <div class="col-sm-8" style="padding-right: 0px">
                              <input type="text" readonly  required="required"  style="text-transform: uppercase;" value="" class="form-control" id="codeviews" required/>
                              <input type="hidden" readonly  required="required" value="" class="form-control" id="getsta"  name="gst_no" data-table="tbl_customer_master" data-colum="gst_no"  data-mass="GST number Already Exit" required/>
                            </div>
                              <div class="col-sm-4" style="padding-left: 0px">
                             <input type="text"  class="form-control GST_TIN" id="gst" name="GST_ddTIN" placeholder="GST No." maxlength="3" /><br></div>
                              
                            </div>
                          </div>
                        </div>
                          -->
                          
                        <div class="form-group">
            
                          <div class="col-sm-offset-3 col-sm-9">
                           <!-- <button type="submit" class="btn btn-default">Submit</button>-->
              <center><!--<a href="{{url('Customer')}}" class="btn btn-primary">Back</a>--><input type="submit" id="success-btn" class="btn btn-primary site-btn" value="Submit"  onclick="return validmobile()"></center>
                          </div>
                        </div>
           
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>

    </div>
  
  <div id="gethomeurl" data-url="{{url('/')}}">
  </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script>


  
  function validateFloatKeyPress() {
    var v = document.getElementById("creidt").value;
    if(v.length>7){
      alert("Please under 0-9999999 value");
     document.getElementById("buttontn").disabled = true;
    }
    else{
       document.getElementById("buttontn").disabled = false;
    }

 
}
function numberofdecimal() {
    var v = document.getElementById("decimal").value;
    if(v.length>2){
      alert("Please under 0-99 value");
     document.getElementById("buttontn").disabled = true;
    }
    else{
       document.getElementById("buttontn").disabled = false;
    }

 
}
  function  validmobile() {
       var mobile = document.getElementById("mobile").value;
        var pattern = /^[0]?[789]\d{9}$/;

     
        var count=0;
        if (!pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
            alert("It is not valid mobile number.input 10 digits number!");
            count=count+1;
          
        }

         var gst = document.getElementById("gst").value;

 

         if(gst){

         

           var pan_no = document.getElementById("pan").value;

           if(!pan_no){
            

             alert("Pan Number is mandatory for GST Number!!!");
            count=count+1;
           }
         }

         if(count>0){

           return false;

         }else{

          return true;
         }
    }
  $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
  $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})

</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
            vil.setvat();
             vil.selectRo();
              vil.getcom();
                vil.getvertical();

                 
         
                vil.getstatecode();
            

               jQuery('form').on('submit',function(){
                  
                  if($("#gst").val() == '') {
                    
                    return true;
                  }

                  else{

                 var gst=jQuery('#gst').val();
                   var gst=gst.toUpperCase();
                   var pan=jQuery("#pan").val();
                   var pan=pan.toUpperCase();
                   gst= gst.substring(2,12);

                 /*  alert('gst '.gst);
                   alert('pan '.pan);

                   if(gst!=pan){
                     alert('Please Enter valid GST');
                      jQuery("#success-btn").attr('disabled',false);

                      return false;
                   }
*/
                  }
                   

                   

              });
               jQuery('#CustomerDeposit').on('keyup',function(){ 
                    var l=0; var l2=0; var ival=jQuery(this).val(); 
                    if(typeof(ival.split('.')[1]) != "undefined" && ival.split('.')[1] !== null) 
                      l=ival.split('.')[1].length; 
                      l2=ival.split('.')[0].length;
                     if(jQuery.trim(ival)=='' || l>2 ||l2>7 || parseInt(ival)== 0){ 
                      jQuery(this).css('border-color','red'); 
                      flg=false; jQuery("#success-btn").attr('disabled',true); 
                    }else{ 
                      jQuery("#success-btn").attr('disabled',false);
                       jQuery(this).css('border-color',''); }
                       });

           },

           setvat:function(){

                      
                        //var sta=jQuery('#pan').val(); 
                          vil.getstatecode();
                        var  sta=jQuery('#getstatecode').val()+jQuery('#pan').val()+jQuery('#gst').val();
                        var  views=jQuery('#getstatecode').val()+jQuery('#pan').val();
                        alert(sta);
                       
                     
                      jQuery('#getsta').val(sta);
                      jQuery('#codeviews').val(views);

                      
           },



           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
              jQuery('#industry').on('change',function(){
                    vil.getvertical();
                });
                jQuery('#pan').keyup(function(){
                vil.setvat();
                 vil.getstatecode();
                });


                 jQuery('#state').on('change',function(){
                
                  vil.getstatecode();
                  vil.setvat();
                });
           
           },
           getvertical:function(){
             
               jQuery.get("{{url('get_vertical')}}",{
                
                industry:jQuery('#industry').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                 
                  var selc= jQuery('#departement').data('dep');
                  jQuery.each(data, function(index,value){
                    
                    if(selc==index)
                     opt+='<option selected value="'+index+'">'+value+'</option>';
                   else
                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#departement').html(opt);
                  
               });
           },
           getcom:function(){
             
               jQuery.post("{{url('getrocode')}}",{
                
                rocode:jQuery('#sel1').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#customer_code').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get("url('getregierids')",{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
               });
           },

           getstatecode:function(){



           $.ajax({
               'url': "{{route('getstategstcoded')}}",
               'type': 'POST',
               'headers': {
                       'X-CSRF-TOKEN': "{{ csrf_token() }}"
               },
               'data': {
                       'state': jQuery('#state').val(),
                      

               },
             })
       .success(function(data) {

            jQuery('#getstatecode').val(data);
                    
                        var  sta=jQuery('#getstatecode').val()+jQuery('#pan').val()+jQuery('#gst').val();
                        var  views=jQuery('#getstatecode').val()+jQuery('#pan').val();
                       
                     
                      jQuery('#getsta').val(sta);
                      jQuery('#codeviews').val(views);
                      
          
       })
       .fail(function() {

        jQuery('#getstatecode').val("");
            
       });
       



             
          /* jQuery.post('/getstateGstcoded',{

                    state:jQuery('#state').val(),

                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                },function(data){
                  

                    alert(data);
                    jQuery('#getstatecode').val(data);
                    
                        var  sta=jQuery('#getstatecode').val()+jQuery('#pan').val()+jQuery('#gst').val();
                        var  views=jQuery('#getstatecode').val()+jQuery('#pan').val();
                       
                     
                      jQuery('#getsta').val(sta);
                      jQuery('#codeviews').val(views);
                      

                   
                });*/
               /*  var state_id=$('#state').val();
               
                    jQuery('#getstatecode').val(state_id);

                    var getstatecode=$('#getstatecode').val();
                   */
            },

           setvat:function(){

                         
                        //var sta=jQuery('#pan').val(); 
                        var  sta=jQuery('#getstatecode').val()+jQuery('#pan').val()+jQuery('#gst').val();
                        var  views=jQuery('#getstatecode').val()+jQuery('#pan').val();

                        console.log(sta);

                        console.log(views);
                       
                     
                      jQuery('#getsta').val(sta);
                      jQuery('#codeviews').val(views);

                      
           },

     }
     vil.init();
  });
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();

           },
           selectRo:function(){
            jQuery('#country').on('change',function(){
                  vil.getcom();
              });
            jQuery('#state').on('change',function(){
                  vil.getcrg();
              });
           
           },
           getcom:function(){
             
               jQuery.get("{{url('get_country_state')}}",{
                
                rocode:jQuery('#country').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                 
                  var selc=jQuery('#state').data('v');
                  jQuery.each(data, function(index,value){

                    if(selc==index)
                     opt+='<option selected value="'+index+'">'+value+'</option>';
                    else
                      opt+='<option value="'+index+'">'+value+'</option>';

                  });
                    console.log(opt);
                   
                   jQuery('#state').html(opt);

                   vil.getcrg();
               });
           },
           getcrg:function(){
            
             jQuery.get("{{url('get_state_city')}}",{
                rocode:jQuery('#state').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                 var selc=jQuery('#city').data('v');
                 var opt='';

                   if(data.length==0)
                   {

                     opt+='<option value="'+jQuery('#state option:selected').val()+'">'+jQuery('#state option:selected').text()+'</option>';
                   }
                   else{


                  jQuery.each(data, function(index,value){

                     if(selc==index)
                     opt+='<option selected value="'+index+'">'+value+'</option>';
                    else
                      opt+='<option value="'+index+'">'+value+'</option>';

                  });
                    console.log(opt);
                   }
                jQuery('#city').html(opt);

               });
           },

     }
     vil.init();
  });
</script>
</body>
</html>