<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
  #errmsg
{
color: red;
}
</style>
<title>Garruda</title>

</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  

  <!-- Side-Nav-->

  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Payment Method</h1>
      </div>
      <div>
        <ul class="breadcrumb">
         <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Payment Method</a></li>
        </ul>
      </div>
    </div>  
 
      <div class="">
         <center>@if(Session::has('success'))
                       <font style="color:red">{!!session('exitdata')!!}</font>
                    @endif</center>
      </div>

    <div class="">
              
      <div class="col-md-12 col-sm-12">
        <div class="row">
           <center>@if(Session::has('success'))
                       <font style="color:red">{!!session('success')!!}</font>
                    @endif</center>
                    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif    
<div class="vat"><div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div></div>   
@if($MasterPayment->count()>0)
              <label>Select Your Payment Method</label>

                <form action="{{url('mastersavepayment')}}" method="post" onsubmit="return confirm('Do you want to Continue?');"> 
                    <div class="row" style="margin:0px!important;">
                        {{ csrf_field() }}
                        @foreach($MasterPayment as $MasterPayments)
                          <div class="col-md-4">
                          <div class="card" style="padding:5px 15px 5px 15px; margin-bottom:8px;">
                                       <input style="margin-right:15px;" type="checkbox" name="names[]" value="{{$MasterPayments->name}}">{{$MasterPayments->name}}
                              </div>
                          </div>
                        @endforeach
                    </div>
                    <div class="col-md-12">
                          <input type="submit" name="" class="btn btn-primary" value="Submit">
                    </div>
                </form>
               
             @endif
             
           
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
             
          
            
            <!-- Modal> -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Add Payment Mode  </h4>
                  </div>  
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal"   action="{{url('save_paymentmode')}}" method="post" onsubmit="return confirm('Do you want to Continue?');">
                      {{ csrf_field() }}
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                           
                            <input type="text" class="form-control"  required="required" name="name" placeholder="Payment Mode"/> 
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-sm-4" for="email">Payment Type <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-8">
                             <select class="form-control" name="mode_type">

                                  <option value="cash">Cash</option>
                                  <option value="credit card">Credit Card</option>
                                  <option value="wallet"> Wallet</option>
                                  <option value="bank transfer">Bank Transfer</option>
                                  <option value="cheque">Cheque</option>

                             </select>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-8">
                            <!--<button type="submit" class="btn btn-default">Submit</button>-->
                           <center><input type="submit"  class="btn btn-primary  submit-button"  value="Submit"></center>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
         <label>Payment Methods</label>
			<div>
				<br/>
				<div><strong>Please Drag the payment mode to set the order</strong></div>
				<br/>
				<ul id="sortablePaymentMode" style="margin-top:10px;padding:0px;">
					@foreach($PaymentModel as $paymentmodes)
						<li  id="item-{{$paymentmodes->id}}" class="card" style="padding:8px 15px 8px 15px; margin-bottom:8px;list-style:none;width:70%;">
							<?php if($paymentmodes->IsActive!=0){?>
						   <a style="padding-right: 10px;"  title="Deactive" href="{{route('active.paymentmode',['id'=>$paymentmodes->id,'act'=>0])}}" onclick="return confirm('Do you want to deactivate?');">

							<img src="{{URL::asset('images')}}/test-pass-icon.png" >

							</a>
						   
						  <?php }else{ ?>

						  
							<a style="padding-right: 10px;" title="Active" href="{{route('active.paymentmode',['id'=>$paymentmodes->id,'act'=>1])}}" onclick="return confirm('Do you want to activate?');">
							  
							   <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">
							</a>

							<?php } ?>
							{{$paymentmodes->name}}
						</li>
					@endforeach	
				</ul>
			
				<!--
			  @foreach($PaymentModel as $paymentmodes)
				<div class="col-md-4">
				<div class="card" style="padding:5px 15px 5px 15px; margin-bottom:8px;">
					  <?php if($paymentmodes->IsActive!=0){?>
					   <a style="padding-right: 10px;"  title="Deactive" href="{{route('active.paymentmode',['id'=>$paymentmodes->id,'act'=>0])}}" onclick="return confirm('Do you want to deactivate?');">

						<img src="{{URL::asset('images')}}/test-pass-icon.png" >

						</a>
					   
					  <?php }else{ ?>

					  
						<a style="padding-right: 10px;" title="Active" href="{{route('active.paymentmode',['id'=>$paymentmodes->id,'act'=>1])}}" onclick="return confirm('Do you want to activate?');">
						  
						   <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">
						</a>

				 <?php } ?> {{$paymentmodes->name}}
					</div>
				</div>
			  @endforeach
					-->   
			</div>
		</div>
    </div>
  </div>
</div>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src="{{URL::asset('js/capitalize.js')}}"></script>

<script type="text/javascript">
jQuery(function(){
   jQuery('#type1').on('change',function(){
        var a = document.getElementById("type2");
    var b = document.getElementById("type1");

    if (a.options[a.selectedIndex].value == b.options[b.selectedIndex].value) {
        alert("Please Choose Select unit symble");
         jQuery('.site-btn').attr('disabled',true);
    } else{
       jQuery('.site-btn').attr('disabled',false);


    }
   }); 
});
</script>
<script type="text/javascript">
  $(function() {
     $('.row_dim').hide(); 
    $('#type').change(function(){
        if($('#type').val() == 'Compound') {
            $('.row_dim').show(); 
             //jQuery('#utint').css("display","none")
        } else {
            //$('#utint').show();
            jQuery('.row_dim').css("display","none")
        } 
    });
    
});
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>

<script type="text/javascript">
  
 $(document).ready(function () {
  //called when key is pressed in textbox
  $("#quantity").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});
</script>
<script>
  jQuery(function(){
     var url="{{url('check_Pedestal_Number_unique')}}";
     var vil={
           init:function(){
             vil.selectRo();
             vil.gstCodeChk();
				
				$('#sortablePaymentMode').sortable({
					axis: 'y',
					update: function (event, ui) {
						var sorted = $(this).sortable('serialize');
						console.log(sorted);
						
						$.ajax({
							headers: {
								'X-CSRF-TOKEN': '{{ csrf_token() }}'
							},
							data: sorted,
							type: 'POST',
							url: "{{route('updatePaymentModeOrder')}}",
							dataType : 'json',
							success : function(response){
								if(response && response.success){
									console.log(response);
								}
							},
							error : function(err){
							}
						});
						
					}
				});
           },

           gstCodeChk:function(){

            jQuery('#chk_gst_code').on('click',function(){
                  var id=0;
                   var table=jQuery('#quantity').data('table');
                   var colum=jQuery('#quantity').data('colum');
                   var mess=jQuery('#quantity').data('mess');

                   // id=jQuery(this).data('id');
                    
                  udat=jQuery('#quantity').val();

                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mess);

                   }else{
                    jQuery('#form_state').submit();
                   }
                       
                   });
              });
            
           
           },

          
           
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                   var colum=jQuery(this).data('colum');
                   var mess=jQuery(this).data('mess');

                   // id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();

                  jQuery.get(url,{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mess);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }

     vil.init();
  });
</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>