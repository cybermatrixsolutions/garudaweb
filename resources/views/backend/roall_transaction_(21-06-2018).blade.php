<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<title>Garruda</title>
<style type="text/css">
   tfoot {
    display: table-header-group;
}
</style>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
   @include('backend-includes.header')

        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; View & Alter Slip Entry </h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          
         
          <li><a href="#"> View & Alter Slip Entry</a></li>
        </ul>
      </div>
    </div>
     <div class="">
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
   @endif
      <center>@if(Session::has('success'))
          <font style="color:red">{!!session('success')!!}</font>
        @endif</center>
      
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">

        <div class="row">
          <!--  <form class="form-horizontal form-inline" name="myForm" action="{{'customerrequestfind'}}" enctype="multipart/form-data" method="post">
       
            
             {{ csrf_field() }}
        <div class="row">
         <div class="col-md-12">
         

          <div class="form-group">
             <label class="control-label"  for="email">Customer Name</label>&nbsp;&nbsp;
                 <select class="form-control"  name="customer_code" id="customer_code">
                      
                        @if(isset($RoCustomertManagement))
                      @foreach($RoCustomertManagement as $RoCustomertManagements)

                       <option value="{{$RoCustomertManagements->Customer_Code}}">{{$RoCustomertManagements->company_name}}</option>
                      @endforeach
                      @endif
                    </select>
          </div>

                 &nbsp;&nbsp;
                      <div class="form-group">
                          <label class="control-label"  for="email">Vehicle No.</label>&nbsp;&nbsp;
                          <select class="form-control" required="required" name="Vehicle_Reg_No" id="registration_number">                
                            </select>
                            &nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                          <label class="control-label"  for="email">Req.No./Slip No.</label>&nbsp;&nbsp;
                          <input type="text"  class="form-control" style="width:123px;" name="slipno" placeholder="req.No./slip no."/>
                            &nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                          <label class="control-label"  for="email">From</label>&nbsp;&nbsp;
                           <input type="text"  class="form-control datepickers" style="width:123px;" required name="fromdate" placeholder="from date"/>&nbsp;&nbsp;
                        </div>
                        <div class="form-group">
                              <label class="control-label" for="pwd">To</label>&nbsp;&nbsp;
                                <input  type="text" class="form-control datepickers" style="width:123px;" name="to_dae" placeholder="To Date"/>&nbsp;&nbsp;
                        </div>
                        <br>
                        <center>
                        <div class="form-group" style="display: none;">
                                <input type="submit" class="btn btn-default" style="width:auto;padding:8px 6px;font-size:11px;" value="Submit">
                        </div>
                        </center>
            </div>
        </div>
 </form> --> 
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Add Transaction</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
     <div class="row">
      <div class="col-md-12"> 
        <div class="col-md-10"></div>
        <div class="col-md-2"><!-- <a target="_blank" href="{{url('ro_transaction_list/export')}}" class="btn btn-primary">Export in CSV file</a> --></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive" style="height:425px;">
          <table class="table table-striped" id="myTable">
            
            <thead>
              
              <tr>
                <th>S.No.</th>
              
                <th>Customer Name</th>
                <th>Vehicle number</th>
                <th>Request Date</th>
                <th>Item Name</th>
                <th>Group</th>
				<th>Price</th>
                <th>Quantity</th>
                <th>Trans Mode</th>
                <th>Req.No./Slip No.</th>
                <th>Action</th>
              </tr>
            </thead>
             @if(count($transaction_data)!=null)
            <tfoot>
              <tr>
                <th>S.No.</th>
               
                <th>Customer Name</th>
                <th>Vehicle number</th>
                <th>Request Date</th>
                <th>Item Name</th>
                <th>Group</th>
				<th>Price</th>
                <th>Quantity</th>
                <th>Trans Mode</th>
                <th>Req.No./Slip No.</th>
               </tr>
            </tfoot>
            @endif
            <tbody>
              @if(count($transaction_data)!=null)
              <?php $i=0;?>
               @foreach($transaction_data as $list)
               <?php $i++;?>
              <tr>
                <td>{{$i}}</td>
                
                <td>@if($list->getCustomername!=null){{$list->getCustomername->company_name}} @endif</td>
                <td>{{$list->Vehicle_Reg_No}}</td>
                <td>{{Carbon\Carbon::parse($list->request_date)->format('d/m/Y')}}</td>
                <td>{{$list->getItemName['Item_Name']}}</td>
                 
                 
                   <td> @if($list->getItemName!=null && $list->getItemName->getgroup!=null){{$list->getItemName->getgroup->Group_Name}} @endif</td>
				   
				 <td style="text-align: right;">{{number_format($list->item_price,2)}}</td>
                  
                <td style="text-align: right;">{{number_format($list->petroldiesel_qty,3)}}</td> 
                <td>{{$list->trans_mode}}</td> 
                <td>{{$list->Request_id}}</td>
                
                
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                       @if($list->petrol_or_lube==1)
                      <li><a href="{{'fuelslipedit'}}/{{$list->id}}">Edit</a></li>
                      @else
                       <li><a href="{{'editlubelist'}}/{{$list->id}}">Edit</a></li>
                       @endif
                    </ul>
                  </div>
               </td>
              </tr>
              @endforeach
                @else
                
               @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
 $(document).ready(function() {
    $('#myTable').DataTable( {
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
  $(function () {
  $(".datepickers").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        defaultDate:'dd/mm/yyy',
        format: 'dd/mm/yyyy',
  }).datepicker('update', new Date());
});
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script
<script  src="js/index.js"></script> 
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>

<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
             
              vil.getcrg();
           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });
           
           },
          
           getcrg:function(){
            
             jQuery.get('getregierids',{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';
                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+value+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                jQuery('#registration_number').html(opt);
               });

           },

     }
     vil.init();
  });
</script>
</body>
</html>