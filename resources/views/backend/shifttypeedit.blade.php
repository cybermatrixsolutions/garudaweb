<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css'>

<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
  @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')
  
  
   <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Shift Type Edit</h1>
      </div>
      <div>
        <ul class="breadcrumb">
           <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="{{url('ro_transaction_list')}}">Shift Type Edit</a></li>
        </ul>
      </div>
    </div>
  
  
                           <div class="">  
                             </div>
  
  
  
  
    <div class="row" style="margin-top: 50px;">

      <div class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
           
            <div class="row" style="margin-top: -31px !important;">
             @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                @endif</center>
                      <form class="form-horizontal" action="{{url('shifttypeedit')}}/{{$Shifttype->id}}" enctype="multipart/form-data" method="post" onsubmit="return confirm('Do you really want to Continue?');">
                      {{ csrf_field() }}


                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Shift Type<span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                           <!--  <input type="text" required="required" class="form-control type" name="type" id="" placeholder="Shift Type"> -->
                           <select  name="shift_type" required="required" class="form-control type" >
                               <option value="regular" <?=$Shifttype->shift_type == 'regular' ? ' selected="selected"' : '';?>>Regular</option>

                    
                               <option value="flexi"  <?=$Shifttype->shift_type == 'flexi' ? ' selected="selected"' : '';?> >Flexi</option>

                        
                        
                           </select>
                          </div>
                        </div>
                 
                          <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Shift Name <span style="color:#f70b0b;">*</span> </label>
                          <div class="col-sm-9">
                            <input type="text"  class="form-control " style="text-transform:uppercase" value="{{$Shifttype->type}}"   name="type" id="registration_number">
                          
                                                   
                     

                             </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Start Time <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control"   value="{{$Shifttype->start_time}}" name="start_time" placeholder="Start Time ">
                          </div>
                        </div>
                        
                           <div class="form-group">
                          <label class="control-label col-sm-3" for="email">End Time <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control"   value="{{$Shifttype->end_time}}" name="end_time" placeholder="End Time">
                          </div>
                        </div>
            
                       
                        <div class="form-group">
            
                          <div class="col-sm-offset-3 col-sm-9">
                           <!-- <button type="submit" class="btn btn-default">Submit</button>-->
              <center><input type="submit" id="success-btn" class="btn btn-primary site-btn" value="Submit" onclick="return getmobilevalid()"></center>
                          </div>
                        </div>
           
                      </form>
                    </div>
          </div>
        </div>
      </div>
    </div>
  
  
  </div>
</div>
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script> 


<script type="text/javascript">



</script>
<script>
  $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
function  getmobilevalid() {
        var mobile = document.getElementById("mobile").value;
        var pattern = /^[0]?[789]\d{9}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
            return true;
        }
        alert("It is not valid mobile number.input 10 digits number!");
        return false;
    }
    $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
      function myFunction() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>



<script type="text/javascript">
  jQuery(function(){
        var url1="{{url('getrocode')}}";
        var url2="{{url('getregierids')}}";
        var url3="{{url('getietmpricebydate')}}";
        var url4="{{url('getShiftByDate')}}";
        var ty="";
     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
              // vil.getItemPrice();
              // vil.getShift();
              jQuery(".datepickers").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true,
                  defaultDate:'dd/mm/yyy',
                  format: 'dd/mm/yyyy',
            }).datepicker('update').on('changeDate', function(){
                       // currentP=jQuery(this).parents('.parent-tr');
                       vil.getShift();
                       vil.getItemPrice();
                       vil.getdate();
                    });

           },
           selectRo:function(){
            jQuery('#sel1').on('change',function(){
                  vil.getcom();
              });
            jQuery('#Fuel_Type').on('change',function(){
                   vil.getItemPrice();
              });
            jQuery('#customer_code').on('change',function(){
                  vil.getcrg();
              });

             jQuery('#registration_number').on('change',function(){
                 vil.getFuel();
              });

           },
           getcom:function(){
               jQuery.get(url1,{
                rocode:jQuery('#sel1').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                  jQuery.each(data, function(index,value){
                   var customer=jQuery('#customer_code').data('customer');
                     if(customer==index)
                        opt+='<option selected value="'+index+'">'+value+'</option>';
                      else
                      opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    // console.log(opt);
                   
                   jQuery('#customer_code').html(opt);
                   vil.getcrg();
               });
           },
           getcrg:function(){

             jQuery.get(url2,{
                rocode:jQuery('#customer_code').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var registration=jQuery('#customer_code').data('registration');
                 var opt='';
                  jQuery.each(data, function(index,value){

                     if(registration==index)
                        opt+='<option selected value="'+value+'">'+value+'</option>';
                      else
                      opt+='<option value="'+value+'">'+value+'</option>';

                  });
                  
                jQuery('#registration_number').html(opt);
                vil.getFuel();
               });
           },
              getShift:function(){
             
             var request_date=jQuery('.datepickers').val();

             if(request_date){debugger;

                 jQuery.get(url4,{
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                    'request_date':request_date,
                   },function(data){
                    // alert(data);
                      var str='';
                      jQuery.each(data,function(i,v){
                         str+='<option selected value="'+i+'">'+v+'</option>';
                      });
                     jQuery('#shift_').html(str);
                     vil.getdate();
                   });
               }
           },
             getdate:function () {
           
                    var quantity =0.000;
                    var priceitem =0.00;
                        quantity = ($('#quantity').val()?$('#quantity').val():0.000);
                        priceitem = ($('#priceitem').val()?$('#priceitem').val():0.00);
                    var value = (parseFloat(priceitem)*parseFloat(quantity));
                        $('#value').val(value.toFixed(2));
                     jQuery(document).on('keyup change', '.value', function() {
                          var quantity =0.000;
                          var priceitem =0.00;
                          quantity = ($('#quantity').val()?$('#quantity').val():0.000);
                          priceitem = ($('#priceitem').val()?$('#priceitem').val():0.00);
                        var value = (parseFloat(priceitem)*parseFloat(quantity));
                      $('#value').val(value.toFixed(2));
                      });
              },
            getItemPrice:function(){
               var item=jQuery('#Fuel_Type').val();
               
              if(ty==1){
               var request_date=jQuery('.datepickers').val();
              
                 jQuery.get(url3+'/'+item,{
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                    'request_date':request_date,
                   },function(data1){
                    var str='';
                     var flg=false;
                        if (data1) {
                         flg=true;
                         str+='<option selected value="'+data1+'">'+data1+'</option>';
                            }

                      jQuery('#priceitem').html(str);
                      vil.getdate();
                     if(flg){
                      jQuery('#success-btn').attr('disabled',false);
                     }else{
                      jQuery('#success-btn').attr('disabled',true);
                     }
                   });
               }
           },
            getFuel:function(){
              var ro=jQuery('#sel1').val();
              var cus=jQuery('#customer_code').val();
              var registration_number=jQuery('#registration_number').val();
              
             jQuery.get("{{url('getfueltype')}}/"+ro+"/"+cus+"/FUEL",{
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                registration_number:registration_number,
               },function(data){
                 var sfuel=jQuery('#Fuel_Type').data('fuel');
                 var opt='';
                  jQuery.each(data, function(index,value){
                     
                     if(sfuel==index)
                      opt+='<option selected value="'+index+'">'+value+'</option>';
                      else
                      opt+='<option value="'+index+'">'+value+'</option>';
                  });
                   
                   
                jQuery('#Fuel_Type').html(opt);
               });
           },


     }
     vil.init();
  });
</script>


</body>
</html>
