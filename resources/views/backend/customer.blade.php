<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link href="css/style.css" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  <!-- Navbar-->
  
   @include('backend-includes.header')
 
        <!-- Side-Nav-->
  
    @include('backend-includes.sidebar')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp; RO Customers</h1>
       
      </div>
      <div>
        
        <ul class="breadcrumb">
            <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">RO Customers</a></li>
        </ul>
      </div>

    </div>
   

     <div class="">
      
      <ul>
        @foreach($errors->all() as $error)
          <li style="color: red">{{ $error}}</li>
        @endforeach
      </ul>
     
      
    </div>
     
    <div class="row">
      <div class="col-md-12 col-sm-12">

        <div class="row">
           <center style="text-align: center;">@if(Session::has('success'))
          <font style="color:red;">{!!session('success')!!}</font>
        @endif</center>
          <div class="col-md-6 col-sm-7 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style=" width: 1200px;">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Customer Management</h4>
                  </div>
                  <div class="modal-body" style="padding: 0px !important;">
                    <div class="row">
                      <form class="form-horizontal" action="{{'save_customer_data'}}" method="post">
                           <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                          <label class="control-label col-sm-5" for="pwd">Company Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-6">
                             <input type="text" required="required" class="form-control" id="" name="company_name" placeholder="Company Name">
                          </div>
                        </div>
                          </div>
                          <div class="col-sm-6">
                             
                         <div class="form-group">
                          <label class="control-label col-sm-5" for="pwd">Contact Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-6">
                             <input type="text"  required="required" class="form-control" id="" name="coustomer_name"   placeholder="Customer Name">
                          </div>
                        </div>
						
						  <div class="form-group">
                          <label class="control-label col-sm-5" for="pwd">Contact Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-6">
                             <input type="text"  required="required" class="form-control" id="" name="coustomer_name"   placeholder="Customer Name">
                          </div>
                        </div>
						
						
						
						
                          </div>
                        </div>
                        <div class="col-md-4">
                           {{ csrf_field() }}
                           <input type="hidden" name="customer_ro_code" id="sel1" value="{{Auth::user()->getRocode->RO_code}}">
                        <!-- <div class="form-group">
                          <label class="control-label col-sm-5" for="pwd">Pump Legal Name</label>
                          <div class="col-sm-7">
                              <select class="form-control" required="required" name="customer_ro_code" id="sel1">
                             
                                  @foreach($datas as $ro_master)
                                      <option  value="{{$ro_master->RO_code}}"
                                              >{{$ro_master->pump_legal_name}}</option>
                                  @endforeach
                            </select>
                          </div>
                        </div> -->
                       <!--  <div class="form-group">
                          <label class="control-label col-sm-5" for="pwd">Customer Code</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="" name="Customer_Code" placeholder="Customer Code">
                          </div>
                        </div> -->
                     
                        
                         <div class="form-group">
                           <label class="control-label col-sm-5" for="pwd">Gender <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-7">
                             <select class="form-control" required="required" name="gender">
                             <option value="Female">Female</option>
                             <option value="Male">Male</option>
                           </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-5" for="pwd">Email/Login ID <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-7">
                             <input type="text" required="required" class="form-control checkunique" id="" data-table=" tbl_customer_master" data-colum="Email" data-mass="Email Already Exist" name="Email" placeholder="Email">
                          </div>
                        </div>
             
                        <div class="form-group">
                          <label class="control-label col-sm-5" for="pwd">Password <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-7">
                             <input type="password" required="required" class="form-control" id="myInput" name="password" placeholder="Password"><input type="checkbox" onclick="myFunction()">Show Password
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-5" for="pwd">Phone Number</label>
                          <div class="col-sm-7">
                             <input type="number"  class="form-control" id="" name="Phone_Number" placeholder="Phone Number">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-5" for="pwd">Mobile(+91) <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-7">
                             <input type="text" required="required" maxlength="10" class="form-control number" id="mobile" name="mobile_no" placeholder="Mobile No." >
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-5" for="email">Address Line 1 <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-7">
                             <textarea cols="1" required="required" class="form-control" name="address_one" style="height: 40px"></textarea>
                          </div>
                          </div>
                           <div class="form-group">
                          <label class="control-label col-sm-5" for="email">Address Line 2</label>
                          <div class="col-sm-7">
                             <textarea cols="1" class="form-control" name="address_two" style="height: 40px"></textarea>
                          </div>
                          </div>
                       
                       <!--  <div class="form-group">
                          <label class="control-label col-sm-5" for="pwd">IMEI No</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="" name="imei_no" placeholder="IMEI No">
                          </div>
                        </div> -->
                        
                         
                         
                        </div>
                        <div class="col-md-4">
                           
                          
                           <div class="form-group">
                          <label class="control-label col-sm-5" for="email">Address Line 3</label>
                          <div class="col-sm-7">
                             <textarea cols="1" class="form-control" name="address_three" style="height: 40px"></textarea>
                          </div>
                          </div>
                          <div class="form-group">
                          <label class="control-label col-sm-5" for="pwd">Country</label>
                          <div class="col-sm-7">
                             <select class="form-control" disabled="disabled" id="country" name="country">
                               @foreach($countries_list as $country)
                                  <option @if($country->id==101) selected @endif  value="{{$country->id}}"
                                          >{{$country->name}}</option>
                              @endforeach
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-5" for="pwd">State <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-7">
                             <select class="form-control" id="state" name="state">
                               
                             </select>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-5" for="pwd">City</label>
                          <div class="col-sm-7">
                             <select class="form-control" id="city" name="city">
                               
                             </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-5" for="email">PIN Code <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-7">
                             <input type="text" required="required" max="6" pattern="[0-9]{6}" class="form-control" name="pin_no" id="" placeholder="PIN Code">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-5" for="email">aaVAT</label>
                          <div class="col-sm-7">
                             <input type="text"  class="form-control" name="vat" id="" placeholder="vat">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-5" for="email">PAN <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-7">
                             <input type="text" required="required" style="text-transform: uppercase;" max="10" pattern="[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}" class="form-control" name="pan_no" id="pan" placeholder="PAN">
                          </div>
                          <input type="hidden" name="" value="" id="getstatecode" ng-bind="name">
                        </div>
                         </div>
                          <div class="col-md-4">
                           
                          <div class="form-group">
                          <label class="control-label col-sm-5" for="email">GSTIN <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-7">
                            <div class="row">
                               <div class="col-sm-8" style="padding-right: 0px">
                              <input type="text" readonly  required="required" value="" class="form-control" id="codeviews" required/>
                              <input type="hidden" readonly  required="required" value="" class="form-control" id="getsta"  name="gst_no" data-table="tbl_customer_master" data-colum="gst_no"  data-mass="GST number Already Exit" required/>
                            </div>
                              <div class="col-sm-4" style="padding-left: 0px">
                             <input type="text"  class="form-control GST_TIN" id="gst" name="GST_ddTIN" placeholder="GST No." maxlength="3" /><br></div>
                              
                            </div>
                          </div>
                        </div>
                          <div class="form-group">
                          <label class="control-label col-sm-5" for="Limit Value">Credit Limit (rs.) <span style="color:red;">*</span></label>
                          <div class="col-sm-7">
                            <input type="text" required="required" class="form-control numeriDesimal" maxlength="99.99" placeholder="Credit Value" name="Limit_Value" />
                          </div>.
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-5" for="Handling Fee">Handling Fee (%) <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-7">
                            <input type="text" class="form-control number" placeholder="Handling Fee" name="handlingfee" />
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-5" for="Discount">Discount (%)</label>
                          <div class="col-sm-7">
                            <input type="text"  class="form-control number" placeholder="Discount" name="Discount" />
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-sm-5" for="email">No. of Days <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-7">
                            <input type="text" required="required" class="form-control number" placeholder="No of Days" name="no_of_days" />
                          </div>
                        </div>


                          </div>
                         <hr>
                        
                        <div class="form-group">
                          <div class="col-sm-offset-6 col-sm-6">
                            <button type="submit" class="btn btn-default" onclick="return gstAndPan()">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
               @if(isset($customer_list) && count($customer_list)  != 0)
              <tr>
                <th>S.No.</th>
                <th>RO Name</th>
                <th>Contact Name</th>
                <th>Company Name</th>
                <th>Status</th>
                 <th>Actions</th>
            </thead>
            <tbody>
              <?php $i=0;?>
              @foreach($customer_list as $list)
              <?php $i++;?>
              <tr>
                <td>{{$i}}</td>
                <td scope="row">{{$list->pump_name}}</td>
                <td>{{$list->Customer_Name}}</td>
                 <td>{{$list->company_name}}</td>
                  <td> <?php if($list->is_active==1){?>

                   <a  title="Active" href="customermanagement/active/{{$list->id}}/0" onclick="return confirm('Do you want to deactivate? if you deactive this all relation will be deactivated.');"><img src="{{URL::asset('images')}}/test-pass-icon.png" style=""></a>

                  <?php }else{ ?>

                   <a title="Deactive" href="customermanagement/active/{{$list->id}}/1" onclick="return confirm('Do you want to activate?');">

                    <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                    </a>
             <?php } ?> 
          </td>
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                     @if($list->is_active==1)
                      <li><a href="customer/edit/{{$list->id}}">Edit</a></li>
                      @endif
                      <li><a href="customer/show/{{$list->id}}">View</a></li>
                     <!-- <li><a href="customer/delete/{{$list->id}}" onclick="return confirm('are you sure to delete');">Delete</a></li>-->
                    </ul>
                  </div>
               </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Javascripts--> 

<script src="js/jquery-2.1.4.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins/pace.min.js"></script> 
<script src="js/main.js"></script>
<script>
  function myFunction() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
  $('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
 jQuery(".numeriDesimal").keyup(function() {
                        var $this = jQuery(this);
                        $this.val($this.val().replace(/[^\d.]/g, ''));
                    });

</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
              vil.getcom();
               vil.setvat();
                


              jQuery('form').on('submit',function(){

                  if($("#gst").val() == '') {
                    
                    return true;
                  }

                  else{
                 //  var gst=jQuery('#gst').val();
                   var gst=gst.toUpperCase();
                   var pan=jQuery("#pan").val();
                   var pan=pan.toUpperCase();
                   gst= gst.substring(2,12);

                   if(gst!=pan){
                     alert('Please Enter valid GST');
                      jQuery("#success-btn").attr('disabled',false);

                      return false;
                   }
                 }

                   

              });

           },
           selectRo:function(){
            jQuery('#country').on('change',function(){
                  vil.getcom();
              });
            jQuery('#state').on('change',function(){
                  vil.getcrg();
              });
             jQuery('#pan').keyup(function(){
                vil.setvat();
                });
                jQuery('#gst').keyup(function(){
                vil.setvat();
                });
                jQuery('#state').on('change',function(){
                    vil.getstatecode();
                });
           
           },
           setvat:function(){

                         
                        //var sta=jQuery('#pan').val(); 
                        var  sta=jQuery('#getstatecode').val()+jQuery('#pan').val()+jQuery('#gst').val();
                        var  views=jQuery('#getstatecode').val()+jQuery('#pan').val();
                       
                     
                      jQuery('#getsta').val(sta);
                      jQuery('#codeviews').val(views);

                      
           },
           getcom:function(){
             
               jQuery.get('get_country_state',{
                
                rocode:jQuery('#country').val(),
               
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){
                var opt='';
                 

                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   
                   jQuery('#state').html(opt);
                   vil.getstatecode();
                   vil.getcrg();
               });
           },
            getstatecode:function(){
               
                jQuery.get('getstatecoded',{

                    astate:jQuery('#state').val(),

                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                },function(data){
                     console.log(data);
                    jQuery('#getstatecode').val(data);

                   
                });
            },
           getcrg:function(){
            
             jQuery.get('get_state_city',{
                rocode:jQuery('#state').val(),
                '_token': jQuery('meta[name="csrf-token"]').attr('content'),
               },function(data){

                 var opt='';

                   if(data.length==0)
                   {

                     opt+='<option value="'+jQuery('#state option:selected').val()+'">'+jQuery('#state option:selected').text()+'</option>';
                   }
                   else{


                  jQuery.each(data, function(index,value){

                     opt+='<option value="'+index+'">'+value+'</option>';
                  });
                    console.log(opt);
                   }
                jQuery('#city').html(opt);

               });
           },

     }
     vil.init();
  });
</script>

<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('.checkunique').on('blur',function(){
                  var id=0;
                  var table=jQuery(this).data('table');
                  var mass=jQuery(this).data('mass');
                   var colum=jQuery(this).data('colum');
                   //id=jQuery(this).data('id');
                    
                  udat=jQuery(this).val();
                  jQuery.get('check_customer_Email_ro_unique',{
                  table:table,
                  colum:colum,
                  unik:udat,
                  id:id,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                   
                   if(data=='true'){
                      alert(mass);
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable();
    });
     function  gstAndPan() {
        var mobile = document.getElementById("mobile").value;
        var pattern = /^[0]?[789]\d{9}$/;
        if (pattern.test(mobile)) {
           // alert("Your mobile number : " + mobile);
            return true;
        }
        alert("It is not valid mobile number.input 10 digits number!");
        return false;
    }
    $('.number').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
</script>
</body>
</html>