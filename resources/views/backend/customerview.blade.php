<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Garruda</title>

    <!-- Bootstrap -->
    <link href="https://cdn.ncsu.edu/brand-assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	
	<!--font family-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">

	<style>
    <?php $uniuntiretary=['AN','CH','DN','DD','DL','LD','PY'];
      $rostate='';
      $cusstate='';

    ?>
body{
	margin:0px;
	padding:0px;
	background:#eee;
	font-family: 'Open Sans', sans-serif;
	font-size:14px;
	color:#222;

}
h1,h2,h3,h4,h5,h6{
	margin:0px;
}
.header_top{
	background:#;	
}
.header_top h1{
	font-size:24px;
	font-weight:600;
	padding:15px 0 ;
}
.titlehead{
	
	margin:0 auto;
	
	margin-bottom:10px !important;
}
.logosection{
	background:#fff;
	padding:20px;
}
.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    padding: 8px;
    line-height: 1.6;
    vertical-align: top;
    /* border-top: 1px solid #eee; */
    border: 1px solid #444;
	
}
address{
	margin-bottom:0px;
}
small{
	font-size:10px !important;
}
.clear{
	
	clear:both;
}

	</style>
  </head>
  <body>
	<!--header-->
		<header>
			
			<div class="container">
				
				<div class="row">
					<div class="col-md-12">
						
						<div class="logosection">
						<div class="header_top text-center">
							<h1>Tax Invoice</h1>
							<div class="titlehead"></div>
						</div>

							<table class="table">

  <tbody>
    <tr>
      <td scope="row" rowspan="3"  width="20px"><a href="#"><img src="{{URL::asset('images')}}/logo.png"></a></td>
      <td rowspan="3" colspan="2">
		<address>

       
			<strong>{{Auth::user()->getRocode->pump_legal_name}}</strong><br>
			{{Auth::user()->getRocode->pump_address}}<br>
			{{Auth::user()->getRocode->getcity->name}} - {{Auth::user()->getRocode->pin_code}}<br>
		   GSTIN/UIN: {{Auth::user()->getRocode->GST_TIN}}<br>
			State Name : {{Auth::user()->getRocode->getstate->name}}<br>
			<?php $rostate=Auth::user()->getRocode->getstate->statecode; ?>
		</address></td>
      <td colspan="3">Invoice No.<br><strong><?php echo  $invoice_no;?></strong></td>
      <td colspan="5">Dated<br><strong><?php echo date(' d-M-Y'); ?>
</strong>
	  </td>
   </tr>
    <tr>
      <td colspan="3">Mode/Terms of Payment</td>
      <td colspan="5">Immediate</td>
    </tr>
	<tr>
      <td colspan="3" style="color:#fff">ketyk</td>
      <td colspan="5" style="color:#fff">tkety</td>
    </tr>
	<!--middle-->
	
	<tr>
     
      <td  colspan="11">
		<address>
		Buyer<br>
			<strong>{{$tbl_invoice->company_name}}</strong><br>

			{{$tbl_invoice->address_one}}<br>

			@if(trim($tbl_invoice->address_two)!='')
			{{$tbl_invoice->address_two}}<br>
			@endif

			@if(trim($tbl_invoice->address_three)!='')
			{{$tbl_invoice->address_three}}<br>
			@endif
             
			{{$tbl_invoice->getcity->name}} - {{$tbl_invoice->pin_no}}<br>

			Contact : Mr/Mrs. {{$tbl_invoice->Customer_Name}} &nbsp;&nbsp;&nbsp; Ph : {{$tbl_invoice->Phone_Number}}<br>
			State Name : {{$tbl_invoice->getstate->name}}<br>
			Place of Supply : {{$tbl_invoice->getstate->name}}<br>
			<?php $cusstate=$tbl_invoice->getstate->statecode;?>
		</address></td>
      
	  </td>
   
    </tr>
	<tbody>
	</table>
	<!--main section-->
	<table class="table">
	
	<tbody>
	<tr>
      <td width="50">SI No.</td>
      <td width="300">Description</td>
      <td>HSN/SAC</td>
	  <td>Handling Fee %</td>
      <td >Total Amount</td>
	  
      <td align='right' style="padding-right:11px">Amount</td>
   
    </tr>
	<tr>
      <td width="50">&nbsp;</td>
      <td><b><small></small></b></td>
      <td></td>
	  <td></td>
      <td></td>
	  <td></td>
     
   
    </tr>
     <?php $total=0; $amount=0; $i=1; $gst=[];$totalne=0;?>
    @foreach($invoiceCustomers as $invoiceCustomer)
		<tr>
	      <td width="50"><?php echo $i; $i++;  $amount=$invoiceCustomer->amount+$amount; ?></td>
	      <td>
	      	<!--Description of Goods-->
	         {{$invoiceCustomer->invoice_no}}/{{date('d/m/Y',strtotime($singletransactionModel->trans_date))}}/{{$invoiceCustomer->amount}}
		     @if($sevice!=null)
			  {{$sevice->name}}
			@endif
		  </td>
	      <td>
	      	
			@if($sevice!=null)
			  {{$sevice->hsn}}
			@endif
	      	 <!--HSN/SAC-->
	      </td>
		  <td>
            <!--GST %-->
            {{$handling_fee}}%
		  </td>
	      <td>
	      	 <!--MRP/ Marginal-->
	      	  {{$invoiceCustomer->amount}}
	      </td>
		  
	      <td align='right' style="padding-right:15px"> 
	      	<!--Amount-->
	      	<?php 
  		          
  	           if($handling_fee!=0)
	      	            $sur=$invoiceCustomer->amount*$handling_fee/100;
	      	    else    
                  $sur=0;

                $total=$sur+$total;
                $totalne=$sur+$totalne;
			?>

			{{round($sur,2)}}
	      </td>
	   
	    </tr>
	 @endforeach
	
	<tr>
      <td width="50"></td>
      <td>
			<table style="width:100%">
            <tr>
                         	<td colspan="2">&nbsp;</td>
                         </tr>
            <tr>
              <td style="padding-top:-15px;">
				Add :</td>
				<td>
						@if($sevice!=null)
						   @if($fule_type==2)
                                @if($rostate==$cusstate)

                                     @foreach($sevice->gettax->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both']) as $gettaxs)
					      		     	<?php $gst[$gettaxs->Description]=$gettaxs->position;?>
					      	      	 @endforeach

                                @else
                                      @foreach($sevice->gettax->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both']) as $gettaxs)
					      		     	<?php $gst[$gettaxs->Description]=$gettaxs->position;?>
					      	      	  @endforeach

                                @endif

						   @else
					      		@foreach($sevice->gettax as $gettaxs)
					      		     <?php $gst[$gettaxs->Description]=$gettaxs->position;?>
					      	    @endforeach
				      	    @endif
			      	    @endif

						<?php asort($gst)?>
						<table style="width:100%;margin-top:-10px;">
							
							@foreach($gst as $key=>$gsts)
                         <tr>
                         	<td align="right"><strong>{{$key}}</strong></td>
                         </tr>
                         @endforeach
						</table>
              </td>
            </tr>
          </table>
	  </td>
      <td>
	  </td>
	  <td>
	  
	  </td>
      <td></td>
	  
	  	    <?php $tearray=[];?>
	  	    @if($sevice!=null)

			   @if($fule_type==2)
                    @if($rostate==$cusstate)

                         @foreach($sevice->gettax->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both']) as $gettaxs)
		      		     	<?php $tearray[$gettaxs->Description]=$gettaxs->Tax_percentage;?>
		      	      	 @endforeach

                    @else
                          @foreach($sevice->gettax->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both']) as $gettaxs)
		      		     	<?php $tearray[$gettaxs->Description]=$gettaxs->Tax_percentage;?>
		      	      	  @endforeach
		      	      	  
                    @endif

			   @else
		      		@foreach($sevice->gettax as $gettaxs)
		      		     <?php $tearray[$gettaxs->Description]=$gettaxs->Tax_percentage;?>
		      	    @endforeach
	      	    @endif

      	    @endif

            
          
      <td>
		<table width="100%">
            	<?php $totals=round($total,2);?>
						<?php $tearray=[];?>
                        
                        @if($sevice!=null)

						   @if($fule_type==2)
			                    @if($rostate==$cusstate)

			                         @foreach($sevice->gettax->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both']) as $gettaxs)
					      		     	<?php   $sur=0;
							                if($handling_fee!=0)
							      	            $sur=$totalne*$gettaxs->Tax_percentage/100; 
							      	        else
						                       $sur=$totalne*$gettaxs->Tax_percentage/100; 
		                                  
		                                     $tearray[$gettaxs->Description]=round($sur,2);
							      	        
		                                     $total=round($sur,2)+$total;
				                		 ?>
					      	      	 @endforeach

			                    @else
			                          @foreach($sevice->gettax->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both']) as $gettaxs)
					      		     	<?php   $sur=0;
							                if($handling_fee!=0)
							      	            $sur=$totalne*$gettaxs->Tax_percentage/100; 
							      	        else
						                       $sur=$totalne*$gettaxs->Tax_percentage/100; 
		                                  
		                                     $tearray[$gettaxs->Description]=round($sur,2);
							      	        
		                                     $total=round($sur,2)+$total;
				                		 ?>
					      	      	  @endforeach
					      	      	  
			                    @endif

						   @else
					      		@foreach($sevice->gettax as $gettaxs)
					      		    <?php   $sur=0;
							                if($handling_fee!=0)
							      	            $sur=$totalne*$gettaxs->Tax_percentage/100; 
							      	        else
						                       $sur=$totalne*$gettaxs->Tax_percentage/100; 
		                                  
		                                     $tearray[$gettaxs->Description]=round($sur,2);
							      	        
		                                     $total=round($sur,2)+$total;
				                	?>
					      	    @endforeach
				      	    @endif
				      	    
			      	    @endif
			      	<tr width="100%">
                     <td align='right' style="padding-right:8px"><strong>{{bcadd(round($totals,2), 0, 2)}}</strong></td>
					</tr>
					@foreach($gst as $key=>$gsts)
					   <tr width="100%">
	                       <td align='right' style="padding-right:8px"><strong>{{ bcadd(round($tearray[$key],2), 0, 2)}}</strong></td>
					   </tr>
					@endforeach

          </table>
	  </td>
	
    </tr>
	<tr>
      <td width="50"></td>
      <td align='right'>Total</td>
      <td></td>
	  <td></td>
      <td></td>
	 
      <td align='right' style="padding-right:15px"><strong>INR {{bcadd(round($total,2), 0, 2)}}</strong></td>
	
    </tr>
  </tbody>
</table>

<?php 
     $sums=round($total,2); 
     $va=explode('.',$sums);
     if(isset($va[1]) && intval($va[1])>50){
       $no = round($sums);
       $no = $no-1;
     }else{
     	$no = round($sums);
     }
   
   $point = intval($va[1]);
   
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array('0' => '', '1' => 'One', '2' => 'Two',
    '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
    '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
    '10' => 'Sen', '11' => 'Eleven', '12' => 'Twelve',
    '13' => 'Thirteen', '14' => 'Fourteen',
    '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
    '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
    '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
    '60' => 'Sixty', '70' => 'Seventy',
    '80' => 'Eighty', '90' => 'Ninety');
   $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $sums = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($sums) {
        $plural = (($counter = count($str)) && $sums > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($sums < 21) ? $words[$sums] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($sums / 10) * 10]
            . " " . $words[$sums % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode('', $str);
  $points = ($point) ?
    "." . $words[$point / 10] . " " . 
          $words[$point = $point % 10] : '';
			 ?>
<!--footer section-->

<table class="table">
	
	<tbody>
   @if($fule_type==2)
	<tr>
      <td  colspan="{{count($gst)+10}}" >
	  <div class="pull-left">
		Amount Chargeable (in words)<br><strong>
		@if($points)
		  INR <?php  echo $result . "Rupees  " . $points . " Paise"; ?> Only</strong>
		@else
		  INR <?php  echo $result . "Rupees  "; ?> Only</strong>
		@endif
	  </div>
	  <div class="pull-right"><i>E. & O.E</i></div><br/><br/><br/>
		
	 </td>
     
   
    </tr>
	<tr>
      <?php 
           $colvalu=0;
	      if(count($gst)==0){
               $colvalu=3;
	        }
        ?>
      <td colspan="{{$colvalu+6}}" rowspan="2" align='center' width="360"><strong>HSN/SAC</strong><br></td>
      <td rowspan="2">Taxable Value</td>
      
	  @foreach($gst as $key=>$gsts)
      <?php $gst[$key]=0; ?>
	  <td colspan="2">{{$key}}</td>
	  @endforeach
	  
	  <td rowspan="2" align='right' style="padding-right:15px">Total Tax Amount</td>
    
    </tr>
	<tr>
	
	  @foreach($gst as $key=>$gsts)
		  <td>Rate</td>
	      <td align='right' style="padding-right:15px">Amount</td>
	  @endforeach

    </tr>
	<tr>
	  <td colspan="{{$colvalu+6}}"  width="360">
	  	<!--hsn code -->
			@if($sevice!=null)
			  {{$sevice->hsn}}
			@endif
		</td>
      <td>{{$totals}}</td>
      <?php $gstva=0;?>
	   @foreach($gst as $key=>$gsts)
		  <td>
             @if($fule_type==2)
	                @if($rostate==$cusstate)

	                     @foreach($sevice->gettax->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['intrastate','both'])->where('Description',$key) as $gettaxs)
		      		     	{{$gettaxs->Tax_percentage}}% <br/>
		      	      	 @endforeach

	                @else
	                      @foreach($sevice->gettax->where('Tax_Code','!=', 'VAT')->whereIn('strate_type',['interstate','both'])->where('Description',$key) as $gettaxs)
		      		     	{{$gettaxs->Tax_percentage}}% <br/>
		      	      	  @endforeach
		      	      	  
	                @endif

	         @endif       
		  </td>

	      <td align='right' style="padding-right:15px">
	      	<?php $gstva=$tearray[$key]+$gstva;?>
	      	{{bcadd(round($tearray[$key],2), 0, 2)}}
	      	
	      </td>
	  @endforeach
	   

      <td align='right' style="padding-right:15px">
      	{{bcadd(round($gstva,2), 0, 2)}}
      </td>
    </tr>

	<tr>
	  <td colspan="{{$colvalu+6}}"  width="360" align="right"><strong>Total</strong></td>
      <td></td>
	  
       @foreach($gst as $key=>$gsts)
         
		  <td></td>
          <td align='right' style="padding-right:15px"><b>{{bcadd(round($tearray[$key],2), 0, 2)}}</b></td>
		@endforeach
       <td align='right' style="padding-right:15px"><b>INR {{bcadd(round($gstva,2), 0, 2)}}</b></td>
	    </tr>
	@endif
	<tr>
	  <td colspan="{{count($gst)+10}}">Terms & Conditions :</td>
    </tr>
	<tr>
	  
	  <td colspan="{{count($gst)+5}}" rowspan="3" style="padding:20px 8px">Terms & Conditions :</td>
	  <td colspan="5" rowspan="3" style="padding:20px 8px"><strong>for {{Auth::user()->getRocode->pump_legal_name}} @ {{Auth::user()->getRocode->pump_address}}</strong>
	  <br><br>
	  <i>Authorised Signatory</i>
	  </td>
    </tr>
	
	

  </tbody>
</table>
{{dd('hello')}}
<p style="text-align:center">This is a Computer Generated Invoice</p>

						</div>
					</div>
				</div>
			</div>
		</header>
	<!--header-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://cdn.ncsu.edu/brand-assets/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>