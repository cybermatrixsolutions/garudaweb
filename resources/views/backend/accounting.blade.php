<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<link href="{{URL::asset('css/style.css')}}" type="text/css" rel="stylesheet">

<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<title>Garruda</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper"> 
  <!-- Navbar-->
  
  
   @include('backend-includes.header')
 
  <!-- Side-Nav-->
  
  @include('backend-includes.sidebar')  
  
  
 
  <!-- Side-Nav-->
  
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Accounting Integration </h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Accounting Integration </a></li>
        </ul>
      </div>
    </div>
	
	                        <div class="">
							      
                            </div>
	
	
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="row">
           <center>@if(Session::has('success'))
                                   <font style="color:red">{!!session('success')!!}</font>
                                   @endif</center>
          <div class="col-md-6 col-sm-6 col-xs-12 vat">
            <div> <a href="#" data-toggle="modal" data-target="#myModal" title="Add"><i class="fa fa-plus-square-o"></i></a></div>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></div>
                    <h4 class="modal-title" id="myModalLabel">Add Accounting Ledger </h4>
                  </div>
				
                  <div class="modal-body">
                    <div class="row">
                      <form class="form-horizontal" action="{{'add_accounting'}}" enctype="multipart/form-data" method="post">
					   <input type="hidden" name="_token" value="{{ csrf_token()}}"/> 
                        <div class="form-group">
                          <label class="control-label col-sm-3" for="pwd">Master Code <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="Master_Code" name="Master_Code"  placeholder="Master Code" required/>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="control-label col-sm-3" for="email">Master Name <span style="color:#f70b0b;">*</span></label>
                          <div class="col-sm-9">
                        	   <input type="text" class="form-control" id="" name="Master_Name" placeholder="Master Name" required/>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <!--<button type="submit" class="btn btn-default">Submit</button>-->
							
							<center><input type="submit" id="success-btn" class="btn btn-primary site-btn" value="Submit"></center>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-striped" id="myTable">
            <thead>
              <tr>
                <th>S.No</th>
                <th>Master Code</th>
                <th>Master Name</th>
                <th>Status</th>
                 <th>Actions</th>
            </thead>
            <tbody>
			
			<?php $i=0; ?>
			
	               @foreach($getAccounting as $row) 
			
	            <?php $i++ ;?>
              <tr>
			  
			  <td>{{$i}}</td>
                <td scope="row">{{$row->Master_Code}}</td>
                <td>{{$row->Master_Name}}</td>
				
				<td> <?php if($row->is_active==0){?>
                      <a  title="Active" href="{{URL('accountingDeactive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to activate?');">
                        <img src="{{URL::asset('images')}}/test-fail-icon.png" style="">

                        </a>
                   
								  
									<?php }else{ ?>

									

                    <a title="Deactive" href="{{URL('accountingActive')}}/<?php echo $row->id;?>" onclick="return confirm('Do you want to deactivate?');">

                    <img src="{{URL::asset('images')}}/test-pass-icon.png" style="">

                    </a>
						 <?php } ?> 
			    </td>
                <td><div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>
                    <ul class="dropdown-menu" role="menu">
                     <!-- <li><a href="#">View</a></li>-->
                      @if($row->is_active==1)
                        <li><a href="accounting_update/{{$row->id}}">Edit</a></li>
                      @endif
                     <!-- <li><a href="accounting_delete/{{$row->id}}" onClick="return confirm('Are you sure?');">Delete</a></li>-->
                    </ul>
                  </div>
               </td>
              </tr>
			  @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="footer-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <span>&#169; Ashwini Agencies Pvt Limited All rights reserved - 2018</span>
        </div>
        <div class="col-md-6">
           <span style="color: #fff;">Version: 1.0   Release 1.0</span>
          <img src="{{URL::asset('images')}}/ft-logo2.png" class="pull-right">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Javascripts--> 

<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script>
<script src="{{URL::asset('js/capitalize.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
<script type="text/javascript">
 jQuery(function(){

     var vil={
           init:function(){
             vil.selectRo();
         
           },
           selectRo:function(){

            jQuery('#Master_Code').on('blur',function(){
                
                  udat=jQuery(this).val();
                  jQuery.get('check_Pedestal_Number_unique',{
                  table:'tbl_accounting_ledger_master',
                  colum:' Master_Code',
                  unik:udat,
               
                    '_token': jQuery('meta[name="csrf-token"]').attr('content'),
                   },function(data){
                    console.log(data);
                   if(data=='true'){
                      alert('Master Code already exists Enter Other!');
                    jQuery('.submit-button').attr('disabled',true);

                   }else{
                    jQuery('.submit-button').attr('disabled',false);
                   }
                       
                   });
              });
            
           
           },
          
     }
     vil.init();
  });



</script>
<script>$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "auto" );
})
</script>
</body>
</html>