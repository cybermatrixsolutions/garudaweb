@extends('backend-includes.app')

@section('content')
<div class="content-wrapper">
  <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i>&nbsp;Lob Info</h1>
      </div>
      <div>
        <ul class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i></a></li>

          <li><a href="#">Lob Info</a></li>
        </ul>
      </div>

    </div>
  <div class="row">

   <form class="form-horizontal col-md-12" action="{{'roLobUpdateSave'}}" method="post" onsubmit="return confirm('Do you want to Continue?');">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group" style="margin-top:45px!important; padding:0px 15px;">
                <div class="row" style="margin-top:0px!important">                  
                  <div style="margin-bottom:96px">
                   <h4>Default LOB'S</h4>
                     @foreach($RoLobSelectionModel as $lob)
                      
                          <div class="col-md-2">
                            <div class="card" style="padding:5px 5px 5px 5px; margin-bottom:8px; text-align:center;">
 
                              {{$lob}}
                              </div>
                              </div>
                            
                             
                           
                     @endforeach
             </div>
             </div>
              <div class="row">      
              <div style="margin-bottom:30px">
             
                    <h4> Select LOB'S</h4>
                   
                  @foreach($lob_item_list as $lob_list)
                    @if($lob_list->lob_name!='LUBES' && $lob_list->lob_name!='FUELS')
                          
                              <div class="col-md-4">
                            <div class="card" style="padding:5px 5px 5px 5px; margin-bottom:8px;">
                               
                             {{$lob_list->lob_name}}
                           
                          
                              <input style="width:20%" class="pull-right" type="checkbox" name="lob_name[]" value="{{$lob_list->lob_name}}">
                         </div>
                         </div>
                         
                      @endif
                  @endforeach
             </div>
             </div>
               <!--  <div class="form-group">
                  <label class="control-label col-sm-3" for="email">LOB</label>
                  <div class="col-sm-9">
                      <select class="form-control" required="required" name="lob_name[]"   id="tags" multiple="multiple">
                          @foreach($lob_item_list as $lob_list)
                              <option value="{{$lob_list->lob_name}}">{{$lob_list->lob_name}}</option>    
                          @endforeach
                      </select>
                  </div>
                </div> -->
              @if($lob_item_list->count()>0)
              <div class="form-group" style="margin-top:60px!important;">
                <div>
                  <button type="submit" class="btn btn-default">Submit</button>
                </div>
              </div>
              @endif
          </form>
          </div>
  </div>
@endsection