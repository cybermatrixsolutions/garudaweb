<style>
 .liover{
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
</style>


<aside class="main-sidebar hidden-print">
  <section class="sidebar">
    <a class="logo" href="#">
      <img src="{{URL::to('/')}}/images/logo/garruda.jpg" alt="garruda" title="garruda">
    </a>
    <div class="user-panel">
        <div >
          
        </div>
      </div>
   
       <?php

        $status=0;
        if(Auth::user()->user_type==3){
          $status=Auth::user()->getRocode->is_active;
        }elseif(Auth::user()->user_type==4){
            $status=Auth::user()->customer->is_active;
        }else{
          if(Auth::user()->getPersonel!=null)
          $status=Auth::user()->getPersonel->is_active;
          
        }
       ?>
      
      <!-- Sidebar Menu-->
      <ul class="sidebar-menu" id="nav">
         @if(Auth::user()->user_type==1 || Auth::user()->user_type==2)

         <li class="treeview"><a href="#"><span> Basic Config</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
                
                <li class="treeview"><a href="#"><span> RO Config</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                     <li><a href="{{URL('roconfig')}}"><span> General </span></a></li>
                     
                     <li><a href="{{URL('masterpayment')}}"><span> Payment Modes</span></a></li>
                  </ul>
                </li>

                <li class="treeview"><a href="#"><span>Items Config</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                     <li><a href="{{URL('unit_of_measure')}}"><span> Unit of Measures</span></a></li>
                     <li><a href="{{URL('stock_item_group')}}"><span> Stock Item Groups</span></a></li>
                   
                  </ul>
                </li>
                <li class="treeview"><a href="#"><span>Other Config</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                     <li><a href="{{URL('taxrates')}}"><span> Tax Config</span></a></li>
                
                     <li><a href="{{URL('lob_master_page')}}"><span> LOB</span></a></li>
                     <li><a href="{{URL('add_state_page')}}"><span> States </span></a></li>
                   
                  </ul>
                </li>
                <li class="treeview"><a href="#"><span>Vehicle</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="{{URL('vechilemake')}}"><span> Vehicle Make</span></a></li> 
                    <li><a href="{{URL('vechilemodel')}}"><span> Vehicle Model </span></a></li>
                  </ul>
                </li>
                 <li class="treeview"><a href="#"><span>Industry/Depart</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="{{URL('industry')}}"><span> Industry /Department</span></a></li> 
                    <!--<li><a href="{{URL('department')}}"><span> Department </span></a></li>-->
                  </ul>
                </li>
                 <li><a href="{{URL('designation')}}"><span> Role </span></a></li>
                  <li><a href="{{URL('accounting')}}"><span> Accounts Integration</span></a></li>
                  <li class="treeview"><a href="{{route('smssetting')}}"><span>Sms Setting</span></a></li>


              

              </ul>
          </li>
          <li class="treeview"><a href="#"><span> Other Config</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="{{URL('principal_listing')}}"> Principal Company</a></li>

              <li class="treeview"><a href="#"><span>Stock Items</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                     <li><a href="{{URL('stock_item_management')}}"><span> Common Items</span></a></li>
                      <li><a href="{{URL('dipCharts')}}"> Dip Chart </a></li>
                   
                  </ul>
                </li>
                 <li><a href="{{URL('retail')}}"> Retail Outlet </a></li>
                 
              </ul>
          </li>
       
        @endif

       @if(Auth::user()->user_type==1 || Auth::user()->user_type==3 && $status==1)
       <?php  
             $rocode='';
              $confirm=0;
               
              $id=0;
             if (Auth::user()->getRocode!=null) {
                $rocode=Auth::user()->getRocode->RO_code;
                $confirm=Auth::user()->getRocode->confirm;
               
                $id=Auth::user()->getRocode->id;
             }

          $lov=App\RoLobSelectionModel::where('RO_code',$rocode)->get();
		  $roManualSlipConfig = App\OutletConfigModel::where('rocode',$rocode)->where('field_name','use_manual_slip_book')->get()->first();
		  ?>
          @if( $lov->count()>0 && $confirm==1 && $status==1)
         
            <li class="treeview"><a href="#"><span> Basic Config</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
                <li class="treeview"><a href="#"><span> RO Config</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                       <li><a href="{{URL('outletconfig')}}"><span> General </span></a></li>
                       <li><a href="{{URL('shifttype')}}"><span> Shift </span></a></li>


                       <li class="treeview"><a href=""><span> Tally Config </span><i class="fa fa-angle-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{Route('tallyConfig')}}"><span> General</span></a></li>
                             <li><a href="{{Route('tallyConfigMaster')}}"><span> Master</span></a></li>
                            <li><a href="{{URL('tallyConfigfuel')}}"><span> Daily Fuel Sales</span></a></li>
                            <li><a href="{{URL('tallyConfigsettlement')}}"><span> Daily Shift Settlement</span></a></li>
                            <li><a href="{{URL('tallyConfigGST')}}"><span>Gst Slip Invoices</span></a></li>
                            <li><a href="{{URL('tallyConfigDailyShift')}}"><span> Periodic VAT Bill (Fuel)</span></a></li>
                            <li><a href="{{URL('tallyConfigPeriodic')}}"><span> Periodic GST Bill (Others)</span></a></li>
                            <li><a href="{{URL('tallyConfigBillingCharges')}}"><span> Periodic Separate Billing Charges</span></a></li>
                             <li><a href="{{URL('tallyConfigRestView')}}"><span> Tally Data Rest</span></a></li>


                            
                          </ul>
                       </li>


                      <li><a href="{{URL('retail_update/'.$id)}}"><span> RO Info </span></a></li>
                       <li><a href="{{URL('roLobUpdate')}}"><span> LOB Info </span></a></li>
                        <li><a href="{{URL('paymentmood')}}"><span> Payment Modes</span></a></li>
                  </ul>
                </li>

                <li class="treeview"><a href="#"><span>Tax Config</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href="{{URL('tax')}}"><span> Tax Structure </span></a></li>
                      <li><a href="{{URL('item_services')}}"><span> Services Master</span></a></li>
                      <li><a href="{{URL('item_tax_allo')}}"> Item Tax Config </a></li>
           
                  </ul>
                </li>
                <li><a href="{{URL('persoonel')}}"><span> Personnel & Staff </span></a></li>
                <li><a href="{{URL('price_list')}}"><span> Stock Item Master </span></a></li>

                <li class="treeview"><a href="#"> Customer Master<i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="{{URL('Customer')}}"> Customer</a></li>
                    <li><a href="{{URL('vehiclemanagement')}}">  Vehicle </a></li>
                    <li><a href="{{URL('driver')}}">  Driver </a></li>
                    <li><a href="{{URL('credit_limit')}}"> Credit Limits Update</a></li>
                    <li><a href="{{URL('qr_code')}}"> Vehicle QR Code </a></li>
                   
                  </ul>
                </li>
                <li class="treeview"><a href="#"> Infra Master<i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                     <li><a href="{{URL('tank')}}"><span> Tanks</span></a></li>
                     <li><a href="{{URL('nozzles')}}"> Pedestals</a></li>
                     <li><a href="{{URL('pedestal')}}"> Nozzles </a></li>
                     

                    
                  </ul>
                </li>
				<?php
				if($roManualSlipConfig && $roManualSlipConfig->value == 'on'){
				?>
					<li><a href="{{route('slipbook')}}"><span> Add Slip Book </span></i></a></li>
					<li><a href="{{route('slipbooklist')}}"><span> Slip Book List</span></i></a></li>
				<?php
				}
				?>
              </ul>
          </li>


          <li class="treeview"><a href="#"><span> Transactions </span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
				<?php
				if($roManualSlipConfig && $roManualSlipConfig->value == 'on'){
				?>
					<li class="treeview"><a href="#"><span>Customer Transactions</span><i class="fa fa-angle-right"></i></a>
					  <ul class="treeview-menu">
						   <li><a href="{{URL('customer_slip_entry_page')}}"><span> Fuel Slip Entry</span></a></li>
						   <li><a href="{{URL('customer_slip_entry_lube_page')}}"><span> Other Item Slip Entry</span></a></li>
						   <li><a href="{{URL('ro_transaction_list')}}"><span> View & Alter Slip Entry</span></a></li>
			
					  </ul>
					</li>
				<?php
				}
				?>	
                <li class="treeview"><a href="#"><span>Billing & Settlement</span><i class="fa fa-angle-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href="{{URL('invoiceindex')}}"> Create Products Bills </a></li>
                      <li><a href="{{URL('customer_commision')}}"> Create Services Bill</a></li>
                      <li><a href="{{URL('billsGenerated')}}"> Bills Generated</a></li>
                      <li><a href="{{URL('invoicePendingCollection')}}"><span>Bills Settlement</span></a></li>
                      <!--<li><a href="{{URL('slipPendingPrinting')}}"><span>Commission Settlement</span></a></li>-->
           
                  </ul>
                </li>
              </ul>
          </li>

        @endif
        
        @endif
       
		<?php
		$customerInfo = App\RoCustomertManagement::where('Email',Auth::user()->email)->where('RO_code',Session::get('ro'))->first();
		?>
         @if(Auth::user()->user_type==1 || Auth::user()->user_type==4 && Session::has('ro') && $status==1 && $customerInfo->confirm==1)
			
          <li class="treeview"><a href="#"><span> Basic Config </span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="{{url('customerprofile')}}/{{Auth::user()->id}}"><span> Customer Info </span></a></li>
                <li><a href="{{url('changeRo')}}"><span> Change Ro </span></a></li>
              </ul>
          </li>
          <li class="treeview"><a href="#"><span> Masters </span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
                  <li><a href="{{URL('customer_vehiclemanagement')}}">  Vehicle </a></li>
                  <li><a href="{{URL('customer_driver')}}"> Driver </a></li>
              </ul>
          </li>
          <li class="treeview"><a href="#"><span> Transactions </span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
                  <li><a href="{{URL('add_petrol_diesel')}}"><span> Request Fuel</span></a></li>
                  <li><a href="{{URL('lube_request')}}"><span> Request Other Items </span></a></li>
              </ul>
          </li>
          <li class="treeview"><a href="#"><span> Reports </span><i class="fa fa-angle-right"></i></a>
            <ul class="treeview-menu">

                <li><a href="{{route('deliveriesPendingReport')}}" class="liover" title="Party-Items-Datewise Pending Delivery"> Pending Deliveries</a></li>
                <li><a href="{{route('partywise-Vehiclewise-Sales-Report')}}" class="liover" title="Detailed Sales">Detailed Sales</a></li>
                <li><a href="{{route('Party-Driver-Vehicle-Listings')}}" class="liover" title="Party-Driver-Vehicle">Party-Driver-Vehicle</a></li>
                 <li><a href="{{URL('billsGenerated')}}"> Bills Generated</a></li>
            </ul>
          </li>
        
         @endif

          @if(Auth::user()->user_type==5 || Auth::user()->user_type==3 && $status==1)
          <?php  
             $rocode='';
             $confirm=0;
             if (Auth::user()->getRocode!=null) {
                $rocode=Auth::user()->getRocode->RO_code;
                 
                  $confirm=Auth::user()->getRocode->confirm;
             }

          $lov=App\RoLobSelectionModel::where('RO_code',$rocode)->get();?>
          @if( $lov->count()>0 && $confirm==1 || Auth::user()->user_type==5 && $status==1)
           

           <li class="treeview"><a href="#"><span> RO Operations </span><i class="fa fa-angle-right"></i></a>
              
              <ul class="treeview-menu">
                  <li class="treeview"><a href="{{route('tankInward')}}"><span> Tank Inward </span></a>  
                  <li><a href="{{URL('shift/create')}}"><span> Shift Allocation </span></a></li>
                  <li><a href="{{URL('shift/closer')}}"><span> Nozzle CMR</span></a></li>
                  <li><a href="{{URL('shift/settlement')}}"><span> Shift Settlement</span>
                  <li><a href="{{URL('shift/settlementconfirm')}}"><span> Shift Settlement Confirm</span>
                   <!-- <i class="fa fa-angle-right"></i> --></a>
                   <!--  <ul class="treeview-menu">
                      <li><a href="{{URL('shift/settlement')}}"><span>Fuel Settlements</span></a></li>
                      <li><a href="{{URL('shift/settlementLueb')}}"><span>Lub Settlements</span></a></li>
                      
                    </ul> -->
          
                   
                     
                  </li>
          
           <li><a href="{{URL('tankRiding')}}"><span> Dip Readings</span></a></li>
           
                   @if(Auth::user()->user_type==3 && $status==1)           
            <li><a href="#"><span> RO price list</span> <i class="fa fa-angle-right"></i></a>
                    <ul class="treeview-menu">
                      <li><a href="{{URL('fuel_price_management')}}"> Fuel Price </a></li>
                      <li><a href="{{URL('Any_price_management')}}"> Item Price </a></li>
                    </ul> 
                  </li>
            @endif 

                  <li class="treeview"><a href="{{URL('customernewtransactions')}}"><span> Customer Request </span></a>         
					<li class="treeview"><a href="#"><span>Reprint</span><i class="fa fa-angle-right"></i></a>
					  <ul class="treeview-menu">
						  <li><a href="{{route('reprint_settlement')}}"> Settlement </a></li>
              <li><a href="{{route('reprint.delivery-slip-fuel')}}"> Delivery Slip Fuel </a></li>
						  <!-- <li><a href="{{route('reprint.gst-slip-fuel')}}"> GST Slip Fuel </a></li> -->
						 
					  </ul>
					</li>
              </ul>

           </li>
              
              </li>
              
             <li class="treeview"><a href="#"><span> Reports </span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu">
               <!--  <li><a href="{{URL('ro_nozzel_reading')}}"> Nozzle CMR Report </a></li> -->
                 <li><a href="{{URL('salesRegisterBilledSummary')}}"> Sales Register-Summary (Billed)</a></li>
                  <li><a href="{{URL('salesRegisterBilled')}}"> Sales Register (Billed) </a></li>
                  <li><a href="{{route('reading-report')}}" class="liover" title=" Shift Fuels Sales">  Shift Fuels Sales </a></li>
                   <li><a href="{{URL('DailyTotalShiftSalesAndSettlementReport')}}" class="liover" title=" Daily Shift Sales And Settlement"> Daily Shift Sales And Settlement</a></li>
                  <li><a href="{{route('deliveriesPendingReport')}}" class="liover" title="Pending Deliveries"> Pending Deliveries</a></li>
                  <li><a href="{{route('partywise-Credit-Limits-Report')}}" class="liover" title="Credit Limit Status">Credit Limit Status</a></li>
                  <li><a href="{{route('partywise-Vehiclewise-Sales-Report')}}" class="liover" title="Detailed Sales">Detailed Sales</a></li>
                  <li><a href="{{route('SalesmanWise-Sales-Report')}}" class="liover" title="Salesman Wise Sales Report">Salesman Wise Sales</a></li>
                  <li><a href="{{route('Party-Driver-Vehicle-Listings')}}" class="liover" title="Party-Driver-Vehicle">Party-Driver-Vehicle</a></li>
                  <li><a href="{{route('PartyDeliveriesPendingforBilling')}}" class="liover" title="Pending Billings">Pending Billings</a></li>
					
              </ul>
          </li>
        
         @endif
         @endif

      </ul>
    
    </section>
      <div class="lionsymbal">
        <img src="{{URL::asset('')}}images/digital-india.png">
      </div>
  </aside>


 