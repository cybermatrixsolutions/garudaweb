  <header class="main-header hidden-print">
    <nav class="navbar navbar-static-top"> 
      <!-- Sidebar toggle button--><a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
	 
      <!-- Navbar Right Menu--> 
       <div class="navbar-custom-menu">

            <ul class="top-nav">
			 <li style="color:#fff; font-weight:600; font-size:16px; padding-right:8px">Welcome,&nbsp;&nbsp; {{Auth::user()->name}},&nbsp;&nbsp;&nbsp;
        @if(Auth::user()->getRocode!=null && Auth::user()->getRocode->getcity!=null){{Auth::user()->getRocode->getcity->name}}
		@elseif(Auth::user()->customer!=null && Auth::user()->customer->getcity!=null){{Auth::user()->customer->getcity->name}}
        @endif
      </li>
			 <li style="color:#fff; font-weight:600; font-size:16px;"><a href="{{route('dashboard')}}"><i class="fa fa-home fa-lg"></i>&nbsp; Home</a></li>
                
  @guest
                            
                        @else
                                    
                                  @if(Auth::user()->user_type==4)
                                    
                                   <!-- <li><a href="{{url('customerprofile')}}/{{Auth::user()->id}}"><i class="fa fa-user fa-lg"></i> Profile</a></li> -->
                                    @endif
                                    
                                    
                                   
                                    <li style="color:#fff; font-weight:600; font-size:16px;">
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                           <i class="fa fa-sign-out fa-lg"></i> Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                  
                              
                        @endguest

                         <?php 
                            if(Session::has('ro'))
                              {

                             

                            if (Auth::user()->customer!=null) {
                            $Customer_Code=Session::get('custm');
                             $email =Auth::user()->email;
                             $Coscode =App\RoCustomertManagement::where('Customer_Code',$Customer_Code)->where('Email',$email)->first();
                          $RO_code =App\MastorRO::where('RO_code',$Coscode->RO_code)->first();
                           $city =App\CityModel::where('id',$RO_code->city)->first();
                          
                    
                          ?>
                         
                         <h5 style="color:#fff;">{{$RO_code->pump_legal_name}},&nbsp;&nbsp;&nbsp;{{$city->name}}</h5> 
                      <?php }
                       else{

                          echo "";
                       }
                       }
                      ?>

                     
                        
                 
               
            
            </ul>
          </div>
    </nav>

  </header>
<?php 

  $allprices=App\TableItemPriceListModel::where('effective_date','>=',date('Y-m-d').' 00:00:00')->where('effective_date','<=',date('Y-m-d').' 23:59:59')->where('is_active',4)->get();

   foreach ($allprices as $allprice) {
    
     App\TableItemPriceListModel::where('item_id',$allprice->item_id)->where('is_active',1)->update(['is_active' =>0]);
     App\TableItemPriceListModel::where('item_id',$allprice->item_id)->where('is_active',4)->update(['is_active' =>1]);
    
   }
  ?>