<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--bootstrap default css-->
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- CSS-->
<link href="{{URL::asset('css/login_style.css')}}" type="text/css" rel="stylesheet">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Garruda</title>
</head>
<body>
<!--
    you can substitue the span of reauth email for a input with the email and
    include the remember me checkbox
    -->
<div class="container">
  <div class="card card-container"> 
 		<div class="circle"></div>
		<center><p style="color:#fff;">Visibility Simplified</p></center>
    <form class="form-signin" action="{{ route('login') }}" method="post">
	
	<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
      <span id="reauth-email" class="reauth-email"></span>
	  
      <input type="email" id="inputEmail" class="form-control" name='email' placeholder="Username" required autofocus>

      <input type="password" id="inputPassword" class="form-control" name='password' placeholder="Password" required>
	  
	  <button type="submit" class="btn btn-lg btn-success btn-block btn-signin">Log In</button>
	  
	  
    </form>
    <!-- /form --> 
  <a href="resetpassword/reset" class="forgot-password">Forgot the password? </a> </div>
    
  <!-- /card-container --> 
</div>
<!-- /container --> 
<!-- Javascripts--> 
<script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script> 
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('js/plugins/pace.min.js')}}"></script> 
<script src="{{URL::asset('js/main.js')}}"></script>
</body>
</html>